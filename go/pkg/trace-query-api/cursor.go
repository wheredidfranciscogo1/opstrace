package query

import (
	"encoding/base64"
	"encoding/json"
	"fmt"
)

// Page holds the details of the page token needed for the next page.
// Requests requesting the next pages should include this cursor in the page token parameter.
type Page struct {
	LastSeenTimestamp string `json:"last_seen_timestamp,omitempty"`
	LastSeenDuration  uint64 `json:"last_seen_duration,omitempty"`
	// Represents traces that have already been returned in previous page
	// that share the same duration as LastSeenDurationDesc.
	// Used in combination with LastSeenDurationDesc to ensure next page
	// doesn't return traces already included in previous page.
	SeenTraceIds []string `json:"seen_trace_ids"`
}

func decodePage(rawCursor string) (*Page, error) {
	result := &Page{}

	raw, err := base64.StdEncoding.DecodeString(rawCursor)
	if err != nil {
		return nil, fmt.Errorf("cursor decode: %w", err)
	}

	err = json.Unmarshal(raw, result)
	if err != nil {
		return nil, fmt.Errorf("unmarshal cursor: %w", err)
	}

	return result, nil
}

func encodePage(p *Page) (string, error) {
	bts, err := json.Marshal(p)
	if err != nil {
		return "", fmt.Errorf("encode page :%w", err)
	}

	return base64.StdEncoding.EncodeToString(bts), nil
}
