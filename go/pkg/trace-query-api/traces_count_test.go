package query

import (
	"fmt"
	"net/http"
	"net/http/httptest"
	"time"

	"github.com/gin-gonic/gin"
	"github.com/google/go-cmp/cmp"
	. "github.com/onsi/ginkgo/v2"
	. "github.com/onsi/gomega"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/testutils"
)

var _ = Context("trace rate handler", func() {
	var (
		router *gin.Engine
	)

	type expected struct {
		dbErr      error
		statusCode int
		json       string
	}

	test := func(url string, expected expected, expectedFilter *requestParams) {
		q := &mockQueryDB{
			err: expected.dbErr,
		}
		controller := &Controller{
			Q:      q,
			Logger: logger.Desugar(),
		}
		SetRoutes(controller, router)

		recorder := httptest.NewRecorder()
		testReq, err := http.NewRequest(http.MethodGet, url, nil)
		Expect(err).NotTo(HaveOccurred())
		router.ServeHTTP(recorder, testReq)
		Expect(recorder.Code).To(Equal(expected.statusCode))
		if expected.json != "" {
			Expect(recorder).To(HaveHTTPBody(MatchJSON(expected.json)))
		}

		Expect(q.passedFilter).To(Equal(expectedFilter))
	}

	BeforeEach(func() {
		router = gin.New()
	})

	Context("traces RED metrics", func() {
		DescribeTable("list services", test,
			Entry("should return 200 with no parameters", "/v3/query/1/traces/analytics",
				expected{
					statusCode: http.StatusOK,
					json: `{
					  "project_id": 1,
					  "results": [
						{
						  "interval": 1000000,
						  "count": 2,
						  "p90_duration_nano": 1000,
						  "p95_duration_nano": 1000,
						  "p75_duration_nano": 1000,
						  "p50_duration_nano": 1000,
						  "error_count": 3
						}
					  ]
					  }`,
				},
				&requestParams{
					PageSize: 100,
				},
			),
			Entry("should return 400 with invalid project id", "/v3/query/invalid/traces/analytics",
				expected{
					statusCode: http.StatusBadRequest,
				},
				nil,
			),
			Entry("should return 500 with db error", "/v3/query/1/traces/analytics",
				expected{
					dbErr:      fmt.Errorf("test err"),
					statusCode: http.StatusInternalServerError,
				},
				nil,
			),
			Entry("should return 200 with period parameters", "/v3/query/1/traces/analytics?period=5m",
				expected{
					statusCode: http.StatusOK,
					json: `{
					  "project_id": 1,
					  "results": [
						{
						  "interval": 1000000,
						  "count": 2,
						  "p90_duration_nano": 1000,
						  "p95_duration_nano": 1000,
						  "p75_duration_nano": 1000,
						  "p50_duration_nano": 1000,
						  "error_count": 3
						}
					  ]
					  }`,
				},
				&requestParams{
					PageSize: 100,
					Period:   "5m",
				},
			),

			Entry("should return 200 with start and end parameters", "/v3/query/1/traces/analytics?start_time=2023-11-23T11:24:10Z&end_time=2023-11-23T13:24:10Z",
				expected{
					statusCode: http.StatusOK,
					json: `{
					  "project_id": 1,
					  "results": [
						{
						  "interval": 1000000,
						  "count": 2,
						  "p90_duration_nano": 1000,
						  "p95_duration_nano": 1000,
						  "p75_duration_nano": 1000,
						  "p50_duration_nano": 1000,
						  "error_count": 3
						}
					  ]
					  }`,
				},
				&requestParams{
					PageSize:  100,
					StartTime: time.Date(2023, 11, 23, 11, 24, 10, 0, time.UTC),
					EndTime:   time.Date(2023, 11, 23, 13, 24, 10, 0, time.UTC),
				},
			),
		)
	})

	Context("traces rate count query builder", func() {
		var (
			refTime          = time.Now().UTC()
			defaultStartTime = refTime.Add(-1 * 24 * time.Hour).Unix()
			defaultEndTime   = refTime.Add(1 * time.Hour).Unix()
		)

		DescribeTable("builds queries",
			func(projectID int64, params *requestParams, expected string, args []interface{}) {
				builder, _ := buildCountQuery(projectID, params, refTime)
				err := testutils.CompareSQLStrings(builder.sql, expected)
				if err != nil {
					GinkgoWriter.Print("diff: ", cmp.Diff(builder.sql, expected))
				}
				Expect(err).ToNot(HaveOccurred())
				Expect(builder.args).To(Equal(args))
			},
			Entry(
				"trace count query with default period and no filters",
				int64(1),
				&requestParams{},
				`
WITH matched_traces AS (
SELECT TraceId FROM (
WITH
    toStartOfHour(toDateTime($1)) AS r_start,
    toStartOfHour(toDateTime($2)) AS r_end
SELECT
    TraceId
FROM tracing.gl_traces_main
WHERE (Timestamp >= r_start) AND (Timestamp <= r_end) AND (ProjectId = $3)

    GROUP BY TraceId
))

SELECT
    toStartOfHour(Start) AS Interval,
    quantileExact(0.9)(Duration) AS P90Duration,
    quantileExact(0.95)(Duration) AS P95Duration,
    quantileExact(0.75)(Duration) AS P75Duration,
    quantileExact(0.5)(Duration) AS P50Duration,
    count() AS TraceCount
FROM tracing.gl_traces_rolled
WHERE ProjectId = $4 AND ParentSpanId = '' AND TraceId IN matched_traces
GROUP BY ProjectId, Interval
    ORDER BY Interval
`,
				[]interface{}{
					defaultStartTime,
					defaultEndTime,
					"1",
					"1",
				},
			),
			Entry(
				"trace count query with 5m period and no filters",
				int64(1),
				&requestParams{
					Period: "5m",
				},
				`
WITH matched_traces AS (
SELECT TraceId FROM (
WITH
    toStartOfMinute(toDateTime($1)) AS r_start,
    toStartOfMinute(toDateTime($2)) AS r_end
SELECT
    TraceId
FROM tracing.gl_traces_main
WHERE (Timestamp >= r_start) AND (Timestamp <= r_end) AND (ProjectId = $3)

    GROUP BY TraceId
))

SELECT
    toStartOfMinute(Start) AS Interval,
    quantileExact(0.9)(Duration) AS P90Duration,
    quantileExact(0.95)(Duration) AS P95Duration,
    quantileExact(0.75)(Duration) AS P75Duration,
    quantileExact(0.5)(Duration) AS P50Duration,
    count() AS TraceCount
FROM tracing.gl_traces_rolled
WHERE ProjectId = $4 AND ParentSpanId = '' AND TraceId IN matched_traces
GROUP BY ProjectId, Interval
    ORDER BY Interval
`,
				[]interface{}{
					refTime.Add(-1 * 5 * time.Minute).Unix(),
					refTime.Add(1 * time.Minute).Unix(),
					"1",
					"1",
				},
			),
			Entry(
				"trace count query with 5m period and service name and span name filters",
				int64(1),
				&requestParams{
					Period:          "5m",
					ServiceNames:    []string{"service-name"},
					Operations:      []string{"op-1"},
					AttributeNames:  []string{"key"},
					AttributeValues: []string{"value"},
				},
				`
WITH matched_traces AS (
SELECT TraceId FROM (
WITH
    toStartOfMinute(toDateTime($1)) AS r_start,
    toStartOfMinute(toDateTime($2)) AS r_end
SELECT
    TraceId
FROM tracing.gl_traces_main
WHERE (Timestamp >= r_start) AND (Timestamp <= r_end) AND (ProjectId = $3)
 AND ( ServiceName IN ( $4 ) )  AND ( SpanName IN ( $5 ) )
    AND ( ResourceAttributes[$6] = $7 OR SpanAttributes[$8] = $9 ) 
    GROUP BY TraceId
))

SELECT
    toStartOfMinute(Start) AS Interval,
    quantileExact(0.9)(Duration) AS P90Duration,
    quantileExact(0.95)(Duration) AS P95Duration,
    quantileExact(0.75)(Duration) AS P75Duration,
    quantileExact(0.5)(Duration) AS P50Duration,
    count() AS TraceCount
FROM tracing.gl_traces_rolled
WHERE ProjectId = $10 AND ParentSpanId = '' AND TraceId IN matched_traces
GROUP BY ProjectId, Interval
    ORDER BY Interval
`,
				[]interface{}{
					refTime.Add(-1 * 5 * time.Minute).Unix(),
					refTime.Add(1 * time.Minute).Unix(),
					"1",
					[]string{"service-name"},
					[]string{"op-1"},
					"key",
					"value",
					"key",
					"value",
					"1",
				},
			),
		)
	})

	Context("traces error count query builder", func() {
		var (
			refTime          = time.Now().UTC()
			defaultStartTime = refTime.Add(-1 * 24 * time.Hour).Unix()
			defaultEndTime   = refTime.Add(1 * time.Hour).Unix()
		)

		DescribeTable("builds queries",
			func(projectID int64, params *requestParams, expected string, args []interface{}) {
				builder, _ := buildErrorCountQuery(projectID, params, refTime)
				err := testutils.CompareSQLStrings(builder.sql, expected)
				if err != nil {
					GinkgoWriter.Print("diff: ", cmp.Diff(builder.sql, expected))
				}
				Expect(err).ToNot(HaveOccurred())
				Expect(builder.args).To(Equal(args))
			},
			Entry(
				"trace error count query with default period and no filters",
				int64(1),
				&requestParams{},
				`
WITH matched_traces AS (
SELECT TraceId FROM (
WITH
    toStartOfHour(toDateTime($1)) AS r_start,
    toStartOfHour(toDateTime($2)) AS r_end
SELECT
    TraceId
FROM tracing.gl_traces_main
WHERE (Timestamp >= r_start) AND (Timestamp <= r_end) AND (ProjectId = $3)

    AND StatusCode = 'STATUS_CODE_ERROR'
    GROUP BY TraceId
))

SELECT
    toStartOfHour(Start) AS Interval,
    count() AS TraceCount
FROM tracing.gl_traces_rolled
WHERE ProjectId = $4 AND ParentSpanId = '' AND TraceId IN matched_traces
GROUP BY ProjectId, Interval
    ORDER BY Interval
`,
				[]interface{}{
					defaultStartTime,
					defaultEndTime,
					"1",
					"1",
				},
			),
			Entry(
				"trace count query with 5m period and service name and span name filters",
				int64(1),
				&requestParams{
					Period:          "5m",
					ServiceNames:    []string{"service-name"},
					Operations:      []string{"op-1"},
					AttributeNames:  []string{"key"},
					AttributeValues: []string{"value"},
				},
				`
WITH matched_traces AS (
SELECT TraceId FROM (
WITH
    toStartOfMinute(toDateTime($1)) AS r_start,
    toStartOfMinute(toDateTime($2)) AS r_end
SELECT
    TraceId
FROM tracing.gl_traces_main
WHERE (Timestamp >= r_start) AND (Timestamp <= r_end) AND (ProjectId = $3)
 AND ( ServiceName IN ( $4 ) )  AND ( SpanName IN ( $5 ) )
    AND ( ResourceAttributes[$6] = $7 OR SpanAttributes[$8] = $9 ) 
    AND StatusCode = 'STATUS_CODE_ERROR'
    GROUP BY TraceId
))

SELECT
    toStartOfMinute(Start) AS Interval,
    count() AS TraceCount
FROM tracing.gl_traces_rolled
WHERE ProjectId = $10 AND ParentSpanId = '' AND TraceId IN matched_traces
GROUP BY ProjectId, Interval
    ORDER BY Interval
`,
				[]interface{}{
					refTime.Add(-1 * 5 * time.Minute).Unix(),
					refTime.Add(1 * time.Minute).Unix(),
					"1",
					[]string{"service-name"},
					[]string{"op-1"},
					"key",
					"value",
					"key",
					"value",
					"1",
				},
			),
		)
	})
})
