package query

import (
	"context"
	"fmt"
	"net/http"
	"strconv"
	"time"

	"github.com/gin-gonic/gin"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/constants"
	"go.uber.org/zap"
)

type SpanDetailedItem struct {
	Timestamp          time.Time         `json:"timestamp"`
	TimestampNano      int64             `json:"timestamp_nano"`
	SpanID             string            `json:"span_id"`
	TraceID            string            `json:"trace_id"`
	ServiceName        string            `json:"service_name"`
	Operation          string            `json:"operation"`
	DurationNano       int64             `json:"duration_nano"`
	ParentSpanID       string            `json:"parent_span_id"`
	StatusCode         string            `json:"status_code"`
	StatusMessage      string            `json:"status_message"`
	TraceState         string            `json:"trace_state"`
	SpanKind           string            `json:"span_kind"`
	ScopeName          string            `json:"scope_name"`
	ScopeVersion       string            `json:"scope_version"`
	SpanAttributes     map[string]string `json:"span_attributes"`
	ResourceAttributes map[string]string `json:"resource_attributes"`
}

type TraceDetailedItem struct {
	Timestamp     time.Time          `json:"timestamp"`
	TimestampNano int64              `json:"timestamp_nano"`
	TraceID       string             `json:"trace_id"`
	ServiceName   string             `json:"service_name"`
	Operation     string             `json:"operation"`
	StatusCode    string             `json:"status_code"`
	DurationNano  int64              `json:"duration_nano"`
	TotalSpans    int64              `json:"total_spans"`
	Spans         []SpanDetailedItem `json:"spans"`
}

type traceHandlerParams struct {
	projectIDParam
	TraceID string `uri:"id" binding:"required,uuid"`
}

func (c *Controller) TraceHandler(ctx *gin.Context) {
	handlerParams := &traceHandlerParams{}
	if err := ctx.BindUri(handlerParams); err != nil {
		c.Logger.Error("bind error", zap.Error(err))
		// context already canceled by BindUri

		return
	}
	projectID := handlerParams.ProjectID
	traceID := handlerParams.TraceID

	trace, err := c.Q.GetTrace(ctx, projectID, traceID)
	if err != nil {
		c.Logger.With(
			zap.String("trace_id", traceID),
			zap.Int64("project_id", projectID)).
			Error("get trace", zap.Error(err))
		ctx.AbortWithError(http.StatusInternalServerError, err)
		return
	}
	if trace != nil {
		ctx.JSON(200, trace)
	} else {
		ctx.AbortWithStatus(404)
	}
}

func (q *querier) GetTrace(ctx context.Context, projectID int64, traceID string) (*TraceDetailedItem, error) {
	// Subquery has rootSpanOrderKey and is included in the ORDER BY.
	// Outer SELECT drops this field.
	const query = `
SELECT
    Timestamp,
    SpanID,
    Operation,
    DurationNano,
    TraceID,
    ParentSpanID,
    TraceState,
    SpanKind,
    ServiceName,
    ResourceAttributes,
    ScopeName,
    ScopeVersion,
    SpanAttributes,
    StatusCode,
    StatusMessage
FROM (SELECT
    Timestamp,
    hex(SpanId) as SpanID,
    SpanName as Operation,
    Duration as DurationNano,
    UUIDNumToString(TraceId) as TraceID,
    if(ParentSpanId = '', '', hex(ParentSpanId)) as ParentSpanID,
    TraceState,
    SpanKind,
    ServiceName,
    ResourceAttributes,
    ScopeName,
    ScopeVersion,
    SpanAttributes,
    StatusCode,
    StatusMessage,
    if(ParentSpanId = '', 1, 2) as rootSpanOrderKey
FROM ` + constants.TracingDistTableName + `
WHERE ProjectId = ? AND TraceId = UUIDStringToNum(?)
ORDER BY (rootSpanOrderKey, Timestamp) ASC)`

	var (
		spans []SpanDetailedItem
		err   error
	)
	if q.cloudDB != nil {
		err = q.cloudDB.Select(ctx, &spans, query, strconv.Itoa(int(projectID)), traceID)
	} else {
		err = q.db.Select(ctx, &spans, query, strconv.Itoa(int(projectID)), traceID)
	}
	if err != nil {
		return nil, fmt.Errorf("query spans: %w", err)
	}
	if len(spans) == 0 {
		return nil, nil
	}

	for i := range spans {
		spans[i].TimestampNano = spans[i].Timestamp.UnixNano()
	}

	trace := TraceDetailedItem{}
	trace.Timestamp = spans[0].Timestamp
	trace.TimestampNano = trace.Timestamp.UnixNano()
	trace.TraceID = spans[0].TraceID
	trace.ServiceName = spans[0].ServiceName
	trace.Operation = spans[0].Operation
	trace.StatusCode = spans[0].StatusCode
	trace.DurationNano = spans[0].DurationNano
	trace.Spans = spans
	trace.TotalSpans = int64(len(spans))

	return &trace, nil
}
