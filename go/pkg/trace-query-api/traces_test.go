package query

import (
	"net/http"
	"net/http/httptest"
	"time"

	"github.com/gin-gonic/gin"
	. "github.com/onsi/ginkgo/v2"
	. "github.com/onsi/gomega"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/testutils"
)

var _ = Context("traces handler", func() {
	var (
		router *gin.Engine
	)

	BeforeEach(func() {
		router = gin.New()
	})

	Context("List Traces", func() {
		DescribeTable("query API",
			func(url string, statusCode int, expectedFilter *requestParams) {
				q := &mockQueryDB{}
				controller := &Controller{
					Q:      q,
					Logger: logger.Desugar(),
				}
				SetRoutes(controller, router)

				recorder := httptest.NewRecorder()
				testReq, err := http.NewRequest(http.MethodGet, url, nil)
				Expect(err).NotTo(HaveOccurred())
				router.ServeHTTP(recorder, testReq)
				Expect(recorder.Code).To(Equal(statusCode))

				Expect(q.passedFilter).To(Equal(expectedFilter))
			},
			Entry(
				"returns 200 for a list of traces",
				"/query/1/123/v1/traces",
				http.StatusOK,
				&requestParams{
					PageSize: 100,
				},
			),
			Entry(
				"gets 404 on missing groupID",
				"/query/1/v1/traces",
				http.StatusNotFound,
				nil,
			),
			Entry(
				"gets 400 on invalid traceID",
				"/query/1/123/v1/traces?trace_id=1222",
				http.StatusBadRequest,
				nil,
			),
			Entry(
				"gets 400 on invalid period",
				"/query/1/123/v1/traces?period=10m",
				http.StatusBadRequest,
				nil,
			),
			Entry(
				"returns 200 with valid params",
				"/query/1/123/v1/traces?trace_id=b7e641ba-918b-3147-2cba-e93b13e7de2a&period=1m",
				http.StatusOK,
				&requestParams{
					TraceIDs: []string{"b7e641ba-918b-3147-2cba-e93b13e7de2a"},
					Period:   "1m",
					PageSize: 100,
				},
			),
			Entry(
				"returns 200 with valid operation and service name",
				"/query/1/123/v1/traces?service_name=blah&operation=fake",
				http.StatusOK,
				&requestParams{
					ServiceNames: []string{"blah"},
					Operations:   []string{"fake"},
					PageSize:     100,
				},
			),
			Entry(
				"returns 200 with repeated params name",
				"/query/1/123/v1/traces?service_name=blah&operation=fake&operation=boo&service_name=fiver",
				http.StatusOK,
				&requestParams{
					ServiceNames: []string{"blah", "fiver"},
					Operations:   []string{"fake", "boo"},
					PageSize:     100,
				},
			),
			Entry(
				"returns 200 with not operator in params name",
				"/query/1/123/v1/traces?not[service_name]=blah&not[operation]=fake&not[operation]=boo&not[service_name]=fiver",
				http.StatusOK,
				&requestParams{
					NotServiceNames: []string{"blah", "fiver"},
					NotOperations:   []string{"fake", "boo"},
					PageSize:        100,
				},
			),
			Entry(
				"returns 400 with incorrect duration params",
				"/query/1/123/v1/traces?lt[duration_nano]=400s&gt[duration_nano]=200s&period=1h",
				http.StatusBadRequest,
				nil,
			),
			Entry(
				"returns 400 with unsupported sort param",
				"/query/1/123/v1/traces?sort=random",
				http.StatusBadRequest,
				nil,
			),
			Entry(
				"returns 200 with timestamp_desc sort param",
				"/query/1/123/v1/traces?sort=timestamp_desc",
				http.StatusOK,
				&requestParams{
					Sort:     "timestamp_desc",
					PageSize: 100,
				},
			),
			Entry(
				"returns 200 with duration_desc sort param",
				"/query/1/123/v1/traces?sort=duration_desc",
				http.StatusOK,
				&requestParams{
					Sort:     "duration_desc",
					PageSize: 100,
				},
			),
			Entry(
				"returns 200 with duration_asc sort param",
				"/query/1/123/v1/traces?sort=duration_asc",
				http.StatusOK,
				&requestParams{
					Sort:     "duration_asc",
					PageSize: 100,
				},
			),
			Entry(
				"returns 200 with timestamp_asc sort param",
				"/query/1/123/v1/traces?sort=timestamp_asc",
				http.StatusOK,
				&requestParams{
					Sort:     "timestamp_asc",
					PageSize: 100,
				},
			),
			Entry(
				"returns 200 with proper duration params",
				"/query/1/123/v1/traces?lt[duration_nano]=400&gt[duration_nano]=200&period=1h",
				http.StatusOK,
				&requestParams{
					Period:     "1h",
					LtDuration: 400,
					GtDuration: 200,
					PageSize:   100,
				},
			),
			Entry(
				"returns 200 with custom period of 4h",
				"/query/1/123/v1/traces?period=4h",
				http.StatusOK,
				&requestParams{
					Period:   "4h",
					PageSize: 100,
				},
			),
			Entry(
				"returns 200 with custom period of 5m",
				"/query/1/123/v1/traces?period=5m",
				http.StatusOK,
				&requestParams{
					Period:   "5m",
					PageSize: 100,
				},
			),
			Entry(
				"returns 200 with page size parameter",
				"/query/1/123/v1/traces?lt[duration_nano]=400&gt[duration_nano]=200&period=1h&page_size=20",
				http.StatusOK,
				&requestParams{
					Period:     "1h",
					LtDuration: 400,
					GtDuration: 200,
					PageSize:   20,
				},
			),
			Entry(
				"returns 200 with page size and page token parameter",
				"/query/1/123/v1/traces?page_size=20&page_token=eyJsYXN0X3NlZW5fdGltZXN0YW1wIjoiMjAyMy0wOS0wNCAxMToyNToyMy4xNTY5MjMwMDAifQ==",
				http.StatusOK,
				&requestParams{
					PageSize:  20,
					PageToken: "eyJsYXN0X3NlZW5fdGltZXN0YW1wIjoiMjAyMy0wOS0wNCAxMToyNToyMy4xNTY5MjMwMDAifQ==",
					cursor: &Page{
						LastSeenTimestamp: "2023-09-04 11:25:23.156923000",
					},
				},
			),
			Entry(
				"returns 400 with wrong page token parameter",
				"/query/1/123/v1/traces?page_size=20&page_token=full",
				http.StatusBadRequest,
				nil,
			),
			Entry(
				"v3 returns 200 for a list of traces",
				"/v3/query/123/traces",
				http.StatusOK,
				&requestParams{
					PageSize: 100,
				},
			),
			Entry(
				"v3 gets 400 on invalid traceID",
				"/v3/query/123/traces?trace_id=1222",
				http.StatusBadRequest,
				nil,
			),
			Entry(
				"v3 gets 400 on invalid period",
				"/v3/query/123/traces?period=10m",
				http.StatusBadRequest,
				nil,
			),
			Entry(
				"v3 returns 200 with valid params",
				"/v3/query/123/traces?trace_id=b7e641ba-918b-3147-2cba-e93b13e7de2a&period=1m",
				http.StatusOK,
				&requestParams{
					TraceIDs: []string{"b7e641ba-918b-3147-2cba-e93b13e7de2a"},
					Period:   "1m",
					PageSize: 100,
				},
			),
			Entry(
				"v3 returns 200 with valid operation and service name",
				"/v3/query/123/traces?service_name=blah&operation=fake",
				http.StatusOK,
				&requestParams{
					ServiceNames: []string{"blah"},
					Operations:   []string{"fake"},
					PageSize:     100,
				},
			),
			Entry(
				"v3 returns 200 with repeated params name",
				"/v3/query/123/traces?service_name=blah&operation=fake&operation=boo&service_name=fiver",
				http.StatusOK,
				&requestParams{
					ServiceNames: []string{"blah", "fiver"},
					Operations:   []string{"fake", "boo"},
					PageSize:     100,
				},
			),
			Entry(
				"v3 returns 200 with not operator in params name",
				"/v3/query/123/traces?not[service_name]=blah&not[operation]=fake&not[operation]=boo&not[service_name]=fiver",
				http.StatusOK,
				&requestParams{
					NotServiceNames: []string{"blah", "fiver"},
					NotOperations:   []string{"fake", "boo"},
					PageSize:        100,
				},
			),
			Entry(
				"v3 returns 400 with incorrect duration params",
				"/v3/query/123/traces?lt[duration_nano]=400s&gt[duration_nano]=200s&period=1h",
				http.StatusBadRequest,
				nil,
			),
			Entry(
				"v3 returns 400 with negative duration param",
				"/v3/query/123/traces?lt[duration_nano]=-400&gt[duration_nano]=200&period=1h",
				http.StatusBadRequest,
				nil,
			),
			Entry(
				"v3 returns 200 with proper duration params",
				"/v3/query/123/traces?lt[duration_nano]=400&gt[duration_nano]=200&period=1h",
				http.StatusOK,
				&requestParams{
					Period:     "1h",
					LtDuration: 400,
					GtDuration: 200,
					PageSize:   100,
				},
			),
			Entry(
				"v3 returns 200 with custom period of 4h",
				"/v3/query/123/traces?period=4h",
				http.StatusOK,
				&requestParams{
					Period:   "4h",
					PageSize: 100,
				},
			),
			Entry(
				"v3 returns 200 with custom period of 5m",
				"/v3/query/123/traces?period=5m",
				http.StatusOK,
				&requestParams{
					Period:   "5m",
					PageSize: 100,
				},
			),
			Entry(
				"v3 returns 200 with page size parameter",
				"/v3/query/123/traces?lt[duration_nano]=400&gt[duration_nano]=200&period=1h&page_size=20",
				http.StatusOK,
				&requestParams{
					Period:     "1h",
					LtDuration: 400,
					GtDuration: 200,
					PageSize:   20,
				},
			),
			Entry(
				"v3 returns 200 with page size and page token parameter",
				"/v3/query/123/traces?page_size=20&page_token=eyJsYXN0X3NlZW5fdGltZXN0YW1wIjoiMjAyMy0wOS0wNCAxMToyNToyMy4xNTY5MjMwMDAifQ==",
				http.StatusOK,
				&requestParams{
					PageSize:  20,
					PageToken: "eyJsYXN0X3NlZW5fdGltZXN0YW1wIjoiMjAyMy0wOS0wNCAxMToyNToyMy4xNTY5MjMwMDAifQ==",
					cursor: &Page{
						LastSeenTimestamp: "2023-09-04 11:25:23.156923000",
					},
				},
			),
			Entry(
				"v3 returns 200 with page size and page token parameter and sort",
				"/v3/query/123/traces?page_size=20&sort=duration_desc&page_token=ewoJImxhc3Rfc2Vlbl9kdXJhdGlvbiI6IDEyMzQ1Njc4OSwKCSJzZWVuX3RyYWNlX2lkcyI6IFsKCQkiYjdlNjQxYmEtOTE4Yi0zMTQ3LTJjYmEtZTkzYjEzZTdkZTJhIiwKCQkiYjdlNjQxYmEtOTE4Yi0zMTQ3LTJjYmEtZTkzYjEzZTdkZTJiIgoJXQp9",
				http.StatusOK,
				&requestParams{
					PageSize:  20,
					PageToken: "ewoJImxhc3Rfc2Vlbl9kdXJhdGlvbiI6IDEyMzQ1Njc4OSwKCSJzZWVuX3RyYWNlX2lkcyI6IFsKCQkiYjdlNjQxYmEtOTE4Yi0zMTQ3LTJjYmEtZTkzYjEzZTdkZTJhIiwKCQkiYjdlNjQxYmEtOTE4Yi0zMTQ3LTJjYmEtZTkzYjEzZTdkZTJiIgoJXQp9",
					Sort:      SortDurationDesc,
					cursor: &Page{
						LastSeenDuration: 123456789,
						SeenTraceIds: []string{
							"b7e641ba-918b-3147-2cba-e93b13e7de2a",
							"b7e641ba-918b-3147-2cba-e93b13e7de2b",
						},
					},
				},
			),
			Entry(
				"v3 returns 400 with wrong page token parameter",
				"/v3/query/123/traces?page_size=20&page_token=full",
				http.StatusBadRequest,
				nil,
			),
			Entry(
				"v3 returns 400 with unmatched attribute names and values",
				"/v3/query/123/traces?attr_name=foo&attr_value=bar&attr_name=baz",
				http.StatusBadRequest,
				nil,
			),
			Entry(
				"v3 returns 200 with matched attribute names and values",
				"/v3/query/123/traces?attr_name=foo&attr_value=bar&attr_name=baz&attr_value=qux",
				http.StatusOK,
				&requestParams{
					AttributeNames:  []string{"foo", "baz"},
					AttributeValues: []string{"bar", "qux"},
					PageSize:        100,
				},
			),
			Entry(
				"v3 returns 400 with mixed period and range params",
				"/v3/query/123/traces?period=1h&start_time=2023-11-02&end_time=2023-11-20",
				http.StatusBadRequest,
				nil,
			),
			Entry(
				"v3 returns 400 when missing one of range params",
				"/v3/query/123/traces?period=1h&start_time=2023-11-02",
				http.StatusBadRequest,
				nil,
			),
			Entry(
				"v3 returns 400 when missing one of range params",
				"/v3/query/123/traces?period=1h&end_time=2023-11-20",
				http.StatusBadRequest,
				nil,
			),
			Entry(
				"v3 returns 200 with provided range params in RFC3339 format",
				"/v3/query/123/traces?start_time=2023-11-23T11:24:10Z&end_time=2023-11-23T13:24:10Z",
				http.StatusOK,
				&requestParams{
					StartTime: time.Date(2023, 11, 23, 11, 24, 10, 0, time.UTC),
					EndTime:   time.Date(2023, 11, 23, 13, 24, 10, 0, time.UTC),
					PageSize:  100,
				},
			),
			Entry(
				"v3 returns 400 with provided range params in non RFC3339 format",
				"/v3/query/123/traces?start_time=2023-11-02&end_time=2023-11-20",
				http.StatusBadRequest,
				nil,
			),
			Entry(
				"v3 gets 400 on invalid status",
				"/v3/query/123/traces?status=invalid",
				http.StatusBadRequest,
				nil,
			),
			Entry(
				"v3 gets 200 on filtering with status",
				"/v3/query/123/traces?status=ok&status=error",
				http.StatusOK,
				&requestParams{
					PageSize: 100,
					Statuses: []string{"STATUS_CODE_OK", "STATUS_CODE_ERROR"},
				},
			),
		)
	})
})

var _ = Context("traces query builder", func() {
	var (
		refTime          = time.Now().UTC()
		defaultStartTime = refTime.Add(-1 * 24 * time.Hour).Unix()
		defaultEndTime   = refTime.Add(1 * time.Hour).Unix()
	)

	DescribeTable("builds queries",
		func(projectID int64, params *requestParams, expected string, args []interface{}) {
			builder := buildTraceMVQuery(projectID, params, refTime)
			err := testutils.CompareSQLStrings(builder.sql, expected)
			Expect(err).ToNot(HaveOccurred())
			Expect(builder.args).To(Equal(args))
		},
		Entry(
			"list trace query with default period and no filters",
			int64(1),
			&requestParams{},
			`
WITH matched_traces AS (
SELECT TraceId, matched_span_count FROM (
WITH
    toStartOfHour(toDateTime($1)) AS r_start,
    toStartOfHour(toDateTime($2)) AS r_end
SELECT
	TraceId,
    count() AS matched_span_count,
	min(Timestamp) as TraceStart,
	max(Duration) as LongestSpanDuration
FROM tracing.gl_traces_main
WHERE (Timestamp >= r_start) AND (Timestamp <= r_end) AND (ProjectId = $3)
	GROUP BY TraceId
	ORDER BY TraceStart DESC
	LIMIT 100
))
SELECT
	UUIDNumToString(TraceId) AS traceID,
	min(Start) as TraceStart,
	max(Duration) as LongestSpanDuration,
	arraySort((x) -> (x.8, x.2), groupArray(
		(
			hex(SpanId) AS spanID,
			Start AS start,
			ServiceName AS serviceName,
			SpanName AS operation,
			StatusCode AS statusCode,
			Duration AS duration,
			if(ParentSpanId = '', '', hex(ParentSpanId)) as parentSpanID,
            if(ParentSpanId = '', 1, 2) as rootSpanOrderKey,
            if(StatusCode = 'STATUS_CODE_ERROR', true, false) AS hasError
		)
	)) AS spans,
	length(spans) AS total_spans,
    any(matched_traces.matched_span_count) AS MatchedSpanCount
FROM tracing.gl_traces_rolled
INNER JOIN matched_traces ON matched_traces.TraceId = gl_traces_rolled.TraceId
WHERE (ProjectId = $4)
GROUP BY TraceId
ORDER BY TraceStart DESC
`,
			[]interface{}{
				defaultStartTime,
				defaultEndTime,
				"1",
				"1",
			},
		),
		Entry(
			"list trace query with no period and 1 trace ID",
			int64(1),
			&requestParams{
				TraceIDs: []string{"b7e641ba-918b-3147-2cba-e93b13e7de2a"},
			},
			`SELECT
    UUIDNumToString(TraceId) AS traceID,
    min(Start) as TraceStart,
    max(Duration) as LongestSpanDuration,
    arraySort((x) -> (x.8, x.2), groupArray(
		(
			hex(SpanId) AS spanID,
			Start AS start,
			ServiceName AS serviceName,
			SpanName AS operation,
			StatusCode AS statusCode,
			Duration AS duration,
			if(ParentSpanId = '', '', hex(ParentSpanId)) as parentSpanID,
			if(ParentSpanId = '', 1, 2) as rootSpanOrderKey,
            if(StatusCode = 'STATUS_CODE_ERROR', true, false) AS hasError
		)
    )) AS spans,
    length(spans) AS total_spans
    FROM tracing.gl_traces_rolled
    WHERE (ProjectId = $1) AND ( TraceId IN [ UUIDStringToNum($2) ] )
    GROUP BY TraceId
    ORDER BY TraceStart DESC
    LIMIT 100
	`,
			[]interface{}{
				"1",
				"b7e641ba-918b-3147-2cba-e93b13e7de2a",
			},
		),
		Entry(
			"list trace query with custom period and list of traceID",
			int64(1),
			&requestParams{
				TraceIDs: []string{
					"b7e641ba-918b-3147-2cba-e93b13e7de2a",
					"b7e641ba-918b-3147-2dss-e13c13edd1x0",
				},
				Period: "1m",
			},
			`
WITH matched_traces AS (
SELECT TraceId, matched_span_count FROM (
WITH
    toStartOfMinute(toDateTime($1)) AS r_start,
    toStartOfMinute(toDateTime($2)) AS r_end
SELECT
	TraceId,
    count() AS matched_span_count,
	min(Timestamp) as TraceStart,
	max(Duration) as LongestSpanDuration
FROM tracing.gl_traces_main
WHERE (Timestamp >= r_start) AND (Timestamp <= r_end) AND (ProjectId = $3) AND ( TraceId IN [ UUIDStringToNum($4), UUIDStringToNum($5) ] )
	GROUP BY TraceId
	ORDER BY TraceStart DESC
	LIMIT 100
))
SELECT
	UUIDNumToString(TraceId) AS traceID,
	min(Start) as TraceStart,
	max(Duration) as LongestSpanDuration,
	arraySort((x) -> (x.8, x.2), groupArray(
		(
			hex(SpanId) AS spanID,
			Start AS start,
			ServiceName AS serviceName,
			SpanName AS operation,
			StatusCode AS statusCode,
			Duration AS duration,
			if(ParentSpanId = '', '', hex(ParentSpanId)) as parentSpanID,
			if(ParentSpanId = '', 1, 2) as rootSpanOrderKey,
            if(StatusCode = 'STATUS_CODE_ERROR', true, false) AS hasError
		)
	)) AS spans,
	length(spans) AS total_spans,
    any(matched_traces.matched_span_count) AS MatchedSpanCount
FROM tracing.gl_traces_rolled
INNER JOIN matched_traces ON matched_traces.TraceId = gl_traces_rolled.TraceId
WHERE (ProjectId = $6)
GROUP BY TraceId
ORDER BY TraceStart DESC
`,
			[]interface{}{
				refTime.Add(-1 * 1 * time.Minute).Unix(),
				refTime.Add(1 * time.Minute).Unix(),
				"1",
				"b7e641ba-918b-3147-2cba-e93b13e7de2a",
				"b7e641ba-918b-3147-2dss-e13c13edd1x0",
				"1",
			},
		),
		Entry(
			"list trace query with custom period and 1 service and operations",
			int64(1),
			&requestParams{
				ServiceNames: []string{"service-1"},
				Operations:   []string{"op-1", "op-2"},
				Period:       "1m",
			},
			`
WITH matched_traces AS (
SELECT TraceId, matched_span_count FROM (
WITH
    toStartOfMinute(toDateTime($1)) AS r_start,
    toStartOfMinute(toDateTime($2)) AS r_end
SELECT
	TraceId,
    count() AS matched_span_count,
	min(Timestamp) as TraceStart,
	max(Duration) as LongestSpanDuration
FROM tracing.gl_traces_main
WHERE (Timestamp >= r_start) AND (Timestamp <= r_end) AND (ProjectId = $3) AND ( ServiceName IN ( $4 ) ) AND ( SpanName IN ( $5 ) )
	GROUP BY TraceId
	ORDER BY TraceStart DESC
	LIMIT 100
))
SELECT
	UUIDNumToString(TraceId) AS traceID,
	min(Start) as TraceStart,
	max(Duration) as LongestSpanDuration,
	arraySort((x) -> (x.8, x.2), groupArray(
		(
			hex(SpanId) AS spanID,
			Start AS start,
			ServiceName AS serviceName,
			SpanName AS operation,
			StatusCode AS statusCode,
			Duration AS duration,
			if(ParentSpanId = '', '', hex(ParentSpanId)) as parentSpanID,
			if(ParentSpanId = '', 1, 2) as rootSpanOrderKey,
            if(StatusCode = 'STATUS_CODE_ERROR', true, false) AS hasError
		)
	)) AS spans,
	length(spans) AS total_spans,
    any(matched_traces.matched_span_count) AS MatchedSpanCount
FROM tracing.gl_traces_rolled
INNER JOIN matched_traces ON matched_traces.TraceId = gl_traces_rolled.TraceId
WHERE (ProjectId = $6)
GROUP BY TraceId
ORDER BY TraceStart DESC
`,
			[]interface{}{
				refTime.Add(-1 * 1 * time.Minute).Unix(),
				refTime.Add(1 * time.Minute).Unix(),
				"1",
				[]string{"service-1"},
				[]string{"op-1", "op-2"},
				"1",
			},
		),
		Entry(
			"list trace query with custom period and multiple service and operations",
			int64(1),
			&requestParams{
				ServiceNames: []string{"service-1", "service-2"},
				Operations:   []string{"op-1", "op-2"},
				Period:       "1m",
			},
			`
WITH matched_traces AS (
SELECT TraceId, matched_span_count FROM (
WITH
    toStartOfMinute(toDateTime($1)) AS r_start,
    toStartOfMinute(toDateTime($2)) AS r_end
SELECT
	TraceId,
    count() AS matched_span_count,
	min(Timestamp) as TraceStart,
	max(Duration) as LongestSpanDuration
FROM tracing.gl_traces_main
WHERE (Timestamp >= r_start) AND (Timestamp <= r_end) AND (ProjectId = $3) AND ( ServiceName IN ( $4 ) ) AND ( SpanName IN ( $5 ) )
	GROUP BY TraceId
	ORDER BY TraceStart DESC
	LIMIT 100
))
SELECT
	UUIDNumToString(TraceId) AS traceID,
	min(Start) as TraceStart,
	max(Duration) as LongestSpanDuration,
	arraySort((x) -> (x.8, x.2), groupArray(
		(
			hex(SpanId) AS spanID,
			Start AS start,
			ServiceName AS serviceName,
			SpanName AS operation,
			StatusCode AS statusCode,
			Duration AS duration,
			if(ParentSpanId = '', '', hex(ParentSpanId)) as parentSpanID,
			if(ParentSpanId = '', 1, 2) as rootSpanOrderKey,
            if(StatusCode = 'STATUS_CODE_ERROR', true, false) AS hasError
		)
	)) AS spans,
	length(spans) AS total_spans,
    any(matched_traces.matched_span_count) AS MatchedSpanCount
FROM tracing.gl_traces_rolled
INNER JOIN matched_traces ON matched_traces.TraceId = gl_traces_rolled.TraceId
WHERE (ProjectId = $6)
GROUP BY TraceId
ORDER BY TraceStart DESC
`,
			[]interface{}{
				refTime.Add(-1 * 1 * time.Minute).Unix(),
				refTime.Add(1 * time.Minute).Unix(),
				"1",
				[]string{"service-1", "service-2"},
				[]string{"op-1", "op-2"},
				"1",
			},
		),
		Entry(
			"list trace query with custom period and duration filter",
			int64(1),
			&requestParams{
				LtDuration: 300,
				GtDuration: 500,
				Period:     "1m",
			},
			`
WITH matched_traces AS (
SELECT TraceId, matched_span_count FROM (
WITH
    toStartOfMinute(toDateTime($1)) AS r_start,
    toStartOfMinute(toDateTime($2)) AS r_end
SELECT
	TraceId,
    count() AS matched_span_count,
	min(Timestamp) as TraceStart,
	max(Duration) as LongestSpanDuration
FROM tracing.gl_traces_main
WHERE (Timestamp >= r_start) AND (Timestamp <= r_end) AND (ProjectId = $3) AND ( Duration <= $4 ) AND ( Duration >= $5 )
	GROUP BY TraceId
	ORDER BY TraceStart DESC
	LIMIT 100
))
SELECT
	UUIDNumToString(TraceId) AS traceID,
	min(Start) as TraceStart,
	max(Duration) as LongestSpanDuration,
	arraySort((x) -> (x.8, x.2), groupArray(
		(
			hex(SpanId) AS spanID,
			Start AS start,
			ServiceName AS serviceName,
			SpanName AS operation,
			StatusCode AS statusCode,
			Duration AS duration,
			if(ParentSpanId = '', '', hex(ParentSpanId)) as parentSpanID,
			if(ParentSpanId = '', 1, 2) as rootSpanOrderKey,
            if(StatusCode = 'STATUS_CODE_ERROR', true, false) AS hasError
		)
	)) AS spans,
	length(spans) AS total_spans,
    any(matched_traces.matched_span_count) AS MatchedSpanCount
FROM tracing.gl_traces_rolled
INNER JOIN matched_traces ON matched_traces.TraceId = gl_traces_rolled.TraceId
WHERE (ProjectId = $6)
GROUP BY TraceId
ORDER BY TraceStart DESC
`,
			[]interface{}{
				refTime.Add(-1 * 1 * time.Minute).Unix(),
				refTime.Add(1 * time.Minute).Unix(),
				"1",
				int64(300),
				int64(500),
				"1",
			},
		),
		Entry(
			"list trace query with custom period of 12h",
			int64(1),
			&requestParams{
				Period: "12h",
			},
			`
WITH matched_traces AS (
SELECT TraceId, matched_span_count FROM (
WITH
    toStartOfHour(toDateTime($1)) AS r_start,
    toStartOfHour(toDateTime($2)) AS r_end
SELECT
	TraceId,
    count() AS matched_span_count,
	min(Timestamp) as TraceStart,
	max(Duration) as LongestSpanDuration
FROM tracing.gl_traces_main
WHERE (Timestamp >= r_start) AND (Timestamp <= r_end) AND (ProjectId = $3)
	GROUP BY TraceId
	ORDER BY TraceStart DESC
	LIMIT 100
))
SELECT
	UUIDNumToString(TraceId) AS traceID,
	min(Start) as TraceStart,
	max(Duration) as LongestSpanDuration,
	arraySort((x) -> (x.8, x.2), groupArray(
		(
			hex(SpanId) AS spanID,
			Start AS start,
			ServiceName AS serviceName,
			SpanName AS operation,
			StatusCode AS statusCode,
			Duration AS duration,
			if(ParentSpanId = '', '', hex(ParentSpanId)) as parentSpanID,
			if(ParentSpanId = '', 1, 2) as rootSpanOrderKey,
            if(StatusCode = 'STATUS_CODE_ERROR', true, false) AS hasError
		)
	)) AS spans,
	length(spans) AS total_spans,
    any(matched_traces.matched_span_count) AS MatchedSpanCount
FROM tracing.gl_traces_rolled
INNER JOIN matched_traces ON matched_traces.TraceId = gl_traces_rolled.TraceId
WHERE (ProjectId = $4)
GROUP BY TraceId
ORDER BY TraceStart DESC
`,
			[]interface{}{
				refTime.Add(-1 * 12 * time.Hour).Unix(),
				refTime.Add(1 * time.Hour).Unix(),
				"1",
				"1",
			},
		),
		Entry(
			"list trace query with custom period of 4h",
			int64(1),
			&requestParams{
				Period: "4h",
			},
			`
WITH matched_traces AS (
SELECT TraceId, matched_span_count FROM (
WITH
    toStartOfMinute(toDateTime($1)) AS r_start,
    toStartOfMinute(toDateTime($2)) AS r_end
SELECT
	TraceId,
    count() AS matched_span_count,
	min(Timestamp) as TraceStart,
	max(Duration) as LongestSpanDuration
FROM tracing.gl_traces_main
WHERE (Timestamp >= r_start) AND (Timestamp <= r_end) AND (ProjectId = $3)
	GROUP BY TraceId
	ORDER BY TraceStart DESC
	LIMIT 100
))
SELECT
	UUIDNumToString(TraceId) AS traceID,
	min(Start) as TraceStart,
	max(Duration) as LongestSpanDuration,
	arraySort((x) -> (x.8, x.2), groupArray(
		(
			hex(SpanId) AS spanID,
			Start AS start,
			ServiceName AS serviceName,
			SpanName AS operation,
			StatusCode AS statusCode,
			Duration AS duration,
			if(ParentSpanId = '', '', hex(ParentSpanId)) as parentSpanID,
			if(ParentSpanId = '', 1, 2) as rootSpanOrderKey,
            if(StatusCode = 'STATUS_CODE_ERROR', true, false) AS hasError
		)
	)) AS spans,
	length(spans) AS total_spans,
    any(matched_traces.matched_span_count) AS MatchedSpanCount
FROM tracing.gl_traces_rolled
INNER JOIN matched_traces ON matched_traces.TraceId = gl_traces_rolled.TraceId
WHERE (ProjectId = $4)
GROUP BY TraceId
ORDER BY TraceStart DESC
`,
			[]interface{}{
				refTime.Add(-1 * 4 * time.Hour).Unix(),
				refTime.Add(1 * time.Minute).Unix(),
				"1",
				"1",
			},
		),
		Entry(
			"list trace query with custom period of 5m",
			int64(1),
			&requestParams{
				Period: "5m",
			},
			`
WITH matched_traces AS (
SELECT TraceId, matched_span_count FROM (
WITH
    toStartOfMinute(toDateTime($1)) AS r_start,
    toStartOfMinute(toDateTime($2)) AS r_end
SELECT
	TraceId,
    count() AS matched_span_count,
	min(Timestamp) as TraceStart,
	max(Duration) as LongestSpanDuration
FROM tracing.gl_traces_main
WHERE (Timestamp >= r_start) AND (Timestamp <= r_end) AND (ProjectId = $3)
	GROUP BY TraceId
	ORDER BY TraceStart DESC
	LIMIT 100
))
SELECT
	UUIDNumToString(TraceId) AS traceID,
	min(Start) as TraceStart,
	max(Duration) as LongestSpanDuration,
	arraySort((x) -> (x.8, x.2), groupArray(
		(
			hex(SpanId) AS spanID,
			Start AS start,
			ServiceName AS serviceName,
			SpanName AS operation,
			StatusCode AS statusCode,
			Duration AS duration,
			if(ParentSpanId = '', '', hex(ParentSpanId)) as parentSpanID,
			if(ParentSpanId = '', 1, 2) as rootSpanOrderKey,
            if(StatusCode = 'STATUS_CODE_ERROR', true, false) AS hasError
		)
	)) AS spans,
	length(spans) AS total_spans,
    any(matched_traces.matched_span_count) AS MatchedSpanCount
FROM tracing.gl_traces_rolled
INNER JOIN matched_traces ON matched_traces.TraceId = gl_traces_rolled.TraceId
WHERE (ProjectId = $4)
GROUP BY TraceId
ORDER BY TraceStart DESC
`,
			[]interface{}{
				refTime.Add(-1 * 5 * time.Minute).Unix(),
				refTime.Add(1 * time.Minute).Unix(),
				"1",
				"1",
			},
		),
		Entry(
			"list trace query with custom period and duration filter and page token",
			int64(1),
			&requestParams{
				LtDuration: 300,
				GtDuration: 500,
				Period:     "1m",
				PageSize:   50,
				PageToken:  "eyJsYXN0X3NlZW5fdGltZXN0YW1wIjoiMjAyMy0wOS0wNCAxMToyNToyMy4xNTY5MjMwMDAifQ==",
				cursor: &Page{
					LastSeenTimestamp: "2023-09-04 11:25:23.156923000",
					SeenTraceIds: []string{
						"b7e641ba-918b-3147-2cba-e93b13e7de2a",
						"b7e641ba-918b-3147-2cba-e93b13e7de2b",
					},
				},
			},
			`
WITH matched_traces AS (
SELECT TraceId, matched_span_count FROM (
WITH
    toStartOfMinute(toDateTime($1)) AS r_start,
    toStartOfMinute(toDateTime($2)) AS r_end
SELECT
	TraceId,
    count() AS matched_span_count,
    min(Timestamp) as TraceStart,
	max(Duration) as LongestSpanDuration
FROM tracing.gl_traces_main
WHERE (Timestamp >= r_start) AND (Timestamp <= r_end) AND (ProjectId = $3) AND ( Duration <= $4 ) AND ( Duration >= $5 )
	GROUP BY TraceId
	HAVING TraceStart <= $6 AND ( TraceId NOT IN [ UUIDStringToNum($7), UUIDStringToNum($8) ] )
	ORDER BY TraceStart DESC
	LIMIT 50
))
SELECT
	UUIDNumToString(TraceId) AS traceID,
    min(Start) as TraceStart,
    max(Duration) as LongestSpanDuration,
	arraySort((x) -> (x.8, x.2), groupArray(
		(
			hex(SpanId) AS spanID,
			Start AS start,
			ServiceName AS serviceName,
			SpanName AS operation,
			StatusCode AS statusCode,
			Duration AS duration,
			if(ParentSpanId = '', '', hex(ParentSpanId)) as parentSpanID,
			if(ParentSpanId = '', 1, 2) as rootSpanOrderKey,
            if(StatusCode = 'STATUS_CODE_ERROR', true, false) AS hasError
		)
	)) AS spans,
	length(spans) AS total_spans,
    any(matched_traces.matched_span_count) AS MatchedSpanCount
FROM tracing.gl_traces_rolled
INNER JOIN matched_traces ON matched_traces.TraceId = gl_traces_rolled.TraceId
WHERE (ProjectId = $9)
GROUP BY TraceId
ORDER BY TraceStart DESC
`,
			[]interface{}{
				refTime.Add(-1 * 1 * time.Minute).Unix(),
				refTime.Add(1 * time.Minute).Unix(),
				"1",
				int64(300),
				int64(500),
				"2023-09-04 11:25:23.156923000",
				"b7e641ba-918b-3147-2cba-e93b13e7de2a",
				"b7e641ba-918b-3147-2cba-e93b13e7de2b",
				"1",
			},
		),
		Entry(
			"list trace query with custom period of 5m and sort by duration desc",
			int64(1),
			&requestParams{
				Period: "5m",
				Sort:   SortDurationDesc,
			},
			`
WITH matched_traces AS (
SELECT TraceId, matched_span_count FROM (
WITH
    toStartOfMinute(toDateTime($1)) AS r_start,
    toStartOfMinute(toDateTime($2)) AS r_end
SELECT
	TraceId,
    count() AS matched_span_count,
	min(Timestamp) as TraceStart,
	max(Duration) as LongestSpanDuration
FROM tracing.gl_traces_main
WHERE (Timestamp >= r_start) AND (Timestamp <= r_end) AND (ProjectId = $3)
	GROUP BY TraceId
	ORDER BY LongestSpanDuration DESC
	LIMIT 100
))
SELECT
	UUIDNumToString(TraceId) AS traceID,
	min(Start) as TraceStart,
	max(Duration) as LongestSpanDuration,
	arraySort((x) -> (x.8, x.2), groupArray(
		(
			hex(SpanId) AS spanID,
			Start AS start,
			ServiceName AS serviceName,
			SpanName AS operation,
			StatusCode AS statusCode,
			Duration AS duration,
			if(ParentSpanId = '', '', hex(ParentSpanId)) as parentSpanID,
			if(ParentSpanId = '', 1, 2) as rootSpanOrderKey,
            if(StatusCode = 'STATUS_CODE_ERROR', true, false) AS hasError
		)
	)) AS spans,
	length(spans) AS total_spans,
    any(matched_traces.matched_span_count) AS MatchedSpanCount
FROM tracing.gl_traces_rolled
INNER JOIN matched_traces ON matched_traces.TraceId = gl_traces_rolled.TraceId
WHERE (ProjectId = $4)
GROUP BY TraceId
ORDER BY LongestSpanDuration DESC
`,
			[]interface{}{
				refTime.Add(-1 * 5 * time.Minute).Unix(),
				refTime.Add(1 * time.Minute).Unix(),
				"1",
				"1",
			},
		),
		Entry(
			"list trace query with custom period and duration filter, sort by duration desc and page token",
			int64(1),
			&requestParams{
				LtDuration: 300,
				GtDuration: 500,
				Period:     "1m",
				Sort:       SortDurationDesc,
				PageSize:   50,
				PageToken:  "eyJsYXN0X3NlZW5fdGltZXN0YW1wIjoiMjAyMy0wOS0wNCAxMToyNToyMy4xNTY5MjMwMDAifQ==",
				cursor: &Page{
					LastSeenDuration: 123456789,
					SeenTraceIds: []string{
						"b7e641ba-918b-3147-2cba-e93b13e7de2a",
						"b7e641ba-918b-3147-2cba-e93b13e7de2b",
					},
				},
			},
			`
WITH matched_traces AS (
SELECT TraceId, matched_span_count FROM (
WITH
    toStartOfMinute(toDateTime($1)) AS r_start,
    toStartOfMinute(toDateTime($2)) AS r_end
SELECT
	TraceId,
    count() AS matched_span_count,
    min(Timestamp) as TraceStart,
	max(Duration) as LongestSpanDuration
FROM tracing.gl_traces_main
WHERE (Timestamp >= r_start) AND (Timestamp <= r_end) AND (ProjectId = $3) AND ( Duration <= $4 ) AND ( Duration >= $5 )
	GROUP BY TraceId
	HAVING LongestSpanDuration <= $6 AND ( TraceId NOT IN [ UUIDStringToNum($7), UUIDStringToNum($8) ] )
	ORDER BY LongestSpanDuration DESC
	LIMIT 50
))
SELECT
	UUIDNumToString(TraceId) AS traceID,
    min(Start) as TraceStart,
    max(Duration) as LongestSpanDuration,
	arraySort((x) -> (x.8, x.2), groupArray(
		(
			hex(SpanId) AS spanID,
			Start AS start,
			ServiceName AS serviceName,
			SpanName AS operation,
			StatusCode AS statusCode,
			Duration AS duration,
			if(ParentSpanId = '', '', hex(ParentSpanId)) as parentSpanID,
			if(ParentSpanId = '', 1, 2) as rootSpanOrderKey,
            if(StatusCode = 'STATUS_CODE_ERROR', true, false) AS hasError
		)
	)) AS spans,
	length(spans) AS total_spans,
    any(matched_traces.matched_span_count) AS MatchedSpanCount
FROM tracing.gl_traces_rolled
INNER JOIN matched_traces ON matched_traces.TraceId = gl_traces_rolled.TraceId
WHERE (ProjectId = $9)
GROUP BY TraceId
ORDER BY LongestSpanDuration DESC
`,
			[]interface{}{
				refTime.Add(-1 * 1 * time.Minute).Unix(),
				refTime.Add(1 * time.Minute).Unix(),
				"1",
				int64(300),
				int64(500),
				uint64(123456789),
				"b7e641ba-918b-3147-2cba-e93b13e7de2a",
				"b7e641ba-918b-3147-2cba-e93b13e7de2b",
				"1",
			},
		),
		Entry(
			"list trace query with custom period and duration filter, sort by duration asc and page token",
			int64(1),
			&requestParams{
				LtDuration: 300,
				GtDuration: 500,
				Period:     "1m",
				Sort:       SortDurationAsc,
				PageSize:   50,
				PageToken:  "eyJsYXN0X3NlZW5fdGltZXN0YW1wIjoiMjAyMy0wOS0wNCAxMToyNToyMy4xNTY5MjMwMDAifQ==",
				cursor: &Page{
					LastSeenDuration: 123456789,
					SeenTraceIds: []string{
						"b7e641ba-918b-3147-2cba-e93b13e7de2a",
						"b7e641ba-918b-3147-2cba-e93b13e7de2b",
					},
				},
			},
			`
WITH matched_traces AS (
SELECT TraceId, matched_span_count FROM (
WITH
    toStartOfMinute(toDateTime($1)) AS r_start,
    toStartOfMinute(toDateTime($2)) AS r_end
SELECT
	TraceId,
    count() AS matched_span_count,
    min(Timestamp) as TraceStart,
	max(Duration) as LongestSpanDuration
FROM tracing.gl_traces_main
WHERE (Timestamp >= r_start) AND (Timestamp <= r_end) AND (ProjectId = $3) AND ( Duration <= $4 ) AND ( Duration >= $5 )
	GROUP BY TraceId
	HAVING LongestSpanDuration >= $6 AND ( TraceId NOT IN [ UUIDStringToNum($7), UUIDStringToNum($8) ] )
	ORDER BY LongestSpanDuration ASC
	LIMIT 50
))
SELECT
	UUIDNumToString(TraceId) AS traceID,
    min(Start) as TraceStart,
    max(Duration) as LongestSpanDuration,
	arraySort((x) -> (x.8, x.2), groupArray(
		(
			hex(SpanId) AS spanID,
			Start AS start,
			ServiceName AS serviceName,
			SpanName AS operation,
			StatusCode AS statusCode,
			Duration AS duration,
			if(ParentSpanId = '', '', hex(ParentSpanId)) as parentSpanID,
			if(ParentSpanId = '', 1, 2) as rootSpanOrderKey,
            if(StatusCode = 'STATUS_CODE_ERROR', true, false) AS hasError
		)
	)) AS spans,
	length(spans) AS total_spans,
    any(matched_traces.matched_span_count) AS MatchedSpanCount
FROM tracing.gl_traces_rolled
INNER JOIN matched_traces ON matched_traces.TraceId = gl_traces_rolled.TraceId
WHERE (ProjectId = $9)
GROUP BY TraceId
ORDER BY LongestSpanDuration ASC
`,
			[]interface{}{
				refTime.Add(-1 * 1 * time.Minute).Unix(),
				refTime.Add(1 * time.Minute).Unix(),
				"1",
				int64(300),
				int64(500),
				uint64(123456789),
				"b7e641ba-918b-3147-2cba-e93b13e7de2a",
				"b7e641ba-918b-3147-2cba-e93b13e7de2b",
				"1",
			},
		),
		Entry(
			"list trace query with custom period and duration filter, sort by timestamp asc and page token",
			int64(1),
			&requestParams{
				LtDuration: 300,
				GtDuration: 500,
				Period:     "1m",
				Sort:       SortTimestampAsc,
				PageSize:   50,
				PageToken:  "eyJsYXN0X3NlZW5fdGltZXN0YW1wIjoiMjAyMy0wOS0wNCAxMToyNToyMy4xNTY5MjMwMDAifQ==",
				cursor: &Page{
					LastSeenTimestamp: "2023-09-04 11:25:23.156923000",
					SeenTraceIds: []string{
						"b7e641ba-918b-3147-2cba-e93b13e7de2a",
						"b7e641ba-918b-3147-2cba-e93b13e7de2b",
					},
				},
			},
			`
WITH matched_traces AS (
SELECT TraceId, matched_span_count FROM (
WITH
    toStartOfMinute(toDateTime($1)) AS r_start,
    toStartOfMinute(toDateTime($2)) AS r_end
SELECT
	TraceId,
    count() AS matched_span_count,
    min(Timestamp) as TraceStart,
	max(Duration) as LongestSpanDuration
FROM tracing.gl_traces_main
WHERE (Timestamp >= r_start) AND (Timestamp <= r_end) AND (ProjectId = $3) AND ( Duration <= $4 ) AND ( Duration >= $5 )
	GROUP BY TraceId
	HAVING TraceStart >= $6 AND ( TraceId NOT IN [ UUIDStringToNum($7), UUIDStringToNum($8) ] )
	ORDER BY TraceStart ASC
	LIMIT 50
))
SELECT
	UUIDNumToString(TraceId) AS traceID,
    min(Start) as TraceStart,
    max(Duration) as LongestSpanDuration,
	arraySort((x) -> (x.8, x.2), groupArray(
		(
			hex(SpanId) AS spanID,
			Start AS start,
			ServiceName AS serviceName,
			SpanName AS operation,
			StatusCode AS statusCode,
			Duration AS duration,
			if(ParentSpanId = '', '', hex(ParentSpanId)) as parentSpanID,
			if(ParentSpanId = '', 1, 2) as rootSpanOrderKey,
            if(StatusCode = 'STATUS_CODE_ERROR', true, false) AS hasError
		)
	)) AS spans,
	length(spans) AS total_spans,
    any(matched_traces.matched_span_count) AS MatchedSpanCount
FROM tracing.gl_traces_rolled
INNER JOIN matched_traces ON matched_traces.TraceId = gl_traces_rolled.TraceId
WHERE (ProjectId = $9)
GROUP BY TraceId
ORDER BY TraceStart ASC
`,
			[]interface{}{
				refTime.Add(-1 * 1 * time.Minute).Unix(),
				refTime.Add(1 * time.Minute).Unix(),
				"1",
				int64(300),
				int64(500),
				"2023-09-04 11:25:23.156923000",
				"b7e641ba-918b-3147-2cba-e93b13e7de2a",
				"b7e641ba-918b-3147-2cba-e93b13e7de2b",
				"1",
			},
		),
		Entry(
			"list trace query with custom attributes searched by name and value",
			int64(1),
			&requestParams{
				AttributeNames:  []string{"foo", "bar"},
				AttributeValues: []string{"baz", "qux"},
			},
			`
WITH matched_traces AS (
SELECT TraceId, matched_span_count FROM (
WITH
    toStartOfHour(toDateTime($1)) AS r_start,
    toStartOfHour(toDateTime($2)) AS r_end
SELECT
	TraceId,
    count() AS matched_span_count,
	min(Timestamp) as TraceStart,
	max(Duration) as LongestSpanDuration
FROM tracing.gl_traces_main
WHERE (Timestamp >= r_start) AND (Timestamp <= r_end) AND (ProjectId = $3)
AND ( ResourceAttributes[$4] = $5 OR SpanAttributes[$6] = $7 ) AND ( ResourceAttributes[$8] = $9 OR SpanAttributes[$10] = $11 )
	GROUP BY TraceId
	ORDER BY TraceStart DESC
	LIMIT 100
))
SELECT
	UUIDNumToString(TraceId) AS traceID,
    min(Start) as TraceStart,
    max(Duration) as LongestSpanDuration,
	arraySort((x) -> (x.8, x.2), groupArray(
		(
			hex(SpanId) AS spanID,
			Start AS start,
			ServiceName AS serviceName,
			SpanName AS operation,
			StatusCode AS statusCode,
			Duration AS duration,
			if(ParentSpanId = '', '', hex(ParentSpanId)) as parentSpanID,
			if(ParentSpanId = '', 1, 2) as rootSpanOrderKey,
            if(StatusCode = 'STATUS_CODE_ERROR', true, false) AS hasError
		)
	)) AS spans,
	length(spans) AS total_spans,
    any(matched_traces.matched_span_count) AS MatchedSpanCount
FROM tracing.gl_traces_rolled
INNER JOIN matched_traces ON matched_traces.TraceId = gl_traces_rolled.TraceId
WHERE (ProjectId = $12)
GROUP BY TraceId
ORDER BY TraceStart DESC
`,
			[]interface{}{
				defaultStartTime,
				defaultEndTime,
				"1",
				"foo",
				"baz",
				"foo",
				"baz",
				"bar",
				"qux",
				"bar",
				"qux",
				"1",
			},
		),
		Entry(
			"list trace query with custom range params",
			int64(1),
			&requestParams{
				StartTime: time.Date(2023, 11, 02, 0, 0, 0, 0, time.UTC),
				EndTime:   time.Date(2023, 11, 20, 0, 0, 0, 0, time.UTC),
			},
			`
WITH matched_traces AS (
SELECT TraceId, matched_span_count FROM (
WITH
    toStartOfHour(toDateTime($1)) AS r_start,
    toStartOfHour(toDateTime($2)) AS r_end
SELECT
	TraceId,
    count() AS matched_span_count,
	min(Timestamp) as TraceStart,
	max(Duration) as LongestSpanDuration
FROM tracing.gl_traces_main
WHERE (Timestamp >= r_start) AND (Timestamp <= r_end) AND (ProjectId = $3)
	GROUP BY TraceId
	ORDER BY TraceStart DESC
	LIMIT 100
))
SELECT
	UUIDNumToString(TraceId) AS traceID,
    min(Start) as TraceStart,
    max(Duration) as LongestSpanDuration,
	arraySort((x) -> (x.8, x.2), groupArray(
		(
			hex(SpanId) AS spanID,
			Start AS start,
			ServiceName AS serviceName,
			SpanName AS operation,
			StatusCode AS statusCode,
			Duration AS duration,
			if(ParentSpanId = '', '', hex(ParentSpanId)) as parentSpanID,
			if(ParentSpanId = '', 1, 2) as rootSpanOrderKey,
            if(StatusCode = 'STATUS_CODE_ERROR', true, false) AS hasError
		)
	)) AS spans,
	length(spans) AS total_spans,
    any(matched_traces.matched_span_count) AS MatchedSpanCount
FROM tracing.gl_traces_rolled
INNER JOIN matched_traces ON matched_traces.TraceId = gl_traces_rolled.TraceId
WHERE (ProjectId = $4)
GROUP BY TraceId
ORDER BY TraceStart DESC
`,
			[]interface{}{
				time.Date(2023, 11, 02, 0, 0, 0, 0, time.UTC),
				time.Date(2023, 11, 20, 0, 0, 0, 0, time.UTC),
				"1",
				"1",
			},
		),
	)
})
