package query

import (
	"net/http"
	"net/http/httptest"

	"github.com/gin-gonic/gin"
	. "github.com/onsi/ginkgo/v2"
	. "github.com/onsi/gomega"
)

var _ = Context("trace handler", func() {
	var (
		router *gin.Engine
	)

	BeforeEach(func() {
		router = gin.New()
	})

	type expected struct {
		statusCode int
		json       string
	}

	test := func(url string, expected expected) {
		q := &mockQueryDB{}
		controller := &Controller{
			Q:      q,
			Logger: logger.Desugar(),
		}
		SetRoutes(controller, router)

		recorder := httptest.NewRecorder()
		testReq, err := http.NewRequest(http.MethodGet, url, nil)
		Expect(err).NotTo(HaveOccurred())
		router.ServeHTTP(recorder, testReq)
		Expect(recorder).To(HaveHTTPStatus(expected.statusCode))
		if expected.json != "" {
			Expect(recorder).To(HaveHTTPBody(MatchJSON(expected.json)))
		}
	}

	DescribeTable("get trace", test,
		Entry("should return 200 with trace", "/v3/query/1/traces/12b797ee-7c32-a226-bf56-d4b3af18af55",
			expected{
				statusCode: http.StatusOK,
				json: `{
					"timestamp":   "2020-08-11T05:02:12.123456789Z",
					"timestamp_nano": 1597122132123456789,
					"trace_id":     "12b797ee-7c32-a226-bf56-d4b3af18af55",
					"service_name": "sample-service",
					"operation":   "sample-operation",
					"duration_nano": 0,
					"status_code":  "OK",
					"total_spans": 0,
					"spans": [
						{
							"timestamp":     "2020-08-11T05:02:12.123456789Z",
							"timestamp_nano": 1597122132123456789,
							"span_id":        "uuid",
							"trace_id":       "12b797ee-7c32-a226-bf56-d4b3af18af55",
							"service_name":   "sample-service",
							"operation":     "sample-operation",
							"duration_nano":  1000,
							"parent_span_id":  "",
							"status_code":    "OK",
							"status_message": "foo",
							"scope_version":  "bar",
							"scope_name":     "baz",
							"span_kind":      "server",
							"trace_state":    "foobar",
							"span_attributes": {
								"http.route":  "/test",
								"custom.attr": "my_value"
							},
							"resource_attributes": {
								"service.name": "sample-service"
							}
						},
						{
							"timestamp":     "2020-08-11T05:02:12.133456789Z",
							"timestamp_nano": 1597122132133456789,
							"span_id":        "uuid",
							"trace_id":       "12b797ee-7c32-a226-bf56-d4b3af18af55",
							"service_name":   "sample-service",
							"operation":     "sample-operation",
							"duration_nano":  1000,
							"parent_span_id":  "",
							"status_code":    "OK",
							"status_message": "foo",
							"scope_version":  "bar",
							"scope_name":     "baz",
							"span_kind":      "server",
							"trace_state":    "foobar",
							"span_attributes": {
								"http.route":  "/test",
								"custom.attr": "my_value"
							},
							"resource_attributes": {
								"service.name": "sample-service"
							}
						}
					]
				}`,
			}),
		Entry("should return 400 with invalid project id", "/v3/query/invalid/traces/12b797ee-7c32-a226-bf56-d4b3af18af55",
			expected{
				statusCode: http.StatusBadRequest,
			}),
		Entry("should return 400 with invalid trace id", "/v3/query/1/traces/12",
			expected{
				statusCode: http.StatusBadRequest,
			}),
		Entry("should return 404 when traceId not found in db", "/v3/query/1/traces/12b797ee-7c32-a226-bf56-d4b3af18af56",
			expected{
				statusCode: http.StatusNotFound,
			}),
	)
})
