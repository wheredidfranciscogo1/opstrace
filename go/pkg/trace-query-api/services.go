package query

import (
	"context"
	"fmt"
	"net/http"
	"strconv"

	"github.com/gin-gonic/gin"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/constants"
	"go.uber.org/zap"
)

type ServicesResult struct {
	Services []ServiceItem `json:"services"`
}

type ServiceItem struct {
	Name string `json:"name"`
}

func (c *Controller) ServicesHandler(ctx *gin.Context) {
	handlerParams := &projectIDParam{}
	if err := ctx.BindUri(handlerParams); err != nil {
		c.Logger.Error("bind error", zap.Error(err))
		// context already canceled by BindUri

		return
	}
	projectID := handlerParams.ProjectID

	services, err := c.Q.GetServices(ctx, projectID)
	if err != nil {
		c.Logger.Error("get services", zap.Error(err))
		ctx.AbortWithError(http.StatusInternalServerError, err)
		return
	}

	ctx.JSON(200, ServicesResult{
		Services: services,
	})
}

func (q *querier) GetServices(ctx context.Context, projectID int64) ([]ServiceItem, error) {
	const query = `
SELECT ServiceName FROM ` + constants.TracingDistTableName + `
WHERE ProjectId = ? GROUP BY ServiceName ORDER BY ServiceName ASC`

	var (
		services []struct {
			ServiceName string
		}
		err error
	)
	if q.cloudDB != nil {
		err = q.cloudDB.Select(ctx, &services, query, strconv.Itoa(int(projectID)))
	} else {
		err = q.db.Select(ctx, &services, query, strconv.Itoa(int(projectID)))
	}
	if err != nil {
		return nil, fmt.Errorf("query services: %w", err)
	}

	sns := make([]ServiceItem, len(services))
	for i, s := range services {
		sns[i].Name = s.ServiceName
	}

	return sns, nil
}

type OperationsResult struct {
	Operations []OperationItem `json:"operations"`
}

type OperationItem struct {
	Name string `json:"name"`
}

type serviceOperationsHandlerParams struct {
	projectIDParam
	ServiceName string `uri:"service_name" binding:"required"`
}

func (c *Controller) ServiceOperationsHandler(ctx *gin.Context) {
	handlerParams := &serviceOperationsHandlerParams{}
	if err := ctx.BindUri(handlerParams); err != nil {
		c.Logger.Error("bind error", zap.Error(err))
		// context already canceled by BindUri
		return
	}
	projectID := handlerParams.ProjectID
	service := handlerParams.ServiceName

	operations, err := c.Q.GetServiceOperations(ctx, projectID, service)
	if err != nil {
		c.Logger.With(
			zap.String("service_name", service),
			zap.Int64("project_id", projectID)).
			Error("get service operations", zap.Error(err))
		ctx.AbortWithError(http.StatusInternalServerError, err)
		return
	}

	ctx.JSON(200, OperationsResult{
		Operations: operations,
	})
}

func (q *querier) GetServiceOperations(
	ctx context.Context, projectID int64, serviceName string) ([]OperationItem, error) {
	const query = `
SELECT SpanName FROM ` + constants.TracingDistTableName + `
WHERE ProjectId = ? AND ServiceName = ? GROUP BY SpanName ORDER BY SpanName ASC`

	var (
		operations []struct {
			SpanName string
		}
		err error
	)
	if q.cloudDB != nil {
		err = q.cloudDB.Select(ctx, &operations, query, strconv.Itoa(int(projectID)), serviceName)
	} else {
		err = q.db.Select(ctx, &operations, query, strconv.Itoa(int(projectID)), serviceName)
	}
	if err != nil {
		return nil, fmt.Errorf("query operations: %w", err)
	}

	ops := make([]OperationItem, len(operations))
	for i, o := range operations {
		ops[i].Name = o.SpanName
	}

	return ops, nil
}
