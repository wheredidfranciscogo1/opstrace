package query

import (
	"context"
	"fmt"
	"strconv"
	"time"

	"github.com/ClickHouse/clickhouse-go/v2/lib/driver"
	"github.com/gin-gonic/gin"
	"go.uber.org/zap"

	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/common"
)

type TracesCountResult struct {
	ProjectID int64 `json:"project_id"`

	Results []*IntervalItem `json:"results,omitempty"`
}

type IntervalItem struct {
	Interval    uint64  `json:"interval,omitempty"`
	TraceCount  uint64  `json:"count,omitempty"`
	P90Duration int64   `json:"p90_duration_nano,omitempty"`
	P95Duration int64   `json:"p95_duration_nano,omitempty"`
	P75Duration int64   `json:"p75_duration_nano,omitempty"`
	P50Duration int64   `json:"p50_duration_nano,omitempty"`
	ErrorCount  uint64  `json:"error_count,omitempty"`
	TraceRate   float64 `json:"trace_rate,omitempty"`
	ErrorRate   float64 `json:"error_rate,omitempty"`
}

type CountDBItem struct {
	Interval    time.Time `ch:"Interval"`
	TraceCount  uint64    `ch:"TraceCount"`
	P90Duration int64     `ch:"P90Duration"`
	P95Duration int64     `ch:"P95Duration"`
	P75Duration int64     `ch:"P75Duration"`
	P50Duration int64     `ch:"P50Duration"`
}

type ErrDBItem struct {
	Interval time.Time `ch:"Interval"`
	ErrCount uint64    `ch:"TraceCount"`
}

const matchedTracesSQL = `
WITH matched_traces AS (
SELECT TraceId FROM (
WITH
    %s(toDateTime(?)) AS r_start,
    %s(toDateTime(?)) AS r_end
SELECT
    TraceId
FROM tracing.gl_traces_main
WHERE (Timestamp >= r_start) AND (Timestamp <= r_end) AND (ProjectId = ?)
`

const traceCountSQL = `
SELECT
    %s(Start) AS Interval,
    quantileExact(0.9)(Duration) AS P90Duration,
    quantileExact(0.95)(Duration) AS P95Duration,
    quantileExact(0.75)(Duration) AS P75Duration,
    quantileExact(0.5)(Duration) AS P50Duration,
    count() AS TraceCount
FROM tracing.gl_traces_rolled
WHERE ProjectId = ? AND ParentSpanId = '' AND TraceId IN matched_traces
`

const errCountSQL = `
SELECT
    %s(Start) AS Interval,
    count() AS TraceCount
FROM tracing.gl_traces_rolled
WHERE ProjectId = ? AND ParentSpanId = '' AND TraceId IN matched_traces
`

func (q *querier) GetTraceAnalytics(
	ctx context.Context,
	projectID int64,
	params *requestParams,
) (*TracesCountResult, error) {
	q.logger.Debug("got param for rates", zap.Any("params", params))

	builder, stepSeconds := buildCountQuery(projectID, params, time.Now().UTC())

	var (
		rows          driver.Rows
		err           error
		mappedResults = make(map[uint64]*IntervalItem)
		results       []*IntervalItem
	)
	if q.cloudDB != nil {
		rows, err = q.cloudDB.Query(ctx, builder.sql, builder.args...)
	} else {
		rows, err = q.db.Query(ctx, builder.sql, builder.args...)
	}
	if err != nil {
		return nil, fmt.Errorf("query count for traces: %w", err)
	}

	for rows.Next() {
		item := &CountDBItem{}
		err = rows.ScanStruct(item)
		if err != nil {
			return nil, fmt.Errorf("scan results from clickhouse: %w", err)
		}

		res := &IntervalItem{
			Interval:    uint64(item.Interval.Unix()),
			TraceCount:  item.TraceCount,
			P90Duration: item.P90Duration,
			P95Duration: item.P95Duration,
			P50Duration: item.P50Duration,
			P75Duration: item.P75Duration,
			TraceRate:   float64(item.TraceCount) / float64(stepSeconds),
		}
		mappedResults[res.Interval] = res
		results = append(results, res)
	}

	errBuilder, stepSeconds := buildErrorCountQuery(projectID, params, time.Now().UTC())
	var errRows driver.Rows
	if q.cloudDB != nil {
		errRows, err = q.cloudDB.Query(ctx, errBuilder.sql, errBuilder.args...)
	} else {
		errRows, err = q.db.Query(ctx, errBuilder.sql, errBuilder.args...)
	}
	if err != nil {
		return nil, fmt.Errorf("query err count for traces: %w", err)
	}

	for errRows.Next() {
		item := &ErrDBItem{}
		err = errRows.ScanStruct(item)
		if err != nil {
			return nil, fmt.Errorf("scan results from clickhouse: %w", err)
		}
		interval := uint64(item.Interval.Unix())

		// This should always be true because if there is an error count then there should be a trace count too.
		if v, ok := mappedResults[interval]; ok {
			v.ErrorCount = item.ErrCount
			v.ErrorRate = float64(item.ErrCount) / float64(stepSeconds)
		} else {
			q.logger.Error(
				"err count aggregate present for interval without an existing count aggregate",
				zap.Any("item", item),
			)
		}
	}
	return &TracesCountResult{Results: results, ProjectID: projectID}, nil
}

func buildMatchedTracesQuery(
	projectID int64,
	params *requestParams,
	refTime time.Time,
	onlyErrors bool,
) (*queryBuilder, int64) {
	b := &queryBuilder{}
	var (
		startTime   interface{}
		endTime     interface{}
		timeFunc    = "toStartOfHour"
		query       string
		stepSeconds int64
	)
	if !params.StartTime.IsZero() && !params.EndTime.IsZero() {
		startTime = params.StartTime
		endTime = params.EndTime
		stepSeconds = int64(params.EndTime.Sub(params.StartTime).Seconds())
		query = fmt.Sprintf(matchedTracesSQL, timeFunc, timeFunc)
	} else {
		startTime, endTime, stepSeconds, timeFunc = common.InferTimelines(refTime, params.Period)
		query = fmt.Sprintf(matchedTracesSQL, timeFunc, timeFunc)
	}
	b.build(
		query,
		startTime,
		endTime, strconv.FormatInt(projectID, 10))

	addWhereFilter(b, params)
	addAttributeFilters(b, params)
	if onlyErrors {
		b.build(`AND StatusCode = 'STATUS_CODE_ERROR'
        `)
	}
	addGroupBy(b)
	b.build(`))
    `)

	return b, stepSeconds
}

func buildCountQuery(projectID int64, params *requestParams, refTime time.Time) (*queryBuilder, int64) {
	b, stepSeconds := buildMatchedTracesQuery(projectID, params, refTime, false)
	//nolint:dogsled
	_, _, _, timeFunc := common.InferTimelines(refTime, params.Period)
	query := fmt.Sprintf(traceCountSQL, timeFunc)
	b.build(query, strconv.FormatInt(projectID, 10))
	addGroupByForCount(b)
	addOrderByForCount(b)

	return b, stepSeconds
}

func addGroupByForCount(b *queryBuilder) {
	b.build(`GROUP BY ProjectId, Interval
    `)
}

func addOrderByForCount(b *queryBuilder) {
	b.build(`ORDER BY Interval
`)
}

type rateHandlerParams struct {
	projectIDParam
	requestParams
}

func (c *Controller) TraceAnalyticsHandler(ctx *gin.Context) {
	handlerParams := &rateHandlerParams{}
	if err := ctx.BindUri(handlerParams); err != nil {
		c.Logger.Error("bind error", zap.Error(err))
		// context already canceled by BindUri
		return
	}

	projectID := handlerParams.ProjectID
	logger := c.Logger.With(
		zap.Int64("project_id", projectID))

	params := &requestParams{}
	if err := ctx.BindQuery(params); err != nil {
		logger.Error("bind trace params", zap.Error(err))
		return
	}

	logger.Debug("trace request params", zap.Any("params", params))

	if err := params.validate(); err != nil {
		ctx.AbortWithError(400, err)
		return
	}

	result, err := c.Q.GetTraceAnalytics(ctx, projectID, params)
	if err != nil {
		logger.Error("get rate for traces", zap.Error(err))
		ctx.AbortWithError(500, err)
		return
	}
	ctx.JSON(200, result)
}

func buildErrorCountQuery(projectID int64, params *requestParams, refTime time.Time) (*queryBuilder, int64) {
	b, stepSeconds := buildMatchedTracesQuery(projectID, params, refTime, true)
	//nolint:dogsled
	_, _, _, timeFunc := common.InferTimelines(refTime, params.Period)
	query := fmt.Sprintf(errCountSQL, timeFunc)
	b.build(query, strconv.FormatInt(projectID, 10))
	addGroupByForCount(b)
	addOrderByForCount(b)

	return b, stepSeconds
}
