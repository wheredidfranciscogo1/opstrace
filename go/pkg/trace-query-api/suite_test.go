package query

import (
	"context"
	"testing"
	"time"

	"github.com/gin-gonic/gin"
	"github.com/google/uuid"
	. "github.com/onsi/ginkgo/v2"
	. "github.com/onsi/gomega"
	"go.uber.org/zap"
	"go.uber.org/zap/zapcore"
)

var testTraceTimestamp = time.Date(2020, time.August, 11, 5, 2, 12, 123456789, time.UTC)

type mockQueryDB struct {
	passedFilter *requestParams
	err          error
}

func (m *mockQueryDB) Close() error {
	return nil
}

func (m *mockQueryDB) GetTraces(ctx context.Context, projectID int64, filter *requestParams) ([]*TraceItem, string, error) {
	m.passedFilter = filter
	traceID := uuid.New().String()
	return []*TraceItem{
		{
			Timestamp:            testTraceTimestamp,
			TimestampNano:        testTraceTimestamp.UnixNano(),
			TraceID:              traceID,
			ServiceName:          "sample-service",
			Operation:            "sample-operation",
			StatusCode:           "OK",
			StatusCodeDeprecated: "OK",
			TotalSpans:           2,
			TotalSpansDeprecated: 2,
		},
	}, "", nil
}

func (m *mockQueryDB) GetTrace(ctx context.Context, projectID int64, traceID string) (*TraceDetailedItem, error) {
	if traceID == "12b797ee-7c32-a226-bf56-d4b3af18af56" {
		// Mock the not-found-in-db case
		return nil, nil
	}
	return &TraceDetailedItem{
		Timestamp:     testTraceTimestamp,
		TimestampNano: testTraceTimestamp.UnixNano(),
		TraceID:       traceID,
		ServiceName:   "sample-service",
		Operation:     "sample-operation",
		StatusCode:    "OK",
		Spans: []SpanDetailedItem{
			{
				Timestamp:     testTraceTimestamp,
				TimestampNano: testTraceTimestamp.UnixNano(),
				SpanID:        "uuid",
				TraceID:       traceID,
				ServiceName:   "sample-service",
				Operation:     "sample-operation",
				DurationNano:  1000,
				ParentSpanID:  "",
				StatusCode:    "OK",
				StatusMessage: "foo",
				ScopeVersion:  "bar",
				ScopeName:     "baz",
				SpanKind:      "server",
				TraceState:    "foobar",
				SpanAttributes: map[string]string{
					"http.route":  "/test",
					"custom.attr": "my_value",
				},
				ResourceAttributes: map[string]string{
					"service.name": "sample-service",
				},
			},
			{
				Timestamp:     testTraceTimestamp.Add(time.Millisecond * 10),
				TimestampNano: testTraceTimestamp.Add(time.Millisecond * 10).UnixNano(),
				SpanID:        "uuid",
				TraceID:       traceID,
				ServiceName:   "sample-service",
				Operation:     "sample-operation",
				DurationNano:  1000,
				ParentSpanID:  "",
				StatusCode:    "OK",
				StatusMessage: "foo",
				ScopeVersion:  "bar",
				ScopeName:     "baz",
				SpanKind:      "server",
				TraceState:    "foobar",
				SpanAttributes: map[string]string{
					"http.route":  "/test",
					"custom.attr": "my_value",
				},
				ResourceAttributes: map[string]string{
					"service.name": "sample-service",
				},
			},
		},
	}, nil
}

func (m *mockQueryDB) GetServices(ctx context.Context, projectID int64) ([]ServiceItem, error) {
	if m.err != nil {
		return nil, m.err
	}
	return []ServiceItem{
		{
			Name: "sample-service",
		},
	}, nil
}

func (m *mockQueryDB) GetServiceOperations(ctx context.Context, projectID int64, serviceName string) ([]OperationItem, error) {
	if m.err != nil {
		return nil, m.err
	}
	return []OperationItem{
		{
			Name: "sample-operation",
		},
	}, nil
}

func (m *mockQueryDB) GetTraceAnalytics(ctx context.Context, projectID int64, params *requestParams) (*TracesCountResult, error) {
	if m.err != nil {
		return nil, m.err
	}
	m.passedFilter = params
	return &TracesCountResult{
		Results: []*IntervalItem{
			{
				Interval:    1000000,
				TraceCount:  2,
				P90Duration: 1000,
				P95Duration: 1000,
				P75Duration: 1000,
				P50Duration: 1000,
				ErrorCount:  3,
			},
		},
		ProjectID: 1,
	}, nil
}

var (
	logger *zap.SugaredLogger
)

func TestTraceQueryAPI(t *testing.T) {
	RegisterFailHandler(Fail)
	suiteConfig, reporterConfig := GinkgoConfiguration()
	RunSpecs(t, "Trace Query API Test suite", suiteConfig, reporterConfig)
}

var _ = BeforeSuite(func() {
	config := zap.NewDevelopmentConfig()
	sink := zapcore.AddSync(GinkgoWriter)
	encoder := zapcore.NewConsoleEncoder(config.EncoderConfig)
	core := zapcore.NewCore(encoder, sink, zap.DebugLevel)
	log := zap.New(core)
	log = log.WithOptions(zap.ErrorOutput(sink))
	logger = log.Sugar()

	gin.SetMode(gin.DebugMode)
	gin.DefaultWriter = GinkgoWriter
})

var _ = AfterSuite(func() {
	err := logger.Sync()
	Expect(err).ToNot(HaveOccurred())
})
