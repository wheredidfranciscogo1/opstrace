package query

import (
	"errors"
	"net/http"
	"net/http/httptest"

	"github.com/gin-gonic/gin"
	. "github.com/onsi/ginkgo/v2"
	. "github.com/onsi/gomega"
)

var _ = Context("services handler", func() {
	var (
		router *gin.Engine
	)

	BeforeEach(func() {
		router = gin.New()
	})

	type expected struct {
		dbErr      error
		statusCode int
		json       string
	}

	test := func(url string, expected expected) {
		q := &mockQueryDB{
			err: expected.dbErr,
		}
		controller := &Controller{
			Q:      q,
			Logger: logger.Desugar(),
		}
		SetRoutes(controller, router)

		recorder := httptest.NewRecorder()
		testReq, err := http.NewRequest(http.MethodGet, url, nil)
		Expect(err).NotTo(HaveOccurred())
		router.ServeHTTP(recorder, testReq)
		Expect(recorder).To(HaveHTTPStatus(expected.statusCode))
		if expected.json != "" {
			Expect(recorder).To(HaveHTTPBody(MatchJSON(expected.json)))
		}
	}

	DescribeTable("list services", test,
		Entry("should return 200 with service name", "/v3/query/1/services",
			expected{
				statusCode: http.StatusOK,
				json: `{
				"services": [{
					"name": "sample-service"
				}]}`,
			}),
		Entry("should return 400 with invalid project id", "/v3/query/invalid/services",
			expected{
				statusCode: http.StatusBadRequest,
			}),
		Entry("should return 500 with db error", "/v3/query/1/services",
			expected{
				dbErr:      errors.New("test err"),
				statusCode: http.StatusInternalServerError,
			}),
	)

	DescribeTable("list service operations", test,
		Entry("should return 200 with service operations", "/v3/query/1/services/sample-service/operations",
			expected{
				statusCode: http.StatusOK,
				json: `{
					"operations": [{
						"name": "sample-operation"
					}]
				}`}),
		Entry("should return 400 with invalid project id", "/v3/query/invalid/services/sample-service/operations",
			expected{
				statusCode: http.StatusBadRequest,
			}),
		Entry("should return 500 with db error", "/v3/query/1/services/sample-service/operations",
			expected{
				dbErr:      errors.New("test err"),
				statusCode: http.StatusInternalServerError,
			}),
	)
})
