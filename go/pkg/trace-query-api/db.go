package query

import (
	"context"
	"fmt"
	"strconv"
	"strings"
	"time"

	"github.com/ClickHouse/clickhouse-go/v2"
	"github.com/ClickHouse/clickhouse-go/v2/lib/driver"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/common"
	"go.uber.org/zap"
)

type QueryDB interface {
	GetTraces(ctx context.Context, projectID int64, param *requestParams) ([]*TraceItem, string, error)
	// GetTrace returns the detailed version of the trace that includes all attributes on all spans
	GetTrace(ctx context.Context, projectID int64, traceID string) (*TraceDetailedItem, error)
	// GetServices returns span service names associated with a project.
	GetServices(ctx context.Context, projectID int64) ([]ServiceItem, error)
	// GetServiceOperations returns span operation names associated with a project and service.
	GetServiceOperations(ctx context.Context, projectID int64, serviceName string) ([]OperationItem, error)

	GetTraceAnalytics(ctx context.Context, projectID int64, params *requestParams) (*TracesCountResult, error)
	Close() error
}

type querier struct {
	db      clickhouse.Conn
	cloudDB clickhouse.Conn
	logger  *zap.Logger
}

func NewQuerier(
	clickHouseDsn string,
	clickHouseCloudDSN string,
	opts *clickhouse.Options,
	logger *zap.Logger) (QueryDB, error) {
	dbOpts, err := clickhouse.ParseDSN(clickHouseDsn)
	if err != nil {
		return nil, fmt.Errorf("failed to parse clickhouse DSN: %w", err)
	}
	if opts != nil {
		dbOpts.MaxOpenConns = opts.MaxOpenConns
		dbOpts.MaxIdleConns = opts.MaxIdleConns
	}
	dbOpts.ConnMaxLifetime = 1 * time.Hour
	dbOpts.Compression = &clickhouse.Compression{Method: clickhouse.CompressionLZ4}
	if logger.Level() == zap.DebugLevel {
		dbOpts.Debug = true
	}

	db, err := clickhouse.Open(dbOpts)
	if err != nil {
		return nil, fmt.Errorf("open DB :%w", err)
	}

	ctx, cancel := context.WithTimeout(context.Background(), time.Second*3)
	defer cancel()
	err = db.Ping(ctx)
	if err != nil {
		return nil, fmt.Errorf("connecting to clickhouse: %w", err)
	}

	var cloudDB clickhouse.Conn
	if clickHouseCloudDSN != "" {
		cloudDB, err = newCloudDB(clickHouseCloudDSN)
		if err != nil {
			return nil, fmt.Errorf("clickhouse cloud :%w", err)
		}
	}

	return &querier{db: db, logger: logger, cloudDB: cloudDB}, nil
}

func newCloudDB(clickHouseDSN string) (clickhouse.Conn, error) {
	dbOpts, err := clickhouse.ParseDSN(clickHouseDSN)
	if err != nil {
		return nil, fmt.Errorf("failed to parse clickhouse DSN: %w", err)
	}
	dbOpts.MaxOpenConns = 100
	dbOpts.MaxIdleConns = 20
	dbOpts.ConnMaxLifetime = 1 * time.Hour
	dbOpts.Compression = &clickhouse.Compression{Method: clickhouse.CompressionLZ4}

	// higher dial timeout as cloud db connection can show variable latencies
	dbOpts.DialTimeout = time.Second * 60
	conn, err := clickhouse.Open(dbOpts)

	if err != nil {
		return nil, fmt.Errorf("clickhouse open: %w", err)
	}

	ctx, cancel := context.WithTimeout(context.Background(), time.Second*60)
	defer cancel()
	err = conn.Ping(ctx)
	if err != nil {
		return nil, fmt.Errorf("failed to ping cloud db %w", err)
	}

	return conn, nil
}

const traceSearch = `
WITH matched_traces AS (
SELECT TraceId, matched_span_count FROM (
WITH
    %s(toDateTime(?)) AS r_start,
    %s(toDateTime(?)) AS r_end
SELECT
    TraceId,
    count() AS matched_span_count,
    min(Timestamp) as TraceStart,
    max(Duration) as LongestSpanDuration
FROM tracing.gl_traces_main
WHERE (Timestamp >= r_start) AND (Timestamp <= r_end) AND (ProjectId = ?)`

const spanSelect = `SELECT
    UUIDNumToString(TraceId) AS traceID,
    min(Start) as TraceStart,
    max(Duration) as LongestSpanDuration,
    arraySort((x) -> (x.8, x.2), groupArray(
        (
            hex(SpanId) AS spanID,
            Start AS start,
            ServiceName AS serviceName,
            SpanName AS operation,
            StatusCode AS statusCode,
            Duration AS duration,
            if(ParentSpanId = '', '', hex(ParentSpanId)) as parentSpanID,
            if(ParentSpanId = '', 1, 2) as rootSpanOrderKey,
            if(StatusCode = 'STATUS_CODE_ERROR', true, false) AS hasError
        )
    )) AS spans,
    length(spans) AS total_spans,
    any(matched_traces.matched_span_count) AS MatchedSpanCount
FROM tracing.gl_traces_rolled
INNER JOIN matched_traces ON matched_traces.TraceId = gl_traces_rolled.TraceId
WHERE (ProjectId = ?)
`

const spanSelectTraceIDVersion = `SELECT
    UUIDNumToString(TraceId) AS traceID,
    min(Start) as TraceStart,
    max(Duration) as LongestSpanDuration,
    arraySort((x) -> (x.8, x.2), groupArray(
        (
            hex(SpanId) AS spanID,
            Start AS start,
            ServiceName AS serviceName,
            SpanName AS operation,
            StatusCode AS statusCode,
            Duration AS duration,
            if(ParentSpanId = '', '', hex(ParentSpanId)) as parentSpanID,
            if(ParentSpanId = '', 1, 2) as rootSpanOrderKey,
            if(StatusCode = 'STATUS_CODE_ERROR', true, false) AS hasError
        )
    )) AS spans,
    length(spans) AS total_spans
FROM tracing.gl_traces_rolled
WHERE (ProjectId = ?)`

func buildTraceMVQuery(projectID int64, param *requestParams, refTime time.Time) *queryBuilder {
	b := &queryBuilder{}
	// special case if traceIDs are provided in param but no period params
	if param.Period == "" && len(param.TraceIDs) > 0 {
		b.build(spanSelectTraceIDVersion, strconv.FormatInt(projectID, 10))
		addWhereFilter(b, param)
		addGroupBy(b)
		addHavingFilter(b, param)
		addOrderBy(b, param)
		addLimit(b, param)

		return b
	}

	var (
		startTime  interface{}
		endTime    interface{}
		timeFunc   = "toStartOfHour"
		traceQuery string
	)
	if !param.StartTime.IsZero() && !param.EndTime.IsZero() {
		startTime = param.StartTime
		endTime = param.EndTime
		traceQuery = fmt.Sprintf(traceSearch, timeFunc, timeFunc)
	} else {
		startTime, endTime, _, timeFunc = common.InferTimelines(refTime, param.Period)
		traceQuery = fmt.Sprintf(traceSearch, timeFunc, timeFunc)
	}

	b.build(
		traceQuery,
		startTime,
		endTime, strconv.FormatInt(projectID, 10))

	addWhereFilter(b, param)
	addAttributeFilters(b, param)
	addGroupBy(b)
	addHavingFilter(b, param)
	addOrderBy(b, param)
	addLimit(b, param)
	b.build(`))
    `)
	b.build(spanSelect, strconv.FormatInt(projectID, 10))
	b.build(` GROUP BY TraceId
    `)
	addOrderBy(b, param)

	return b
}

type DBItem struct {
	TraceID             string    `ch:"traceID" json:"trace_id"`
	TraceStartTS        time.Time `ch:"TraceStart"`
	TotalSpans          uint64    `ch:"total_spans"`
	LongestSpanDuration *int64    `ch:"LongestSpanDuration"`
	Values              [][]any   `ch:"spans"`
	MatchedSpanCount    uint64    `ch:"MatchedSpanCount"`
}

// Wrapper for TraceItem to hold spans temporarily for page processing
type traceItem struct {
	*TraceItem
	spans []spanItem
}

type spanItem struct {
	Timestamp time.Time
	SpanID    string
}

func (q *querier) GetTraces(
	ctx context.Context,
	projectID int64,
	param *requestParams) ([]*TraceItem, string, error) {
	q.logger.Debug("got param", zap.Any("params", param))
	results := []traceItem{}
	builder := buildTraceMVQuery(projectID, param, time.Now().UTC())

	var rows driver.Rows
	var err error
	if q.cloudDB != nil {
		rows, err = q.cloudDB.Query(ctx, builder.sql, builder.args...)
	} else {
		rows, err = q.db.Query(ctx, builder.sql, builder.args...)
	}
	if err != nil {
		return nil, "", fmt.Errorf("query traces: %w", err)
	}

	for rows.Next() {
		item := &DBItem{}
		err = rows.ScanStruct(item)
		if err != nil {
			return nil, "", fmt.Errorf("scan results from clickhouse: %w", err)
		}

		trace := traceItem{
			TraceItem: &TraceItem{
				TraceID:              item.TraceID,
				TotalSpans:           item.TotalSpans,
				TotalSpansDeprecated: item.TotalSpans,
				Timestamp:            item.TraceStartTS,
				TimestampNano:        item.TraceStartTS.UnixNano(),
				MatchedSpanCount:     item.MatchedSpanCount,
			},
			spans: make([]spanItem, len(item.Values)),
		}

		//nolint:errcheck
		for i, v := range item.Values {
			// TODO(Arun): Investigate if we can improve marshaling strategy.
			// This is a straight pass from DB results and should be fastest.
			spanItem := spanItem{}
			spanItem.SpanID = v[0].(string)
			spanItem.Timestamp = v[1].(time.Time)

			trace.spans[i] = spanItem

			if i == 0 {
				serviceName := v[2].(string)
				operation := v[3].(string)
				statusCode := v[4].(string)
				durationNano := uint64(v[5].(int64))

				// The first span will always be root span as they are ordered by start timestamp.
				trace.ServiceName = serviceName
				trace.Operation = operation
				trace.StatusCode = statusCode
				// Estimate duration of the trace from the root span.
				// See discussion on https://gitlab.com/gitlab-org/opstrace/opstrace/-/issues/2274.
				trace.DurationNano = durationNano
			}

			hasError := v[8].(bool)
			if hasError {
				trace.ErrorSpanCount += 1
			}
		}
		// Cases where direct spans are fetched so CTE is not used, instead just return total spans.
		if trace.MatchedSpanCount == 0 {
			trace.MatchedSpanCount = trace.TotalSpans
		}
		results = append(results, trace)
	}

	nextPageToken, err := getNextPageToken(results, param)
	if err != nil {
		return nil, "", fmt.Errorf("encoding page cursor: %w", err)
	}

	r := make([]*TraceItem, len(results))
	for i, v := range results {
		r[i] = v.TraceItem
	}

	return r, nextPageToken, nil
}

func (q *querier) Close() error {
	err := q.db.Close()
	if err != nil {
		return fmt.Errorf("close DB: %w", err)
	}
	err = q.cloudDB.Close()
	if err != nil {
		return fmt.Errorf("close cloud DB :%w", err)
	}
	return nil
}

func getNextPageToken(results []traceItem, param *requestParams) (string, error) {
	//nolint:lll
	// The logic to pick up the next cursor is described below.
	// The SQL query looks like this:
	//	WITH
	//	toStartOfHour(toDateTime(1694438312)) AS r_start,
	//	toStartOfHour(toDateTime(1694528312)) AS r_end
	//	SELECT
	//	UUIDNumToString(TraceId) AS traceID,
	//		min(Start) as TraceStart,
	//		groupArray(
	//			(hex(SpanId) AS spanID, Start AS start, ServiceName AS serviceName, SpanName AS operation, StatusCode AS statusCode, Duration AS duration, if(ParentSpanId = '', '', hex(ParentSpanId)) as parentSpanID)
	// ) AS spans,
	//		length(spans) AS total_spans
	//	FROM gl_traces_rolled
	//	WHERE (Start > r_start) AND (Start < r_end) AND (ProjectId = '123')
	//	GROUP BY TraceId
	//	ORDER BY TraceStart DESC
	//	LIMIT $PAGE_SIZE
	// The results returned from the query are sorted descending when returned.
	// MV as defined will keep the ASC order of included spans to enforce the root span and child span relationship.
	// Taking an example with span timestamps:
	// Full result of 5 total items without pagination can be : [ {t5, ..}, {t4, ..}, {t3, ..}, {t2, ..}, {t1, ..} ]
	// After enforcing page size of 3, the result is : [ {t5, ..}, {t4, ..}, {t3, ..} ]
	// So in order to pick the next page, the cursor should be set to choose items earlier than t3
	// i.e. 'HAVING TraceStart < results[-1].Timestamp'
	// The next result will be:  [ {t2 ..}, {t1, ..} ].

	// Note(Arun): Currently there is no indication if there are no more result rows.
	// So we always return a next page token in responses. Clients will see no results
	// after latest rows were returned in previous response.
	n := len(results)
	if n == 0 {
		return "", nil
	}

	if param.Sort == SortDurationDesc || param.Sort == SortDurationAsc {
		lastSeenDuration := results[n-1].DurationNano
		seenTraceIds := make([]string, 0)
		// Since traces are sorted from longest duration to shortest
		// duration, our keyset pagination relies on the last seen
		// duration as the starting point for the next page query, where
		// the next page query will be `HAVING LongestSpanDuration <= last_seen_duration`.
		// We must use the `<=` operator since there could be multiple traces
		// that share the same duration and some are returned in this page query
		// but not all. So in the next page query, we have `<=` to ensure we don't
		// accidentally skip any traces that share the same last_seen_duration.

		// Get all traceIds already sent in this page that share the
		// last seen duration. We iterate over the slice in reverse
		// because we care about the last_seen_duration in the sorting
		// order of this page, same reasoning as we pick the last timestamp
		// further below if sorting is by most recent.
		for i := n - 1; i >= 0; i-- {
			if results[i].DurationNano == lastSeenDuration {
				seenTraceIds = append(seenTraceIds, results[i].TraceID)
			} else {
				break
			}
		}
		nextPage := &Page{
			LastSeenDuration: lastSeenDuration,
			SeenTraceIds:     seenTraceIds,
		}
		return encodePage(nextPage)
	}
	// See comment for more details on logic and reasoning
	// https://gitlab.com/gitlab-org/opstrace/opstrace/-/merge_requests/2311#note_1619238295
	seenTraceIds := make([]string, 0)
	// Have to use root span Timestamp:
	lastSeenTimestamp := results[n-1].spans[0].Timestamp
	for i := n - 1; i >= 0; i-- {
		if results[i].spans[0].Timestamp == lastSeenTimestamp {
			seenTraceIds = append(seenTraceIds, results[i].TraceID)
		} else {
			break
		}
	}
	nextPage := &Page{
		LastSeenTimestamp: lastSeenTimestamp.Format("2006-01-02 15:04:05.000000000"),
		SeenTraceIds:      seenTraceIds,
	}
	return encodePage(nextPage)
}

func mapTraceIDsToFuncString(traceIDs []string) (string, []interface{}) {
	// TODO(Arun): This does not work as expected because of a bug in
	// ClickHouse https://github.com/ClickHouse/ClickHouse/issues/14980
	// b.build(" AND ( TraceId IN arrayMap(x -> UUIDStringToNum(x), ? ) ) ", param.TraceIDs)

	// Workaround to the above transformation to wrap the
	// list of traceIDs inside the conversion function
	// on the client side. This has the same effect of
	// the above query and uses the primary index.

	transformed := make([]interface{}, len(traceIDs))
	for i, v := range traceIDs {
		transformed[i] = v
	}
	// Note that we pass each string as a separate bind parameter instead of binding the whole array as
	// parameter. This is to avoid quoting the conversion function itself.
	// TrimRight is to remove an extra "," at the end of the string.
	return strings.TrimRight(strings.Repeat(" UUIDStringToNum(?),", len(transformed)), ","), transformed
}

func addWhereFilter(b *queryBuilder, param *requestParams) {
	if param.TraceIDs != nil {
		appliedFunc, transformed := mapTraceIDsToFuncString(param.TraceIDs)

		b.build(" AND ( TraceId IN ["+appliedFunc+" ] ) ", transformed...)
	}

	if param.ServiceNames != nil {
		b.build(" AND ( ServiceName IN ( ? ) ) ", param.ServiceNames)
	}
	if param.NotServiceNames != nil {
		b.build(" AND ( ServiceName NOT IN ( ? ) ) ", param.NotServiceNames)
	}
	if param.Operations != nil {
		b.build(" AND ( SpanName IN ( ? ) ) ", param.Operations)
	}
	if param.NotOperations != nil {
		b.build(" AND ( SpanName NOT IN ( ? ) ) ", param.NotOperations)
	}
	if param.LtDuration != 0 {
		b.build(" AND ( Duration <= ? ) ", param.LtDuration)
	}
	if param.GtDuration != 0 {
		b.build(" AND ( Duration >= ? ) ", param.GtDuration)
	}
	if param.Statuses != nil {
		b.build(" AND ( StatusCode IN ( ? ) ) ", param.Statuses)
	}
	if param.NotStatuses != nil {
		b.build(" AND ( StatusCode NOT IN ( ? ) ) ", param.Statuses)
	}
	// Add newline after params applied
	b.build(`
    `)
}

// separate from the where filter as it's only applied to the main table
func addAttributeFilters(b *queryBuilder, param *requestParams) {
	if len(param.AttributeNames) == 0 {
		return
	}

	for i, n := range param.AttributeNames {
		v := param.AttributeValues[i]
		b.build(" AND ( ResourceAttributes[?] = ? OR SpanAttributes[?] = ? ) ", n, v, n, v)
	}
	b.build(`
    `)
}

func addGroupBy(b *queryBuilder) {
	b.build(`GROUP BY TraceId
    `)
}

func addOrderBy(b *queryBuilder, param *requestParams) {
	switch param.Sort {
	case SortDurationDesc:
		b.build(`ORDER BY LongestSpanDuration DESC
    `)
	case SortDurationAsc:
		b.build(`ORDER BY LongestSpanDuration ASC
    `)
	case SortTimestampAsc:
		b.build(`ORDER BY TraceStart ASC
    `)
	default:
		b.build(`ORDER BY TraceStart DESC
    `)
	}
}

func addHavingFilter(b *queryBuilder, param *requestParams) {
	if param.cursor != nil && param.cursor.LastSeenTimestamp != "" {
		appliedFunc, transformed := mapTraceIDsToFuncString(param.cursor.SeenTraceIds)
		args := append(
			[]interface{}{param.cursor.LastSeenTimestamp},
			transformed...,
		)
		direction := "<"
		if param.Sort == SortTimestampAsc {
			direction = ">"
		}
		b.build(fmt.Sprintf("HAVING TraceStart %s= ? AND ( TraceId NOT IN ["+appliedFunc+" ] )", direction),
			args...)
		b.build(`
    `)

		return
	}

	if param.cursor != nil && param.cursor.LastSeenDuration > 0 {
		appliedFunc, transformed := mapTraceIDsToFuncString(param.cursor.SeenTraceIds)
		args := append(
			[]interface{}{param.cursor.LastSeenDuration},
			transformed...,
		)
		direction := "<"
		if param.Sort == SortDurationAsc {
			direction = ">"
		}
		b.build(fmt.Sprintf("HAVING LongestSpanDuration %s= ? AND ( TraceId NOT IN ["+appliedFunc+" ] )", direction),
			args...)
		b.build(`
    `)
	}
}

func addLimit(b *queryBuilder, param *requestParams) {
	const defaultPageSize = 100
	const queryLimit = `LIMIT %d
    `

	pageSize := int64(defaultPageSize)
	if param.PageSize > 0 {
		pageSize = param.PageSize
	}
	b.build(fmt.Sprintf(queryLimit, pageSize))
}
