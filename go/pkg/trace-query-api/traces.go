package query

import (
	"fmt"
	"net/http"
	"strings"
	"time"

	"github.com/gin-gonic/gin"
	"github.com/google/uuid"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/common"
	"go.uber.org/zap"
	"golang.org/x/exp/slices"
)

type TracesResult struct {
	ProjectID int64 `json:"project_id"`

	Traces      []*TraceItem `json:"traces"`
	TotalTraces int64        `json:"total_traces"`

	// NextPageToken is the token for next page if the results are greater than page size.
	NextPageToken string `json:"next_page_token,omitempty"`
}

type TraceItem struct {
	// Snake Case all attributes
	Timestamp        time.Time `json:"timestamp"`
	TimestampNano    int64     `json:"timestamp_nano"`
	TraceID          string    `json:"trace_id"`
	ServiceName      string    `json:"service_name"`
	Operation        string    `json:"operation"`
	StatusCode       string    `json:"status_code"`
	DurationNano     uint64    `json:"duration_nano"`
	TotalSpans       uint64    `json:"total_spans"`
	MatchedSpanCount uint64    `json:"matched_span_count"`
	ErrorSpanCount   uint64    `json:"error_span_count"`
	// Deprecated
	TotalSpansDeprecated uint64 `json:"totalSpans"`
	StatusCodeDeprecated string `ch:"statusCode" json:"statusCode"`
}

type tracesHandlerParams struct {
	GroupID   int64 `uri:"group_id" binding:"required,numeric,min=1"`
	ProjectID int64 `uri:"project_id" binding:"required,numeric,min=1"`
}

type projectIDParam struct {
	ProjectID int64 `uri:"project_id" binding:"required,numeric,min=1"`
}

type requestParams struct {
	TraceIDs []string `form:"trace_id"`
	Period   string   `form:"period"`
	// Custom range parameters only accept RFC3339 format.
	StartTime       time.Time `form:"start_time"`
	EndTime         time.Time `form:"end_time"`
	Operations      []string  `form:"operation"`
	ServiceNames    []string  `form:"service_name"`
	NotOperations   []string  `form:"not[operation]"`
	NotServiceNames []string  `form:"not[service_name]"`
	NotTraceIDs     []string  `form:"not[trace_id]"`
	Statuses        []string  `form:"status"`
	NotStatuses     []string  `form:"not[status]"`

	AttributeNames  []string `form:"attr_name"`
	AttributeValues []string `form:"attr_value"`

	LtDuration int64 `form:"lt[duration_nano]" binding:"numeric,gte=0"`
	GtDuration int64 `form:"gt[duration_nano]" binding:"numeric,gte=0"`

	Sort string `form:"sort"`

	PageSize  int64  `form:"page_size,default=100" binding:"numeric,gte=0,max=1000"`
	PageToken string `form:"page_token"`

	cursor *Page
}

type APIVersion int64

const (
	NoVersion = iota
	V3
)

type HandlerFactoryOptions struct {
	version APIVersion
}

func (c *Controller) TracesHandlerFactory(options HandlerFactoryOptions) func(ctx *gin.Context) {
	return func(ctx *gin.Context) {
		projectID := int64(0)

		switch options.version {
		case V3:
			handlerParams := &projectIDParam{}
			if err := ctx.BindUri(handlerParams); err != nil {
				c.Logger.Error("bind error", zap.Error(err))
				// context already canceled by BindUri

				return
			}
			projectID = handlerParams.ProjectID
		default:
			handlerParams := &tracesHandlerParams{}
			if err := ctx.BindUri(handlerParams); err != nil {
				c.Logger.Error("bind error", zap.Error(err))
				// context already canceled by BindUri

				return
			}
			projectID = handlerParams.ProjectID
		}

		logger := c.Logger.With(
			zap.Int64("project_id", projectID))

		params := &requestParams{}
		if err := ctx.BindQuery(params); err != nil {
			logger.Error("bind trace params", zap.Error(err))
			return
		}

		logger.Debug("trace request params", zap.Any("params", params))

		if err := params.validate(); err != nil {
			ctx.AbortWithError(400, err)
			return
		}

		if params.PageToken != "" {
			cursor, err := decodePage(params.PageToken)
			if err != nil {
				logger.Error("decoding page:", zap.Error(err))
				ctx.AbortWithError(http.StatusBadRequest, err)
				return
			}
			params.cursor = cursor
		}

		results, nextPageToken, err := c.Q.GetTraces(ctx, projectID, params)
		if err != nil {
			logger.Error("get traces", zap.Error(err))
			ctx.AbortWithError(500, err)
			return
		}

		resp := &TracesResult{
			ProjectID:     projectID,
			TotalTraces:   int64(len(results)),
			Traces:        results,
			NextPageToken: nextPageToken,
		}
		ctx.JSON(200, resp)
	}
}

// Helper to register metrics for traces Handler
// In order to avoid cardinality issues with metrics additionally sliced with path that have groupID/projectID
// just return the handler path.
func stripProjectIDFromPath(path string) string {
	if strings.Contains(path, "/v1/traces") {
		return "/v1/traces"
	}
	return path
}

const (
	SortingDirective = "sort"

	SortDurationDesc = "duration_desc"
	SortDurationAsc  = "duration_asc"

	SortTimestampDesc = "timestamp_desc"
	SortTimestampAsc  = "timestamp_asc"
)

var SupportedSortingDirectives = []string{
	SortTimestampDesc,
	SortDurationDesc,
	SortTimestampAsc,
	SortDurationAsc,
}

var ValidPeriods = map[string]bool{
	common.Period1m:  true,
	common.Period5m:  true,
	common.Period15m: true,
	common.Period30m: true,
	common.Period1h:  true,
	common.Period4h:  true,
	common.Period12h: true,
	common.Period24h: true,
	common.Period7d:  true,
	common.Period14d: true,
	common.Period30d: true,
}

var ValidStatus = map[string]string{
	"ok":    "STATUS_CODE_OK",
	"error": "STATUS_CODE_ERROR",
}

//nolint:cyclop
func (r *requestParams) validate() error {
	if err := validateTraceIDs(r.TraceIDs); err != nil {
		return err
	}
	if err := validateTraceIDs(r.NotTraceIDs); err != nil {
		return err
	}

	if r.Period != "" {
		valid := ValidPeriods[r.Period]
		if !valid {
			return fmt.Errorf("invalid period: %s", r.Period)
		}
	}

	// Either period or a time range pair should be provided but not both.
	if r.Period != "" && (!r.StartTime.IsZero() || !r.EndTime.IsZero()) {
		return fmt.Errorf("both period and time range are provided")
	}
	if (!r.StartTime.IsZero() && r.EndTime.IsZero()) || (!r.EndTime.IsZero() && r.StartTime.IsZero()) {
		return fmt.Errorf("both ends for time range are not provided")
	}

	if r.Sort != "" {
		if !slices.Contains(SupportedSortingDirectives, r.Sort) {
			return fmt.Errorf("unsupported sort - %s", r.Sort)
		}
	}

	if len(r.AttributeNames) != len(r.AttributeValues) {
		return fmt.Errorf("attribute names and values must be the same length")
	}

	if err := validateStatus(r.Statuses); err != nil {
		return err
	}
	if err := validateStatus(r.NotStatuses); err != nil {
		return err
	}
	return nil
}

// Validate slice trace ids against uuid.Parse
// https://pkg.go.dev/github.com/google/uuid@v1.3.1#Parse
// Updates the ids with standard 36 char string representation that works with ClickHouse
// i.e. xxxxxxxx-xxxx-xxxx-xxxx-xxxxxxxxxxxx
func validateTraceIDs(ids []string) error {
	for i, t := range ids {
		id, err := uuid.Parse(t)
		if err != nil {
			return fmt.Errorf("invalid trace_id: %s - %w", t, err)
		}
		ids[i] = id.String()
	}
	return nil
}

func validateStatus(statuses []string) error {
	for i, s := range statuses {
		mapped, ok := ValidStatus[s]
		if !ok {
			return fmt.Errorf("unsupported status filter: %s", s)
		}
		statuses[i] = mapped
	}
	return nil
}
