package migrations

import (
	"bytes"
	"fmt"
	"text/template"

	"gitlab.com/gitlab-org/opstrace/goose/v3"
)

type RegisteredMigration interface {
	Name() string
	Schema() string
	ValidateData(interface{}) error
	Render(interface{}) (string, error)
	Setup(interface{}) (*goose.Migration, error)
}

func Render(tmplName, tmpl string, data interface{}) (string, error) {
	t, err := template.New(tmplName).Parse(tmpl)
	if err != nil {
		return "", fmt.Errorf("parsing template %s: %w", tmplName, err)
	}
	var buf bytes.Buffer
	if err := t.Execute(&buf, data); err != nil {
		return "", fmt.Errorf("executing template %s: %w", tmplName, err)
	}
	return buf.String(), nil
}
