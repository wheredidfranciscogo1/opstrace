package logging

// MigrationData defines all data we need to render logging-specific
// migrations.
type MigrationData struct {
	DatabaseName      string
	TableName         string
	SelfHostedVersion bool
}
