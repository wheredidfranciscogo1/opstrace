package metrics

//nolint:lll
const MetricsMetadataMVTargetTmpl = `
CREATE TABLE {{.DatabaseName}}.{{.TableName}}{{if .SelfHostedVersion}} ON CLUSTER '{cluster}'{{end}}
(
  ProjectId String CODEC(ZSTD(1)),
  MetricName String CODEC(ZSTD(1)),
  MetricDescription String CODEC(ZSTD(1)),
  AttributeKeysState AggregateFunction(groupUniqArrayArray, Array(String)),
  LastIngestedAt DateTime64(9) CODEC(Delta, ZSTD(1))
)
ENGINE = ReplicatedAggregatingMergeTree{{if .SelfHostedVersion}}('/clickhouse/{cluster}/tables/{shard}/{{.TableName}}', '{replica}'){{ end }}
ORDER BY (ProjectId, MetricName);
`

//nolint:lll
const MetricsMetadataMVTmpl = `
CREATE MATERIALIZED VIEW IF NOT EXISTS {{.DatabaseName}}.{{.TableName}}{{if .SelfHostedVersion}} ON CLUSTER '{cluster}'{{end}}
TO {{.DatabaseName}}.{{.TargetTableName}} AS
SELECT
  ProjectId,
  MetricName,
  any(MetricDescription) AS MetricDescription,
  groupUniqArrayArrayState(mapKeys(Attributes)) AS AttributeKeysState,
  max(IngestionTimestamp) AS LastIngestedAt
FROM {{.DatabaseName}}.{{.SourceTableName}}
GROUP BY ProjectId, MetricName
ORDER BY ProjectId, MetricName;
`
