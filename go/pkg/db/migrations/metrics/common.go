package metrics

// MigrationData defines all data we need to render tracing-specific
// migrations.
type MigrationData struct {
	DatabaseName      string
	TableName         string
	SelfHostedVersion bool
}

type MVMigrationData struct {
	DatabaseName      string
	TableName         string
	SelfHostedVersion bool
	TargetTableName   string
	SourceTableName   string
}
