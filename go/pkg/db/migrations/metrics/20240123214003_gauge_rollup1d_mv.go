package metrics

import (
	"context"
	"database/sql"
	"fmt"
	"runtime"

	"gitlab.com/gitlab-org/opstrace/goose/v3"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/db/migrations"
)

//nolint:lll
const GaugeRollup1dMVTmpl = `
CREATE MATERIALIZED VIEW IF NOT EXISTS {{.DatabaseName}}.{{.TableName}}{{if .SelfHostedVersion}} ON CLUSTER '{cluster}'{{end}}
TO {{.DatabaseName}}.{{.TargetTableName}} AS
SELECT
  ProjectId,
  MetricName,
  any(MetricDescription) AS MetricDescription,
  any(MetricUnit) AS MetricUnit,
  max(IngestionTimestamp) AS LastIngestedAt,
  toStartOfDay(TimeUnix) AS TimeUnixDay,
  avgState(Value) AS AvgValueState,
  anyLastState(Value) AS LastValueState
FROM {{.DatabaseName}}.{{.SourceTableName}}
GROUP BY ProjectId, MetricName, TimeUnixDay
ORDER BY ProjectId, MetricName, TimeUnixDay;
`

type GaugeRollup1dMV struct{}

var _ migrations.RegisteredMigration = (*GaugeRollup1dMV)(nil)

func (t *GaugeRollup1dMV) Name() string {
	return "gaugeRollup1dMV"
}

func (t *GaugeRollup1dMV) Schema() string {
	return GaugeRollup1dMVTmpl
}

func (t *GaugeRollup1dMV) ValidateData(data interface{}) error {
	d, ok := data.(MVMigrationData)
	if !ok {
		return fmt.Errorf("passed interface not applicable")
	}
	if d.DatabaseName == "" || d.TableName == "" {
		return fmt.Errorf("any of database or table name cannot be empty")
	}
	if d.SourceTableName == "" || d.TargetTableName == "" {
		return fmt.Errorf("none of source or target table name can be empty")
	}
	return nil
}

func (t *GaugeRollup1dMV) Render(data interface{}) (string, error) {
	return migrations.Render(t.Name(), t.Schema(), data)
}

func (t *GaugeRollup1dMV) Setup(data interface{}) (*goose.Migration, error) {
	// check if this migration has already been setup/registered
	//nolint:dogsled
	_, filename, _, _ := runtime.Caller(0)

	// validate passed data
	if err := t.ValidateData(data); err != nil {
		return nil, err
	}
	// render template
	query, err := migrations.Render(t.Name(), t.Schema(), data)
	if err != nil {
		return nil, fmt.Errorf("rendering registered template %s: %w", t.Name(), err)
	}
	//nolint:errcheck
	v, _ := goose.NumericComponent(filename)
	return goose.NewGoMigration(
		v,
		&goose.GoFunc{
			RunTx: func(ctx context.Context, tx *sql.Tx) error {
				_, err := tx.Exec(query)
				//nolint:wrapcheck
				return err
			},
		},
		nil,
	), nil
}
