package metrics

import (
	"context"
	"database/sql"
	"fmt"
	"runtime"

	"gitlab.com/gitlab-org/opstrace/goose/v3"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/db/migrations"
)

//nolint:lll
const HistogramMainTmpl = `
CREATE TABLE IF NOT EXISTS {{.DatabaseName}}.{{.TableName}}{{if .SelfHostedVersion}} ON CLUSTER '{cluster}'{{ end }} (
    ProjectId String CODEC(ZSTD(1)),
    NamespaceId Int64 CODEC(ZSTD(1)),
    Fingerprint String CODEC(ZSTD(1)),
    IngestionTimestamp DateTime64(9) CODEC(Delta, ZSTD(1)),
    ResourceAttributes Map(LowCardinality(String), String) CODEC(ZSTD(1)),
    ResourceSchemaUrl String CODEC(ZSTD(1)),
    ScopeName String CODEC(ZSTD(1)),
    ScopeVersion String CODEC(ZSTD(1)),
    ScopeAttributes Map(LowCardinality(String), String) CODEC(ZSTD(1)),
    ScopeDroppedAttrCount UInt32 CODEC(ZSTD(1)),
    ScopeSchemaUrl String CODEC(ZSTD(1)),
    MetricName String CODEC(ZSTD(1)),
    MetricDescription String CODEC(ZSTD(1)),
    MetricUnit String CODEC(ZSTD(1)),
    Attributes Map(LowCardinality(String), String) CODEC(ZSTD(1)),
    StartTimeUnix DateTime64(9) CODEC(Delta, ZSTD(1)),
    TimeUnix DateTime64(9) CODEC(Delta, ZSTD(1)),
    Count UInt64 CODEC(Delta, ZSTD(1)),
    Sum Float64 CODEC(ZSTD(1)),
    BucketCounts Array(UInt64) CODEC(ZSTD(1)),
    ExplicitBounds Array(Float64) CODEC(ZSTD(1)),
    Exemplars Nested (
        FilteredAttributes Map(LowCardinality(String), String),
        TimeUnix DateTime64(9),
        Value Float64,
        SpanId FixedString(8),
        TraceId FixedString(16)
    ) CODEC(ZSTD(1)),
    Flags UInt32 CODEC(ZSTD(1)),
    AggTemp Int32 CODEC(ZSTD(1)),
    Min Float64 CODEC(ZSTD(1)),
    Max Float64 CODEC(ZSTD(1)),
    INDEX idx_res_attr_key mapKeys(ResourceAttributes) TYPE bloom_filter(0.01) GRANULARITY 1,
    INDEX idx_res_attr_value mapValues(ResourceAttributes) TYPE bloom_filter(0.01) GRANULARITY 1,
    INDEX idx_scope_attr_key mapKeys(ScopeAttributes) TYPE bloom_filter(0.01) GRANULARITY 1,
    INDEX idx_scope_attr_value mapValues(ScopeAttributes) TYPE bloom_filter(0.01) GRANULARITY 1,
    INDEX idx_attr_key mapKeys(Attributes) TYPE bloom_filter(0.01) GRANULARITY 1,
    INDEX idx_attr_value mapValues(Attributes) TYPE bloom_filter(0.01) GRANULARITY 1
) ENGINE ReplicatedMergeTree{{if .SelfHostedVersion}}('/clickhouse/{cluster}/tables/{shard}/metrics_main_histogram', '{replica}'){{ end }}
TTL toDateTime(IngestionTimestamp) + toIntervalDay(7)
PARTITION BY toDate(TimeUnix)
ORDER BY (ProjectId, MetricName, Fingerprint, toUnixTimestamp64Nano(TimeUnix))
SETTINGS index_granularity=8192, ttl_only_drop_parts = 1;
`

type HistMain struct{}

var _ migrations.RegisteredMigration = (*HistMain)(nil)

func (t *HistMain) Name() string {
	return "histMain"
}

func (t *HistMain) Schema() string {
	return HistogramMainTmpl
}

func (t *HistMain) ValidateData(data interface{}) error {
	d, ok := data.(MigrationData)
	if !ok {
		return fmt.Errorf("passed interface not applicable")
	}
	if d.DatabaseName == "" || d.TableName == "" {
		return fmt.Errorf("any of database or table name cannot be empty")
	}
	return nil
}

func (t *HistMain) Render(data interface{}) (string, error) {
	return migrations.Render(t.Name(), t.Schema(), data)
}

func (t *HistMain) Setup(data interface{}) (*goose.Migration, error) {
	// check if this migration has already been setup/registered
	//nolint:dogsled
	_, filename, _, _ := runtime.Caller(0)

	// NOTE(prozlach): A hack meant to support old naming schema. This way we
	// can avoid duplicating whole file. Initially, both migration file had
	// different data IDs enven though they were meant for different
	// environments (cloud/self-hosted) and were 99.9% identical. Ideally we
	// should be re-use the code if possible and not worry about versions as
	// environments differ anyway.
	//nolint:errcheck
	d, _ := data.(MigrationData)
	if !d.SelfHostedVersion {
		filename = "20231120131518_histogram_main.go"
	}

	// validate passed data
	if err := t.ValidateData(data); err != nil {
		return nil, err
	}
	// render template
	query, err := migrations.Render(t.Name(), t.Schema(), data)
	if err != nil {
		return nil, fmt.Errorf("rendering registered template %s: %w", t.Name(), err)
	}
	//nolint:errcheck
	v, _ := goose.NumericComponent(filename)
	return goose.NewGoMigration(
		v,
		&goose.GoFunc{
			RunTx: func(ctx context.Context, tx *sql.Tx) error {
				_, err := tx.Exec(query)
				//nolint:wrapcheck
				return err
			},
		},
		nil,
	), nil
}
