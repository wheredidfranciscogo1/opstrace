package metrics

import (
	"context"
	"database/sql"
	"fmt"
	"runtime"

	"gitlab.com/gitlab-org/opstrace/goose/v3"

	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/db/migrations"
)

//nolint:lll
const GaugeRollup1dMVTargetTmpl = `
CREATE TABLE {{.DatabaseName}}.{{.TableName}}{{if .SelfHostedVersion}} ON CLUSTER '{cluster}'{{end}}
(
  ProjectId String CODEC(ZSTD(1)),
  MetricName String CODEC(ZSTD(1)),
  MetricDescription String CODEC(ZSTD(1)),
  MetricUnit String CODEC(ZSTD(1)),
  LastIngestedAt DateTime64(9) CODEC(Delta, ZSTD(1)),
  TimeUnixDay DateTime64(9) CODEC(Delta, ZSTD(1)),
  AvgValueState AggregateFunction(avg, Float64),
  LastValueState AggregateFunction(anyLast, Float64)
)
ENGINE = ReplicatedAggregatingMergeTree{{if .SelfHostedVersion}}('/clickhouse/{cluster}/tables/{shard}/metrics_main_gauge_1d', '{replica}'){{ end }}
ORDER BY (ProjectId, MetricName, TimeUnixDay);
`

type GaugeRollup1dMVTarget struct{}

var _ migrations.RegisteredMigration = (*GaugeRollup1dMVTarget)(nil)

func (t *GaugeRollup1dMVTarget) Name() string {
	return "gaugeRollup1dMVTarget"
}

func (t *GaugeRollup1dMVTarget) Schema() string {
	return GaugeRollup1dMVTargetTmpl
}

func (t *GaugeRollup1dMVTarget) ValidateData(data interface{}) error {
	d, ok := data.(MVMigrationData)
	if !ok {
		return fmt.Errorf("passed interface not applicable")
	}
	if d.DatabaseName == "" || d.TableName == "" {
		return fmt.Errorf("any of database or table name cannot be empty")
	}
	return nil
}

func (t *GaugeRollup1dMVTarget) Render(data interface{}) (string, error) {
	return migrations.Render(t.Name(), t.Schema(), data)
}

func (t *GaugeRollup1dMVTarget) Setup(data interface{}) (*goose.Migration, error) {
	// check if this migration has already been setup/registered
	//nolint:dogsled
	_, filename, _, _ := runtime.Caller(0)

	// validate passed data
	if err := t.ValidateData(data); err != nil {
		return nil, err
	}
	// render template
	query, err := migrations.Render(t.Name(), t.Schema(), data)
	if err != nil {
		return nil, fmt.Errorf("rendering registered template %s: %w", t.Name(), err)
	}
	//nolint:errcheck
	v, _ := goose.NumericComponent(filename)
	return goose.NewGoMigration(
		v,
		&goose.GoFunc{
			RunTx: func(ctx context.Context, tx *sql.Tx) error {
				_, err := tx.Exec(query)
				//nolint:wrapcheck
				return err
			},
		},
		nil,
	), nil
}
