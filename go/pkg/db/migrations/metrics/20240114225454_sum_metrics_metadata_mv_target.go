package metrics

import (
	"context"
	"database/sql"
	"fmt"
	"runtime"

	"gitlab.com/gitlab-org/opstrace/goose/v3"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/db/migrations"
)

type SumMetricsMetadataMVTarget struct{}

var _ migrations.RegisteredMigration = (*SumMetricsMetadataMV)(nil)

func (t *SumMetricsMetadataMVTarget) Name() string {
	return "SumMetricsMetadataMVTarget"
}

func (t *SumMetricsMetadataMVTarget) Schema() string {
	return MetricsMetadataMVTargetTmpl // we use a common template right now
}

func (t *SumMetricsMetadataMVTarget) ValidateData(data interface{}) error {
	d, ok := data.(MVMigrationData)
	if !ok {
		return fmt.Errorf("passed interface not applicable")
	}
	if d.DatabaseName == "" || d.TableName == "" {
		return fmt.Errorf("any of database or table name cannot be empty")
	}
	return nil
}

func (t *SumMetricsMetadataMVTarget) Render(data interface{}) (string, error) {
	return migrations.Render(t.Name(), t.Schema(), data)
}

func (t *SumMetricsMetadataMVTarget) Setup(data interface{}) (*goose.Migration, error) {
	// check if this migration has already been setup/registered
	//nolint:dogsled
	_, filename, _, _ := runtime.Caller(0)
	// validate passed data
	if err := t.ValidateData(data); err != nil {
		return nil, err
	}
	// render template
	query, err := migrations.Render(t.Name(), t.Schema(), data)
	if err != nil {
		return nil, fmt.Errorf("rendering registered template %s: %w", t.Name(), err)
	}
	//nolint:errcheck
	v, _ := goose.NumericComponent(filename)
	return goose.NewGoMigration(
		v,
		&goose.GoFunc{
			RunTx: func(ctx context.Context, tx *sql.Tx) error {
				_, err := tx.Exec(query)
				//nolint:wrapcheck
				return err
			},
		},
		nil,
	), nil
}
