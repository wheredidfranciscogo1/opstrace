package tracing

import (
	"context"
	"database/sql"
	"fmt"
	"runtime"

	"gitlab.com/gitlab-org/opstrace/goose/v3"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/db/migrations"
)

//nolint:lll
const TracesMVTargetTmpl = `
CREATE TABLE  {{.DatabaseName}}.{{.TableName}}{{if .SelfHostedVersion}} ON CLUSTER '{cluster}'{{end}}
(
	ProjectId String CODEC(ZSTD(1)),
	TraceId FixedString(16) CODEC(ZSTD(1)),
	SpanId FixedString(8) CODEC(ZSTD(1)),
	Start DateTime64(9) CODEC(Delta(8), ZSTD(1)),
	ServiceName String CODEC(ZSTD(1)),
	SpanName String CODEC(ZSTD(1)),
	StatusCode LowCardinality(String) CODEC(ZSTD(1)),
	ParentSpanId FixedString(8) CODEC(ZSTD(1)),
	Duration Int64 CODEC(ZSTD(1))
)
ENGINE = ReplicatedMergeTree{{if .SelfHostedVersion}}('/clickhouse/{cluster}/tables/{shard}/{{.DatabaseName}}.{{.TableName}}', '{replica}'){{end}}
PARTITION BY toDate(Start)
ORDER BY (ProjectId, TraceId, toUnixTimestamp(Start))
TTL toDateTime(Start) + toIntervalDay(30);
`

type TracesMVTarget struct{}

var _ migrations.RegisteredMigration = (*TracesMVTarget)(nil)

func (t *TracesMVTarget) Name() string {
	return "tracesMVTarget"
}

func (t *TracesMVTarget) Schema() string {
	return TracesMVTargetTmpl
}

func (t *TracesMVTarget) ValidateData(data interface{}) error {
	d, ok := data.(MigrationData)
	if !ok {
		return fmt.Errorf("passed interface not applicable")
	}
	if d.DatabaseName == "" || d.TableName == "" {
		return fmt.Errorf("any of database or table name cannot be empty")
	}
	return nil
}

func (t *TracesMVTarget) Render(data interface{}) (string, error) {
	return migrations.Render(t.Name(), t.Schema(), data)
}

func (t *TracesMVTarget) Setup(data interface{}) (*goose.Migration, error) {
	// check if this migration has already been setup/registered
	//nolint:dogsled
	_, filename, _, _ := runtime.Caller(0)

	// NOTE(prozlach): A hack meant to support old naming schema. This way we
	// can avoid duplicating whole file. Initially, both migration file had
	// different data IDs enven though they were meant for different
	// environments (cloud/self-hosted) and were 99.9% identical. Ideally we
	// should be re-use the code if possible and not worry about versions as
	// environments differ anyway.
	//nolint:errcheck
	d, _ := data.(MigrationData)
	if !d.SelfHostedVersion {
		filename = "20231004130953_traces_mv_target.go"
	}

	// validate passed data
	if err := t.ValidateData(data); err != nil {
		return nil, err
	}
	// render template
	query, err := migrations.Render(t.Name(), t.Schema(), data)
	if err != nil {
		return nil, fmt.Errorf("rendering registered template %s: %w", t.Name(), err)
	}
	//nolint:errcheck
	v, _ := goose.NumericComponent(filename)
	return goose.NewGoMigration(
		v,
		&goose.GoFunc{
			RunTx: func(ctx context.Context, tx *sql.Tx) error {
				_, err := tx.Exec(query)
				//nolint:wrapcheck
				return err
			},
		},
		nil,
	), nil
}
