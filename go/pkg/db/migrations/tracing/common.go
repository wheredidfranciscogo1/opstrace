package tracing

// MigrationData defines all data we need to render tracing-specific
// migrations.
type MigrationData struct {
	DatabaseName      string
	TableName         string
	DistTableName     string
	TargetTableName   string
	SourceTableName   string
	SelfHostedVersion bool
}
