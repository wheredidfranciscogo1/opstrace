package tracing

import (
	"context"
	"database/sql"
	"fmt"
	"runtime"

	"gitlab.com/gitlab-org/opstrace/goose/v3"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/db/migrations"
)

//nolint:lll
const TracesMVTmpl = `
CREATE MATERIALIZED VIEW IF NOT EXISTS {{.DatabaseName}}.{{.TableName}}{{if .SelfHostedVersion}} ON CLUSTER '{cluster}'{{end}}
TO {{.DatabaseName}}.{{.TargetTableName}} AS
SELECT
    ProjectId,
    TraceId,
    SpanId,
    min(Timestamp) AS Start,
    any(ServiceName) AS ServiceName,
    any(SpanName) AS SpanName,
    any(StatusCode) AS StatusCode,
    any(Duration) AS Duration,
    any(ParentSpanId) AS ParentSpanId
FROM {{.DatabaseName}}.{{.SourceTableName}}
WHERE TraceId != ''
GROUP BY
    ProjectId,
    TraceId,
    SpanId
ORDER BY Start;
`

type TracesMV struct{}

var _ migrations.RegisteredMigration = (*TracesMV)(nil)

func (t *TracesMV) Name() string {
	return "tracesMV"
}

func (t *TracesMV) Schema() string {
	return TracesMVTmpl
}

func (t *TracesMV) ValidateData(data interface{}) error {
	d, ok := data.(MigrationData)
	if !ok {
		return fmt.Errorf("passed interface not applicable")
	}
	if d.DatabaseName == "" || d.TableName == "" || d.TargetTableName == "" || d.SourceTableName == "" {
		return fmt.Errorf("any of database, table, source table or target table name cannot be empty")
	}
	return nil
}

func (t *TracesMV) Render(data interface{}) (string, error) {
	return migrations.Render(t.Name(), t.Schema(), data)
}

func (t *TracesMV) Setup(data interface{}) (*goose.Migration, error) {
	// check if this migration has already been setup/registered
	//nolint:dogsled
	_, filename, _, _ := runtime.Caller(0)

	// NOTE(prozlach): A hack meant to support old naming schema. This way we
	// can avoid duplicating whole file. Initially, both migration file had
	// different data IDs enven though they were meant for different
	// environments (cloud/self-hosted) and were 99.9% identical. Ideally we
	// should be re-use the code if possible and not worry about versions as
	// environments differ anyway.
	//nolint:errcheck
	d, _ := data.(MigrationData)
	if !d.SelfHostedVersion {
		filename = "20231004131042_traces_mv.go"
	}

	// validate passed data
	if err := t.ValidateData(data); err != nil {
		return nil, err
	}
	// render template
	query, err := migrations.Render(t.Name(), t.Schema(), data)
	if err != nil {
		return nil, fmt.Errorf("rendering registered template %s: %w", t.Name(), err)
	}
	//nolint:errcheck
	v, _ := goose.NumericComponent(filename)
	return goose.NewGoMigration(
		v,
		&goose.GoFunc{
			RunTx: func(ctx context.Context, tx *sql.Tx) error {
				_, err := tx.Exec(query)
				//nolint:wrapcheck
				return err
			},
		},
		nil,
	), nil
}
