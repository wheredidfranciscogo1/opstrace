package tracing

import (
	"context"
	"database/sql"
	"fmt"
	"runtime"

	"gitlab.com/gitlab-org/opstrace/goose/v3"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/db/migrations"
)

const (
	TracesMVTargetDistributedTmpl = `
CREATE TABLE IF NOT EXISTS {{.DatabaseName}}.{{.DistTableName}}
ON CLUSTER '{cluster}' AS {{.DatabaseName}}.{{.TableName}}
ENGINE = Distributed('{cluster}', {{.DatabaseName}}, {{.TableName}}, cityHash64(ProjectId, TraceId));
`
)

type TracesMVTargetDist struct{}

var _ migrations.RegisteredMigration = (*TracesMVTargetDist)(nil)

func (t *TracesMVTargetDist) Name() string {
	return "tracesMVTargetDist"
}

func (t *TracesMVTargetDist) Schema() string {
	return TracesMVTargetDistributedTmpl
}

func (t *TracesMVTargetDist) ValidateData(data interface{}) error {
	d, ok := data.(MigrationData)
	if !ok {
		return fmt.Errorf("passed interface not applicable")
	}
	if d.DatabaseName == "" || d.TableName == "" || d.DistTableName == "" {
		return fmt.Errorf("any of database or table name cannot be empty")
	}
	return nil
}

func (t *TracesMVTargetDist) Render(data interface{}) (string, error) {
	return migrations.Render(t.Name(), t.Schema(), data)
}

func (t *TracesMVTargetDist) Setup(data interface{}) (*goose.Migration, error) {
	// check if this migration has already been setup/registered
	//nolint:dogsled
	_, filename, _, _ := runtime.Caller(0)
	// validate passed data
	if err := t.ValidateData(data); err != nil {
		return nil, err
	}
	// render template
	query, err := migrations.Render(t.Name(), t.Schema(), data)
	if err != nil {
		return nil, fmt.Errorf("rendering registered template %s: %w", t.Name(), err)
	}
	//nolint:errcheck
	v, _ := goose.NumericComponent(filename)
	return goose.NewGoMigration(
		v,
		&goose.GoFunc{
			RunTx: func(ctx context.Context, tx *sql.Tx) error {
				_, err := tx.Exec(query)
				//nolint:wrapcheck
				return err
			},
		},
		nil,
	), nil
}
