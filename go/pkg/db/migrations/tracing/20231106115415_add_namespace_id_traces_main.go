package tracing

import (
	"context"
	"database/sql"
	"fmt"
	"runtime"

	"gitlab.com/gitlab-org/opstrace/goose/v3"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/db/migrations"
)

const (
	AddNamespaceIDTmpl = `
ALTER TABLE {{.DatabaseName}}.{{.TableName}}{{if .SelfHostedVersion}} ON CLUSTER '{cluster}'{{ end }}
ADD COLUMN IF NOT EXISTS NamespaceId Int64 CODEC(ZSTD(1)) AFTER ProjectId;
`
)

type AddNamespaceIDTracesMain struct{}

var _ migrations.RegisteredMigration = (*AddNamespaceIDTracesMain)(nil)

func (t *AddNamespaceIDTracesMain) Name() string {
	return "addNamespaceIDTracesMain"
}

func (t *AddNamespaceIDTracesMain) Schema() string {
	return AddNamespaceIDTmpl
}

func (t *AddNamespaceIDTracesMain) ValidateData(data interface{}) error {
	d, ok := data.(MigrationData)
	if !ok {
		return fmt.Errorf("passed interface not applicable")
	}
	if d.DatabaseName == "" || d.TableName == "" {
		return fmt.Errorf("any of database or table name cannot be empty")
	}
	return nil
}

func (t *AddNamespaceIDTracesMain) Render(data interface{}) (string, error) {
	return migrations.Render(t.Name(), t.Schema(), data)
}

func (t *AddNamespaceIDTracesMain) Setup(data interface{}) (*goose.Migration, error) {
	// check if this migration has already been setup/registered
	//nolint:dogsled
	_, filename, _, _ := runtime.Caller(0)

	// NOTE(prozlach): A hack meant to support old naming schema. This way we
	// can avoid duplicating whole file. Initially, both migration file had
	// different data IDs enven though they were meant for different
	// environments (cloud/self-hosted) and were 99.9% identical. Ideally we
	// should be re-use the code if possible and not worry about versions as
	// environments differ anyway.
	//nolint:errcheck
	d, _ := data.(MigrationData)
	if !d.SelfHostedVersion {
		filename = "20231106121910_add_namespace_id_traces_main.go"
	}

	// validate passed data
	if err := t.ValidateData(data); err != nil {
		return nil, err
	}
	// render template
	query, err := migrations.Render(t.Name(), t.Schema(), data)
	if err != nil {
		return nil, fmt.Errorf("rendering registered template %s: %w", t.Name(), err)
	}
	//nolint:errcheck
	v, _ := goose.NumericComponent(filename)
	return goose.NewGoMigration(
		v,
		&goose.GoFunc{
			RunTx: func(ctx context.Context, tx *sql.Tx) error {
				_, err := tx.Exec(query)
				//nolint:wrapcheck
				return err
			},
		},
		nil,
	), nil
}
