package common

import (
	"errors"
	"fmt"
	"strings"
)

var ErrUnknownProjectID = errors.New("unknown project ID")

// Helper struct to help construct a sql query.
type QueryBuilder struct {
	sql  string
	args []interface{}
	idx  int
}

// build takes the given sql string replaces any ? with the equivalent $<idx>
// and appends elems to the args slice.
func (qb *QueryBuilder) Build(stmt string, elems ...interface{}) {
	// add the query params to the args slice, if any
	qb.args = append(qb.args, elems...)
	q := stmt
	// replace ? with corresponding $<idx>
	for range elems {
		qb.idx += 1
		// placeholder that builds the string, for example, $1 when idx is 1
		p := fmt.Sprintf("$%d", qb.idx)
		// replace the first ? found in the string
		q = strings.Replace(q, "?", p, 1)
	}
	// add the sanitized query statement to the current sql query
	qb.sql += q
}

func (qb *QueryBuilder) SQL() string {
	return qb.sql
}

func (qb *QueryBuilder) Args() []interface{} {
	return qb.args
}
