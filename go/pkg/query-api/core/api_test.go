package core

import (
	"net/http"
	"net/http/httptest"

	"github.com/gin-gonic/gin"
	. "github.com/onsi/ginkgo/v2"
	. "github.com/onsi/gomega"

	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/query-api/logs"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/query-api/metrics"
)

var _ = Context("query API", func() {
	var router *gin.Engine

	BeforeEach(func() {
		router = gin.New()
	})

	Context("querying metrics", func() {
		DescribeTable(
			"get list of metrics via autocomplete",
			func(url string, statusCode int, responseJSON string) {
				qm := &metrics.MockQuerier{}
				ql := &logs.MockQuerier{}
				api := NewQueryAPI(logger.Desugar(), qm, ql)
				api.SetRoutes(router)

				recorder := httptest.NewRecorder()
				testReq, err := http.NewRequest(http.MethodGet, url, nil)
				Expect(err).NotTo(HaveOccurred())
				router.ServeHTTP(recorder, testReq)
				Expect(recorder.Code).To(Equal(statusCode))
				if responseJSON != "" {
					Expect(recorder).To(HaveHTTPBody(MatchJSON(responseJSON)))
				}
			},
			Entry("returns 200 with a list of metrics", "/v3/query/12345/metrics/autocomplete",
				http.StatusOK,
				`{
					"metrics": [
						{
							"name": "foo",
							"description": "description for foo",
							"attributes": [
								"foo",
								"bar"
							],
							"type": "Gauge",
							"last_ingested_at": 1701264345000000000
						},
						{
							"name": "bar",
							"description": "description for bar",
							"attributes": [
								"foo",
								"bar"
							],
							"type": "Gauge",
							"last_ingested_at": 1701264345000000000
						},
						{
							"name": "baz",
							"description": "description for baz",
							"attributes": [
								"foo",
								"bar"
							],
							"type": "Histogram",
							"last_ingested_at": 1701264345000000000
						}
					]
				}`,
			),
			Entry("returns 400 with invalid project id", "/v3/query/abc/metrics/autocomplete",
				http.StatusBadRequest,
				"",
			),
			Entry("returns 400 with invalid limit param", "/v3/query/abc/metrics/autocomplete?limit=foo",
				http.StatusBadRequest,
				"",
			),
		)

		DescribeTable(
			"query ingested metrics via search",
			func(url string, statusCode int, responseJSON string) {
				qm := &metrics.MockQuerier{}
				ql := &logs.MockQuerier{}
				api := NewQueryAPI(logger.Desugar(), qm, ql)
				api.SetRoutes(router)

				recorder := httptest.NewRecorder()
				testReq, err := http.NewRequest(http.MethodGet, url, nil)
				Expect(err).NotTo(HaveOccurred())
				router.ServeHTTP(recorder, testReq)
				Expect(recorder.Code).To(Equal(statusCode))
				if responseJSON != "" {
					Expect(recorder).To(HaveHTTPBody(MatchJSON(responseJSON)))
				}
			},
			Entry(
				"returns 200 with metrics data response",
				"/v3/query/12345/metrics/search?mname=cpu_seconds_total&mtype=gauge",
				http.StatusOK,
				`{
					"start_ts": 1701264345000000000,
					"end_ts": 1701264345000000000,
					"results": [
						{
							"name": "cpu_seconds_total",
							"description": "random description",
							"unit": "foo",
							"type": "Gauge",
							"attributes": {
								"baz": "blah",
								"foo": "bar"
							},
							"values": [
								[
									1701264345000000000,
									12345
								]
							]
						}
					]
				}`,
			),
			Entry(
				"returns 400 with invalid project id",
				"/v3/query/abc/metrics/search",
				http.StatusBadRequest,
				"",
			),
			Entry(
				"returns 400 with missing target metric",
				"/v3/query/12345/metrics/search",
				http.StatusBadRequest,
				"",
			),
			Entry(
				"returns 400 with missing target type",
				"/v3/query/12345/metrics/search?mname=cpu_seconds_total",
				http.StatusBadRequest,
				"",
			),
			Entry(
				"returns 400 with both predefined period and custom time intervals used",
				"/v3/query/12345/metrics/search?mname=cpu_seconds_total&mtype=gauge&period=1h&start_time=2023-11-23T11:24:10Z",
				http.StatusBadRequest,
				"",
			),
			Entry(
				"returns 400 with only start_time used",
				"/v3/query/12345/metrics/search?mname=cpu_seconds_total&mtype=gauge&start_time=2023-11-23T11:24:10Z",
				http.StatusBadRequest,
				"",
			),
			Entry(
				"returns 400 with only end_time used",
				"/v3/query/12345/metrics/search?mname=cpu_seconds_total&mtype=gauge&end_time=2023-11-23T11:24:10Z",
				http.StatusBadRequest,
				"",
			),
			Entry(
				"returns 400 with provided range params in non RFC3339 format",
				"/v3/query/12345/metrics/search?mname=cpu_seconds_total&mtype=gauge&start_time=2023-11-02&end_time=2023-11-20",
				http.StatusBadRequest,
				nil,
			),
			Entry(
				"returns 200 with metrics data response",
				"/v3/query/12345/metrics/search?mname=cpu_seconds_total&mtype=gauge&period=1h",
				http.StatusOK,
				`{
					"start_ts": 1701264345000000000,
					"end_ts": 1701264345000000000,
					"results": [
						{
							"name": "cpu_seconds_total",
							"description": "random description",
							"unit": "foo",
							"type": "Gauge",
							"attributes": {
								"baz": "blah",
								"foo": "bar"
							},
							"values": [
								[
									1701264345000000000,
									12345
								]
							]
						}
					]
				}`,
			),
			Entry(
				"returns 200 with metrics data response provided range params in RFC3339 format",
				"/v3/query/12345/metrics/search?mname=cpu_seconds_total&mtype=gauge&start_time=2023-11-23T11:24:10Z&end_time=2023-11-23T13:24:10Z",
				http.StatusOK,
				`{
					"start_ts": 1701264345000000000,
					"end_ts": 1701264345000000000,
					"results": [
						{
							"name": "cpu_seconds_total",
							"description": "random description",
							"unit": "foo",
							"type": "Gauge",
							"attributes": {
								"baz": "blah",
								"foo": "bar"
							},
							"values": [
								[
									1701264345000000000,
									12345
								]
							]
						}
					]
				}`,
			),
		)

		DescribeTable(
			"get search metadata for given metric",
			func(url string, statusCode int, responseJSON string) {
				qm := &metrics.MockQuerier{}
				ql := &logs.MockQuerier{}
				api := NewQueryAPI(logger.Desugar(), qm, ql)
				api.SetRoutes(router)

				recorder := httptest.NewRecorder()
				testReq, err := http.NewRequest(http.MethodGet, url, nil)
				Expect(err).NotTo(HaveOccurred())
				router.ServeHTTP(recorder, testReq)
				Expect(recorder.Code).To(Equal(statusCode))
				if responseJSON != "" {
					Expect(recorder).To(HaveHTTPBody(MatchJSON(responseJSON)))
				}
			},
			Entry("returns 200 with search metadata", "/v3/query/12345/metrics/searchmetadata?mname=cpu_seconds_total&mtype=gauge",
				http.StatusOK,
				`{
					"name": "cpu_seconds_total",
					"type": "Gauge",
					"description": "random description",
					"attribute_keys": [
					  "foo",
					  "bar"
					],
					"last_ingested_at": 1701264345000000000,
					"supported_aggregations": [
					  "1m",
					  "1h"
					],
					"supported_functions": [
					  "sum",
					  "count"
					],
					"default_group_by_attributes": [
					  "*"
					],
					"default_group_by_function": "sum"
				  }`,
			),
			Entry("returns 400 with missing target metric", "/v3/query/abc/metrics/searchmetadata?mtype=gauge",
				http.StatusBadRequest,
				"",
			),
			Entry("returns 400 with missing target type", "/v3/query/abc/metrics/searchmetadata?mname=cpu_seconds_total",
				http.StatusBadRequest,
				"",
			),
		)
	})
})
