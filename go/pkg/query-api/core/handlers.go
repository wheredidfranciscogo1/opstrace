package core

import (
	"errors"
	"fmt"
	"time"

	"github.com/gin-gonic/gin"
	"go.uber.org/zap"

	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/common"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/query-api/logs"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/query-api/metrics"
)

func (e *QueryAPI) MetricsAutoCompleteHandler(ctx *gin.Context) {
	filters := &metrics.MetricNameFilters{}
	// bind project ID
	if err := ctx.BindUri(filters); err != nil {
		e.logger.Error("autocomplete uri bind error", zap.Error(err))
		return
	}
	// bind other filters
	if err := ctx.BindQuery(filters); err != nil {
		e.logger.Error("autocomplete form bind error", zap.Error(err))
		return
	}
	e.logger.Debug("filters", zap.Any("filters", filters))
	results, err := e.metricsQuerier.GetMetricNames(ctx, filters)
	if err != nil {
		ctx.AbortWithError(500, err)
	}
	ctx.JSON(200, results)
}

type timeQueryParams struct {
	Period    string    `form:"period"`
	StartTime time.Time `form:"start_time"`
	EndTime   time.Time `form:"end_time"`
}

type metricsQueryParams struct {
	timeQueryParams
	MetricName string `form:"mname" binding:"required"`
	MetricType string `form:"mtype" binding:"required"`
}

type logsQueryParams struct {
	timeQueryParams
	// TODO(prozlach): add other query params here
}

type urlParams struct {
	ProjectID int64 `uri:"project_id" binding:"required,numeric,min=1"`
}

func (e *QueryAPI) MetricsSearchHandler(ctx *gin.Context) {
	// bind project ID
	up := new(urlParams)
	if err := ctx.BindUri(&up); err != nil {
		e.logger.Error("search uri bind error", zap.Error(err))
		return
	}
	// bind query params
	qp := new(metricsQueryParams)
	if err := ctx.BindQuery(qp); err != nil {
		e.logger.Error("search form bind error", zap.Error(err))
		return
	}
	queries := make(map[string]*metrics.Query)
	queries["alias"] = &metrics.Query{
		TargetMetric: qp.MetricName,
		TargetType:   qp.MetricType,
	}

	refTime := time.Now().UTC() // always query from now
	startTime, endTime, err := parseQueryTimes(refTime, qp.timeQueryParams)
	if err != nil {
		ctx.AbortWithError(400, err) // bad request
	}

	qc := &metrics.QueryContext{
		ProjectID:      up.ProjectID,
		StartTimestamp: startTime.UTC(),
		EndTimestamp:   endTime.UTC(),
		Queries:        queries,
	}
	e.logger.Debug("query context", zap.Any("parsed object", qc))
	results, err := e.metricsQuerier.GetMetrics(ctx, qc)
	if err != nil {
		ctx.AbortWithError(500, err)
	}
	ctx.JSON(200, results)
}

func (e *QueryAPI) MetricsSearchMetadataHandler(ctx *gin.Context) {
	// bind project ID
	up := new(urlParams)
	if err := ctx.BindUri(up); err != nil {
		e.logger.Error("searchmetadata uri bind error", zap.Error(err))
		return
	}
	// bind query params
	qp := new(metricsQueryParams)
	if err := ctx.BindQuery(qp); err != nil {
		e.logger.Error("searchmetadata query bind error", zap.Error(err))
		return
	}
	queries := make(map[string]*metrics.Query)
	queries["alias"] = &metrics.Query{
		TargetMetric: qp.MetricName,
		TargetType:   qp.MetricType,
	}
	qc := &metrics.QueryContext{
		ProjectID: up.ProjectID,
		Queries:   queries,
	}
	e.logger.Debug("search metadata query", zap.Any("parsed object", qc))
	results, err := e.metricsQuerier.GetSearchMetadata(ctx, qc)
	if err != nil {
		ctx.AbortWithError(500, err)
	}
	ctx.JSON(200, results)
}

func (e *QueryAPI) SearchLogsHandlerFactory(ctx *gin.Context) {
	// bind project ID
	up := new(urlParams)
	if err := ctx.BindUri(&up); err != nil {
		e.logger.Error("search uri bind error", zap.Error(err))
		return
	}
	// bind query params
	qp := new(logsQueryParams)
	if err := ctx.BindQuery(qp); err != nil {
		e.logger.Error("search form bind error", zap.Error(err))
		return
	}

	refTime := time.Now().UTC() // always query from now
	startTime, endTime, err := parseQueryTimes(refTime, qp.timeQueryParams)
	if err != nil {
		ctx.AbortWithError(400, err) // bad request
	}

	qc := &logs.QueryContext{
		ProjectID:      up.ProjectID,
		StartTimestamp: startTime.UTC(),
		EndTimestamp:   endTime.UTC(),

		Queries: map[string]*logs.Query{
			"main": {
				// TODO(prozlach): Add query params once we go beyond simple
				// quering by timestamp
			},
		},
	}
	e.logger.Debug("query context", zap.Any("parsed object", qc))
	results, err := e.logsQuerier.GetLogs(ctx, qc)
	if err != nil {
		ctx.AbortWithError(500, err)
	}
	ctx.JSON(200, results)
}

var validPeriods = map[string]struct{}{
	common.Period1m:  {},
	common.Period5m:  {},
	common.Period15m: {},
	common.Period30m: {},
	common.Period1h:  {},
	common.Period4h:  {},
	common.Period12h: {},
	common.Period24h: {},
	common.Period7d:  {},
	common.Period14d: {},
	common.Period30d: {},
}

const defaultTimePeriodHours int = 1 // 1h

var (
	errBothPeriodAndTimeProvided     = errors.New("both period and time range are provided")
	errBothTimeBoundariesNotProvided = errors.New("both ends for time range are not provided")
)

func parseQueryTimes(refTime time.Time, p timeQueryParams) (time.Time, time.Time, error) {
	var (
		startTime, endTime time.Time
	)
	// either a period or a time range pair should be provided but not both.
	if p.Period != "" && (!p.StartTime.IsZero() || !p.EndTime.IsZero()) {
		return startTime, endTime, errBothPeriodAndTimeProvided
	}
	if (!p.StartTime.IsZero() && p.EndTime.IsZero()) || (!p.EndTime.IsZero() && p.StartTime.IsZero()) {
		return startTime, endTime, errBothTimeBoundariesNotProvided
	}
	// when a predefined interval is provided, use that and compute
	// start time and end time accordingly
	if p.Period != "" {
		_, ok := validPeriods[p.Period]
		if !ok {
			return startTime, endTime, fmt.Errorf("invalid period: %s", p.Period)
		}
		startTimeUnix, _, _, _ := common.InferTimelines(refTime, p.Period)
		return time.Unix(startTimeUnix, 0), refTime, nil
	}
	// otherwise return the custom time range provided via params OR use default
	// timestamps when not provided via the query
	if p.StartTime.IsZero() {
		startTime = refTime.Add(-1 * time.Duration(defaultTimePeriodHours) * time.Hour)
	} else {
		startTime = p.StartTime
	}
	if p.EndTime.IsZero() {
		endTime = refTime
	} else {
		endTime = p.EndTime
	}
	return startTime, endTime, nil
}
