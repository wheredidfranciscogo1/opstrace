package core

import (
	"fmt"
	"time"

	. "github.com/onsi/ginkgo/v2"
	. "github.com/onsi/gomega"

	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/common"
)

var _ = Context("query API", func() {
	timeZero := time.Time{}
	refTime := time.Now().UTC()
	timeago1m := refTime.Add(-1 * time.Minute)
	timeago5m := refTime.Add(-1 * 5 * time.Minute)
	timeago15m := refTime.Add(-1 * 15 * time.Minute)
	timeago30m := refTime.Add(-1 * 30 * time.Minute)
	timeago1h := refTime.Add(-1 * time.Hour)
	timeago4h := refTime.Add(-1 * 4 * time.Hour)
	timeago12h := refTime.Add(-1 * 12 * time.Hour)
	timeago24h := refTime.Add(-1 * 24 * time.Hour)
	timeago7d := refTime.Add(-1 * 7 * 24 * time.Hour)
	timeago14d := refTime.Add(-1 * 14 * 24 * time.Hour)
	timeago30d := refTime.Add(-1 * 30 * 24 * time.Hour)

	Context("processing metrics query params", func() {
		DescribeTable(
			"parseQueryTimes",
			func(
				refTime time.Time,
				p timeQueryParams,
				expectedStartTime time.Time,
				expectedEndTime time.Time,
				expectedErr error,
			) {
				startTime, endTime, err := parseQueryTimes(refTime, p)
				if expectedErr != nil {
					Expect(err).To(HaveOccurred())
					Expect(err.Error()).To(Equal(expectedErr.Error()))
					return
				} else {
					Expect(err).ToNot(HaveOccurred())
				}
				if !expectedStartTime.IsZero() {
					Expect(expectedStartTime.UTC().Unix()).To(Equal(startTime.UTC().Unix()))
				}
				if !expectedEndTime.IsZero() {
					Expect(expectedEndTime.UTC().Unix()).To(Equal(endTime.UTC().Unix()))
				}
			},
			Entry("errors when both period and start_time or end_time are used",
				refTime,
				timeQueryParams{
					Period:    "1h",
					StartTime: refTime,
					EndTime:   refTime,
				},
				timeZero,
				timeZero,
				errBothPeriodAndTimeProvided,
			),
			Entry("errors when only start_time is used",
				refTime,
				timeQueryParams{StartTime: refTime},
				timeZero,
				timeZero,
				errBothTimeBoundariesNotProvided,
			),
			Entry("errors when only end_time is used",
				refTime,
				timeQueryParams{EndTime: refTime},
				timeZero,
				timeZero,
				errBothTimeBoundariesNotProvided,
			),
			Entry("errors when a random period is used",
				refTime,
				timeQueryParams{Period: "foo"},
				timeZero,
				timeZero,
				fmt.Errorf("invalid period: foo"),
			),
			Entry("parses period=1m correctly",
				refTime,
				timeQueryParams{Period: common.Period1m},
				timeago1m,
				refTime,
				nil,
			),
			Entry("parses period=5m correctly",
				refTime,
				timeQueryParams{Period: common.Period5m},
				timeago5m,
				refTime,
				nil,
			),
			Entry("parses period=15m correctly",
				refTime,
				timeQueryParams{Period: common.Period15m},
				timeago15m,
				refTime,
				nil,
			),
			Entry("parses period=30m correctly",
				refTime,
				timeQueryParams{Period: common.Period30m},
				timeago30m,
				refTime,
				nil,
			),
			Entry("parses period=1h correctly",
				refTime,
				timeQueryParams{Period: common.Period1h},
				timeago1h,
				refTime,
				nil,
			),
			Entry("parses period=4h correctly",
				refTime,
				timeQueryParams{Period: common.Period4h},
				timeago4h,
				refTime,
				nil,
			),
			Entry("parses period=12h correctly",
				refTime,
				timeQueryParams{Period: common.Period12h},
				timeago12h,
				refTime,
				nil,
			),
			Entry("parses period=24h correctly",
				refTime,
				timeQueryParams{Period: common.Period24h},
				timeago24h,
				refTime,
				nil,
			),
			Entry("parses period=7d correctly",
				refTime,
				timeQueryParams{Period: common.Period7d},
				timeago7d,
				refTime,
				nil,
			),
			Entry("parses period=14d correctly",
				refTime,
				timeQueryParams{Period: common.Period14d},
				timeago14d,
				refTime,
				nil,
			),
			Entry("parses period=30d correctly",
				refTime,
				timeQueryParams{Period: common.Period30d},
				timeago30d,
				refTime,
				nil,
			),
			Entry("parses custom time ranges correctly",
				refTime,
				timeQueryParams{
					StartTime: refTime.Add(-1 * 123 * time.Second),
					EndTime:   refTime,
				},
				refTime.Add(-1*123*time.Second),
				refTime,
				nil,
			),
			Entry("uses default settings when no time parameters are provided",
				refTime,
				timeQueryParams{},
				refTime.Add(-1*time.Duration(defaultTimePeriodHours)*time.Hour),
				refTime,
				nil,
			),
		)
	})
})
