package core

import (
	"net/http"

	"github.com/gin-gonic/gin"
	"go.uber.org/zap"

	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/query-api/logs"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/query-api/metrics"
)

type QueryAPI struct {
	logger         *zap.Logger
	metricsQuerier metrics.Querier
	logsQuerier    logs.Querier
}

func NewQueryAPI(
	l *zap.Logger,
	mq metrics.Querier,
	lq logs.Querier,
) *QueryAPI {
	return &QueryAPI{
		logger:         l,
		metricsQuerier: mq,
		logsQuerier:    lq,
	}
}

func (e *QueryAPI) SetRoutes(router *gin.Engine) {
	v3 := router.Group("/v3")
	{
		query := v3.Group("/query/:project_id")
		{
			metricsGroup := query.Group("/metrics")
			{
				metricsGroup.GET("/autocomplete", e.MetricsAutoCompleteHandler)
				metricsGroup.GET("/search", e.MetricsSearchHandler)
				metricsGroup.GET("/searchmetadata", e.MetricsSearchMetadataHandler)
			}

			logsGroup := query.Group("/logs")
			{
				logsGroup.GET("/search", e.SearchLogsHandlerFactory)
			}
		}
	}

	// `GET /readyz` is used by k8s for healtchecking the application.
	router.GET("/readyz", func(ginCtx *gin.Context) {
		ginCtx.String(http.StatusOK, "Success")
	})

	router.NoRoute(func(ginCtx *gin.Context) {
		ginCtx.AbortWithStatus(http.StatusNotFound)
	})
}
