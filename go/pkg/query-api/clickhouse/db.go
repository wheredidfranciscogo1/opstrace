package clickhouse

import (
	"context"
	"fmt"
	"time"

	"github.com/ClickHouse/clickhouse-go/v2"
	"go.uber.org/zap"
)

func GetDatabaseConnection(
	clickHouseDSN string,
	opts *clickhouse.Options,
	logger *zap.Logger,
	pingDuration time.Duration,
) (clickhouse.Conn, error) {
	dbOpts, err := clickhouse.ParseDSN(clickHouseDSN)
	if err != nil {
		return nil, fmt.Errorf("parsing clickhouse DSN: %w", err)
	}
	if opts != nil {
		dbOpts.MaxOpenConns = opts.MaxOpenConns
		dbOpts.MaxIdleConns = opts.MaxIdleConns
	}
	dbOpts.ConnMaxLifetime = 1 * time.Hour
	dbOpts.Compression = &clickhouse.Compression{
		Method: clickhouse.CompressionLZ4,
	}
	if logger.Level() == zap.DebugLevel {
		dbOpts.Debug = true
	}

	db, err := clickhouse.Open(dbOpts)
	if err != nil {
		return nil, fmt.Errorf("opening db: %w", err)
	}

	ctx, cancel := context.WithTimeout(context.Background(), pingDuration)
	defer cancel()

	if err := db.Ping(ctx); err != nil {
		return nil, fmt.Errorf("connecting to the database: %w", err)
	}
	return db, nil
}
