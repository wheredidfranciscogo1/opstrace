package metrics

import (
	"context"
	"fmt"
	"strings"
	"time"

	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/constants"
)

type MetricNameRow struct {
	ProjectID      string    `ch:"ProjectId"`
	Name           string    `ch:"MetricName"`
	Description    string    `ch:"MetricDescription"`
	Attributes     []string  `ch:"Attributes"`
	LastIngestedAt time.Time `ch:"LastIngestedAt"`
}

func (q *chQuerier) getSources(filters *MetricNameFilters) map[string]MetricNameGetFn {
	getters := make(map[string]MetricNameGetFn)
	// process all registered OTEL metric-types
	for _, t := range registeredOTELMetricTables {
		t := t // get loop variable
		getters[t.tableName] = func(ctx context.Context) ([]MetricName, error) {
			qb := buildQuery(t, filters)

			var rows []MetricNameRow
			if err := q.db.Select(ctx, &rows, qb.sql, qb.args...); err != nil {
				return nil, fmt.Errorf("executing metrics list query: %w", err)
			}

			response := make([]MetricName, 0)
			for _, r := range rows {
				response = append(response, MetricName{
					Name:           r.Name,
					Description:    r.Description,
					Attributes:     r.Attributes,
					Type:           t.typeName,
					LastIngestedAt: r.LastIngestedAt.UnixNano(),
				})
			}
			return response, nil
		}
	}
	return getters
}

const baseQueryTmpl = `
SELECT
  ProjectId,
  MetricName,
  any(MetricDescription) AS MetricDescription,
  arraySort(groupUniqArrayArrayMerge(AttributeKeysState)) AS Attributes,
  max(LastIngestedAt) AS LastIngestedAt
FROM
  %s.%s
`

func buildQuery(t metricTable, f *MetricNameFilters) *queryBuilder {
	builder := &queryBuilder{}
	targetMVName := t.tableName + "_metadata"
	builder.build(fmt.Sprintf(baseQueryTmpl, constants.MetricsDatabaseName, targetMVName))
	// process filters if any
	if f != nil {
		var filters []string
		if f.ProjectID != 0 {
			filters = append(filters, fmt.Sprintf("ProjectId = '%d'", f.ProjectID))
		}
		if f.StartsWith != "" {
			filters = append(filters, fmt.Sprintf("MetricName LIKE '%s%%'", f.StartsWith))
		}
		if len(filters) > 0 {
			builder.build(fmt.Sprintf(" WHERE %s", strings.Join(filters, clauseJoinerAND)))
		}
	}
	// process grouping
	builder.build(" GROUP BY ProjectId, MetricName")
	// process order
	builder.build(" ORDER BY ProjectId, MetricName")
	// process limit
	if f != nil {
		if f.Limit > 0 {
			builder.build(fmt.Sprintf(" LIMIT %d", f.Limit))
		}
	}
	return builder
}
