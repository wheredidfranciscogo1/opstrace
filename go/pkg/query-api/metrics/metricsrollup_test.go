package metrics

import (
	"context"
	"fmt"
	"time"

	"github.com/ClickHouse/clickhouse-go/v2/lib/driver"
	. "github.com/onsi/ginkgo/v2"
	. "github.com/onsi/gomega"
	"go.opentelemetry.io/collector/pdata/pmetric"

	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/constants"
	metricsmigrations "gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/db/migrations/metrics"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/testutils"
)

type gaugeRollupRow struct {
	ProjectID         string    `ch:"ProjectId"`
	MetricName        string    `ch:"MetricName"`
	MetricDescription string    `ch:"MetricDescription"`
	LastIngestedAt    time.Time `ch:"LastIngestedAt"`
	AvgValue          float64   `ch:"AvgValue"`
	LastValue         float64   `ch:"LastValue"`
}

type sumRollupRow struct {
	ProjectID         string    `ch:"ProjectId"`
	MetricName        string    `ch:"MetricName"`
	MetricDescription string    `ch:"MetricDescription"`
	LastIngestedAt    time.Time `ch:"LastIngestedAt"`
	SumValue          float64   `ch:"SumValue"`
}

const sumRollupQueryTmpl = `
SELECT
  ProjectId,
  MetricName,
  any(MetricDescription) AS MetricDescription,
  max(LastIngestedAt) AS LastIngestedAt,
  sumMerge(SumValueState) AS SumValue
FROM
  %s.%s
WHERE
  ProjectId = ?
  AND MetricName = ?
GROUP BY ProjectId, MetricName
`

const gaugeRollupQueryTmpl = `
SELECT
  ProjectId,
  MetricName,
  any(MetricDescription) AS MetricDescription,
  max(LastIngestedAt) AS LastIngestedAt,
  avgMerge(AvgValueState) AS AvgValue,
  anyLastMerge(LastValueState) AS LastValue
FROM
  %s.%s
WHERE
  ProjectId = ?
  AND MetricName = ?
GROUP BY ProjectId, MetricName
`

type setupFnRollup func(ctx context.Context, testEnv testutils.ClickHouseServer) (string, error)

var _ = Describe("metric rollup", func() {
	setupTestServer := func(ctx context.Context) (testutils.ClickHouseServer, driver.Conn) {
		testEnv, err := testutils.NewClickHouseServer(ctx)
		Expect(err).ToNot(HaveOccurred())
		// test table state/data
		conn, err := testEnv.GetConnection()
		Expect(err).ToNot(HaveOccurred())
		err = conn.Ping(ctx)
		Expect(err).ToNot(HaveOccurred())
		return testEnv, conn
	}

	Context("metrics rollup for gauges", func() {
		DescribeTable(
			"querying metrics via materialized view",
			func(mtype pmetric.MetricType, setupFn setupFnRollup) {
				// ensure the tests run within the stipulated timeout
				ctx, cancel := context.WithTimeout(context.Background(), 180*time.Second)
				defer cancel()
				testEnv, conn := setupTestServer(ctx)
				// ensure cleaning up the container afterwards
				defer testEnv.Container.Terminate(ctx)
				// setup test dependencies
				rollup, err := setupFn(ctx, testEnv)
				Expect(err).ToNot(HaveOccurred())
				// insert data into raw table, then query it back via the MV
				m := generateOTELMetrics(mtype, map[string]string{
					"keyone": "valone",
					"keytwo": "valtwo",
				}, 10)
				err = persistOTELMetrics(ctx, conn, logger.Desugar(), m)
				Expect(err).ToNot(HaveOccurred())

				tableName, err := getTableForMetricType(mtype)
				Expect(err).ToNot(HaveOccurred())
				query := fmt.Sprintf(gaugeRollupQueryTmpl, constants.MetricsDatabaseName, tableName+"_"+rollup)

				var rows []gaugeRollupRow
				err = conn.Select(ctx, &rows, query, randomProjectID, randomMetricName)
				Expect(err).ToNot(HaveOccurred())
				{
					values := make([]float64, 0)
					for _, r := range rows {
						GinkgoWriter.Println("row: ", r)
						values = append(values, r.AvgValue)
					}
					Expect(values).To(HaveLen(1))
					expectedValues := []float64{10}
					Expect(values).To(Equal(expectedValues))
				}
				// Generate the same value and test average
				m = generateOTELMetrics(mtype, map[string]string{
					"keyone": "valone",
					"keytwo": "valtwo",
				}, 5)
				err = persistOTELMetrics(ctx, conn, logger.Desugar(), m)
				Expect(err).ToNot(HaveOccurred())
				m = generateOTELMetrics(mtype, map[string]string{
					"keyone": "valone",
					"keytwo": "valtwo",
				}, 10)
				err = persistOTELMetrics(ctx, conn, logger.Desugar(), m)
				Expect(err).ToNot(HaveOccurred())
				m = generateOTELMetrics(mtype, map[string]string{
					"keyone": "valone",
					"keytwo": "valtwo",
				}, 5)
				err = persistOTELMetrics(ctx, conn, logger.Desugar(), m)
				Expect(err).ToNot(HaveOccurred())
				rows = []gaugeRollupRow{}
				err = conn.Select(ctx, &rows, query, randomProjectID, randomMetricName)
				Expect(err).ToNot(HaveOccurred())
				{
					values := make([]float64, 0)
					lastValues := make([]float64, 0)
					for _, r := range rows {
						GinkgoWriter.Println("row: ", r)
						values = append(values, r.AvgValue)
						lastValues = append(lastValues, r.LastValue)
					}
					Expect(values).To(HaveLen(1))
					expectedValues := []float64{7.5}
					Expect(values).To(Equal(expectedValues))
					Expect(lastValues).To(HaveLen(1))
					expectedLast := []float64{5}
					Expect(lastValues).To(Equal(expectedLast))
				}
			},
			Entry("gauge rollup mv 1m",
				pmetric.MetricTypeGauge,
				setupOTELGauge1mMV,
			),
			Entry("gauge rollup mv 1h",
				pmetric.MetricTypeGauge,
				setupOTELGauge1hMV,
			),
			Entry("gauge rollup mv 1d",
				pmetric.MetricTypeGauge,
				setupOTELGauge1dMV,
			),
		)
	})

	Context("metrics rollup for sum", func() {
		DescribeTable(
			"querying metrics via materialized view",
			func(mtype pmetric.MetricType, setupFn setupFnRollup) {
				// ensure the tests run within the stipulated timeout
				ctx, cancel := context.WithTimeout(context.Background(), 180*time.Second)
				defer cancel()
				testEnv, conn := setupTestServer(ctx)
				// ensure cleaning up the container afterwards
				defer testEnv.Container.Terminate(ctx)
				// setup test dependencies
				rollup, err := setupFn(ctx, testEnv)
				Expect(err).ToNot(HaveOccurred())
				// insert data into raw table, then query it back via the MV
				m := generateOTELMetrics(mtype, map[string]string{
					"keyone": "valone",
					"keytwo": "valtwo",
				}, 10)
				err = persistOTELMetrics(ctx, conn, logger.Desugar(), m)
				Expect(err).ToNot(HaveOccurred())

				tableName, err := getTableForMetricType(mtype)
				Expect(err).ToNot(HaveOccurred())
				query := fmt.Sprintf(sumRollupQueryTmpl, constants.MetricsDatabaseName, tableName+"_"+rollup)

				var rows []sumRollupRow
				err = conn.Select(ctx, &rows, query, randomProjectID, randomMetricName)
				Expect(err).ToNot(HaveOccurred())
				{
					values := make([]float64, 0)
					for _, r := range rows {
						GinkgoWriter.Println("row: ", r)
						values = append(values, r.SumValue)
					}
					Expect(values).To(HaveLen(1))
					expectedValues := []float64{10}
					Expect(values).To(Equal(expectedValues))
				}
				// Generate the same value and test average
				m = generateOTELMetrics(mtype, map[string]string{
					"keyone": "valone",
					"keytwo": "valtwo",
				}, 5)
				err = persistOTELMetrics(ctx, conn, logger.Desugar(), m)
				Expect(err).ToNot(HaveOccurred())
				rows = []sumRollupRow{}
				err = conn.Select(ctx, &rows, query, randomProjectID, randomMetricName)
				Expect(err).ToNot(HaveOccurred())
				{
					values := make([]float64, 0)
					for _, r := range rows {
						GinkgoWriter.Println("row: ", r)
						values = append(values, r.SumValue)
					}
					Expect(values).To(HaveLen(1))
					expectedValues := []float64{15}
					Expect(values).To(Equal(expectedValues))
				}
			},
			Entry("sum rollup mv 1m",
				pmetric.MetricTypeSum,
				setupOTELSum1mMV,
			),
			Entry("sum rollup mv 1h",
				pmetric.MetricTypeSum,
				setupOTELSum1hMV,
			),
			Entry("sum rollup mv 1d",
				pmetric.MetricTypeSum,
				setupOTELSum1dMV,
			),
		)
	})
})

func setupOTELGauge1mMV(ctx context.Context, testEnv testutils.ClickHouseServer) (string, error) {
	// create necessary database/tables
	if err := testEnv.CreateDatabase(ctx, constants.MetricsDatabaseName); err != nil {
		return "", err
	}
	// source table
	schema, err := (&metricsmigrations.GaugeMain{}).Render(
		metricsmigrations.MigrationData{
			DatabaseName:      constants.MetricsDatabaseName,
			TableName:         constants.MetricsGaugeTableName,
			SelfHostedVersion: true,
		},
	)
	if err != nil {
		return "", err
	}
	if err := testEnv.Run(ctx, schema); err != nil {
		return "", err
	}
	// target table
	schema, err = (&metricsmigrations.GaugeRollup1mMVTarget{}).Render(
		metricsmigrations.MVMigrationData{
			DatabaseName:      constants.MetricsDatabaseName,
			TableName:         constants.MetricsGauge1mTableName,
			SelfHostedVersion: true,
		},
	)
	if err != nil {
		return "", err
	}
	if err := testEnv.Run(ctx, schema); err != nil {
		return "", err
	}
	// materialized view
	schema, err = (&metricsmigrations.GaugeRollup1mMV{}).Render(
		metricsmigrations.MVMigrationData{
			DatabaseName:      constants.MetricsDatabaseName,
			TableName:         constants.MetricsGauge1mMVName,
			SourceTableName:   constants.MetricsGaugeTableName,
			TargetTableName:   constants.MetricsGauge1mTableName,
			SelfHostedVersion: true,
		},
	)
	if err != nil {
		return "", err
	}
	if err := testEnv.Run(ctx, schema); err != nil {
		return "", err
	}
	return "1m", nil
}

func setupOTELGauge1hMV(ctx context.Context, testEnv testutils.ClickHouseServer) (string, error) {
	// create necessary database/tables
	if err := testEnv.CreateDatabase(ctx, constants.MetricsDatabaseName); err != nil {
		return "", err
	}
	// source table
	schema, err := (&metricsmigrations.GaugeMain{}).Render(
		metricsmigrations.MigrationData{
			DatabaseName:      constants.MetricsDatabaseName,
			TableName:         constants.MetricsGaugeTableName,
			SelfHostedVersion: true,
		},
	)
	if err != nil {
		return "", err
	}
	if err := testEnv.Run(ctx, schema); err != nil {
		return "", err
	}
	// target table
	schema, err = (&metricsmigrations.GaugeRollup1hMVTarget{}).Render(
		metricsmigrations.MVMigrationData{
			DatabaseName:      constants.MetricsDatabaseName,
			TableName:         constants.MetricsGauge1hTableName,
			SelfHostedVersion: true,
		},
	)
	if err != nil {
		return "", err
	}
	if err := testEnv.Run(ctx, schema); err != nil {
		return "", err
	}
	// materialized view
	schema, err = (&metricsmigrations.GaugeRollup1hMV{}).Render(
		metricsmigrations.MVMigrationData{
			DatabaseName:      constants.MetricsDatabaseName,
			TableName:         constants.MetricsGauge1hMVName,
			SourceTableName:   constants.MetricsGaugeTableName,
			TargetTableName:   constants.MetricsGauge1hTableName,
			SelfHostedVersion: true,
		},
	)
	if err != nil {
		return "", err
	}
	if err := testEnv.Run(ctx, schema); err != nil {
		return "", err
	}
	return "1h", nil
}

func setupOTELGauge1dMV(ctx context.Context, testEnv testutils.ClickHouseServer) (string, error) {
	// create necessary database/tables
	if err := testEnv.CreateDatabase(ctx, constants.MetricsDatabaseName); err != nil {
		return "", err
	}
	// source table
	schema, err := (&metricsmigrations.GaugeMain{}).Render(
		metricsmigrations.MigrationData{
			DatabaseName:      constants.MetricsDatabaseName,
			TableName:         constants.MetricsGaugeTableName,
			SelfHostedVersion: true,
		},
	)
	if err != nil {
		return "", err
	}
	if err := testEnv.Run(ctx, schema); err != nil {
		return "", err
	}
	// target table
	schema, err = (&metricsmigrations.GaugeRollup1dMVTarget{}).Render(
		metricsmigrations.MVMigrationData{
			DatabaseName:      constants.MetricsDatabaseName,
			TableName:         constants.MetricsGauge1dTableName,
			SelfHostedVersion: true,
		},
	)
	if err != nil {
		return "", err
	}
	if err := testEnv.Run(ctx, schema); err != nil {
		return "", err
	}
	// materialized view
	schema, err = (&metricsmigrations.GaugeRollup1dMV{}).Render(
		metricsmigrations.MVMigrationData{
			DatabaseName:      constants.MetricsDatabaseName,
			TableName:         constants.MetricsGauge1dMVName,
			SourceTableName:   constants.MetricsGaugeTableName,
			TargetTableName:   constants.MetricsGauge1dTableName,
			SelfHostedVersion: true,
		},
	)
	if err != nil {
		return "", err
	}
	if err := testEnv.Run(ctx, schema); err != nil {
		return "", err
	}
	return "1d", nil
}

func setupOTELSum1mMV(ctx context.Context, testEnv testutils.ClickHouseServer) (string, error) {
	// create necessary database/tables
	if err := testEnv.CreateDatabase(ctx, constants.MetricsDatabaseName); err != nil {
		return "", err
	}
	// source table
	schema, err := (&metricsmigrations.SumMain{}).Render(
		metricsmigrations.MigrationData{
			DatabaseName:      constants.MetricsDatabaseName,
			TableName:         constants.MetricsSumTableName,
			SelfHostedVersion: true,
		},
	)
	if err != nil {
		return "", err
	}
	if err := testEnv.Run(ctx, schema); err != nil {
		return "", err
	}
	// target table
	schema, err = (&metricsmigrations.SumRollup1mMVTarget{}).Render(
		metricsmigrations.MVMigrationData{
			DatabaseName:      constants.MetricsDatabaseName,
			TableName:         constants.MetricsSum1mTableName,
			SelfHostedVersion: true,
		},
	)
	if err != nil {
		return "", err
	}
	if err := testEnv.Run(ctx, schema); err != nil {
		return "", err
	}
	// materialized view
	schema, err = (&metricsmigrations.SumRollup1mMV{}).Render(
		metricsmigrations.MVMigrationData{
			DatabaseName:      constants.MetricsDatabaseName,
			TableName:         constants.MetricsSum1mMVName,
			SourceTableName:   constants.MetricsSumTableName,
			TargetTableName:   constants.MetricsSum1mTableName,
			SelfHostedVersion: true,
		},
	)
	if err != nil {
		return "", err
	}
	if err := testEnv.Run(ctx, schema); err != nil {
		return "", err
	}
	return "1m", nil
}

func setupOTELSum1hMV(ctx context.Context, testEnv testutils.ClickHouseServer) (string, error) {
	// create necessary database/tables
	if err := testEnv.CreateDatabase(ctx, constants.MetricsDatabaseName); err != nil {
		return "", err
	}
	// source table
	schema, err := (&metricsmigrations.SumMain{}).Render(
		metricsmigrations.MigrationData{
			DatabaseName:      constants.MetricsDatabaseName,
			TableName:         constants.MetricsSumTableName,
			SelfHostedVersion: true,
		},
	)
	if err != nil {
		return "", err
	}
	if err := testEnv.Run(ctx, schema); err != nil {
		return "", err
	}
	// target table
	schema, err = (&metricsmigrations.SumRollup1hMVTarget{}).Render(
		metricsmigrations.MVMigrationData{
			DatabaseName:      constants.MetricsDatabaseName,
			TableName:         constants.MetricsSum1hTableName,
			SelfHostedVersion: true,
		},
	)
	if err != nil {
		return "", err
	}
	if err := testEnv.Run(ctx, schema); err != nil {
		return "", err
	}
	// materialized view
	schema, err = (&metricsmigrations.SumRollup1hMV{}).Render(
		metricsmigrations.MVMigrationData{
			DatabaseName:      constants.MetricsDatabaseName,
			TableName:         constants.MetricsSum1hMVName,
			SourceTableName:   constants.MetricsSumTableName,
			TargetTableName:   constants.MetricsSum1hTableName,
			SelfHostedVersion: true,
		},
	)
	if err != nil {
		return "", err
	}
	if err := testEnv.Run(ctx, schema); err != nil {
		return "", err
	}
	return "1h", nil
}

func setupOTELSum1dMV(ctx context.Context, testEnv testutils.ClickHouseServer) (string, error) {
	// create necessary database/tables
	if err := testEnv.CreateDatabase(ctx, constants.MetricsDatabaseName); err != nil {
		return "", err
	}
	// source table
	schema, err := (&metricsmigrations.SumMain{}).Render(
		metricsmigrations.MigrationData{
			DatabaseName:      constants.MetricsDatabaseName,
			TableName:         constants.MetricsSumTableName,
			SelfHostedVersion: true,
		},
	)
	if err != nil {
		return "", err
	}
	if err := testEnv.Run(ctx, schema); err != nil {
		return "", err
	}
	// target table
	schema, err = (&metricsmigrations.SumRollup1dMVTarget{}).Render(
		metricsmigrations.MVMigrationData{
			DatabaseName:      constants.MetricsDatabaseName,
			TableName:         constants.MetricsSum1dTableName,
			SelfHostedVersion: true,
		},
	)
	if err != nil {
		return "", err
	}
	if err := testEnv.Run(ctx, schema); err != nil {
		return "", err
	}
	// materialized view
	schema, err = (&metricsmigrations.SumRollup1dMV{}).Render(
		metricsmigrations.MVMigrationData{
			DatabaseName:      constants.MetricsDatabaseName,
			TableName:         constants.MetricsSum1dMVName,
			SourceTableName:   constants.MetricsSumTableName,
			TargetTableName:   constants.MetricsSum1dTableName,
			SelfHostedVersion: true,
		},
	)
	if err != nil {
		return "", err
	}
	if err := testEnv.Run(ctx, schema); err != nil {
		return "", err
	}
	return "1d", nil
}
