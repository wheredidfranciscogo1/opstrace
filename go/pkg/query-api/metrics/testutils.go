package metrics

import (
	"context"
	"time"

	"github.com/ClickHouse/clickhouse-go/v2"
	"go.opentelemetry.io/collector/pdata/pcommon"
	"go.opentelemetry.io/collector/pdata/pmetric"
	conventions "go.opentelemetry.io/collector/semconv/v1.18.0"
	"go.uber.org/zap"

	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/otel/exporter/gitlabobservabilityexporter/common"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/otel/exporter/gitlabobservabilityexporter/metrics"
)

var (
	sampleTimestamp = time.Unix(1701264345, 0).UTC()
	randomTraceID   = [16]byte{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 2, 3, 4}
	randomSpanID    = [8]byte{0, 0, 0, 0, 1, 2, 3, 4}
)

const (
	randomProjectID   string = "12345"
	randomNamespaceID string = "123"
	randomServiceName string = "test-service"
	randomMetricName  string = "random-metric-name"
)

var corpus = []MetricName{
	{
		Name:           "foo",
		Description:    "description for foo",
		Attributes:     []string{"foo", "bar"},
		Type:           pmetric.MetricTypeGauge.String(),
		LastIngestedAt: sampleTimestamp.UnixNano(),
	},
	{
		Name:           "bar",
		Description:    "description for bar",
		Attributes:     []string{"foo", "bar"},
		Type:           pmetric.MetricTypeGauge.String(),
		LastIngestedAt: sampleTimestamp.UnixNano(),
	},
	{
		Name:           "baz",
		Description:    "description for baz",
		Attributes:     []string{"foo", "bar"},
		Type:           pmetric.MetricTypeHistogram.String(),
		LastIngestedAt: sampleTimestamp.UnixNano(),
	},
}

var sampleData = CounterMetricsResponse{
	StartTimestamp: sampleTimestamp.UnixNano(),
	EndTimestamp:   sampleTimestamp.UnixNano(),

	Results: []QueryData{
		{
			MetricName:        "cpu_seconds_total",
			MetricDescription: "random description",
			MetricUnit:        "foo",
			MetricType:        pmetric.MetricTypeGauge.String(),
			Attributes: map[string]string{
				"foo": "bar",
				"baz": "blah",
			},
			Values: []QueryPoint{
				{
					float64(sampleTimestamp.UnixNano()),
					float64(12345),
				},
			},
		},
	},
}

var searchMetadataResponse = MetricSearchMetadataResponse{
	MetricName:               "cpu_seconds_total",
	MetricType:               pmetric.MetricTypeGauge.String(),
	MetricDescription:        "random description",
	AttributeKeys:            []string{"foo", "bar"},
	LastIngestedAt:           sampleTimestamp.UnixNano(),
	SupportedFunctions:       []string{"sum", "count"},
	SupportedAggregations:    []string{"1m", "1h"},
	DefaultGroupByAttributes: []string{"*"},
	DefaultGroupByFunction:   "sum",
}

type MockQuerier struct{}

var _ Querier = (*MockQuerier)(nil)

func (*MockQuerier) GetMetricNames(_ context.Context, _ *MetricNameFilters) (*MetricNameResponse, error) {
	return &MetricNameResponse{Metrics: corpus}, nil
}

func (*MockQuerier) GetMetrics(_ context.Context, _ *QueryContext) (interface{}, error) {
	return &sampleData, nil
}

func (*MockQuerier) GetSearchMetadata(_ context.Context, _ *QueryContext) (interface{}, error) {
	return &searchMetadataResponse, nil
}

func generateOTELMetrics(
	mtype pmetric.MetricType,
	attributes map[string]string,
	value float64,
) pmetric.Metrics {
	//nolint:exhaustive
	switch mtype {
	case pmetric.MetricTypeSum:
		return generateOTELSumMetrics(attributes, value)
	case pmetric.MetricTypeGauge:
		return generateOTELGaugeMetrics(attributes, value)
	case pmetric.MetricTypeHistogram:
		return generateOTELHistogramMetrics(attributes)
	case pmetric.MetricTypeExponentialHistogram:
		return generateOTELExponentialHistogramMetrics(attributes)
	}
	return pmetric.Metrics{}
}

func generateOTELSumMetrics(attributes map[string]string, value float64) pmetric.Metrics {
	metrics := pmetric.NewMetrics()
	rm := metrics.ResourceMetrics().AppendEmpty()
	rm.Resource().Attributes().PutStr(conventions.AttributeServiceName, randomServiceName)
	rm.Resource().SetDroppedAttributesCount(10)
	rm.SetSchemaUrl("https://opentelemetry.io/schemas/1.4.0")

	sm := rm.ScopeMetrics().AppendEmpty()
	sm.Scope().SetName("io.opentelemetry.contrib.clickhouse")
	sm.Scope().SetVersion("1.0.0")
	sm.SetSchemaUrl("https://opentelemetry.io/schemas/1.4.0")
	sm.Scope().SetDroppedAttributesCount(10)
	sm.Scope().Attributes().PutStr("lib", "clickhouse")

	m := sm.Metrics().AppendEmpty()
	m.SetName(randomMetricName)
	m.SetUnit("count")
	m.SetDescription("Random description for a random sum metric")

	dp := m.SetEmptySum().DataPoints().AppendEmpty()
	dp.SetDoubleValue(value)
	dp.Attributes().PutStr(common.ProjectIDHeader, randomProjectID)
	for k, v := range attributes {
		dp.Attributes().PutStr(k, v)
	}
	dp.SetStartTimestamp(pcommon.NewTimestampFromTime(time.Now()))
	dp.SetTimestamp(pcommon.NewTimestampFromTime(time.Now()))

	exemplars := dp.Exemplars().AppendEmpty()
	exemplars.SetIntValue(10)
	exemplars.FilteredAttributes().PutStr("key", "value")
	exemplars.SetTraceID(randomTraceID)
	exemplars.SetSpanID(randomSpanID)

	m.Sum().SetAggregationTemporality(pmetric.AggregationTemporalityCumulative)
	return metrics
}

func generateOTELGaugeMetrics(attributes map[string]string, value float64) pmetric.Metrics {
	metrics := pmetric.NewMetrics()
	rm := metrics.ResourceMetrics().AppendEmpty()
	rm.Resource().Attributes().PutStr(conventions.AttributeServiceName, randomServiceName)
	rm.Resource().SetDroppedAttributesCount(10)
	rm.SetSchemaUrl("https://opentelemetry.io/schemas/1.4.0")

	sm := rm.ScopeMetrics().AppendEmpty()
	sm.Scope().SetName("io.opentelemetry.contrib.clickhouse")
	sm.Scope().SetVersion("1.0.0")
	sm.SetSchemaUrl("https://opentelemetry.io/schemas/1.4.0")
	sm.Scope().SetDroppedAttributesCount(10)
	sm.Scope().Attributes().PutStr("lib", "clickhouse")

	m := sm.Metrics().AppendEmpty()
	m.SetName(randomMetricName)
	m.SetUnit("count")
	m.SetDescription("Random description for a random gauge metric")

	dp := m.SetEmptyGauge().DataPoints().AppendEmpty()
	dp.SetDoubleValue(value)
	dp.Attributes().PutStr(common.ProjectIDHeader, randomProjectID)
	for k, v := range attributes {
		dp.Attributes().PutStr(k, v)
	}
	dp.SetStartTimestamp(pcommon.NewTimestampFromTime(time.Now()))
	dp.SetTimestamp(pcommon.NewTimestampFromTime(time.Now()))

	exemplars := dp.Exemplars().AppendEmpty()
	exemplars.SetIntValue(10)
	exemplars.FilteredAttributes().PutStr("key", "value")
	exemplars.SetTraceID(randomTraceID)
	exemplars.SetSpanID(randomSpanID)

	return metrics
}

func generateOTELHistogramMetrics(attributes map[string]string) pmetric.Metrics {
	metrics := pmetric.NewMetrics()
	rm := metrics.ResourceMetrics().AppendEmpty()
	rm.Resource().Attributes().PutStr(conventions.AttributeServiceName, randomServiceName)
	rm.Resource().SetDroppedAttributesCount(10)
	rm.SetSchemaUrl("https://opentelemetry.io/schemas/1.4.0")

	sm := rm.ScopeMetrics().AppendEmpty()
	sm.Scope().SetName("io.opentelemetry.contrib.clickhouse")
	sm.Scope().SetVersion("1.0.0")
	sm.SetSchemaUrl("https://opentelemetry.io/schemas/1.4.0")
	sm.Scope().SetDroppedAttributesCount(10)
	sm.Scope().Attributes().PutStr("lib", "clickhouse")

	m := sm.Metrics().AppendEmpty()
	m.SetName(randomMetricName)
	m.SetDescription("Random description for a random histogram metric")

	dp := m.SetEmptyHistogram().DataPoints().AppendEmpty()
	dp.SetCount(1)
	dp.SetSum(1)
	dp.ExplicitBounds().FromRaw([]float64{0, 0, 0, 0, 0})
	dp.BucketCounts().FromRaw([]uint64{0, 0, 0, 1, 0})
	dp.SetMin(0)
	dp.SetMax(1)
	dp.Attributes().PutStr(common.ProjectIDHeader, randomProjectID)
	for k, v := range attributes {
		dp.Attributes().PutStr(k, v)
	}
	dp.SetStartTimestamp(pcommon.NewTimestampFromTime(time.Now()))
	dp.SetTimestamp(pcommon.NewTimestampFromTime(time.Now()))

	exemplars := dp.Exemplars().AppendEmpty()
	exemplars.SetIntValue(10)
	exemplars.FilteredAttributes().PutStr("key", "value")
	exemplars.SetTraceID(randomTraceID)
	exemplars.SetSpanID(randomSpanID)

	m.Histogram().SetAggregationTemporality(pmetric.AggregationTemporalityCumulative)

	return metrics
}

func generateOTELExponentialHistogramMetrics(attributes map[string]string) pmetric.Metrics {
	metrics := pmetric.NewMetrics()
	rm := metrics.ResourceMetrics().AppendEmpty()
	rm.Resource().Attributes().PutStr(conventions.AttributeServiceName, randomServiceName)
	rm.Resource().SetDroppedAttributesCount(10)
	rm.SetSchemaUrl("https://opentelemetry.io/schemas/1.4.0")

	sm := rm.ScopeMetrics().AppendEmpty()
	sm.Scope().SetName("io.opentelemetry.contrib.clickhouse")
	sm.Scope().SetVersion("1.0.0")
	sm.SetSchemaUrl("https://opentelemetry.io/schemas/1.4.0")
	sm.Scope().SetDroppedAttributesCount(10)
	sm.Scope().Attributes().PutStr("lib", "clickhouse")

	m := sm.Metrics().AppendEmpty()
	m.SetName(randomMetricName)
	m.SetDescription("Random description for a random exponential histogram metric")

	dp := m.SetEmptyExponentialHistogram().DataPoints().AppendEmpty()
	dp.SetCount(1)
	dp.SetSum(1)
	dp.SetMin(0)
	dp.SetMax(1)
	dp.SetZeroCount(0)
	dp.Negative().SetOffset(1)
	dp.Negative().BucketCounts().FromRaw([]uint64{0, 0, 0, 1, 0})
	dp.Positive().SetOffset(1)
	dp.Positive().BucketCounts().FromRaw([]uint64{0, 0, 0, 1, 0})
	dp.Attributes().PutStr(common.ProjectIDHeader, randomProjectID)
	for k, v := range attributes {
		dp.Attributes().PutStr(k, v)
	}
	dp.SetStartTimestamp(pcommon.NewTimestampFromTime(time.Now()))
	dp.SetTimestamp(pcommon.NewTimestampFromTime(time.Now()))

	exemplars := dp.Exemplars().AppendEmpty()
	exemplars.SetIntValue(10)
	exemplars.FilteredAttributes().PutStr("key", "value")
	exemplars.SetTraceID(randomTraceID)
	exemplars.SetSpanID(randomSpanID)

	m.ExponentialHistogram().SetAggregationTemporality(pmetric.AggregationTemporalityCumulative)

	return metrics
}

func persistOTELMetrics(
	ctx context.Context,
	db clickhouse.Conn,
	logger *zap.Logger,
	md pmetric.Metrics,
) error {
	ingester := metrics.NewClickHouseIngester(db, logger)
	for i := 0; i < md.ResourceMetrics().Len(); i++ {
		metrics := md.ResourceMetrics().At(i)
		resAttr := common.AttributesToMap(metrics.Resource().Attributes())
		for j := 0; j < metrics.ScopeMetrics().Len(); j++ {
			rs := metrics.ScopeMetrics().At(j).Metrics()
			scopeInstr := metrics.ScopeMetrics().At(j).Scope()
			scopeURL := metrics.ScopeMetrics().At(j).SchemaUrl()
			for k := 0; k < rs.Len(); k++ {
				ingester.Add(resAttr, metrics.SchemaUrl(), scopeInstr, scopeURL, rs.At(k))
			}
		}
	}
	return ingester.Write(ctx)
}
