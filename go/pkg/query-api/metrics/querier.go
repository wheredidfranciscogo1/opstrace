package metrics

import (
	"context"
	"fmt"
	"sort"
	"sync"
	"time"

	"github.com/ClickHouse/clickhouse-go/v2"
	"go.opentelemetry.io/collector/pdata/pmetric"
	"go.uber.org/zap"

	chinternal "gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/query-api/clickhouse"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/query-api/common"
)

type chQuerier struct {
	db     clickhouse.Conn
	logger *zap.Logger
}

// ascertain interface implementation
var _ Querier = (*chQuerier)(nil)

func NewQuerier(
	clickHouseDSN string,
	clickHouseCloudDSN string,
	opts *clickhouse.Options,
	logger *zap.Logger,
) (Querier, error) {
	var (
		db  clickhouse.Conn
		err error
	)
	if clickHouseCloudDSN != "" {
		db, err = chinternal.GetDatabaseConnection(clickHouseCloudDSN, opts, logger, 30*time.Second)
		if err != nil {
			return nil, fmt.Errorf("getting cloud database handle: %w", err)
		}
	} else {
		db, err = chinternal.GetDatabaseConnection(clickHouseDSN, opts, logger, 3*time.Second)
		if err != nil {
			return nil, fmt.Errorf("getting database handle: %w", err)
		}
	}

	return &chQuerier{
		db:     db,
		logger: logger,
	}, nil
}

// GetMetricNames returns a MetricNameResponse which contains a list of all
// distinct metric-names ingested into the metrics subsystem.
//
// Within our current design/implementation, we query data from multiple metric
// schemas in a concurrent manner to optimize for query-latency. `chQuerier.getSources`
// internally returns a map of all registered table-names to their corresponding
// fetcher methods, which are then invoked inside a separate goroutine per table-name
// concurrently. Once all queries are complete, the results are clubbed together and
// filtered as needed, before returning within the response.
func (q *chQuerier) GetMetricNames(ctx context.Context, filters *MetricNameFilters) (*MetricNameResponse, error) {
	metrics := make([]MetricName, 0) // ensure an empty response when applicable
	var metricsGuard sync.Mutex      // guards metrics above
	// query all registered metrics sources
	var wg sync.WaitGroup
	for k, v := range q.getSources(filters) {
		q.logger.Debug("querying name getter", zap.String("tablename", k))
		v := v // construct loop variable
		wg.Add(1)
		go func() {
			defer wg.Done()

			queryCtx, cancel := context.WithTimeout(ctx, 5*time.Second)
			defer cancel()

			m, err := v(queryCtx)
			if err != nil {
				q.logger.Error("querying metric table for distinct names", zap.Error(err))
				return
			}

			metricsGuard.Lock()
			defer metricsGuard.Unlock()
			metrics = append(metrics, m...)
		}()
	}
	// wait for all metrics sources to have been queried
	wg.Wait()
	// sort results since they collect metrics of multiple types/sources
	sort.Sort(ByMetricName(metrics))
	// limit the response as was requested
	if filters.Limit > 0 && filters.Limit <= len(metrics) {
		metrics = metrics[0:filters.Limit]
	}
	return &MetricNameResponse{Metrics: metrics}, nil
}

// GetMetrics returns a response specific to the type of metric being queried,
// which contains data ingested into the metrics subsystem that matches a given
// search criteria, which is internally modeled via a QueryContext object.
func (q *chQuerier) GetMetrics(ctx context.Context, queryContext *QueryContext) (interface{}, error) {
	if queryContext == nil {
		return nil, nil // nothing to do
	}

	if queryContext.ProjectID == 0 {
		return nil, common.ErrUnknownProjectID
	}

	if len(queryContext.Queries) > 1 {
		panic("multiple queries with expressions are not supported yet")
	}

	for alias, query := range queryContext.Queries {
		q.logger.Debug(
			"processing query",
			zap.String("query alias", alias),
			zap.Any("query", query),
		)

		mtype := getMetricTypeFromString(query.TargetType)
		if mtype == pmetric.MetricTypeEmpty {
			return nil, errUnknownMetricType
		}

		mHandler := getMetricHandlerForMetricType(mtype)
		if mHandler == nil {
			return nil, fmt.Errorf("couldn't get a metric handler for type: %s", mtype.String())
		}
		if err := mHandler.setup(q.db, q.logger); err != nil {
			return nil, fmt.Errorf("setting up metric handler for type: %s: %w", mtype.String(), err)
		}

		response, err := mHandler.executeQuery(
			ctx,
			queryContext.ProjectID,
			queryContext.StartTimestamp,
			queryContext.EndTimestamp,
			query,
		)
		if err != nil {
			return nil, fmt.Errorf("executing query for type: %s: %w", mtype.String(), err)
		}

		return response, nil
	}

	return nil, nil
}

// GetSearchMetadata returns search metadata specific to the query being performed which is
// used by components of the query builder to populate attributes, supported aggregations etc.
func (q *chQuerier) GetSearchMetadata(ctx context.Context, queryContext *QueryContext) (interface{}, error) {
	if queryContext == nil {
		return nil, nil // nothing to do
	}

	if queryContext.ProjectID == 0 {
		return nil, common.ErrUnknownProjectID
	}

	if len(queryContext.Queries) > 1 {
		panic("multiple queries via expressions are not supported yet")
	}

	for alias, query := range queryContext.Queries {
		q.logger.Debug(
			"processing search metadata for query",
			zap.String("query alias", alias),
			zap.Any("query", query),
		)

		mtype := getMetricTypeFromString(query.TargetType)
		if mtype == pmetric.MetricTypeEmpty {
			return nil, errUnknownMetricType
		}

		mHandler := getMetricHandlerForMetricType(mtype)
		if mHandler == nil {
			return nil, fmt.Errorf("couldn't get a metric handler for type: %s", mtype.String())
		}
		if err := mHandler.setup(q.db, q.logger); err != nil {
			return nil, fmt.Errorf("setting up metric handler for type: %s: %w", mtype.String(), err)
		}

		response, err := mHandler.getSearchMetadata(
			ctx,
			queryContext.ProjectID,
			query,
		)
		if err != nil {
			return nil, fmt.Errorf("executing search metadata query for type: %s: %w", mtype.String(), err)
		}
		return response, nil
	}

	return nil, nil
}
