package metrics

import (
	"context"
	"fmt"
	"time"

	"github.com/ClickHouse/clickhouse-go/v2"
	"go.opentelemetry.io/collector/pdata/pmetric"
	"go.uber.org/zap"

	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/constants"
)

type histogramHandler struct {
	db     clickhouse.Conn
	logger *zap.Logger
}

var _ metricHandler = (*histogramHandler)(nil)

func (h *histogramHandler) setup(db clickhouse.Conn, logger *zap.Logger) error {
	h.db = db
	h.logger = logger
	return nil
}

const baseHistogramSearchTmpl string = `
SELECT
  any(MetricName) AS MetricName,
  any(MetricDescription) AS MetricDescription,
  any(MetricUnit) AS MetricUnit,
  Fingerprint,
  any(Attributes) AS Attributes,
  groupArray(Avg) AS Values,
  groupArray(TimeUnix) AS Times
FROM
(
  SELECT
    MetricName,
    MetricDescription,
    MetricUnit,
    Fingerprint,
    Attributes,
    (Sum/Count) AS Avg,
    TimeUnix
  FROM %s.%s
  WHERE
    ProjectId = ?
    AND MetricName = ?
    AND TimeUnix >= ?
    AND TimeUnix < ?
  ORDER BY MetricName, Fingerprint, TimeUnix
)
GROUP BY Fingerprint
`

func (h *histogramHandler) buildQuery(
	ctx context.Context,
	projectID int64,
	startTimestamp time.Time,
	endTimestamp time.Time,
	query *Query,
) (*queryBuilder, error) {
	mtype := getMetricTypeFromString(query.TargetType)
	if mtype == pmetric.MetricTypeEmpty {
		return nil, errUnknownMetricType
	}

	tableName, err := getTableForMetricType(mtype)
	if err != nil {
		return nil, fmt.Errorf("getting table for metric type %s: %w", query.TargetType, err)
	}

	queryStr := fmt.Sprintf(baseHistogramSearchTmpl, constants.MetricsDatabaseName, tableName)
	builder := &queryBuilder{}
	builder.build(
		queryStr,
		fmt.Sprintf("%d", projectID),
		query.TargetMetric,
		startTimestamp,
		endTimestamp,
	)
	return builder, nil
}

func (h *histogramHandler) executeQuery(
	ctx context.Context,
	projectID int64,
	startTimestamp time.Time,
	endTimestamp time.Time,
	query *Query,
) (interface{}, error) {
	qb, err := h.buildQuery(ctx, projectID, startTimestamp, endTimestamp, query)
	if err != nil {
		return nil, fmt.Errorf("building metrics query: %w", err)
	}

	h.logger.Debug(
		"executing query",
		zap.Any("query string", qb.sql),
		zap.Any("query args", qb.args),
	)

	var rows []MetricRow
	if err := h.db.Select(ctx, &rows, qb.sql, qb.args...); err != nil {
		return nil, fmt.Errorf("executing metrics query: %w", err)
	}

	m := CounterMetricsResponse{
		StartTimestamp: startTimestamp.UnixNano(),
		EndTimestamp:   endTimestamp.UnixNano(),
		Results:        make([]QueryData, 0),
	}
	for _, r := range rows {
		qd := QueryData{
			MetricName:        r.MetricName,
			MetricDescription: r.MetricDescription,
			MetricUnit:        r.MetricUnit,
			MetricType:        query.TargetType,
			Attributes:        r.Attributes,
			Values:            make([]QueryPoint, 0),
		}
		for idx := 0; idx < len(r.Times); idx++ {
			qd.Values = append(qd.Values, QueryPoint{
				float64(r.Times[idx].UnixNano()), r.Values[idx],
			})
		}
		m.Results = append(m.Results, qd)
	}
	return m, nil
}

func (h *histogramHandler) getSearchMetadata(ctx context.Context, projectID int64, query *Query) (interface{}, error) {
	qb, err := buildSearchMetadataQuery(projectID, query)
	if err != nil {
		return nil, fmt.Errorf("building metrics query: %w", err)
	}

	h.logger.Debug(
		"executing search metadata query",
		zap.Any("query string", qb.sql),
		zap.Any("query args", qb.args),
	)

	var rows []MetricSearchMetadataRow
	if err := h.db.Select(ctx, &rows, qb.sql, qb.args...); err != nil {
		return nil, fmt.Errorf("executing metrics search metadata query: %w", err)
	}

	response := MetricSearchMetadataResponse{}
	for _, r := range rows {
		response.MetricName = r.MetricName
		response.MetricType = query.TargetType
		response.MetricDescription = r.MetricDescription
		response.AttributeKeys = r.AttributeKeys
		response.LastIngestedAt = r.LastIngestedAt.UnixNano()
	}
	response.SupportedAggregations = SupportedTimeAggregations
	response.SupportedFunctions = []string{
		string(AggregateFunctionP50),
		string(AggregateFunctionP75),
		string(AggregateFunctionP90),
		string(AggregateFunctionP95),
		string(AggregateFunctionP99),
		string(AggregateFunctionMin),
		string(AggregateFunctionMax),
	}
	response.DefaultGroupByAttributes = []string{GroupByAttributesBucket}
	response.DefaultGroupByFunction = string(AggregateFunctionCount)
	return response, nil
}
