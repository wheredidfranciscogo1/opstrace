package metrics

import (
	. "github.com/onsi/ginkgo/v2"
	. "github.com/onsi/gomega"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/testutils"
)

var _ = Context("metrics", func() {
	DescribeTable(
		"building metric name source queries",
		func(filters *MetricNameFilters, expectedQuery string) {
			m := metricTable{
				tableName: "metrics_table",
				typeName:  "metrics_type",
			}
			q := buildQuery(m, filters)
			err := testutils.CompareSQLStrings(q.sql, expectedQuery)
			Expect(err).ToNot(HaveOccurred())
		},
		Entry(
			"list all metrics",
			nil,
			`
SELECT
  ProjectId,
  MetricName,
  any(MetricDescription) AS MetricDescription,
  arraySort(groupUniqArrayArrayMerge(AttributeKeysState)) AS Attributes,
  max(LastIngestedAt) AS LastIngestedAt
FROM
  metrics.metrics_table_metadata
GROUP BY ProjectId, MetricName ORDER BY ProjectId, MetricName`,
		),
		Entry(
			"list metrics with starts_with filter",
			&MetricNameFilters{ProjectID: 12345, StartsWith: "foo"},
			`
SELECT
  ProjectId,
  MetricName,
  any(MetricDescription) AS MetricDescription,
  arraySort(groupUniqArrayArrayMerge(AttributeKeysState)) AS Attributes,
  max(LastIngestedAt) AS LastIngestedAt
FROM
  metrics.metrics_table_metadata
WHERE ProjectId = '12345' AND MetricName LIKE 'foo%' GROUP BY ProjectId, MetricName ORDER BY ProjectId, MetricName`,
		),
		Entry(
			"list metrics with limit filter",
			&MetricNameFilters{ProjectID: 12345, Limit: 10},
			`
SELECT
  ProjectId,
  MetricName,
  any(MetricDescription) AS MetricDescription,
  arraySort(groupUniqArrayArrayMerge(AttributeKeysState)) AS Attributes,
  max(LastIngestedAt) AS LastIngestedAt
FROM
  metrics.metrics_table_metadata
WHERE ProjectId = '12345' GROUP BY ProjectId, MetricName ORDER BY ProjectId, MetricName LIMIT 10`,
		),
		Entry(
			"list metrics with multiple filters",
			&MetricNameFilters{ProjectID: 12345, StartsWith: "foo", Limit: 10},
			`
SELECT
  ProjectId,
  MetricName,
  any(MetricDescription) AS MetricDescription,
  arraySort(groupUniqArrayArrayMerge(AttributeKeysState)) AS Attributes,
  max(LastIngestedAt) AS LastIngestedAt
FROM
  metrics.metrics_table_metadata
WHERE ProjectId = '12345' AND MetricName LIKE 'foo%' GROUP BY ProjectId, MetricName ORDER BY ProjectId, MetricName LIMIT 10`,
		),
	)
})
