package metrics

import (
	"context"
	"fmt"
	"time"

	"github.com/ClickHouse/clickhouse-go/v2"
	"go.opentelemetry.io/collector/pdata/pmetric"
	"go.uber.org/zap"

	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/constants"
)

type counterHandler struct {
	db     clickhouse.Conn
	logger *zap.Logger
}

var _ metricHandler = (*counterHandler)(nil)

func (g *counterHandler) setup(db clickhouse.Conn, logger *zap.Logger) error {
	g.db = db
	g.logger = logger
	return nil
}

const baseMetricSearchTmpl string = `
SELECT
  any(MetricName) AS MetricName,
  any(MetricDescription) AS MetricDescription,
  any(MetricUnit) AS MetricUnit,
  Fingerprint,
  any(Attributes) AS Attributes,
  groupArray(Value) AS Values,
  groupArray(TimeUnix) AS Times
FROM
(
  SELECT
    MetricName,
    MetricDescription,
    MetricUnit,
    Fingerprint,
    Attributes,
    Value,
    TimeUnix
  FROM %s.%s
  WHERE
    ProjectId = ?
    AND MetricName = ?
    AND TimeUnix >= ?
    AND TimeUnix < ?
  ORDER BY MetricName, Fingerprint, TimeUnix
)
GROUP BY Fingerprint
`

func (g *counterHandler) buildQuery(
	ctx context.Context,
	projectID int64,
	startTimestamp time.Time,
	endTimestamp time.Time,
	query *Query,
) (*queryBuilder, error) {
	mtype := getMetricTypeFromString(query.TargetType)
	if mtype == pmetric.MetricTypeEmpty {
		return nil, errUnknownMetricType
	}

	tableName, err := getTableForMetricType(mtype)
	if err != nil {
		return nil, fmt.Errorf("getting table for metric type %s: %w", query.TargetType, err)
	}

	builder := &queryBuilder{}
	queryStr := fmt.Sprintf(baseMetricSearchTmpl, constants.MetricsDatabaseName, tableName)
	builder.build(
		queryStr,
		fmt.Sprintf("%d", projectID),
		query.TargetMetric,
		startTimestamp,
		endTimestamp,
	)
	return builder, nil
}

type CounterMetricsResponse struct {
	StartTimestamp int64       `json:"start_ts"`
	EndTimestamp   int64       `json:"end_ts"`
	Results        []QueryData `json:"results"`
}

type QueryData struct {
	MetricName        string            `json:"name"`
	MetricDescription string            `json:"description"`
	MetricUnit        string            `json:"unit"`
	MetricType        string            `json:"type"`
	Attributes        map[string]string `json:"attributes"`
	Values            []QueryPoint      `json:"values,omitempty"`
}

type MetricRow struct {
	MetricName        string            `ch:"MetricName"`
	MetricDescription string            `ch:"MetricDescription"`
	MetricUnit        string            `ch:"MetricUnit"`
	Fingerprint       string            `ch:"Fingerprint"`
	Attributes        map[string]string `ch:"Attributes"`
	Values            []float64         `ch:"Values"`
	Times             []time.Time       `ch:"Times"`
}

func (g *counterHandler) executeQuery(
	ctx context.Context,
	projectID int64,
	startTimestamp time.Time,
	endTimestamp time.Time,
	query *Query,
) (interface{}, error) {
	if err := validateMetricQuery(projectID, startTimestamp, endTimestamp, query); err != nil {
		return nil, fmt.Errorf("validating query: %w", err)
	}

	qb, err := g.buildQuery(ctx, projectID, startTimestamp, endTimestamp, query)
	if err != nil {
		return nil, fmt.Errorf("building metrics query: %w", err)
	}

	g.logger.Debug(
		"executing query",
		zap.Any("query string", qb.sql),
		zap.Any("query args", qb.args),
	)

	var rows []MetricRow
	if err := g.db.Select(ctx, &rows, qb.sql, qb.args...); err != nil {
		return nil, fmt.Errorf("executing metrics query: %w", err)
	}

	m := CounterMetricsResponse{
		StartTimestamp: startTimestamp.UnixNano(),
		EndTimestamp:   endTimestamp.UnixNano(),
		Results:        make([]QueryData, 0),
	}
	for _, r := range rows {
		qd := QueryData{
			MetricName:        r.MetricName,
			MetricDescription: r.MetricDescription,
			MetricUnit:        r.MetricUnit,
			MetricType:        query.TargetType,
			Attributes:        r.Attributes,
			Values:            make([]QueryPoint, 0),
		}
		for idx := 0; idx < len(r.Times); idx++ {
			qd.Values = append(qd.Values, QueryPoint{
				float64(r.Times[idx].UnixNano()), r.Values[idx],
			})
		}
		m.Results = append(m.Results, qd)
	}
	return m, nil
}

func (g *counterHandler) getSearchMetadata(ctx context.Context, projectID int64, query *Query) (interface{}, error) {
	qb, err := buildSearchMetadataQuery(projectID, query)
	if err != nil {
		return nil, fmt.Errorf("building metrics query: %w", err)
	}

	g.logger.Debug(
		"executing search metadata query",
		zap.Any("query string", qb.sql),
		zap.Any("query args", qb.args),
	)

	var rows []MetricSearchMetadataRow
	if err := g.db.Select(ctx, &rows, qb.sql, qb.args...); err != nil {
		return nil, fmt.Errorf("executing metrics search metadata query: %w", err)
	}

	response := MetricSearchMetadataResponse{}
	for _, r := range rows {
		response.MetricName = r.MetricName
		response.MetricType = query.TargetType
		response.MetricDescription = r.MetricDescription
		response.AttributeKeys = r.AttributeKeys
		response.LastIngestedAt = r.LastIngestedAt.UnixNano()
	}
	response.SupportedAggregations = SupportedTimeAggregations
	response.SupportedFunctions = []string{
		string(AggregateFunctionAvg),
		string(AggregateFunctionSum),
		string(AggregateFunctionMin),
		string(AggregateFunctionMax),
		string(AggregateFunctionCount),
	}
	response.DefaultGroupByAttributes = []string{GroupByAttributesAll}
	response.DefaultGroupByFunction = string(AggregateFunctionSum)
	return response, nil
}
