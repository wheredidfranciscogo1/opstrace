package metrics

import (
	"context"
	"time"

	"github.com/ClickHouse/clickhouse-go/v2"
	"go.uber.org/zap"
)

type QueryContext struct {
	ProjectID int64 `json:"project_id"`

	Queries    map[string]*Query `json:"queries,omitempty"`
	Expression string            `json:"expression,omitempty"`

	StartTimestamp time.Time `json:"start_time"`
	EndTimestamp   time.Time `json:"end_time"`
}

type Query struct {
	TargetMetric string `json:"mname"`
	TargetType   string `json:"mtype"`
	TargetVisual string `json:"mvisual"`
}

type Querier interface {
	GetMetricNames(context.Context, *MetricNameFilters) (*MetricNameResponse, error)
	GetMetrics(context.Context, *QueryContext) (interface{}, error)
	GetSearchMetadata(context.Context, *QueryContext) (interface{}, error)
}

type metricHandler interface {
	setup(clickhouse.Conn, *zap.Logger) error
	buildQuery(context.Context, int64, time.Time, time.Time, *Query) (*queryBuilder, error)
	executeQuery(context.Context, int64, time.Time, time.Time, *Query) (interface{}, error)
	getSearchMetadata(context.Context, int64, *Query) (interface{}, error)
}

type MetricNameFilters struct {
	ProjectID  int64  `uri:"project_id" binding:"required,numeric,min=1"`
	StartsWith string `form:"starts_with"`
	Limit      int    `form:"limit" binding:"numeric"`
}

type MetricNameResponse struct {
	Metrics []MetricName `json:"metrics"`
}

type MetricName struct {
	Name           string   `json:"name"`
	Description    string   `json:"description"`
	Attributes     []string `json:"attributes"`
	Type           string   `json:"type"`
	LastIngestedAt int64    `json:"last_ingested_at"`
}

type ByMetricName []MetricName

func (s ByMetricName) Len() int           { return len(s) }
func (s ByMetricName) Swap(i, j int)      { s[i], s[j] = s[j], s[i] }
func (s ByMetricName) Less(i, j int) bool { return s[i].Name < s[j].Name }

type MetricNameGetFn func(context.Context) ([]MetricName, error)

type MetricsResponse struct {
	StartTimestamp int64 `json:"start_ts"`
	EndTimestamp   int64 `json:"end_ts"`

	Results []QueryData `json:"results"`
}

type QueryPoint [2]float64 // time, value
