package metrics

import (
	"errors"
	"fmt"
	"strings"
	"time"

	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/constants"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/query-api/common"
	"go.opentelemetry.io/collector/pdata/pmetric"
)

type AggregationInterval string

const (
	AggregationInterval1m = "1m" // minutely
	AggregationInterval1h = "1h" // hourly
	AggregationInterval1d = "1d" // daily
)

var SupportedTimeAggregations = []string{
	AggregationInterval1m,
	AggregationInterval1h,
	AggregationInterval1d,
}

type AggregateFunction string

const (
	AggregateFunctionSum   AggregateFunction = "sum"
	AggregateFunctionAvg   AggregateFunction = "avg"
	AggregateFunctionMin   AggregateFunction = "min"
	AggregateFunctionMax   AggregateFunction = "max"
	AggregateFunctionCount AggregateFunction = "count"
	AggregateFunctionP50   AggregateFunction = "p50"
	AggregateFunctionP75   AggregateFunction = "p75"
	AggregateFunctionP90   AggregateFunction = "p90"
	AggregateFunctionP95   AggregateFunction = "p95"
	AggregateFunctionP99   AggregateFunction = "p99"
)

const (
	GroupByAttributesAll    string = "*"
	GroupByAttributesBucket string = "bucket"
)

var (
	errInvalidQueryTime      = errors.New("invalid query time")
	errUnknownMetricType     = errors.New("unknown metric type")
	errUnsupportedMetricType = errors.New("unsupported metric type")
	errUnknownMetricName     = errors.New("unknown metric name")
)

const (
	clauseJoinerAND   string = " AND "
	clauseJoinerOR    string = " OR "
	stringJoinerCOMMA string = ","
)

// OTEL metrics
type metricTable struct {
	tableName string
	typeName  string
}

var registeredOTELMetricTables = []metricTable{
	{
		tableName: constants.MetricsGaugeTableName,
		typeName:  pmetric.MetricTypeGauge.String(),
	},
	{
		tableName: constants.MetricsSumTableName,
		typeName:  pmetric.MetricTypeSum.String(),
	},
	{
		tableName: constants.MetricsHistogramTableName,
		typeName:  pmetric.MetricTypeHistogram.String(),
	},
	{
		tableName: constants.MetricsExponentialHistogramTableName,
		typeName:  pmetric.MetricTypeExponentialHistogram.String(),
	},
}

func getMetricTypeFromString(t string) pmetric.MetricType {
	switch strings.ToLower(t) {
	case "gauge":
		return pmetric.MetricTypeGauge
	case "sum":
		return pmetric.MetricTypeSum
	case "histogram":
		return pmetric.MetricTypeHistogram
	case "exponentialhistogram":
		return pmetric.MetricTypeExponentialHistogram
	default:
		return pmetric.MetricTypeEmpty
	}
}

func getTableForMetricType(mtype pmetric.MetricType) (string, error) {
	for _, t := range registeredOTELMetricTables {
		if strings.EqualFold(t.typeName, mtype.String()) {
			return t.tableName, nil
		}
	}
	return "", errUnsupportedMetricType
}

func getMetricHandlerForMetricType(mtype pmetric.MetricType) metricHandler {
	var mHandler metricHandler
	//nolint:exhaustive
	switch mtype {
	case pmetric.MetricTypeGauge, pmetric.MetricTypeSum:
		mHandler = &counterHandler{}
	case pmetric.MetricTypeHistogram, pmetric.MetricTypeExponentialHistogram:
		mHandler = &histogramHandler{}
	}
	return mHandler
}

func validateMetricQuery(
	projectID int64,
	startTimestamp time.Time,
	endTimestamp time.Time,
	query *Query,
) error {
	if projectID == 0 {
		return common.ErrUnknownProjectID
	}

	if startTimestamp.IsZero() || endTimestamp.IsZero() {
		return errInvalidQueryTime
	}

	if query.TargetMetric == "" {
		return errUnknownMetricName
	}

	if query.TargetType == "" {
		return errUnknownMetricType
	}

	var typeKnown bool
	for _, t := range registeredOTELMetricTables {
		if strings.EqualFold(t.typeName, query.TargetType) {
			typeKnown = true
			break
		}
	}
	if !typeKnown {
		return errUnsupportedMetricType
	}

	return nil
}

// Helper struct to help construct a sql query.
type queryBuilder struct {
	sql  string
	args []interface{}
	idx  int
}

// build takes the given sql string replaces any ? with the equivalent $<idx>
// and appends elems to the args slice.
func (s *queryBuilder) build(stmt string, elems ...interface{}) {
	// add the query params to the args slice, if any
	s.args = append(s.args, elems...)
	q := stmt
	// replace ? with corresponding $<idx>
	for range elems {
		s.idx += 1
		// placeholder that builds the string, for example, $1 when idx is 1
		p := fmt.Sprintf("$%d", s.idx)
		// replace the first ? found in the string
		q = strings.Replace(q, "?", p, 1)
	}
	// add the sanitized query statement to the current sql query
	s.sql += q
}
