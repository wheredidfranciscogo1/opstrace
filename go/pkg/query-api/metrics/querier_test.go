package metrics

import (
	"context"
	"fmt"
	"strings"
	"time"

	. "github.com/onsi/ginkgo/v2"
	. "github.com/onsi/gomega"
	"go.opentelemetry.io/collector/pdata/pmetric"

	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/query-api/common"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/testutils"
)

var _ = Context("metrics", func() {
	var (
		defaultProjectID  int64 = 12345
		defaultMetricName       = "cpu_seconds_total"
		refTime                 = time.Now().UTC()
		defaultStartTime        = refTime.Add(-1 * 24 * time.Hour)
		defaultEndTime          = refTime
	)

	DescribeTable(
		"validating metrics queries",
		func(pid int64, startTS, endTS time.Time, q *Query, expectedErr error) {
			err := validateMetricQuery(pid, startTS, endTS, q)
			if expectedErr == nil {
				Expect(err).ToNot(HaveOccurred())
			} else {
				Expect(err).To(Equal(expectedErr))
			}
		},
		Entry(
			"valid query",
			defaultProjectID,
			defaultStartTime,
			defaultEndTime,
			&Query{
				TargetMetric: defaultMetricName,
				TargetType:   pmetric.MetricTypeGauge.String(),
			},
			nil,
		),
		Entry(
			"query with project ID unset",
			int64(0),
			defaultStartTime,
			defaultEndTime,
			&Query{
				TargetMetric: defaultMetricName,
				TargetType:   pmetric.MetricTypeGauge.String(),
			},
			common.ErrUnknownProjectID,
		),
		Entry(
			"query with metric name missing",
			defaultProjectID,
			defaultStartTime,
			defaultEndTime,
			&Query{
				TargetType: pmetric.MetricTypeGauge.String(),
			},
			errUnknownMetricName,
		),
		Entry(
			"query with metric type missing",
			defaultProjectID,
			defaultStartTime,
			defaultEndTime,
			&Query{
				TargetMetric: defaultMetricName,
			},
			errUnknownMetricType,
		),
		Entry(
			"query with unsupported metric type",
			defaultProjectID,
			defaultStartTime,
			defaultEndTime,
			&Query{
				TargetMetric: defaultMetricName,
				TargetType:   "foo",
			},
			errUnsupportedMetricType,
		),
		Entry(
			"query with metric type cased differently",
			defaultProjectID,
			defaultStartTime,
			defaultEndTime,
			&Query{
				TargetMetric: defaultMetricName,
				TargetType:   strings.ToUpper(pmetric.MetricTypeGauge.String()),
			},
			nil,
		),
	)

	DescribeTable(
		"building metrics queries",
		func(pid int64, startTS, endTS time.Time, query *Query, expectedQuery string, args []interface{}) {
			mtype := getMetricTypeFromString(query.TargetType)
			Expect(mtype).ToNot(Equal(pmetric.MetricTypeEmpty))

			mHandler := getMetricHandlerForMetricType(mtype)
			Expect(mHandler).ToNot(BeNil())

			ctx := context.Background()
			qb, err := mHandler.buildQuery(ctx, pid, startTS, endTS, query)
			Expect(err).ToNot(HaveOccurred())
			err = testutils.CompareSQLStrings(qb.sql, expectedQuery)
			Expect(err).ToNot(HaveOccurred())
			Expect(qb.args).To(Equal(args))
		},
		Entry(
			"to fetch sample data for a sample gauge metric",
			defaultProjectID,
			defaultStartTime,
			defaultEndTime,
			&Query{
				TargetMetric: defaultMetricName,
				TargetType:   pmetric.MetricTypeGauge.String(),
			},
			`
SELECT
  any(MetricName) AS MetricName,
  any(MetricDescription) AS MetricDescription,
  any(MetricUnit) AS MetricUnit,
  Fingerprint,
  any(Attributes) AS Attributes,
  groupArray(Value) AS Values,
  groupArray(TimeUnix) AS Times
FROM
(
  SELECT
    MetricName,
    MetricDescription,
    MetricUnit,
    Fingerprint,
    Attributes,
    Value,
    TimeUnix
  FROM metrics.metrics_main_gauge
  WHERE
    ProjectId = $1
    AND MetricName = $2
    AND TimeUnix >= $3
    AND TimeUnix < $4
  ORDER BY MetricName, Fingerprint, TimeUnix
)
GROUP BY Fingerprint
`,
			[]interface{}{
				fmt.Sprintf("%d", defaultProjectID),
				defaultMetricName,
				defaultStartTime,
				defaultEndTime,
			},
		),
		Entry(
			"to fetch sample data for a sample histogram metric",
			defaultProjectID,
			defaultStartTime,
			defaultEndTime,
			&Query{
				TargetMetric: defaultMetricName,
				TargetType:   pmetric.MetricTypeHistogram.String(),
			},
			`
SELECT
  any(MetricName) AS MetricName,
  any(MetricDescription) AS MetricDescription,
  any(MetricUnit) AS MetricUnit,
  Fingerprint,
  any(Attributes) AS Attributes,
  groupArray(Avg) AS Values,
  groupArray(TimeUnix) AS Times
FROM
(
  SELECT
    MetricName,
    MetricDescription,
    MetricUnit,
    Fingerprint,
    Attributes,
    (Sum/Count) AS Avg,
    TimeUnix
  FROM metrics.metrics_main_histogram
  WHERE
    ProjectId = $1
    AND MetricName = $2
    AND TimeUnix >= $3
    AND TimeUnix < $4
  ORDER BY MetricName, Fingerprint, TimeUnix
)
GROUP BY Fingerprint
`,
			[]interface{}{
				fmt.Sprintf("%d", defaultProjectID),
				defaultMetricName,
				defaultStartTime,
				defaultEndTime,
			},
		),
	)
})
