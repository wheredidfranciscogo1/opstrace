package metrics

import (
	"context"
	"fmt"
	"slices"
	"time"

	. "github.com/onsi/ginkgo/v2"
	. "github.com/onsi/gomega"
	"go.opentelemetry.io/collector/pdata/pmetric"

	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/constants"
	metricsmigrations "gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/db/migrations/metrics"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/testutils"
)

type dataRow struct {
	ProjectID         string    `ch:"ProjectId"`
	MetricName        string    `ch:"MetricName"`
	MetricDescription string    `ch:"MetricDescription"`
	Attributes        []string  `ch:"Attributes"`
	LastIngestedAt    time.Time `ch:"LastIngestedAt"`
}

const queryTmpl = `
SELECT
  ProjectId,
  MetricName,
  any(MetricDescription) AS MetricDescription,
  arraySort(groupUniqArrayArrayMerge(AttributeKeysState)) AS Attributes,
  max(LastIngestedAt) AS LastIngestedAt
FROM
  %s.%s
WHERE
  ProjectId = ?
  AND MetricName = ?
GROUP BY ProjectId, MetricName
`

type setupFn func(ctx context.Context, testEnv testutils.ClickHouseServer) error

var _ = Context("metrics metadata", func() {
	DescribeTable(
		"querying metrics metadata via materialized view",
		func(mtype pmetric.MetricType, setupFn setupFn) {
			// ensure the tests run within the stipulated timeout
			ctx, cancel := context.WithTimeout(context.Background(), 180*time.Second)
			defer cancel()
			testEnv, err := testutils.NewClickHouseServer(ctx)
			Expect(err).ToNot(HaveOccurred())
			// ensure cleaning up the container afterwards
			defer testEnv.Container.Terminate(ctx)
			// test table state/data
			conn, err := testEnv.GetConnection()
			Expect(err).ToNot(HaveOccurred())
			err = conn.Ping(ctx)
			Expect(err).ToNot(HaveOccurred())
			// setup test dependencies
			err = setupFn(ctx, testEnv)
			Expect(err).ToNot(HaveOccurred())
			// insert data into raw table, then query it back via the MV
			m := generateOTELMetrics(mtype, map[string]string{
				"keyone": "valone",
				"keytwo": "valtwo",
			}, 10)
			err = persistOTELMetrics(ctx, conn, logger.Desugar(), m)
			Expect(err).ToNot(HaveOccurred())

			tableName, err := getTableForMetricType(mtype)
			Expect(err).ToNot(HaveOccurred())
			query := fmt.Sprintf(queryTmpl, constants.MetricsDatabaseName, tableName+"_metadata")

			var rows []dataRow
			err = conn.Select(ctx, &rows, query, randomProjectID, randomMetricName)
			Expect(err).ToNot(HaveOccurred())
			{
				attrKeys := make([]string, 0)
				for _, r := range rows {
					attrKeys = append(attrKeys, r.Attributes...)
				}
				Expect(attrKeys).To(HaveLen(2))
				expectedKeys := []string{"keyone", "keytwo"}
				slices.Sort(expectedKeys)
				Expect(attrKeys).To(Equal(expectedKeys))
			}
			// insert new metrics with new attributes, verify attributes again
			m = generateOTELMetrics(mtype, map[string]string{
				"keyone":   "valone",
				"keytwo":   "valtwo",
				"keythree": "valthree",
				"keyfour":  "valfour",
			}, 10)
			err = persistOTELMetrics(ctx, conn, logger.Desugar(), m)
			Expect(err).ToNot(HaveOccurred())
			rows = []dataRow{}
			err = conn.Select(ctx, &rows, query, randomProjectID, randomMetricName)
			Expect(err).ToNot(HaveOccurred())
			{
				attrKeys := make([]string, 0)
				for _, r := range rows {
					attrKeys = append(attrKeys, r.Attributes...)
				}
				Expect(attrKeys).To(HaveLen(4))
				expectedKeys := []string{"keyone", "keytwo", "keythree", "keyfour"}
				slices.Sort(expectedKeys)
				Expect(attrKeys).To(Equal(expectedKeys))
			}
			// insert new datapoints with some attributes removed, verify attributes again
			m = generateOTELMetrics(mtype, map[string]string{
				"keyone":   "valone",
				"keytwo":   "valtwo",
				"keythree": "valthree",
			}, 10)
			err = persistOTELMetrics(ctx, conn, logger.Desugar(), m)
			Expect(err).ToNot(HaveOccurred())
			rows = []dataRow{}
			err = conn.Select(ctx, &rows, query, randomProjectID, randomMetricName)
			Expect(err).ToNot(HaveOccurred())
			{
				attrKeys := make([]string, 0)
				for _, r := range rows {
					attrKeys = append(attrKeys, r.Attributes...)
				}
				Expect(attrKeys).To(HaveLen(4))
				expectedKeys := []string{"keyone", "keytwo", "keythree", "keyfour"}
				slices.Sort(expectedKeys)
				Expect(attrKeys).To(Equal(expectedKeys))
			}
		},
		Entry(
			"list sum metrics metadata",
			pmetric.MetricTypeSum,
			setupOTELSumMetrics,
		),
		Entry(
			"list gauge metrics metadata",
			pmetric.MetricTypeGauge,
			setupOTELGaugeMetrics,
		),
		Entry(
			"list histogram metrics metadata",
			pmetric.MetricTypeHistogram,
			setupOTELHistogramMetrics,
		),
		Entry(
			"list exponential histogram metrics metadata",
			pmetric.MetricTypeExponentialHistogram,
			setupOTELExpHistogramMetrics,
		),
	)
})

func setupOTELSumMetrics(ctx context.Context, testEnv testutils.ClickHouseServer) error {
	// create necessary database/tables
	if err := testEnv.CreateDatabase(ctx, constants.MetricsDatabaseName); err != nil {
		return err
	}
	// source table
	schema, err := (&metricsmigrations.SumMain{}).Render(
		metricsmigrations.MigrationData{
			DatabaseName:      constants.MetricsDatabaseName,
			TableName:         constants.MetricsSumTableName,
			SelfHostedVersion: true,
		},
	)
	if err != nil {
		return err
	}
	if err := testEnv.Run(ctx, schema); err != nil {
		return err
	}
	// target table
	schema, err = (&metricsmigrations.SumMetricsMetadataMVTarget{}).Render(
		metricsmigrations.MVMigrationData{
			DatabaseName:      constants.MetricsDatabaseName,
			TableName:         constants.MetricsSumMetadataTableName,
			SelfHostedVersion: true,
		},
	)
	if err != nil {
		return err
	}
	if err := testEnv.Run(ctx, schema); err != nil {
		return err
	}
	// materialized view
	schema, err = (&metricsmigrations.SumMetricsMetadataMV{}).Render(
		metricsmigrations.MVMigrationData{
			DatabaseName:      constants.MetricsDatabaseName,
			TableName:         constants.MetricsSumMetadataMVName,
			SourceTableName:   constants.MetricsSumTableName,
			TargetTableName:   constants.MetricsSumMetadataTableName,
			SelfHostedVersion: true,
		},
	)
	if err != nil {
		return err
	}
	if err := testEnv.Run(ctx, schema); err != nil {
		return err
	}
	return nil
}

func setupOTELGaugeMetrics(ctx context.Context, testEnv testutils.ClickHouseServer) error {
	// create necessary database/tables
	if err := testEnv.CreateDatabase(ctx, constants.MetricsDatabaseName); err != nil {
		return err
	}
	// source table
	schema, err := (&metricsmigrations.GaugeMain{}).Render(
		metricsmigrations.MigrationData{
			DatabaseName:      constants.MetricsDatabaseName,
			TableName:         constants.MetricsGaugeTableName,
			SelfHostedVersion: true,
		},
	)
	if err != nil {
		return err
	}
	if err := testEnv.Run(ctx, schema); err != nil {
		return err
	}
	// target table
	schema, err = (&metricsmigrations.GaugeMetricsMetadataMVTarget{}).Render(
		metricsmigrations.MVMigrationData{
			DatabaseName:      constants.MetricsDatabaseName,
			TableName:         constants.MetricsGaugeMetadataTableName,
			SelfHostedVersion: true,
		},
	)
	if err != nil {
		return err
	}
	if err := testEnv.Run(ctx, schema); err != nil {
		return err
	}
	// materialized view
	schema, err = (&metricsmigrations.GaugeMetricsMetadataMV{}).Render(
		metricsmigrations.MVMigrationData{
			DatabaseName:      constants.MetricsDatabaseName,
			TableName:         constants.MetricsGaugeMetadataMVName,
			SourceTableName:   constants.MetricsGaugeTableName,
			TargetTableName:   constants.MetricsGaugeMetadataTableName,
			SelfHostedVersion: true,
		},
	)
	if err != nil {
		return err
	}
	if err := testEnv.Run(ctx, schema); err != nil {
		return err
	}
	return nil
}

func setupOTELHistogramMetrics(ctx context.Context, testEnv testutils.ClickHouseServer) error {
	// create necessary database/tables
	if err := testEnv.CreateDatabase(ctx, constants.MetricsDatabaseName); err != nil {
		return err
	}
	// source table
	schema, err := (&metricsmigrations.HistMain{}).Render(
		metricsmigrations.MigrationData{
			DatabaseName:      constants.MetricsDatabaseName,
			TableName:         constants.MetricsHistogramTableName,
			SelfHostedVersion: true,
		},
	)
	if err != nil {
		return err
	}
	if err := testEnv.Run(ctx, schema); err != nil {
		return err
	}
	// target table
	schema, err = (&metricsmigrations.HistogramMetricsMetadataMVTarget{}).Render(
		metricsmigrations.MVMigrationData{
			DatabaseName:      constants.MetricsDatabaseName,
			TableName:         constants.MetricsHistogramMetadataTableName,
			SelfHostedVersion: true,
		},
	)
	if err != nil {
		return err
	}
	if err := testEnv.Run(ctx, schema); err != nil {
		return err
	}
	// materialized view
	schema, err = (&metricsmigrations.HistogramMetricsMetadataMV{}).Render(
		metricsmigrations.MVMigrationData{
			DatabaseName:      constants.MetricsDatabaseName,
			TableName:         constants.MetricsHistogramMetadataMVName,
			SourceTableName:   constants.MetricsHistogramTableName,
			TargetTableName:   constants.MetricsHistogramMetadataTableName,
			SelfHostedVersion: true,
		},
	)
	if err != nil {
		return err
	}
	if err := testEnv.Run(ctx, schema); err != nil {
		return err
	}
	return nil
}

func setupOTELExpHistogramMetrics(ctx context.Context, testEnv testutils.ClickHouseServer) error {
	// create necessary database/tables
	if err := testEnv.CreateDatabase(ctx, constants.MetricsDatabaseName); err != nil {
		return err
	}
	// source table
	schema, err := (&metricsmigrations.ExpHistMain{}).Render(
		metricsmigrations.MigrationData{
			DatabaseName:      constants.MetricsDatabaseName,
			TableName:         constants.MetricsExponentialHistogramTableName,
			SelfHostedVersion: true,
		},
	)
	if err != nil {
		return err
	}
	if err := testEnv.Run(ctx, schema); err != nil {
		return err
	}
	// target table
	schema, err = (&metricsmigrations.ExpHistogramMetricsMetadataMVTarget{}).Render(
		metricsmigrations.MVMigrationData{
			DatabaseName:      constants.MetricsDatabaseName,
			TableName:         constants.MetricsExponentialHistogramMetadataTableName,
			SelfHostedVersion: true,
		},
	)
	if err != nil {
		return err
	}
	if err := testEnv.Run(ctx, schema); err != nil {
		return err
	}
	// materialized view
	schema, err = (&metricsmigrations.ExpHistogramMetricsMetadataMV{}).Render(
		metricsmigrations.MVMigrationData{
			DatabaseName:      constants.MetricsDatabaseName,
			TableName:         constants.MetricsExponentialHistogramMetadataMVName,
			SourceTableName:   constants.MetricsExponentialHistogramTableName,
			TargetTableName:   constants.MetricsExponentialHistogramMetadataTableName,
			SelfHostedVersion: true,
		},
	)
	if err != nil {
		return err
	}
	if err := testEnv.Run(ctx, schema); err != nil {
		return err
	}
	return nil
}
