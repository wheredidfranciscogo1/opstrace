package logs

import (
	"context"
	"time"
)

type QueryContext struct {
	ProjectID int64 `json:"project_id"`

	Queries    map[string]*Query `json:"queries,omitempty"`
	Expression string            `json:"expression,omitempty"`

	StartTimestamp time.Time `json:"start_time"`
	EndTimestamp   time.Time `json:"end_time"`
}

type Query struct {
	// TODO(prozlach): Add support for querying of other attributes
}

type Querier interface {
	GetLogs(context.Context, *QueryContext) (*LogsResponse, error)
}

type LogsResponse struct {
	StartTimestamp int64     `json:"start_ts"`
	EndTimestamp   int64     `json:"end_ts"`
	Results        []LogsRow `json:"results"`
}

type LogsRow struct {
	Timestamp          time.Time         `json:"timestamp"`
	TraceID            string            `json:"trace_id" ch:"TraceId"`
	SpanID             string            `json:"span_id" ch:"SpanId"`
	TraceFlags         uint32            `json:"trace_flags"`
	SeverityText       string            `json:"severity_text"`
	SeverityNumber     uint8             `json:"severity_number"`
	ServiceName        string            `json:"service_name"`
	Body               string            `json:"body"`
	ResourceAttributes map[string]string `json:"resource_attributes"`
	LogAttributes      map[string]string `json:"log_attributes"`
}
