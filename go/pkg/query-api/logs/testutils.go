package logs

import (
	"context"
	"time"
)

var startTS = time.Unix(1701264345, 0).UTC()
var middleTS = startTS.Add(30 * time.Minute)
var endTS = startTS.Add(1 * time.Hour)

var sampleData = LogsResponse{
	StartTimestamp: startTS.UnixNano(),
	EndTimestamp:   endTS.UnixNano(),

	Results: []LogsRow{
		{
			Timestamp:      middleTS,
			TraceID:        "tID.1",
			SpanID:         "sID.1",
			TraceFlags:     0,
			SeverityText:   "Info",
			SeverityNumber: 9,
			ServiceName:    "Boryna service",
			Body:           "Boryna unexpectedly Maryned",
			ResourceAttributes: map[string]string{
				"foo": "bar",
				"baz": "blah",
			},
			LogAttributes: map[string]string{
				"maryna": "boryna",
			},
		},
	},
}

type MockQuerier struct{}

var _ Querier = (*MockQuerier)(nil)

func (*MockQuerier) GetLogs(_ context.Context, _ *QueryContext) (*LogsResponse, error) {
	return &sampleData, nil
}
