package logs

import (
	"context"
	"fmt"
	"time"

	"github.com/ClickHouse/clickhouse-go/v2"
	"go.uber.org/zap"

	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/constants"
	chinternal "gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/query-api/clickhouse"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/query-api/common"
)

const baseLogsSearchTmpl string = `
SELECT
  Timestamp,
  TraceId,
  SpanId,
  TraceFlags,
  SeverityText,
  SeverityNumber,
  ServiceName,
  Body,
  ResourceAttributes,
  LogAttributes
FROM %s.%s
WHERE
  ProjectId = ?
  AND Timestamp >= ?
  AND Timestamp < ?
ORDER BY Timestamp, ServiceName
`

type chQuerier struct {
	db     clickhouse.Conn
	logger *zap.Logger
}

// ascertain interface implementation
var _ Querier = (*chQuerier)(nil)

func NewQuerier(
	clickHouseDSN string,
	clickHouseCloudDSN string,
	opts *clickhouse.Options,
	logger *zap.Logger,
) (Querier, error) {
	var (
		db  clickhouse.Conn
		err error
	)
	if clickHouseCloudDSN != "" {
		db, err = chinternal.GetDatabaseConnection(clickHouseCloudDSN, opts, logger, 30*time.Second)
		if err != nil {
			return nil, fmt.Errorf("getting cloud database handle: %w", err)
		}
	} else {
		db, err = chinternal.GetDatabaseConnection(clickHouseDSN, opts, logger, 3*time.Second)
		if err != nil {
			return nil, fmt.Errorf("getting database handle: %w", err)
		}
	}

	return &chQuerier{
		db:     db,
		logger: logger,
	}, nil
}

func (q *chQuerier) GetLogs(ctx context.Context, queryContext *QueryContext) (*LogsResponse, error) {
	if queryContext == nil {
		return nil, nil // nothing to do
	}

	if queryContext.ProjectID == 0 {
		return nil, common.ErrUnknownProjectID
	}

	if len(queryContext.Queries) > 1 {
		// NOTE(prozlach): we return the result of the first query that
		// iteration loop starts with. In case there are more than one query
		// and we do not adjust this loop, the bug may not be imediatelly
		// obvious as the iteration order for has is randomized AFAIK.<F3>
		panic("multiple queries are not supported yet")
	}

	for alias, query := range queryContext.Queries {
		q.logger.Debug(
			"processing query",
			zap.String("query alias", alias),
			zap.Any("query", query),
		)

		sqlQuery, sqlArgs := q.buildQuery(
			queryContext.ProjectID,
			queryContext.StartTimestamp,
			queryContext.EndTimestamp,
		)

		q.logger.Debug(
			"executing query",
			zap.Any("query string", sqlQuery),
			zap.Any("query args", sqlArgs),
		)

		response, err := q.executeQuery(
			ctx,
			queryContext.StartTimestamp, queryContext.EndTimestamp,
			sqlQuery, sqlArgs,
		)
		if err != nil {
			return nil, fmt.Errorf("executing query: %w", err)
		}

		return response, nil
	}

	return nil, nil
}

func (q *chQuerier) buildQuery(
	projectID int64,
	startTimestamp, endTimestamp time.Time,
) (string, []interface{}) {
	builder := &common.QueryBuilder{}
	queryStr := fmt.Sprintf(
		baseLogsSearchTmpl, constants.LoggingDatabaseName, constants.LoggingTableName,
	)
	builder.Build(
		queryStr,
		fmt.Sprintf("%d", projectID),
		startTimestamp,
		endTimestamp,
	)
	return builder.SQL(), builder.Args()
}

func (q *chQuerier) executeQuery(
	ctx context.Context,
	startTimestamp, endTimestamp time.Time,
	sql string, args []interface{},
) (*LogsResponse, error) {
	res := &LogsResponse{
		StartTimestamp: startTimestamp.UnixNano(),
		EndTimestamp:   endTimestamp.UnixNano(),
		Results:        make([]LogsRow, 0),
	}
	if err := q.db.Select(ctx, &res.Results, sql, args...); err != nil {
		return nil, fmt.Errorf("executing logs query: %w", err)
	}

	return res, nil
}
