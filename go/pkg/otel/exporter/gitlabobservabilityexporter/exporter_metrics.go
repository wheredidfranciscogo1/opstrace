package gitlabobservabilityexporter

import (
	"context"
	"fmt"
	"time"

	"go.opentelemetry.io/collector/component"
	"go.opentelemetry.io/collector/pdata/pmetric"
	"go.uber.org/zap"

	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/otel/exporter/gitlabobservabilityexporter/common"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/otel/exporter/gitlabobservabilityexporter/metrics"
)

type metricsExporter struct {
	db     *database
	logger *zap.Logger
	cfg    *config
}

func newMetricsExporter(logger *zap.Logger, cfg *config) (*metricsExporter, error) {
	var (
		db  *database
		err error
	)
	if cfg.ClickHouseCloudDSN != "" {
		db, err = newCloudDB(cfg)
		if err != nil {
			return nil, err
		}
	} else {
		// if cloud db is not provided, use self-hosted instance
		db, err = newDB(cfg)
		if err != nil {
			return nil, err
		}
	}
	return &metricsExporter{
		db:     db,
		logger: logger,
		cfg:    cfg,
	}, nil
}

func (e *metricsExporter) start(ctx context.Context, _ component.Host) error {
	pingCtx, cancel := context.WithTimeout(ctx, 5*time.Second) // arbitrary timeout for now
	defer cancel()

	if err := e.db.conn.Ping(pingCtx); err != nil {
		return fmt.Errorf("pinging clickhouse backend failed: %w", err)
	}

	return nil
}

func (e *metricsExporter) shutdown(_ context.Context) error {
	if e.db != nil {
		if e.db.conn != nil {
			if err := e.db.conn.Close(); err != nil {
				return fmt.Errorf("closing underlying connection: %w", err)
			}
		}
	}
	return nil
}

func (e *metricsExporter) pushMetricsData(ctx context.Context, md pmetric.Metrics) error {
	ingester := metrics.NewClickHouseIngester(e.db.conn, e.logger)
	for i := 0; i < md.ResourceMetrics().Len(); i++ {
		metrics := md.ResourceMetrics().At(i)
		resAttr := common.AttributesToMap(metrics.Resource().Attributes())
		for j := 0; j < metrics.ScopeMetrics().Len(); j++ {
			rs := metrics.ScopeMetrics().At(j).Metrics()
			scopeInstr := metrics.ScopeMetrics().At(j).Scope()
			scopeURL := metrics.ScopeMetrics().At(j).SchemaUrl()
			for k := 0; k < rs.Len(); k++ {
				r := rs.At(k)
				//nolint:exhaustive
				switch r.Type() {
				case pmetric.MetricTypeGauge,
					pmetric.MetricTypeSum,
					pmetric.MetricTypeHistogram,
					pmetric.MetricTypeExponentialHistogram:
					ingester.Add(resAttr, metrics.SchemaUrl(), scopeInstr, scopeURL, r)
				case pmetric.MetricTypeEmpty:
					return fmt.Errorf("metrics type is unset")
				default:
					return fmt.Errorf("unsupported metric type: %s", r.Type().String())
				}
			}
		}
	}
	if err := ingester.Write(ctx); err != nil {
		return err
	}
	return nil
}
