package gitlabobservabilityexporter

import (
	"context"
	"encoding/hex"
	"fmt"
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/constants"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/db/migrations/tracing"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/otel/exporter/gitlabobservabilityexporter/common"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/testutils"
	"go.opentelemetry.io/collector/pdata/pcommon"
	"go.opentelemetry.io/collector/pdata/ptrace"
	conventions "go.opentelemetry.io/collector/semconv/v1.18.0"
	"go.uber.org/zap/zaptest"
)

const baseQueryTmpl string = `
SELECT
	ProjectId,
	hex(TraceId) AS TraceId,
	hex(SpanId) AS SpanId,
	ServiceName,
	ResourceAttributes,
	SpanAttributes
FROM
	%s.%s
`

type dataRow struct {
	ProjectID          string            `ch:"ProjectId"`
	TraceID            string            `ch:"TraceId"`
	SpanID             string            `ch:"SpanId"`
	ServiceName        string            `ch:"ServiceName"`
	ResourceAttributes map[string]string `ch:"ResourceAttributes"`
	SpanAttributes     map[string]string `ch:"SpanAttributes"`
}

var (
	randomTraceID = [16]byte{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 2, 3, 4}
	randomSpanID  = [8]byte{0, 0, 0, 0, 1, 2, 3, 4}
)

const (
	randomProjectID   string = "12345"
	randomNamespaceID string = "123"
	randomServiceName string = "test-service"
)

func TestPushTraceData(t *testing.T) {
	// ensure the tests run within the stipulated timeout
	ctx, cancel := context.WithTimeout(context.Background(), 180*time.Second)
	defer cancel()

	testEnv, err := testutils.NewClickHouseServer(ctx)
	require.NoError(t, err)
	// ensure cleaning up the container afterwards
	defer testEnv.Container.Terminate(ctx)

	dsn := testEnv.GetDSN()
	t.Run("test data insertion", func(t *testing.T) {
		// create necessary database/tables
		err := testEnv.CreateDatabase(ctx, constants.TracingDatabaseName)
		require.NoError(t, err)
		// generate table schemas
		testCreateTableSchema, err := (&tracing.TracesMain{}).Render(
			tracing.MigrationData{
				DatabaseName:      constants.TracingDatabaseName,
				TableName:         constants.TracingDistTableName,
				SelfHostedVersion: true,
			},
		)
		require.NoError(t, err)
		// create tables
		err = testEnv.Run(ctx, testCreateTableSchema)
		require.NoError(t, err)
		// setup exporter, configure database
		exporter := newTracesExporterInternal(t, ctx, fmt.Sprintf("%s/%s", dsn, constants.TracingDatabaseName))
		// ensure data was inserted correctly
		traceCount := 2
		err = exporter.pushTraceData(ctx, generateTraces(traceCount))
		require.NoError(t, err)
		// test table state/data
		conn, err := testEnv.GetConnection()
		require.NoError(t, err)
		rows, err := conn.Query(
			ctx,
			fmt.Sprintf(baseQueryTmpl, constants.TracingDatabaseName, constants.TracingDistTableName),
		)
		require.NoError(t, err)
		var rowsFound int
		for rows.Next() {
			rowsFound++
			var r dataRow
			if err := rows.ScanStruct(&r); err != nil {
				t.Error(err)
			}
			assert.Equal(t, randomProjectID, r.ProjectID)
			assert.Equal(t, hex.EncodeToString(randomTraceID[:]), r.TraceID)
			assert.Equal(t, hex.EncodeToString(randomSpanID[:]), r.SpanID)
			assert.Equal(t, randomServiceName, r.ServiceName)
			assert.Equal(t, randomServiceName, r.ResourceAttributes[conventions.AttributeServiceName])
			assert.Equal(t, randomServiceName, r.SpanAttributes[conventions.AttributeServiceName])
		}
		assert.Equal(t, traceCount, rowsFound, "count of spans inserted")
	})
}

func newTracesExporterInternal(t *testing.T, ctx context.Context, dsn string) *tracesExporter {
	t.Helper()
	c := &config{ClickHouseDSN: dsn}
	exporter, err := newTracesExporter(zaptest.NewLogger(t), c)
	require.NoError(t, err)
	require.NoError(t, exporter.start(ctx, nil))
	t.Cleanup(func() {
		_ = exporter.shutdown(ctx)
	})
	return exporter
}

func generateTraces(count int) ptrace.Traces {
	traces := ptrace.NewTraces()
	rs := traces.ResourceSpans().AppendEmpty()
	rs.SetSchemaUrl("https://opentelemetry.io/schemas/1.4.0")
	rs.Resource().SetDroppedAttributesCount(10)
	rs.Resource().Attributes().PutStr(conventions.AttributeServiceName, randomServiceName)
	ss := rs.ScopeSpans().AppendEmpty()
	ss.Scope().SetName("io.opentelemetry.contrib.clickhouse")
	ss.Scope().SetVersion("1.0.0")
	ss.SetSchemaUrl("https://opentelemetry.io/schemas/1.7.0")
	ss.Scope().SetDroppedAttributesCount(10)
	ss.Scope().Attributes().PutStr("lib", "clickhouse")
	for i := 0; i < count; i++ {
		s := ss.Spans().AppendEmpty()
		s.SetTraceID(randomTraceID)
		s.SetSpanID(randomSpanID)
		s.SetStartTimestamp(pcommon.NewTimestampFromTime(time.Now()))
		s.SetEndTimestamp(pcommon.NewTimestampFromTime(time.Now()))
		s.Attributes().PutStr(conventions.AttributeServiceName, randomServiceName)
		s.Attributes().PutStr(common.ProjectIDHeader, randomProjectID)
		s.Attributes().PutStr(common.NamespaceIDHeader, randomNamespaceID)

		event := s.Events().AppendEmpty()
		event.SetName("event1")
		event.SetTimestamp(pcommon.NewTimestampFromTime(time.Now()))
		link := s.Links().AppendEmpty()
		link.Attributes().PutStr("k", "v")
	}
	return traces
}
