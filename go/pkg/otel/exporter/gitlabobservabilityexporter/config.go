package gitlabobservabilityexporter

import (
	"errors"
	"fmt"
	"time"

	"github.com/ClickHouse/clickhouse-go/v2"
	"go.opentelemetry.io/collector/exporter/exporterhelper"
	"go.uber.org/multierr"
)

type config struct {
	exporterhelper.TimeoutSettings `mapstructure:",squash"`
	exporterhelper.RetrySettings   `mapstructure:"retry_on_failure"`
	// TenantID is the top-level namespace identifier.
	TenantID string `mapstructure:"tenant_id"`
	// ClickHouseDSN is the clickhouse endpoint.
	ClickHouseDSN             string        `mapstructure:"clickhouse_dsn"`
	ClickHouseMaxOpenConns    int           `mapstructure:"clickhouse_max_open_conns"`
	ClickHouseMaxIdleConns    int           `mapstructure:"clickhouse_max_idle_conns"`
	ClickHouseConnMaxLifetime time.Duration `mapstructure:"clickhouse_conn_max_lifetime"`

	ClickHouseCloudDSN string `mapstructure:"clickhouse_cloud_dsn"`
}

var (
	errConfigNoDSN      = errors.New("clickhouse DSN must be specified")
	errConfigInvalidDSN = errors.New("clickhouse DSN must be valid and url-format")
	errConfigNoTenantID = errors.New("tenant ID must be specified")
)

// Validate the clickhouse server configuration.
func (cfg *config) Validate() error {
	var err error
	if cfg.ClickHouseDSN == "" {
		err = multierr.Append(err, errConfigNoDSN)
	}
	// Validate DSN with clickhouse driver.
	// Last chance to catch invalid config.
	if _, e := clickhouse.ParseDSN(cfg.ClickHouseDSN); e != nil {
		err = multierr.Append(err, errConfigInvalidDSN)
	}
	// Validate Tenant ID
	if cfg.TenantID == "" {
		err = multierr.Append(err, errConfigNoTenantID)
	}
	if err != nil {
		return fmt.Errorf("validating provided config: %w", err)
	}
	// setup ClickHouse defaults
	if cfg.ClickHouseMaxOpenConns == 0 {
		cfg.ClickHouseMaxOpenConns = 100
	}
	if cfg.ClickHouseMaxIdleConns == 0 {
		cfg.ClickHouseMaxIdleConns = 5
	}
	if cfg.ClickHouseConnMaxLifetime == 0 {
		cfg.ClickHouseConnMaxLifetime = 1 * time.Hour
	}
	return nil
}
