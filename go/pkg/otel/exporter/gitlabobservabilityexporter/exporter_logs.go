package gitlabobservabilityexporter

import (
	"context"
	"fmt"
	"time"

	"github.com/ClickHouse/clickhouse-go/v2/lib/driver"
	"github.com/prometheus/common/model"
	"go.opentelemetry.io/collector/component"
	"go.opentelemetry.io/collector/pdata/plog"
	conventions "go.opentelemetry.io/collector/semconv/v1.18.0"
	"go.uber.org/zap"

	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/constants"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/otel/exporter/gitlabobservabilityexporter/common"
)

type logsExporter struct {
	db     *database
	logger *zap.Logger
	cfg    *config
}

func newLogsExporter(logger *zap.Logger, cfg *config) (*logsExporter, error) {
	var (
		db  *database
		err error
	)
	if cfg.ClickHouseCloudDSN != "" {
		db, err = newCloudDB(cfg)
		if err != nil {
			return nil, err
		}
	} else {
		// if cloud db is not provided, use self-hosted instance
		db, err = newDB(cfg)
		if err != nil {
			return nil, err
		}
	}
	return &logsExporter{
		db:     db,
		logger: logger,
		cfg:    cfg,
	}, nil
}

func (e *logsExporter) start(ctx context.Context, _ component.Host) error {
	pingCtx, cancel := context.WithTimeout(ctx, 5*time.Second) // arbitrary timeout for now
	defer cancel()

	if err := e.db.conn.Ping(pingCtx); err != nil {
		return fmt.Errorf("pinging clickhouse backend failed: %w", err)
	}

	return nil
}

func (e *logsExporter) shutdown(_ context.Context) error {
	if e.db != nil && e.db.conn != nil {
		if err := e.db.conn.Close(); err != nil {
			return fmt.Errorf("closing underlying connection: %w", err)
		}
	}
	return nil
}

const (
	insertLogsSQL = `
INSERT INTO ` + constants.LoggingDatabaseName + `.` + constants.LoggingTableName + ` (
    ProjectId,
    Fingerprint,
    Timestamp,
    ObservedTimestamp,
    TraceId,
    SpanId,
    TraceFlags,
    SeverityText,
    SeverityNumber,
    ServiceName,
    Body,
    ResourceAttributes,
    LogAttributes
)`
)

func (e *logsExporter) pushLogsData(ctx context.Context, ld plog.Logs) error {
	m := &plog.ProtoMarshaler{}
	logsdataSizeBytes.WithLabelValues(e.cfg.TenantID).Add(float64(m.LogsSize(ld)))

	var (
		batchSize float64
		batch     driver.Batch
		err       error
	)
	batch, err = e.db.conn.PrepareBatch(ctx, insertLogsSQL)
	if err != nil {
		return fmt.Errorf("preparing logs batch for clickhouse backend: %w", err)
	}

	logs := ld.ResourceLogs()
	for i := 0; i < logs.Len(); i++ {
		scopeLogs := logs.At(i)
		scopeLogsResource := scopeLogs.Resource()
		scopeLogsAttributes := common.AttributesToMap(scopeLogsResource.Attributes())

		var serviceName string
		if v, ok := scopeLogsResource.Attributes().Get(conventions.AttributeServiceName); ok {
			serviceName = v.Str()
		}

		scopeLogsSlice := scopeLogs.ScopeLogs()
		for j := 0; j < scopeLogsSlice.Len(); j++ {
			logRecords := scopeLogsSlice.At(j).LogRecords()

			logsReceivedCounter.WithLabelValues(e.cfg.TenantID).Add(float64(logRecords.Len()))
			for k := 0; k < logRecords.Len(); k++ {
				logRecord := logRecords.At(k)

				logRecordAttributes := common.AttributesToMap(logRecord.Attributes())
				projectID := common.GetProjectID(logRecordAttributes)
				logRecordBody := logRecord.Body().Str()
				fingerprint := getLogFingerprint(logRecordBody, scopeLogsAttributes, logRecordAttributes)

				delete(logRecordAttributes, common.NamespaceIDHeader)
				delete(logRecordAttributes, common.ProjectIDHeader)

				severityNumber := logRecord.SeverityNumber()
				if severityNumber == plog.SeverityNumberUnspecified {
					severityNumber = severityTextToNumber(logRecord.SeverityText())
				} // otherwise we assume that ServerityNumber and SeverityText are in sync.

				e.logger.Debug("append log record",
					zap.String("ProjectId", projectID),
					zap.String("Fingerprint", fingerprint),
					zap.Time("Timestamp", logRecord.Timestamp().AsTime()),
					zap.Time("ObservedTimestamp", time.Now()),
					zap.String("TraceId", logRecord.TraceID().String()),
					zap.String("SpanId", logRecord.SpanID().String()),
					zap.Uint32("TraceFlags", uint32(logRecord.Flags())),
					zap.String("SeverityText", logRecord.SeverityText()),
					zap.Int32("SeverityNumber", int32(severityNumber)),
					zap.String("ServiceName", serviceName),
					zap.String("Body", logRecordBody),
					zap.Reflect("ResourceAttributes", scopeLogsAttributes),
					zap.Reflect("LogAttributes", logRecordAttributes),
				)

				if err := batch.Append(
					projectID,
					fingerprint,
					logRecord.Timestamp().AsTime(),
					time.Now(),
					common.TraceIDToFixedString(logRecord.TraceID()),
					common.SpanIDToFixedString(logRecord.SpanID()),
					uint32(logRecord.Flags()),
					logRecord.SeverityText(),
					severityNumber,
					serviceName,
					logRecordBody,
					scopeLogsAttributes,
					logRecordAttributes,
				); err != nil {
					e.logger.Warn("appending log entry failed", zap.Error(err))
					continue
				}
				batchSize++
			}
		}
	}

	logsIngestedCounter.WithLabelValues(e.cfg.TenantID).Add(batchSize)

	if err := batch.Send(); err != nil {
		return fmt.Errorf("sending logs batch to clickhouse backend: %w", err)
	}
	return nil
}

func getLogFingerprint(body string, resourceAttributes, logAttributes map[string]string) string {
	labelSet := model.LabelSet{}
	for k, v := range resourceAttributes {
		labelSet[model.LabelName("resAttr-"+k)] = model.LabelValue(v)
	}
	for k, v := range logAttributes {
		labelSet[model.LabelName("logAttr-"+k)] = model.LabelValue(v)
	}
	labelSet[model.LabelName("__log_body")] = model.LabelValue(body)
	return labelSet.Fingerprint().String()
}

//nolint:funlen,gocyclo,cyclop // It is just a long switch statement
func severityTextToNumber(severityText string) plog.SeverityNumber {
	switch severityText {
	case "Trace":
		return plog.SeverityNumberTrace
	case "Trace2":
		return plog.SeverityNumberTrace2
	case "Trace3":
		return plog.SeverityNumberTrace3
	case "Trace4":
		return plog.SeverityNumberTrace4
	case "Debug":
		return plog.SeverityNumberDebug
	case "Debug2":
		return plog.SeverityNumberDebug2
	case "Debug3":
		return plog.SeverityNumberDebug3
	case "Debug4":
		return plog.SeverityNumberDebug4
	case "Info":
		return plog.SeverityNumberInfo
	case "Info2":
		return plog.SeverityNumberInfo2
	case "Info3":
		return plog.SeverityNumberInfo3
	case "Info4":
		return plog.SeverityNumberInfo4
	case "Warn":
		return plog.SeverityNumberWarn
	case "Warn2":
		return plog.SeverityNumberWarn2
	case "Warn3":
		return plog.SeverityNumberWarn3
	case "Warn4":
		return plog.SeverityNumberWarn4
	case "Error":
		return plog.SeverityNumberError
	case "Error2":
		return plog.SeverityNumberError2
	case "Error3":
		return plog.SeverityNumberError3
	case "Error4":
		return plog.SeverityNumberError4
	case "Fatal":
		return plog.SeverityNumberFatal
	case "Fatal2":
		return plog.SeverityNumberFatal2
	case "Fatal3":
		return plog.SeverityNumberFatal3
	case "Fatal4":
		return plog.SeverityNumberFatal4
	default:
		// NOTE(prozlach): Not sure about this, but OTOH there is no other way
		// to represent malformed SeverityText field. Dunno if spec forsees
		// such a case.
		return plog.SeverityNumberUnspecified
	}
}
