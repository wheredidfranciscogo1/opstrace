package common

import (
	"fmt"
	"strconv"

	"go.opentelemetry.io/collector/pdata/pcommon"
)

const (
	ProjectIDUnknown                   = "unknown"
	ProjectIDHeader                    = "gitlab.target_project_id"
	NamespaceIDHeader                  = "gitlab.target_namespace_id"
	MetricAggregationTemporalityHeader = "otel.aggregation_temporality"
)

func GetProjectID(attributes map[string]string) string {
	if _, ok := attributes[ProjectIDHeader]; ok {
		return attributes[ProjectIDHeader]
	}
	return ProjectIDUnknown
}

func GetNamespaceID(attributes map[string]string) (int64, error) {
	if _, ok := attributes[NamespaceIDHeader]; ok {
		parsed, err := strconv.ParseInt(attributes[NamespaceIDHeader], 10, 64)
		if err != nil {
			return 0, fmt.Errorf("parsing namespaceID as integer: %w", err)
		}
		return parsed, nil
	}
	return 0, nil
}

func AttributesToMap(attributes pcommon.Map) map[string]string {
	m := make(map[string]string, attributes.Len())
	attributes.Range(func(k string, v pcommon.Value) bool {
		m[k] = v.AsString()
		return true
	})
	return m
}

func SpanIDToFixedString(id pcommon.SpanID) string {
	bytes := make([]byte, 8)
	if !id.IsEmpty() {
		copy(bytes, id[:])
	}
	return string(bytes)
}

func TraceIDToFixedString(id pcommon.TraceID) string {
	bytes := make([]byte, 16)
	if !id.IsEmpty() {
		copy(bytes, id[:])
	}
	return string(bytes)
}
