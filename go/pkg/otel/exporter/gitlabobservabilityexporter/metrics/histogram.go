package metrics

import (
	"context"
	"fmt"
	"time"

	"github.com/ClickHouse/clickhouse-go/v2"
	"go.opentelemetry.io/collector/pdata/pcommon"
	"go.opentelemetry.io/collector/pdata/pmetric"
	"go.uber.org/zap"

	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/constants"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/otel/exporter/gitlabobservabilityexporter/common"
)

type histogramModel struct {
	metricName        string
	metricDescription string
	metricUnit        string
	metadata          *MetricsMetadata
	histogram         pmetric.Histogram
}

type histogramMetrics struct {
	count          int
	histogramModel []*histogramModel
}

var _ MetricsModel = (*histogramMetrics)(nil)

func (h *histogramMetrics) Add(
	resAttr map[string]string,
	resURL string,
	scopeInstr pcommon.InstrumentationScope,
	scopeURL string,
	m pmetric.Metric,
) {
	h.count += m.Histogram().DataPoints().Len()
	h.histogramModel = append(h.histogramModel, &histogramModel{
		metricName:        m.Name(),
		metricDescription: m.Description(),
		metricUnit:        m.Unit(),
		metadata: &MetricsMetadata{
			ResAttr:    resAttr,
			ResURL:     resURL,
			ScopeURL:   scopeURL,
			ScopeInstr: scopeInstr,
		},
		histogram: m.Histogram(),
	})
}

const (
	insertHistogramTableSQL = `INSERT INTO %s.%s (
		ProjectId,
		NamespaceId,
		Fingerprint,
		IngestionTimestamp,
		ResourceAttributes,
		ResourceSchemaUrl,
		ScopeName,
		ScopeVersion,
		ScopeAttributes,
		ScopeDroppedAttrCount,
		ScopeSchemaUrl,
		MetricName,
		MetricDescription,
		MetricUnit,
		Attributes,
		StartTimeUnix,
		TimeUnix,
		Count,
		Sum,
		BucketCounts,
		ExplicitBounds,
		Exemplars.FilteredAttributes,
		Exemplars.TimeUnix,
		Exemplars.Value,
		Exemplars.SpanId,
		Exemplars.TraceId,
		Flags,
		AggTemp,
		Min,
		Max
	)`
)

func (h *histogramMetrics) write(ctx context.Context, db clickhouse.Conn, logger *zap.Logger) error {
	if h.count == 0 {
		return nil // nothing to do
	}

	batch, err := db.PrepareBatch(
		ctx,
		fmt.Sprintf(insertHistogramTableSQL, constants.MetricsDatabaseName, constants.MetricsHistogramTableName),
	)
	if err != nil {
		return fmt.Errorf("preparing histogram metrics batch: %w", err)
	}

	for _, model := range h.histogramModel {
		temporality := model.histogram.AggregationTemporality()

		for i := 0; i < model.histogram.DataPoints().Len(); i++ {
			dp := model.histogram.DataPoints().At(i)

			attributes := common.AttributesToMap(dp.Attributes())
			projectID := common.GetProjectID(attributes)
			namespaceID, err := common.GetNamespaceID(attributes)
			if err != nil {
				logger.Error("collecting namespaceID from attributes: ", zap.Error(err))
			}
			// add temporality within metric attributes to add to the fingerprint
			attributes[common.MetricAggregationTemporalityHeader] = temporality.String()
			fingerprint := GetMetricFingerprint(model.metricName, attributes)
			// delete additionally added headers before persisting
			delete(attributes, common.ProjectIDHeader)
			delete(attributes, common.NamespaceIDHeader)
			delete(attributes, common.MetricAggregationTemporalityHeader)

			attrs, times, values, traceIDs, spanIDs := convertExemplars(dp.Exemplars())
			if err := batch.Append(
				projectID,
				namespaceID,
				fingerprint,
				pcommon.NewTimestampFromTime(time.Now()).AsTime(),
				model.metadata.ResAttr,
				model.metadata.ResURL,
				model.metadata.ScopeInstr.Name(),
				model.metadata.ScopeInstr.Version(),
				common.AttributesToMap(model.metadata.ScopeInstr.Attributes()),
				model.metadata.ScopeInstr.DroppedAttributesCount(),
				model.metadata.ScopeURL,
				model.metricName,
				model.metricDescription,
				model.metricUnit,
				attributes,
				dp.StartTimestamp().AsTime(),
				dp.Timestamp().AsTime(),
				dp.Count(),
				dp.Sum(),
				convertSliceToArraySet(dp.BucketCounts().AsRaw()),
				convertSliceToArraySet(dp.ExplicitBounds().AsRaw()),
				attrs,
				times,
				values,
				spanIDs,
				traceIDs,
				uint32(dp.Flags()),
				int32(temporality),
				dp.Min(),
				dp.Max(),
			); err != nil {
				logger.Debug("error appending histogram to batch", zap.Error(err))
				continue
			}
		}
	}

	if err := batch.Send(); err != nil {
		return fmt.Errorf("sending histogram metrics batch: %w", err)
	}
	return nil
}
