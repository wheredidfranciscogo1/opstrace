package metrics

import (
	"context"
	"fmt"
	"time"

	"github.com/ClickHouse/clickhouse-go/v2"
	"go.opentelemetry.io/collector/pdata/pcommon"
	"go.opentelemetry.io/collector/pdata/pmetric"
	"go.uber.org/zap"

	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/constants"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/otel/exporter/gitlabobservabilityexporter/common"
)

type sumModel struct {
	metricName        string
	metricDescription string
	metricUnit        string
	metadata          *MetricsMetadata
	sum               pmetric.Sum
}

type sumMetrics struct {
	count    int
	sumModel []*sumModel
}

var _ MetricsModel = (*sumMetrics)(nil)

func (s *sumMetrics) Add(
	resAttr map[string]string,
	resURL string,
	scopeInstr pcommon.InstrumentationScope,
	scopeURL string,
	m pmetric.Metric,
) {
	s.count += m.Sum().DataPoints().Len()
	s.sumModel = append(s.sumModel, &sumModel{
		metricName:        m.Name(),
		metricDescription: m.Description(),
		metricUnit:        m.Unit(),
		metadata: &MetricsMetadata{
			ResAttr:    resAttr,
			ResURL:     resURL,
			ScopeURL:   scopeURL,
			ScopeInstr: scopeInstr,
		},
		sum: m.Sum(),
	})
}

const (
	insertSumTableSQL = `INSERT INTO %s.%s (
		ProjectId,
		NamespaceId,
		Fingerprint,
		IngestionTimestamp,
		ResourceAttributes,
		ResourceSchemaUrl,
		ScopeName,
		ScopeVersion,
		ScopeAttributes,
		ScopeDroppedAttrCount,
		ScopeSchemaUrl,
		MetricName,
		MetricDescription,
		MetricUnit,
		Attributes,
		StartTimeUnix,
		TimeUnix,
		Value,
		Flags,
		Exemplars.FilteredAttributes,
		Exemplars.TimeUnix,
		Exemplars.Value,
		Exemplars.SpanId,
		Exemplars.TraceId,
		AggTemp,
		IsMonotonic
	)`
)

func (s *sumMetrics) write(ctx context.Context, db clickhouse.Conn, logger *zap.Logger) error {
	if s.count == 0 {
		return nil // nothing to do
	}

	batch, err := db.PrepareBatch(
		ctx,
		fmt.Sprintf(insertSumTableSQL, constants.MetricsDatabaseName, constants.MetricsSumTableName),
	)
	if err != nil {
		return fmt.Errorf("preparing sum metrics batch: %w", err)
	}

	for _, model := range s.sumModel {
		temporality := model.sum.AggregationTemporality()

		for i := 0; i < model.sum.DataPoints().Len(); i++ {
			dp := model.sum.DataPoints().At(i)

			attributes := common.AttributesToMap(dp.Attributes())
			projectID := common.GetProjectID(attributes)
			namespaceID, err := common.GetNamespaceID(attributes)
			if err != nil {
				logger.Error("collecting namespaceID from attributes: ", zap.Error(err))
			}
			// add temporality within metric attributes to add to the fingerprint
			attributes[common.MetricAggregationTemporalityHeader] = temporality.String()
			fingerprint := GetMetricFingerprint(model.metricName, attributes)
			// delete additionally added headers before persisting
			delete(attributes, common.ProjectIDHeader)
			delete(attributes, common.NamespaceIDHeader)
			delete(attributes, common.MetricAggregationTemporalityHeader)

			attrs, times, values, traceIDs, spanIDs := convertExemplars(dp.Exemplars())
			if err := batch.Append(
				projectID,
				namespaceID,
				fingerprint,
				pcommon.NewTimestampFromTime(time.Now()).AsTime(),
				model.metadata.ResAttr,
				model.metadata.ResURL,
				model.metadata.ScopeInstr.Name(),
				model.metadata.ScopeInstr.Version(),
				common.AttributesToMap(model.metadata.ScopeInstr.Attributes()),
				model.metadata.ScopeInstr.DroppedAttributesCount(),
				model.metadata.ScopeURL,
				model.metricName,
				model.metricDescription,
				model.metricUnit,
				attributes,
				dp.StartTimestamp().AsTime(),
				dp.Timestamp().AsTime(),
				getValue(dp.IntValue(), dp.DoubleValue(), dp.ValueType()),
				uint32(dp.Flags()),
				attrs,
				times,
				values,
				spanIDs,
				traceIDs,
				int32(temporality),
				model.sum.IsMonotonic(),
			); err != nil {
				logger.Debug("error appending sum to batch", zap.Error(err))
				continue
			}
		}
	}

	if err := batch.Send(); err != nil {
		return fmt.Errorf("sending sum metrics batch: %w", err)
	}
	return nil
}
