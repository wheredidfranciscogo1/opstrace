package metrics

import (
	"github.com/ClickHouse/clickhouse-go/v2"
	"github.com/prometheus/common/model"
	"go.opentelemetry.io/collector/pdata/pmetric"

	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/otel/exporter/gitlabobservabilityexporter/common"
)

const (
	ProjectIDLabel  string = "__projectid__"
	MetricNameLabel string = "__name__"
)

func convertExemplars(exemplars pmetric.ExemplarSlice) (
	attrs clickhouse.ArraySet,
	times clickhouse.ArraySet,
	values clickhouse.ArraySet,
	traceIDs clickhouse.ArraySet,
	spanIDs clickhouse.ArraySet,
) {
	for i := 0; i < exemplars.Len(); i++ {
		exemplar := exemplars.At(i)
		attrs = append(attrs, common.AttributesToMap(exemplar.FilteredAttributes()))
		times = append(times, exemplar.Timestamp().AsTime())
		values = append(values, getValue(exemplar.IntValue(), exemplar.DoubleValue(), exemplar.ValueType()))
		traceIDs = append(traceIDs, common.TraceIDToFixedString(exemplar.TraceID()))
		spanIDs = append(spanIDs, common.SpanIDToFixedString(exemplar.SpanID()))
	}
	return attrs, times, values, traceIDs, spanIDs
}

func GetMetricFingerprint(metricName string, attributes map[string]string) string {
	// for now, we rely on how Prometheus computes a fingerprint from
	// a set of metric-labels, which internally uses fnv64a hashing
	labelSet := model.LabelSet{}
	for k, v := range attributes {
		labelSet[model.LabelName(k)] = model.LabelValue(v)
	}
	// add projectID, metricName to the attributes to get metric fingerprints
	labelSet[model.LabelName(ProjectIDLabel)] = model.LabelValue(common.GetProjectID(attributes))
	labelSet[model.MetricNameLabel] = model.LabelValue(metricName)
	return labelSet.Fingerprint().String()
}

// https://github.com/open-telemetry/opentelemetry-proto/blob/main/opentelemetry/proto/metrics/v1/metrics.proto#L358
// define two types for one datapoint value, clickhouse only use one value of float64 to store them
func getValue(intValue int64, floatValue float64, dataType any) float64 {
	switch t := dataType.(type) {
	case pmetric.ExemplarValueType:
		switch t {
		case pmetric.ExemplarValueTypeDouble:
			return floatValue
		case pmetric.ExemplarValueTypeInt:
			return float64(intValue)
		case pmetric.ExemplarValueTypeEmpty:
			return 0.0
		default:
			return 0.0
		}
	case pmetric.NumberDataPointValueType:
		switch t {
		case pmetric.NumberDataPointValueTypeDouble:
			return floatValue
		case pmetric.NumberDataPointValueTypeInt:
			return float64(intValue)
		case pmetric.NumberDataPointValueTypeEmpty:
			return 0.0
		default:
			return 0.0
		}
	default:
		return 0.0
	}
}

func convertSliceToArraySet[T any](slice []T) clickhouse.ArraySet {
	var set clickhouse.ArraySet
	for _, item := range slice {
		set = append(set, item)
	}
	return set
}
