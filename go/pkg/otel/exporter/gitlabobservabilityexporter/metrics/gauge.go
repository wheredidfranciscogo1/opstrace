package metrics

import (
	"context"
	"fmt"
	"time"

	"github.com/ClickHouse/clickhouse-go/v2"
	"go.opentelemetry.io/collector/pdata/pcommon"
	"go.opentelemetry.io/collector/pdata/pmetric"
	"go.uber.org/zap"

	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/constants"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/otel/exporter/gitlabobservabilityexporter/common"
)

type gaugeModel struct {
	metricName        string
	metricDescription string
	metricUnit        string
	metadata          *MetricsMetadata
	gauge             pmetric.Gauge
}

type gaugeMetrics struct {
	count       int
	gaugeModels []*gaugeModel
}

var _ MetricsModel = (*gaugeMetrics)(nil)

func (g *gaugeMetrics) Add(
	resAttr map[string]string,
	resURL string,
	scopeInstr pcommon.InstrumentationScope,
	scopeURL string,
	m pmetric.Metric,
) {
	g.count = m.Gauge().DataPoints().Len()
	g.gaugeModels = append(g.gaugeModels, &gaugeModel{
		metricName:        m.Name(),
		metricDescription: m.Description(),
		metricUnit:        m.Unit(),
		metadata: &MetricsMetadata{
			ResAttr:    resAttr,
			ResURL:     resURL,
			ScopeInstr: scopeInstr,
			ScopeURL:   scopeURL,
		},
		gauge: m.Gauge(),
	})
}

const (
	insertGaugeTableSQL = `INSERT INTO %s.%s (
		ProjectId,
		NamespaceId,
		Fingerprint,
		IngestionTimestamp,
		ResourceAttributes,
		ResourceSchemaUrl,
		ScopeName,
		ScopeVersion,
		ScopeAttributes,
		ScopeDroppedAttrCount,
		ScopeSchemaUrl,
		MetricName,
		MetricDescription,
		MetricUnit,
		Attributes,
		StartTimeUnix,
		TimeUnix,
		Value,
		Flags,
		Exemplars.FilteredAttributes,
		Exemplars.TimeUnix,
		Exemplars.Value,
		Exemplars.SpanId,
		Exemplars.TraceId
	)`
)

func (g *gaugeMetrics) write(ctx context.Context, db clickhouse.Conn, logger *zap.Logger) error {
	if g.count == 0 {
		return nil // nothing to do
	}

	batch, err := db.PrepareBatch(
		ctx,
		fmt.Sprintf(insertGaugeTableSQL, constants.MetricsDatabaseName, constants.MetricsGaugeTableName),
	)
	if err != nil {
		return fmt.Errorf("preparing gauge metrics batch: %w", err)
	}

	for _, model := range g.gaugeModels {
		for i := 0; i < model.gauge.DataPoints().Len(); i++ {
			dp := model.gauge.DataPoints().At(i)

			attributes := common.AttributesToMap(dp.Attributes())
			projectID := common.GetProjectID(attributes)
			namespaceID, err := common.GetNamespaceID(attributes)
			if err != nil {
				logger.Error("collecting namespaceID from attributes: ", zap.Error(err))
			}
			fingerprint := GetMetricFingerprint(model.metricName, attributes)
			delete(attributes, common.ProjectIDHeader) // delete projectID header before persisting
			delete(attributes, common.NamespaceIDHeader)

			attrs, times, values, traceIDs, spanIDs := convertExemplars(dp.Exemplars())
			err = batch.Append(
				projectID,
				namespaceID,
				fingerprint,
				pcommon.NewTimestampFromTime(time.Now()).AsTime(),
				model.metadata.ResAttr,
				model.metadata.ResURL,
				model.metadata.ScopeInstr.Name(),
				model.metadata.ScopeInstr.Version(),
				common.AttributesToMap(model.metadata.ScopeInstr.Attributes()),
				model.metadata.ScopeInstr.DroppedAttributesCount(),
				model.metadata.ScopeURL,
				model.metricName,
				model.metricDescription,
				model.metricUnit,
				attributes,
				dp.StartTimestamp().AsTime(),
				dp.Timestamp().AsTime(),
				getValue(dp.IntValue(), dp.DoubleValue(), dp.ValueType()),
				uint32(dp.Flags()),
				attrs,
				times,
				values,
				spanIDs,
				traceIDs,
			)
			if err != nil {
				logger.Debug("error appending gauge to batch", zap.Error(err))
				continue
			}
		}
	}

	if err := batch.Send(); err != nil {
		return fmt.Errorf("sending gauge metrics batch: %w", err)
	}
	return nil
}
