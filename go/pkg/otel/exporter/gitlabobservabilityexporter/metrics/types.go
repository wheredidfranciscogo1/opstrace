package metrics

import (
	"context"

	"github.com/ClickHouse/clickhouse-go/v2"
	"go.opentelemetry.io/collector/pdata/pcommon"
	"go.opentelemetry.io/collector/pdata/pmetric"
	"go.uber.org/zap"
)

type MetricsModel interface {
	Add(
		resAttr map[string]string,
		resURL string,
		scopeInstr pcommon.InstrumentationScope,
		scopeURL string,
		m pmetric.Metric,
	)

	write(
		ctx context.Context,
		db clickhouse.Conn,
		logger *zap.Logger,
	) error
}

// MetricsMetaData  contain specific metric data
type MetricsMetadata struct {
	ResAttr    map[string]string
	ResURL     string
	ScopeURL   string
	ScopeInstr pcommon.InstrumentationScope
}
