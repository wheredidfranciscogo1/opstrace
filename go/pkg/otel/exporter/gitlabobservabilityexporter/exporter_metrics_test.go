package gitlabobservabilityexporter

import (
	"context"
	"encoding/hex"
	"fmt"
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
	"go.opentelemetry.io/collector/pdata/pcommon"
	"go.opentelemetry.io/collector/pdata/pmetric"
	conventions "go.opentelemetry.io/collector/semconv/v1.18.0"
	"go.uber.org/zap/zaptest"

	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/constants"
	metricsmigrations "gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/db/migrations/metrics"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/otel/exporter/gitlabobservabilityexporter/common"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/testutils"
)

type metricsDatapoint interface {
	getExemplarComponents() (map[string]string, float64, []byte, []byte)
}

const readGaugeMetricsTmpl string = `
SELECT
	ProjectId,
	MetricName,
	MetricDescription,
	MetricUnit,
	Attributes,
	StartTimeUnix,
	TimeUnix,
	Value,
	Exemplars.FilteredAttributes,
	Exemplars.Value,
	Exemplars.SpanId,
	Exemplars.TraceId
FROM
	%s.%s
`

type gaugeMetricsDataRow struct {
	ProjectID         string              `ch:"ProjectId"`
	MetricName        string              `ch:"MetricName"`
	MetricDescription string              `ch:"MetricDescription"`
	MetricUnit        string              `ch:"MetricUnit"`
	Attributes        map[string]string   `ch:"Attributes"`
	StartTimeUnix     time.Time           `ch:"StartTimeUnix"`
	TimeUnix          time.Time           `ch:"TimeUnix"`
	Value             float64             `ch:"Value"`
	ExemplarAttrs     []map[string]string `ch:"Exemplars.FilteredAttributes"`
	ExemplarValues    []float64           `ch:"Exemplars.Value"`
	ExemplarSpanIDs   [][]byte            `ch:"Exemplars.SpanId"`
	ExemplarTraceIDs  [][]byte            `ch:"Exemplars.TraceId"`
}

var _ metricsDatapoint = (*gaugeMetricsDataRow)(nil)

func (g *gaugeMetricsDataRow) getExemplarComponents() (map[string]string, float64, []byte, []byte) {
	return g.ExemplarAttrs[0], g.ExemplarValues[0], g.ExemplarSpanIDs[0], g.ExemplarTraceIDs[0]
}

const readSumMetricsTmpl string = `
SELECT
	ProjectId,
	MetricName,
	MetricDescription,
	MetricUnit,
	Attributes,
	StartTimeUnix,
	TimeUnix,
	Value,
	Exemplars.FilteredAttributes,
	Exemplars.Value,
	Exemplars.SpanId,
	Exemplars.TraceId,
	AggTemp
FROM
	%s.%s
`

type sumMetricsDataRow struct {
	ProjectID         string              `ch:"ProjectId"`
	MetricName        string              `ch:"MetricName"`
	MetricDescription string              `ch:"MetricDescription"`
	MetricUnit        string              `ch:"MetricUnit"`
	Attributes        map[string]string   `ch:"Attributes"`
	StartTimeUnix     time.Time           `ch:"StartTimeUnix"`
	TimeUnix          time.Time           `ch:"TimeUnix"`
	Value             float64             `ch:"Value"`
	ExemplarAttrs     []map[string]string `ch:"Exemplars.FilteredAttributes"`
	ExemplarValues    []float64           `ch:"Exemplars.Value"`
	ExemplarSpanIDs   [][]byte            `ch:"Exemplars.SpanId"`
	ExemplarTraceIDs  [][]byte            `ch:"Exemplars.TraceId"`
	AggTemp           int32               `ch:"AggTemp"`
}

var _ metricsDatapoint = (*sumMetricsDataRow)(nil)

func (g *sumMetricsDataRow) getExemplarComponents() (map[string]string, float64, []byte, []byte) {
	return g.ExemplarAttrs[0], g.ExemplarValues[0], g.ExemplarSpanIDs[0], g.ExemplarTraceIDs[0]
}

const readHistogramMetricsTmpl string = `
SELECT
	ProjectId,
	MetricName,
	MetricDescription,
	Attributes,
	StartTimeUnix,
	TimeUnix,
	Count,
	Sum,
	Min,
	Max,
	BucketCounts,
	ExplicitBounds,
	Exemplars.FilteredAttributes,
	Exemplars.Value,
	Exemplars.SpanId,
	Exemplars.TraceId,
	AggTemp
FROM
	%s.%s
`

type histogramMetricsDataRow struct {
	ProjectID         string              `ch:"ProjectId"`
	MetricName        string              `ch:"MetricName"`
	MetricDescription string              `ch:"MetricDescription"`
	Attributes        map[string]string   `ch:"Attributes"`
	StartTimeUnix     time.Time           `ch:"StartTimeUnix"`
	TimeUnix          time.Time           `ch:"TimeUnix"`
	Count             uint64              `ch:"Count"`
	Sum               float64             `ch:"Sum"`
	Min               float64             `ch:"Min"`
	Max               float64             `ch:"Max"`
	BucketCounts      []uint64            `ch:"BucketCounts"`
	ExplicitBounds    []float64           `ch:"ExplicitBounds"`
	ExemplarAttrs     []map[string]string `ch:"Exemplars.FilteredAttributes"`
	ExemplarValues    []float64           `ch:"Exemplars.Value"`
	ExemplarSpanIDs   [][]byte            `ch:"Exemplars.SpanId"`
	ExemplarTraceIDs  [][]byte            `ch:"Exemplars.TraceId"`
	AggTemp           int32               `ch:"AggTemp"`
}

var _ metricsDatapoint = (*histogramMetricsDataRow)(nil)

func (g *histogramMetricsDataRow) getExemplarComponents() (map[string]string, float64, []byte, []byte) {
	return g.ExemplarAttrs[0], g.ExemplarValues[0], g.ExemplarSpanIDs[0], g.ExemplarTraceIDs[0]
}

const readExponentialHistogramMetricsTmpl string = `
SELECT
	ProjectId,
	MetricName,
	MetricDescription,
	Attributes,
	StartTimeUnix,
	TimeUnix,
	Count,
	Sum,
	Min,
	Max,
	ZeroCount,
	PositiveOffset,
	PositiveBucketCounts,
	NegativeOffset,
	NegativeBucketCounts,
	Exemplars.FilteredAttributes,
	Exemplars.Value,
	Exemplars.SpanId,
	Exemplars.TraceId,
	AggTemp
FROM
	%s.%s
`

type exponentialHistogramMetricsDataRow struct {
	ProjectID            string              `ch:"ProjectId"`
	MetricName           string              `ch:"MetricName"`
	MetricDescription    string              `ch:"MetricDescription"`
	Attributes           map[string]string   `ch:"Attributes"`
	StartTimeUnix        time.Time           `ch:"StartTimeUnix"`
	TimeUnix             time.Time           `ch:"TimeUnix"`
	Count                uint64              `ch:"Count"`
	Sum                  float64             `ch:"Sum"`
	Min                  float64             `ch:"Min"`
	Max                  float64             `ch:"Max"`
	ZeroCount            uint64              `ch:"ZeroCount"`
	PositiveOffset       int32               `ch:"PositiveOffset"`
	PositiveBucketCounts []uint64            `ch:"PositiveBucketCounts"`
	NegativeOffset       int32               `ch:"NegativeOffset"`
	NegativeBucketCounts []uint64            `ch:"NegativeBucketCounts"`
	ExemplarAttrs        []map[string]string `ch:"Exemplars.FilteredAttributes"`
	ExemplarValues       []float64           `ch:"Exemplars.Value"`
	ExemplarSpanIDs      [][]byte            `ch:"Exemplars.SpanId"`
	ExemplarTraceIDs     [][]byte            `ch:"Exemplars.TraceId"`
	AggTemp              int32               `ch:"AggTemp"`
}

var _ metricsDatapoint = (*exponentialHistogramMetricsDataRow)(nil)

func (g *exponentialHistogramMetricsDataRow) getExemplarComponents() (map[string]string, float64, []byte, []byte) {
	return g.ExemplarAttrs[0], g.ExemplarValues[0], g.ExemplarSpanIDs[0], g.ExemplarTraceIDs[0]
}

func TestPushMetricsData(t *testing.T) {
	// ensure the tests run within the stipulated timeout
	ctx, cancel := context.WithTimeout(context.Background(), 180*time.Second)
	defer cancel()

	testEnv, err := testutils.NewClickHouseServer(ctx)
	require.NoError(t, err)
	// ensure cleaning up the container afterwards
	defer testEnv.Container.Terminate(ctx)

	dsn := testEnv.GetDSN()
	t.Run("test gauge metrics insertion", func(t *testing.T) {
		// create necessary database/tables
		err := testEnv.CreateDatabase(ctx, constants.MetricsDatabaseName)
		require.NoError(t, err)
		// generate table schemas
		schema, err := (&metricsmigrations.GaugeMain{}).Render(
			metricsmigrations.MigrationData{
				DatabaseName:      constants.MetricsDatabaseName,
				TableName:         constants.MetricsGaugeTableName,
				SelfHostedVersion: true,
			},
		)
		require.NoError(t, err)
		// create tables
		err = testEnv.Run(ctx, schema)
		require.NoError(t, err)
		// setup exporter, configure database
		exporter := newMetricsExporterInternal(t, ctx, fmt.Sprintf("%s/%s", dsn, constants.MetricsDatabaseName))
		// ensure data was inserted correctly
		err = exporter.pushMetricsData(ctx, generateGaugeMetrics(1))
		require.NoError(t, err)
		// test table state/data
		conn, err := testEnv.GetConnection()
		require.NoError(t, err)
		rows, err := conn.Query(
			ctx,
			fmt.Sprintf(readGaugeMetricsTmpl, constants.MetricsDatabaseName, constants.MetricsGaugeTableName),
		)
		require.NoError(t, err)
		var rowsFound int
		for rows.Next() {
			rowsFound++
			var r gaugeMetricsDataRow
			if err := rows.ScanStruct(&r); err != nil {
				t.Error(err)
			}
			assert.Equal(t, randomProjectID, r.ProjectID)
			assert.Equal(t, "random_gauge_metric", r.MetricName)
			assert.Equal(t, "Random description for a random gauge metric", r.MetricDescription)
			assert.Equal(t, float64(10), r.Value)
			assert.Equal(t, "app.foo.local", r.Attributes["host.name"])

			ensureExemplars(t, &r)
		}
		assert.Equal(t, 1, rowsFound)
	})

	t.Run("test sum metrics insertion", func(t *testing.T) {
		// create necessary database/tables
		err := testEnv.CreateDatabase(ctx, constants.MetricsDatabaseName)
		require.NoError(t, err)
		// generate table schemas
		schema, err := (&metricsmigrations.SumMain{}).Render(
			metricsmigrations.MigrationData{
				DatabaseName:      constants.MetricsDatabaseName,
				TableName:         constants.MetricsSumTableName,
				SelfHostedVersion: true,
			},
		)
		require.NoError(t, err)
		// create tables
		err = testEnv.Run(ctx, schema)
		require.NoError(t, err)
		// setup exporter, configure database
		exporter := newMetricsExporterInternal(t, ctx, fmt.Sprintf("%s/%s", dsn, constants.MetricsDatabaseName))
		// ensure data was inserted correctly
		err = exporter.pushMetricsData(ctx, generateSumMetrics(1))
		require.NoError(t, err)
		// test table state/data
		conn, err := testEnv.GetConnection()
		require.NoError(t, err)
		rows, err := conn.Query(
			ctx,
			fmt.Sprintf(readSumMetricsTmpl, constants.MetricsDatabaseName, constants.MetricsSumTableName),
		)
		require.NoError(t, err)
		var rowsFound int
		for rows.Next() {
			rowsFound++
			var r sumMetricsDataRow
			if err := rows.ScanStruct(&r); err != nil {
				t.Error(err)
			}
			assert.Equal(t, randomProjectID, r.ProjectID)
			assert.Equal(t, "random_sum_metric", r.MetricName)
			assert.Equal(t, "Random description for a random sum metric", r.MetricDescription)
			assert.Equal(t, float64(100), r.Value)
			assert.Equal(t, "app.foo.local", r.Attributes["host.name"])
			assert.Equal(t, int32(2), r.AggTemp)

			ensureExemplars(t, &r)
		}
		assert.Equal(t, 1, rowsFound)
	})

	t.Run("test histogram metrics insertion", func(t *testing.T) {
		// create necessary database/tables
		err := testEnv.CreateDatabase(ctx, constants.MetricsDatabaseName)
		require.NoError(t, err)
		// generate table schemas
		schema, err := (&metricsmigrations.HistMain{}).Render(
			metricsmigrations.MigrationData{
				DatabaseName:      constants.MetricsDatabaseName,
				TableName:         constants.MetricsHistogramTableName,
				SelfHostedVersion: true,
			},
		)
		require.NoError(t, err)
		// create tables
		err = testEnv.Run(ctx, schema)
		require.NoError(t, err)
		// setup exporter, configure database
		exporter := newMetricsExporterInternal(t, ctx, fmt.Sprintf("%s/%s", dsn, constants.MetricsDatabaseName))
		// ensure data was inserted correctly
		err = exporter.pushMetricsData(ctx, generateHistogramMetrics(1))
		require.NoError(t, err)
		// test table state/data
		conn, err := testEnv.GetConnection()
		require.NoError(t, err)
		rows, err := conn.Query(
			ctx,
			fmt.Sprintf(readHistogramMetricsTmpl, constants.MetricsDatabaseName, constants.MetricsHistogramTableName),
		)
		require.NoError(t, err)
		var rowsFound int
		for rows.Next() {
			rowsFound++
			var r histogramMetricsDataRow
			if err := rows.ScanStruct(&r); err != nil {
				t.Error(err)
			}
			assert.Equal(t, randomProjectID, r.ProjectID)
			assert.Equal(t, "random_histogram_metric", r.MetricName)
			assert.Equal(t, "Random description for a random histogram metric", r.MetricDescription)
			assert.Equal(t, "app.foo.local", r.Attributes["host.name"])
			assert.Equal(t, uint64(1), r.Count)
			assert.Equal(t, float64(1), r.Sum)
			assert.Equal(t, float64(0), r.Min)
			assert.Equal(t, float64(1), r.Max)
			assert.Equal(t, []uint64{0, 0, 0, 1, 0}, r.BucketCounts)
			assert.Equal(t, []float64{0, 0, 0, 0, 0}, r.ExplicitBounds)
			assert.Equal(t, int32(2), r.AggTemp)

			ensureExemplars(t, &r)
		}
		assert.Equal(t, 1, rowsFound)
	})

	t.Run("test exponential histogram metrics insertion", func(t *testing.T) {
		// create necessary database/tables
		err := testEnv.CreateDatabase(ctx, constants.MetricsDatabaseName)
		require.NoError(t, err)
		// generate table schemas
		schema, err := (&metricsmigrations.ExpHistMain{}).Render(
			metricsmigrations.MigrationData{
				DatabaseName:      constants.MetricsDatabaseName,
				TableName:         constants.MetricsExponentialHistogramTableName,
				SelfHostedVersion: true,
			},
		)
		require.NoError(t, err)
		// create tables
		err = testEnv.Run(ctx, schema)
		require.NoError(t, err)
		// setup exporter, configure database
		exporter := newMetricsExporterInternal(t, ctx, fmt.Sprintf("%s/%s", dsn, constants.MetricsDatabaseName))
		// ensure data was inserted correctly
		err = exporter.pushMetricsData(ctx, generateExponentialHistogramMetrics(1))
		require.NoError(t, err)
		// test table state/data
		conn, err := testEnv.GetConnection()
		require.NoError(t, err)
		rows, err := conn.Query(
			ctx,
			fmt.Sprintf(
				readExponentialHistogramMetricsTmpl,
				constants.MetricsDatabaseName,
				constants.MetricsExponentialHistogramTableName,
			),
		)
		require.NoError(t, err)
		var rowsFound int
		for rows.Next() {
			rowsFound++
			var r exponentialHistogramMetricsDataRow
			if err := rows.ScanStruct(&r); err != nil {
				t.Error(err)
			}
			assert.Equal(t, randomProjectID, r.ProjectID)
			assert.Equal(t, "random_exponential_histogram_metric", r.MetricName)
			assert.Equal(t, "Random description for a random exponential histogram metric", r.MetricDescription)
			assert.Equal(t, "app.foo.local", r.Attributes["host.name"])
			assert.Equal(t, uint64(1), r.Count)
			assert.Equal(t, float64(1), r.Sum)
			assert.Equal(t, float64(0), r.Min)
			assert.Equal(t, float64(1), r.Max)
			assert.Equal(t, uint64(0), r.ZeroCount)
			assert.Equal(t, int32(1), r.PositiveOffset)
			assert.Equal(t, []uint64{0, 0, 0, 1, 0}, r.PositiveBucketCounts)
			assert.Equal(t, int32(1), r.NegativeOffset)
			assert.Equal(t, []uint64{0, 0, 0, 1, 0}, r.NegativeBucketCounts)
			assert.Equal(t, int32(2), r.AggTemp)

			ensureExemplars(t, &r)
		}
		assert.Equal(t, 1, rowsFound)
	})
}

func newMetricsExporterInternal(t *testing.T, ctx context.Context, dsn string) *metricsExporter {
	t.Helper()
	c := &config{ClickHouseDSN: dsn}
	exporter, err := newMetricsExporter(zaptest.NewLogger(t), c)
	require.NoError(t, err)
	require.NoError(t, exporter.start(ctx, nil))
	t.Cleanup(func() {
		_ = exporter.shutdown(ctx)
	})
	return exporter
}

func generateGaugeMetrics(count int) pmetric.Metrics {
	metrics := pmetric.NewMetrics()
	rm := metrics.ResourceMetrics().AppendEmpty()
	rm.Resource().Attributes().PutStr(conventions.AttributeServiceName, randomServiceName)
	rm.Resource().SetDroppedAttributesCount(10)
	rm.SetSchemaUrl("https://opentelemetry.io/schemas/1.4.0")

	sm := rm.ScopeMetrics().AppendEmpty()
	sm.Scope().SetName("io.opentelemetry.contrib.clickhouse")
	sm.Scope().SetVersion("1.0.0")
	sm.SetSchemaUrl("https://opentelemetry.io/schemas/1.4.0")
	sm.Scope().SetDroppedAttributesCount(10)
	sm.Scope().Attributes().PutStr("lib", "clickhouse")

	for i := 0; i < count; i++ {
		m := sm.Metrics().AppendEmpty()
		m.SetName("random_gauge_metric")
		m.SetUnit("count")
		m.SetDescription("Random description for a random gauge metric")

		dp := m.SetEmptyGauge().DataPoints().AppendEmpty()
		dp.SetDoubleValue(float64(10))
		dp.Attributes().PutStr(common.ProjectIDHeader, randomProjectID)
		dp.Attributes().PutStr("host.name", "app.foo.local")
		dp.SetStartTimestamp(pcommon.NewTimestampFromTime(time.Now()))
		dp.SetTimestamp(pcommon.NewTimestampFromTime(time.Now()))

		exemplars := dp.Exemplars().AppendEmpty()
		exemplars.SetIntValue(10)
		exemplars.FilteredAttributes().PutStr("key", "value")
		exemplars.SetTraceID(randomTraceID)
		exemplars.SetSpanID(randomSpanID)
	}

	return metrics
}

func generateSumMetrics(count int) pmetric.Metrics {
	metrics := pmetric.NewMetrics()
	rm := metrics.ResourceMetrics().AppendEmpty()
	rm.Resource().Attributes().PutStr(conventions.AttributeServiceName, randomServiceName)
	rm.Resource().SetDroppedAttributesCount(10)
	rm.SetSchemaUrl("https://opentelemetry.io/schemas/1.4.0")

	sm := rm.ScopeMetrics().AppendEmpty()
	sm.Scope().SetName("io.opentelemetry.contrib.clickhouse")
	sm.Scope().SetVersion("1.0.0")
	sm.SetSchemaUrl("https://opentelemetry.io/schemas/1.4.0")
	sm.Scope().SetDroppedAttributesCount(10)
	sm.Scope().Attributes().PutStr("lib", "clickhouse")

	for i := 0; i < count; i++ {
		m := sm.Metrics().AppendEmpty()
		m.SetName("random_sum_metric")
		m.SetUnit("count")
		m.SetDescription("Random description for a random sum metric")

		dp := m.SetEmptySum().DataPoints().AppendEmpty()
		dp.SetDoubleValue(float64(100))
		dp.Attributes().PutStr(common.ProjectIDHeader, randomProjectID)
		dp.Attributes().PutStr("host.name", "app.foo.local")
		dp.SetStartTimestamp(pcommon.NewTimestampFromTime(time.Now()))
		dp.SetTimestamp(pcommon.NewTimestampFromTime(time.Now()))

		exemplars := dp.Exemplars().AppendEmpty()
		exemplars.SetIntValue(10)
		exemplars.FilteredAttributes().PutStr("key", "value")
		exemplars.SetTraceID(randomTraceID)
		exemplars.SetSpanID(randomSpanID)

		m.Sum().SetAggregationTemporality(pmetric.AggregationTemporalityCumulative)
	}

	return metrics
}

func generateHistogramMetrics(count int) pmetric.Metrics {
	metrics := pmetric.NewMetrics()
	rm := metrics.ResourceMetrics().AppendEmpty()
	rm.Resource().Attributes().PutStr(conventions.AttributeServiceName, randomServiceName)
	rm.Resource().SetDroppedAttributesCount(10)
	rm.SetSchemaUrl("https://opentelemetry.io/schemas/1.4.0")

	sm := rm.ScopeMetrics().AppendEmpty()
	sm.Scope().SetName("io.opentelemetry.contrib.clickhouse")
	sm.Scope().SetVersion("1.0.0")
	sm.SetSchemaUrl("https://opentelemetry.io/schemas/1.4.0")
	sm.Scope().SetDroppedAttributesCount(10)
	sm.Scope().Attributes().PutStr("lib", "clickhouse")

	for i := 0; i < count; i++ {
		m := sm.Metrics().AppendEmpty()
		m.SetName("random_histogram_metric")
		m.SetDescription("Random description for a random histogram metric")

		dp := m.SetEmptyHistogram().DataPoints().AppendEmpty()
		dp.SetCount(1)
		dp.SetSum(1)
		dp.ExplicitBounds().FromRaw([]float64{0, 0, 0, 0, 0})
		dp.BucketCounts().FromRaw([]uint64{0, 0, 0, 1, 0})
		dp.SetMin(0)
		dp.SetMax(1)
		dp.Attributes().PutStr(common.ProjectIDHeader, randomProjectID)
		dp.Attributes().PutStr("host.name", "app.foo.local")
		dp.SetStartTimestamp(pcommon.NewTimestampFromTime(time.Now()))
		dp.SetTimestamp(pcommon.NewTimestampFromTime(time.Now()))

		exemplars := dp.Exemplars().AppendEmpty()
		exemplars.SetIntValue(10)
		exemplars.FilteredAttributes().PutStr("key", "value")
		exemplars.SetTraceID(randomTraceID)
		exemplars.SetSpanID(randomSpanID)

		m.Histogram().SetAggregationTemporality(pmetric.AggregationTemporalityCumulative)
	}

	return metrics
}

func generateExponentialHistogramMetrics(count int) pmetric.Metrics {
	metrics := pmetric.NewMetrics()
	rm := metrics.ResourceMetrics().AppendEmpty()
	rm.Resource().Attributes().PutStr(conventions.AttributeServiceName, randomServiceName)
	rm.Resource().SetDroppedAttributesCount(10)
	rm.SetSchemaUrl("https://opentelemetry.io/schemas/1.4.0")

	sm := rm.ScopeMetrics().AppendEmpty()
	sm.Scope().SetName("io.opentelemetry.contrib.clickhouse")
	sm.Scope().SetVersion("1.0.0")
	sm.SetSchemaUrl("https://opentelemetry.io/schemas/1.4.0")
	sm.Scope().SetDroppedAttributesCount(10)
	sm.Scope().Attributes().PutStr("lib", "clickhouse")

	for i := 0; i < count; i++ {
		m := sm.Metrics().AppendEmpty()
		m.SetName("random_exponential_histogram_metric")
		m.SetDescription("Random description for a random exponential histogram metric")

		dp := m.SetEmptyExponentialHistogram().DataPoints().AppendEmpty()
		dp.SetCount(1)
		dp.SetSum(1)
		dp.SetMin(0)
		dp.SetMax(1)
		dp.SetZeroCount(0)
		dp.Negative().SetOffset(1)
		dp.Negative().BucketCounts().FromRaw([]uint64{0, 0, 0, 1, 0})
		dp.Positive().SetOffset(1)
		dp.Positive().BucketCounts().FromRaw([]uint64{0, 0, 0, 1, 0})
		dp.Attributes().PutStr(common.ProjectIDHeader, randomProjectID)
		dp.Attributes().PutStr("host.name", "app.foo.local")
		dp.SetStartTimestamp(pcommon.NewTimestampFromTime(time.Now()))
		dp.SetTimestamp(pcommon.NewTimestampFromTime(time.Now()))

		exemplars := dp.Exemplars().AppendEmpty()
		exemplars.SetIntValue(10)
		exemplars.FilteredAttributes().PutStr("key", "value")
		exemplars.SetTraceID(randomTraceID)
		exemplars.SetSpanID(randomSpanID)

		m.ExponentialHistogram().SetAggregationTemporality(pmetric.AggregationTemporalityCumulative)
	}

	return metrics
}

func ensureExemplars(t *testing.T, r metricsDatapoint) {
	t.Helper()

	attrs, value, spanID, traceID := r.getExemplarComponents()
	assert.Equal(t, map[string]string{"key": "value"}, attrs)
	assert.Equal(t, float64(10), value)
	assert.Equal(t, hex.EncodeToString(randomSpanID[:]), hex.EncodeToString(spanID))
	assert.Equal(t, hex.EncodeToString(randomTraceID[:]), hex.EncodeToString(traceID))
}
