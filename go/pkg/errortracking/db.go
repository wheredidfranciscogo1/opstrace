package errortracking

import (
	"context"
	"database/sql"
	"embed"
	"fmt"
	"time"

	"github.com/ClickHouse/clickhouse-go/v2"
	"gitlab.com/gitlab-org/opstrace/goose/v3"

	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/common"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/errortracking/gen/models"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/errortracking/gen/restapi/operations/errors"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/errortracking/gen/restapi/operations/messages"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/errortracking/gen/restapi/operations/projects"
	et "gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/errortracking/models"
)

type errorStatus uint8

const (
	errorUnresolved errorStatus = iota
	errorResolved
	errorIgnored
)

type errorStatusStr string

const (
	errorUnresolvedStr = "unresolved"
	errorResolvedStr   = "resolved"
	errorIgnoredStr    = "ignored"
)

var errorStatusToInt = map[errorStatusStr]errorStatus{
	errorUnresolvedStr: errorUnresolved,
	errorResolvedStr:   errorResolved,
	errorIgnoredStr:    errorIgnored,
}

var errorStatusToStr = map[errorStatus]errorStatusStr{
	errorUnresolved: errorUnresolvedStr,
	errorResolved:   errorResolvedStr,
	errorIgnored:    errorIgnoredStr,
}

const defaultListIssueLimit = 1000

// Embed all the files in the migrations directory.
//
//go:embed db/*
var dbMigrations embed.FS

type Insertable interface {
	AsInsertStmt(tz *time.Location) string
}

type Database interface {
	GetConn() (*clickhouse.Conn, error)
	GetTZ() (*time.Location, error)

	InsertErrorTrackingErrorEvent(e *et.ErrorTrackingErrorEvent) error
	InsertErrorTrackingMessageEvent(e *et.ErrorTrackingMessageEvent) error
	InsertErrorTrackingSession(e *et.ErrorTrackingSession) error

	ListErrors(params errors.ListErrorsParams) ([]*models.Error, error)
	ListEvents(params errors.ListEventsParams) ([]*models.ErrorEvent, error)
	ListMessages(params messages.ListMessagesParams) ([]*models.MessageEvent, error)

	GetError(params errors.GetErrorParams) (*models.Error, error)
	UpdateError(params errors.UpdateErrorParams) (*models.Error, error)
	DeleteProject(params projects.DeleteProjectParams) error

	Close() error
}

// Implements the Database interface.
type database struct {
	// db holds the clickhouse connection
	conn clickhouse.Conn
	tz   *time.Location
}

type DatabaseOptions struct {
	MaxOpenConns     int
	MaxIdleConns     int
	UseRemoteStorage string
}

// NOTE: This clickhouse database initialization procedure should move to a new
// location so we can handle migrations.
func NewDB(clickHouseDsn string, opts *DatabaseOptions) (Database, error) {
	dbOpts, err := clickhouse.ParseDSN(clickHouseDsn)
	if err != nil {
		return nil, fmt.Errorf("failed to parse clickhouse DSN: %w", err)
	}
	dbOpts.MaxOpenConns = opts.MaxOpenConns
	dbOpts.MaxIdleConns = opts.MaxIdleConns
	dbOpts.ConnMaxLifetime = 1 * time.Hour
	dbOpts.Compression = &clickhouse.Compression{Method: clickhouse.CompressionLZ4}

	// Requirement per
	// https://gitlab.com/ahegyi/error-tracking-data-generator#example-queries
	dbOpts.Settings["join_use_nulls"] = 1

	var db *sql.DB
	db, err = goose.OpenDBWithDriver("clickhouse", clickHouseDsn)
	if err != nil {
		return nil, fmt.Errorf("clickhouse open: %w", err)
	}
	goose.SetBaseFS(dbMigrations)

	goose.SetTableName("goose_db_version_v2")
	if err := goose.SetDialect("clickhouse"); err != nil {
		return nil, fmt.Errorf("failed to setup dialect for migration: %w", err)
	}
	err = goose.AttachOptions(map[string]string{
		"ON_CLUSTER": "true",
	})
	if err != nil {
		return nil, fmt.Errorf("failed to attach options for migration: %w", err)
	}

	if err := goose.Up(db, "db/migrations", goose.WithAllowMissing()); err != nil {
		return nil, fmt.Errorf("failed to run migrations: %w", err)
	}

	// If remote storage is enabled, also execute specific migrations
	if opts.UseRemoteStorage == string(common.GCP) {
		// NOTE: It would be better to put these migrations in the same directory as the DDLs. But due to possibility
		// of running without a remote storage backend this has to be made conditional.
		// We assume that the actual tables were create beforehand on the specific database.
		if err := goose.Up(db, "db/gcsmigrations", goose.WithAllowMissing()); err != nil {
			return nil, fmt.Errorf("failed to run gcs specific migrations: %w", err)
		}
	}

	conn, err := clickhouse.Open(dbOpts)
	if err != nil {
		return nil, fmt.Errorf("clickhouse open: %w", err)
	}

	params, err := conn.ServerVersion()
	if err != nil {
		return nil, fmt.Errorf("getting server version: %w", err)
	}

	return &database{
		conn: conn,
		tz:   params.Timezone,
	}, nil
}

func NewCloudDB(clickHouseDsn string, opts *DatabaseOptions) (Database, error) {
	dbOpts, err := clickhouse.ParseDSN(clickHouseDsn)
	if err != nil {
		return nil, fmt.Errorf("failed to parse clickhouse DSN: %w", err)
	}
	dbOpts.MaxOpenConns = opts.MaxOpenConns
	dbOpts.MaxIdleConns = opts.MaxIdleConns
	dbOpts.ConnMaxLifetime = 1 * time.Hour
	dbOpts.Compression = &clickhouse.Compression{Method: clickhouse.CompressionLZ4}

	// Requirement per
	// https://gitlab.com/ahegyi/error-tracking-data-generator#example-queries
	dbOpts.Settings["join_use_nulls"] = 1
	// higher dial timeout as cloud db connection can show variable latencies
	dbOpts.DialTimeout = time.Second * 60

	var db *sql.DB
	db, err = goose.OpenDBWithDriver("clickhouse", clickHouseDsn)
	if err != nil {
		return nil, fmt.Errorf("clickhouse open: %w", err)
	}
	goose.SetBaseFS(dbMigrations)

	goose.SetTableName("goose_db_version_v2")
	if err := goose.SetDialect("clickhouse"); err != nil {
		return nil, fmt.Errorf("failed to setup dialect for migration: %w", err)
	}

	if err := goose.Up(db, "db/cloud_migrations", goose.WithAllowMissing()); err != nil {
		return nil, fmt.Errorf("failed to run migrations: %w", err)
	}

	conn, err := clickhouse.Open(dbOpts)
	if err != nil {
		return nil, fmt.Errorf("clickhouse open: %w", err)
	}

	ctx, cancel := context.WithTimeout(context.Background(), time.Second*60)
	defer cancel()
	err = conn.Ping(ctx)
	if err != nil {
		return nil, fmt.Errorf("failed to ping cloud db %w", err)
	}

	params, err := conn.ServerVersion()
	if err != nil {
		return nil, fmt.Errorf("getting server version: %w", err)
	}

	return &database{
		conn: conn,
		tz:   params.Timezone,
	}, nil
}

// helper function to insert the given value
// Currently, it is the responsibility of the caller to properly sanitize the query string.
// Check db.conn.AsyncInsert but handle SQL injection or find an alternative.
func (db *database) insert(e Insertable) error {
	ctx, cancel := context.WithTimeout(context.Background(), time.Second*5)
	defer cancel()

	// Default max_data_size is 100000 bytes = 100KB.
	// We use 1 MB as the async buffer size but, it has to be set in server configuration.
	// See https://clickhouse.com/docs/en/operations/settings/settings#async-insert-max-data-size for details.
	// async_insert_busy_timeout_ms also controls flush behavior which is 200 ms by default.

	// Note: Currently the insert call is set to fire and forget and does not wait for acknowledgement if the insert has
	// succeeded or not.
	//nolint:wrapcheck
	return db.conn.AsyncInsert(ctx, e.AsInsertStmt(db.tz), false)
}

func (db *database) GetConn() (*clickhouse.Conn, error) {
	return &db.conn, nil
}

func (db *database) Close() error {
	err := db.conn.Close()
	if err != nil {
		return fmt.Errorf("failed to close underlying db conn: %w", err)
	}
	return nil
}

func (db *database) GetTZ() (*time.Location, error) {
	return db.tz, nil
}

func (db *database) DeleteProject(params projects.DeleteProjectParams) error {
	ctx := context.Background()
	q := &queryBuilder{}

	runDeleteQuery := func(query string) error {
		q.reset(query)
		q.build(" project_id = ?", params.ID)

		err := db.conn.Exec(ctx, q.sql, q.args)
		if err != nil {
			return fmt.Errorf("failed to delete project %w", err)
		}
		return nil
	}
	err := runDeleteQuery("DELETE FROM gl_error_tracking_error_events_local ON CLUSTER '{cluster}' WHERE")
	if err != nil {
		return err
	}
	err = runDeleteQuery("DELETE FROM gl_error_tracking_error_status_local ON CLUSTER '{cluster}' WHERE")
	if err != nil {
		return err
	}

	return err
}

const (
	period15m = "15m" // 15 minutes
	period30m = "30m" // 30 minutes
	period1h  = "1h"  // 1 hour
	period24h = "24h" // 24 hours
	period7d  = "7d"  // 7 days
	period14d = "14d" // 14 days
	period30d = "30d" // 30 days
)

var supportedTimePeriods = [7]string{
	period15m, period30m, period1h,
	period24h, period7d, period14d, period30d,
}
