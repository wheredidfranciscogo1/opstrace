package errortracking

import (
	"fmt"
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/errortracking/gen/restapi/operations/errors"
)

type testcase struct {
	name     string
	params   errors.ListErrorsParams
	expected string
	args     []interface{}
	err      error
}

func TestBuildListErrorsQuery(t *testing.T) {
	// reference time to test time-based filtering against
	refTime := time.Now()
	defaultStartTime := refTime.Add(-1 * 30 * 24 * time.Hour).Unix()
	defaultEndTime := refTime.Add(1 * 24 * time.Hour).Unix()
	testcases := []testcase{
		{
			name: "should build a simple query",
			params: errors.ListErrorsParams{
				ProjectID: 1,
				Limit:     int64Pointer(20),
				Query:     nil,
				Cursor:    nil,
				Sort:      stringPointer("last_seen_desc"),
				Status:    stringPointer("unresolved"),
			},
			expected: `
SELECT
    gl_error_tracking_errors.project_id AS project_id,
    gl_error_tracking_errors.fingerprint AS fingerprint,
    gl_error_tracking_errors.name AS name,
    gl_error_tracking_errors.description AS description,
    gl_error_tracking_errors.actor AS actor,
    gl_error_tracking_errors.event_count AS event_count,
    gl_error_tracking_errors.approximated_user_count AS approximated_user_count,
    gl_error_tracking_errors.last_seen_at AS last_seen_at,
    gl_error_tracking_errors.first_seen_at AS first_seen_at,
    COALESCE(gl_error_tracking_error_status.status, 1) AS status,
    COALESCE(gl_error_tracking_ignored_errors.ignored, FALSE) AS ignored
FROM (
SELECT
    project_id,
    fingerprint,
    any(name) as name,
    any(description) as description,
    any(actor) as actor,
    sum(event_count) as event_count,
    uniqMerge(approximated_user_count) as approximated_user_count,
    max(last_seen_at) as last_seen_at,
    min(first_seen_at) as first_seen_at
  FROM gl_error_tracking_errors_mv
  GROUP BY project_id, fingerprint
) as gl_error_tracking_errors
LEFT JOIN (
  SELECT project_id, argMax(status, updated_at) as status, fingerprint
  FROM gl_error_tracking_error_status
  GROUP BY project_id, fingerprint
) gl_error_tracking_error_status ON gl_error_tracking_error_status.project_id = gl_error_tracking_errors.project_id AND
  gl_error_tracking_error_status.fingerprint = gl_error_tracking_errors.fingerprint
LEFT JOIN (
  SELECT project_id, fingerprint, TRUE AS ignored
  FROM gl_error_tracking_ignored_errors
  GROUP BY project_id, fingerprint
) gl_error_tracking_ignored_errors ON gl_error_tracking_ignored_errors.project_id = gl_error_tracking_errors.project_id
  AND gl_error_tracking_ignored_errors.fingerprint = gl_error_tracking_errors.fingerprint
WHERE project_id = $1 AND last_seen_at >= $2 AND last_seen_at <= $3 AND COALESCE(gl_error_tracking_ignored_errors.ignored, FALSE) = FALSE
AND gl_error_tracking_error_status.status = $4 ORDER BY last_seen_at DESC, fingerprint DESC LIMIT $5 OFFSET $6`,
			args: []interface{}{
				uint64(1),
				defaultStartTime,
				defaultEndTime,
				uint8(0),
				int64(20),
				0,
			},
			err: nil,
		},
		{
			name: "should build a query with custom sort",
			params: errors.ListErrorsParams{
				ProjectID: 1,
				Limit:     int64Pointer(20),
				Query:     nil,
				Cursor:    nil,
				Sort:      stringPointer("first_seen_desc"),
				Status:    stringPointer("unresolved"),
			},
			expected: `
SELECT
    gl_error_tracking_errors.project_id AS project_id,
    gl_error_tracking_errors.fingerprint AS fingerprint,
    gl_error_tracking_errors.name AS name,
    gl_error_tracking_errors.description AS description,
    gl_error_tracking_errors.actor AS actor,
    gl_error_tracking_errors.event_count AS event_count,
    gl_error_tracking_errors.approximated_user_count AS approximated_user_count,
    gl_error_tracking_errors.last_seen_at AS last_seen_at,
    gl_error_tracking_errors.first_seen_at AS first_seen_at,
    COALESCE(gl_error_tracking_error_status.status, 1) AS status,
    COALESCE(gl_error_tracking_ignored_errors.ignored, FALSE) AS ignored
FROM (
SELECT
    project_id,
    fingerprint,
    any(name) as name,
    any(description) as description,
    any(actor) as actor,
    sum(event_count) as event_count,
    uniqMerge(approximated_user_count) as approximated_user_count,
    max(last_seen_at) as last_seen_at,
    min(first_seen_at) as first_seen_at
  FROM gl_error_tracking_errors_mv
  GROUP BY project_id, fingerprint
) as gl_error_tracking_errors
LEFT JOIN (
  SELECT project_id, argMax(status, updated_at) as status, fingerprint
  FROM gl_error_tracking_error_status
  GROUP BY project_id, fingerprint
) gl_error_tracking_error_status ON gl_error_tracking_error_status.project_id = gl_error_tracking_errors.project_id AND
  gl_error_tracking_error_status.fingerprint = gl_error_tracking_errors.fingerprint
LEFT JOIN (
  SELECT project_id, fingerprint, TRUE AS ignored
  FROM gl_error_tracking_ignored_errors
  GROUP BY project_id, fingerprint
) gl_error_tracking_ignored_errors ON gl_error_tracking_ignored_errors.project_id = gl_error_tracking_errors.project_id
  AND gl_error_tracking_ignored_errors.fingerprint = gl_error_tracking_errors.fingerprint
WHERE project_id = $1 AND last_seen_at >= $2 AND last_seen_at <= $3 AND COALESCE(gl_error_tracking_ignored_errors.ignored, FALSE) = FALSE
AND gl_error_tracking_error_status.status = $4 ORDER BY first_seen_at DESC, fingerprint DESC LIMIT $5 OFFSET $6`,
			args: []interface{}{
				uint64(1),
				defaultStartTime,
				defaultEndTime,
				uint8(0),
				int64(20),
				0,
			},
			err: nil,
		},
		{
			name: "should build a query with custom status",
			params: errors.ListErrorsParams{
				ProjectID: 1,
				Limit:     int64Pointer(20),
				Query:     nil,
				Cursor:    nil,
				Sort:      stringPointer("first_seen_desc"),
				Status:    stringPointer("ignored"),
			},
			expected: `
SELECT
    gl_error_tracking_errors.project_id AS project_id,
    gl_error_tracking_errors.fingerprint AS fingerprint,
    gl_error_tracking_errors.name AS name,
    gl_error_tracking_errors.description AS description,
    gl_error_tracking_errors.actor AS actor,
    gl_error_tracking_errors.event_count AS event_count,
    gl_error_tracking_errors.approximated_user_count AS approximated_user_count,
    gl_error_tracking_errors.last_seen_at AS last_seen_at,
    gl_error_tracking_errors.first_seen_at AS first_seen_at,
    COALESCE(gl_error_tracking_error_status.status, 1) AS status,
    COALESCE(gl_error_tracking_ignored_errors.ignored, FALSE) AS ignored
FROM (
SELECT
    project_id,
    fingerprint,
    any(name) as name,
    any(description) as description,
    any(actor) as actor,
    sum(event_count) as event_count,
    uniqMerge(approximated_user_count) as approximated_user_count,
    max(last_seen_at) as last_seen_at,
    min(first_seen_at) as first_seen_at
  FROM gl_error_tracking_errors_mv
  GROUP BY project_id, fingerprint
) as gl_error_tracking_errors
LEFT JOIN (
  SELECT project_id, argMax(status, updated_at) as status, fingerprint
  FROM gl_error_tracking_error_status
  GROUP BY project_id, fingerprint
) gl_error_tracking_error_status ON gl_error_tracking_error_status.project_id = gl_error_tracking_errors.project_id AND
  gl_error_tracking_error_status.fingerprint = gl_error_tracking_errors.fingerprint
LEFT JOIN (
  SELECT project_id, fingerprint, TRUE AS ignored
  FROM gl_error_tracking_ignored_errors
  GROUP BY project_id, fingerprint
) gl_error_tracking_ignored_errors ON gl_error_tracking_ignored_errors.project_id = gl_error_tracking_errors.project_id
  AND gl_error_tracking_ignored_errors.fingerprint = gl_error_tracking_errors.fingerprint
WHERE project_id = $1 AND last_seen_at >= $2 AND last_seen_at <= $3 AND COALESCE(gl_error_tracking_ignored_errors.ignored, FALSE) = TRUE ORDER BY first_seen_at DESC, fingerprint DESC LIMIT $4 OFFSET $5`,
			args: []interface{}{
				uint64(1),
				defaultStartTime,
				defaultEndTime,
				int64(20),
				0,
			},
			err: nil,
		},
		{
			name: "should build a query with custom query parameter",
			params: errors.ListErrorsParams{
				ProjectID: 1,
				Limit:     int64Pointer(20),
				Query:     stringPointer("foo"),
				Cursor:    nil,
				Sort:      stringPointer("first_seen_desc"),
				Status:    stringPointer("ignored"),
			},
			expected: `
SELECT
    gl_error_tracking_errors.project_id AS project_id,
    gl_error_tracking_errors.fingerprint AS fingerprint,
    gl_error_tracking_errors.name AS name,
    gl_error_tracking_errors.description AS description,
    gl_error_tracking_errors.actor AS actor,
    gl_error_tracking_errors.event_count AS event_count,
    gl_error_tracking_errors.approximated_user_count AS approximated_user_count,
    gl_error_tracking_errors.last_seen_at AS last_seen_at,
    gl_error_tracking_errors.first_seen_at AS first_seen_at,
    COALESCE(gl_error_tracking_error_status.status, 1) AS status,
    COALESCE(gl_error_tracking_ignored_errors.ignored, FALSE) AS ignored
FROM (
SELECT
    project_id,
    fingerprint,
    any(name) as name,
    any(description) as description,
    any(actor) as actor,
    sum(event_count) as event_count,
    uniqMerge(approximated_user_count) as approximated_user_count,
    max(last_seen_at) as last_seen_at,
    min(first_seen_at) as first_seen_at
  FROM gl_error_tracking_errors_mv
  GROUP BY project_id, fingerprint
) as gl_error_tracking_errors
LEFT JOIN (
  SELECT project_id, argMax(status, updated_at) as status, fingerprint
  FROM gl_error_tracking_error_status
  GROUP BY project_id, fingerprint
) gl_error_tracking_error_status ON gl_error_tracking_error_status.project_id = gl_error_tracking_errors.project_id AND
  gl_error_tracking_error_status.fingerprint = gl_error_tracking_errors.fingerprint
LEFT JOIN (
  SELECT project_id, fingerprint, TRUE AS ignored
  FROM gl_error_tracking_ignored_errors
  GROUP BY project_id, fingerprint
) gl_error_tracking_ignored_errors ON gl_error_tracking_ignored_errors.project_id = gl_error_tracking_errors.project_id
  AND gl_error_tracking_ignored_errors.fingerprint = gl_error_tracking_errors.fingerprint
WHERE project_id = $1 AND last_seen_at >= $2 AND last_seen_at <= $3 AND COALESCE(gl_error_tracking_ignored_errors.ignored, FALSE) = TRUE AND (gl_error_tracking_errors.name ILIKE $4 OR gl_error_tracking_errors.description ILIKE $5) ORDER BY first_seen_at DESC, fingerprint DESC LIMIT $6 OFFSET $7`,
			args: []interface{}{
				uint64(1),
				defaultStartTime,
				defaultEndTime,
				"%foo%",
				"%foo%",
				int64(20),
				0,
			},
			err: nil,
		},
		{
			name: "should fail with invalid custom cursor parameter",
			params: errors.ListErrorsParams{
				ProjectID: 1,
				Limit:     int64Pointer(10),
				Query:     nil,
				Cursor:    stringPointer("foobar"),
				Sort:      stringPointer("frequency_desc"),
				Status:    stringPointer("resolved"),
			},
			expected: "",
			args:     nil,
			err:      fmt.Errorf("unexpected end of JSON input"),
		},
		{
			name: "should not fail with valid custom cursor parameter",
			params: errors.ListErrorsParams{
				ProjectID: 1,
				Limit:     int64Pointer(9),
				Query:     nil,
				// cursor is the base64 encoded json string: {"page":3}
				Cursor: stringPointer("eyJwYWdlIjozfQo="),
				Sort:   stringPointer("frequency_desc"),
				Status: stringPointer("resolved"),
			},
			expected: `
SELECT
    gl_error_tracking_errors.project_id AS project_id,
    gl_error_tracking_errors.fingerprint AS fingerprint,
    gl_error_tracking_errors.name AS name,
    gl_error_tracking_errors.description AS description,
    gl_error_tracking_errors.actor AS actor,
    gl_error_tracking_errors.event_count AS event_count,
    gl_error_tracking_errors.approximated_user_count AS approximated_user_count,
    gl_error_tracking_errors.last_seen_at AS last_seen_at,
    gl_error_tracking_errors.first_seen_at AS first_seen_at,
    COALESCE(gl_error_tracking_error_status.status, 1) AS status,
    COALESCE(gl_error_tracking_ignored_errors.ignored, FALSE) AS ignored
FROM (
SELECT
    project_id,
    fingerprint,
    any(name) as name,
    any(description) as description,
    any(actor) as actor,
    sum(event_count) as event_count,
    uniqMerge(approximated_user_count) as approximated_user_count,
    max(last_seen_at) as last_seen_at,
    min(first_seen_at) as first_seen_at
  FROM gl_error_tracking_errors_mv
  GROUP BY project_id, fingerprint
) as gl_error_tracking_errors
LEFT JOIN (
  SELECT project_id, argMax(status, updated_at) as status, fingerprint
  FROM gl_error_tracking_error_status
  GROUP BY project_id, fingerprint
) gl_error_tracking_error_status ON gl_error_tracking_error_status.project_id = gl_error_tracking_errors.project_id AND
  gl_error_tracking_error_status.fingerprint = gl_error_tracking_errors.fingerprint
LEFT JOIN (
  SELECT project_id, fingerprint, TRUE AS ignored
  FROM gl_error_tracking_ignored_errors
  GROUP BY project_id, fingerprint
) gl_error_tracking_ignored_errors ON gl_error_tracking_ignored_errors.project_id = gl_error_tracking_errors.project_id
  AND gl_error_tracking_ignored_errors.fingerprint = gl_error_tracking_errors.fingerprint
WHERE project_id = $1 AND last_seen_at >= $2 AND last_seen_at <= $3 AND COALESCE(gl_error_tracking_ignored_errors.ignored, FALSE) = FALSE
AND gl_error_tracking_error_status.status = $4 ORDER BY event_count DESC, fingerprint DESC LIMIT $5 OFFSET $6`,
			args: []interface{}{
				uint64(1),
				defaultStartTime,
				defaultEndTime,
				uint8(1),
				int64(9),
				18,
			},
			err: nil,
		},
		{
			name: "should not fail with custom cursor parameter without page field",
			params: errors.ListErrorsParams{
				ProjectID: 1,
				Limit:     int64Pointer(7),
				Query:     nil,
				// cursor is the base64 encoded json string: {"foo":"bar"}
				Cursor: stringPointer("eyJmb28iOiJiYXIifQo="),
				Sort:   stringPointer("frequency_desc"),
				Status: stringPointer("resolved"),
			},
			expected: `
SELECT
    gl_error_tracking_errors.project_id AS project_id,
    gl_error_tracking_errors.fingerprint AS fingerprint,
    gl_error_tracking_errors.name AS name,
    gl_error_tracking_errors.description AS description,
    gl_error_tracking_errors.actor AS actor,
    gl_error_tracking_errors.event_count AS event_count,
    gl_error_tracking_errors.approximated_user_count AS approximated_user_count,
    gl_error_tracking_errors.last_seen_at AS last_seen_at,
    gl_error_tracking_errors.first_seen_at AS first_seen_at,
    COALESCE(gl_error_tracking_error_status.status, 1) AS status,
    COALESCE(gl_error_tracking_ignored_errors.ignored, FALSE) AS ignored
FROM (
SELECT
    project_id,
    fingerprint,
    any(name) as name,
    any(description) as description,
    any(actor) as actor,
    sum(event_count) as event_count,
    uniqMerge(approximated_user_count) as approximated_user_count,
    max(last_seen_at) as last_seen_at,
    min(first_seen_at) as first_seen_at
  FROM gl_error_tracking_errors_mv
  GROUP BY project_id, fingerprint
) as gl_error_tracking_errors
LEFT JOIN (
  SELECT project_id, argMax(status, updated_at) as status, fingerprint
  FROM gl_error_tracking_error_status
  GROUP BY project_id, fingerprint
) gl_error_tracking_error_status ON gl_error_tracking_error_status.project_id = gl_error_tracking_errors.project_id AND
  gl_error_tracking_error_status.fingerprint = gl_error_tracking_errors.fingerprint
LEFT JOIN (
  SELECT project_id, fingerprint, TRUE AS ignored
  FROM gl_error_tracking_ignored_errors
  GROUP BY project_id, fingerprint
) gl_error_tracking_ignored_errors ON gl_error_tracking_ignored_errors.project_id = gl_error_tracking_errors.project_id
  AND gl_error_tracking_ignored_errors.fingerprint = gl_error_tracking_errors.fingerprint
WHERE project_id = $1 AND last_seen_at >= $2 AND last_seen_at <= $3 AND COALESCE(gl_error_tracking_ignored_errors.ignored, FALSE) = FALSE
AND gl_error_tracking_error_status.status = $4 ORDER BY event_count DESC, fingerprint DESC LIMIT $5 OFFSET $6`,
			args: []interface{}{
				uint64(1),
				defaultStartTime,
				defaultEndTime,
				uint8(1),
				int64(7),
				0,
			},
			err: nil,
		},
		{
			name: "should fail with custom cursor parameter with invalid json",
			params: errors.ListErrorsParams{
				ProjectID: 1,
				Limit:     int64Pointer(7),
				Query:     nil,
				// cursor is the base64 encoded json string: "foobar"
				Cursor: stringPointer("Zm9vYmFyCg=="),
				Sort:   stringPointer("frequency_desc"),
				Status: stringPointer("resolved"),
			},
			expected: "",
			args:     nil,
			err:      fmt.Errorf("invalid character"),
		},
		{
			name: "should fail with unexpected status",
			params: errors.ListErrorsParams{
				ProjectID: 1,
				Limit:     int64Pointer(20),
				Query:     nil,
				Cursor:    nil,
				Sort:      stringPointer("first_seen_desc"),
				Status:    stringPointer("_ignored"),
			},
			expected: "",
			args:     nil,
			err:      fmt.Errorf("unexpected error status _ignored"),
		},
	}
	// end of base testcases
	// now, add time-filter specific testcases
	for _, queryPeriod := range supportedTimePeriods {
		var (
			startTime, endTime int64
		)
		switch queryPeriod {
		case period15m:
			startTime = refTime.Add(-1 * 15 * time.Minute).Unix()
			endTime = refTime.Add(1 * time.Minute).Unix()
		case period30m:
			startTime = refTime.Add(-1 * 30 * time.Minute).Unix()
			endTime = refTime.Add(time.Minute).Unix()
		case period1h:
			startTime = refTime.Add(-1 * 60 * time.Minute).Unix()
			endTime = refTime.Add(time.Minute).Unix()
		case period24h:
			startTime = refTime.Add(-1 * 24 * time.Hour).Unix()
			endTime = refTime.Add(time.Hour).Unix()
		case period7d:
			startTime = refTime.Add(-1 * 7 * 24 * time.Hour).Unix()
			endTime = refTime.Add(24 * time.Hour).Unix()
		case period14d:
			startTime = refTime.Add(-1 * 14 * 24 * time.Hour).Unix()
			endTime = refTime.Add(24 * time.Hour).Unix()
		case period30d:
			startTime = refTime.Add(-1 * 30 * 24 * time.Hour).Unix()
			endTime = refTime.Add(24 * time.Hour).Unix()
		}

		testcases = append(testcases, testcase{
			name: fmt.Sprintf("should build a simple query with data filtered for: %s", queryPeriod),
			params: errors.ListErrorsParams{
				ProjectID:   1,
				Limit:       int64Pointer(20),
				Query:       nil,
				Cursor:      nil,
				Sort:        stringPointer("last_seen_desc"),
				Status:      stringPointer("unresolved"),
				QueryPeriod: stringPointer(queryPeriod),
			},
			expected: `
SELECT
    gl_error_tracking_errors.project_id AS project_id,
    gl_error_tracking_errors.fingerprint AS fingerprint,
    gl_error_tracking_errors.name AS name,
    gl_error_tracking_errors.description AS description,
    gl_error_tracking_errors.actor AS actor,
    gl_error_tracking_errors.event_count AS event_count,
    gl_error_tracking_errors.approximated_user_count AS approximated_user_count,
    gl_error_tracking_errors.last_seen_at AS last_seen_at,
    gl_error_tracking_errors.first_seen_at AS first_seen_at,
    COALESCE(gl_error_tracking_error_status.status, 1) AS status,
    COALESCE(gl_error_tracking_ignored_errors.ignored, FALSE) AS ignored
FROM (
SELECT
    project_id,
    fingerprint,
    any(name) as name,
    any(description) as description,
    any(actor) as actor,
    sum(event_count) as event_count,
    uniqMerge(approximated_user_count) as approximated_user_count,
    max(last_seen_at) as last_seen_at,
    min(first_seen_at) as first_seen_at
  FROM gl_error_tracking_errors_mv
  GROUP BY project_id, fingerprint
) as gl_error_tracking_errors
LEFT JOIN (
  SELECT project_id, argMax(status, updated_at) as status, fingerprint
  FROM gl_error_tracking_error_status
  GROUP BY project_id, fingerprint
) gl_error_tracking_error_status ON gl_error_tracking_error_status.project_id = gl_error_tracking_errors.project_id AND
  gl_error_tracking_error_status.fingerprint = gl_error_tracking_errors.fingerprint
LEFT JOIN (
  SELECT project_id, fingerprint, TRUE AS ignored
  FROM gl_error_tracking_ignored_errors
  GROUP BY project_id, fingerprint
) gl_error_tracking_ignored_errors ON gl_error_tracking_ignored_errors.project_id = gl_error_tracking_errors.project_id
  AND gl_error_tracking_ignored_errors.fingerprint = gl_error_tracking_errors.fingerprint
WHERE project_id = $1 AND last_seen_at >= $2 AND last_seen_at <= $3 AND COALESCE(gl_error_tracking_ignored_errors.ignored, FALSE) = FALSE
AND gl_error_tracking_error_status.status = $4 ORDER BY last_seen_at DESC, fingerprint DESC LIMIT $5 OFFSET $6`,
			args: []interface{}{
				uint64(1),
				startTime,
				endTime,
				uint8(0),
				int64(20),
				0,
			},
			err: nil,
		})
	}
	// now run all testcases
	for _, tc := range testcases {
		t.Log(tc.name)
		sql, args, err := buildListErrorsQuery(tc.params, refTime)
		if tc.err != nil {
			assert.Error(t, err)
		} else {
			assert.Nil(t, err)
		}
		assert.Equal(t, tc.expected, sql)
		assert.EqualValues(t, tc.args, args)
	}
}

func TestBuildGetErrorQuery(t *testing.T) {
	for _, tc := range []struct {
		name     string
		params   errors.GetErrorParams
		expected string
		args     []interface{}
	}{
		{
			name: "should build a simple query",
			params: errors.GetErrorParams{
				ProjectID:   1,
				Fingerprint: 1,
			},
			expected: `
SELECT
    gl_error_tracking_errors.project_id AS project_id,
    gl_error_tracking_errors.fingerprint AS fingerprint,
    gl_error_tracking_errors.name AS name,
    gl_error_tracking_errors.description AS description,
    gl_error_tracking_errors.actor AS actor,
    gl_error_tracking_errors.event_count AS event_count,
    gl_error_tracking_errors.approximated_user_count AS approximated_user_count,
    gl_error_tracking_errors.last_seen_at AS last_seen_at,
    gl_error_tracking_errors.first_seen_at AS first_seen_at,
    COALESCE(gl_error_tracking_error_status.status, 1) AS status,
    COALESCE(gl_error_tracking_ignored_errors.ignored, FALSE) AS ignored
FROM (
SELECT
    project_id,
    fingerprint,
    any(name) as name,
    any(description) as description,
    any(actor) as actor,
    sum(event_count) as event_count,
    uniqMerge(approximated_user_count) as approximated_user_count,
    max(last_seen_at) as last_seen_at,
    min(first_seen_at) as first_seen_at
  FROM gl_error_tracking_errors_mv
  GROUP BY project_id, fingerprint
) as gl_error_tracking_errors
LEFT JOIN (
  SELECT project_id, argMax(status, updated_at) as status, fingerprint
  FROM gl_error_tracking_error_status
  GROUP BY project_id, fingerprint
) gl_error_tracking_error_status ON gl_error_tracking_error_status.project_id = gl_error_tracking_errors.project_id AND
  gl_error_tracking_error_status.fingerprint = gl_error_tracking_errors.fingerprint
LEFT JOIN (
  SELECT project_id, fingerprint, TRUE AS ignored
  FROM gl_error_tracking_ignored_errors
  GROUP BY project_id, fingerprint
) gl_error_tracking_ignored_errors ON gl_error_tracking_ignored_errors.project_id = gl_error_tracking_errors.project_id
  AND gl_error_tracking_ignored_errors.fingerprint = gl_error_tracking_errors.fingerprint
WHERE project_id = $1 AND fingerprint = $2`,
			args: []interface{}{uint64(1), uint32(1)},
		},
	} {
		t.Log(tc.name)
		sql, args := buildGetErrorQuery(tc.params)
		assert.Equal(t, tc.expected, sql)
		assert.EqualValues(t, tc.args, args)
	}
}
