package errortracking

import (
	"errors"
	"os"
	"strings"
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/mock"
	et "gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/errortracking/models"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/errortracking/types"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/persistentqueue"
)

type MockDatabase struct {
	mock.Mock
	Database
}

func (m *MockDatabase) InsertErrorTrackingErrorEvent(e *et.ErrorTrackingErrorEvent) error {
	args := m.Called(e)
	return args.Error(0)
}

func (m *MockDatabase) InsertErrorTrackingMessageEvent(e *et.ErrorTrackingMessageEvent) error {
	args := m.Called(e)
	return args.Error(0)
}

func (m *MockDatabase) InsertErrorTrackingSession(e *et.ErrorTrackingSession) error {
	args := m.Called(e)
	return args.Error(0)
}

func TestClickhouseIngestEventData(t *testing.T) {
	assert := assert.New(t)
	projectID := uint64(1)

	dbMock := &MockDatabase{}
	ci := ClickhouseIngester{
		db: dbMock,
	}
	payload, err := os.ReadFile("testdata/exceptions/ruby_event.json")
	assert.Nil(err)
	event := types.Event{}
	assert.Nil(event.UnmarshalJSON(payload))

	expectedErrorEvent := et.NewErrorTrackingErrorEvent(projectID, &event, payload)

	t.Run("Fails if cannot insert error event", func(t *testing.T) {
		dbMock.On("InsertErrorTrackingErrorEvent", expectedErrorEvent).Times(1).Return(errors.New("mock"))

		err := ci.IngestEventData(projectID, types.EventType, payload, &event)

		assert.True(strings.Contains(err.Error(), "failed to insert error event"))
		dbMock.AssertExpectations(t)
	})

	t.Run("Succeeds", func(t *testing.T) {
		dbMock.On("InsertErrorTrackingErrorEvent", expectedErrorEvent).Times(1).Return(nil)
		err = ci.IngestEventData(projectID, types.EventType, payload, &event)

		assert.Nil(err)
		dbMock.AssertExpectations(t)
	})
}

func TestClickhouseIngestMessageData(t *testing.T) {
	assert := assert.New(t)
	projectID := uint64(1)

	dbMock := &MockDatabase{}
	ci := ClickhouseIngester{
		db: dbMock,
	}
	payload, err := os.ReadFile("testdata/message/go_message.json")
	assert.Nil(err)
	event := types.Event{}
	assert.Nil(event.UnmarshalJSON(payload))

	t.Run("Succeeds", func(t *testing.T) {
		dbMock.On("InsertErrorTrackingMessageEvent",
			et.NewErrorTrackingMessageEvent(projectID, &event, payload)).Times(1).Return(nil)
		err = ci.IngestMessageData(projectID, types.EventType, payload, &event)

		assert.Nil(err)
		dbMock.AssertExpectations(t)
	})

	t.Run("Fails if it fails to insert message event", func(t *testing.T) {
		dbMock.On("InsertErrorTrackingMessageEvent",
			et.NewErrorTrackingMessageEvent(projectID, &event, payload)).Times(1).Return(errors.New("mock"))
		err = ci.IngestMessageData(projectID, types.EventType, payload, &event)

		assert.True(strings.Contains(err.Error(), "failed to insert message event to the database"))
		dbMock.AssertExpectations(t)
	})
}

func TestClickhouseIngestSessionData(t *testing.T) {
	assert := assert.New(t)
	projectID := uint64(1)

	dbMock := &MockDatabase{}
	ci := ClickhouseIngester{
		db: dbMock,
	}
	payload, err := os.ReadFile("testdata/sessions/session_payload.txt")
	assert.Nil(err)
	session := types.Session{}
	assert.Nil(session.UnmarshalJSON(payload))

	t.Run("Succeeds", func(t *testing.T) {
		dbMock.On("InsertErrorTrackingSession",
			et.NewErrorTrackingSession(projectID, &session, payload)).Times(1).Return(nil)
		err = ci.IngestSessionData(projectID, types.SessionType, payload, &session)

		assert.Nil(err)
		dbMock.AssertExpectations(t)
	})

	t.Run("Fails if it fails to insert session", func(t *testing.T) {
		dbMock.On("InsertErrorTrackingSession",
			et.NewErrorTrackingSession(projectID, &session, payload)).Times(1).Return(errors.New("mock"))
		err = ci.IngestSessionData(projectID, types.SessionType, payload, &session)

		assert.True(strings.Contains(err.Error(), "failed to insert session to the database"))
		dbMock.AssertExpectations(t)
	})
}

type MockQueue struct {
	mock.Mock
	persistentqueue.IQueue
}

func (m *MockQueue) Insert(v []byte) error {
	args := m.Called(v)
	return args.Error(0)
}

func TestIngestData(t *testing.T) {
	assert := assert.New(t)
	projectID := uint64(1)

	queueMock := &MockQueue{}
	qi := QueueIngester{
		queue: queueMock,
	}
	payload, err := os.ReadFile("testdata/message/go_message.json")
	assert.Nil(err)
	event := types.Event{}
	assert.Nil(event.UnmarshalJSON(payload))

	t.Run("Succeeds", func(t *testing.T) {
		queueMock.On("Insert", mock.Anything).Times(1).Return(nil)
		assert.Nil(qi.ingestData(projectID, types.EventType, payload))
		queueMock.AssertExpectations(t)
	})

	t.Run("Fails if it fails to insert item in the queue", func(t *testing.T) {
		queueMock.On("Insert", mock.Anything).Times(1).Return(errors.New("mock"))
		err := qi.ingestData(projectID, types.EventType, payload)
		assert.True(strings.Contains(err.Error(), "enqueueing event"))
		queueMock.AssertExpectations(t)
	})
}
