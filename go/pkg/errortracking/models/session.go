package models

import (
	"fmt"
	"time"

	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/errortracking/types"
)

// ErrorTrackingSession maps to the corresponding table in the clickhouse
// database.
type ErrorTrackingSession struct {
	ProjectID   uint64    `json:"project_id" ch:"project_id"`
	SessionID   string    `json:"session_id" ch:"session_id"`
	UserID      string    `json:"user_id" ch:"user_id"`
	Init        uint8     `json:"init" ch:"init"`
	Payload     string    `json:"payload,omitempty" ch:"payload"`
	Started     time.Time `json:"started" ch:"started"`
	OccurredAt  time.Time `json:"occurred_at" ch:"occurred_at"`
	Duration    float64   `json:"duration,omitempty" ch:"duration"`
	Status      string    `json:"status,omitempty" ch:"status"`
	Release     string    `json:"release,omitempty" ch:"release"`
	Environment string    `json:"environment,omitempty" ch:"environment"`
}

func NewErrorTrackingSession(projectID uint64, e *types.Session, payload []byte) *ErrorTrackingSession {
	return &ErrorTrackingSession{
		ProjectID:   projectID,
		SessionID:   e.SessionID,
		UserID:      e.UserID,
		Init:        e.Init,
		Payload:     string(payload),
		Started:     e.Started,
		OccurredAt:  e.OccuredAt,
		Duration:    e.Duration,
		Status:      e.Status,
		Release:     e.Attributes.Release,
		Environment: e.Attributes.Environment,
	}
}

func (e ErrorTrackingSession) AsInsertStmt(tz *time.Location) string {
	return fmt.Sprintf(`INSERT INTO gl_error_tracking_sessions (
		project_id,
		session_id,
		user_id,
		init,
		payload,
		started,
		occurred_at,
		duration,
		status,
		release,
		environment
	) VALUES (%d,%s,%s,%d,%s,%s,%s,%f,%s,%s,%s)`,
		e.ProjectID,
		quote(e.SessionID),
		quote(e.UserID),
		e.Init,
		quote(e.Payload),
		formatTime(e.Started, tz),
		formatTime(e.OccurredAt, tz),
		e.Duration,
		quote(e.Status),
		quote(e.Release),
		quote(e.Environment),
	)
}
