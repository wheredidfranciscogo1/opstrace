package errortracking

import (
	"fmt"

	et "gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/errortracking/models"
)

// InsertErrorTrackingSession inserts the given session in the
// error_tracking_session table in the clickhouse database.
func (db *database) InsertErrorTrackingSession(e *et.ErrorTrackingSession) error {
	err := db.insert(e)
	if err != nil {
		return fmt.Errorf("inserting error tracking session: %w", err)
	}

	return nil
}
