package errortracking

import (
	"context"
	"database/sql"
	stdErrors "errors"
	"fmt"
	"time"

	"github.com/go-openapi/strfmt"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/common"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/errortracking/gen/models"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/errortracking/gen/restapi/operations/errors"
	et "gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/errortracking/models"
)

func (db *database) ListErrors(params errors.ListErrorsParams) ([]*models.Error, error) {
	ctx := context.Background()

	refTime := time.Now()
	query, args, err := buildListErrorsQuery(params, refTime)
	if err != nil {
		return nil, err
	}

	rows, err := db.conn.Query(ctx, query, args...)
	if err != nil {
		return nil, fmt.Errorf("failed to query clickhouse: %w", err)
	}

	var fingerprints []uint32
	var result []*models.Error
	for rows.Next() {
		// Scan the row as a ErrorTrackingError struct
		e := &et.ErrorTrackingError{}
		err := rows.ScanStruct(e)
		if err != nil {
			return nil, fmt.Errorf("failed to scan struct into ErrorTrackingError: %w", err)
		}
		// Check if the error has been marked ignored first, then update status
		// value accordingly.
		var status string
		if e.Ignored {
			status = errorIgnoredStr
		} else {
			if _, ok := errorStatusToStr[errorStatus(e.Status)]; !ok {
				return nil, fmt.Errorf("unexpected error status value %v", e.Status)
			}
			status = string(errorStatusToStr[errorStatus(e.Status)])
		}
		// Convert the ErrorTrackingError struct to a models.Error. This was
		// done because golang doesn't allow adding tags dinamically at runtime
		// to structs to be able to decode a models.Error which only defines
		// json tags.
		result = append(result, &models.Error{
			Actor:                 e.Actor,
			ApproximatedUserCount: e.ApproximatedUserCount,
			Description:           e.Description,
			EventCount:            e.EventCount,
			Fingerprint:           e.Fingerprint,
			FirstSeenAt:           strfmt.DateTime(e.FirstSeenAt),
			LastSeenAt:            strfmt.DateTime(e.LastSeenAt),
			Name:                  e.Name,
			ProjectID:             e.ProjectID,
			Status:                status,
		})
		// Keep a note of all fingerprints to fetch stats for
		fingerprints = append(fingerprints, e.Fingerprint)
	}
	rows.Close()

	if err := rows.Err(); err != nil {
		return nil, fmt.Errorf("failed to read errors from clickhouse: %w", err)
	}

	// Get error stats for all the errors fetched
	if len(fingerprints) > 0 {
		statsPeriod := "24h" // swagger defn defaults to 24h, but just in case
		if params.StatsPeriod != nil {
			statsPeriod = *params.StatsPeriod
		}
		stats, err := db.getErrorStats(ctx, fingerprints, statsPeriod)
		if err != nil {
			return nil, fmt.Errorf("getting error stats: %w", err)
		}
		// For all errors we have...
		for idx, e := range result {
			// Check if we got corresponding stats...
			if _, ok := stats[e.Fingerprint]; ok {
				// If we did, update it in-place
				result[idx].Stats = &models.ErrorStats{
					Frequency: map[string]interface{}{
						statsPeriod: stats[e.Fingerprint],
					},
				}
			}
		}
	}

	return result, nil
}

const baseQuery = `
SELECT
    gl_error_tracking_errors.project_id AS project_id,
    gl_error_tracking_errors.fingerprint AS fingerprint,
    gl_error_tracking_errors.name AS name,
    gl_error_tracking_errors.description AS description,
    gl_error_tracking_errors.actor AS actor,
    gl_error_tracking_errors.event_count AS event_count,
    gl_error_tracking_errors.approximated_user_count AS approximated_user_count,
    gl_error_tracking_errors.last_seen_at AS last_seen_at,
    gl_error_tracking_errors.first_seen_at AS first_seen_at,
    COALESCE(gl_error_tracking_error_status.status, 1) AS status,
    COALESCE(gl_error_tracking_ignored_errors.ignored, FALSE) AS ignored
FROM (
SELECT
    project_id,
    fingerprint,
    any(name) as name,
    any(description) as description,
    any(actor) as actor,
    sum(event_count) as event_count,
    uniqMerge(approximated_user_count) as approximated_user_count,
    max(last_seen_at) as last_seen_at,
    min(first_seen_at) as first_seen_at
  FROM gl_error_tracking_errors_mv
  GROUP BY project_id, fingerprint
) as gl_error_tracking_errors
LEFT JOIN (
  SELECT project_id, argMax(status, updated_at) as status, fingerprint
  FROM gl_error_tracking_error_status
  GROUP BY project_id, fingerprint
) gl_error_tracking_error_status ON gl_error_tracking_error_status.project_id = gl_error_tracking_errors.project_id AND
  gl_error_tracking_error_status.fingerprint = gl_error_tracking_errors.fingerprint
LEFT JOIN (
  SELECT project_id, fingerprint, TRUE AS ignored
  FROM gl_error_tracking_ignored_errors
  GROUP BY project_id, fingerprint
) gl_error_tracking_ignored_errors ON gl_error_tracking_ignored_errors.project_id = gl_error_tracking_errors.project_id
  AND gl_error_tracking_ignored_errors.fingerprint = gl_error_tracking_errors.fingerprint
`

func buildListErrorsQuery(params errors.ListErrorsParams, refTime time.Time) (string, []interface{}, error) {
	q := &queryBuilder{}
	q.reset(baseQuery)

	q.build("WHERE project_id = ?", params.ProjectID)

	// Setup time filtering
	queryPeriod := "30d" // default to 30 days of data
	if params.QueryPeriod != nil {
		queryPeriod = *params.QueryPeriod
	}
	startTime, endTime, _, _ := common.InferTimelines(refTime, queryPeriod)
	q.build(" AND last_seen_at >= ? AND last_seen_at <= ?", startTime, endTime)

	// Status default value is unresolved so we can skip the nil check.
	status, ok := errorStatusToInt[errorStatusStr(*params.Status)]
	if !ok {
		return "", nil, fmt.Errorf("unexpected error status %v", *params.Status)
	}
	if status == errorIgnored {
		q.build(" AND COALESCE(gl_error_tracking_ignored_errors.ignored, FALSE) = TRUE")
	} else {
		q.build(
			` AND COALESCE(gl_error_tracking_ignored_errors.ignored, FALSE) = FALSE
AND gl_error_tracking_error_status.status = ?`,
			uint8(status),
		)
	}

	if params.Query != nil && len(*params.Query) > 2 {
		// clickhouse ILIKE search operator uses percentage character % to match
		// any byte. golang requires %% to escape the percentage % character.
		wildcard := fmt.Sprintf("%%%s%%", *params.Query)
		q.build(
			" AND (gl_error_tracking_errors.name ILIKE ? OR gl_error_tracking_errors.description ILIKE ?)",
			wildcard,
			wildcard,
		)
	}

	// Sort default value is last_seen_desc so we can skip the nil check.
	orderBy := " ORDER BY"
	switch *params.Sort {
	case "first_seen_desc":
		orderBy += " first_seen_at DESC, fingerprint DESC"
	case "frequency_desc":
		orderBy += " event_count DESC, fingerprint DESC"
	default:
		orderBy += " last_seen_at DESC, fingerprint DESC"
	}
	q.build(orderBy)

	q.build(" LIMIT ?", *params.Limit)

	page, err := decodePage(params.Cursor)
	if err != nil {
		return "", nil, err
	}
	// Limit default value is 20 so we can skip the nil check.
	offset := (page - 1) * int(*params.Limit)
	q.build(" OFFSET ?", offset)

	return q.sql, q.args, err
}

func (db *database) GetError(params errors.GetErrorParams) (*models.Error, error) {
	ctx := context.Background()
	var result *models.Error

	query, args := buildGetErrorQuery(params)

	row := db.conn.QueryRow(ctx, query, args...)
	if err := row.Err(); err != nil {
		return nil, fmt.Errorf("failed to query error : %w", err)
	}

	// Scan the row as a ErrorTrackingError struct
	e := &et.ErrorTrackingError{}
	err := row.ScanStruct(e)

	if err != nil {
		if stdErrors.Is(err, sql.ErrNoRows) {
			return nil, nil
		}
		return nil, fmt.Errorf("failed to scan struct into ErrorTrackingError: %w", err)
	}
	// Check if the error has been marked ignored first, then update status
	// value accordingly.
	var status string
	if e.Ignored {
		status = errorIgnoredStr
	} else {
		if _, ok := errorStatusToStr[errorStatus(e.Status)]; !ok {
			return nil, fmt.Errorf("unexpected error status value %v", e.Status)
		}
		status = string(errorStatusToStr[errorStatus(e.Status)])
	}
	// Get error stats for the error fetched
	// We align the response here with Sentry's which returns 24h & 30d stats by default,
	// see: https://docs.sentry.io/api/events/retrieve-an-issue/
	perPeriodStats := make(map[string]interface{})
	for _, statsPeriod := range []string{period24h, period30d} {
		stats, err := db.getErrorStats(ctx, []uint32{e.Fingerprint}, statsPeriod)
		if err != nil {
			return nil, fmt.Errorf("getting error stats: %w", err)
		}
		perPeriodStats[statsPeriod] = stats[e.Fingerprint]
	}

	// Convert the ErrorTrackingError struct to a models.Error. This was
	// done because golang doesn't allow adding tags dinamically at runtime
	// to structs to be able to decode a models.Error which only defines
	// json tags.
	result = &models.Error{
		Actor:                 e.Actor,
		ApproximatedUserCount: e.ApproximatedUserCount,
		Description:           e.Description,
		EventCount:            e.EventCount,
		Fingerprint:           e.Fingerprint,
		FirstSeenAt:           strfmt.DateTime(e.FirstSeenAt),
		LastSeenAt:            strfmt.DateTime(e.LastSeenAt),
		Name:                  e.Name,
		ProjectID:             e.ProjectID,
		Status:                status,
		Stats:                 &models.ErrorStats{Frequency: perPeriodStats},
	}

	if err := row.Err(); err != nil {
		return nil, fmt.Errorf("failed to read error from clickhouse: %w", err)
	}
	return result, nil
}

func buildGetErrorQuery(params errors.GetErrorParams) (string, []interface{}) {
	q := &queryBuilder{}
	q.reset(baseQuery)

	q.build("WHERE project_id = ? AND fingerprint = ?",
		params.ProjectID,
		params.Fingerprint,
	)

	return q.sql, q.args
}

// See: https://gitlab.com/gitlab-org/gitlab/-/merge_requests/86544/diffs#7e6d17e7e7c8bf3957ef0d12127876ba75b05591_0_35
func (db *database) UpdateError(params errors.UpdateErrorParams) (*models.Error, error) {
	// Check the error exists before proceeding.
	res, err := db.GetError(errors.GetErrorParams{ProjectID: params.ProjectID, Fingerprint: params.Fingerprint})
	if err != nil {
		return nil, err
	}

	if res == nil {
		return nil, fmt.Errorf("failed to find the corresponding error for fingerprint: %v", params.Fingerprint)
	}
	if errorStatusStr(params.Body.Status) == errorIgnoredStr {
		err = db.insertErrorTrackingIgnoredError(params)
	} else {
		err = db.updateErrorTrackingIgnoredError(params)
	}

	// The status was updated but the object doesn't yet reflect that change. To
	// avoid doing another query we return the object with the updated status.
	if err == nil {
		res.Status = params.Body.Status
	}

	return res, err
}

func (db *database) insertErrorTrackingIgnoredError(params errors.UpdateErrorParams) error {
	value := &et.ErrorTrackingIgnoredError{
		ProjectID:   params.ProjectID,
		Fingerprint: params.Fingerprint,
		UserID:      uint64(params.Body.UpdatedByID),
		UpdatedAt:   time.Now(),
	}
	return db.insert(value)
}

func (db *database) updateErrorTrackingIgnoredError(params errors.UpdateErrorParams) error {
	status, ok := errorStatusToInt[errorStatusStr(params.Body.Status)]
	if !ok {
		return fmt.Errorf("unexpected error status: %v", params.Body.Status)
	}
	errorStatus := &et.ErrorTrackingErrorStatus{
		ProjectID:   params.ProjectID,
		Fingerprint: params.Fingerprint,
		Status:      uint8(status),
		UserID:      uint64(params.Body.UpdatedByID),
		Actor:       uint8(0),
		UpdatedAt:   time.Now(),
	}
	err := db.insert(errorStatus)
	if err != nil {
		return err
	}

	q := &queryBuilder{}
	//nolint:lll
	// Deletion relies on light weight deletes.
	// Note(Arun): We should be moving tables names as constants but that is TBD. See https://gitlab.com/gitlab-org/opstrace/opstrace/-/issues/2138.
	// Reference on how DELETE works: https://clickhouse.com/docs/en/sql-reference/statements/delete#lightweight-delete-internal
	q.reset("DELETE FROM gl_error_tracking_ignored_errors WHERE")
	q.build(" project_id = ?", params.ProjectID)
	q.build(" AND fingerprint = ? ", params.Fingerprint)

	err = db.conn.Exec(context.Background(), q.sql, q.args...)
	if err != nil {
		return fmt.Errorf("failed to update error: %w", err)
	}
	return nil
}
