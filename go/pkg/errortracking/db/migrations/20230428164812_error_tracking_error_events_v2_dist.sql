-- +goose Up
-- +goose StatementBegin
CREATE TABLE IF NOT EXISTS gl_error_tracking_events_v2 ON CLUSTER '{cluster}' AS gl_error_tracking_events_v2_local
    ENGINE = Distributed('{cluster}', currentDatabase(), gl_error_tracking_events_v2_local, cityHash64(project_id, event_id));
-- +goose StatementEnd

-- +goose Down
-- +goose StatementBegin
-- +goose StatementEnd
