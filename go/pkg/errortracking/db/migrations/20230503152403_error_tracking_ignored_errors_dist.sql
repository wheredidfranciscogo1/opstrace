-- +goose Up
-- +goose StatementBegin
CREATE TABLE IF NOT EXISTS gl_error_tracking_ignored_errors ON CLUSTER '{cluster}' AS gl_error_tracking_ignored_errors_local
    ENGINE = Distributed('{cluster}', currentDatabase(), gl_error_tracking_ignored_errors_local, cityHash64(project_id, fingerprint));
-- +goose StatementEnd

-- +goose Down
-- +goose StatementBegin
-- +goose StatementEnd
