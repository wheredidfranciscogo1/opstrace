-- +goose Up
-- +goose StatementBegin
CREATE TABLE IF NOT EXISTS gl_error_tracking_events_v2_local ON CLUSTER '{cluster}' (
  event_id UUID,
  project_id UInt64,
  timestamp DateTime64(6, 'UTC'), -- Precision till microseconds
  is_deleted UInt8,

  type Nullable(String),
  fingerprint FixedString(32),

  title Nullable(String),
  message Nullable(String),
  platform LowCardinality(Nullable(String)),
  environment LowCardinality(Nullable(String)),
  level LowCardinality(Nullable(String)),

  transaction Nullable(String),
  actor Nullable(String), -- actor is built from "transaction" key or first item in stacktrace that has a function and module name.

  tags Nested
  (
    key Nullable(String),
    value Nullable(String)
  ),

  user_id Nullable(String),
  username Nullable(String),

  exception_stacks Nested
  (
    type Nullable(String),
    value Nullable(String),
    mechanism_type Nullable(String),
    mechanism_handled Nullable(UInt8),
    module Nullable(String),
    thread_id Nullable(String)
  ),

  exception_stacktrace_frames Nested
  (
    abs_path Nullable(String),
    colno Nullable(UInt32),
    filename Nullable(String),
    function Nullable(String),
    lineno Nullable(UInt32),
    in_app Nullable(UInt8),
    package Nullable(String),
    module Nullable(String)
  ),

  payload String CODEC(LZ4HC(9)) -- AVG 20K bytes

)
ENGINE = ReplicatedReplacingMergeTree('/clickhouse/{cluster}/tables/{shard}/errortracking_api.gl_error_tracking_events_v2_local', '{replica}', is_deleted)
PARTITION BY toYYYYMM(timestamp)
ORDER BY (project_id, timestamp, fingerprint, event_id);
-- +goose StatementEnd

-- +goose Down
-- +goose StatementBegin
-- +goose StatementEnd
