-- +goose Up
-- +goose StatementBegin
CREATE TABLE IF NOT EXISTS gl_error_tracking_error_status ON CLUSTER '{cluster}'
(
  project_id UInt64,
  fingerprint UInt32,
  status UInt8, -- 0 unresolved, 1 resolved
  user_id UInt64,
  actor UInt8, -- 0 status changed by user, 1 status changed by system (new event happened after resolve)
  updated_at DateTime64(6, 'UTC')
)
ENGINE = ReplicatedReplacingMergeTree('/clickhouse/{cluster}/tables/{shard}/errortracking_api.gl_error_tracking_error_status', '{replica}', updated_at)
ORDER BY (project_id, fingerprint);
-- +goose StatementEnd

-- +goose Down
-- +goose StatementBegin
-- +goose StatementEnd
