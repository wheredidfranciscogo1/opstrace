-- +goose Up
-- +goose StatementBegin
ALTER TABLE gl_error_tracking_error_events MODIFY TTL toDateTime(occurred_at) + INTERVAL 90 day ;
-- +goose StatementEnd

-- +goose Down
-- +goose StatementBegin
-- +goose StatementEnd
