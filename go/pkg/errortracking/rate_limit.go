//go:generate go run github.com/vektra/mockery/v2 --name RateLimiter --dir . --structname RateLimiterMock --output ./test/ --outpkg test --filename rate_limiter.go
package errortracking

import (
	"context"
	"fmt"
	"strconv"
	"sync"
	"time"

	"github.com/go-redis/redis_rate/v10"
	"github.com/prometheus/client_golang/prometheus"
	"github.com/redis/go-redis/extra/redisprometheus/v9"
	"github.com/redis/go-redis/v9"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/ratelimiting"
)

const (
	IDTypeProject = "project"
	IDTypeGroup   = "organization"

	EndpointTypeWrite = "write"
	EndpointTypeRead  = "read"
)

func GetRedis(
	redisAddr, redisPassword string,
	useRedisSentinel bool,
	redisConnectionPoolSize int,
) *redis.Client {
	var redisClient *redis.Client
	if useRedisSentinel {
		redisClient = redis.NewFailoverClient(&redis.FailoverOptions{
			MasterName:    "mymaster",
			SentinelAddrs: []string{redisAddr},
			Password:      redisPassword,
			DB:            0, // use default DB
			PoolSize:      redisConnectionPoolSize,
		})
	} else {
		redisClient = redis.NewClient(&redis.Options{
			Addr:     redisAddr,
			Password: redisPassword,
			DB:       0,
			PoolSize: redisConnectionPoolSize,
		})
	}
	prometheus.MustRegister(redisprometheus.NewCollector("redis", "", redisClient))

	return redisClient
}

func NewRateLimiter(limiter RateLimiterBackend) RateLimiter {
	res := new(rateLimiterT)

	res.ratelimitCounter = prometheus.NewCounterVec(
		prometheus.CounterOpts{
			Name: "http_ratelimit_total",
			Help: "number of requests by status code, method and path",
		},
		[]string{"id", "endpointName", "allowed"},
	)
	prometheus.MustRegister(res.ratelimitCounter)

	res.limiter = limiter

	return res
}

type RateLimiter interface {
	IsAllowed(int, string, string) (*redis_rate.Result, error)
	SetLimits(*ratelimiting.LimitsConfig)
}

type RateLimiterBackend interface {
	Allow(context.Context, string, redis_rate.Limit) (*redis_rate.Result, error)
}

func NewNullRateLimiter() RateLimiter {
	return new(nullRateLimiterT)
}

// nullRateLimiterT is a limiter that does no limiting at all. It is used in
// tests and in cases when rate-limiting should be disabled.
type nullRateLimiterT struct{}

func (rl *nullRateLimiterT) SetLimits(_ *ratelimiting.LimitsConfig) {
}

func (rl *nullRateLimiterT) IsAllowed(_ int, _, _ string) (*redis_rate.Result, error) {
	return nil, nil
}

type rateLimiterT struct {
	ratelimitCounter *prometheus.CounterVec
	limiter          RateLimiterBackend
	limits           *ratelimiting.LimitsConfig
	limitsMutex      sync.RWMutex
}

func (rl *rateLimiterT) SetLimits(newLimits *ratelimiting.LimitsConfig) {
	rl.limitsMutex.Lock()
	defer rl.limitsMutex.Unlock()

	rl.limits = newLimits
}

func (rl *rateLimiterT) getLimit(idType, endpointname string, id int) redis_rate.Limit {
	// NOTE(prozlach): We should be using PerMInute limits here instead of
	// per-hour, as redis-rate does not adjust the data stored in Redis after
	// updating the limits, so even though we e.g. bumped limits for given
	// sizing, we would get inconsistent limits for the remaining part of the
	// hour.
	// The solution is to use PerMinute instead, so the limits one changed,
	// will get relativelly quickly refreshed.
	rl.limitsMutex.RLock()
	defer rl.limitsMutex.RUnlock()

	if rl.limits == nil {
		// If limits configuration is not set, return the default.
		// This branch is just a precaution though, as the initialization of the
		// limits in `main.go` will not let the program start if the limits
		// could not be loaded.
		return redis_rate.PerMinute(120)
	}

	// Check for overrides first:
	var binding string
	var ok bool
	if idType == IDTypeGroup {
		binding, ok = rl.limits.Bindings.Groups[id]
	} else {
		binding, ok = rl.limits.Bindings.Projects[id]
	}
	if ok {
		sizing := rl.limits.Sizings[binding]
		if endpointname == EndpointTypeWrite {
			return redis_rate.PerMinute(int(sizing.ETAPIWritesLimit))
		} else {
			return redis_rate.PerMinute(int(sizing.ETAPIReadsLimit))
		}
	}

	// No group/project-specific overrides, return defaults:
	sizing := rl.limits.Sizings["default"]
	if endpointname == EndpointTypeWrite {
		return redis_rate.PerMinute(int(sizing.ETAPIWritesLimit))
	} else {
		return redis_rate.PerMinute(int(sizing.ETAPIReadsLimit))
	}
}

// IsAllowed determines if the given request is allowed:
//   - id - either project or group ID
//   - idType - defines whether id is of type Group or Project
//   - endpointName - for now we just specify `write` or `read`, later we may
//     opt into more fine grained categories defined here:
//     https://github.com/getsentry/relay/blob/d16851ecce0bf9311d48f618ecce12cb3b3b2acc/relay-common/src/constants.rs#L97-L115
//
//nolint:lll
func (rl *rateLimiterT) IsAllowed(id int, idType, endpointName string) (*redis_rate.Result, error) {
	// NOTE(prozlach): 5s value is chosen arbitrary, may need tuning depending
	// on how redis behaves
	ctx, cancelF := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancelF()

	limitID := fmt.Sprintf("%s:%d:%s", idType, id, endpointName)
	limit := rl.getLimit(idType, endpointName, id)
	result, err := rl.limiter.Allow(ctx, limitID, limit)
	if err != nil {
		return nil, fmt.Errorf("unable to determine if rate limit quota is exceeded: %w", err)
	}

	rl.ratelimitCounter.WithLabelValues(
		strconv.FormatUint(uint64(id), 10),
		endpointName,
		fmt.Sprintf("%v", result.Allowed > 0),
	)

	return result, nil
}

type RequestPermittedHeadersSetter interface {
	SetXSentryRateLimitLimit(xSentryRateLimitLimit int64)
	SetXSentryRateLimitRemaining(xSentryRateLimitRemaining int64)
	SetXSentryRateLimitReset(xSentryRateLimitReset int64)
}

func SetPermittedResponseHeaders(
	limitsData *redis_rate.Result, response RequestPermittedHeadersSetter,
) {
	response.SetXSentryRateLimitLimit(int64(limitsData.Limit.Rate))
	response.SetXSentryRateLimitRemaining(int64(limitsData.Remaining))
	response.SetXSentryRateLimitReset(time.Now().Add(limitsData.ResetAfter).Unix())
}

type RequestDeniedHeadersSetter interface {
	SetRetryAfter(retryAfter int64)
	SetXSentryRateLimitLimit(xSentryRateLimitLimit int64)
	SetXSentryRateLimitReset(xSentryRateLimitReset int64)
	SetXSentryRateLimits(xSentryRateLimits string)
}

func SetDeniedResponseHeaders(
	limitsData *redis_rate.Result, response RequestDeniedHeadersSetter, idType string,
) {
	retryAfter := int64(limitsData.ResetAfter.Seconds())

	response.SetRetryAfter(retryAfter)
	response.SetXSentryRateLimitLimit(int64(limitsData.Limit.Rate))
	response.SetXSentryRateLimitReset(time.Now().Add(limitsData.ResetAfter).Unix())

	// From https://develop.sentry.dev/sdk/rate-limiting/
	// X-Sentry-Rate-Limits: <int1>::<str1>
	// * str1 - `organization` for group, `project` for project
	// * int1 - same as Retry-After header - number of seconds until this
	// limit expires
	response.SetXSentryRateLimits(fmt.Sprintf("%d::%s", retryAfter, idType))
}
