package types_test

import (
	"bytes"
	"compress/zlib"
	"encoding/base64"
	"fmt"
	"io"
	"os"
	"path/filepath"
	"strings"
	"testing"

	"github.com/stretchr/testify/assert"

	et "gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/errortracking/models"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/errortracking/types"
)

type testCase struct {
	name     string
	filename string
}

func TestNewEnvelopeFrom(t *testing.T) {
	executeTest := func(path string) {
		matches, err := filepath.Glob(path)
		assert.Nil(t, err, "failed to load the testdata files")
		assert.NotEqual(t, 0, len(matches))
		var cases = make([]testCase, len(matches))
		for i, name := range matches {
			cases[i] = testCase{
				name:     "should parse envelope in " + name,
				filename: name,
			}
		}

		for _, tc := range cases {
			t.Log(tc.name)
			payload, err := os.ReadFile(tc.filename)
			assert.Nil(t, err, "failed to load test data")

			e, err := types.NewEnvelopeFrom(payload)
			assert.Nil(t, err, "failed to parse test envelope from %v", tc.filename)

			for _, item := range e.Items {
				switch item.Type.Type {
				case types.EventType:
					event, err := types.NewEventFrom(item.Payload)
					assert.Nil(t, err, "failed to parse event from envelope")

					assert.NotPanics(t, func() {
						et.NewErrorTrackingErrorEvent(uint64(1), event, payload)
					}, "failed to convert error event")
				case types.SessionType:
					session, err := types.NewSessionFrom(item.Payload)
					assert.Nil(t, err, "failed to parse session from envelope")
					assert.NotPanics(t, func() {
						et.NewErrorTrackingSession(uint64(1), session, payload)
					}, "failed to convert session event")
				}
			}
		}
	}

	t.Run("Envelopes for messages", func(t *testing.T) {
		// Messages are also events
		executeTest("../testdata/message/ruby_message_envelope.txt")
	})

	t.Run("Envelopes for exceptions", func(t *testing.T) {
		executeTest("../testdata/exceptions/*envelope*.txt")
	})

	t.Run("Envelopes for transactions", func(t *testing.T) {
		path := "../testdata/transactions/node-envelope.txt"
		tc := testCase{
			name:     "should parse envelope of a transaction item" + path,
			filename: path,
		}

		t.Log(tc.name)
		payload, err := os.ReadFile(tc.filename)
		assert.Nil(t, err, "failed to load test data")

		_, err = types.NewEnvelopeFrom(payload)
		assert.Nil(t, err)
	})

	t.Run("Envelopes for sessions types", func(t *testing.T) {
		matches, err := filepath.Glob("../testdata/sessions/*_session_envelope.txt")
		assert.Nil(t, err, "failed to load the testdata files")
		assert.NotEqual(t, 0, len(matches))
		var cases = make([]testCase, len(matches))
		for i, name := range matches {
			cases[i] = testCase{
				name:     "should parse envelope in " + name,
				filename: name,
			}
		}

		for _, tc := range cases {
			t.Log(tc.name)
			payload, err := os.ReadFile(tc.filename)
			assert.Nil(t, err, "failed to load test data")

			_, err = types.NewEnvelopeFrom(payload)
			assert.Nil(t, err, "session error was not nil")
		}
	})
}

func TestNewEventFrom(t *testing.T) {
	matches, err := filepath.Glob("../testdata/exceptions/*event*.json")
	assert.Nil(t, err, "failed to load the testdata files")
	assert.NotEqual(t, 0, len(matches))
	var cases = make([]testCase, len(matches))
	for i, name := range matches {
		cases[i] = testCase{
			name:     "should parse event in " + name,
			filename: name,
		}
	}

	for _, tc := range cases {
		t.Logf(tc.name)
		payload, err := os.ReadFile(tc.filename)
		assert.Nil(t, err, "failed to load test data")

		e, err := types.NewEventFrom(payload)
		assert.Nil(t, err, "failed to parse event")

		assert.NotPanics(t, func() {
			et.NewErrorTrackingErrorEvent(uint64(1), e, payload)
		}, "failed to convert error event")
	}
}

func TestCompressedEventPayload(t *testing.T) {
	payload, err := os.ReadFile("../testdata/exceptions/ruby_base64_zlib.txt")
	assert.Nil(t, err)

	raw, err := base64.StdEncoding.DecodeString(string(payload))
	assert.Nil(t, err)

	r := bytes.NewReader(raw)
	zr, err := zlib.NewReader(r)
	assert.Nil(t, err)

	result, err := io.ReadAll(zr)
	assert.Nil(t, err)

	_, err = types.NewEventFrom(result)
	assert.Nil(t, err)
}

func TestExtractDataItemType(t *testing.T) {
	assert := assert.New(t)

	t.Run("Works for exception", func(t *testing.T) {
		matches, err := filepath.Glob("../testdata/exceptions/*event*.json")
		assert.Nil(err)
		assert.NotEqual(0, len(matches))
		var cases = make([]testCase, len(matches))
		for i, name := range matches {
			cases[i] = testCase{
				name:     "should extract exception event type " + name,
				filename: name,
			}
		}

		for _, tc := range cases {
			payload, err := os.ReadFile(tc.filename)
			assert.Nil(err)

			e, err := types.NewEventFrom(payload)
			assert.Nil(err)
			assert.Equal(types.SupportedTypeException, e.ExtractDataItemType())
		}
	})

	t.Run("Works for message", func(t *testing.T) {
		matches, err := filepath.Glob("../testdata/message/*message*.json")
		assert.Nil(err)
		assert.NotEqual(0, len(matches))
		var cases = make([]testCase, len(matches))
		for i, name := range matches {
			cases[i] = testCase{
				name:     "should extract message event type " + name,
				filename: name,
			}
		}

		for _, tc := range cases {
			payload, err := os.ReadFile(tc.filename)
			assert.Nil(err)

			e, err := types.NewEventFrom(payload)
			assert.Nil(err)

			assert.Equal(types.SupportedTypeMessage, e.ExtractDataItemType())
		}
	})

	t.Run("Works for unsupported", func(t *testing.T) {
		matches, err := filepath.Glob("../testdata/unknown_type.json")
		assert.Nil(err)
		assert.Equal(1, len(matches))
		file := matches[0]

		payload, err := os.ReadFile(file)
		assert.Nil(err)

		e, err := types.NewEventFrom(payload)
		assert.Nil(err)

		assert.Equal(types.UnsupportedDataItem, e.ExtractDataItemType())
	})
}

func TestValidateMessage(t *testing.T) {
	assert := assert.New(t)
	var e types.Event
	// Validate correct message event
	e = types.Event{Platform: "platform", Message: "message"}
	assert.Nil(e.ValidateMessage())

	// Validate incorrect Message event due to missing platform
	e = types.Event{Message: "message"}
	err := e.ValidateMessage()
	assert.True(strings.Contains(err.Error(), "platform is not set"))

	// Validate incorrect Message event due to missing Message
	e = types.Event{Platform: "Platform"}
	err = e.ValidateMessage()
	assert.True(strings.Contains(err.Error(), "message is not set"))
}

func TestValidateStatus(t *testing.T) {
	assert := assert.New(t)

	// Validate correct status
	session := types.Session{Status: "ok"}
	assert.Nil(session.ValidateStatus())

	// Validate incorrect status
	session = types.Session{Status: "something wrong"}
	assert.Equal(fmt.Errorf("session status unknown: something wrong"), session.ValidateStatus())
}

func TestValidate(t *testing.T) {
	assert := assert.New(t)
	releaseSessionErr := fmt.Errorf("validation failed: session is missing required filed Release or SessionID")

	t.Run("Fails if SessionID is empty", func(t *testing.T) {
		s := types.Session{Status: "ok", Attributes: types.SessionAttributes{Release: "1"}, SessionID: ""}
		assert.Equal(releaseSessionErr, s.Validate())
	})
	t.Run("Fails if Release is empty", func(t *testing.T) {
		s := types.Session{Status: "ok", Attributes: types.SessionAttributes{Release: ""}, SessionID: "1"}
		assert.Equal(releaseSessionErr, s.Validate())
	})
	t.Run("Fails if SessionID and Release are empty", func(t *testing.T) {
		s := types.Session{Status: "ok", Attributes: types.SessionAttributes{Release: ""}, SessionID: ""}
		assert.Equal(releaseSessionErr, s.Validate())
	})
	t.Run("Fails if status is wrong", func(t *testing.T) {
		s := types.Session{Status: "something", Attributes: types.SessionAttributes{Release: "1"}, SessionID: "1"}
		expected := fmt.Errorf("validation failed: session status unknown: something")
		assert.Equal(expected.Error(), s.Validate().Error())
	})
	t.Run("Succeeds", func(t *testing.T) {
		s := types.Session{Status: "ok", Attributes: types.SessionAttributes{Release: "1"}, SessionID: "1"}
		assert.Nil(s.Validate())
	})
}

func TestSessionUnmarshalJSON(t *testing.T) {
	matches, err := filepath.Glob("../testdata/sessions/error_cases/with_*.txt")
	assert.Nil(t, err, "failed to load the testdata files")
	assert.NotEqual(t, 0, len(matches))
	var cases = make([]testCase, len(matches))
	for i, name := range matches {
		cases[i] = testCase{
			name:     "should unmarshal " + name,
			filename: name,
		}
	}

	for _, tc := range cases {
		t.Logf(tc.name)
		payload, err := os.ReadFile(tc.filename)
		assert.Nil(t, err, "failed to load test data")

		var s types.Session
		err = s.UnmarshalJSON(payload)
		assert.NotNil(t, err, "failed to parse session")
	}

	t.Run("Succeeds", func(t *testing.T) {
		mathces, err := filepath.Glob("../testdata/sessions/session_payload.txt")
		assert.Nil(t, err, "failed to load the testdata files")
		assert.Equal(t, 1, len(mathces))
		match := mathces[0]
		tc := testCase{
			name:     "should unmarshal " + match,
			filename: match,
		}

		t.Logf(tc.name)
		payload, err := os.ReadFile(tc.filename)
		assert.Nil(t, err, "failed to load test data")

		var s types.Session
		err = s.UnmarshalJSON(payload)
		assert.Nil(t, err, "failed to parse session")
	})
}
