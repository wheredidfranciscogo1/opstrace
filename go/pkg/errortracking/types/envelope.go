package types

import (
	"bufio"
	"bytes"
	"encoding/json"
	"errors"
	"fmt"
	"io"
	"time"
)

// Some envelope sizes can be bigger, we clamp it down at 1 MB.
const maxPayloadSize = 1024 * 1024

// Sentry Envelopes follow a data format similar to HTTP form data,
// see [https://develop.sentry.dev/sdk/envelopes/] for further details.
//
// The full grammar for an Envelope is:
// Envelope = Headers { "\n" Item } [ "\n" ] ;
// Item = Headers "\n" Payload ;
// Payload = { * } ;
type Envelope struct {
	Headers *EnvelopeHeaders
	Items   []Item
}

type EnvelopeHeaders struct {
	EventID string       `json:"event_id"`
	SentAt  time.Time    `json:"sent_at,omitempty"`
	SDK     *EnvelopeSDK `json:"sdk"`
}

type EnvelopeSDK struct {
	Name    string `json:"name,omitempty"`
	Version string `json:"version,omitempty"`
}

type Item struct {
	Type    ItemType
	Payload []byte
}

type ItemType struct {
	Type   string `json:"type"`
	Length int    `json:"length"`
}

// NewEnvelopeFrom attempts to serialize the given payload into
// an Envelope object. The structure of this object follows the
// grammar as documented above in the corresponding struct
// definition - simply put:
//
// An Envelope consists of EnvelopeHeaders and a list of Items
// where each Item itself consists of an ItemType (headers) and
// its corresponding serialized payload as a []byte.
func NewEnvelopeFrom(payload []byte) (*Envelope, error) {
	r := bytes.NewReader(payload)
	s := &envelopeScanner{
		s: bufio.NewReaderSize(r, maxPayloadSize),
	}

	e := &Envelope{Headers: &EnvelopeHeaders{}}
	if err := s.do(e.Headers, decodeJSON); err != nil {
		if errors.Is(errors.Unwrap(err), io.EOF) {
			return nil, nil
		}
		return nil, err
	}

	for {
		var itemType ItemType
		if err := s.do(&itemType, decodeJSON); err != nil {
			if errors.Is(err, io.EOF) {
				break
			}
			return nil, err
		}

		var payload []byte
		if err := s.do(&payload, decodeRaw); err != nil {
			if errors.Is(err, io.EOF) {
				break
			}
			return nil, err
		}

		e.Items = append(e.Items, Item{Type: itemType, Payload: payload})
	}

	return e, nil
}

type envelopeScanner struct {
	s *bufio.Reader
}

//nolint:wrapcheck
func decodeJSON(payload []byte, v interface{}) error {
	return json.Unmarshal(payload, v)
}

func decodeRaw(payload []byte, v interface{}) error {
	buf, ok := v.(*[]byte)
	if !ok {
		return fmt.Errorf("expected a pointer to a byte slice")
	}
	*buf = payload // copy payload as is
	return nil
}

func (e *envelopeScanner) do(v interface{}, decoder func([]byte, interface{}) error) error {
	payload, isPrefix, err := e.s.ReadLine()
	// Quoting from docs:
	// If the line was too long for the buffer then isPrefix is set and the beginning of the line is returned.
	if isPrefix {
		return fmt.Errorf("envelope payload too large( > 1MB)")
	}
	if err != nil {
		return fmt.Errorf("failed to read payload: %w", err)
	}
	err = decoder(payload, v)
	if err != nil {
		return fmt.Errorf("failed to unmarshal payload key: %w", err)
	}
	return nil
}
