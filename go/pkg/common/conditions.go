package common

const (
	// ReconciliationSucceededReason represents the fact that the reconciliation of
	// the resource has succeeded.
	ReconciliationSuccessReason string = "Success"

	// ReconciliationSuccessReason represents the fact that the reconciliation of
	// the resource has succeeded and is ready. Expectation is that the resource
	// checks itself for readiness before attaching this condition.
	ConditionTypeReady string = "Ready"

	// comes into play to catch something that is taking too long to become Ready
	// ReconciliationFailedReason represents the fact that the reconciliation of
	// the resource has failed.
	ReconciliationFailedReason string = "Reconciling"
)
