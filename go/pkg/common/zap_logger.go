package common

import (
	"fmt"

	"go.uber.org/zap"
	"go.uber.org/zap/zapcore"
)

func SetupLogger(logLevel string) (*zap.SugaredLogger, func(), zap.Config) {
	logLevelParsed, err := zap.ParseAtomicLevel(logLevel)
	if err != nil {
		panic(fmt.Errorf("parsing log level %q: %w", logLevel, err).Error())
	}

	loggerCfg := zap.Config{
		Level:             logLevelParsed,
		Development:       false,
		DisableCaller:     false,
		DisableStacktrace: true,
		Encoding:          "json",
		EncoderConfig:     getEncoderConfig(),
		OutputPaths:       []string{"stderr"},
		ErrorOutputPaths:  []string{"stderr"},
	}
	logger, err := loggerCfg.Build()
	if err != nil {
		panic(fmt.Errorf("building logger: %w", err).Error())
	}

	syncF := func() {
		if err := logger.Sync(); err != nil {
			fmt.Printf("failed to flush logger: %v\n", err)
		}
	}

	return logger.Sugar(), syncF, loggerCfg
}

func getEncoderConfig() zapcore.EncoderConfig {
	encoderConfig := zap.NewProductionEncoderConfig()
	encoderConfig.EncodeTime = zapcore.ISO8601TimeEncoder
	return encoderConfig
}
