package ratelimiting

import (
	"errors"
	"fmt"
	"os"

	"sigs.k8s.io/yaml"
)

/*
---
ApiVersion: 1
sizings:
  default:
    errortracking_api_reads: <uint64>
    errortracking_api_writes: <uint64>
    ...
  xl:
    ...
  xxl:
    ...
bindings:
  groups:
    22: xl
  projects:
    1: xxl
*/

type Limits struct {
	ETAPIReadsLimit  int64 `json:"errortracking_api_reads"`
	ETAPIWritesLimit int64 `json:"errortracking_api_writes"`

	TRBytesWriteLimit int64 `json:"tracing_bytes_write"`
}

type LimitsConfig struct {
	APIVersion uint64            `json:"api_version"`
	Sizings    map[string]Limits `json:"sizings"`
	Bindings   struct {
		Groups   map[int]string `json:"groups"`
		Projects map[int]string `json:"projects"`
	} `json:"bindings"`
}

var ErrInvalidLimitsConfiguration = errors.New("limits configuration is invalid")

func verifyConfig(in *LimitsConfig) error {
	if in.APIVersion != 1 {
		return fmt.Errorf("%w: usupported format of the limits config: %d", ErrInvalidLimitsConfiguration, in.APIVersion)
	}

	if _, ok := in.Sizings["default"]; !ok {
		return fmt.Errorf("%w: default limits configuration is not defined", ErrInvalidLimitsConfiguration)
	}

	for k, v := range in.Sizings {
		if v.ETAPIReadsLimit == 0 || v.ETAPIWritesLimit == 0 || v.TRBytesWriteLimit == 0 {
			return fmt.Errorf("%w: invalid limits for sizing %s", ErrInvalidLimitsConfiguration, k)
		}
	}

	for k, v := range in.Bindings.Groups {
		if _, ok := in.Sizings[v]; !ok {
			return fmt.Errorf(
				"%w: binding for group %d does not point to existing sizing %s",
				ErrInvalidLimitsConfiguration, k, v,
			)
		}
	}

	for k, v := range in.Bindings.Projects {
		if _, ok := in.Sizings[v]; !ok {
			return fmt.Errorf(
				"%w: binding for project %d does not point to existing sizing %s",
				ErrInvalidLimitsConfiguration, k, v,
			)
		}
	}

	return nil
}

func ReadConfig(path string) (*LimitsConfig, error) {
	blob, err := os.ReadFile(path)
	if err != nil {
		return nil, fmt.Errorf("error while reading limits file: %w", err)
	}
	res := new(LimitsConfig)

	err = yaml.Unmarshal(blob, res)
	if err != nil {
		return nil, fmt.Errorf("error occurred while parsing limits yaml: %w", err)
	}

	err = verifyConfig(res)
	if err != nil {
		return nil, fmt.Errorf("limits configuration is invalid: %w", err)
	}

	return res, nil
}
