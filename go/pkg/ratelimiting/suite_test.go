package ratelimiting

import (
	"testing"

	. "github.com/onsi/ginkgo/v2"
	. "github.com/onsi/gomega"

	log "github.com/sirupsen/logrus"
)

func TestRateLimiting(t *testing.T) {
	RegisterFailHandler(Fail)
	suiteConfig, reporterConfig := GinkgoConfiguration()

	RunSpecs(t, "Rate limiting", suiteConfig, reporterConfig)
}

var _ = BeforeSuite(func() {
	log.SetLevel(log.DebugLevel)
	log.SetOutput(GinkgoWriter)
})
