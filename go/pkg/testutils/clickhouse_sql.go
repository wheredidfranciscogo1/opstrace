package testutils

import (
	"fmt"
	"strings"
)

// For two SQL strings, this function does a line-by-line comparison while
// also normalizing any whitespace to avoid auto-formatting differences such
// as the usage of tabs vs spaces which can vary across development setups
// across contributors/developers.
//
// Returns no error if they're equal, fails with an error otherwise.
func CompareSQLStrings(actual, expected string) error {
	actualLines := strings.Split(actual, "\n")
	expectedLines := strings.Split(expected, "\n")

	if len(actualLines) != len(expectedLines) {
		return fmt.Errorf(
			"found line lengths unequal, actual: %d, expected: %d",
			len(actualLines),
			len(expectedLines),
		)
	}

	for idx := 0; idx < len(expectedLines); idx++ {
		normalizedWhiteSpaceActual := strings.Join(strings.Fields(actualLines[idx]), " ")
		normalizedWhiteSpaceExpected := strings.Join(strings.Fields(expectedLines[idx]), " ")
		if normalizedWhiteSpaceActual != normalizedWhiteSpaceExpected {
			return fmt.Errorf(
				"found string content unequal on line: %d\nDiff - actual: %s\nDiff - expected: %s",
				idx+1,
				normalizedWhiteSpaceActual,
				normalizedWhiteSpaceExpected,
			)
		}
	}
	return nil
}
