package testutils

//nolint:lll
// portforward proxy test helpers, adapted from kubectl cmd/portforward.
// https://github.com/kubernetes/kubectl/blob/197123726db24c61aa0f78d1f0ba6e91a2ec2f35/pkg/cmd/portforward/portforward.go

import (
	"context"
	"fmt"
	"net/http"
	"net/url"
	"os"
	"path"
	"time"

	"k8s.io/cli-runtime/pkg/resource"
	"k8s.io/client-go/discovery"
	"k8s.io/client-go/discovery/cached/memory"
	"k8s.io/client-go/restmapper"
	"k8s.io/client-go/tools/clientcmd"
	"k8s.io/client-go/tools/portforward"
	"k8s.io/client-go/transport/spdy"
	"k8s.io/kubectl/pkg/polymorphichelpers"
	"k8s.io/kubectl/pkg/scheme"
	"k8s.io/kubectl/pkg/util"

	corev1 "k8s.io/api/core/v1"
	"k8s.io/apimachinery/pkg/api/meta"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	clientset "k8s.io/client-go/kubernetes"
	"k8s.io/client-go/rest"
)

type PortForwardOptions struct {
	// LocalPort for forwarding. 0 picks a random free port.
	LocalPort uint16
	// RemotePort for forwarding. Must match a service or pod port.
	RemotePort uint16
	// Namespace to target.
	Namespace string
	// Resource type/name such as deployment/foo or pod/foo.
	// Can be a service name.
	Resource string

	// derived fields
	podName       string
	targetPort    int
	restConfig    *rest.Config
	client        *clientset.Clientset
	stopCh        chan struct{}
	readyCh       chan struct{}
	portForwarder *portforward.PortForwarder
}

// PortForward will try to configure a port-forward proxy for the given opts.
// Returns the local port number, closer function and an error.
// The closer must be used to shut down the proxy, but will not block.
func PortForward(restConfig *rest.Config, options PortForwardOptions) (localPort uint16, closer func(), err error) {
	if options.Resource == "" {
		return 0, nil, fmt.Errorf("port-forward resource name must be set")
	}
	if options.RemotePort == 0 {
		return 0, nil, fmt.Errorf("port-forward remote port must be set")
	}

	if options.Namespace == "" {
		options.Namespace = "default"
	}

	options.restConfig = restConfig
	options.client, err = clientset.NewForConfig(restConfig)
	if err != nil {
		return 0, nil, fmt.Errorf("clientset new for config: %w", err)
	}

	obj, err := resource.NewBuilder(options).
		WithScheme(scheme.Scheme, scheme.Scheme.PrioritizedVersionsAllGroups()...).
		ContinueOnError().
		NamespaceParam(options.Namespace).DefaultNamespace().
		ResourceNames("pods", options.Resource).
		Do().Object()
	if err != nil {
		return 0, nil, fmt.Errorf("resource builder: %w", err)
	}

	pod, err := polymorphichelpers.AttachablePodForObjectFn(options, obj, time.Second*10)
	if err != nil {
		return 0, nil, fmt.Errorf("attachable pod for object: %w", err)
	}
	options.podName = pod.Name

	// handle service port mapping to target port if needed
	switch t := obj.(type) {
	case *corev1.Service:
		options.targetPort, err = options.translateServicePortToTargetPort(*t, *pod)
		if err != nil {
			return 0, nil, fmt.Errorf("translate service port to target port: %w", err)
		}
	default:
		options.targetPort = int(options.RemotePort)
	}

	options.stopCh = make(chan struct{}, 2)
	options.readyCh = make(chan struct{})

	err = options.configurePortForward()
	if err != nil {
		return 0, nil, err
	}

	go func() {
		// ignore any port-forwarding error rather than do actual error handling.
		// the caller should handle the error when the port-forward is closed.
		// this can happen for many reasons, such as a pod being recycled.
		if err := options.portForwarder.ForwardPorts(); err != nil {
			options.stopCh <- struct{}{}
		}
	}()

	<-options.readyCh
	localPorts, err := options.portForwarder.GetPorts()
	if err != nil {
		return 0, nil, fmt.Errorf("port-forward get ports: %w", err)
	}
	if len(localPorts) != 1 {
		return 0, nil, fmt.Errorf("expected one local port, got %d", len(localPorts))
	}

	return localPorts[0].Local, func() {
		options.stopCh <- struct{}{}
	}, nil
}

func (o *PortForwardOptions) configurePortForward() error {
	pod, err := o.client.CoreV1().Pods(o.Namespace).Get(context.TODO(), o.podName, metav1.GetOptions{})
	if err != nil {
		return fmt.Errorf("pods get: %w", err)
	}

	if pod.Status.Phase != corev1.PodRunning {
		return fmt.Errorf("unable to forward port because pod is not running. Current status=%v", pod.Status.Phase)
	}

	targetURL, err := url.Parse(o.restConfig.Host)
	if err != nil {
		return fmt.Errorf("restconfig url parse: %w", err)
	}

	targetURL.Path = path.Join(
		"api", "v1",
		"namespaces", pod.Namespace,
		"pods", pod.Name,
		"portforward",
	)

	transport, upgrader, err := spdy.RoundTripperFor(o.restConfig)
	if err != nil {
		return fmt.Errorf("spdy setup: %w", err)
	}
	dialer := spdy.NewDialer(upgrader, &http.Client{Transport: transport}, "POST", targetURL)
	fw, err := portforward.New(
		dialer, []string{fmt.Sprintf("%d:%d", o.LocalPort, o.RemotePort)}, o.stopCh, o.readyCh, os.Stdout, os.Stderr)
	if err != nil {
		return fmt.Errorf("portforward new: %w", err)
	}
	o.portForwarder = fw

	return nil
}

// Translates service port to target port
// It rewrites ports as needed if the Service port declares targetPort.
// It returns an error when a named targetPort can't find a match in the pod, or the Service did not declare
// the port.
// Adapted from https://github.com/kubernetes/kubectl/blob/197123726db24c61aa0f78d1f0ba6e91a2ec2f35/pkg/cmd/portforward/portforward.go#L162.
//
//nolint:lll
func (o PortForwardOptions) translateServicePortToTargetPort(svc corev1.Service, pod corev1.Pod) (int, error) {
	containerPort, err := util.LookupContainerPortNumberByServicePort(svc, pod, int32(o.RemotePort))
	if err != nil {
		// can't resolve a named port, or Service did not declare this port, return an error
		return 0, fmt.Errorf("lookup container port: %w", err)
	}

	return int(containerPort), nil
}

func (o PortForwardOptions) ToRESTConfig() (*rest.Config, error) {
	return o.restConfig, nil
}

func (o PortForwardOptions) ToRESTMapper() (meta.RESTMapper, error) {
	dc, err := o.ToDiscoveryClient()
	if err != nil {
		return nil, err
	}
	return restmapper.NewDeferredDiscoveryRESTMapper(dc), nil
}

func (o PortForwardOptions) ToDiscoveryClient() (discovery.CachedDiscoveryInterface, error) {
	return memory.NewMemCacheClient(o.client), nil
}

func (o PortForwardOptions) ToRawKubeConfigLoader() clientcmd.ClientConfig {
	return nil
}
