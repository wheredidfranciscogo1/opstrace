package constants

import (
	_ "embed"
	"encoding/json"
	"fmt"
	"time"
)

//go:embed docker-images.json
var images []byte

type ImagesFromJSON struct {
	JaegerAllInOneImage   string `json:"jaegerAllInOne"`
	JaegerClickHouseImage string `json:"jaegerClickHouse"`
	JaegerOperatorImage   string `json:"jaegerOperator"`
	ClickHouseImage       string `json:"clickHouse"`
}

func OpstraceImages() ImagesFromJSON {
	var opstraceImages = ImagesFromJSON{}
	err := json.Unmarshal(images, &opstraceImages)
	if err != nil {
		panic(fmt.Sprintf("unable to unmarshal embedded images.json: %s", err))
	}

	return opstraceImages
}

// DockerImageTag is set by Makefiles via ldflags, needs to be exported.
// same of 2 other variables below.
var DockerImageTag string
var DockerImageRegistry string

func DockerImageName(name string) string {
	return fmt.Sprintf("%s/%s", DockerImageRegistry, name)
}

func DockerImageFullName(name string) string {
	return fmt.Sprintf("%s/%s:%s", DockerImageRegistry, name, DockerImageTag)
}

func DockerImageFullNameWithRegistry(registry, name string) string {
	return fmt.Sprintf("%s/%s:%s", registry, name, DockerImageTag)
}

// Controller.
const (
	RequeueDelay                                  = time.Second
	DefaultDriftPreventionInterval                = 5 * time.Minute
	SelectorLabelName                             = "app"
	HTTPSCertSecretName                           = "https-cert" // #nosec G101
	SelfSignedCACertName                          = "self-signed-ca"
	SelfSignedCACertSecretName                    = "self-signed-ca-secret" // #nosec G101
	SelfSignedCAIssuerName                        = "self-signed-ca-issuer"
	TenantLabelIdentifier                         = "opstrace.com/tenant"
	StorageClassName                              = "pd-ssd"
	StorageInventoryID                            = "storage"
	SelfSignedIssuer                              = "selfsigned-issuer"
	LetsEncryptProd                               = "letsencrypt-prod"
	LetsEncryptStaging                            = "letsencrypt-staging"
	ClusterFieldManagerIDString                   = "scheduler-cluster"
	GroupFieldManagerIDString                     = "scheduler-group"
	GitlabNamespaceFieldManagerIDString           = "scheduler-gns"
	GitLabObservabilityTenantFieldManagerIDString = "scheduler-obs-tenant"
	TenantFieldManagerIDString                    = "scheduler-tenant"
	// #nosec
	AuthSecretOAuthClientIDKey = "gitlab_oauth_client_id"
	// #nosec
	AuthSecretOAuthClientSecretKey = "gitlab_oauth_client_secret"

	// Cloudflare external-dns auth credentials.
	// #nosec
	CloudflareAPITokenKey = "CF_API_TOKEN"
	// #nosec
	CloudflareAPIKeyKey = "CF_API_KEY"
	// #nosec
	CloudflareAPIEmailKey = "CF_API_EMAIL"
)

// OpenTelemetry GOB Monitoring.
const (
	OpenTelemetryOperatorInventoryID        = "opentelemetry-operator"
	OpenTelemetryOperatorServiceAccountName = "opentelemetry-operator"
	OpenTelemetrySystemTracingCollector     = "system-tracing"
)

// Clickhouse.
const (
	ClickHouseOperatorName               = "clickhouse-operator"
	ClickHouseOperatorInventoryID        = "clickhouse-operator"
	ClickHouseOperatorServiceMonitorName = "clickhouse-operator"
	ClickHouseOperatorServiceAccountName = "clickhouse-operator"
	ClickHouseOperatorRoleBindingName    = "clickhouse-operator"

	ClickHouseImageName                      = "clickhouse-operator"
	ClickHouseOperatorCredentialsSecretName  = "clickhouse-operator-credentials" // #nosec G101
	ClickHouseOperatorUsername               = "clickhouse_operator"
	ClickHouseClusterServiceName             = "cluster"
	ClickHouseServiceMonitorName             = "clickhouse"
	ClickHouseSchedulerUsername              = "opstrace_scheduler"
	ClickHouseSchedulerCredentialsSecretName = "opstrace-scheduler-clickhouse-credentials" // #nosec G101
	ClickHouseCredentialsUserKey             = "user"
	ClickHouseCredentialsPasswordKey         = "password" // #nosec G101
	ClickHouseCredentialsHTTPEndpointKey     = "http-endpoint"
	ClickHouseCredentialsNativeEndpointKey   = "native-endpoint"
)

// Jaeger.
const (
	LastConfigAnnotation               = "last-config"
	JaegerOperatorInventoryID          = "jaeger-operator"
	JaegerOperatorName                 = "jaeger-operator"
	JaegerPluginConfigPrefix           = "jaeger-plugin-config"
	JaegerNamePrefix                   = "jaeger"
	JaegerOperatorServiceCertName      = "jaeger-operator-service-cert"
	JaegerClickhouseSecretName         = "jaeger-clickhouse-credentials" // #nosec G101
	JaegerClickhouseQuotaConfigmapName = "jaeger-clickhouse-quotas"
	JaegerDatabaseName                 = "tracing"
	JaegerGCSDatabaseName              = "tracing_gcs"
	JaegerS3DatabaseName               = "tracing_s3"

	TracingDatabaseName          = "tracing"
	TracingTableName             = "gl_traces_main_local"
	TracingDistTableName         = "gl_traces_main"
	TracingMVTargetTableName     = "gl_traces_rolled_local"
	TracingMVTargetDistTableName = "gl_traces_rolled"
	TracingMVName                = "gl_traces_mv_local"
)

// Metrics.
const (
	MetricsDatabaseName                          = "metrics"
	MetricsGaugeTableName                        = "metrics_main_gauge"
	MetricsGaugeMetadataTableName                = "metrics_main_gauge_metadata"
	MetricsGaugeMetadataMVName                   = "metrics_main_gauge_metadata_mv"
	MetricsSumTableName                          = "metrics_main_sum"
	MetricsSumMetadataTableName                  = "metrics_main_sum_metadata"
	MetricsSumMetadataMVName                     = "metrics_main_sum_metadata_mv"
	MetricsHistogramTableName                    = "metrics_main_histogram"
	MetricsHistogramMetadataTableName            = "metrics_main_histogram_metadata"
	MetricsHistogramMetadataMVName               = "metrics_main_histogram_metadata_mv"
	MetricsExponentialHistogramTableName         = "metrics_main_exp_histogram"
	MetricsExponentialHistogramMetadataTableName = "metrics_main_exp_histogram_metadata"
	MetricsExponentialHistogramMetadataMVName    = "metrics_main_exp_histogram_metadata_mv"
	//nolint:gosec
	MetricsGauge1mMVName    = "metrics_main_gauge_mv_1m"
	MetricsGauge1mTableName = "metrics_main_gauge_1m"
	//nolint:gosec
	MetricsGauge1hMVName    = "metrics_main_gauge_mv_1h"
	MetricsGauge1hTableName = "metrics_main_gauge_1h"
	//nolint:gosec
	MetricsGauge1dMVName    = "metrics_main_gauge_mv_1d"
	MetricsGauge1dTableName = "metrics_main_gauge_1d"
	MetricsSum1mMVName      = "metrics_main_sum_mv_1m"
	MetricsSum1mTableName   = "metrics_main_sum_1m"
	MetricsSum1hMVName      = "metrics_main_sum_mv_1h"
	MetricsSum1hTableName   = "metrics_main_sum_1h"
	MetricsSum1dMVName      = "metrics_main_sum_mv_1d"
	MetricsSum1dTableName   = "metrics_main_sum_1d"
)

// Logging.
const (
	LoggingDatabaseName = "logging"
	LoggingTableName    = "logs_main"
)

// Otel.
const (
	ArgusPodLabel               = "argus"
	OtelImageName               = "tracing-api"
	OtelIngressPortName         = "otlp-http"
	OtelJaegerIngressPortName   = "otlp-jaeger"
	OtelJaegerIngressNamePrefix = "opentelemetry-jaeger"
	OtelDeploymentNamePrefix    = "opentelemetry"
	OtelDeploymentConfigPrefix  = "opentelemetry-config"
	OtelPodLabel                = "opentelemetry"
)

// CertManager.
const (
	CertManagerName        = "cert-manager"
	CainjectorName         = "cert-manager-cainjector"
	CertManagerInventoryID = "cert-manager"
)

// Redis.
const (
	RedisOperatorInventoryID        = "redis-operator"
	RedisOperatorName               = "redis-operator"
	RedisName                       = "redis"
	RedisOperatorServiceMonitorName = "redis"
	RedisOperatorServiceAccountName = "redis-operator"
)

// ExternalDNS.
const (
	ExternalDNSInventoryID = "external-dns"
)

// Gatekeeper.
const (
	GatekeeperInventoryID            = "gatekeeper"
	GatekeeperImageName              = "gatekeeper"
	GatekeeperName                   = "gatekeeper"
	GatekeeperOperatorName           = "gatekeeper"
	GatekeeperServiceMonitorName     = "gatekeeper"
	GatekeeperServiceAccountName     = "gatekeeper"
	GatekeeperClusterRoleBindingName = "gatekeeper-clusteradmin-binding"
	SessionCookieSecretName          = "session-cookie-secret" // #nosec G101
)

const SessionCookieName = "gob.sid"

// Ingress.
const (
	IngressControllerDeploymentName     = "traefik"
	IngressControllerServiceName        = "traefik"
	IngressControllerServiceAccountName = "traefik"
	IngressControllerPodMonitorName     = "traefik"
	IngressControllerInventoryID        = "ingress-controller"
)

// Reloader.
const (
	ReloaderInventoryID        = "reloader"
	ReloaderServiceAccountName = "reloader-reloader"
)

// Prometheus.
const (
	PrometheusInventoryID           = "prometheus"
	PrometheusServiceAccountName    = "prometheus"
	PrometheusServiceMonitorName    = "prometheus"
	PrometheusClusterRoleBidingName = "prometheus"
	PrometheusRoleBidingName        = "prometheus"
)

// Prometheus-operator.
const (
	PrometheusOperatorName               = "prometheus-operator"
	PrometheusOperatorInventoryID        = "prometheus-operator"
	PrometheusOperatorServiceAccountName = "prometheus-operator"
	PrometheusOperatorServiceMonitorName = "prometheus-operator"
)

// TenantOperator.
const (
	TenantImageName    = "tenant-operator"
	TenantOperatorName = "tenant-operator"
	TenantName         = "opstrace-tenant"
)

// GitLabObservabilityTenant
const (
	GitLabObservabilityDatabaseName = "tracing"
	// #nosec
	GitLabObservabilityTenantCHCredentialsSecretName = "tenant-clickhouse-credentials"
	// #nosec
	GitLabObservabilityTenantCloudCHCredentialsSecretName = "tenant-cloud-clickhouse-credentials"
	GitLabObservabilityTenantNamespaceInventoryID         = "tenant-namespace"
)

// GitLabObservabilityTenant operator
const (
	OtelCollectorNamespaceComponentName = "namespace"
	OtelCollectorComponentName          = "otel-collector"
	OtelCollectorInventoryID            = "otel-collector"
	OtelCollectorComponentMetrics       = "otel-collector-metrics"
	OtelCollectorComponentTraces        = "otel-collector-traces"
	OtelCollectorComponentLogs          = "otel-collector-logs"
)

// Error Tracking API.
const (
	ErrortrackingAPIInventoryID  = "errortracking-api"
	ErrorTrackingOperatorName    = "errortracking-api"
	ErrorTrackingImageName       = "errortracking-api"
	ErrorTrackingAPIName         = "errortracking-api"
	ErrorTrackingAPIDatabaseName = "errortracking_api"

	// https://gitlab.com/gitlab-org/opstrace/opstrace/-/issues/1783
	// https://gitlab.com/gitlab-org/gitlab/-/merge_requests/91928
	GitlabInternalErrorTrackingEndpoint = "/api/v4/internal/error_tracking/allowed"

	///nolint
	GitlabErrorTrackingTokenHeader = "Gitlab-Error-Tracking-Token"
)

// Error tracking.
const (
	ErrorTrackingAPIQueueVolumeName       = "errortracking-queue"
	ErrorTrackingAPIQueueDataPVCName      = "errortracking-queue-data"
	ErrorTrackingAPIQueueDataMountPath    = "/etc/errortracking/queue"
	ErrorTrackingAPIQueueStorageClassName = "pd-ssd"
)

// Monitoring.
const (
	MonitoringInventoryID = "monitoring"
)

const (
	TraceQueryAPIInventoryID = "trace-query-api"
	TraceQueryAPIName        = "trace-query-api"
	TraceQueryV3APIName      = "trace-query-api-v3"
	TraceQueryAPIImageName   = "trace-query-api"
	TraceQueryAPIDBName      = "tracing"
)

const (
	ProvisioningAPIInventoryID = "provisioning-api"
	ProvisioningAPIName        = "provisioning-api"
	ProvisioningAPIImageName   = "provisioning-api"
)

const (
	QueryAPIInventoryID = "query-api"
	QueryAPIName        = "query-api"
	QueryAPIImageName   = "query-api"
)

// Swagger UI API docs.
const (
	SwaggerUIInventoryID = "swagger-ui"
)
