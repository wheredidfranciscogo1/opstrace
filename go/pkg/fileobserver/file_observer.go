package fileobserver

import (
	"crypto/sha256"
	"fmt"
	"os"
	"path"
	"sync"

	"github.com/rjeczalik/notify"
	log "github.com/sirupsen/logrus"
)

type FileActionFn func(chan struct{}) error

type FileObserver interface {
	Run() error
	Stop()
}

type fileObserverT struct {
	wg     *sync.WaitGroup
	doneCh chan struct{}

	filePath string
	checksum [sha256.Size]byte
	actionFn FileActionFn
	eventCh  chan notify.EventInfo
}

func NewFileObserver(filePath string, actionFn FileActionFn) FileObserver {
	res := &fileObserverT{
		wg: new(sync.WaitGroup),

		doneCh:   make(chan struct{}, 1),
		filePath: filePath,
		actionFn: actionFn,
	}

	return res
}

func (f *fileObserverT) calculateFileChecksum() ([sha256.Size]byte, error) {
	content, err := os.ReadFile(f.filePath)
	if err != nil {
		return [sha256.Size]byte{}, fmt.Errorf("failed to read the contents of %s: %w", f.filePath, err)
	}

	return sha256.Sum256(content), nil
}

func (f *fileObserverT) verifyFileChanges() (bool, error) {
	checksum, err := f.calculateFileChecksum()
	if err != nil {
		return false, fmt.Errorf("failed to verify if file changed: %w", err)
	}

	if checksum != f.checksum {
		f.checksum = checksum
		return true, nil
	}

	return false, nil
}

func (f *fileObserverT) mainLoop(syncCh chan struct{}) {
	defer f.wg.Done()

	log.WithFields(log.Fields{
		"filePath": f.filePath,
	}).Debug("main fileobserver loop is starting")

	close(syncCh)
	for {
		select {
		case <-f.doneCh:
			log.Warn("fileobserver received termination signal, bye!")
			return
		case event := <-f.eventCh:
			logFields := log.Fields{
				"event.Path": event.Path(),
				"event":      event.Event().String(),
			}

			// NOTE(prozlach): do not filter on path names, as k8s does some
			// funky linking of configmaps/secrets in containers. Hence we need
			// to investigate every event we get to make sure that the data
			// that link is pointing to has not changed. It is still better
			// than just periodically fetching the file, as the code reacts
			// quickly to changes and the changes in the directory are
			// connected to the change in the config map itself.
			// We may iterate on this once there is a use case where there is
			// huge number of files in a single directory, and each one of them
			// is changing often, as this would generate enough notifications
			// to start being problematic.
			fileChanged, err := f.verifyFileChanges()
			if err != nil {
				log.WithFields(logFields).WithError(err).
					Error("failed to determine if the actionFn should be run")
				continue
			}

			if !fileChanged {
				log.WithFields(logFields).
					Debug("event received but no changes have been detected")
				continue
			}

			if err := f.actionFn(f.doneCh); err != nil {
				log.WithFields(logFields).WithError(err).
					Error("fileobserver actionFn function failed")
			} else {
				log.WithFields(logFields).
					Info("fileobserver event has been processed")
			}
		}
	}
}

// Run starts the file observer.
func (f *fileObserverT) Run() error {
	// Make the channel sufficiently buffered to ensure that in case when event
	// is still processed, the next processing will start immediately after. We
	// need to have a buffer large enough to accommodate unrelated changes that
	// may otherwise block/cause dropping of interesting events
	f.eventCh = make(chan notify.EventInfo, 1024)

	err := notify.Watch(path.Dir(f.filePath), f.eventCh, eventMask)
	if err != nil {
		return fmt.Errorf(
			"failed to start inotify observer for %s: %w", f.filePath, err)
	}

	log.Info("fileobserver is starting")

	f.checksum, err = f.calculateFileChecksum()
	if err != nil {
		// E.g. file does not exist yet, ignore
		return fmt.Errorf("unable to determine the initial checksum of the file: %w", err)
	}

	// Lauch the actionFn as we do not really know if the files have changed
	// before we started or not. We also want to make sure that once the
	// processing switches to goroutine, it has good chance of success.
	log.WithFields(log.Fields{
		"filePath": f.filePath,
	}).Info("executing initial run of the actionFn function")
	if err = f.actionFn(f.doneCh); err != nil {
		return fmt.Errorf("initial run of actionFn  returned an error: %w", err)
	}

	f.wg.Add(1)
	syncCh := make(chan struct{})
	go f.mainLoop(syncCh)
	// We want to make sure that the main loop is running before we return to
	// the caller, otherwise the loop may miss the first update.
	<-syncCh

	return nil
}

func (f *fileObserverT) Stop() {
	notify.Stop(f.eventCh)
	close(f.doneCh)
	f.wg.Wait()
}
