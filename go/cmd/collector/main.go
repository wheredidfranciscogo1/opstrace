package main

import (
	"context"
	"fmt"
	"log"
	"net/http"
	"os"
	"strconv"
	"time"

	"github.com/open-telemetry/opentelemetry-collector-contrib/extension/pprofextension"
	"github.com/open-telemetry/opentelemetry-collector-contrib/processor/attributesprocessor"
	"github.com/open-telemetry/opentelemetry-collector-contrib/receiver/jaegerreceiver"
	"github.com/prometheus/client_golang/prometheus"
	"github.com/prometheus/client_golang/prometheus/promhttp"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/constants"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/otel/exporter/gitlabobservabilityexporter"
	"go.opentelemetry.io/collector/component"
	"go.opentelemetry.io/collector/exporter"
	"go.opentelemetry.io/collector/exporter/loggingexporter"
	"go.opentelemetry.io/collector/extension"
	"go.opentelemetry.io/collector/extension/ballastextension"
	"go.opentelemetry.io/collector/extension/zpagesextension"
	"go.opentelemetry.io/collector/otelcol"
	"go.opentelemetry.io/collector/processor"
	"go.opentelemetry.io/collector/processor/batchprocessor"
	"go.opentelemetry.io/collector/processor/memorylimiterprocessor"
	"go.opentelemetry.io/collector/receiver"
	"go.opentelemetry.io/collector/receiver/otlpreceiver"
)

func main() {
	// setup custom metrics
	prometheus.MustRegister(gitlabobservabilityexporter.ConfiguredCollectors()...)
	// setup custom metrics server
	mux := http.NewServeMux()
	mux.Handle("/metrics", promhttp.Handler())

	metricsPort := getEnvInt("CUSTOM_METRICS_PORT", 2112)
	metricsAddr := fmt.Sprintf("0.0.0.0:%d", metricsPort)
	metricsServ := &http.Server{
		Addr:              metricsAddr,
		Handler:           mux,
		ReadHeaderTimeout: time.Second * 3,
	}
	go func() {
		log.Printf("metrics server starting at %s", metricsAddr)
		if err := metricsServ.ListenAndServe(); err != nil {
			if err != http.ErrServerClosed {
				log.Fatalf("unable to start metric server: %v", err)
			}
		}
	}()

	cmd := otelcol.NewCommand(
		otelcol.CollectorSettings{
			BuildInfo: component.BuildInfo{
				Command:     "collector",
				Description: "OpenTelemetry Collector for GOB",
				Version:     constants.DockerImageTag,
			},
			Factories: getComponents,
		},
	)
	if err := cmd.Execute(); err != nil {
		log.Fatalf("collector exited: %v", err)
	}

	ctx, cancel := context.WithTimeout(context.Background(), time.Second*5)
	defer cancel()
	if err := metricsServ.Shutdown(ctx); err != nil {
		log.Printf("failed to shutdown the metric server: %v", err)
	}
}

func getComponents() (otelcol.Factories, error) {
	var err error
	factories := otelcol.Factories{}

	factories.Extensions, err = extension.MakeFactoryMap(
		ballastextension.NewFactory(),
		pprofextension.NewFactory(),
		zpagesextension.NewFactory(),
	)
	if err != nil {
		return otelcol.Factories{}, fmt.Errorf("extension factory: %w", err)
	}

	factories.Receivers, err = receiver.MakeFactoryMap(
		otlpreceiver.NewFactory(),
		jaegerreceiver.NewFactory(),
	)
	if err != nil {
		return otelcol.Factories{}, fmt.Errorf("receiver factory: %w", err)
	}

	factories.Processors, err = processor.MakeFactoryMap(
		attributesprocessor.NewFactory(),
		memorylimiterprocessor.NewFactory(),
		batchprocessor.NewFactory(),
	)
	if err != nil {
		return otelcol.Factories{}, fmt.Errorf("processor factory: %w", err)
	}

	factories.Exporters, err = exporter.MakeFactoryMap(
		gitlabobservabilityexporter.NewFactory(),
		loggingexporter.NewFactory(),
	)
	if err != nil {
		return otelcol.Factories{}, fmt.Errorf("exporter factory: %w", err)
	}

	return factories, nil
}

func getEnvInt(key string, fallback int) int {
	s := os.Getenv(key)
	if len(s) == 0 {
		return fallback
	}

	v, err := strconv.Atoi(s)
	if err != nil {
		return fallback
	}

	return v
}
