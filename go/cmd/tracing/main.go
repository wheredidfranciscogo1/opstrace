package main

import (
	"fmt"
	"log"

	_ "go.uber.org/automaxprocs"

	"github.com/open-telemetry/opentelemetry-collector-contrib/extension/pprofextension"
	"github.com/open-telemetry/opentelemetry-collector-contrib/receiver/jaegerreceiver"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/constants"
	"go.opentelemetry.io/collector/component"
	"go.opentelemetry.io/collector/exporter"
	"go.opentelemetry.io/collector/exporter/loggingexporter"
	"go.opentelemetry.io/collector/exporter/otlpexporter"
	"go.opentelemetry.io/collector/extension"
	"go.opentelemetry.io/collector/extension/ballastextension"
	"go.opentelemetry.io/collector/extension/zpagesextension"
	"go.opentelemetry.io/collector/otelcol"
	"go.opentelemetry.io/collector/processor"
	"go.opentelemetry.io/collector/processor/batchprocessor"
	"go.opentelemetry.io/collector/processor/memorylimiterprocessor"
	"go.opentelemetry.io/collector/receiver"
	"go.opentelemetry.io/collector/receiver/otlpreceiver"
)

func components() (otelcol.Factories, error) {
	var err error
	factories := otelcol.Factories{}

	factories.Extensions, err = extension.MakeFactoryMap(
		ballastextension.NewFactory(), // for performance optimization via memory pool
		pprofextension.NewFactory(),   // for in-process debugging if needed
		zpagesextension.NewFactory(),  // for in-process debugging if needed
	)
	if err != nil {
		return otelcol.Factories{}, fmt.Errorf("make extension factory %w", err)
	}

	factories.Receivers, err = receiver.MakeFactoryMap(
		otlpreceiver.NewFactory(),
		jaegerreceiver.NewFactory(),
	)
	if err != nil {
		return otelcol.Factories{}, fmt.Errorf("make receiver factory %w", err)
	}

	// see also recommendations here:
	// https://github.com/open-telemetry/opentelemetry-collector/tree/main/processor#recommended-processors
	factories.Processors, err = processor.MakeFactoryMap(
		memorylimiterprocessor.NewFactory(),
		batchprocessor.NewFactory(),
	)
	if err != nil {
		return otelcol.Factories{}, fmt.Errorf("make processor factory %w", err)
	}

	factories.Exporters, err = exporter.MakeFactoryMap(
		otlpexporter.NewFactory(),
		loggingexporter.NewFactory(), // optional, for debugging
	)
	if err != nil {
		return otelcol.Factories{}, fmt.Errorf("make exporter factory %w", err)
	}

	return factories, nil
}

func main() {
	info := component.BuildInfo{
		Command:     "otelcol-opstrace",
		Description: "OpenTelemetry Collector for GOB",
		Version:     constants.DockerImageTag,
	}

	if err := runInteractive(otelcol.CollectorSettings{BuildInfo: info, Factories: components}); err != nil {
		log.Fatal(err)
	}
}

func runInteractive(params otelcol.CollectorSettings) error {
	cmd := otelcol.NewCommand(params)
	if err := cmd.Execute(); err != nil {
		return fmt.Errorf("collector server exit: %w", err)
	}

	return nil
}
