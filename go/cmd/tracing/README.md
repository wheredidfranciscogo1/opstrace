# Tracing ingest service

This is a build of [otel-collector](https://github.com/open-telemetry/opentelemetry-collector/) and [otel-collector-contrib](https://github.com/open-telemetry/opentelemetry-collector-contrib/) modules.

The modules receive and process tracing data:

1. Inputs otel-format data over gRPC or HTTP (configured to use HTTP to allow subdir URIs for each tenant)
2. Converts data from otel to jaeger format
3. Submits data to the tenant Jaeger instance in jaeger format

## Notes

This service could be expanded to also support several log and/or metrics formats. This would be done by adding other otel-collector[-contrib] modules.

This service is currently single-tenant. Separate instances of this service are required for each tenant.

This service does not perform its own authentication, it assumes that the ingress controller has already handled authentication and routing.
