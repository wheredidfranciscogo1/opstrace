package main

import (
	"net/http"
	"os"
	"os/signal"
	"sync/atomic"
	"syscall"
	"time"

	"github.com/gin-gonic/gin"
	"github.com/prometheus/client_golang/prometheus"
	"github.com/prometheus/client_golang/prometheus/promhttp"
	"go.uber.org/zap"
	"go.uber.org/zap/zapcore"

	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/common"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/telemetry"
	query "gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/trace-query-api"
)

var (
	metricsAddr   = common.GetEnv("METRICS_ADDR", ":8081")
	addr          = common.GetEnv("ADDR", ":8080")
	clickHouseDsn = common.GetEnv("CLICKHOUSE_DSN", "tcp://localhost:9000/tracing")
	logLevel      = common.GetEnv("LOG_LEVEL", "debug")
	cloudDSN      = common.GetEnv("CLICKHOUSE_CLOUD_DSN", "")
)

func main() {
	logger, syncF, config := common.SetupLogger(logLevel)
	defer syncF()
	if config.Level.Level() >= zapcore.InfoLevel {
		gin.SetMode(gin.ReleaseMode)
	}

	q, err := query.NewQuerier(clickHouseDsn, cloudDSN, nil, logger.Desugar())
	if err != nil {
		logger.Fatal("initializing querier: %w", zap.Error(err))
	}
	controller := &query.Controller{
		Q: q, Logger: logger.Desugar(),
	}

	routerMain := gin.Default()
	query.SetRoutes(controller, routerMain)

	prometheus.MustRegister(telemetry.ConfiguredCollectors()...)
	mux := http.NewServeMux()
	mux.Handle("/metrics", promhttp.Handler())

	isReady := &atomic.Bool{}
	mux.Handle("/readyz", common.ReadyHandler(isReady))

	g := common.NewGracefulServer(
		logger.Desugar(),
		[]*http.Server{
			// metrics server
			{
				Addr:              metricsAddr,
				Handler:           mux,
				ReadHeaderTimeout: time.Second * 3,
			},
			// app server
			{
				Addr:              addr,
				Handler:           routerMain,
				ReadHeaderTimeout: time.Second * 3,
			},
		},
		10, // graceful sleep before shutdown, in seconds
	)
	logger.Info("starting managed servers")
	g.Start()
	isReady.Store(true)
	logger.Info("started managed servers")

	sigChan := make(chan os.Signal, 1)
	signal.Notify(sigChan, syscall.SIGTERM)
	<-sigChan

	logger.Info("stopping managed servers")
	isReady.Store(false)
	g.Stop()
	logger.Info("stopped managed servers")

	if err := controller.Q.Close(); err != nil {
		logger.Error("shutting down database connection(s)", zap.Error(err))
	}
}
