package main

import (
	"context"
	"flag"
	"net/http"
	"os"
	"os/signal"
	"syscall"
	"time"

	ginzap "github.com/gin-contrib/zap"
	"github.com/gin-gonic/gin"
	"github.com/prometheus/client_golang/prometheus"
	"github.com/prometheus/client_golang/prometheus/collectors"
	"github.com/prometheus/client_golang/prometheus/promhttp"
	_ "go.uber.org/automaxprocs"
	"go.uber.org/zap"

	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/common"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/query-api/core"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/query-api/logs"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/query-api/metrics"
)

var (
	metricsAddr string
	serviceAddr string
	logLevel    string

	clickHouseDSN string

	clickHouseCloudDSN string
)

func parseCmdline() {
	flag.StringVar(&metricsAddr, "metrics-bind-address", ":8081", "The address the metric endpoint binds to.")
	flag.StringVar(&serviceAddr, "service-bind-address", ":8080", "The address the service endpoint binds to.")
	flag.StringVar(&logLevel, "log-level", "info", "The log-level to use.")
	flag.StringVar(&clickHouseDSN, "clickhouse-dsn", "tcp://localhost:9000", "clickhouse addr")
	flag.StringVar(&clickHouseCloudDSN, "clickhouse-cloud-dsn", "", "clickhouse cloud addr")

	flag.Parse()
}

func main() {
	os.Exit(runQueryAPI())
}

func runQueryAPI() int {
	parseCmdline()

	logger, _, _ := common.SetupLogger(logLevel)

	ctx, cancelFunc := signal.NotifyContext(context.Background(), os.Interrupt, syscall.SIGTERM)
	defer cancelFunc()

	metricsQuerier, err := metrics.NewQuerier(clickHouseDSN, clickHouseCloudDSN, nil, logger.Desugar())
	if err != nil {
		logger.Errorw("initializing metrics querier", zap.Error(err))
		return 1
	}

	logsQuerier, err := logs.NewQuerier(clickHouseDSN, clickHouseCloudDSN, nil, logger.Desugar())
	if err != nil {
		logger.Errorw("initializing logs querier", zap.Error(err))
		return 1
	}

	queryAPI := core.NewQueryAPI(logger.Desugar(), metricsQuerier, logsQuerier)

	metricsRegistry := prometheus.NewRegistry()
	metricsRegistry.MustRegister(collectors.NewProcessCollector(collectors.ProcessCollectorOpts{}))
	metricsRegistry.MustRegister(collectors.NewGoCollector())
	metricsHandler := promhttp.HandlerFor(metricsRegistry, promhttp.HandlerOpts{
		ErrorHandling:     promhttp.HTTPErrorOnError,
		EnableOpenMetrics: true,
	})
	routerMetrics := http.NewServeMux()
	routerMetrics.Handle("/metrics", metricsHandler)

	if logLevel == "debug" {
		gin.SetMode(gin.DebugMode)
	} else {
		gin.SetMode(gin.ReleaseMode)
	}
	routerMain := gin.New()
	routerMain.Use(
		ginzap.GinzapWithConfig(
			logger.Desugar(),
			&ginzap.Config{
				TimeFormat: time.RFC3339,
				UTC:        true,
				SkipPaths:  []string{"/readyz"},
			},
		),
	)
	routerMain.Use(ginzap.RecoveryWithZap(logger.Desugar(), true))
	routerMain.Use(common.RouteMetrics(metricsRegistry))

	queryAPI.SetRoutes(routerMain)

	logger.Infof("log level: %s", logLevel)
	logger.Infow("starting main HTTP server", "address", serviceAddr)
	logger.Infow("starting metrics HTTP server", "address", metricsAddr)

	metricServer := &http.Server{
		Addr:              metricsAddr,
		Handler:           routerMetrics,
		ReadHeaderTimeout: time.Second * 3,
	}

	mainServer := &http.Server{
		Addr:              serviceAddr,
		Handler:           routerMain,
		ReadHeaderTimeout: time.Second * 3,
	}

	err = common.ServeWithGracefulShutdown(ctx, logger, mainServer, metricServer)
	if err != nil {
		logger.Errorf("shutting down http servers: %w", err)
		return 1
	}

	logger.Info("server shutdown is complete")
	return 0
}
