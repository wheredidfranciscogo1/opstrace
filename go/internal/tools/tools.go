//go:build tool

// tools that are used as part of the build process
package tools

import (
	_ "github.com/swaggo/swag/cmd/swag"
	_ "github.com/vburenin/ifacemaker"
	_ "github.com/vektra/mockery/v2"
	_ "sigs.k8s.io/controller-runtime/tools/setup-envtest"
	_ "sigs.k8s.io/controller-tools/cmd/controller-gen"
)
