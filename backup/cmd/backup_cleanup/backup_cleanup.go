package main

import (
	"context"
	"encoding/json"
	"errors"
	"fmt"
	"os"
	"os/signal"
	"strconv"
	"time"

	"cloud.google.com/go/storage"
	"go.uber.org/zap"
	"google.golang.org/api/iterator"

	backup "gitlab.com/gitlab-org/opstrace/opstrace/clickhouse-backup"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/common"
)

var (
	logLevel    = common.GetEnv("LOG_LEVEL", "debug")
	bucketName  = common.GetEnv("BUCKET_NAME", "")
	folderNames = common.GetEnv("FOLDER_NAMES", "testbackups/")
	dryRunStr   = common.GetEnv("DRY_RUN", "false")
	dryRun      bool
)

func main() {
	os.Exit(runCleanupExecutor())
}

func runCleanupExecutor() int {
	logger, _, _ := common.SetupLogger(logLevel)
	// Sync has underlying bug where it errors out unnecessarily. See https://github.com/uber-go/zap/issues/880
	//nolint:errcheck
	defer logger.Sync()

	ctx := context.Background()
	client, err := storage.NewClient(ctx)
	if err != nil {
		logger.Error("create storage client: ", err)
		return 1
	}
	defer client.Close()

	cleanupExecutor := executor{
		storageClient: client,
		logger:        logger,
	}

	if err := cleanupExecutor.validateParameters(); err != nil {
		logger.Error("validateParameters: ", err)
		return 1
	}

	ctx, stop := signal.NotifyContext(context.Background(), os.Interrupt)
	defer stop()

	logger.Debug("bucket name: ", bucketName)

	penultimateDate := backup.BeginningOfMonth().Format(time.DateOnly)
	logger.Info("cleaning up backups from: ", penultimateDate)
	foundObjs, attrs := cleanupExecutor.checkOldBackups(ctx, bucketName, folderNames, penultimateDate)
	if !foundObjs {
		logger.Info("no old backups found, exiting")
		return 0
	}

	logger.Info("found old backups, collecting objects to delete")
	err = cleanupExecutor.collectOldBackups(ctx, bucketName, attrs)
	if err != nil {
		logger.Error("collecting backups: ", err)
		return 1
	}

	//nolint:errcheck
	bts, _ := json.Marshal(cleanupExecutor.toDeletePrefix)
	logger.Desugar().Info("backups to be deleted under: ", zap.Any("items", json.RawMessage(bts)))
	cleanupExecutor.deleteObjects(ctx, bucketName, dryRun)

	return 0
}

type executor struct {
	storageClient   *storage.Client
	logger          *zap.SugaredLogger
	toDeleteObjects []*storage.ObjectAttrs

	// kept for logging purposes
	toDeletePrefix []string
}

func (e *executor) validateParameters() (err error) {
	if bucketName == "" {
		return fmt.Errorf("missing arg: BUCKET_NAME")
	}
	if folderNames == "" {
		return fmt.Errorf("missing arg: FOLDER_NAMES")
	}

	if dryRunStr != "" {
		dryRun, err = strconv.ParseBool(dryRunStr)
		if err != nil {
			return fmt.Errorf("arg: DRY_RUN: %w", err)
		}
	}
	return nil
}

// get the list of old backups from over last 2 weeks
func (e *executor) checkOldBackups(
	parentCtx context.Context,
	bucketName, folderNames string, date string,
) (backupFound bool, attrs []*storage.ObjectAttrs) {
	bucket := e.storageClient.Bucket(bucketName)

	ctx, cancel := context.WithTimeout(parentCtx, time.Second*30)
	defer cancel()

	results := make([]*storage.ObjectAttrs, 0)

	// This query will get the objects in a directory-like fashion.
	// Assumes the backup structure to be of structure "folder/date/".
	// We get the objects that match the prefix of "folder", and are lexicographically less than
	// the "folder/date".
	// Nice thing that date format used with `time.DateOnly` keeps the date sorted lexicographically.
	query := &storage.Query{
		Delimiter: "/",
		Prefix:    folderNames,
		EndOffset: folderNames + date,
	}
	e.logger.Info("checking backups for: ", query.Prefix)

	it := bucket.Objects(ctx, query)
	it.PageInfo()
	for {
		attrs, err := it.Next()
		if errors.Is(err, iterator.Done) {
			break
		}
		if err != nil {
			e.logger.Fatal(err)
		}
		e.logger.Debug("found: ", attrs.Name, attrs.Prefix)
		backupFound = true
		results = append(results, attrs)
	}

	return backupFound, results
}

// collectOldBackups gets all the objects that needs to be deleted. Relies on the assumed hierarchy used by the backup
// script. This is not intended to be a generic deletion function.
func (e *executor) collectOldBackups(parentCtx context.Context, bucketName string, attrs []*storage.ObjectAttrs) error {
	bucket := e.storageClient.Bucket(bucketName)
	ctx, cancel := context.WithTimeout(parentCtx, time.Second*30)
	defer cancel()

	for _, attr := range attrs {
		// Prefix will be set for objects that are indicated as "folder" in gcs terms.
		if attr.Prefix != "" {
			// This will get all the objects inside the folder (including any synthetic folders).
			query := &storage.Query{
				Prefix: attr.Prefix,
			}

			it := bucket.Objects(ctx, query)
			it.PageInfo()
			for {
				at, err := it.Next()
				if errors.Is(err, iterator.Done) {
					break
				}
				if err != nil {
					return fmt.Errorf("iterator: %w", err)
				}
				e.logger.Debug("found object: ", at.Name)
				e.toDeleteObjects = append(e.toDeleteObjects, at)
			}

			e.toDeletePrefix = append(e.toDeletePrefix, attr.Prefix)
		} else {
			// This should be a top-level synthetic folder that should be kept as is.
			handle := e.storageClient.Bucket(bucketName).Object(attr.Name)
			a, err := handle.Attrs(parentCtx)
			if err != nil {
				return fmt.Errorf("attrs: %w", err)
			}
			e.logger.Info("top level object: ", a.Name, a.Size, a.Created, a.MediaLink)
		}
	}
	return nil
}

func (e *executor) deleteObjects(parentCtx context.Context, bucketName string, dryRun bool) {
	bucket := e.storageClient.Bucket(bucketName)
	totalDeleted := 0

	for _, attr := range e.toDeleteObjects {
		if dryRun {
			e.logger.Info("will delete: ", attr.Name)
		} else {
			ctx, cancel := context.WithTimeout(parentCtx, time.Second*5)
			err := bucket.Object(attr.Name).Delete(ctx)
			if err != nil {
				e.logger.Error("deletion: ", err)
			}
			totalDeleted += 1
			cancel()
		}
	}
	e.logger.Infof("total deleted objects: %d", totalDeleted)
}
