ARG BASE_IMG

FROM ${BASE_IMG}

# Set up the Docker:
ARG DIND_COMMIT="3b5fac462d21ca164b3778647420016315289034"

# Install Docker
RUN set -ex \
    # set up subuid/subgid so that "--userns-remap=default" works out-of-the-box
    && addgroup dockremap \
    && useradd -g dockremap dockremap \
    && echo 'dockremap:165536:65536' >> /etc/subuid \
    && echo 'dockremap:165536:65536' >> /etc/subgid \
    && wget -q "https://raw.githubusercontent.com/docker/docker/${DIND_COMMIT}/hack/dind" -O /usr/local/bin/dind \
    && chmod +x /usr/local/bin/dind

# Required by DinD on debian:
RUN set -ex \
    && update-alternatives --set iptables /usr/sbin/iptables-legacy \
    && update-alternatives --set ip6tables /usr/sbin/ip6tables-legacy

VOLUME /var/lib/docker

COPY containers/ci/dockerd-entrypoint.sh /usr/local/bin/dockerd-entrypoint.sh

ENTRYPOINT ["/usr/local/bin/dockerd-entrypoint.sh"]
