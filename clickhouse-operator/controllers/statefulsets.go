package controllers

import (
	"fmt"
	"strconv"

	clickhousev1alpha1 "gitlab.com/gitlab-org/opstrace/opstrace/clickhouse-operator/api/v1alpha1"
	appsv1 "k8s.io/api/apps/v1"
	corev1 "k8s.io/api/core/v1"
	"k8s.io/apimachinery/pkg/api/resource"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/util/intstr"
	"k8s.io/utils/ptr"
	ctrl "sigs.k8s.io/controller-runtime"
)

const (
	clickhouseServerContainerName = "clickhouse-server"
)

func newClickHouseStatefulSets(
	req ctrl.Request,
	ch *clickHouseCluster,
) []*KubernetesResource {
	kr := make([]*KubernetesResource, 0)

	labels := ch.labels()
	for i, h := range ch.Hosts {
		stsLabels := ch.labelsMerged(map[string]string{
			"replica": strconv.Itoa(i),
			"shard":   strconv.Itoa(0),
		})
		// 'replica=#' used by granular per-pod Services
		kr = append(kr, newClickHouseStatefulSet(
			req.Namespace,
			h,
			i,
			ch,
			labels,
			stsLabels,
		))
	}

	return kr
}

//nolint:funlen
func newClickHouseStatefulSet(
	namespace string,
	host string,
	replica int,
	ch *clickHouseCluster,
	clusterLabels map[string]string,
	stsLabels map[string]string,
) *KubernetesResource {
	sts := &appsv1.StatefulSet{
		ObjectMeta: metav1.ObjectMeta{
			Name:      host,
			Namespace: namespace,
		},
	}

	return &KubernetesResource{
		obj: sts,
		mutator: func() error {
			sts.ObjectMeta.Labels = stsLabels

			// Each StatefulSet has a matching per-node Service with the same name
			sts.Spec.ServiceName = host
			// Each node is being deployed in its own StatefulSet
			var replicas int32 = 1
			sts.Spec.Replicas = &replicas
			sts.Spec.PodManagementPolicy = appsv1.OrderedReadyPodManagement
			sts.Spec.Selector = &metav1.LabelSelector{
				MatchLabels: stsLabels,
			}
			sts.Spec.Template.Labels = stsLabels
			sts.Spec.Template.Spec.NodeSelector = ch.Spec.NodeSelector
			sts.Spec.Template.Spec.Tolerations = ch.Spec.Tolerations

			affinity := &corev1.Affinity{
				PodAntiAffinity: &corev1.PodAntiAffinity{
					PreferredDuringSchedulingIgnoredDuringExecution: []corev1.WeightedPodAffinityTerm{
						{
							PodAffinityTerm: corev1.PodAffinityTerm{
								LabelSelector: &metav1.LabelSelector{
									MatchLabels: clusterLabels,
								},
								TopologyKey: "kubernetes.io/hostname",
							},
							Weight: 1,
						},
					},
				},
			}
			if ch.Spec.Affinity != nil {
				a := ch.Spec.Affinity.DeepCopy()
				if a.PodAntiAffinity == nil {
					a.PodAntiAffinity = affinity.PodAntiAffinity
				} else {
					a.PodAntiAffinity.PreferredDuringSchedulingIgnoredDuringExecution =
						append(
							a.PodAntiAffinity.PreferredDuringSchedulingIgnoredDuringExecution,
							affinity.PodAntiAffinity.PreferredDuringSchedulingIgnoredDuringExecution...)
				}

				affinity = a
			}
			sts.Spec.Template.Spec.Affinity = affinity
			sts.Spec.Template.Spec.Containers = clickHouseContainers(ch, host, replica)
			var uidGID int64 = 101
			var groupChangePolicy = corev1.FSGroupChangeOnRootMismatch
			sts.Spec.Template.Spec.SecurityContext = &corev1.PodSecurityContext{
				RunAsUser:  &uidGID,
				RunAsGroup: &uidGID,
				FSGroup:    &uidGID,
				// setting fsGroupChangePolicy to "OnRootMismatch" so if the root of the volume
				// already has the correct permissions, the recursive permission change can be skipped.
				// Kubernetes ensures that permissions of the top-level directory are changed last
				// the first time it applies permissions.
				FSGroupChangePolicy: &groupChangePolicy,
			}
			sts.Spec.Template.Spec.TerminationGracePeriodSeconds = ptr.To(int64(30))
			sts.Spec.Template.Spec.Volumes = []corev1.Volume{
				{
					Name: "configd",
					VolumeSource: corev1.VolumeSource{
						ConfigMap: &corev1.ConfigMapVolumeSource{
							LocalObjectReference: corev1.LocalObjectReference{Name: ch.Name},
							Items: []corev1.KeyToPath{
								{
									Key:  "config.xml",
									Path: "config.xml",
								},
							},
						},
					},
				},
				{
					Name: "usersd",
					VolumeSource: corev1.VolumeSource{
						ConfigMap: &corev1.ConfigMapVolumeSource{
							LocalObjectReference: corev1.LocalObjectReference{Name: ch.Name},
							Items: []corev1.KeyToPath{
								{
									Key:  "users.xml",
									Path: "users.xml",
								},
							},
						},
					},
				},
				{
					Name: CHDataVolumeName,
					VolumeSource: corev1.VolumeSource{
						PersistentVolumeClaim: &corev1.PersistentVolumeClaimVolumeSource{
							ClaimName: CHDataVolumeName,
						},
					},
				},
				{
					Name: "logs",
					VolumeSource: corev1.VolumeSource{
						PersistentVolumeClaim: &corev1.PersistentVolumeClaimVolumeSource{
							ClaimName: "logs",
						},
					},
				},
				{
					Name: "keeper-logs",
					VolumeSource: corev1.VolumeSource{
						PersistentVolumeClaim: &corev1.PersistentVolumeClaimVolumeSource{
							ClaimName: "keeper-logs",
						},
					},
				},
				{
					Name: "cliendconfigd",
					VolumeSource: corev1.VolumeSource{
						ConfigMap: &corev1.ConfigMapVolumeSource{
							LocalObjectReference: corev1.LocalObjectReference{Name: ch.Name},
							Items: []corev1.KeyToPath{
								{
									Key:  "client-config.xml",
									Path: "config.xml",
								},
							},
						},
					},
				},
				{
					Name: "keeperconfig",
					VolumeSource: corev1.VolumeSource{
						ConfigMap: &corev1.ConfigMapVolumeSource{
							LocalObjectReference: corev1.LocalObjectReference{Name: ch.Name},
							Items: []corev1.KeyToPath{
								{
									Key:  "keeper.xml",
									Path: "keeper_config.xml",
								},
							},
						},
					},
				},
			}
			if ch.InternalSSL {
				sts.Spec.Template.Spec.Volumes = append(sts.Spec.Template.Spec.Volumes, corev1.Volume{
					Name: "ssl",
					VolumeSource: corev1.VolumeSource{
						Projected: &corev1.ProjectedVolumeSource{
							Sources: []corev1.VolumeProjection{
								{
									Secret: &corev1.SecretProjection{
										LocalObjectReference: corev1.LocalObjectReference{
											Name: ch.prependClusterName(clickhouseCertSecretName),
										},
										Items: []corev1.KeyToPath{
											{Key: "ca.crt", Path: "ca.crt"},
											{Key: "tls.crt", Path: "tls.crt"},
											{Key: "tls.key", Path: "tls.key"},
										},
									},
								},
								{
									Secret: &corev1.SecretProjection{
										LocalObjectReference: corev1.LocalObjectReference{
											Name: ch.prependClusterName("dhparam")},
										Items: []corev1.KeyToPath{
											{Key: dhParamsKey, Path: "dhparam.pem"},
										},
									},
								},
							},
						},
					},
				})
			}

			sts.Spec.VolumeClaimTemplates = []corev1.PersistentVolumeClaim{
				{
					ObjectMeta: metav1.ObjectMeta{Name: CHDataVolumeName},
					Spec: corev1.PersistentVolumeClaimSpec{
						StorageClassName: ch.Spec.StorageClass,
						AccessModes:      []corev1.PersistentVolumeAccessMode{corev1.ReadWriteOnce},
						Resources: corev1.ResourceRequirements{
							Requests: corev1.ResourceList{
								corev1.ResourceStorage: ch.Spec.StorageSize,
							},
						},
					},
				},
				// Separate log volumes to avoid interruption to logs if data volume is full
				{
					ObjectMeta: metav1.ObjectMeta{Name: "logs"},
					Spec: corev1.PersistentVolumeClaimSpec{
						StorageClassName: ch.Spec.StorageClass,
						AccessModes:      []corev1.PersistentVolumeAccessMode{corev1.ReadWriteOnce},
						Resources: corev1.ResourceRequirements{
							Requests: corev1.ResourceList{
								corev1.ResourceStorage: resource.MustParse("3Gi"),
							},
						},
					},
				},
				{
					ObjectMeta: metav1.ObjectMeta{Name: "keeper-logs"},
					Spec: corev1.PersistentVolumeClaimSpec{
						StorageClassName: ch.Spec.StorageClass,
						AccessModes:      []corev1.PersistentVolumeAccessMode{corev1.ReadWriteOnce},
						Resources: corev1.ResourceRequirements{
							Requests: corev1.ResourceList{
								corev1.ResourceStorage: resource.MustParse("3Gi"),
							},
						},
					},
				},
			}

			return nil
		},
	}
}

// creates containers for the ClickHouse server and Keeper with keeper exporter for metrics.
func clickHouseContainers(ch *clickHouseCluster, host string, replica int) []corev1.Container {
	serverPorts := []corev1.ContainerPort{
		{
			Name:          "http",
			ContainerPort: 8123,
		},
		{
			Name:          "client",
			ContainerPort: 9000,
		},
		{
			Name:          "interserver",
			ContainerPort: 9009,
		},
		{
			Name:          "metrics",
			ContainerPort: 8001,
		},
	}

	dataMount := corev1.VolumeMount{
		Name:      CHDataVolumeName,
		MountPath: "/var/lib/clickhouse",
	}

	sslMount := corev1.VolumeMount{
		Name:      "ssl",
		MountPath: "/etc/clickhouse-server/ssl",
	}
	if ch.InternalSSL {
		serverPorts = append(serverPorts, []corev1.ContainerPort{
			{
				Name:          "https",
				ContainerPort: 8443,
			},
			{
				Name:          "client-secure",
				ContainerPort: 9440,
			},
			{
				Name:          "int-serv-secure",
				ContainerPort: 9010,
			},
		}...)
	}

	securityContext := &corev1.SecurityContext{
		Capabilities: &corev1.Capabilities{
			Add:  []corev1.Capability{"IPC_LOCK", "SYS_NICE"},
			Drop: []corev1.Capability{"ALL"},
		},
	}

	var resources corev1.ResourceRequirements
	if ch.Spec.ServerResourceRequirements != nil {
		resources = *ch.Spec.ServerResourceRequirements
	} else {
		resources = clickHouseServerResourceRequirements()
	}

	return []corev1.Container{
		{
			Name:            clickhouseServerContainerName,
			Image:           ch.Spec.Image,
			ImagePullPolicy: corev1.PullIfNotPresent,
			Resources:       resources,
			Env:             clickHouseServerEnv(ch, host),
			Ports:           serverPorts,
			// NOTE(joe): just use readiness probe, we don't want to restart pods in this cluster.
			// Readiness in combination with headless PublishNotReadyAddresses service allows
			// cluster to form while correctly reporting pod status.
			ReadinessProbe: &corev1.Probe{
				ProbeHandler: corev1.ProbeHandler{
					HTTPGet: &corev1.HTTPGetAction{
						Path:   "/ping",
						Port:   intstr.FromString("http"),
						Scheme: corev1.URISchemeHTTP,
					},
				},
				InitialDelaySeconds: 5,
				TimeoutSeconds:      1,
				PeriodSeconds:       10,
				SuccessThreshold:    1,
				FailureThreshold:    3,
			},
			SecurityContext: securityContext,
			VolumeMounts:    clickHouseServerMounts(ch, dataMount, sslMount),
		},
		keeperContainer(ch, replica, dataMount, sslMount, securityContext),
		keeperExporterContainer(ch),
	}
}

func clickHouseServerResourceRequirements() corev1.ResourceRequirements {
	return corev1.ResourceRequirements{
		Requests: corev1.ResourceList{
			corev1.ResourceCPU:    resource.MustParse("1"),
			corev1.ResourceMemory: resource.MustParse("1Gi"),
		},
		Limits: corev1.ResourceList{
			corev1.ResourceCPU:    resource.MustParse("2"),
			corev1.ResourceMemory: resource.MustParse("2Gi"),
		},
	}
}

func clickHouseServerMounts(ch *clickHouseCluster,
	dataMount corev1.VolumeMount, sslMount corev1.VolumeMount) []corev1.VolumeMount {
	serverVolumeMounts := []corev1.VolumeMount{
		dataMount,
		{
			Name:      "configd",
			MountPath: "/etc/clickhouse-server/config.d",
		},
		{
			Name:      "usersd",
			MountPath: "/etc/clickhouse-server/users.d",
		},
		{
			Name:      "logs",
			MountPath: "/var/log/clickhouse-server",
		},
		{
			Name:      "cliendconfigd",
			MountPath: "/etc/clickhouse-client/config.d",
		},
	}

	if ch.InternalSSL {
		serverVolumeMounts = append(serverVolumeMounts, sslMount)
	}
	return serverVolumeMounts
}

func clickHouseServerEnv(ch *clickHouseCluster, host string) []corev1.EnvVar {
	env := []corev1.EnvVar{
		// Add host service name so this can be referenced in config
		{
			Name:  "HOST",
			Value: host,
		},
	}
	// For each admin user, create a password envvar to be referenced by users.xml
	for _, user := range ch.Spec.AdminUsers {
		// TODO(joe): passwords should not be in env vars.
		// We should load the secret and sha256_hex the password.
		// See https://clickhouse.com/docs/en/operations/settings/settings-users/#user-namepassword.
		env = append(env, corev1.EnvVar{
			Name: fmt.Sprintf("ADMIN_PASSWORD_%s", user.Name),
			ValueFrom: &corev1.EnvVarSource{
				SecretKeyRef: &corev1.SecretKeySelector{
					LocalObjectReference: corev1.LocalObjectReference{Name: user.SecretKeyRef.Name},
					Key:                  user.SecretKeyRef.Key,
				},
			},
		})
	}

	if ch.ObjectStorageEnabled && ch.Spec.ObjectStorage.Backend == clickhousev1alpha1.BackendGCS {
		env = append(env, []corev1.EnvVar{
			{
				Name: "ACCESS_KEY_ID",
				ValueFrom: &corev1.EnvVarSource{
					SecretKeyRef: &corev1.SecretKeySelector{
						LocalObjectReference: corev1.LocalObjectReference{Name: ch.Spec.ObjectStorage.AccessKeyIDSecret.Name},
						Key:                  ch.Spec.ObjectStorage.AccessKeyIDSecret.Key,
					},
				},
			},
			{
				Name: "SECRET_ACCESS_KEY",
				ValueFrom: &corev1.EnvVarSource{
					SecretKeyRef: &corev1.SecretKeySelector{
						LocalObjectReference: corev1.LocalObjectReference{Name: ch.Spec.ObjectStorage.AccessKeySecret.Name},
						Key:                  ch.Spec.ObjectStorage.AccessKeySecret.Key,
					},
				},
			},
		}...)

		if ch.Spec.ObjectStorage.Region != nil {
			env = append(env, corev1.EnvVar{
				Name:  "STORAGE_REGION",
				Value: *ch.Spec.ObjectStorage.Region,
			})
		}
	}

	if ch.ObjectStorageEnabled && ch.Spec.ObjectStorage.Backend == clickhousev1alpha1.BackendS3 {
		env = append(env, []corev1.EnvVar{
			{
				Name: "ACCESS_KEY_ID",
				ValueFrom: &corev1.EnvVarSource{
					SecretKeyRef: &corev1.SecretKeySelector{
						LocalObjectReference: corev1.LocalObjectReference{Name: ch.Spec.ObjectStorage.AccessKeyIDSecret.Name},
						Key:                  ch.Spec.ObjectStorage.AccessKeyIDSecret.Key,
					},
				},
			},
			{
				Name: "SECRET_ACCESS_KEY",
				ValueFrom: &corev1.EnvVarSource{
					SecretKeyRef: &corev1.SecretKeySelector{
						LocalObjectReference: corev1.LocalObjectReference{Name: ch.Spec.ObjectStorage.AccessKeySecret.Name},
						Key:                  ch.Spec.ObjectStorage.AccessKeySecret.Key,
					},
				},
			},
		}...)

		if ch.Spec.ObjectStorage.CustomerKey != nil {
			env = append(env, corev1.EnvVar{
				Name: "SSE_CUSTOMER_KEY",
				ValueFrom: &corev1.EnvVarSource{
					SecretKeyRef: &corev1.SecretKeySelector{
						LocalObjectReference: corev1.LocalObjectReference{Name: ch.Spec.ObjectStorage.CustomerKey.Name},
						Key:                  ch.Spec.ObjectStorage.CustomerKey.Key,
					},
				},
			})
		}
		if ch.Spec.ObjectStorage.Region != nil {
			env = append(env, corev1.EnvVar{
				Name:  "STORAGE_REGION",
				Value: *ch.Spec.ObjectStorage.Region,
			})
		}
	}
	return env
}

func keeperContainer(ch *clickHouseCluster, replica int,
	dataMount corev1.VolumeMount, sslMount corev1.VolumeMount, securityContext *corev1.SecurityContext) corev1.Container {
	keeperPorts := []corev1.ContainerPort{
		{
			Name:          "keeper",
			ContainerPort: 2181,
		},
		{
			Name:          "raft",
			ContainerPort: 9444,
		},
	}

	keeperVolumeMounts := []corev1.VolumeMount{
		dataMount,
		{
			Name:      "keeperconfig",
			MountPath: "/etc/clickhouse-keeper",
		},
		{
			Name:      "keeper-logs",
			MountPath: "/var/log/clickhouse-keeper",
		},
	}

	if ch.InternalSSL {
		keeperPorts = append(keeperPorts, corev1.ContainerPort{
			Name:          "keeper-secure",
			ContainerPort: 9281,
		})
		keeperVolumeMounts = append(keeperVolumeMounts, sslMount)
	}

	return corev1.Container{
		Name:            "clickhouse-keeper",
		Image:           ch.KeeperImage,
		ImagePullPolicy: corev1.PullIfNotPresent,
		Resources: corev1.ResourceRequirements{
			Requests: corev1.ResourceList{
				corev1.ResourceCPU:    resource.MustParse("0.2"),
				corev1.ResourceMemory: resource.MustParse("256M"),
			},
			Limits: corev1.ResourceList{
				corev1.ResourceCPU:    resource.MustParse("2"),
				corev1.ResourceMemory: resource.MustParse("4Gi"),
			},
		},
		Env: []corev1.EnvVar{
			// Add the pod's replica index, referenced in keeper.tmpl.xml
			{
				Name:  "REPLICA",
				Value: strconv.Itoa(replica),
			},
			{
				Name:  "KEEPER_CONFIG",
				Value: "/etc/clickhouse-keeper/keeper_config.xml",
			},
		},
		Ports: keeperPorts,
		// Readiness needs to wait for keeper leader election before allowing
		// incoming requests.
		ReadinessProbe: &corev1.Probe{
			ProbeHandler: corev1.ProbeHandler{
				TCPSocket: &corev1.TCPSocketAction{
					Port: intstr.FromString("raft"),
				},
			},
			// longer delay allows for keeper node to come online.
			InitialDelaySeconds: 5,
			TimeoutSeconds:      1,
			PeriodSeconds:       10,
			SuccessThreshold:    1,
			FailureThreshold:    3,
		},
		SecurityContext: securityContext,
		VolumeMounts:    keeperVolumeMounts,
	}
}

// Exporter for [zoo]keeper metrics also provides useful readiness checks.
// Checks are performed through ZooKeeper four letter word commands.
func keeperExporterContainer(ch *clickHouseCluster) corev1.Container {
	// keeper mode check used probe command grep
	expectedKeeperMode := "standalone"
	if ch.Replicas > 1 {
		expectedKeeperMode = "leader|follower"
	}

	return corev1.Container{
		Name: "zookeeper-exporter",
		//nolint:lll
		Image:           "dabealu/zookeeper-exporter:latest@sha256:c7cff5b9174e15b2e0ba1c22406d16c38f3c242e0c1ea182a828546790a1e117",
		ImagePullPolicy: corev1.PullIfNotPresent,
		Command: []string{
			"zookeeper-exporter",
			"-listen", "0.0.0.0:7000",
			"-zk-hosts", "127.0.0.1:2181",
		},
		Ports: []corev1.ContainerPort{
			{
				Name:          "keeper-metrics",
				ContainerPort: 7000,
			},
		},
		Resources: corev1.ResourceRequirements{
			Requests: corev1.ResourceList{
				corev1.ResourceCPU:    resource.MustParse("0.1"),
				corev1.ResourceMemory: resource.MustParse("64M"),
			},
			Limits: corev1.ResourceList{
				corev1.ResourceCPU:    resource.MustParse("0.5"),
				corev1.ResourceMemory: resource.MustParse("128M"),
			},
		},
		// Startup using ZooKeeper ruok command for server health.
		// see: https://kubernetes.io/docs/tutorials/stateful-application/zookeeper/#testing-for-liveness
		StartupProbe: &corev1.Probe{
			ProbeHandler: corev1.ProbeHandler{
				Exec: &corev1.ExecAction{
					Command: []string{
						"sh", "-c",
						"OK=$(echo ruok | nc 127.0.0.1 2181); if [[ \"$OK\" == \"imok\" ]]; then exit 0; else exit 1; fi",
					},
				},
			},
			InitialDelaySeconds: 1,
			TimeoutSeconds:      1,
			PeriodSeconds:       5,
			SuccessThreshold:    1,
			FailureThreshold:    3,
		},
		// Readiness ensures keeper cluster is ready for DDL queries.
		// Verify quorum exists through stats "Mode".
		ReadinessProbe: &corev1.Probe{
			ProbeHandler: corev1.ProbeHandler{
				Exec: &corev1.ExecAction{
					Command: []string{
						"sh", "-c",
						fmt.Sprintf("echo stats | nc 127.0.0.1 2181 | grep -E 'Mode: %s'", expectedKeeperMode),
					},
				},
			},
			InitialDelaySeconds: 5,
			TimeoutSeconds:      1,
			PeriodSeconds:       5,
			// double check that quorum
			SuccessThreshold: 2,
			FailureThreshold: 2,
		},
	}
}
