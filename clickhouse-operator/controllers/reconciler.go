package controllers

import (
	"context"
	"fmt"
	"strings"
	"time"

	"github.com/go-logr/logr"
	appsv1 "k8s.io/api/apps/v1"
	corev1 "k8s.io/api/core/v1"
	apierrors "k8s.io/apimachinery/pkg/api/errors"
	"k8s.io/apimachinery/pkg/api/resource"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	apiruntime "k8s.io/apimachinery/pkg/runtime"
	"k8s.io/apimachinery/pkg/types"
	"k8s.io/client-go/tools/record"
	ctrl "sigs.k8s.io/controller-runtime"
	"sigs.k8s.io/controller-runtime/pkg/client"
	"sigs.k8s.io/controller-runtime/pkg/controller/controllerutil"

	clickhousev1alpha1 "gitlab.com/gitlab-org/opstrace/opstrace/clickhouse-operator/api/v1alpha1"
)

// KubernetesResource abstracts a K8s resource for reconciliation.
// The mutator is used in CreateOrUpdate to maintain desired state.
type KubernetesResource struct {
	obj     client.Object
	mutator controllerutil.MutateFn
}

// Reconciler is used to reconcile any number of resources.
type Reconciler struct {
	log       logr.Logger
	client    client.Client
	apiReader client.Reader
	crd       *clickhousev1alpha1.ClickHouse
	scheme    *apiruntime.Scheme
	recorder  record.EventRecorder
}

// CreateOrUpdate will create or update the resource and set the controller ownership.
func (r *Reconciler) CreateOrUpdate(
	ctx context.Context, kr *KubernetesResource) (controllerutil.OperationResult, error) {
	obj := kr.obj

	// Set up garbage collection. The object (resource.obj) will be
	// automatically deleted when the owner (clickhouse) is deleted.
	err := controllerutil.SetControllerReference(r.crd, obj, r.scheme)
	if err != nil {
		r.log.Error(
			err,
			"failed to set owner reference on resource",
			"kind", obj.GetObjectKind().GroupVersionKind().Kind,
			"name", obj.GetName(),
		)
		r.recorder.Eventf(
			r.crd,
			corev1.EventTypeWarning,
			"ReconcileFailed",
			"failed to reconcile crd=%s resource=%s/%s err=%s",
			types.NamespacedName{Namespace: r.crd.GetNamespace(), Name: r.crd.GetName()},
			obj.GetObjectKind().GroupVersionKind().Kind,
			obj.GetName(),
			err,
		)
		return "", fmt.Errorf("reconcile set owner ref: %w", err)
	}

	op, err := controllerutil.CreateOrUpdate(ctx, r.client, obj, kr.mutator)
	if err != nil {
		r.log.Error(
			err,
			"failed to reconcile resource",
			"kind", obj.GetObjectKind().GroupVersionKind().Kind,
			"name", obj.GetName(),
		)
		return op, fmt.Errorf("reconcile create or update: %w", err)
	}

	r.log.Info(
		"Reconcile successful",
		"operation", op,
		"kind", obj.GetObjectKind().GroupVersionKind().Kind,
		"name", obj.GetName(),
	)

	return op, nil
}

// ReconcileStatefulSets reconciles the cluster StatefulSets.
// This uses a rollout strategy of one-by-one if the StatefulSets are being updated.
// For new clusters, it will create the StatefulSets all at once.
func (r *Reconciler) ReconcileStatefulSets(
	ctx context.Context, req ctrl.Request, ch *clickHouseCluster) (*ctrl.Result, error) {
	sts := newClickHouseStatefulSets(req, ch)
	for _, s := range sts {
		op, err := r.CreateOrUpdate(ctx, s)
		if err != nil {
			return nil, err
		}
		r.log.V(4).Info("Reconciling statefulset", "result", op, "name", s.obj.GetName())
		// continue if the sts was just created
		if op == controllerutil.OperationResultCreated {
			continue
		}
		// otherwise, check if the sts is ready before moving on
		check := &appsv1.StatefulSet{}
		n := types.NamespacedName{
			Namespace: s.obj.GetNamespace(),
			Name:      s.obj.GetName(),
		}
		if err := r.apiReader.Get(ctx, n, check); err != nil {
			return nil, fmt.Errorf("checking statefulset %s: %w", n, err)
		}
		if !isStsReady(check) {
			return &ctrl.Result{RequeueAfter: 5 * time.Second}, nil
		}

		// TODO(joe): check actual clickhouse-server connectivity to continue?
		// Any other checks we can do here?
	}

	// TODO(joe): delete statefulsets that are no longer needed?
	// might be risky to do this automatically at the moment.

	return nil, nil
}

func (r *Reconciler) gatherStorageInformation(
	ctx context.Context,
	req ctrl.Request,
	ch *clickHouseCluster,
) ([]*appsv1.StatefulSet, []types.NamespacedName, []types.NamespacedName, error) {
	stsToResize := make([]*appsv1.StatefulSet, 0)
	pvcsToResize := make([]types.NamespacedName, 0)
	unreadyHosts := make([]types.NamespacedName, 0)

	for _, host := range ch.Hosts {
		stsKey := types.NamespacedName{
			Namespace: req.Namespace,
			Name:      host,
		}
		pvcKey := types.NamespacedName{
			Name:      fmt.Sprintf("data-%s-0", stsKey.Name),
			Namespace: stsKey.Namespace,
		}

		pvcNeedsResize, err := r.pvcNeedsResize(ctx, pvcKey, ch.Spec.StorageSize)
		if err != nil {
			resErr := fmt.Errorf("unable to verify pvc %q of sts %s/%s: %w", pvcKey.Name, stsKey.Namespace, stsKey.Name, err)
			return nil, nil, nil, resErr
		}
		if pvcNeedsResize {
			pvcsToResize = append(pvcsToResize, pvcKey)
		}

		sts := new(appsv1.StatefulSet)
		if err := r.client.Get(ctx, stsKey, sts); err != nil {
			if !apierrors.IsNotFound(err) {
				return nil, nil, nil, fmt.Errorf("unable to fetch sts %s/%s: %w", stsKey.Namespace, stsKey.Name, err)
			}

			// STS does not exist yet. There may be multiple reasons for that:
			// * controller is just starting
			// * there could be a failed resize
			// * user manually deleted STS
			// In any case controller should recreate it, and  the new object
			// will have the correct size set, so no need to adjust it here.
			continue
		}

		if !isStsReady(sts) {
			unreadyHosts = append(unreadyHosts, stsKey)
		}

		stsNeedsResize, err := r.stsNeedsResize(sts, ch.Spec.StorageSize)
		if err != nil {
			return nil, nil, nil, err
		}
		if stsNeedsResize || pvcNeedsResize {
			// Resizing pvs requires sts to be deleted, hence we mark STS for
			// deletion.
			stsToResize = append(stsToResize, sts)
		}
	}

	return stsToResize, pvcsToResize, unreadyHosts, nil
}

// ReconcileStorageSize is the place where scaling CH cluster data volumes
// takes place.
//
// The way we do resize is in line with what Altinity operator does:
//
// https://github.com/Altinity/clickhouse-operator/blob/d7e654a341e4102038f6226febc338b8060c64dd/pkg/controller/chi/worker.go#L2074-L2102
//
// and how k8s community does it in general:
//
// https://itnext.io/resizing-statefulset-persistent-volumes-with-zero-downtime-916ebc65b1d4
//
// No pods gets restarted so DB should keep working undisturbed during the
// whole process. The steps can be summarized as:
// * make sure that stateful sets are in steady state/Ready
// * remove stateful sets but without removing the pods themselves - orphan them
// * increase PVCs storage capacity
// * re-add stateful sets, allow k8s to adopt orphaned pods
//
// Both STSes re-creation and PVC scaling is done by the same controller -
// clickhouse operator. Reconciliation for the given resource is a single
// threaded operation. I added one more stage in the pipeline that does the
// storage scalling in front of the previous reconciliation stages.
//
// So it looks like this ATM:
// |-> reconciliation event starts -> STSes are removed (new code) -> PVCs are scaled (new code) -> STSes are re-created with the new PVC templates that match scaled PVCs (old code) -> reconciliation ends ->|
//
// Decreasing size of storage is not supported. Scaling storage on Kind
// clusters is not supported:
//
// https://github.com/rancher/local-path-provisioner/issues/18
//
// GKE not only supports scaling storage but it also does it LIVE - without the
// need of restarting pods:
//
// https://kubernetes.io/docs/concepts/storage/persistent-volumes/#resizing-an-in-use-persistentvolumeclaim
//
// StorageClass for the given PVC must have `allowVolumeExpansion: yes` for
// expansion to work. We do not check it in the controller and leave it to the
// operator. Controller will print appropriate message in the logs in such
// case.
//
//nolint:lll
func (r *Reconciler) ReconcileStorageSize(
	ctx context.Context,
	req ctrl.Request,
	ch *clickHouseCluster,
) (*ctrl.Result, error) {
	stsToResize, pvcsToResize, unreadyHosts, err := r.gatherStorageInformation(ctx, req, ch)
	if err != nil {
		return nil, fmt.Errorf("unable to gather information about the current state of storage: %w", err)
	}

	resizeNeeded := len(stsToResize) > 0 && len(pvcsToResize) > 0
	if !resizeNeeded {
		r.log.V(5).Info("resizing of the CH cluster data-volumes is not needed")
		return nil, nil
	}
	if len(stsToResize) > 0 {
		tmp := make([]string, len(stsToResize))
		for i, v := range stsToResize {
			tmp[i] = fmt.Sprintf("%s/%s", v.Namespace, v.Name)
		}
		r.log.V(5).Info("STSes that need resizing/restarting", "sts_list", strings.Join(tmp, ","))
	}
	if len(pvcsToResize) > 0 {
		tmp := make([]string, len(pvcsToResize))
		for i, v := range pvcsToResize {
			tmp[i] = fmt.Sprintf("%s/%s", v.Namespace, v.Name)
		}
		r.log.V(5).Info("PVCs that need resizing", "pvc_list", strings.Join(tmp, ","))
	}

	allStsAreReady := len(unreadyHosts) == 0
	if !allStsAreReady {
		tmp := make([]string, len(unreadyHosts))
		for i, uh := range unreadyHosts {
			tmp[i] = fmt.Sprintf("%s/%s", uh.Namespace, uh.Name)
		}
		r.log.Info("found unready StatefulsSets, can't proceed with the resize", "unreadySTSes", strings.Join(tmp, ","))
		return &ctrl.Result{RequeueAfter: 5 * time.Second}, nil
	}

	for _, sts := range stsToResize {
		if err := r.client.Delete(ctx, sts, client.PropagationPolicy(metav1.DeletePropagationOrphan)); err != nil {
			if !apierrors.IsNotFound(err) {
				return nil, fmt.Errorf("unable to delete sts %s/%s: %w", sts.Namespace, sts.Name, err)
			}
			r.log.V(5).Info("sts is already removed", "name", sts.Name, "namespace", sts.Namespace)
		}
		// Pod will be re-created during main reconciliation loop
		r.log.V(5).Info("sts removed", "name", sts.Name, "namespace", sts.Namespace)
	}

	for _, pvcKey := range pvcsToResize {
		patch := []byte(fmt.Sprintf(`{"spec":{"resources":{"requests":{"storage": "%s"}}}}`, ch.Spec.StorageSize.String()))
		obj := &corev1.PersistentVolumeClaim{
			ObjectMeta: metav1.ObjectMeta{
				Name:      pvcKey.Name,
				Namespace: pvcKey.Namespace,
			},
		}
		err := r.client.Patch(ctx, obj, client.RawPatch(types.StrategicMergePatchType, patch))
		if err != nil {
			return nil, fmt.Errorf("unable to patch pvc %s/%s: %w", pvcKey.Namespace, pvcKey.Name, err)
		}
		r.log.V(5).Info("pvc patched", "name", pvcKey.Name, "namespace", pvcKey.Namespace)
	}

	return nil, nil
}

var ErrDataPVCNotFound = fmt.Errorf("`data` PVC has not been found for given STS")

func (r *Reconciler) pvcNeedsResize(
	ctx context.Context,
	key types.NamespacedName,
	storageSize resource.Quantity,
) (bool, error) {
	pvc := new(corev1.PersistentVolumeClaim)
	if err := r.client.Get(ctx, key, pvc); err != nil {
		if !apierrors.IsNotFound(err) {
			return false, fmt.Errorf("unable to fetch pvc %s/%s: %w", key.Namespace, key.Name, err)
		}
		// PVC has not been created yet, so it does not need resizing too.
		return false, nil
	}

	v, ok := pvc.Spec.Resources.Requests[corev1.ResourceStorage]
	if !ok {
		return true, nil
	}
	switch v.Cmp(storageSize) {
	case 1:
		return false, fmt.Errorf(
			"shrinking CH volumes is not supported, please adjust the CH CR storage size to be equal or greater than %s",
			v.String(),
		)
	case 0:
		return false, nil
	case -1:
		return true, nil
	}

	return false, nil
}

func (r *Reconciler) stsNeedsResize(sts *appsv1.StatefulSet, storageSize resource.Quantity) (bool, error) {
	for _, vct := range sts.Spec.VolumeClaimTemplates {
		if vct.Name != CHDataVolumeName {
			continue
		}

		// NOTE(prozlach): In case of the STS we are not worried about
		// shrinking claims, as K8s does not care about it really. Only
		// shrinking of the PVC is forbidden. Hence we allow for correcting the
		// claim of the STS itself, but not PVC.
		v, ok := vct.Spec.Resources.Requests[corev1.ResourceStorage]
		if !ok || !v.Equal(storageSize) {
			return true, nil
		}

		return false, nil
	}

	// PVC was not found, this is a serious inconsistency as PVC configuration
	// of stses is owned by this controllers.
	return false, fmt.Errorf("sts %s/%s is invalid: %w", sts.Namespace, sts.Name, ErrDataPVCNotFound)
}

func isStsReady(sts *appsv1.StatefulSet) bool {
	if sts.Spec.Replicas == nil {
		return false
	}
	replicas := *sts.Spec.Replicas

	// generation is being applied to replicas - it is observed right now
	if sts.Status.ObservedGeneration == 0 || sts.Generation > sts.Status.ObservedGeneration {
		return false
	}
	// All replicas are in "Ready" status - meaning ready to be used - no failure inside
	if sts.Status.ReadyReplicas < replicas {
		return false
	}
	// all replicas are of expected generation
	if sts.Status.CurrentReplicas < replicas {
		return false
	}
	// all replicas are updated - meaning rolling update completed over all replicas
	if sts.Status.UpdatedReplicas < replicas {
		return false
	}
	// available replicas are ready for minReadySeconds
	if sts.Status.AvailableReplicas < replicas {
		return false
	}
	// current revision is an updated one - meaning rolling update completed over all replicas
	if sts.Status.CurrentRevision != sts.Status.UpdateRevision {
		return false
	}

	return true
}
