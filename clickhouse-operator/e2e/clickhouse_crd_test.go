package e2e

import (
	. "github.com/onsi/ginkgo/v2"
	. "github.com/onsi/gomega"
	clickhousev1alpha1 "gitlab.com/gitlab-org/opstrace/opstrace/clickhouse-operator/api/v1alpha1"
	"k8s.io/utils/ptr"
)

var _ = Describe("ClickHouse cluster", func() {
	var clickhouseCluster *clickhousev1alpha1.ClickHouse
	beforeEachCreateCluster(&clickhouseCluster, clusterConfig{
		replicas: ptr.To(int32(2)),
	})

	Context("with 2 replicas", func() {

		BeforeEach(func(ctx SpecContext) {
			By("assert cluster readiness")
			assertClusterReady(ctx, clickhouseCluster)
			assertClusterRunning(ctx, clickhouseCluster)
		})

		It("should allow connection to a replica through named service", func(ctx SpecContext) {
			By("port-forward the ClickHouse replicas")

			db, closer := openDB(ctx, clickhouseCluster, dbConfig{})
			defer closer()
			Expect(db.Ping(ctx)).To(Succeed())
		})
	})
})
