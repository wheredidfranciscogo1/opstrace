package v1alpha1

import (
	"fmt"

	"github.com/fluxcd/kustomize-controller/api/v1beta2"
	v1 "k8s.io/api/core/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/runtime"
)

// EDIT THIS FILE!  THIS IS SCAFFOLDING FOR YOU TO OWN!
// NOTE: json tags are required.  Any new fields you add must have json tags for the fields to be serialized.

// GroupSpec defines the desired state of Group.
type GroupSpec struct {
	ID                int64   `json:"id"`
	Name              string  `json:"name"`
	Path              string  `json:"path"`
	FullPath          string  `json:"full_path"`
	Description       string  `json:"description"`
	AvatarURL         string  `json:"avatar_url"`
	WebURL            string  `json:"web_url"`
	ClickhouseAddress *string `json:"clickhouseAddress,omitempty"`
	// domain which this group is configured for, e.g. observe.gitlab.com
	// +optional
	Domain *string `json:"domain,omitempty"`
	// +optional
	ImagePullSecrets []v1.LocalObjectReference `json:"imagePullSecrets,omitempty"`
	// Set overrides for components and config
	// +optional
	Overrides GroupOverridesSpec `json:"overrides"`
	// Setup features for group and its resources
	// +optional
	Features map[string]string `json:"features,omitempty"`
}

func (g *GroupSpec) GetHost() string {
	if g.Domain != nil {
		return *g.Domain
	}
	return localhost
}

func (g *GroupSpec) GetHostURL() string {
	if g.Domain != nil {
		return fmt.Sprintf("https://%s", *g.Domain)
	}
	return localhostURL
}

type GroupOverridesSpec struct {
	// +optional
	ErrorTracking ErrorTrackingSpec `json:"errortracking"`
	// +optional
	Jaeger JaegerSpec `json:"jaeger"`
	// +optional
	OpenTelemetry OpenTelemetrySpec `json:"opentelemetry"`
	// +optional
	Features map[string]string `json:"features,omitempty"`
}

type ErrorTrackingSpec struct {
	// +optional
	Components ErrorTrackingComponentsSpec `json:"components"`
}

type ErrorTrackingComponentsSpec struct {
	// +optional
	IngressRoute IngressRoute `json:"ingressRoute"`
	// +optional
	Service Service `json:"service"`
	// +optional
	Deployment Deployment `json:"deployment"`
	// +optional
	StatefulSet StatefulSet `json:"statefulset"`
	// +optional
	ServiceMonitor ServiceMonitor `json:"serviceMonitor"`
	// +optional
	Quotas v1.ConfigMap `json:"quotas"`
}

type OpenTelemetrySpec struct {
	// See https://opentelemetry.io/docs/collector/configuration/
	// +kubebuilder:pruning:PreserveUnknownFields
	// +optional
	Config runtime.RawExtension `json:"config"`
	// +optional
	Components OpenTelemetryComponentsSpec `json:"components"`
}

// Considering this is a temporary change only specific to the OpenTelemetrySpec
// object, creating a new one here. We can resort back to using our general
// ComponentsSpec once we drop jaeger-support from our OpenTelemetry collector
// deployment.
type OpenTelemetryComponentsSpec struct {
	// +optional
	IngressRoute IngressRoute `json:"ingressRoute"`
	// +optional
	JaegerIngress IngressRoute `json:"jaegerIngressRoute"`
	// +optional
	Service Service `json:"service"`
	// +optional
	Deployment Deployment `json:"deployment"`
	// +optional
	StatefulSet StatefulSet `json:"statefulset"`
	// +optional
	ServiceMonitor ServiceMonitor `json:"serviceMonitor"`
}

type JaegerSpec struct {
	// +optional
	Sampling JaegerSamplingSpec `json:"sampling"`
	// +optional
	Config JaegerPluginConfigOptions `json:"plugin_config"`
	// +optional
	Components JaegerComponentsSpec `json:"components"`
}

// FreeForm defines a common options parameter that maintains the hierarchical structure of the data,
// unlike Options which flattens the hierarchy into a key/value map
// where the hierarchy is converted to '.' separated items in the key.
type FreeForm struct {
	Json *[]byte `json:"-"`
}
type JaegerSamplingSpec struct {
	// +optional
	// +kubebuilder:pruning:PreserveUnknownFields
	Options FreeForm `json:"options,omitempty"`
}

type JaegerComponentsSpec struct {
	// +optional
	IngressRoute IngressRoute `json:"ingressRoute"`
	// +optional
	Service Service `json:"service"`
	// +optional
	Deployment JaegerDeploymentSpec `json:"deployment"`
	// +optional
	ServiceMonitor ServiceMonitor `json:"serviceMonitor"`
	// +optional
	Quotas v1.ConfigMap `json:"quotas"`
}

type JaegerDeploymentSpec struct {
	// +optional
	Labels map[string]string `json:"labels"`
	// +optional
	Annotations map[string]string `json:"annotations"`
	// +optional
	Spec *[]byte `json:"-"`
}

type JaegerPluginConfigOptions struct {
	// ClickHouse service address
	Address string `json:"address,omitempty"`
	// maximum number of pending spans in memory before discarding data.
	// default is 10_000_000, but that was observed to use multiple gigs of memory if
	// clickhouse was too bogged down, to the point of evicting the pod on a 16GB host node.
	// meanwhile 100K results in ~1.2GB of memory usage when the queue is full/stuck.
	MaxSpanCount int `json:"max_span_count,omitempty"`
	// wait until 10k spans have been received, or after 5s, whichever comes first
	BatchWriteSize int `json:"batch_write_size,omitempty"`
	// duration e.g. "5s"
	BatchFlushInterval string `json:"batch_flush_interval,omitempty"`
	// json or protobuf: guessing that protobuf is a bit faster/smaller
	Encoding string `json:"encoding,omitempty"`
	// ClickHouse username
	Username string `json:"username,omitempty"`
	// ClickHouse password
	Password string `json:"password,omitempty"`
	// shared tracing DB across tenants
	Database string `json:"database,omitempty"`
	// include a tenant name for shared/multitenant tables
	// the value just needs to be a valid string that's distinct to the tenant
	Tenant string `json:"tenant,omitempty"`
	// prometheus scraping endpoint
	MetricsEndpoint string `json:"metrics_endpoint,omitempty"`
	// use replication-enabled schemas in clickhouse
	Replication bool `json:"replication,omitempty"`
	// days of data retention, or 0 to disable. configured at table setup via init sql scripts
	// reference: https://clickhouse.com/docs/en/engines/table-engines/mergetree-family/mergetree/#table_engine-mergetree-ttl
	TTLDays int `json:"ttl,omitempty"`
	// Directory with .sql files to run at plugin startup
	// Depending on the value of "init_tables", this can be run as a
	// replacement or supplement to creating default tables for span storage.
	// If `init_tables` is also enabled, the scripts in this directory will be run first.
	InitSQLScriptsDir string `json:"init_sql_scripts_dir,omitempty"`
	// Whether to automatically attempt to create tables in ClickHouse.
	// By default, this is enabled if init_sql_scripts_dir is empty,
	// or disabled if init_sql_scripts_dir is provided.
	InitTables bool `json:"init_tables,omitempty"`
	// Expands TraceID to its hex string representation while writing.
	ExpandTraceID *bool `json:"expand_trace_id,omitempty"`
	// Table with spans. Default "jaeger_spans_local" or "jaeger_spans" when replication is enabled.
	SpansTable string `json:"spans_table,omitempty"`
	// Span index table. Default "jaeger_index_local" or "jaeger_index" when replication is enabled.
	SpansIndexTable string `json:"spans_index_table,omitempty"`
	// Operations table. Default "jaeger_operations_local" or "jaeger_operations" when replication is enabled.
	OperationsTable string `json:"operations_table,omitempty"`
}

// GroupStatus defines the observed state of Group.
type GroupStatus struct {
	Conditions []metav1.Condition `json:"conditions,omitempty"`

	// Inventory contains the list of Kubernetes resource object references that have been successfully applied.
	// +optional
	Inventory map[string]*v1beta2.ResourceInventory `json:"inventory,omitempty"`
}

// +k8s:deepcopy-gen:interfaces=k8s.io/apimachinery/pkg/runtime.Object
// +kubebuilder:object:root=true
// +kubebuilder:subresource:status
// Group is the Schema for the Groups API.
type Group struct {
	metav1.TypeMeta   `json:",inline"`
	metav1.ObjectMeta `json:"metadata,omitempty"`

	Spec   GroupSpec   `json:"spec,omitempty"`
	Status GroupStatus `json:"status,omitempty"`
}

// +k8s:deepcopy-gen:interfaces=k8s.io/apimachinery/pkg/runtime.Object
// +kubebuilder:object:root=true
// +kubebuilder:subresource:status
// GroupList contains a list of Group.
type GroupList struct {
	metav1.TypeMeta `json:",inline"`
	metav1.ListMeta `json:"metadata,omitempty"`
	Items           []Group `json:"items"`
}

func init() {
	SchemeBuilder.Register(&Group{}, &GroupList{})
}
