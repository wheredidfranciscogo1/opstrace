package jaeger

import (
	"fmt"
	"net/url"

	utils "gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/common"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/constants"
	"gitlab.com/gitlab-org/opstrace/opstrace/tenant-operator/api/v1alpha1"
	corev1 "k8s.io/api/core/v1"
	v1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"sigs.k8s.io/controller-runtime/pkg/client"
)

func getClickHouseCredentialsName(cr *v1alpha1.Group) string {
	return fmt.Sprintf("%s-%d", constants.JaegerClickhouseSecretName, cr.Spec.ID)
}

// Create a username for the Jaeger-clickhouse plugin deployed for this group.
// This allows us to enforce quota in Clickhouse per group for tracing.
func getClickHouseUser(cr *v1alpha1.Group) string {
	return fmt.Sprintf("%s_%d", constants.JaegerNamePrefix, cr.Spec.ID)
}

func GetClickHouseCredentialsUser(cr *v1alpha1.Group, current *corev1.Secret) string {
	if current != nil && current.Data[constants.ClickHouseCredentialsUserKey] != nil {
		return string(current.Data[constants.ClickHouseCredentialsUserKey])
	}
	return getClickHouseUser(cr)
}

func GetClickHouseCredentialsPassword(current *corev1.Secret) string {
	if current != nil && current.Data[constants.ClickHouseCredentialsPasswordKey] != nil {
		return string(current.Data[constants.ClickHouseCredentialsPasswordKey])
	}
	return utils.RandStringRunes(10)
}

func getClickhouseCredentialsData(
	cr *v1alpha1.Group,
	current *corev1.Secret,
	clickHouseURL *url.URL,
) map[string][]byte {
	user := GetClickHouseCredentialsUser(cr, current)
	pass := GetClickHouseCredentialsPassword(current)
	// set the new user/password on the same clickhouse address for this group's Jaeger plugin
	clickHouseURL.User = url.UserPassword(user, pass)

	credentials := map[string][]byte{
		constants.ClickHouseCredentialsUserKey:           []byte(user),
		constants.ClickHouseCredentialsPasswordKey:       []byte(pass),
		constants.ClickHouseCredentialsNativeEndpointKey: []byte(clickHouseURL.String()),
	}
	return credentials
}

func ClickHouseCredentialsSecret(cr *v1alpha1.Group, clickHouseURL url.URL) *corev1.Secret {
	data := getClickhouseCredentialsData(cr, nil, &clickHouseURL)

	return &corev1.Secret{
		ObjectMeta: v1.ObjectMeta{
			Name:      getClickHouseCredentialsName(cr),
			Namespace: cr.Namespace,
		},
		Data: data,
		Type: corev1.SecretTypeOpaque,
	}
}

func ClickHouseCredentialsSecretMutator(cr *v1alpha1.Group, current *corev1.Secret, clickHouseURL url.URL) {
	data := getClickhouseCredentialsData(cr, current, &clickHouseURL)
	current.Data = data
}

func ClickHouseCredentialsSecretSelector(cr *v1alpha1.Group) client.ObjectKey {
	return client.ObjectKey{
		Namespace: cr.Namespace,
		Name:      getClickHouseCredentialsName(cr),
	}
}
