package jaeger

import (
	"fmt"

	traefik "github.com/traefik/traefik/v2/pkg/provider/kubernetes/crd/traefikio/v1alpha1"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/common"
	"gitlab.com/gitlab-org/opstrace/opstrace/tenant-operator/api/v1alpha1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/util/intstr"
	"sigs.k8s.io/controller-runtime/pkg/client"
)

func getIngressRouteSpec(cr *v1alpha1.Group) traefik.IngressRouteSpec {
	return traefik.IngressRouteSpec{
		EntryPoints: []string{"websecure"},
		TLS:         &traefik.TLS{},
		Routes: []traefik.Route{
			{
				Match:    fmt.Sprintf("Host(`%s`) && PathPrefix(`/v1/jaeger/%d`)", cr.Spec.GetHost(), cr.Spec.ID),
				Priority: 50,
				Kind:     "Rule",
				Middlewares: []traefik.MiddlewareRef{
					// TODO(prozlach): I am hardcoding the namespace of the
					// `Cluster` CR here as this whole code will soon be
					// removed/replaced by the new GitlabObservabilityTenant CRD.
					{
						Name:      "cors-headers-jaegerotel",
						Namespace: "default",
					},
					{
						Name:      "forward-auth",
						Namespace: "default",
					},
					{
						Name:      "strip-prefix-jaeger",
						Namespace: "default",
					},
					{
						Name:      "legacy-tenant-authheader-rm",
						Namespace: "default",
					},
				},
				Services: []traefik.Service{
					{
						LoadBalancerSpec: traefik.LoadBalancerSpec{
							Kind:      "Service",
							Name:      GetJaegerName(cr),
							Namespace: cr.Namespace,
							Port: intstr.IntOrString{
								Type:   intstr.String,
								StrVal: "query",
							},
							Scheme:   "http",
							Strategy: "RoundRobin",
							NativeLB: true,
						},
					},
				},
			},
		},
	}
}

func IngressRoute(cr *v1alpha1.Group) *traefik.IngressRoute {
	return &traefik.IngressRoute{
		ObjectMeta: metav1.ObjectMeta{
			Name:      GetJaegerName(cr),
			Namespace: cr.Namespace,
		},
		Spec: getIngressRouteSpec(cr),
	}
}

func IngressRouteMutator(cr *v1alpha1.Group, current *traefik.IngressRoute) error {
	currentSpec := &current.Spec
	spec := getIngressRouteSpec(cr)
	// Apply default overrides
	if err := common.PatchObject(
		currentSpec,
		&spec,
	); err != nil {
		return err
	}
	// Apply CR overrides
	if err := common.PatchObject(
		currentSpec,
		cr.Spec.Overrides.Jaeger.Components.IngressRoute.Spec,
	); err != nil {
		return err
	}
	current.Spec = *currentSpec

	return nil
}
func IngressSelector(cr *v1alpha1.Group) client.ObjectKey {
	return client.ObjectKey{
		Namespace: cr.Namespace,
		Name:      GetJaegerName(cr),
	}
}
