package tenant

import (
	"context"
	"fmt"

	"github.com/fluxcd/kustomize-controller/api/v1beta2"
	"github.com/fluxcd/pkg/runtime/patch"
	"github.com/fluxcd/pkg/ssa"
	"github.com/go-logr/logr"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/common"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/constants"
	opstracev1alpha1 "gitlab.com/gitlab-org/opstrace/opstrace/tenant-operator/api/v1alpha1"
	"gitlab.com/gitlab-org/opstrace/opstrace/tenant-operator/controllers/config"
	appsv1 "k8s.io/api/apps/v1"
	v1 "k8s.io/api/core/v1"
	netv1 "k8s.io/api/networking/v1"
	"k8s.io/apimachinery/pkg/api/errors"
	apierrors "k8s.io/apimachinery/pkg/api/errors"
	apimeta "k8s.io/apimachinery/pkg/api/meta"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/runtime"
	"k8s.io/apimachinery/pkg/types"
	kerrors "k8s.io/apimachinery/pkg/util/errors"
	"k8s.io/client-go/tools/record"
	"sigs.k8s.io/cli-utils/pkg/kstatus/polling"
	ctrl "sigs.k8s.io/controller-runtime"
	"sigs.k8s.io/controller-runtime/pkg/client"
	"sigs.k8s.io/controller-runtime/pkg/controller/controllerutil"
	"sigs.k8s.io/controller-runtime/pkg/handler"
	"sigs.k8s.io/controller-runtime/pkg/reconcile"
)

const (
	ControllerName = "tenant-controller"
	finalizerName  = "tenant.opstrace.com/finalizer"
)

//nolint:lll
// +kubebuilder:rbac:groups=opstrace.com,resources=tenants;tenants/finalizers,verbs=get;list;watch;create;update;patch;delete
// +kubebuilder:rbac:groups=opstrace.com,resources=tenants/status,verbs=get;update;patch
// +kubebuilder:rbac:groups=opstrace.com,resources=tenants/finalizers,verbs=update
// +kubebuilder:rbac:groups=extensions;apps,resources=deployments;deployments/finalizers,verbs=get;list;watch;create;update;patch;delete
// +kubebuilder:rbac:groups="",resources=events,verbs=get;list;watch;create;patch
// +kubebuilder:rbac:groups="",resources=configmaps;secrets;serviceaccounts;services;persistentvolumeclaims,verbs=get;list;watch;create;update;patch;delete
// +kubebuilder:rbac:groups=networking.k8s.io,resources=ingresses,verbs=get;list;watch;create;update;patch;delete
// +kubebuilder:rbac:groups=monitoring.coreos.com,resources=servicemonitors,verbs=get;list;watch;create;update;patch;delete

// SetupWithManager sets up the controller with the Manager.
func (r *ReconcileTenant) SetupWithManager(mgr ctrl.Manager) error {
	return ctrl.NewControllerManagedBy(mgr).
		For(&opstracev1alpha1.Tenant{}).
		// enqueue a reconcile for this tenant if any groups change.
		// this is primarily useful for tracking tenant teardown
		Watches(&opstracev1alpha1.Group{},
			handler.EnqueueRequestsFromMapFunc(
				func(_ context.Context, a client.Object) []reconcile.Request {
					return []reconcile.Request{
						{NamespacedName: types.NamespacedName{
							Name:      constants.TenantName,
							Namespace: a.GetNamespace(),
						}},
					}
				},
			)).
		Owns(&appsv1.Deployment{}).
		Owns(&netv1.Ingress{}).
		Owns(&v1.ConfigMap{}).
		Owns(&v1.Service{}).
		Owns(&v1.ServiceAccount{}).
		Complete(r)
}

var _ reconcile.Reconciler = &ReconcileTenant{}

// ReconcileTenant reconciles a Grafana object
type ReconcileTenant struct {
	// This client, initialized using mgr.Client() above, is a split client
	// that reads objects from the cache and writes to the apiserver
	Client       client.Client
	Scheme       *runtime.Scheme
	Log          logr.Logger
	Config       *config.ControllerConfig
	Recorder     record.EventRecorder
	StatusPoller *polling.StatusPoller
}

// TODO(joe): this is temporary function to gradually cleanup all unneeded CustomeResources.
func (r *ReconcileTenant) RemoveGOUI(ctx context.Context, request reconcile.Request) error {
	// delete unused postgres secret
	err := r.Client.Delete(ctx, &v1.Secret{
		ObjectMeta: metav1.ObjectMeta{
			Name:      "argus-postgres-credentials",
			Namespace: request.Namespace,
		},
	}, &client.DeleteOptions{})
	if client.IgnoreNotFound(err) != nil {
		return fmt.Errorf("delete postgres secret: %w", err)
	}

	return nil
}

// Reconcile reads the state of the cluster for a Tenant object and makes changes based on the state read
// and what's defined in its spec.
func (r *ReconcileTenant) Reconcile(
	ctx context.Context,
	request reconcile.Request,
) (result reconcile.Result, err error) {
	if err := r.RemoveGOUI(ctx, request); err != nil {
		r.Log.Error(err, "Removing GOUI", "name", request.Name, "namespace", request.Namespace)
		return reconcile.Result{}, err
	}

	tenant := &opstracev1alpha1.Tenant{}
	err = r.Client.Get(ctx, request.NamespacedName, tenant)
	if err != nil {
		if errors.IsNotFound(err) {
			return reconcile.Result{}, nil
		}
		return reconcile.Result{}, err
	}

	cr := tenant.DeepCopy()

	// Initialize the runtime patcher with the current version of the object.
	patcher := patch.NewSerialPatcher(cr, r.Client)

	// Finalize the reconciliation
	defer func() {
		// Configure the runtime patcher.
		patchOpts := []patch.Option{
			patch.WithFieldOwner(constants.TenantFieldManagerIDString),
		}

		// Patch the object status, conditions and finalizers.
		if patchErr := patcher.Patch(ctx, cr, patchOpts...); patchErr != nil {
			if !cr.GetDeletionTimestamp().IsZero() {
				patchErr = kerrors.FilterOut(patchErr, apierrors.IsNotFound)
			}
			err = kerrors.NewAggregate([]error{err, patchErr})
		}
	}()

	// Add finalizer first if it doesn't exist to avoid the race condition
	// between init and delete.
	if !controllerutil.ContainsFinalizer(cr, finalizerName) {
		controllerutil.AddFinalizer(cr, finalizerName)
		return ctrl.Result{Requeue: true}, nil
	}

	if cr.Status.Inventory == nil {
		cr.Status.Inventory = make(map[string]*v1beta2.ResourceInventory)
	}

	teardown := !cr.ObjectMeta.DeletionTimestamp.IsZero()

	// Read current state
	currentState := NewTenantState()
	err = currentState.Read(ctx, cr, r.Client)
	if err != nil {
		r.Log.Error(err, "error reading state")
		r.manageError(cr, err)
		return reconcile.Result{}, err
	}
	// Get the actions required to reach the desired state
	tenantReconciler := NewTenantReconciler(teardown, r.Log)
	desiredState := tenantReconciler.Reconcile(currentState, cr)

	// Create the server-side apply manager.
	resourceManager := ssa.NewResourceManager(r.Client, r.StatusPoller, ssa.Owner{
		Field: constants.ClusterFieldManagerIDString,
		Group: cr.GetObjectKind().GroupVersionKind().Group,
	})

	// Run the actions to reach the desired state
	actionRunner := common.NewActionRunner(ctx, r.Client, r.Scheme, cr, resourceManager)
	err = actionRunner.RunAll(desiredState)
	if err != nil {
		r.manageError(cr, err)
		return reconcile.Result{}, err
	}

	if teardown {
		if controllerutil.ContainsFinalizer(cr, finalizerName) {
			// Successfully deleted everything we care about.
			// Remove our finalizer from the list and update it
			controllerutil.RemoveFinalizer(cr, finalizerName)
		}
	} else {
		r.manageSuccess(cr)
	}

	return reconcile.Result{}, nil
}

func (r *ReconcileTenant) manageError(cr *opstracev1alpha1.Tenant, issue error) {
	condition := metav1.Condition{
		Status:             metav1.ConditionFalse,
		Reason:             common.ReconciliationFailedReason,
		Message:            issue.Error(),
		Type:               common.ConditionTypeReady,
		ObservedGeneration: cr.GetGeneration(),
	}
	apimeta.SetStatusCondition(&cr.Status.Conditions, condition)
}

func (r *ReconcileTenant) manageSuccess(cr *opstracev1alpha1.Tenant) {
	condition := metav1.Condition{
		Status:             metav1.ConditionTrue,
		Reason:             common.ReconciliationSuccessReason,
		Message:            "All components are in ready state",
		Type:               common.ConditionTypeReady,
		ObservedGeneration: cr.GetGeneration(),
	}
	apimeta.SetStatusCondition(&cr.Status.Conditions, condition)

	r.Log.Info("desired Tenant state met")
}
