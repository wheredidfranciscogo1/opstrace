# Error Tracking

Error Tracking allows developers to ingest, view & discover the errors that their application may be generating during their execution.

Error Tracking is in GA and is available for all GitLab.com users. See the [public documentation](https://docs.gitlab.com/ee/operations/error_tracking.html) for details.
