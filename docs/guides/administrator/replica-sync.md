# Administrator Guide to re-sync diverged replicated tables on any specific ClickHouse node

> We will soon be migrating to ClickHouse Cloud so this guide will be deprecated. Details in [this epic](https://gitlab.com/groups/gitlab-org/opstrace/-/epics/82).

In a scenario where a replicated table in ClickHouse has separated from the correct active replica set, this guide can be followed.

An example situation can be seen [here](https://gitlab.com/gitlab-org/opstrace/opstrace/-/issues/2068). We'll take the tables from this same issue to illustrate this guide.

The symptoms of a separate set of tables is inconsistent query results and ideally we should be alerted with metrics around active replicas in ClickHouse.
Gist of the following steps is that we need to drop the separately created table from the problematic node and recreate it with _correct_ replica paths.

## Steps

1. Get the correct `replica_path` for the diverged table from a replica that is in **correct** state. For example, it could be `cluster-0-1` and `cluster-0-2`.
    We should follow the same step repeatedly for every table that has been diverged.

    ```sql
    SELECT
    database,
    table,
    engine,
    replica_name,
    active_replicas,
    replica_is_active,
    replica_path
    FROM system.replicas
    WHERE database = 'errortracking_api'  -- example Database that has diverged tables, replace it the specific Database in any particular case
    AND table = 'error_tracking_error_events' -- replace table name with the specific table or leave it altogether in case of multiple tables
    FORMAT Vertical

    Row 1:
    ──────
    database:          errortracking_api
    table:             error_tracking_error_events
    engine:            ReplicatedMergeTree
    replica_name:      cluster-0-2
    active_replicas:   2
    replica_is_active: {'cluster-0-1':1,'cluster-0-2':1}   -- Notice that active replicas are 2 instead of 3 and misses one node
    replica_path:      /clickhouse/cluster/tables/0/errortracking_api/error_tracking_error_events/d772b827-f991-4c3f-bceb-6933cdc3c12e/replicas/cluster-0-2
    ```

2. Copy the `replica_path` of the diverged table(s). We would need them to re-create the table on the node that shows the problems.

3. SSH into the node with incorrect tables. In this example, it would be `cluster-0-0`.
4. Execute `DROP TABLE $TABLE_NAME`. Replace `$TABLE_NAME` with the specific table.
    If all the tables of a specific database have been diverged, we can also use `DROP DATABASE`.
    Be careful to **avoid** `ON CLUSTER` statements when performing this action.
5. If the whole database was dropped, recreate the database with `CREATE DATABASE $DATABASE_NAME`.
6. Recreate the missing table(s) with the **same** attached replica path(we fetched it in step 1) to their DDL. Be careful to avoid `ON CLUSTER` statement.
    For example:

    ```sql
    CREATE TABLE IF NOT EXISTS error_tracking_error_events (
    project_id UInt64,
    fingerprint UInt32,
    name String,
    description String,
    actor String,
    environment LowCardinality(String),
    platform String,
    level LowCardinality(String),
    user_identifier String,
    payload String CODEC(LZ4HC(9)), -- AVG 20K bytes
    occurred_at DateTime64(6, 'UTC') -- Precision till microseconds
    ) ENGINE = ReplicatedMergeTree('/clickhouse/{cluster}/tables/{shard}/{database}/{table}/d772b827-f991-4c3f-bceb-6933cdc3c12e', '{replica_path}') --- notice that the replica path is the same as the tables in the correct set
    PARTITION BY toYYYYMM(occurred_at)
    ORDER BY (project_id, fingerprint, occurred_at);

    --- Notice the lack of `ON CLUSTER` statement and the addition of hardcoded replica path to the `ReplicatedMergeTree(...)`. This is required to reattach the table to an existing replica metadata.
    ```

7. Repeat the above step carefully for each of the missing table(s) and replacing their replica path with their corresponding one.
8. Verify that the replicas have been correct synced after running the same query as in step 1.

    ```sql
    SELECT
    database,
    table,
    engine,
    replica_name,
    active_replicas,
    replica_is_active,
    replica_path
    FROM system.replicas
    WHERE database = 'errortracking_api'  -- example Database that has diverged tables, replace it the specific Database in any particular case
    AND table = 'error_tracking_error_events' -- replace table name with the specific table or leave it altogether in case of multiple tables
    FORMAT Vertical

    Row 1:
    ──────
    database:          errortracking_api
    table:             error_tracking_error_events
    engine:            ReplicatedMergeTree
    replica_name:      cluster-0-0
    active_replicas:   3
    replica_is_active: {'cluster-0-1':1,'cluster-0-2':1,'cluster-0-0':1}
    replica_path:      /clickhouse/cluster/tables/0/errortracking_api/error_tracking_error_events/d772b827-f991-4c3f-bceb-6933cdc3c12e/replicas/cluster-0-0
    ```

9. Also verify that the data in recreated table(s) has been replicated by issuing `SELECT COUNT(*)` queries.
