# Contributor Guide

Get started with:

* [setting up your dev env](./setting-up-your-dev-env.md)
* [development](./development.md))
* [testing](./testing.md)
