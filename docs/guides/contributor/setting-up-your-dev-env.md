# Development environment

We support Linux (AMD64) and macOS (ARM64) as development environments.

To install GitLab Observability Backend (GOB), install and configure the following third-party dependencies.
You can do this manually, or [automatically by using asdf](https://asdf-vm.com/) or [rtx](https://github.com/jdx/rtx):

* [kind](https://kind.sigs.k8s.io/docs/user/quick-start/#installation) for creating a local Kubernetes cluster.
* [Docker](https://docs.docker.com/install)
  * [Docker Compose](https://docs.docker.com/compose/compose-v2/) is now part of the `docker` distribution.
* [kubectl](https://kubernetes.io/docs/tasks/tools/#kubectl) for interacting with GitLab Observability.
* [Telepresence](https://www.telepresence.io/) allows you to code and test microservices locally against a remote Kubernetes cluster.
* [gojq](https://github.com/itchyny/gojq) for some Makefile utilities.
* [Go 1.21](https://go.dev/doc/install).

The current versions of these dependencies are pinned in the `.tool-versions` file in the project.

You can run the following commands to check the availability and versions of these dependencies on your machine:

```shell
kind --version
docker --version
kubectl version
telepresence version
gojq --version
go version
```
