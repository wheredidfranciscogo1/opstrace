# API Docs

We provide API documentation in [OpenAPI v2 (aka Swagger v2) formats](https://swagger.io/specification/v2/).

## Swagger UI

A [Swagger UI](https://swagger.io/docs/open-source-tools/swagger-ui) deployment is provided as part of the GOB. This can be accessed on `<gob_host>/swagger` and requires being logged into the connected GitLab instance.#

## Versioning

We dynamically set the spec `info.version` to the docker image version so we can
clearly see the current version of the APIs.

For a tagged release this is a semver compatible string like `0.1.2`.

We don't use the versions in URLs (e.g. `/v1/foo`) for the spec version as a single API will likely have multiple of these versions
and it does not accurately represent the version of the API.

## Error Tracking

Currently error tracking uses a hand-written [swagger.yaml](../../../go/pkg/errortracking/swagger.yaml) along with [go-swagger](https://goswagger.io/) to generate the code from the specification.

For this API we did not use Gin as we do for all subsequent APIs.

## Gin APIs

For the APIs using Gin we have used the [gin-swagger](https://github.com/swaggo/gin-swagger) library.
This allows for a [declarative comments format](https://github.com/swaggo/swag#declarative-comments-format) in the code to generate the swagger docs.

In the `main.go` file for each API there will be a similar directive:

```go
//go:generate go run github.com/swaggo/swag/cmd/swag init
// swaggo parameters:
// @Title MY API
// @Description What is my API?
```

This creates a `docs/` dir containing the code scaffolding and swagger files.

Follow the [Gin howto](https://github.com/swaggo/swag#how-to-use-it-with-gin) for more instructions.

## Generation

Each API package has a `go:generate` directive to issue the swagger code generation.

Running `make regenerate` will run all these `go generate` operations.
This is also verified in CI.
