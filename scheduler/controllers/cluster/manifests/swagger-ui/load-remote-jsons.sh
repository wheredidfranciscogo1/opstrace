#!/usr/bin/env sh

set -e

# docs path to write jsons to
docs_path="${DOCS_PATH:?DOCS_PATH is not set}"

# get json from url and write to docs
get_doc() {
  wget -O "${docs_path}/$1.json" "$2"
}

# pairs of "doc-name url" to wget separated by newlines
swagger_url_pairs=${SWAGGER_URL_PAIRS:?SWAGGER_URL_PAIRS is not set}

echo "$swagger_url_pairs" | while read -r line; do get_doc $line; done
