This directory contains all the source manifests used by scheduler to launch components.
They are kustomized internally by scheduler during reconciliation to match configuration specified by `Cluster` object.

Each directory MUST (as in RFC sense):
* contain an `UPDATING.md` file that describes how manifests can be updated,
* contain a `kustomization.yaml` file which:
  * performs adjustments to the source manifests that need to be done for all kinds of `Cluster` objects (i.e. base changes),
  * concatenates all resources/manifests into a single YAML file using kustomize's `resources` field,

Optionally, each directory may also contain files starting with `delete_` prefix.
These files will be available during reconciliation stage.
So it is possible to conditionally remove manifests from the bundle created during initial kustomization run during controller start.
See the `cert-manager` directory and reconciler code for an example how it is done.
The Letsencrypt cluster issuers are created conditionally, basing on the cluster type.
