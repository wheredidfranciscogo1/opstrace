package cluster

import (
	"fmt"
	"strings"

	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/constants"
	"gitlab.com/gitlab-org/opstrace/opstrace/scheduler/api/v1alpha1"
	logf "sigs.k8s.io/controller-runtime/pkg/log"
	"sigs.k8s.io/kustomize/api/types"
	kustomize "sigs.k8s.io/kustomize/api/types"
	"sigs.k8s.io/kustomize/kyaml/resid"
)

const (
	ingressPathMetrics = `/v3/query/{projectID:[0-9]+}/{endpoint:(metrics|logs)}`
)

type QueryAPIReconciler struct {
	BaseReconciler
	clusterState *ClusterState
}

func NewQueryAPIReconciler(
	initialManifests map[string][]byte,
	teardown bool,
	clusterState *ClusterState,
) *QueryAPIReconciler {
	res := &QueryAPIReconciler{
		clusterState: clusterState,
		BaseReconciler: BaseReconciler{
			Teardown:         teardown,
			Log:              logf.Log.WithName(fmt.Sprintf("manifests/%s", constants.QueryAPIInventoryID)),
			initialManifests: initialManifests,
			inventoryID:      constants.QueryAPIInventoryID,
			reconcilerName:   "query-api",
			getOverridesF: func(cr *v1alpha1.Cluster) *v1alpha1.KustomizeOverridesSpec {
				return cr.Spec.Overrides.QueryAPI
			},
			serviceAccountName: constants.QueryAPIName,
		},
	}
	res.subclassApplyMethod = res.applyConfiguration
	return res
}

func (r *QueryAPIReconciler) applyConfiguration(cr *v1alpha1.Cluster) (*kustomize.Kustomization, error) {
	res, err := r.BaseReconciler.applyBaseConfiguration(cr)
	if err != nil {
		return nil, err
	}
	res.Images = []kustomize.Image{
		{
			Name:    "query-api-image-placeholder",
			NewName: constants.DockerImageName(constants.QueryAPIImageName),
			NewTag:  constants.DockerImageTag,
		},
	}
	err = r.applyDeploymentConfiguration(res)
	if err != nil {
		return nil, err
	}

	res.Patches = append(
		res.Patches,
		types.Patch{
			// Ingress
			Patch: fmt.Sprintf(
				`[
					{"op": "replace", "path": "/spec/routes/0/match", "value": "Host(\"%s\") && PathPrefix(\"%s\")" },
					{"op": "replace", "path": "/spec/routes/0/services/0/namespace", "value": "%s" },
					{"op": "replace", "path": "/spec/routes/0/middlewares/0/namespace", "value": "%s" },
					{"op": "replace", "path": "/spec/routes/0/middlewares/1/namespace", "value": "%s" },
					{"op": "replace", "path": "/spec/routes/0/middlewares/2/namespace", "value": "%s" },
					{"op": "replace", "path": "/spec/tls/domains/0/main", "value": "%s" }
				]`,
				cr.Spec.GetHost(), ingressPathMetrics,
				cr.Namespace(),
				cr.Namespace(),
				cr.Namespace(),
				cr.Namespace(),
				cr.Spec.GetHost(),
			),
			Target: &types.Selector{
				ResId: resid.ResId{
					Gvk: resid.Gvk{
						Kind: "IngressRoute",
					},
					Name: constants.QueryAPIName,
				},
			},
		},
		// Middleware
		types.Patch{
			Patch: fmt.Sprintf(
				`[{"op": "replace", "path": "/spec/headers/accessControlAllowOriginList", "value": ["%s"]}]`,
				cr.Spec.GitLab.TrimInstanceURL(),
			),
			Target: &types.Selector{
				ResId: resid.ResId{
					Gvk: resid.Gvk{
						Kind: "Middleware",
					},
					Name: "cors-headers-queryapi",
				},
			},
		},
		// PodMonitor
		types.Patch{
			Target: &types.Selector{
				ResId: resid.ResId{
					Gvk: resid.Gvk{
						Kind: "PodMonitor",
					},
					Name: constants.QueryAPIName,
				},
			},
			Patch: fmt.Sprintf(
				`[{"op": "replace", "path": "/spec/namespaceSelector", "value": { "matchNames": ["%s"] }}]`,
				cr.Namespace(),
			),
		},
	)

	return res, nil
}

func (r *QueryAPIReconciler) applyDeploymentConfiguration(k *kustomize.Kustomization) error {
	clickHouseEndpoints, err := r.clusterState.ClickHouse.GetSelfhostedEndpoints()
	if err != nil {
		return fmt.Errorf("retrieving clickhouse endpoints: %w", err)
	}
	clickHouseDSN := clickHouseEndpoints.Native.String()

	patches := []string{
		fmt.Sprintf(
			`{"op": "add", "path": "/spec/template/spec/containers/0/args/-", "value": "-clickhouse-dsn=%s" }`,
			clickHouseDSN,
		),
	}
	if r.clusterState.LogLevel != "" {
		patches = append(patches,
			fmt.Sprintf(
				`{"op": "add", "path": "/spec/template/spec/containers/0/args/-", "value": "-log-level=%s" }`,
				r.clusterState.LogLevel,
			),
		)
	}

	clickhouseCloudEndpoint, err := r.clusterState.ClickHouse.GetCloudEndpoints()
	if err != nil {
		return fmt.Errorf("failed to retrieve clickhouse cloud endpoints %w", err)
	}
	if clickhouseCloudEndpoint != nil {
		// Add database for metrics to the DSN
		url := clickhouseCloudEndpoint.Native.JoinPath(constants.MetricsDatabaseName)
		patches = append(patches, fmt.Sprintf(
			`{"op": "add", "path": "/spec/template/spec/containers/0/args/-", "value": "-clickhouse-cloud-dsn=%s" }`,
			url,
		))
	} else {
		patches = append(patches, fmt.Sprintf(
			`{"op": "add", "path": "/spec/template/spec/containers/0/args/-", "value": "-clickhouse-cloud-dsn=%s" }`,
			"",
		))
	}

	k.Patches = append(
		k.Patches,
		types.Patch{
			Patch: fmt.Sprintf(
				`[ %s ]`, strings.Join(patches, ","),
			),
			Target: &types.Selector{
				ResId: resid.ResId{
					Gvk: resid.Gvk{
						Kind: "Deployment",
					},
					Name: constants.QueryAPIName,
				},
			},
		},
	)

	return nil
}
