package cluster

import (
	"errors"
	"fmt"

	"github.com/go-logr/logr"
	"gitlab.com/gitlab-org/opstrace/goose/v3"
	"k8s.io/utils/ptr"

	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/common"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/constants"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/db/migrations/logging"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/db/migrations/metrics"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/db/migrations/tracing"
	"gitlab.com/gitlab-org/opstrace/opstrace/scheduler/api/v1alpha1"
	"gitlab.com/gitlab-org/opstrace/opstrace/scheduler/controllers/cluster/clickhouse"
)

type ClickHouseReconciler struct {
	Teardown bool
	Log      logr.Logger
}

func NewClickHouseReconciler(teardown bool, logger logr.Logger) *ClickHouseReconciler {
	return &ClickHouseReconciler{
		Teardown: teardown,
		Log:      logger.WithName("clickhouse"),
	}
}

func (i *ClickHouseReconciler) Reconcile(state *ClusterState, cr *v1alpha1.Cluster) common.DesiredState {
	desired := common.DesiredState{}

	// when tearing down our desired state, it is important we follow a sequence of
	// actions which allows all components to be deleted correctly without creating
	// orphans OR causing a component to be gone before anything that still references
	// and/or needs it. Though not mandatory, a good sequence to follow is the reverse
	// order of how each component got provisioned when building the desired state
	// initially.
	if i.Teardown {
		desired = desired.AddAction(i.getServiceMonitorDesiredState(cr))
		desired = desired.AddActions(i.getClickHouseDesiredState(state, cr))
		desired = desired.AddAction(i.getCredentialsDesiredState(cr))
		desired = desired.AddActions(i.getReadiness(state))
	} else {
		desired = desired.AddAction(i.getCredentialsDesiredState(cr))
		desired = desired.AddActions(i.getClickHouseDesiredState(state, cr))
		desired = desired.AddAction(i.getServiceMonitorDesiredState(cr))

		desired = desired.AddActions(i.getReadiness(state))
		desired = desired.AddActions(i.setupSelfhostedDatabases(state, cr))
		desired = desired.AddActions(i.setupCloudDatabases(state, cr))
	}

	return desired
}

func (i *ClickHouseReconciler) getReadiness(state *ClusterState) []common.Action {
	if i.Teardown {
		if state.ClickHouse.Cluster != nil {
			return []common.Action{
				common.CheckGoneAction{
					Ref: state.ClickHouse.Cluster,
					Msg: "check clickhouse cluster is gone",
				},
			}
		} else {
			return []common.Action{} // nothing to do
		}
	}

	if !state.ClickHouse.isClusterReady() {
		return []common.Action{
			common.LogAction{
				Msg:   "clickhouse cluster not ready",
				Error: errors.New("clickhouse cluster not ready"),
			},
		}
	}

	return []common.Action{
		common.LogAction{
			Msg: "clickhouse cluster is ready",
		},
	}
}

func (i *ClickHouseReconciler) setupSelfhostedDatabases(state *ClusterState, cr *v1alpha1.Cluster) common.DesiredState {
	var actions []common.Action //nolint:prealloc

	// check if needed migrations have already been applied
	if cr.Status.LastMigrationApplied != nil {
		if *cr.Status.LastMigrationApplied == constants.DockerImageTag {
			return []common.Action{
				common.LogAction{
					Msg: "Self-hosted migrations were already applied",
				},
			}
		}
	}

	useRemoteStorageTracing, err := common.ParseFeatureAsBool(cr.Spec.Features, "TRACING_USE_REMOTE_STORAGE_BACKEND")
	if err != nil {
		return []common.Action{
			common.LogAction{
				Msg:   "failed to parse value for feature <TRACING_USE_REMOTE_STORAGE_BACKEND> as a boolean",
				Error: err,
			},
		}
	}

	endpoints, err := state.ClickHouse.GetSelfhostedEndpoints()
	if err != nil {
		return []common.Action{
			common.LogAction{
				Msg:   "failed to obtain clickhouse scheduler endpoints, this should be a transient error that resolves on next reconcile",
				Error: err,
			},
		}
	}

	dbNames := []string{
		constants.JaegerDatabaseName,
		constants.ErrorTrackingAPIDatabaseName,
		constants.MetricsDatabaseName,
		constants.LoggingDatabaseName,
	}
	if useRemoteStorageTracing {
		switch cr.Spec.Target {
		case common.GCP:
			dbNames = append(dbNames, constants.JaegerGCSDatabaseName)
		case common.AWS:
			dbNames = append(dbNames, constants.JaegerS3DatabaseName)
		}
	}

	for _, dbName := range dbNames {
		actions = append(actions, common.ClickHouseAction{
			Msg: fmt.Sprintf("create clickhouse database if not exists: %s", dbName),
			SQL: fmt.Sprintf("CREATE DATABASE IF NOT EXISTS %s ON CLUSTER '{cluster}'", dbName),
			URL: endpoints.Native,
		})
	}

	actions = append(
		actions,
		processMigrations(
			setupTracingMigrations, true,
			"clickhouse migrations for self-hosted tracing",
			endpoints.Native.JoinPath(constants.TracingDatabaseName).String(),
			cr,
		),
		processMigrations(
			setupMetricsMigrations, true,
			"clickhouse migrations for self-hosted metrics",
			endpoints.Native.JoinPath(constants.MetricsDatabaseName).String(),
			cr,
		),
		processMigrations(
			setupLoggingMigrations, true,
			"clickhouse migrations for self-hosted logs",
			endpoints.Native.JoinPath(constants.LoggingDatabaseName).String(),
			cr,
		),
	)

	return actions
}

func (i *ClickHouseReconciler) setupCloudDatabases(state *ClusterState, cr *v1alpha1.Cluster) common.DesiredState {
	var actions []common.Action //nolint:prealloc

	// check if needed migrations have already been applied
	if cr.Status.LastCloudMigrationApplied != nil {
		if *cr.Status.LastCloudMigrationApplied == constants.DockerImageTag {
			return []common.Action{
				common.LogAction{
					Msg: "Cloud migrations were already applied",
				},
			}
		}
	}

	endpoint, err := state.ClickHouse.GetCloudEndpoints()
	if err != nil {
		actions = append(actions, common.LogAction{
			Msg:   "failed to obtain clickhouse cloud endpoint",
			Error: err,
		})
		return actions
	}

	if endpoint == nil {
		return []common.Action{
			common.LogAction{
				Msg: "ClickHouse Cloud DSN not provided",
			},
		}
	}

	for _, dbName := range []string{
		constants.ErrorTrackingAPIDatabaseName,
		constants.TracingDatabaseName,
		constants.MetricsDatabaseName,
		constants.LoggingDatabaseName,
	} {
		actions = append(actions, common.ClickHouseAction{
			Msg: fmt.Sprintf("create clickhouse database on CH Cloud if not exists: %s", dbName),
			SQL: fmt.Sprintf("CREATE DATABASE IF NOT EXISTS %s", dbName),
			URL: endpoint.Native,
		})
	}

	actions = append(
		actions,
		processMigrations(
			setupTracingMigrations, false,
			"clickhouse cloud migrations for tracing",
			endpoint.Native.JoinPath(constants.TracingDatabaseName).String(),
			cr,
		),
		processMigrations(
			setupMetricsMigrations, false,
			"clickhouse cloud migrations for metrics",
			endpoint.Native.JoinPath(constants.MetricsDatabaseName).String(),
			cr,
		),
		processMigrations(
			setupLoggingMigrations, false,
			"clickhouse cloud migrations for logs",
			endpoint.Native.JoinPath(constants.LoggingDatabaseName).String(),
			cr,
		),
	)

	return actions
}

func processMigrations(
	migrationsFunc func(bool) func() ([]*goose.Migration, error),
	selfHostedVersion bool,
	name string,
	dsn string,
	cr *v1alpha1.Cluster,
) common.Action {
	collectedMigrations, err := migrationsFunc(selfHostedVersion)()
	if err != nil {
		panic(fmt.Sprintf("migration setup failed for %s: %s", name, err.Error()))
	}

	if len(collectedMigrations) == 0 {
		return common.LogAction{
			Msg: fmt.Sprintf("%q has no migrations to run", name),
		}
	}

	res := common.ClickHouseCloudMigrationAction{
		Msg:               "create %s",
		ClickHouseDSN:     dsn,
		GoMigrationsToRun: collectedMigrations,
	}

	if selfHostedVersion {
		res.SetLastMigrationApplied = func(version string) {
			cr.Status.LastMigrationApplied = ptr.To(version)
		}
	} else {
		res.SetLastMigrationApplied = func(version string) {
			cr.Status.LastCloudMigrationApplied = ptr.To(version)
		}
	}

	return res
}

func setupTracingMigrations(selfHostedVersion bool) func() ([]*goose.Migration, error) {
	if selfHostedVersion {
		return setupTracingMigrationsForSelfhosted
	} else {
		return setupTracingMigrationsForCloud
	}
}

func setupLoggingMigrations(selfHostedVersion bool) func() ([]*goose.Migration, error) {
	return func() ([]*goose.Migration, error) {
		var migrations []*goose.Migration

		m, err := (&logging.LogsMain{}).Setup(
			logging.MigrationData{
				DatabaseName:      constants.LoggingDatabaseName,
				TableName:         constants.LoggingTableName,
				SelfHostedVersion: selfHostedVersion,
			},
		)
		if err != nil {
			return nil, err
		}
		if m != nil {
			migrations = append(migrations, m)
		}
		return migrations, nil
	}
}

func setupTracingMigrationsForSelfhosted() ([]*goose.Migration, error) {
	var migrations []*goose.Migration

	m, err := (&tracing.TracesMain{}).Setup(
		tracing.MigrationData{
			DatabaseName:      constants.TracingDatabaseName,
			TableName:         constants.TracingTableName,
			SelfHostedVersion: true,
		},
	)
	if err != nil {
		return nil, err
	}
	if m != nil {
		migrations = append(migrations, m)
	}

	m, err = (&tracing.TracesMainDist{}).Setup(
		tracing.MigrationData{
			DatabaseName:  constants.TracingDatabaseName,
			TableName:     constants.TracingTableName,
			DistTableName: constants.TracingDistTableName,
		},
	)
	if err != nil {
		return nil, err
	}
	if m != nil {
		migrations = append(migrations, m)
	}

	// Note: Order of migrations is important
	m, err = (&tracing.TracesMVTarget{}).Setup(
		tracing.MigrationData{
			DatabaseName:      constants.TracingDatabaseName,
			TableName:         constants.TracingMVTargetTableName,
			SelfHostedVersion: true,
		})
	if err != nil {
		return nil, err
	}
	if m != nil {
		migrations = append(migrations, m)
	}

	m, err = (&tracing.TracesMVTargetDist{}).Setup(
		tracing.MigrationData{
			DatabaseName:  constants.TracingDatabaseName,
			TableName:     constants.TracingMVTargetTableName,
			DistTableName: constants.TracingMVTargetDistTableName,
		})
	if err != nil {
		return nil, err
	}
	if m != nil {
		migrations = append(migrations, m)
	}

	m, err = (&tracing.TracesMV{}).Setup(
		tracing.MigrationData{
			DatabaseName:      constants.TracingDatabaseName,
			TableName:         constants.TracingMVName,
			TargetTableName:   constants.TracingMVTargetTableName,
			SourceTableName:   constants.TracingTableName,
			SelfHostedVersion: true,
		})
	if err != nil {
		return nil, err
	}
	if m != nil {
		migrations = append(migrations, m)
	}

	m, err = (&tracing.AddNamespaceIDTracesMain{}).Setup(
		tracing.MigrationData{
			DatabaseName: constants.TracingDatabaseName,
			TableName:    constants.TracingTableName,
		})
	if err != nil {
		return nil, err
	}
	if m != nil {
		migrations = append(migrations, m)
	}

	m, err = (&tracing.AddNamespaceIDTraceDist{}).Setup(
		tracing.MigrationData{
			DatabaseName:  constants.TracingDatabaseName,
			DistTableName: constants.TracingDistTableName,
		})
	if err != nil {
		return nil, err
	}
	if m != nil {
		migrations = append(migrations, m)
	}
	return migrations, nil
}

func setupTracingMigrationsForCloud() ([]*goose.Migration, error) {
	var migrations []*goose.Migration

	m, err := (&tracing.TracesMain{}).Setup(
		tracing.MigrationData{
			DatabaseName:      constants.TracingDatabaseName,
			TableName:         constants.TracingDistTableName,
			SelfHostedVersion: false,
		},
	)
	if err != nil {
		return nil, err
	}
	if m != nil {
		migrations = append(migrations, m)
	}
	// Note: Order of migrations is important
	m, err = (&tracing.TracesMVTarget{}).Setup(
		tracing.MigrationData{
			DatabaseName:      constants.TracingDatabaseName,
			TableName:         constants.TracingMVTargetDistTableName,
			SelfHostedVersion: false,
		})
	if err != nil {
		return nil, err
	}
	if m != nil {
		migrations = append(migrations, m)
	}

	m, err = (&tracing.TracesMV{}).Setup(
		tracing.MigrationData{
			DatabaseName:      constants.TracingDatabaseName,
			TableName:         constants.TracingMVName,
			TargetTableName:   constants.TracingMVTargetDistTableName,
			SourceTableName:   constants.TracingDistTableName,
			SelfHostedVersion: false,
		})
	if err != nil {
		return nil, err
	}
	if m != nil {
		migrations = append(migrations, m)
	}

	m, err = (&tracing.AddNamespaceIDTracesMain{}).Setup(
		tracing.MigrationData{
			DatabaseName:      constants.TracingDatabaseName,
			TableName:         constants.TracingDistTableName,
			SelfHostedVersion: false,
		})
	if err != nil {
		return nil, err
	}
	if m != nil {
		migrations = append(migrations, m)
	}
	return migrations, nil
}

func (i *ClickHouseReconciler) getCredentialsDesiredState(cr *v1alpha1.Cluster) common.Action {
	s := clickhouse.Credentials(cr)

	if i.Teardown {
		return common.GenericDeleteAction{
			Ref: s,
			Msg: "clickhouse credentials",
		}
	}

	return common.GenericCreateOrUpdateAction{
		Ref: s,
		Msg: "clickhouse credentials",
		Mutator: func() error {
			return clickhouse.CredentialsMutator(cr, s)
		},
	}
}

func (i *ClickHouseReconciler) getClickHouseDesiredState(state *ClusterState, cr *v1alpha1.Cluster) []common.Action {
	if state.ClickHouse.Credentials == nil {
		if i.Teardown {
			// if the secret does not exist now AND we're tearing down the CR, we're done here
			// because the CR should have been deleted in a previous reconcile loop. Just make
			// sure that has happened successfully.
			if state.ClickHouse.Cluster != nil {
				return []common.Action{
					common.CheckGoneAction{
						Ref: state.ClickHouse.Cluster,
						Msg: "check clickhouse CR is gone",
					},
				}
			} else {
				return []common.Action{} // nothing to do
			}
		}
		// if the secret does not exist yet BUT we're in a provisioning loop, report transient error
		// and come back again during the next reconcile.
		return []common.Action{
			common.LogAction{
				Msg:   "clickhouse CR",
				Error: fmt.Errorf("haven't read clickhouse scheduler user credentials yet, this should be a transient error that resolves on next reconcile"),
			},
		}
	}

	user, err := state.ClickHouse.GetSchedulerCredentials()
	if err != nil {
		return []common.Action{
			common.LogAction{
				Msg:   "clickhouse CR",
				Error: err,
			},
		}
	}
	current := clickhouse.ClickHouse(cr, user)

	if i.Teardown {
		actions := []common.Action{}
		actions = append(actions, common.GenericDeleteAction{
			Ref: current,
			Msg: "clickhouse CR",
		})
		// make sure the CR is correctly deleted after the operator is done
		// cleaning up all associated resources
		if state.ClickHouse.Cluster != nil {
			actions = append(actions, common.CheckGoneAction{
				Ref: state.ClickHouse.Cluster,
				Msg: "check clickhouse CR is gone",
			})
		}
		return actions
	}

	return []common.Action{
		common.GenericCreateOrUpdateAction{
			Ref: current,
			Msg: "Clickhouse CR",
			Mutator: func() error {
				return clickhouse.ClickHouseMutator(cr, current, user)
			},
		},
	}
}

func (i *ClickHouseReconciler) getServiceMonitorDesiredState(cr *v1alpha1.Cluster) common.Action {
	monitor := clickhouse.ServiceMonitor(cr)

	if i.Teardown {
		return common.GenericDeleteAction{
			Ref: monitor,
			Msg: "clickhouse cluster servicemonitor",
		}
	}

	return common.GenericCreateOrUpdateAction{
		Ref: monitor,
		Msg: "clickhouse cluster servicemonitor",
		Mutator: func() error {
			return clickhouse.ServiceMonitorMutator(cr, monitor)
		},
	}
}

//nolint:funlen,cyclop,gocyclo
func setupMetricsMigrations(selfHostedVersion bool) func() ([]*goose.Migration, error) {
	return func() ([]*goose.Migration, error) {
		var migrations []*goose.Migration

		m, err := (&metrics.GaugeMain{}).Setup(
			metrics.MigrationData{
				DatabaseName:      constants.MetricsDatabaseName,
				TableName:         constants.MetricsGaugeTableName,
				SelfHostedVersion: selfHostedVersion,
			},
		)
		if err != nil {
			return nil, err
		}
		migrations = append(migrations, m)

		m, err = (&metrics.SumMain{}).Setup(
			metrics.MigrationData{
				DatabaseName:      constants.MetricsDatabaseName,
				TableName:         constants.MetricsSumTableName,
				SelfHostedVersion: selfHostedVersion,
			})
		if err != nil {
			return nil, err
		}
		migrations = append(migrations, m)

		m, err = (&metrics.HistMain{}).Setup(
			metrics.MigrationData{
				DatabaseName:      constants.MetricsDatabaseName,
				TableName:         constants.MetricsHistogramTableName,
				SelfHostedVersion: selfHostedVersion,
			})
		if err != nil {
			return nil, err
		}
		migrations = append(migrations, m)

		m, err = (&metrics.ExpHistMain{}).Setup(
			metrics.MigrationData{
				DatabaseName:      constants.MetricsDatabaseName,
				TableName:         constants.MetricsExponentialHistogramTableName,
				SelfHostedVersion: selfHostedVersion,
			})
		if err != nil {
			return nil, err
		}
		migrations = append(migrations, m)

		m, err = (&metrics.AddAggregationTemporalityHistogramMain{}).Setup(
			metrics.MigrationData{
				DatabaseName:      constants.MetricsDatabaseName,
				TableName:         constants.MetricsHistogramTableName,
				SelfHostedVersion: selfHostedVersion,
			},
		)
		if err != nil {
			return nil, err
		}
		migrations = append(migrations, m)

		m, err = (&metrics.AddAggregationTemporalityExpHistogramMain{}).Setup(
			metrics.MigrationData{
				DatabaseName:      constants.MetricsDatabaseName,
				TableName:         constants.MetricsExponentialHistogramTableName,
				SelfHostedVersion: selfHostedVersion,
			},
		)
		if err != nil {
			return nil, err
		}
		migrations = append(migrations, m)

		// metadata MVs
		m, err = (&metrics.SumMetricsMetadataMVTarget{}).Setup(
			metrics.MVMigrationData{
				DatabaseName:      constants.MetricsDatabaseName,
				TableName:         constants.MetricsSumMetadataTableName,
				SelfHostedVersion: selfHostedVersion,
			},
		)
		if err != nil {
			return nil, err
		}
		migrations = append(migrations, m)

		m, err = (&metrics.SumMetricsMetadataMV{}).Setup(
			metrics.MVMigrationData{
				DatabaseName:      constants.MetricsDatabaseName,
				TableName:         constants.MetricsSumMetadataMVName,
				SourceTableName:   constants.MetricsSumTableName,
				TargetTableName:   constants.MetricsSumMetadataTableName,
				SelfHostedVersion: selfHostedVersion,
			},
		)
		if err != nil {
			return nil, err
		}
		migrations = append(migrations, m)

		m, err = (&metrics.GaugeMetricsMetadataMVTarget{}).Setup(
			metrics.MVMigrationData{
				DatabaseName:      constants.MetricsDatabaseName,
				TableName:         constants.MetricsGaugeMetadataTableName,
				SelfHostedVersion: selfHostedVersion,
			},
		)
		if err != nil {
			return nil, err
		}
		migrations = append(migrations, m)

		m, err = (&metrics.GaugeMetricsMetadataMV{}).Setup(
			metrics.MVMigrationData{
				DatabaseName:      constants.MetricsDatabaseName,
				TableName:         constants.MetricsGaugeMetadataMVName,
				SourceTableName:   constants.MetricsGaugeTableName,
				TargetTableName:   constants.MetricsGaugeMetadataTableName,
				SelfHostedVersion: selfHostedVersion,
			},
		)
		if err != nil {
			return nil, err
		}
		migrations = append(migrations, m)

		m, err = (&metrics.HistogramMetricsMetadataMVTarget{}).Setup(
			metrics.MVMigrationData{
				DatabaseName:      constants.MetricsDatabaseName,
				TableName:         constants.MetricsHistogramMetadataTableName,
				SelfHostedVersion: selfHostedVersion,
			},
		)
		if err != nil {
			return nil, err
		}
		migrations = append(migrations, m)

		m, err = (&metrics.HistogramMetricsMetadataMV{}).Setup(
			metrics.MVMigrationData{
				DatabaseName:      constants.MetricsDatabaseName,
				TableName:         constants.MetricsHistogramMetadataMVName,
				SourceTableName:   constants.MetricsHistogramTableName,
				TargetTableName:   constants.MetricsHistogramMetadataTableName,
				SelfHostedVersion: selfHostedVersion,
			},
		)
		if err != nil {
			return nil, err
		}
		migrations = append(migrations, m)

		m, err = (&metrics.ExpHistogramMetricsMetadataMVTarget{}).Setup(
			metrics.MVMigrationData{
				DatabaseName:      constants.MetricsDatabaseName,
				TableName:         constants.MetricsExponentialHistogramMetadataTableName,
				SelfHostedVersion: selfHostedVersion,
			},
		)
		if err != nil {
			return nil, err
		}
		migrations = append(migrations, m)

		m, err = (&metrics.ExpHistogramMetricsMetadataMV{}).Setup(
			metrics.MVMigrationData{
				DatabaseName:      constants.MetricsDatabaseName,
				TableName:         constants.MetricsExponentialHistogramMetadataMVName,
				SourceTableName:   constants.MetricsExponentialHistogramTableName,
				TargetTableName:   constants.MetricsExponentialHistogramMetadataTableName,
				SelfHostedVersion: selfHostedVersion,
			},
		)
		if err != nil {
			return nil, err
		}
		migrations = append(migrations, m)
		// metadata MVs end

		m, err = (&metrics.GaugeRollup1mMVTarget{}).Setup(
			metrics.MVMigrationData{
				DatabaseName:      constants.MetricsDatabaseName,
				TableName:         constants.MetricsGauge1mTableName,
				SelfHostedVersion: selfHostedVersion,
			},
		)
		if err != nil {
			return nil, err
		}
		migrations = append(migrations, m)

		m, err = (&metrics.GaugeRollup1mMV{}).Setup(
			metrics.MVMigrationData{
				DatabaseName:      constants.MetricsDatabaseName,
				TableName:         constants.MetricsGauge1mMVName,
				SourceTableName:   constants.MetricsGaugeTableName,
				TargetTableName:   constants.MetricsGauge1mTableName,
				SelfHostedVersion: selfHostedVersion,
			},
		)
		if err != nil {
			return nil, err
		}
		migrations = append(migrations, m)

		m, err = (&metrics.GaugeRollup1hMVTarget{}).Setup(
			metrics.MVMigrationData{
				DatabaseName:      constants.MetricsDatabaseName,
				TableName:         constants.MetricsGauge1hTableName,
				SelfHostedVersion: selfHostedVersion,
			},
		)
		if err != nil {
			return nil, err
		}
		migrations = append(migrations, m)

		m, err = (&metrics.GaugeRollup1hMV{}).Setup(
			metrics.MVMigrationData{
				DatabaseName:      constants.MetricsDatabaseName,
				TableName:         constants.MetricsGauge1hMVName,
				SourceTableName:   constants.MetricsGaugeTableName,
				TargetTableName:   constants.MetricsGauge1hTableName,
				SelfHostedVersion: selfHostedVersion,
			},
		)
		if err != nil {
			return nil, err
		}
		migrations = append(migrations, m)

		m, err = (&metrics.GaugeRollup1dMVTarget{}).Setup(
			metrics.MVMigrationData{
				DatabaseName:      constants.MetricsDatabaseName,
				TableName:         constants.MetricsGauge1dTableName,
				SelfHostedVersion: selfHostedVersion,
			},
		)
		if err != nil {
			return nil, err
		}
		migrations = append(migrations, m)

		m, err = (&metrics.GaugeRollup1dMV{}).Setup(
			metrics.MVMigrationData{
				DatabaseName:      constants.MetricsDatabaseName,
				TableName:         constants.MetricsGauge1dMVName,
				SourceTableName:   constants.MetricsGaugeTableName,
				TargetTableName:   constants.MetricsGauge1dTableName,
				SelfHostedVersion: selfHostedVersion,
			},
		)
		if err != nil {
			return nil, err
		}
		migrations = append(migrations, m)

		m, err = (&metrics.SumRollup1mMVTarget{}).Setup(
			metrics.MVMigrationData{
				DatabaseName:      constants.MetricsDatabaseName,
				TableName:         constants.MetricsSum1mTableName,
				SelfHostedVersion: selfHostedVersion,
			},
		)
		if err != nil {
			return nil, err
		}
		migrations = append(migrations, m)

		m, err = (&metrics.SumRollup1mMV{}).Setup(
			metrics.MVMigrationData{
				DatabaseName:      constants.MetricsDatabaseName,
				TableName:         constants.MetricsSum1mMVName,
				SourceTableName:   constants.MetricsSumTableName,
				TargetTableName:   constants.MetricsSum1mTableName,
				SelfHostedVersion: selfHostedVersion,
			},
		)
		if err != nil {
			return nil, err
		}
		migrations = append(migrations, m)

		m, err = (&metrics.SumRollup1hMVTarget{}).Setup(
			metrics.MVMigrationData{
				DatabaseName:      constants.MetricsDatabaseName,
				TableName:         constants.MetricsSum1hTableName,
				SelfHostedVersion: selfHostedVersion,
			},
		)
		if err != nil {
			return nil, err
		}
		migrations = append(migrations, m)

		m, err = (&metrics.SumRollup1hMV{}).Setup(
			metrics.MVMigrationData{
				DatabaseName:      constants.MetricsDatabaseName,
				TableName:         constants.MetricsSum1hMVName,
				SourceTableName:   constants.MetricsSumTableName,
				TargetTableName:   constants.MetricsSum1hTableName,
				SelfHostedVersion: selfHostedVersion,
			},
		)
		if err != nil {
			return nil, err
		}
		migrations = append(migrations, m)

		m, err = (&metrics.SumRollup1dMVTarget{}).Setup(
			metrics.MVMigrationData{
				DatabaseName:      constants.MetricsDatabaseName,
				TableName:         constants.MetricsSum1dTableName,
				SelfHostedVersion: selfHostedVersion,
			},
		)
		if err != nil {
			return nil, err
		}
		migrations = append(migrations, m)

		m, err = (&metrics.SumRollup1dMV{}).Setup(
			metrics.MVMigrationData{
				DatabaseName:      constants.MetricsDatabaseName,
				TableName:         constants.MetricsSum1dMVName,
				SourceTableName:   constants.MetricsSumTableName,
				TargetTableName:   constants.MetricsSum1dTableName,
				SelfHostedVersion: selfHostedVersion,
			},
		)
		if err != nil {
			return nil, err
		}
		migrations = append(migrations, m)

		return migrations, nil
	}
}
