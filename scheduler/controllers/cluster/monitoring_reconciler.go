package cluster

import (
	"fmt"

	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/constants"
	"gitlab.com/gitlab-org/opstrace/opstrace/scheduler/api/v1alpha1"
	logf "sigs.k8s.io/controller-runtime/pkg/log"
	"sigs.k8s.io/kustomize/api/types"
	kustomize "sigs.k8s.io/kustomize/api/types"
	"sigs.k8s.io/kustomize/kyaml/resid"
)

type MonitoringReconciler struct {
	BaseReconciler

	chState *ClickHouseState
}

func NewMonitoringReconciler(
	initialManifests map[string][]byte,
	teardown bool,
	chState *ClickHouseState,
) *MonitoringReconciler {
	res := &MonitoringReconciler{
		BaseReconciler: BaseReconciler{
			Teardown:         teardown,
			Log:              logf.Log.WithName(fmt.Sprintf("manifests/%s", constants.MonitoringInventoryID)),
			initialManifests: initialManifests,

			inventoryID:    constants.MonitoringInventoryID,
			reconcilerName: "monitoring",
			getOverridesF: func(cr *v1alpha1.Cluster) *v1alpha1.KustomizeOverridesSpec {
				return cr.Spec.Overrides.Monitoring
			},
		},

		chState: chState,
	}

	res.subclassApplyMethod = res.applyConfiguration

	return res
}

func (i *MonitoringReconciler) applyConfiguration(cr *v1alpha1.Cluster) (*kustomize.Kustomization, error) {
	res, err := i.BaseReconciler.applyBaseConfiguration(cr)
	if err != nil {
		return nil, err
	}

	// clickhouse-exporter needs to talk to the native HTTP endpoint for the
	// clickhouse cluster being monitored. Lets first check if the necessary
	// credentials are in place or bail early
	if i.chState == nil {
		return nil, fmt.Errorf("clickhouse state cannot be nil")
	}

	clickhouseEndpoints, err := i.chState.GetSelfhostedEndpoints()
	if err != nil {
		return nil, err
	}

	user := clickhouseEndpoints.Http.User.Username()
	pwd, _ := clickhouseEndpoints.Http.User.Password()
	url := fmt.Sprintf("%s://%s", clickhouseEndpoints.Http.Scheme, clickhouseEndpoints.Http.Host)

	// NOTE(prozlach): Overriding patches instead of appending is intentional
	// here - the monitoring configuration does not need image pull secrets
	// applied. We want to keep the rest of the kustomization that base struct
	// genearted though, hence we just override the patches.
	res.Patches = []types.Patch{
		{
			Patch: fmt.Sprintf(
				`[
						{"op": "replace", "path": "/spec/template/spec/containers/0/env/0/value", "value": "%s"},
						{"op": "replace", "path": "/spec/template/spec/containers/0/env/1/value", "value": "%s"},
						{"op": "replace", "path": "/spec/template/spec/containers/0/args/0", "value": "-scrape_uri=%s"}
					]`,
				user, pwd, url,
			),
			Target: &types.Selector{
				ResId: resid.ResId{
					Gvk: resid.Gvk{
						Kind: "Deployment",
					},
					Name: "clickhouse-exporter",
				},
			},
		},
	}
	return res, nil
}
