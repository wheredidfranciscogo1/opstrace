package cluster

import (
	"encoding/json"
	"fmt"

	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/common"
	utils "gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/common"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/constants"
	"gitlab.com/gitlab-org/opstrace/opstrace/scheduler/api/v1alpha1"
	v1 "k8s.io/api/core/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	logf "sigs.k8s.io/controller-runtime/pkg/log"
	"sigs.k8s.io/kustomize/api/types"
	kustomize "sigs.k8s.io/kustomize/api/types"
	"sigs.k8s.io/kustomize/kyaml/resid"
)

type RedisOperatorReconciler struct {
	BaseReconciler
}

func NewRedisOperatorReconciler(
	initialManifests map[string][]byte,
	teardown bool,
) *RedisOperatorReconciler {
	res := &RedisOperatorReconciler{
		BaseReconciler: BaseReconciler{
			Teardown:         teardown,
			Log:              logf.Log.WithName(fmt.Sprintf("manifests/%s", constants.RedisOperatorInventoryID)),
			initialManifests: initialManifests,

			inventoryID:    constants.RedisOperatorInventoryID,
			reconcilerName: "redis-operator",
			getOverridesF: func(cr *v1alpha1.Cluster) *v1alpha1.KustomizeOverridesSpec {
				return cr.Spec.Overrides.RedisOperator
			},

			serviceAccountName: constants.RedisOperatorServiceAccountName,
		},
	}

	res.subclassApplyMethod = res.applyConfiguration

	return res
}

func (i *RedisOperatorReconciler) Reconcile(cr *v1alpha1.Cluster) common.DesiredState {
	tmp := i.BaseReconciler.Reconcile(cr)

	if i.Teardown {
		tmp = append(tmp, i.getCredentialsDesiredState(cr))
		return tmp
	}

	if _, ok := tmp[0].(common.LogAction); ok {
		// There was a problem, return it without appending credentials action:
		return tmp
	}

	// Normal reconcile run, append the credentials action:
	res := common.DesiredState{i.getCredentialsDesiredState(cr)}
	res = append(res, tmp...)
	return res
}

func (i *RedisOperatorReconciler) getCredentialsDesiredState(cr *v1alpha1.Cluster) common.Action {
	secret := &v1.Secret{}
	secret.ObjectMeta = metav1.ObjectMeta{
		Name:      constants.RedisName,
		Namespace: cr.Namespace(),
	}
	secret.Data = map[string][]byte{
		"password": []byte(utils.RandStringRunes(10)),
	}

	if i.Teardown {
		return common.GenericDeleteAction{
			Ref: secret,
			Msg: "redis credentials",
		}
	}

	return common.GenericCreateOrUpdateAction{
		Ref: secret,
		Msg: "redis credentials",
		Mutator: func() error {
			// Don't mutate to ensure we don't change the password after
			// it's first created
			return nil
		},
	}
}

func (i *RedisOperatorReconciler) applyConfiguration(cr *v1alpha1.Cluster) (*kustomize.Kustomization, error) {
	res, err := i.BaseReconciler.applyBaseConfiguration(cr)
	if err != nil {
		return nil, err
	}

	res.Patches = append(
		res.Patches,
		types.Patch{
			Patch: fmt.Sprintf(
				`[{"op": "replace", "path": "/spec/namespaceSelector", "value": { "matchNames": ["%s"] }}]`,
				cr.Namespace(),
			),
			Target: &types.Selector{
				ResId: resid.ResId{
					Gvk: resid.Gvk{
						Kind: "ServiceMonitor",
					},
					Name: constants.RedisOperatorServiceMonitorName,
				},
			},
		},
	)

	if len(cr.Spec.ImagePullSecrets) > 0 {
		b, err := json.Marshal(cr.Spec.ImagePullSecrets)
		if err != nil {
			return nil, fmt.Errorf("unable to marshal image pull secrets: %w", err)
		}
		res.Patches = append(res.Patches,
			types.Patch{
				Patch: fmt.Sprintf(`[{"op": "add", "path": "/imagePullSecrets", "value": %s}]`, string(b)),
				Target: &types.Selector{
					ResId: resid.ResId{
						Gvk: resid.Gvk{
							Kind: "RedisFailover",
						},
						Name: constants.RedisName,
					},
				},
			},
		)
	}

	return res, nil
}
