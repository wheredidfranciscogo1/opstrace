package cluster

import (
	"fmt"
	"strings"

	logf "sigs.k8s.io/controller-runtime/pkg/log"
	"sigs.k8s.io/kustomize/api/types"
	kustomize "sigs.k8s.io/kustomize/api/types"
	"sigs.k8s.io/kustomize/kyaml/resid"

	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/constants"
	"gitlab.com/gitlab-org/opstrace/opstrace/scheduler/api/v1alpha1"
)

const (
	ingressPath = `/query/{groupID:[0-9]+}/{projectID:[0-9]+}`
	// Allow handling both /traces & /services URLs on this service
	// For docs, see: https://doc.traefik.io/traefik/routing/routers/#rule
	ingressPathV3 = `/v3/query/{projectID:[0-9]+}/{endpoint:(traces|services)}`
)

type TraceQueryAPIReconciler struct {
	BaseReconciler
	clusterState *ClusterState
}

func NewTraceQueryAPIReconciler(
	initialManifests map[string][]byte,
	teardown bool,
	clusterState *ClusterState) *TraceQueryAPIReconciler {
	res := &TraceQueryAPIReconciler{
		clusterState: clusterState,

		BaseReconciler: BaseReconciler{
			Teardown:         teardown,
			Log:              logf.Log.WithName(fmt.Sprintf("manifests/%s", constants.TraceQueryAPIInventoryID)),
			initialManifests: initialManifests,

			inventoryID:    constants.TraceQueryAPIInventoryID,
			reconcilerName: "trace-query-api",
			getOverridesF: func(cr *v1alpha1.Cluster) *v1alpha1.KustomizeOverridesSpec {
				return cr.Spec.Overrides.TraceQueryAPI
			},

			serviceAccountName: constants.TraceQueryAPIName,
		},
	}

	res.subclassApplyMethod = res.applyConfiguration
	return res
}

//nolint:funlen
func (r *TraceQueryAPIReconciler) applyConfiguration(cr *v1alpha1.Cluster) (*kustomize.Kustomization, error) {
	res, err := r.BaseReconciler.applyBaseConfiguration(cr)
	if err != nil {
		return nil, err
	}
	res.Images = []kustomize.Image{
		{
			Name:    "trace-query-api-image-placeholder",
			NewName: constants.DockerImageName(constants.TraceQueryAPIImageName),
			NewTag:  constants.DockerImageTag,
		},
	}
	err = r.applyDeploymentConfiguration(res)
	if err != nil {
		return nil, err
	}

	res.Patches = append(
		res.Patches,
		// This will be removed once clients are migrated to V1 ingressPath
		types.Patch{
			Patch: fmt.Sprintf(
				`[
						{"op": "replace", "path": "/spec/routes/0/match", "value": "Host(\"%s\") && PathPrefix(\"%s\")" },
						{"op": "replace", "path": "/spec/routes/0/services/0/namespace", "value": "%s" },
						{"op": "replace", "path": "/spec/routes/0/middlewares/0/namespace", "value": "%s" },
						{"op": "replace", "path": "/spec/routes/0/middlewares/1/namespace", "value": "%s" },
						{"op": "replace", "path": "/spec/routes/0/middlewares/2/namespace", "value": "%s" },
						{"op": "replace", "path": "/spec/tls/domains/0/main", "value": "%s" }
					  ]`,
				cr.Spec.GetHost(), ingressPath,
				cr.Namespace(),
				cr.Namespace(),
				cr.Namespace(),
				cr.Namespace(),
				cr.Spec.GetHost(),
			),
			Target: &types.Selector{
				ResId: resid.ResId{
					Gvk: resid.Gvk{
						Kind: "IngressRoute",
					},
					Name: constants.TraceQueryAPIName,
				},
			},
		},
		types.Patch{
			Patch: fmt.Sprintf(
				`[
						{"op": "replace", "path": "/spec/routes/0/match", "value": "Host(\"%s\") && PathPrefix(\"%s\")" },
						{"op": "replace", "path": "/spec/routes/0/services/0/namespace", "value": "%s" },
						{"op": "replace", "path": "/spec/routes/0/middlewares/0/namespace", "value": "%s" },
						{"op": "replace", "path": "/spec/routes/0/middlewares/1/namespace", "value": "%s" },
						{"op": "replace", "path": "/spec/routes/0/middlewares/2/namespace", "value": "%s" },
						{"op": "replace", "path": "/spec/tls/domains/0/main", "value": "%s" }
					  ]`,
				cr.Spec.GetHost(), ingressPathV3,
				cr.Namespace(),
				cr.Namespace(),
				cr.Namespace(),
				cr.Namespace(),
				cr.Spec.GetHost(),
			),
			Target: &types.Selector{
				ResId: resid.ResId{
					Gvk: resid.Gvk{
						Kind: "IngressRoute",
					},
					Name: constants.TraceQueryV3APIName,
				},
			},
		},
		types.Patch{
			Patch: fmt.Sprintf(
				`[{"op": "replace", "path": "/spec/headers/accessControlAllowOriginList", "value": ["%s"]}]`,
				cr.Spec.GitLab.TrimInstanceURL(),
			),
			Target: &types.Selector{
				ResId: resid.ResId{
					Gvk: resid.Gvk{
						Kind: "Middleware",
					},
					Name: "cors-headers-tapi",
				},
			},
		},
		// PodMonitor
		types.Patch{
			Target: &types.Selector{
				ResId: resid.ResId{
					Gvk: resid.Gvk{
						Kind: "PodMonitor",
					},
					Name: constants.TraceQueryAPIName,
				},
			},
			Patch: fmt.Sprintf(
				`[
				{"op": "replace", "path": "/spec/namespaceSelector", "value": { "matchNames": ["%s"] }}
			]`,
				cr.Namespace(),
			),
		},
	)

	return res, nil
}

func (r *TraceQueryAPIReconciler) applyDeploymentConfiguration(k *kustomize.Kustomization) error {
	clickHouseEndpoints, err := r.clusterState.ClickHouse.GetSelfhostedEndpoints()
	if err != nil {
		return fmt.Errorf("failed to retrieve clickhouse endpoints: %w", err)
	}
	clickHouseDSN := clickHouseEndpoints.Native.String() + "/" + constants.TraceQueryAPIDBName

	patches := []string{
		fmt.Sprintf(
			`{"op": "replace", "path": "/spec/template/spec/containers/0/env/1/value", "value": "%s" }`,
			clickHouseDSN,
		),
	}

	clickhouseCloudEndpoint, err := r.clusterState.ClickHouse.GetCloudEndpoints()
	if err != nil {
		return fmt.Errorf("failed to retrieve clickhouse cloud endpoints %w", err)
	}
	if clickhouseCloudEndpoint != nil {
		// Add database for tracing to the DSN
		url := clickhouseCloudEndpoint.Native.JoinPath(constants.TracingDatabaseName)
		patches = append(patches, fmt.Sprintf(
			`{"op": "replace", "path": "/spec/template/spec/containers/0/env/2/value", "value": "%s" }`,
			url,
		))
	} else {
		patches = append(patches, fmt.Sprintf(
			`{"op": "replace", "path": "/spec/template/spec/containers/0/env/2/value", "value": "%s" }`,
			"",
		))
	}

	if r.clusterState.LogLevel != "" {
		patches = append(patches,
			fmt.Sprintf(
				`{"op": "replace", "path": "/spec/template/spec/containers/0/env/0/value", "value": "%s" }`,
				r.clusterState.LogLevel,
			),
		)
	}

	k.Patches = append(
		k.Patches,
		types.Patch{
			Patch: fmt.Sprintf(
				`[ %s ]`, strings.Join(patches, ","),
			),
			Target: &types.Selector{
				ResId: resid.ResId{
					Gvk: resid.Gvk{
						Kind: "Deployment",
					},
					Name: constants.TraceQueryAPIName,
				},
			},
		},
	)

	return nil
}
