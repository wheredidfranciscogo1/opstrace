package cluster

import (
	"fmt"

	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/constants"
	"gitlab.com/gitlab-org/opstrace/opstrace/scheduler/api/v1alpha1"
	logf "sigs.k8s.io/controller-runtime/pkg/log"
	"sigs.k8s.io/kustomize/api/types"
	kustomize "sigs.k8s.io/kustomize/api/types"
	"sigs.k8s.io/kustomize/kyaml/resid"
)

type ReloaderReconciler struct {
	BaseReconciler
}

func NewReloaderReconciler(
	initialManifests map[string][]byte,
	teardown bool,
) *ReloaderReconciler {
	res := &ReloaderReconciler{
		BaseReconciler: BaseReconciler{
			Teardown:         teardown,
			Log:              logf.Log.WithName(fmt.Sprintf("manifests/%s", constants.ReloaderInventoryID)),
			initialManifests: initialManifests,

			inventoryID:    constants.ReloaderInventoryID,
			reconcilerName: "reloader",
			getOverridesF: func(cr *v1alpha1.Cluster) *v1alpha1.KustomizeOverridesSpec {
				return cr.Spec.Overrides.Reloader
			},

			serviceAccountName: constants.ReloaderServiceAccountName,
		},
	}

	res.subclassApplyMethod = res.applyConfiguration

	return res
}

func (i *ReloaderReconciler) applyConfiguration(cr *v1alpha1.Cluster) (*kustomize.Kustomization, error) {
	res, err := i.BaseReconciler.applyBaseConfiguration(cr)
	if err != nil {
		return nil, err
	}

	res.Patches = append(
		res.Patches,
		types.Patch{
			Patch: fmt.Sprintf(
				`[{"op": "replace", "path": "/subjects/0/namespace", "value": "%s"}]`,
				cr.Namespace(),
			),
			Target: &types.Selector{
				ResId: resid.ResId{
					Gvk: resid.Gvk{
						Kind: "RoleBinding",
					},
					Name: "reloader-reloader-role-binding",
				},
			},
		},
	)

	return res, nil
}
