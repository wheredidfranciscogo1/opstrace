package cluster

import (
	"fmt"
	"strings"

	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/common"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/constants"
	"gitlab.com/gitlab-org/opstrace/opstrace/scheduler/api/v1alpha1"
	logf "sigs.k8s.io/controller-runtime/pkg/log"
	"sigs.k8s.io/kustomize/api/types"
	kustomize "sigs.k8s.io/kustomize/api/types"
	"sigs.k8s.io/kustomize/kyaml/resid"
)

type ErrortrackingAPIReconciler struct {
	BaseReconciler
	clusterState *ClusterState
}

func NewErrorTrackingAPIReconciler(
	initialManifests map[string][]byte,
	teardown bool,
	clusterState *ClusterState,
) *ErrortrackingAPIReconciler {
	res := &ErrortrackingAPIReconciler{
		clusterState: clusterState,

		BaseReconciler: BaseReconciler{
			Teardown:         teardown,
			Log:              logf.Log.WithName(fmt.Sprintf("manifests/%s", constants.ErrortrackingAPIInventoryID)),
			initialManifests: initialManifests,

			inventoryID:    constants.ErrortrackingAPIInventoryID,
			reconcilerName: "errortracking-api",
			getOverridesF: func(cr *v1alpha1.Cluster) *v1alpha1.KustomizeOverridesSpec {
				return cr.Spec.Overrides.ErrorTrackingAPI
			},

			serviceAccountName: constants.ErrorTrackingAPIName,
		},
	}

	res.subclassApplyMethod = res.applyConfiguration

	return res
}

//nolint:funlen
func (i *ErrortrackingAPIReconciler) applyStsConfiguration(
	cr *v1alpha1.Cluster,
	k *kustomize.Kustomization,
) error {
	clickhouseEndpoints, err := i.clusterState.ClickHouse.GetSelfhostedEndpoints()
	if err != nil {
		return fmt.Errorf("failed to retrieve clickhouse endpoints: %w", err)
	}
	// TODO: check how to create a user/pwd for error tracking
	clickhouseDSN := clickhouseEndpoints.Native.String() + "/" + constants.ErrorTrackingAPIDatabaseName

	apiBaseURL := cr.Spec.GetHostURL()
	if cr.Spec.DNS.Domain != nil {
		apiBaseURL = "https://errortracking." + *cr.Spec.DNS.Domain
	}

	stsPatches := []string{
		fmt.Sprintf(
			`{"op": "replace", "path": "/spec/template/spec/containers/0/env/3/value", "value": "rfs-%s.%s.svc.cluster.local:26379" }`,
			constants.RedisName, cr.Namespace(),
		),
		fmt.Sprintf(
			`{"op": "replace", "path": "/spec/template/spec/containers/0/env/4/value", "value": "%s" }`,
			gatekeeperURL(cr),
		),
		fmt.Sprintf(
			`{"op": "replace", "path": "/spec/template/spec/containers/0/env/5/value", "value": "%s" }`,
			apiBaseURL,
		),
		fmt.Sprintf(
			`{"op": "replace", "path": "/spec/template/spec/containers/0/env/6/value", "value": "%s" }`,
			clickhouseDSN,
		),
	}

	clickhouseCloudEndpoint, err := i.clusterState.ClickHouse.GetCloudEndpoints()
	if err != nil {
		return fmt.Errorf("failed to retrieve clickhouse cloud endpoints %w", err)
	}
	if clickhouseCloudEndpoint != nil {
		// Add database for error tracking API to the DSN
		url := clickhouseCloudEndpoint.Native.JoinPath(constants.ErrorTrackingAPIDatabaseName)
		stsPatches = append(stsPatches, fmt.Sprintf(
			`{"op": "replace", "path": "/spec/template/spec/containers/0/env/7/value", "value": "%s" }`,
			url,
		))
	} else {
		stsPatches = append(stsPatches, fmt.Sprintf(
			`{"op": "replace", "path": "/spec/template/spec/containers/0/env/7/value", "value": "%s" }`,
			"",
		))
	}

	if i.clusterState.LogLevel != "" {
		stsPatches = append(stsPatches,
			fmt.Sprintf(
				`{"op": "replace", "path": "/spec/template/spec/containers/0/env/2/value", "value": "%s" }`,
				i.clusterState.LogLevel,
			),
		)
	}

	useRemoteStorage, err := common.ParseFeatureAsBool(cr.Spec.Features, "ET_USE_REMOTE_STORAGE_BACKEND")
	if err != nil {
		return fmt.Errorf("unable to parse feature ET_USE_REMOTE_STORAGE_BACKEND: %w", err)
	}
	if useRemoteStorage {
		stsPatches = append(
			stsPatches,
			fmt.Sprintf(
				`{"op": "add", "path": "/spec/template/spec/containers/0/env/-", "value": { "name": "USE_REMOTE_STORAGE", "value": "%s" }}`,
				cr.Spec.Target,
			),
		)
	}

	useClientSideBuffering, err := common.ParseFeatureAsBool(cr.Spec.Features, "ET_USE_CLIENT_SIDE_BUFFERING")
	if err != nil {
		return fmt.Errorf("unable to parse feature ET_USE_CLIENT_SIDE_BUFFERING: %w", err)
	}
	if useClientSideBuffering {
		stsPatches = append(
			stsPatches,
			`{"op": "add", "path": "/spec/template/spec/containers/0/env/-", "value": { "name": "CLIENT_SIDE_BUFFERING_ENABLED", "value": "true" }}`,
			`{"op": "add", "path": "/spec/template/spec/containers/0/env/-", "value": { "name": "MAX_BATCH_SIZE", "value": "1000" }}`,
			`{"op": "add", "path": "/spec/template/spec/containers/0/env/-", "value": { "name": "MAX_PROCESSOR_COUNT", "value": "1" }}`,
		)
	}

	useDebugServer, err := common.ParseFeatureAsBool(cr.Spec.Features, "ET_USE_DEBUG_SERVER")
	if err != nil {
		return fmt.Errorf("unable to parse feature ET_USE_DEBUG_SERVER: %w", err)
	}
	if useDebugServer {
		stsPatches = append(
			stsPatches,
			`{"op": "add", "path": "/spec/template/spec/containers/0/env/-", "value": { "name": "DEBUG_ENABLED", "value": "true" }}`,
		)
	}

	k.Patches = append(
		k.Patches,
		types.Patch{
			Patch: fmt.Sprintf(
				`[ %s ]`, strings.Join(stsPatches, ","),
			),
			Target: &types.Selector{
				ResId: resid.ResId{
					Gvk: resid.Gvk{
						Kind: "StatefulSet",
					},
					Name: constants.ErrorTrackingAPIName,
				},
			},
		},
	)

	return nil
}

func (i *ErrortrackingAPIReconciler) applyConfiguration(cr *v1alpha1.Cluster) (*kustomize.Kustomization, error) {
	res, err := i.BaseReconciler.applyBaseConfiguration(cr)
	if err != nil {
		return nil, err
	}

	res.Images = []kustomize.Image{
		{
			Name:    "errortracking-api-image-placeholder",
			NewName: constants.DockerImageName(constants.ErrorTrackingImageName),
			NewTag:  constants.DockerImageTag,
		},
	}

	err = i.applyStsConfiguration(cr, res)
	if err != nil {
		return nil, err
	}

	res.Patches = append(
		res.Patches,
		types.Patch{
			Patch: fmt.Sprintf(
				`[{"op": "replace", "path": "/spec/namespaceSelector", "value": { "matchNames": ["%s"] }}]`,
				cr.Namespace(),
			),
			Target: &types.Selector{
				ResId: resid.ResId{
					Gvk: resid.Gvk{
						Kind: "ServiceMonitor",
					},
					Name: constants.ErrorTrackingAPIName,
				},
			},
		},
		types.Patch{
			Patch: fmt.Sprintf(
				`[
						{"op": "replace", "path": "/spec/routes/0/match", "value": "Host(\"%s\") && PathPrefix(\"/errortracking\")" },
						{"op": "replace", "path": "/spec/routes/0/services/0/namespace", "value": "%s" },
						{"op": "replace", "path": "/spec/routes/0/middlewares/0/namespace", "value": "%s" },
						{"op": "replace", "path": "/spec/routes/0/middlewares/1/namespace", "value": "%s" },
						{"op": "replace", "path": "/spec/routes/0/middlewares/2/namespace", "value": "%s" },
						{"op": "replace", "path": "/spec/tls/domains/0/main", "value": "%s" }
					  ]`,
				cr.Spec.GetHost(),
				cr.Namespace(),
				cr.Namespace(),
				cr.Namespace(),
				cr.Namespace(),
				cr.Spec.GetHost(),
			),
			Target: &types.Selector{
				ResId: resid.ResId{
					Gvk: resid.Gvk{
						Kind: "IngressRoute",
					},
					Name: constants.ErrorTrackingAPIName,
				},
			},
		},
	)

	return res, nil
}
