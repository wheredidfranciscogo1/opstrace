package cluster

import (
	"errors"
	"fmt"

	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/common"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/constants"
	"gitlab.com/gitlab-org/opstrace/opstrace/scheduler/api/v1alpha1"
	ctrlcommon "gitlab.com/gitlab-org/opstrace/opstrace/scheduler/controllers/common"
	logf "sigs.k8s.io/controller-runtime/pkg/log"
	"sigs.k8s.io/kustomize/api/types"
	kustomize "sigs.k8s.io/kustomize/api/types"
	"sigs.k8s.io/kustomize/kyaml/resid"
)

type ExternalDNSReconciler struct {
	BaseReconciler
}

func NewExternalDNSReconciler(
	initialManifests map[string][]byte,
	teardown bool,
) *ExternalDNSReconciler {
	res := &ExternalDNSReconciler{
		BaseReconciler: BaseReconciler{
			Teardown:         teardown,
			Log:              logf.Log.WithName(fmt.Sprintf("manifests/%s", constants.ExternalDNSInventoryID)),
			initialManifests: initialManifests,

			inventoryID:    constants.ExternalDNSInventoryID,
			reconcilerName: "external-dns",
			getOverridesF: func(cr *v1alpha1.Cluster) *v1alpha1.KustomizeOverridesSpec {
				return cr.Spec.Overrides.ExternalDNS
			},
		},
	}

	res.subclassApplyMethod = res.applyConfiguration

	return res
}

func (i *ExternalDNSReconciler) Reconcile(cr *v1alpha1.Cluster) common.DesiredState {
	// return early if we're running on a cluster that does not need
	// external-dns setup, e.g. KIND clusters within our development
	// environments
	if !cr.Spec.UseExternalDNS() {
		return common.DesiredState{
			common.LogAction{
				Msg:   "[external-dns] skipping setup, due to cluster not needing external-dns",
				Error: nil,
			},
		}
	}

	return i.BaseReconciler.Reconcile(cr)
}

var (
	errNoProvider       = errors.New("no supported externaldns provider configured")
	errMultipleProvider = errors.New("multiple externaldns providers configured")
)

func (i *ExternalDNSReconciler) applyConfiguration(cr *v1alpha1.Cluster) (*kustomize.Kustomization, error) {
	var providerCount int = 0
	// gcp
	if cr.Spec.DNS.ExternalDNSProvider.GCP != nil {
		providerCount++
	}
	// cloudflare
	if cr.Spec.DNS.ExternalDNSProvider.Cloudflare != nil {
		providerCount++
	}
	// assertions
	if providerCount == 0 {
		return nil, errNoProvider
	}
	if providerCount > 1 {
		return nil, errMultipleProvider
	}

	var patches []types.Patch
	// gcp
	if cr.Spec.DNS.ExternalDNSProvider.GCP != nil {
		// ServiceAccount
		patches = append(patches, types.Patch{
			Target: serviceAccountSelector(),
			Patch: fmt.Sprintf(
				`[
					{"op": "add", "path": "/metadata/annotations/iam.gke.io~1gcp-service-account", "value": "%s"}
				]`,
				cr.Spec.DNS.ExternalDNSProvider.GCP.DNSServiceAccountName,
			),
		})
		// Deployment
		args := []string{
			"--provider=google",
			"--txt-cache-interval=1m",
			"--events",
			fmt.Sprintf("--txt-owner-id=opstrace-%s", cr.Name),
		}
		// external-dns detects zones to update via zone ID filter or domain filter
		// prefer to use the managed zone name if it's available.
		if cr.Spec.DNS.ExternalDNSProvider.GCP.ManagedZoneName != nil {
			args = append(args, fmt.Sprintf("--zone-id-filter=%s", *cr.Spec.DNS.ExternalDNSProvider.GCP.ManagedZoneName))
		} else if cr.Spec.DNS.Domain != nil {
			args = append(args, fmt.Sprintf("--domain-filter=%s", *cr.Spec.DNS.Domain))
		}
		for _, arg := range args {
			patches = append(patches, types.Patch{
				Target: deploymentSelector(),
				Patch:  fmt.Sprintf(`[{"op": "add", "path": "/spec/template/spec/containers/0/args/-", "value": "%s"}]`, arg),
			})
		}
	}
	// cloudflare
	if cr.Spec.DNS.ExternalDNSProvider.Cloudflare != nil {
		args := []string{
			"--provider=cloudflare",
			"--cloudflare-proxied",
		}
		if cr.Spec.DNS.Domain != nil {
			args = append(args, fmt.Sprintf("--domain-filter=%s", *cr.Spec.DNS.Domain))
		}
		if cr.Spec.DNS.ExternalDNSProvider.Cloudflare.ZoneID != nil {
			args = append(args, fmt.Sprintf("--zone-id-filter=%s", *cr.Spec.DNS.ExternalDNSProvider.Cloudflare.ZoneID))
		}
		for _, arg := range args {
			patches = append(patches, types.Patch{
				Target: deploymentSelector(),
				Patch:  fmt.Sprintf(`[{"op": "add", "path": "/spec/template/spec/containers/0/args/-", "value": "%s"}]`, arg),
			})
		}
	}

	res := &kustomize.Kustomization{
		TypeMeta: kustomize.TypeMeta{
			Kind:       kustomize.KustomizationKind,
			APIVersion: kustomize.KustomizationVersion,
		},
		Namespace: cr.Namespace(),
		Resources: []string{
			ctrlcommon.TmpResourceData,
		},
		Patches: patches,
	}
	return res, nil
}

func serviceAccountSelector() *kustomize.Selector {
	return &types.Selector{
		ResId: resid.ResId{
			Gvk: resid.Gvk{
				Kind: "ServiceAccount",
			},
			Name: "external-dns",
		},
	}
}

func deploymentSelector() *kustomize.Selector {
	return &types.Selector{
		ResId: resid.ResId{
			Gvk: resid.Gvk{
				Kind: "Deployment",
			},
			Name: "external-dns",
		},
	}
}
