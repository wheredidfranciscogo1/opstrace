package cluster

import (
	"fmt"
	"strings"

	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/common"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/constants"
	"gitlab.com/gitlab-org/opstrace/opstrace/scheduler/api/v1alpha1"
	v1 "k8s.io/api/core/v1"
	logf "sigs.k8s.io/controller-runtime/pkg/log"
	"sigs.k8s.io/kustomize/api/types"
	kustomize "sigs.k8s.io/kustomize/api/types"
	"sigs.k8s.io/kustomize/kyaml/resid"
)

type IngressControllerReconciler struct {
	BaseReconciler
}

func NewIngressControllerReconciler(
	initialManifests map[string][]byte,
	teardown bool,
) *IngressControllerReconciler {
	res := &IngressControllerReconciler{
		BaseReconciler: BaseReconciler{
			Teardown:         teardown,
			Log:              logf.Log.WithName(fmt.Sprintf("manifests/%s", constants.IngressControllerInventoryID)),
			initialManifests: initialManifests,

			inventoryID:    constants.IngressControllerInventoryID,
			reconcilerName: "ingress-controller",
			getOverridesF: func(cr *v1alpha1.Cluster) *v1alpha1.KustomizeOverridesSpec {
				return cr.Spec.Overrides.IngressController
			},

			serviceAccountName: constants.IngressControllerServiceAccountName,
		},
	}

	res.subclassApplyMethod = res.applyConfiguration

	return res
}

func gatekeeperURL(cr *v1alpha1.Cluster) string {
	return fmt.Sprintf("http://gatekeeper.%s.svc.cluster.local:3001", cr.Namespace())
}

//nolint:funlen // This is just a long object definition with few ifs, not a complex function
func (i *IngressControllerReconciler) applyConfiguration(cr *v1alpha1.Cluster) (*kustomize.Kustomization, error) {
	res, err := i.BaseReconciler.applyBaseConfiguration(cr)
	if err != nil {
		return nil, err
	}

	args := []string{
		"--tracing.jaeger=true",
		"--tracing.jaeger.traceContextHeaderName=x-request-id",
		fmt.Sprintf(
			"--tracing.jaeger.collector.endpoint=http://%s-collector.%s.svc.cluster.local:14268/api/traces?format=jaeger.thrift",
			constants.OpenTelemetrySystemTracingCollector,
			cr.Namespace(),
		),
	}

	for _, arg := range args {
		res.Patches = append(res.Patches,
			types.Patch{
				Target: &types.Selector{
					ResId: resid.ResId{
						Gvk: resid.Gvk{
							Kind: "Deployment",
						},
						Name: constants.IngressControllerDeploymentName,
					},
				},
				Patch: fmt.Sprintf(`[{"op": "add", "path": "/spec/template/spec/containers/0/args/-", "value": "%s"}]`, arg),
			},
		)
	}

	res.Patches = append(
		res.Patches,
		types.Patch{
			Patch: fmt.Sprintf(
				`[
						{"op": "add", "path": "/metadata/annotations/external-dns.alpha.kubernetes.io~1hostname", "value": "%s" },
						{"op": "add", "path": "/metadata/annotations/external-dns.alpha.kubernetes.io~1ttl", "value": "30" }
					]`,
				cr.Spec.GetHost(),
			),
			Target: &types.Selector{
				ResId: resid.ResId{
					Gvk: resid.Gvk{
						Kind: "Service",
					},
					Name: constants.IngressControllerServiceName,
				},
			},
		},
		types.Patch{
			Patch: fmt.Sprintf(
				`[{"op": "replace", "path": "/spec/namespaceSelector", "value": { "matchNames": ["%s"] }}]`,
				cr.Namespace(),
			),
			Target: &types.Selector{
				ResId: resid.ResId{
					Gvk: resid.Gvk{
						Kind: "PodMonitor",
					},
					Name: constants.IngressControllerPodMonitorName,
				},
			},
		},
		types.Patch{
			Patch: fmt.Sprintf(
				`[{"op": "replace", "path": "/spec/plugin/gob-forward-auth/baseAddress", "value": "%s"}]`,
				gatekeeperURL(cr),
			),
			Target: &types.Selector{
				ResId: resid.ResId{
					Gvk: resid.Gvk{
						Kind: "Middleware",
					},
					Name: "forward-auth",
				},
			},
		},
		types.Patch{
			Patch: fmt.Sprintf(
				`[{"op": "replace", "path": "/spec/errors/service/namespace", "value": "%s" }]`,
				cr.Namespace(),
			),
			Target: &types.Selector{
				ResId: resid.ResId{
					Gvk: resid.Gvk{
						Kind: "Middleware",
					},
					Name: "custom-errors-gk",
				},
			},
		},
		types.Patch{
			Patch: fmt.Sprintf(
				`[{"op": "replace", "path": "/subjects/0/namespace", "value": "%s"}]`,
				cr.Namespace(),
			),
			Target: &types.Selector{
				ResId: resid.ResId{
					Gvk: resid.Gvk{
						Kind: "ClusterRoleBinding",
					},
					Name: "traefik-ingress-controller",
				},
			},
		},
	)

	if cr.Spec.Target == common.AWS {
		res.Patches = append(res.Patches,
			// * Use an NLB type loadbalancer
			// * Ensure the ELB idle timeout is less than nginx keep-alive timeout. Because we're
			//   using WebSockets, the value will need to be
			//   increased to '3600' to avoid any potential issues. We set the NGINX timeout to 3700
			//   so that it's longer than the LB timeout.
			types.Patch{
				Patch: `[
					{"op": "add", "path": "/metadata/annotations/service.beta.kubernetes.io~1aws-load-balancer-type", "value": "nlb"},
					{"op": "add", "path": "/metadata/annotations/service.beta.kubernetes.io~1aws-load-balancer-backend-protocol", "value": "tcp"},
					{"op": "add", "path": "/metadata/annotations/service.beta.kubernetes.io~1aws-load-balancer-connection-idle-timeout", "value": "3600"}
				]`,
				Target: &types.Selector{
					ResId: resid.ResId{
						Gvk: resid.Gvk{
							Kind: "Service",
						},
						Name: constants.IngressControllerServiceName,
					},
				},
			},
		)
	}

	if cr.Spec.Target == common.KIND {
		res.Patches = append(res.Patches,
			// Use a NodePort type loadbalancer
			types.Patch{
				Patch: fmt.Sprintf(
					`[{"op": "replace", "path": "/spec/type", "value": "%s"}]`,
					v1.ServiceTypeNodePort,
				),
				Target: &types.Selector{
					ResId: resid.ResId{
						Gvk: resid.Gvk{
							Kind: "Service",
						},
						Name: constants.IngressControllerServiceName,
					},
				},
			},
			types.Patch{
				Patch: `[
					{"op": "add", "path": "/spec/template/spec/nodeSelector", "value": {"ingress-ready": "true" }},
					{"op": "add", "path": "/spec/template/spec/containers/0/ports/2/hostPort", "value": 80 },
					{"op": "add", "path": "/spec/template/spec/containers/0/ports/3/hostPort", "value": 443 },
					{"op": "replace", "path": "/spec/replicas", "value": 1}
					]`,
				Target: &types.Selector{
					ResId: resid.ResId{
						Gvk: resid.Gvk{
							Kind: "Deployment",
						},
						Name: constants.IngressControllerDeploymentName,
					},
				},
			},
		)
	} else {
		res.Patches = append(res.Patches,
			// Use an NLB type loadbalancer
			types.Patch{
				Patch: fmt.Sprintf(
					`[{"op": "replace", "path": "/spec/type", "value": "%s"}]`,
					v1.ServiceTypeLoadBalancer,
				),
				Target: &types.Selector{
					ResId: resid.ResId{
						Gvk: resid.Gvk{
							Kind: "Service",
						},
						Name: constants.IngressControllerServiceName,
					},
				},
			},
		)
	}

	if len(cr.Spec.DNS.FirewallSourceIPsAllowed) > 0 {
		tmp := make([]string, len(cr.Spec.DNS.FirewallSourceIPsAllowed))
		for idx, s := range cr.Spec.DNS.FirewallSourceIPsAllowed {
			tmp[idx] = fmt.Sprintf(`"%s"`, s)
		}
		tmpJoined := strings.Join(tmp, ",")
		res.Patches = append(res.Patches,
			// Use an NLB type loadbalancer
			types.Patch{
				Patch: fmt.Sprintf(
					`[{"op": "add", "path": "/spec/loadBalancerSourceRanges", "value": [%s]}]`,
					tmpJoined,
				),
				Target: &types.Selector{
					ResId: resid.ResId{
						Gvk: resid.Gvk{
							Kind: "Service",
						},
						Name: constants.IngressControllerServiceName,
					},
				},
			},
		)
	}

	return res, nil
}
