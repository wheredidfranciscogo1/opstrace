package cluster

import (
	"context"
	"fmt"
	"net/url"

	clickhousev1alpha1 "gitlab.com/gitlab-org/opstrace/opstrace/clickhouse-operator/api/v1alpha1"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/constants"
	"gitlab.com/gitlab-org/opstrace/opstrace/scheduler/api/v1alpha1"
	"gitlab.com/gitlab-org/opstrace/opstrace/scheduler/controllers/cluster/clickhouse"
	"gitlab.com/gitlab-org/opstrace/opstrace/scheduler/controllers/config"
	v1 "k8s.io/api/core/v1"
	"k8s.io/apimachinery/pkg/api/errors"
	"sigs.k8s.io/controller-runtime/pkg/client"
)

type ClickHouseState struct {
	// Keep track of Credentials so we can pass them to our ClickHouse client
	Credentials *v1.Secret
	// Track Clickhouse Cluster CR status:
	Cluster *clickhousev1alpha1.ClickHouse

	// ClickHouse Cloud credentials
	CloudCredentials *v1.Secret
	cloudCredKey     string
}

func NewClickHouseState() *ClickHouseState {
	return &ClickHouseState{}
}

func (i *ClickHouseState) ReadCredentials(ctx context.Context, cr *v1alpha1.Cluster, client client.Client) error {
	err := i.readCredentials(ctx, cr, client)
	if err != nil {
		return err
	}
	return i.readCloudCredentials(ctx, cr, client)
}

func (i *ClickHouseState) Read(ctx context.Context, cr *v1alpha1.Cluster, client client.Client) error {
	err := i.readCredentials(ctx, cr, client)
	if err != nil {
		return err
	}
	err = i.readCluster(ctx, cr, client)
	if err != nil {
		return err
	}
	err = i.readCloudCredentials(ctx, cr, client)
	if err != nil {
		return err
	}
	return nil
}

// Get ClickHouse username/password for the scheduler
func (i *ClickHouseState) GetSchedulerCredentials() (*url.Userinfo, error) {
	e, err := i.GetSelfhostedEndpoints()
	if err != nil {
		return nil, err
	}
	// user is the same for http and native endpoints
	return e.Http.User, nil
}

// Get ClickHouse endpoints for the scheduler user
func (i *ClickHouseState) GetSelfhostedEndpoints() (*config.ClickHouseEndpoints, error) {
	// check existence of the secret
	if i.Credentials == nil {
		return nil, fmt.Errorf("clickhouse scheduler credentials secret not found")
	}

	// check existence of the HTTP endpoint key
	if i.Credentials.Data[constants.ClickHouseCredentialsHTTPEndpointKey] == nil {
		return nil, fmt.Errorf("%s key not set in clickhouse scheduler credentials secret",
			constants.ClickHouseCredentialsHTTPEndpointKey)
	}
	http := string(i.Credentials.Data[constants.ClickHouseCredentialsHTTPEndpointKey])
	httpURL, err := url.Parse(http)
	if err != nil {
		return nil, err
	}

	// check existence of the native endpoint key
	if i.Credentials.Data[constants.ClickHouseCredentialsNativeEndpointKey] == nil {
		return nil, fmt.Errorf("%s key not set in clickhouse scheduler credentials secret",
			constants.ClickHouseCredentialsNativeEndpointKey)
	}
	native := string(i.Credentials.Data[constants.ClickHouseCredentialsNativeEndpointKey])
	nativeURL, err := url.Parse(native)
	if err != nil {
		return nil, err
	}

	return &config.ClickHouseEndpoints{
		Http:   *httpURL,
		Native: *nativeURL,
	}, nil
}

// Get ClickHouse cluster endpoint
func (i *ClickHouseState) isClusterReady() bool {
	if i.Cluster == nil {
		return false
	}
	return i.Cluster.Status.Status == clickhousev1alpha1.StatusCompleted
}

func (i *ClickHouseState) readCredentials(ctx context.Context, cr *v1alpha1.Cluster, client client.Client) error {
	currentState := &v1.Secret{}
	selector := clickhouse.CredentialsSelector(cr)
	err := client.Get(ctx, selector, currentState)
	if err != nil {
		if errors.IsNotFound(err) {
			return nil
		}
		return err
	}
	i.Credentials = currentState.DeepCopy()
	return nil
}

func (i *ClickHouseState) readCluster(ctx context.Context, cr *v1alpha1.Cluster, client client.Client) error {
	currentState := &clickhousev1alpha1.ClickHouse{}
	selector := clickhouse.ClickHouseSelector(cr)
	err := client.Get(ctx, selector, currentState)
	if err != nil {
		if errors.IsNotFound(err) {
			return nil
		}
		return err
	}
	i.Cluster = currentState.DeepCopy()
	return nil
}

func (i *ClickHouseState) readCloudCredentials(ctx context.Context, cr *v1alpha1.Cluster, cli client.Client) error {
	if cr.Spec.Overrides.ClickHouse.ClickHouseCloudDSN != nil {
		cloudDSNRef := *cr.Spec.Overrides.ClickHouse.ClickHouseCloudDSN
		// DSN should be in the same namespace as Cluster.
		selector := client.ObjectKey{
			Name:      cloudDSNRef.Name,
			Namespace: cr.Namespace(),
		}
		DSNSecret := &v1.Secret{}
		err := cli.Get(ctx, selector, DSNSecret)
		if err != nil {
			if errors.IsNotFound(err) {
				return nil
			}
			return err
		}

		i.CloudCredentials = DSNSecret.DeepCopy()
		i.cloudCredKey = cloudDSNRef.Key
	}
	return nil
}

func (i *ClickHouseState) GetCloudEndpoints() (*config.ClickHouseEndpoints, error) {
	// check existence of the secret, it is not an error if there are none
	if i.CloudCredentials == nil {
		return nil, nil
	}

	secret, ok := i.CloudCredentials.Data[i.cloudCredKey]
	if !ok {
		return nil, fmt.Errorf("%s key not set in the cloud credentials secret", i.cloudCredKey)
	}

	native, err := url.Parse(string(secret))
	if err != nil {
		return nil, fmt.Errorf("provided dsn in the secret is not a URL :%w", err)
	}

	// Cloud DSN only has native DSN currently
	return &config.ClickHouseEndpoints{
		Native: *native,
	}, nil
}
