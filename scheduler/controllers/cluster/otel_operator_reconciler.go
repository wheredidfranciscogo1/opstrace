package cluster

import (
	"fmt"

	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/constants"
	"gitlab.com/gitlab-org/opstrace/opstrace/scheduler/api/v1alpha1"
	logf "sigs.k8s.io/controller-runtime/pkg/log"
	"sigs.k8s.io/kustomize/api/types"
	kustomize "sigs.k8s.io/kustomize/api/types"
	"sigs.k8s.io/kustomize/kyaml/resid"
)

type OpenTelemetryOperatorReconciler struct {
	BaseReconciler
}

func NewOpenTelemetryOperatorReconciler(
	initialManifests map[string][]byte,
	teardown bool,
) *OpenTelemetryOperatorReconciler {
	res := &OpenTelemetryOperatorReconciler{
		BaseReconciler: BaseReconciler{
			Teardown:         teardown,
			Log:              logf.Log.WithName(fmt.Sprintf("manifests/%s", constants.OpenTelemetryOperatorInventoryID)),
			initialManifests: initialManifests,

			inventoryID:    constants.OpenTelemetryOperatorInventoryID,
			reconcilerName: "opentelemetry-operator",
			getOverridesF: func(cr *v1alpha1.Cluster) *v1alpha1.KustomizeOverridesSpec {
				return cr.Spec.Overrides.OpenTelemetryOperator
			},

			serviceAccountName: constants.OpenTelemetryOperatorServiceAccountName,
		},
	}

	res.subclassApplyMethod = res.applyConfiguration

	return res
}

func (i *OpenTelemetryOperatorReconciler) applyConfiguration(cr *v1alpha1.Cluster) (*kustomize.Kustomization, error) {
	res, err := i.BaseReconciler.applyBaseConfiguration(cr)
	if err != nil {
		return nil, err
	}

	// Put opentelemetry-operator in its own, default namespace: `opentelemetry-operator-system`
	res.Namespace = ""

	res.Patches = append(
		res.Patches,
		types.Patch{
			Patch: fmt.Sprintf(
				`[
					{"op": "replace", "path": "/metadata/namespace", "value": "%s" },
					{"op": "replace", "path": "/spec/env/0/valueFrom/secretKeyRef/name", "value": "%s" },
					{"op": "replace", "path": "/spec/env/1/valueFrom/secretKeyRef/name", "value": "%s" },
					{"op": "replace", "path": "/spec/env/2/value", "value": "%s" },
					{"op": "replace", "path": "/spec/env/3/value", "value": "%s" },
					{"op": "replace", "path": "/spec/env/4/value", "value": "%s" }
				]`,
				cr.Namespace(),
				cr.Spec.GitLab.AuthSecret.Name,
				cr.Spec.GitLab.AuthSecret.Name,
				cr.Name,
				cr.Spec.GetHost(),
				cr.Spec.Target,
			),
			Target: &types.Selector{
				ResId: resid.ResId{
					Gvk: resid.Gvk{
						Kind: "OpenTelemetryCollector",
					},
					Name: constants.OpenTelemetrySystemTracingCollector,
				},
			},
		},
	)

	return res, nil
}
