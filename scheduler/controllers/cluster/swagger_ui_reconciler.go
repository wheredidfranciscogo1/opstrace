package cluster

import (
	"encoding/json"
	"fmt"
	"strings"

	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/constants"
	"gitlab.com/gitlab-org/opstrace/opstrace/scheduler/api/v1alpha1"
	logf "sigs.k8s.io/controller-runtime/pkg/log"
	"sigs.k8s.io/kustomize/api/types"
	kustomize "sigs.k8s.io/kustomize/api/types"
	"sigs.k8s.io/kustomize/kyaml/resid"
)

type SwaggerUIReconciler struct {
	BaseReconciler
}

func NewSwaggerUIReconciler(
	initialManifests map[string][]byte,
	teardown bool,
) *SwaggerUIReconciler {
	res := &SwaggerUIReconciler{
		BaseReconciler: BaseReconciler{
			Teardown:         teardown,
			Log:              logf.Log.WithName(fmt.Sprintf("manifests/%s", constants.SwaggerUIInventoryID)),
			initialManifests: initialManifests,

			inventoryID:    constants.SwaggerUIInventoryID,
			reconcilerName: "swagger-ui",
		},
	}

	res.subclassApplyMethod = res.applyConfiguration

	return res
}

func (i *SwaggerUIReconciler) applyConfiguration(cr *v1alpha1.Cluster) (*kustomize.Kustomization, error) {
	res, err := i.BaseReconciler.applyBaseConfiguration(cr)
	if err != nil {
		return nil, err
	}

	// swagger docs - order is preserved in the UI
	swaggerURLs := []struct {
		URL      string `json:"url"`
		Name     string `json:"name"`
		filename string
	}{
		{
			Name:     "Error Tracking",
			URL:      "http://errortracking-api." + cr.Namespace() + ".svc.cluster.local:8080/swagger.json",
			filename: "error-tracking",
		},
		{
			Name:     "Gatekeeper",
			URL:      gatekeeperURL(cr) + "/swagger/doc.json",
			filename: "gatekeeper",
		},
	}

	// used for init container to get the swagger docs
	// space separated filename and URL
	initPairs := make([]string, len(swaggerURLs))
	for i, swaggerURL := range swaggerURLs {
		initPairs[i] = swaggerURL.filename + " " + swaggerURL.URL
	}

	// urls used by swagger-ui for URLS env var
	// update the URL to form the relative docs path
	for i, s := range swaggerURLs {
		swaggerURLs[i].URL = fmt.Sprintf("/swagger/docs/%s.json", s.filename)
	}

	urls, err := json.Marshal(swaggerURLs)
	if err != nil {
		return nil, fmt.Errorf("json marshal swagger urls: %w", err)
	}

	res.Patches = append(
		res.Patches,
		types.Patch{
			Patch: fmt.Sprintf(
				`[
				    {"op": "replace", "path": "/spec/template/spec/initContainers/0/env/1/value", "value": "%s" },
					{"op": "replace", "path": "/spec/template/spec/containers/0/env/0/value", "value": "%s" },
					{"op": "add", "path": "/spec/template/metadata/annotations", "value": { "version": "%s" } }
				]`,
				strings.Join(initPairs, "\\n"),
				strings.ReplaceAll(string(urls), "\"", "\\\""),
				// update version annotation to trigger redeployment
				constants.DockerImageTag,
			),
			Target: &types.Selector{
				ResId: resid.ResId{
					Gvk: resid.Gvk{
						Kind: "Deployment",
					},
					Name: "swagger-ui",
				},
			},
		},
		types.Patch{
			Patch: fmt.Sprintf(
				`[
					{"op": "replace", "path": "/spec/routes/0/match", "value": "Host(\"%s\") && PathPrefix(\"/swagger\")" },
					{"op": "replace", "path": "/spec/routes/0/services/0/namespace", "value": "%s" },
					{"op": "replace", "path": "/spec/routes/0/middlewares/0/namespace", "value": "%s" },
					{"op": "replace", "path": "/spec/tls/domains/0/main", "value": "%s" }
				]`,
				cr.Spec.GetHost(),
				cr.Namespace(),
				cr.Namespace(),
				cr.Spec.GetHost(),
			),
			Target: &types.Selector{
				ResId: resid.ResId{
					Gvk: resid.Gvk{
						Kind: "IngressRoute",
					},
					Name: "swagger-ui",
				},
			},
		},
	)

	return res, nil
}
