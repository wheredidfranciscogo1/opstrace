package cluster

import (
	"fmt"

	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/constants"
	"gitlab.com/gitlab-org/opstrace/opstrace/scheduler/api/v1alpha1"
	logf "sigs.k8s.io/controller-runtime/pkg/log"
	"sigs.k8s.io/kustomize/api/types"
	kustomize "sigs.k8s.io/kustomize/api/types"
	"sigs.k8s.io/kustomize/kyaml/resid"
)

type JaegerOperatorReconciler struct {
	BaseReconciler
}

func NewJaegerOperatorReconciler(
	initialManifests map[string][]byte,
	teardown bool,
) *JaegerOperatorReconciler {
	res := &JaegerOperatorReconciler{
		BaseReconciler: BaseReconciler{
			Teardown:         teardown,
			Log:              logf.Log.WithName(fmt.Sprintf("manifests/%s", constants.JaegerOperatorInventoryID)),
			initialManifests: initialManifests,

			inventoryID:    constants.JaegerOperatorInventoryID,
			reconcilerName: "jaeger-operator",
			getOverridesF: func(cr *v1alpha1.Cluster) *v1alpha1.KustomizeOverridesSpec {
				return cr.Spec.Overrides.JaegerOperator
			},

			serviceAccountName: constants.JaegerOperatorName,
		},
	}

	res.subclassApplyMethod = res.applyConfiguration

	return res
}

//nolint:funlen
func (i *JaegerOperatorReconciler) applyConfiguration(cr *v1alpha1.Cluster) (*kustomize.Kustomization, error) {
	res, err := i.BaseReconciler.applyBaseConfiguration(cr)
	if err != nil {
		return nil, err
	}

	res.Patches = append(
		res.Patches,
		types.Patch{
			Patch: fmt.Sprintf(
				`[{"op": "replace", "path": "/spec/namespaceSelector", "value": { "matchNames": ["%s"] }}]`,
				cr.Namespace(),
			),
			Target: &types.Selector{
				ResId: resid.ResId{
					Gvk: resid.Gvk{
						Kind: "ServiceMonitor",
					},
					Name: constants.JaegerOperatorName,
				},
			},
		},
		types.Patch{
			Patch: fmt.Sprintf(
				`[{"op": "replace", "path": "/spec/dnsNames", "value": ["jaeger-operator-webhook-service.%s.svc", "jaeger-operator-webhook-service.%s.svc.cluster.local"]}]`,
				cr.Namespace(),
				cr.Namespace(),
			),
			Target: &types.Selector{
				ResId: resid.ResId{
					Gvk: resid.Gvk{
						Kind: "Certificate",
					},
					Name: "jaeger-operator-serving-cert",
				},
			},
		},
		types.Patch{
			Patch: fmt.Sprintf(
				`[
						{"op": "replace", "path": "/metadata/annotations/cert-manager.io~1inject-ca-from", "value": "%s/jaeger-operator-serving-cert" },
						{"op": "replace", "path": "/webhooks/0/clientConfig/service/namespace", "value": "%s" },
						{"op": "replace", "path": "/webhooks/1/clientConfig/service/namespace", "value": "%s" }
					]`,
				cr.Namespace(),
				cr.Namespace(),
				cr.Namespace(),
			),
			Target: &types.Selector{
				ResId: resid.ResId{
					Gvk: resid.Gvk{
						Kind: "MutatingWebhookConfiguration",
					},
					Name: "jaeger-operator-mutating-webhook-configuration",
				},
			},
		},
		types.Patch{
			Patch: fmt.Sprintf(
				`[
						{"op": "replace", "path": "/metadata/annotations/cert-manager.io~1inject-ca-from", "value": "%s/jaeger-operator-serving-cert" },
						{"op": "replace", "path": "/webhooks/0/clientConfig/service/namespace", "value": "%s" }
					]`,
				cr.Namespace(),
				cr.Namespace(),
			),
			Target: &types.Selector{
				ResId: resid.ResId{
					Gvk: resid.Gvk{
						Kind: "ValidatingWebhookConfiguration",
					},
					Name: "jaeger-operator-validating-webhook-configuration",
				},
			},
		},
		types.Patch{
			Patch: fmt.Sprintf(
				`[
						{"op": "replace", "path": "/metadata/annotations/cert-manager.io~1inject-ca-from", "value": "%s/jaeger-operator-serving-cert" }
					]`,
				cr.Namespace(),
			),
			Target: &types.Selector{
				ResId: resid.ResId{
					Gvk: resid.Gvk{
						Kind: "CustomResourceDefinition",
					},
					Name: "jaegers.jaegertracing.io",
				},
			},
		},
		types.Patch{
			Patch: fmt.Sprintf(
				`[{"op": "replace", "path": "/subjects/0/namespace", "value": "%s"}]`,
				cr.Namespace(),
			),
			Target: &types.Selector{
				ResId: resid.ResId{
					Gvk: resid.Gvk{
						Kind: "RoleBinding",
					},
					Name: "jaeger-leader-election-rolebinding",
				},
			},
		},
		types.Patch{
			Patch: fmt.Sprintf(
				`[{"op": "replace", "path": "/subjects/0/namespace", "value": "%s"}]`,
				cr.Namespace(),
			),
			Target: &types.Selector{
				ResId: resid.ResId{
					Gvk: resid.Gvk{
						Kind: "ClusterRoleBinding",
					},
					Name: "jaeger-operator-proxy-rolebinding",
				},
			},
		},
		types.Patch{
			Patch: fmt.Sprintf(
				`[{"op": "replace", "path": "/subjects/0/namespace", "value": "%s"}]`,
				cr.Namespace(),
			),
			Target: &types.Selector{
				ResId: resid.ResId{
					Gvk: resid.Gvk{
						Kind: "ClusterRoleBinding",
					},
					Name: "jaeger-manager-rolebinding",
				},
			},
		},
	)

	return res, nil
}
