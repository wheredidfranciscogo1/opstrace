package cluster

import (
	"fmt"

	logf "sigs.k8s.io/controller-runtime/pkg/log"
	"sigs.k8s.io/kustomize/api/types"
	kustomize "sigs.k8s.io/kustomize/api/types"
	"sigs.k8s.io/kustomize/kyaml/resid"

	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/constants"
	"gitlab.com/gitlab-org/opstrace/opstrace/scheduler/api/v1alpha1"
)

type ProvisioningAPIReconciler struct {
	BaseReconciler

	LogLevel string
}

func NewProvisioningAPIReconciler(
	initialManifests map[string][]byte,
	teardown bool,
	logLevel string,
) *ProvisioningAPIReconciler {
	res := &ProvisioningAPIReconciler{
		BaseReconciler: BaseReconciler{
			Teardown:         teardown,
			Log:              logf.Log.WithName(fmt.Sprintf("manifests/%s", constants.ProvisioningAPIInventoryID)),
			initialManifests: initialManifests,

			inventoryID:    constants.ProvisioningAPIInventoryID,
			reconcilerName: "provisioning-api",
			getOverridesF: func(cr *v1alpha1.Cluster) *v1alpha1.KustomizeOverridesSpec {
				return cr.Spec.Overrides.ProvisioningAPI
			},

			serviceAccountName: constants.ProvisioningAPIName,
		},
		LogLevel: logLevel,
	}

	res.subclassApplyMethod = res.applyConfiguration
	return res
}

//nolint:funlen // Just a bunch of patches, splitting it up would IMO make things harder to read
func (r *ProvisioningAPIReconciler) applyConfiguration(cr *v1alpha1.Cluster) (*kustomize.Kustomization, error) {
	res, err := r.BaseReconciler.applyBaseConfiguration(cr)
	if err != nil {
		return nil, err
	}
	res.Images = []kustomize.Image{
		{
			Name:    "provisioning-api-image-placeholder",
			NewName: constants.DockerImageName(constants.ProvisioningAPIImageName),
			NewTag:  constants.DockerImageTag,
		},
	}
	res.Patches = append(
		res.Patches,
		types.Patch{
			Patch: fmt.Sprintf(
				`[
						{"op": "replace", "path": "/spec/routes/0/match", "value": "Host(\"%s\") && Path(\"/v3/tenant/{projectID:[0-9]+}\")" },
						{"op": "replace", "path": "/spec/routes/0/services/0/namespace", "value": "%s" },
						{"op": "replace", "path": "/spec/routes/0/middlewares/0/namespace", "value": "%s" },
						{"op": "replace", "path": "/spec/routes/0/middlewares/1/namespace", "value": "%s" },
						{"op": "replace", "path": "/spec/tls/domains/0/main", "value": "%s" }
					  ]`,
				cr.Spec.GetHost(),
				cr.Namespace(),
				cr.Namespace(),
				cr.Namespace(),
				cr.Spec.GetHost(),
			),
			Target: &types.Selector{
				ResId: resid.ResId{
					Gvk: resid.Gvk{
						Kind: "IngressRoute",
					},
					Name: constants.ProvisioningAPIName,
				},
			},
		},
		types.Patch{
			Patch: fmt.Sprintf(
				`[{"op": "replace", "path": "/spec/headers/accessControlAllowOriginList", "value": ["%s"]}]`,
				cr.Spec.GitLab.TrimInstanceURL(),
			),
			Target: &types.Selector{
				ResId: resid.ResId{
					Gvk: resid.Gvk{
						Kind: "Middleware",
					},
					Name: "cors-headers-papi",
				},
			},
		},
		types.Patch{
			Target: &types.Selector{
				ResId: resid.ResId{
					Gvk: resid.Gvk{
						Kind: "PodMonitor",
					},
					Name: constants.ProvisioningAPIName,
				},
			},
			Patch: fmt.Sprintf(
				`[ {"op": "replace", "path": "/spec/namespaceSelector", "value": { "matchNames": ["%s"] }} ]`,
				cr.Namespace(),
			),
		},
		types.Patch{
			Patch: fmt.Sprintf(
				`[{"op": "replace", "path": "/subjects/0/namespace", "value": "%s" }]`,
				cr.Namespace(),
			),
			Target: &types.Selector{
				ResId: resid.ResId{
					Gvk: resid.Gvk{
						Kind: "ClusterRoleBinding",
					},
					Name: constants.ProvisioningAPIName,
				},
			},
		},
	)

	if r.LogLevel != "" {
		res.Patches = append(
			res.Patches,
			types.Patch{
				Patch: fmt.Sprintf(
					`[ {"op": "add", "path": "/spec/template/spec/containers/0/args", "value": ["-log-level", "%s"]} ]`,
					r.LogLevel,
				),
				Target: &types.Selector{
					ResId: resid.ResId{
						Gvk: resid.Gvk{
							Kind: "Deployment",
						},
						Name: constants.ProvisioningAPIName,
					},
				},
			},
		)
	}

	return res, nil
}
