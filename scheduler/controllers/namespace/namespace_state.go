package namespace

import (
	"context"
	"fmt"
	"net/url"

	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/constants"
	"gitlab.com/gitlab-org/opstrace/opstrace/scheduler/api/v1alpha1"
	"gitlab.com/gitlab-org/opstrace/opstrace/scheduler/controllers/cluster"
	"gitlab.com/gitlab-org/opstrace/opstrace/scheduler/controllers/config"
	tenantOperator "gitlab.com/gitlab-org/opstrace/opstrace/tenant-operator/api/v1alpha1"
	"gitlab.com/gitlab-org/opstrace/opstrace/tenant-operator/controllers/group/jaeger"
	tenant "gitlab.com/gitlab-org/opstrace/opstrace/tenant-operator/controllers/tenant"
	v1 "k8s.io/api/apps/v1"
	corev1 "k8s.io/api/core/v1"
	"k8s.io/apimachinery/pkg/api/errors"
	"sigs.k8s.io/controller-runtime/pkg/client"
)

type SubsystemName string

const (
	JaegerSubsystemName        SubsystemName = "jaeger"
	ErrorTrackingSubsystemName SubsystemName = "errortracking"
)

type ClickhouseAssetsT struct {
	Jaeger struct {
		Credentials *corev1.Secret
		Quotas      *corev1.ConfigMap
	}
	ErrorTracking struct {
		Credentials *corev1.Secret
		Quotas      *corev1.ConfigMap
	}
}

type GitLabNamespaceState struct {
	Tenant *tenantOperator.Tenant
	Group  *tenantOperator.Group
	// Track all remaining groups in tenant so we
	// can tear down the tenant if no more groups exist
	TenantGroups *tenant.GroupsState
	Operator     *v1.Deployment
	Namespace    *corev1.Namespace

	Cluster *v1alpha1.Cluster

	ClickHouseAssets    *ClickhouseAssetsT
	clickHouseEndpoints *config.ClickHouseEndpoints
}

func NewGitLabNamespaceState() *GitLabNamespaceState {
	return &GitLabNamespaceState{
		ClickHouseAssets: &ClickhouseAssetsT{
			Jaeger: struct {
				Credentials *corev1.Secret
				Quotas      *corev1.ConfigMap
			}{},
			ErrorTracking: struct {
				Credentials *corev1.Secret
				Quotas      *corev1.ConfigMap
			}{},
		},
	}
}

// TenantEmpty returns true if no groups exist for the tenant
func (i *GitLabNamespaceState) TenantEmpty() bool {
	return i.TenantGroups.IsEmpty()
}

// Get the Clickhouse user and password
func (i *GitLabNamespaceState) ClickHouseUserInfo(subsystem SubsystemName) (*url.Userinfo, error) {
	if subsystem == "" {
		return nil, fmt.Errorf("must specify name of the subsystem, one of jaeger, errortracking")
	}

	var (
		s        *corev1.Secret
		user     string
		password string
	)

	switch subsystem {
	case JaegerSubsystemName:
		// parse jaeger specific credentials
		if i.ClickHouseAssets.Jaeger.Credentials == nil {
			return nil, fmt.Errorf("jaeger ClickHouseCredentials not set in GitLabNamespaceState")
		}
		s = i.ClickHouseAssets.Jaeger.Credentials
	case ErrorTrackingSubsystemName:
		// parse errortracking specific credentials
		if i.ClickHouseAssets.ErrorTracking.Credentials == nil {
			return nil, fmt.Errorf("errortracking ClickHouseCredentials not set in GitlabNamespaceState")
		}
		s = i.ClickHouseAssets.ErrorTracking.Credentials
	}

	if pwd, got := s.Data[constants.ClickHouseCredentialsPasswordKey]; got {
		password = string(pwd)
	} else {
		return nil, fmt.Errorf("%s ClickHouseCredentialsPasswordKey not set for ClickHouseUserInfo", subsystem)
	}
	if usr, got := s.Data[constants.ClickHouseCredentialsUserKey]; got {
		user = string(usr)
	} else {
		return nil, fmt.Errorf("%s ClickHouseCredentialsUserKey not set for ClickHouseUserInfo", subsystem)
	}

	return url.UserPassword(user, password), nil
}

func (i *GitLabNamespaceState) ClickHouseQuotasInfo(subsystem SubsystemName) (map[string]string, error) {
	if subsystem == "" {
		return nil, fmt.Errorf("must specify name of the subsystem, one of jaeger, errortracking")
	}

	var c *corev1.ConfigMap

	switch subsystem {
	case JaegerSubsystemName:
		// parse jaeger specific credentials
		if i.ClickHouseAssets.Jaeger.Quotas == nil {
			return nil, fmt.Errorf("jaeger clickhouse quotas not set in GitLabNamespaceState")
		}
		c = i.ClickHouseAssets.Jaeger.Quotas
	case ErrorTrackingSubsystemName:
		// parse errortracking specific credentials
		if i.ClickHouseAssets.ErrorTracking.Quotas == nil {
			return nil, fmt.Errorf("errortracking clickhouse quotas not set in GitlabNamespaceState")
		}
		c = i.ClickHouseAssets.ErrorTracking.Quotas
	}

	return c.Data, nil
}

func (i *GitLabNamespaceState) Read(
	ctx context.Context,
	cr *v1alpha1.GitLabNamespace,
	client client.Client,
) error {
	err := i.readClusterState(ctx, client)
	if err != nil {
		return err
	}

	err = i.readClickHouseCredentials(ctx, client)
	if err != nil {
		return err
	}

	err = i.readTenantState(ctx, cr, client)
	if err != nil {
		return err
	}

	if err = i.readJaegerClickHouseCredentials(ctx, cr, client); err != nil {
		return err
	}

	if err = i.readJaegerClickHouseQuotas(ctx, cr, client); err != nil {
		return err
	}

	err = i.readGroupState(ctx, cr, client)
	if err != nil {
		return err
	}

	err = i.readOperatorState(ctx, cr, client)
	if err != nil {
		return err
	}

	err = i.readNamespaceState(ctx, cr, client)
	if err != nil {
		return err
	}

	tenantGroupsState := tenant.NewGroupsState()
	i.TenantGroups = tenantGroupsState
	err = tenantGroupsState.Read(ctx, Tenant(i.GetClusterState(), cr), client)

	return err
}

func (i *GitLabNamespaceState) GetClusterState() *v1alpha1.Cluster {
	return i.Cluster.DeepCopy()
}

func (i *GitLabNamespaceState) readClusterState(ctx context.Context, client client.Client) error {
	// NOTE(prozlach): The GitlabNamespace objects should point to the cluster
	// object they belong to, but this is currently not the case. It is also
	// assumed that there is always going to be only a single Cluster CR
	// defined in the k8s API. Hence we just list the objects and assume there
	// is always going to be only one as a workaround.
	clusters := new(v1alpha1.ClusterList)
	err := client.List(ctx, clusters)
	if err != nil {
		return err
	}
	if len(clusters.Items) != 1 {
		return fmt.Errorf("Unexpected number of Cluster objects CRs found: %d, expecting 1", len(clusters.Items))
	}

	i.Cluster = &clusters.Items[0]

	return nil
}

func (i *GitLabNamespaceState) readClickHouseCredentials(ctx context.Context, client client.Client) error {
	clickHouseState := cluster.NewClickHouseState()
	if err := clickHouseState.ReadCredentials(ctx, i.GetClusterState(), client); err != nil {
		return err
	}

	var err error
	i.clickHouseEndpoints, err = clickHouseState.GetSelfhostedEndpoints()
	if err != nil {
		return err
	}
	return nil
}

func (i *GitLabNamespaceState) GetClickhouseEndpoints() *config.ClickHouseEndpoints {
	return i.clickHouseEndpoints.Copy()
}

func (i *GitLabNamespaceState) readJaegerClickHouseCredentials(
	ctx context.Context,
	cr *v1alpha1.GitLabNamespace,
	client client.Client,
) error {
	currentState := &corev1.Secret{}
	selector := jaeger.ClickHouseCredentialsSecretSelector(Group(i.GetClusterState(), cr))
	if err := client.Get(ctx, selector, currentState); err != nil {
		if errors.IsNotFound(err) {
			i.ClickHouseAssets.Jaeger.Credentials = nil
			return nil
		}
		return err
	}
	i.ClickHouseAssets.Jaeger.Credentials = currentState.DeepCopy()
	return nil
}

func (i *GitLabNamespaceState) readJaegerClickHouseQuotas(
	ctx context.Context,
	cr *v1alpha1.GitLabNamespace,
	client client.Client,
) error {
	currentState := &corev1.ConfigMap{}
	selector := jaeger.ClickHouseQuotasConfigMapSelector(Group(i.GetClusterState(), cr))
	if err := client.Get(ctx, selector, currentState); err != nil {
		if errors.IsNotFound(err) {
			i.ClickHouseAssets.Jaeger.Quotas = nil
			return nil
		}
		return err
	}
	i.ClickHouseAssets.Jaeger.Quotas = currentState.DeepCopy()
	return nil
}

func (i *GitLabNamespaceState) readTenantState(ctx context.Context, cr *v1alpha1.GitLabNamespace, client client.Client) error {
	currentState := &tenantOperator.Tenant{}
	selector := TenantSelector(cr)
	err := client.Get(ctx, selector, currentState)
	if err != nil {
		if errors.IsNotFound(err) {
			return nil
		}
		return err
	}
	i.Tenant = currentState.DeepCopy()
	return nil
}

func (i *GitLabNamespaceState) readGroupState(ctx context.Context, cr *v1alpha1.GitLabNamespace, client client.Client) error {
	currentState := &tenantOperator.Group{}
	selector := GroupSelector(cr)
	err := client.Get(ctx, selector, currentState)
	if err != nil {
		if errors.IsNotFound(err) {
			return nil
		}
		return err
	}
	i.Group = currentState.DeepCopy()
	return nil
}

func (i *GitLabNamespaceState) readOperatorState(ctx context.Context, cr *v1alpha1.GitLabNamespace, client client.Client) error {
	currentState := &v1.Deployment{}
	selector := DeploymentSelector(cr)
	err := client.Get(ctx, selector, currentState)
	if err != nil {
		if errors.IsNotFound(err) {
			return nil
		}
		return err
	}
	i.Operator = currentState.DeepCopy()
	return nil
}

func (i *GitLabNamespaceState) readNamespaceState(ctx context.Context, cr *v1alpha1.GitLabNamespace, client client.Client) error {
	currentState := &corev1.Namespace{}
	selector := NamespaceSelector(cr)
	err := client.Get(ctx, selector, currentState)
	if err != nil {
		if errors.IsNotFound(err) {
			return nil
		}
		return err
	}
	i.Namespace = currentState.DeepCopy()
	return nil
}
