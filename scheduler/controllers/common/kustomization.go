package common

import (
	"bytes"
	"embed"
	"encoding/json"
	"fmt"
	"io/fs"
	"strings"

	"github.com/fluxcd/pkg/ssa"
	"k8s.io/apimachinery/pkg/apis/meta/v1/unstructured"
	"sigs.k8s.io/controller-runtime/pkg/log"
	"sigs.k8s.io/kustomize/api/krusty"
	kustomize "sigs.k8s.io/kustomize/api/types"
	"sigs.k8s.io/kustomize/kyaml/filesys"
)

const TmpResourceData = "data.yml"

func embeddedFStoInMemFs(upstream embed.FS) (filesys.FileSystem, error) {
	res := filesys.MakeFsInMemory()

	// embed.FS and filesys.FileSystem interfaces are not compatible, hence we
	// copy everything over as krusty requires filesys.FileSystem

	err := fs.WalkDir(upstream, ".", func(path string, d fs.DirEntry, walkErr error) error {
		if walkErr != nil {
			return fmt.Errorf("encountered a walk error: %w", walkErr)
		}

		if d.IsDir() {
			//nolint:wrapcheck
			return res.Mkdir(path)
		}

		data, err := upstream.ReadFile(path)
		if err != nil {
			// Wut?
			return fmt.Errorf("failed to read path %s from embedded file system: %w", path, err)
		}

		if err := res.WriteFile(path, data); err != nil {
			return fmt.Errorf("failed to write path %s to the in-memory file system: %w", path, err)
		}
		log.Log.Info("manifest was read from embedded filesystem", "path", path)

		return nil
	})
	if err != nil {
		return nil, fmt.Errorf("unable to traverse embedded filesystem: %w", err)
	}

	return res, nil
}

// ParseManifests parses embedded manifests and then applies default
// kustomizations. The result is a map with keys being
// directories in the resulting filesystem, and values being byte blobs
// containing parsed manifests.
func InitialManifests(upstream embed.FS) (map[string]map[string][]byte, error) {
	manifestsFS, err := embeddedFStoInMemFs(upstream)
	if err != nil {
		return nil, fmt.Errorf("unable to merge overlay onto embedded manifests: %w", err)
	}
	log.Log.Info("Embedded manifests were uploaded into Memoryfs")

	// NOTE(prozlach): We assume that every directory we find is non-empty and
	// contains correct manifests, if not - krusty will fail. We can perform
	// extra checks during the mergeFSes call to handle it more gracefully
	dirs, err := manifestsFS.ReadDir(".")
	if err != nil {
		return nil, fmt.Errorf("unable to list directories in merged FS: %w", err)
	}

	res := make(map[string]map[string][]byte)
	opts := krusty.MakeDefaultOptions()
	kustomizer := krusty.MakeKustomizer(opts)

	for _, dir := range dirs {
		res[dir] = make(map[string][]byte)

		log.Log.Info("kustomizing initial manifests", "subcomponent", dir)
		resourceMap, err := kustomizer.Run(manifestsFS, dir)
		if err != nil {
			return nil, fmt.Errorf("kustomization apply of directory %q failed: %w", dir, err)
		}

		yamlResources, err := resourceMap.AsYaml()
		if err != nil {
			return nil, fmt.Errorf("failed to render resource map as YAML for dir %q: %w", dir, err)
		}

		res[dir][TmpResourceData] = yamlResources

		// Add any files which start with `delete_` prefix, as these are
		// patches used for conditional removal of manifests from the main blob
		files, err := manifestsFS.ReadDir(dir)
		if err != nil {
			return nil, fmt.Errorf("unable to list directories in merged FS' subdirectory %s: %w", dir, err)
		}

		for _, file := range files {
			if !strings.HasPrefix(file, "delete_") {
				continue
			}

			path := fmt.Sprintf("%s/%s", dir, file)
			b, err := manifestsFS.ReadFile(path)
			if err != nil {
				return nil, fmt.Errorf("unable load file from merged FS' path %s: %w", path, err)
			}

			res[dir][file] = b
		}
	}

	return res, nil
}

// ApplyKustomization applies kustomizations to given set of manifests and returns
// a resulting set of manifests parsed into unstructured.Unstructured.
func ApplyKustomization(
	manifests map[string][]byte,
	overrideKustomization *kustomize.Kustomization,
) ([]*unstructured.Unstructured, error) {
	opts := krusty.MakeDefaultOptions()
	kustomizer := krusty.MakeKustomizer(opts)

	tmpfs := filesys.MakeFsInMemory()

	data, err := json.Marshal(overrideKustomization)
	if err != nil {
		return nil, fmt.Errorf("unable to marshal kustomization to YAML: %w", err)
	}
	err = tmpfs.WriteFile("kustomization.yaml", data)
	if err != nil {
		return nil, fmt.Errorf("unable to write kustomization blob to in-memory FS: %w", err)
	}

	for filename, contents := range manifests {
		err = tmpfs.WriteFile(filename, contents)
		if err != nil {
			return nil, fmt.Errorf("unable to write manifest blob to in-memory FS for file %s: %w", filename, err)
		}
	}

	resourceMap, err := kustomizer.Run(tmpfs, ".")
	if err != nil {
		return nil, fmt.Errorf("kustomization apply failed: %w", err)
	}

	yamlResources, err := resourceMap.AsYaml()
	if err != nil {
		return nil, fmt.Errorf("failed to render resource as YAML: %w", err)
	}

	// Convert the build result into Kubernetes unstructured objects.
	objects, err := ssa.ReadObjects(bytes.NewReader(yamlResources))
	if err != nil {
		return nil, fmt.Errorf("failed to parse overridden objects into Unstructured")
	}

	return objects, nil
}
