package tenant

import (
	"fmt"
	"net/url"
	"strings"

	"github.com/go-logr/logr"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/common"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/constants"
	"gitlab.com/gitlab-org/opstrace/opstrace/scheduler/api/v1alpha1"
	opstracev1alpha1 "gitlab.com/gitlab-org/opstrace/opstrace/scheduler/api/v1alpha1"
	v1 "k8s.io/api/core/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	logf "sigs.k8s.io/controller-runtime/pkg/log"
)

type ClickHouseReconciler struct {
	Teardown bool
	Log      logr.Logger
	State    *GitLabObservabilityTenantState
}

func NewClickHouseReconciler(
	teardown bool,
	state *GitLabObservabilityTenantState,
	tenantID int64,
) *ClickHouseReconciler {
	return &ClickHouseReconciler{
		Teardown: teardown,
		Log: logf.Log.WithName(
			fmt.Sprintf("tenantmanifests/%s-%d", "clickhouse", tenantID),
		),
		State: state,
	}
}

// Note: Function length can be reduced after simplifying dual writing.
//
//nolint:funlen
func (i *ClickHouseReconciler) Reconcile(
	cr *opstracev1alpha1.GitLabObservabilityTenant,
) common.DesiredState {
	actions := []common.Action{}

	user := fmt.Sprintf("tenant_%d", cr.Spec.TopLevelNamespaceID)

	if i.Teardown {
		// teardown CH user
		actions = append(actions,
			common.ClickHouseAction{
				Msg:      fmt.Sprintf("clickhouse SQL drop user %s if exists", user),
				SQL:      fmt.Sprintf("DROP USER IF EXISTS %s ON CLUSTER '{cluster}'", user),
				URL:      i.State.getSchedulerCHCredentials().Native,
				Database: constants.TracingDatabaseName,
			},
		)
		// teardown secret
		if i.State.TenantCHCredentials != nil {
			actions = append(actions,
				common.GenericDeleteAction{
					Ref: i.State.TenantCHCredentials,
					Msg: "clickhouse credentials",
				},
			)
		}

		if i.State.TenantCHCloudCredentials != nil {
			actions = append(actions,
				common.GenericDeleteAction{
					Ref: i.State.TenantCHCloudCredentials,
					Msg: "clickhouse cloud credentials",
				},
			)
			//nolint:errcheck
			cred, _ := i.State.getCloudCHCredentials()
			actions = append(actions,
				common.ClickHouseAction{
					Msg:      fmt.Sprintf("clickhouse cloud SQL drop user %s if exists", user),
					SQL:      fmt.Sprintf("DROP USER IF EXISTS %s ", user),
					URL:      cred.Native,
					Database: constants.TracingDatabaseName,
				},
			)
		}
		return actions
	}

	if i.State.TenantCHCredentials == nil {
		// we've likely not provisioned tenant-specific CH credentials yet, lets
		credentialsData := getCredentialsData(cr)
		secret := &v1.Secret{}
		secret.ObjectMeta = metav1.ObjectMeta{
			Name:      constants.GitLabObservabilityTenantCHCredentialsSecretName,
			Namespace: cr.Namespace(),
		}
		secret.Data = credentialsData
		// provision secret
		actions = append(actions,
			common.GenericCreateOrUpdateAction{
				Ref: secret,
				Msg: "tenant clickhouse credentials",
				Mutator: func() error {
					return nil // don't mutate ever
				},
			},
		)
		// provision CH user & their grants
		actions = append(actions, ClickHouseUsersProvision(
			user,
			string(credentialsData[constants.ClickHouseCredentialsPasswordKey]),
			[]string{"SELECT", "INSERT"},
			[]string{constants.TracingDatabaseName, constants.MetricsDatabaseName, constants.LoggingDatabaseName},
			i.State.getSchedulerCHCredentials().Native,
		)...)
	}

	cloudDSNProvided := i.State.CloudCHCredentials != nil
	if cloudDSNProvided && i.State.TenantCHCloudCredentials == nil {
		cloudDSN, err := i.State.getCloudCHCredentials()
		if err != nil {
			actions = append(actions, common.LogAction{
				Msg:   "get clickhouse cloud credentials",
				Error: err,
			})
			return actions
		}

		// we've likely not provisioned tenant-specific Cloud CH credentials yet, lets
		credentialsData := getCloudCredentialsData(cr, cloudDSN.Native)
		secret := &v1.Secret{}
		secret.ObjectMeta = metav1.ObjectMeta{
			Name:      constants.GitLabObservabilityTenantCloudCHCredentialsSecretName,
			Namespace: cr.Namespace(),
		}
		secret.Data = credentialsData
		// provision secret
		actions = append(actions,
			common.GenericCreateOrUpdateAction{
				Ref: secret,
				Msg: "tenant clickhouse cloud credentials",
				Mutator: func() error {
					return nil // don't mutate ever
				},
			},
		)

		// provision CH user & their grants
		actions = append(actions, ClickHouseUsersProvisionOnCloud(
			user,
			string(credentialsData[constants.ClickHouseCredentialsPasswordKey]),
			[]string{"SELECT", "INSERT"},
			[]string{constants.TracingDatabaseName, constants.MetricsDatabaseName, constants.LoggingDatabaseName},
			cloudDSN.Native,
		)...)
	}

	return actions
}

func getCredentialsData(
	cr *v1alpha1.GitLabObservabilityTenant,
) map[string][]byte {
	user := fmt.Sprintf("tenant_%d", cr.Spec.TopLevelNamespaceID)
	pass := common.RandStringRunesRaw(10)
	hostname := fmt.Sprintf("%s.%s.svc.cluster.local", constants.ClickHouseClusterServiceName, "default")
	nativeEndpoint := url.URL{
		Host:   fmt.Sprintf("%s:9000", hostname),
		User:   url.UserPassword(user, pass),
		Scheme: "tcp",
	}
	httpEndpoint := url.URL{
		Host:   fmt.Sprintf("%s:8123", hostname),
		User:   url.UserPassword(user, pass),
		Scheme: "http",
	}

	return map[string][]byte{
		constants.ClickHouseCredentialsHTTPEndpointKey:   []byte(httpEndpoint.String()),
		constants.ClickHouseCredentialsNativeEndpointKey: []byte(nativeEndpoint.String()),
		constants.ClickHouseCredentialsPasswordKey:       []byte(pass),
	}
}

func ClickHouseUsersProvision(
	username string,
	password string,
	grants []string,
	databaseNames []string,
	chEndpoint url.URL,
) []common.Action {
	actions := []common.Action{}

	actions = append(actions,
		common.ClickHouseAction{
			Msg: fmt.Sprintf("clickhouse SQL create user %s if not exists with password [...]", username),
			//nolint:lll
			SQL: fmt.Sprintf("CREATE USER IF NOT EXISTS %s ON CLUSTER '{cluster}' IDENTIFIED WITH plaintext_password BY '%s' HOST ANY DEFAULT DATABASE default",
				username,
				password,
			),
			URL:      chEndpoint,
			Database: "default",
		},
		// REMOTE source privilege required for Distributed tables
		// This can't be specified on the database level
		common.ClickHouseAction{
			Msg: fmt.Sprintf("clickhouse SQL grant remote to user %s", username),
			SQL: fmt.Sprintf("GRANT ON CLUSTER '{cluster}' REMOTE ON *.* TO %s",
				username,
			),
			URL:      chEndpoint,
			Database: "default",
		},
	)

	// add grants as requested
	if len(grants) > 0 {
		grantsStr := strings.Join(grants, ",")
		for _, db := range databaseNames {
			actions = append(actions,
				common.ClickHouseAction{
					Msg: fmt.Sprintf("clickhouse SQL grant permissions to user %s", username),
					SQL: fmt.Sprintf("GRANT ON CLUSTER '{cluster}' %s ON %s.* TO %s",
						grantsStr,
						db,
						username,
					),
					URL:      chEndpoint,
					Database: "default",
				},
			)
		}
	}

	return actions
}

func ClickHouseUsersProvisionOnCloud(
	username string,
	password string,
	grants []string,
	databaseNames []string,
	chEndpoint url.URL) []common.Action {
	actions := []common.Action{}

	actions = append(actions,
		common.ClickHouseAction{
			Msg: fmt.Sprintf("clickhouse SQL create user %s if not exists with password [...]", username),
			//nolint:lll
			SQL: fmt.Sprintf("CREATE USER IF NOT EXISTS %s IDENTIFIED WITH sha256_password BY '%s' HOST ANY DEFAULT DATABASE default",
				username,
				password,
			),
			URL:      chEndpoint,
			Database: "default",
		},
		// REMOTE source privilege required for Distributed tables
		// This can't be specified on the database level
		common.ClickHouseAction{
			Msg: fmt.Sprintf("clickhouse SQL grant remote to user %s", username),
			SQL: fmt.Sprintf("GRANT REMOTE ON *.* TO %s",
				username,
			),
			URL:      chEndpoint,
			Database: "default",
		},
	)

	// add grants as requested
	if len(grants) > 0 {
		grantsStr := strings.Join(grants, ",")
		for _, db := range databaseNames {
			actions = append(actions,
				common.ClickHouseAction{
					Msg: fmt.Sprintf("clickhouse SQL grant permissions to user %s", username),
					SQL: fmt.Sprintf("GRANT %s ON %s.* TO %s",
						grantsStr,
						db,
						username,
					),
					URL:      chEndpoint,
					Database: "default",
				},
			)
		}
	}

	return actions
}

func getCloudCredentialsData(
	cr *v1alpha1.GitLabObservabilityTenant,
	cloudDSN url.URL,
) map[string][]byte {
	user := fmt.Sprintf("tenant_%d", cr.Spec.TopLevelNamespaceID)
	pass := common.ClickHouseCloudPassword(12)
	nativeEndpoint := cloudDSN
	nativeEndpoint.User = url.UserPassword(user, pass)
	nativeEndpoint = *nativeEndpoint.JoinPath(constants.TracingDatabaseName)

	httpEndpoint := cloudDSN
	httpEndpoint.User = url.UserPassword(user, pass)
	httpEndpoint.Host = fmt.Sprintf("%s:8443", cloudDSN.Hostname())
	httpEndpoint.Scheme = "https"
	httpEndpoint = *httpEndpoint.JoinPath(constants.TracingDatabaseName)

	return map[string][]byte{
		constants.ClickHouseCredentialsNativeEndpointKey: []byte(nativeEndpoint.String()),
		constants.ClickHouseCredentialsHTTPEndpointKey:   []byte(httpEndpoint.String()),
		constants.ClickHouseCredentialsPasswordKey:       []byte(pass),
	}
}
