package tenant

import (
	"fmt"
	"time"

	"github.com/fluxcd/kustomize-controller/api/v1beta2"
	"github.com/go-logr/logr"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/common"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/constants"
	"gitlab.com/gitlab-org/opstrace/opstrace/scheduler/api/v1alpha1"
	ctrlcommon "gitlab.com/gitlab-org/opstrace/opstrace/scheduler/controllers/common"
	logf "sigs.k8s.io/controller-runtime/pkg/log"
	"sigs.k8s.io/kustomize/api/types"
	kustomize "sigs.k8s.io/kustomize/api/types"
	"sigs.k8s.io/kustomize/kyaml/resid"
)

const (
	IngressBasePath = `/v3/%d/{projectID:[0-9]+}/ingest`
	// our original paths /ingest/<type> remain for consistency
	IngressPathTracesTmpl  = IngressBasePath + "/traces"
	IngressPathMetricsTmpl = IngressBasePath + "/metrics"
	IngressPathLogsTmpl    = IngressBasePath + "/logs"
	// OTEL compatible paths that are ingest/v1/<type> compatible
	IngressPathTracesOTELTmpl  = IngressBasePath + "/v1/traces"
	IngressPathMetricsOTELTmpl = IngressBasePath + "/v1/metrics"
	IngressPathLogsOTELTmpl    = IngressBasePath + "/v1/logs"
)

type OTELCollectorReconciler struct {
	Teardown         bool
	Log              logr.Logger
	initialManifests map[string][]byte
	State            *GitLabObservabilityTenantState
}

func NewOTELCollectorReconciler(
	initialManifests map[string][]byte,
	teardown bool,
	tenantID int64,
	state *GitLabObservabilityTenantState,
) *OTELCollectorReconciler {
	return &OTELCollectorReconciler{
		Teardown: teardown,
		Log: logf.Log.WithName(
			fmt.Sprintf("tenantmanifests/%s-%d", constants.OtelCollectorComponentName, tenantID),
		),
		initialManifests: initialManifests,
		State:            state,
	}
}

func (i *OTELCollectorReconciler) Reconcile(cr *v1alpha1.GitLabObservabilityTenant) common.DesiredState {
	if i.Teardown {
		// We are cleaning up, we'll just use the inventory to enlist things
		// to clean up
		return common.DesiredState{
			common.ManifestsPruneAction{
				ComponentName: constants.OtelCollectorComponentName,
				GetInventory: func() *v1beta2.ResourceInventory {
					return cr.Status.Inventory[constants.OtelCollectorInventoryID]
				},
				Msg: "[tenantmanifests/otelcollector] prune",
				Log: i.Log,
			},
		}
	}

	if i.State.TenantNamespace == nil {
		return common.DesiredState{
			common.LogAction{
				Msg:   "[tenantmanifests/otelcollector] failed to read tenant namespace, will retry",
				Error: fmt.Errorf("tenant namespace not available yet"),
			},
		}
	}

	if i.State.getTenantCHCredentials() == nil {
		return common.DesiredState{
			common.LogAction{
				Msg:   "[tenantmanifests/otelcollector] failed to read tenant CH credentials, will retry",
				Error: fmt.Errorf("tenant CH credentials not available yet"),
			},
		}
	}

	cloudDSNProvided := i.State.CloudCHCredentials != nil
	if cloudDSNProvided && i.State.getTenantCHCloudCredentials() == nil {
		return common.DesiredState{
			common.LogAction{
				Msg:   "[tenantmanifests/otelcollector] failed to read tenant cloud CH credentials, will retry",
				Error: fmt.Errorf("tenant cloud CH credentials not available yet"),
			},
		}
	}

	overrideKustomization, err := i.ApplyConfiguration(cr)
	if err != nil {
		return common.DesiredState{
			common.LogAction{
				Msg:   "[tenantmanifests/otelcollector] failed to apply configuration to manifests",
				Error: err,
			},
		}
	}
	i.ApplyOverrides(overrideKustomization, cr)

	manifests, err := ctrlcommon.ApplyKustomization(i.initialManifests, overrideKustomization)
	if err != nil {
		return common.DesiredState{
			common.LogAction{
				Msg:   "[tenantmanifests/otelcollector] failed to apply configuration to manifests",
				Error: err,
			},
		}
	}

	return common.DesiredState{
		common.ManifestsEnsureAction{
			HealthcheckTimeout: 2 * time.Minute,
			Manifests:          manifests,
			ComponentName:      constants.OtelCollectorComponentName,
			GetInventory: func() *v1beta2.ResourceInventory {
				return cr.Status.Inventory[constants.OtelCollectorInventoryID]
			},
			SetInventory: func(inv *v1beta2.ResourceInventory) {
				cr.Status.Inventory[constants.OtelCollectorInventoryID] = inv
			},
			Msg: "[tenantmanifests/otelcollector] ensure",
			Log: i.Log,
		},
	}
}

//nolint:unparam,funlen
func (i *OTELCollectorReconciler) ApplyConfiguration(
	cr *v1alpha1.GitLabObservabilityTenant,
) (*kustomize.Kustomization, error) {
	res := &kustomize.Kustomization{
		TypeMeta: kustomize.TypeMeta{
			Kind:       kustomize.KustomizationKind,
			APIVersion: kustomize.KustomizationVersion,
		},
		Namespace: cr.Namespace(),
		Resources: []string{
			ctrlcommon.TmpResourceData,
		},
	}

	res.Images = []kustomize.Image{
		{
			Name:    "otel-collector-image-placeholder",
			NewName: constants.DockerImageName(constants.OtelCollectorComponentName),
			NewTag:  constants.DockerImageTag,
		},
	}

	patches := []types.Patch{}

	// deployment
	// we set CH credentials & tenant ID here explicitly as CLI args to avoid them
	// from being overridden from other collector configs whenever applicable
	patches = append(patches, types.Patch{
		Target: deploymentSelector(),
		Patch: fmt.Sprintf(
			`[
				{"op": "add", "path": "/spec/template/spec/containers/0/args/-", "value": "%s"}
			]`,
			fmt.Sprintf(
				"--set=exporters.gitlabobservability.tenant_id=%d",
				cr.Spec.TopLevelNamespaceID,
			),
		),
	})
	if i.State.getTenantCHCredentials() != nil {
		patches = append(patches, types.Patch{
			Target: deploymentSelector(),
			Patch: fmt.Sprintf(
				`[
					{"op": "add", "path": "/spec/template/spec/containers/0/args/-", "value": "%s"}
				]`,
				fmt.Sprintf(
					"--set=exporters.gitlabobservability.clickhouse_dsn=%s/tracing",
					&i.State.getTenantCHCredentials().Native,
				),
			),
		})
	}

	cloudDSNProvided := i.State.CloudCHCredentials != nil
	if cloudDSNProvided && i.State.getTenantCHCloudCredentials() != nil {
		patches = append(patches, types.Patch{
			Target: deploymentSelector(),
			Patch: fmt.Sprintf(
				`[
					{"op": "add", "path": "/spec/template/spec/containers/0/args/-", "value": "%s"}
				]`,
				fmt.Sprintf(
					"--set=exporters.gitlabobservability.clickhouse_cloud_dsn=%s",
					&i.State.getTenantCHCloudCredentials().Native,
				),
			),
		})
	}

	// ingress
	if i.State.Cluster != nil {
		host := i.State.Cluster.Spec.GetHost()
		// setup ingress auth handling urls
		// e.g. http://GATEKEEPER_ENDPOINT/v1/auth/both/webhook/GROUP_ID/PROJECT_ID/write?min_accesslevel=0
		patches = append(patches,
			types.Patch{
				Target: tracesIngressRouteSelector(),
				Patch: fmt.Sprintf(
					`[
					{"op": "replace", "path": "/spec/routes/0/match", "value": "Host(\"%s\") && Path(\"%s\", \"%s\")" },
					{"op": "replace", "path": "/spec/routes/0/services/0/namespace", "value": "%s" },
					{"op": "replace", "path": "/spec/routes/0/middlewares/0/namespace", "value": "%s" },
					{"op": "replace", "path": "/spec/routes/0/middlewares/1/namespace", "value": "%s" },
					{"op": "replace", "path": "/spec/routes/0/middlewares/2/namespace", "value": "%s" },
					{"op": "replace", "path": "/spec/routes/0/middlewares/3/namespace", "value": "%s" },
					{"op": "replace", "path": "/spec/routes/0/middlewares/4/namespace", "value": "%s" },
					{"op": "replace", "path": "/spec/routes/0/middlewares/5/namespace", "value": "%s" }
				]`,
					host,
					fmt.Sprintf(IngressPathTracesTmpl, cr.Spec.TopLevelNamespaceID),
					fmt.Sprintf(IngressPathTracesOTELTmpl, cr.Spec.TopLevelNamespaceID),
					cr.Namespace(),
					i.State.Cluster.Namespace(),
					i.State.Cluster.Namespace(),
					i.State.Cluster.Namespace(),
					i.State.Cluster.Namespace(),
					i.State.Cluster.Namespace(),
					i.State.Cluster.Namespace(),
				),
			},
			types.Patch{
				Target: metricsIngressRouteSelector(),
				Patch: fmt.Sprintf(
					`[
					{"op": "replace", "path": "/spec/routes/0/match", "value": "Host(\"%s\") && Path(\"%s\", \"%s\")" },
					{"op": "replace", "path": "/spec/routes/0/services/0/namespace", "value": "%s" },
					{"op": "replace", "path": "/spec/routes/0/middlewares/0/namespace", "value": "%s" },
					{"op": "replace", "path": "/spec/routes/0/middlewares/1/namespace", "value": "%s" },
					{"op": "replace", "path": "/spec/routes/0/middlewares/2/namespace", "value": "%s" },
					{"op": "replace", "path": "/spec/routes/0/middlewares/3/namespace", "value": "%s" },
					{"op": "replace", "path": "/spec/routes/0/middlewares/4/namespace", "value": "%s" },
					{"op": "replace", "path": "/spec/routes/0/middlewares/5/namespace", "value": "%s" }
				]`,
					host,
					fmt.Sprintf(IngressPathMetricsTmpl, cr.Spec.TopLevelNamespaceID),
					fmt.Sprintf(IngressPathMetricsOTELTmpl, cr.Spec.TopLevelNamespaceID),
					cr.Namespace(),
					i.State.Cluster.Namespace(),
					i.State.Cluster.Namespace(),
					i.State.Cluster.Namespace(),
					i.State.Cluster.Namespace(),
					i.State.Cluster.Namespace(),
					i.State.Cluster.Namespace(),
				),
			},
			types.Patch{
				Target: logsIngressRouteSelector(),
				Patch: fmt.Sprintf(
					`[
					{"op": "replace", "path": "/spec/routes/0/match", "value": "Host(\"%s\") && Path(\"%s\", \"%s\")" },
					{"op": "replace", "path": "/spec/routes/0/services/0/namespace", "value": "%s" },
					{"op": "replace", "path": "/spec/routes/0/middlewares/0/namespace", "value": "%s" },
					{"op": "replace", "path": "/spec/routes/0/middlewares/1/namespace", "value": "%s" },
					{"op": "replace", "path": "/spec/routes/0/middlewares/2/namespace", "value": "%s" },
					{"op": "replace", "path": "/spec/routes/0/middlewares/3/namespace", "value": "%s" },
					{"op": "replace", "path": "/spec/routes/0/middlewares/4/namespace", "value": "%s" },
					{"op": "replace", "path": "/spec/routes/0/middlewares/5/namespace", "value": "%s" }
				]`,
					host,
					fmt.Sprintf(IngressPathLogsTmpl, cr.Spec.TopLevelNamespaceID),
					fmt.Sprintf(IngressPathLogsOTELTmpl, cr.Spec.TopLevelNamespaceID),
					cr.Namespace(),
					i.State.Cluster.Namespace(),
					i.State.Cluster.Namespace(),
					i.State.Cluster.Namespace(),
					i.State.Cluster.Namespace(),
					i.State.Cluster.Namespace(),
					i.State.Cluster.Namespace(),
				),
			},
		)
	}

	// podmonitor
	patches = append(patches, types.Patch{
		Target: podmonitorSelector(),
		Patch: fmt.Sprintf(
			`[
				{"op": "replace", "path": "/spec/namespaceSelector", "value": { "matchNames": ["%s"] }}
			]`,
			cr.Namespace(),
		),
	})
	res.Patches = patches

	return res, nil
}

func (i *OTELCollectorReconciler) ApplyOverrides(k *kustomize.Kustomization, cr *v1alpha1.GitLabObservabilityTenant) {
	if cr.Spec.Overrides.OTELCollector == nil {
		return
	}

	k.CommonLabels = cr.Spec.Overrides.OTELCollector.CommonLabels
	k.Labels = cr.Spec.Overrides.OTELCollector.Labels
	k.CommonAnnotations = cr.Spec.Overrides.OTELCollector.CommonAnnotations
	k.Patches = append(k.Patches, cr.Spec.Overrides.OTELCollector.Patches...)
	k.Images = cr.Spec.Overrides.OTELCollector.Images
	k.Replicas = cr.Spec.Overrides.OTELCollector.Replicas
}

func deploymentSelector() *kustomize.Selector {
	return &types.Selector{
		ResId: resid.ResId{
			Gvk: resid.Gvk{
				Kind: "Deployment",
			},
			Name: constants.OtelCollectorComponentName,
		},
	}
}

func podmonitorSelector() *kustomize.Selector {
	return &types.Selector{
		ResId: resid.ResId{
			Gvk: resid.Gvk{
				Kind: "PodMonitor",
			},
			Name: constants.OtelCollectorComponentName,
		},
	}
}

func tracesIngressRouteSelector() *kustomize.Selector {
	return &types.Selector{
		ResId: resid.ResId{
			Gvk: resid.Gvk{
				Kind: "IngressRoute",
			},
			Name: constants.OtelCollectorComponentTraces,
		},
	}
}

func metricsIngressRouteSelector() *kustomize.Selector {
	return &types.Selector{
		ResId: resid.ResId{
			Gvk: resid.Gvk{
				Kind: "IngressRoute",
			},
			Name: constants.OtelCollectorComponentMetrics,
		},
	}
}

func logsIngressRouteSelector() *kustomize.Selector {
	return &types.Selector{
		ResId: resid.ResId{
			Gvk: resid.Gvk{
				Kind: "IngressRoute",
			},
			Name: constants.OtelCollectorComponentLogs,
		},
	}
}
