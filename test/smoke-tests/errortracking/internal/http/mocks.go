package http

import (
	"io"
	"net/http"

	"github.com/stretchr/testify/mock"
)

type MockClient struct {
	mock.Mock
	RequestInterface
}

func (m *MockClient) Do(req *http.Request) (*http.Response, error) {
	args := m.Called(req)
	//nolint:wrapcheck
	return args.Get(0).(*http.Response), args.Error(1)
}

func (m *MockClient) CreateRequest(method, url string, body io.Reader) (*http.Request, error) {
	args := m.Called(method, url, body)
	//nolint:wrapcheck
	return args.Get(0).(*http.Request), args.Error(1)
}

func (m *MockClient) Get(url string) (*http.Response, error) {
	args := m.Called(url)
	//nolint:wrapcheck
	return args.Get(0).(*http.Response), args.Error(1)
}
