include ../../../Makefile.go.mk

DOCKER_IMAGE_NAME := smoke-test-errortracking

# The following are env vars needed by the error-tracking smoke-test application
export  GROUP_ERROR_TRACKING_ENDPOINT=https://observe.gitlab.com/v2/errortracking/14485840/projects/32149347/errors
# ERROR_TRACKING_INGESTION_DELAY_SEC represents the delay that we should have between sending and reading the same error. We choose a value that
# will give us confidence that the error should already be ingested.
export  ERROR_TRACKING_INGESTION_DELAY_SEC=10
# TESTS_PERIOD_SEC the time between two consequtive smoke tests for error tracking
export  TESTS_PERIOD_SEC=10
# RETRIES the number of retries before exiting the smoketests
export  RETRIES=5

export GO_BUILD_LDFLAGS = \
	-X main.release=${DOCKER_IMAGE_TAG} \

.PHONT: clean
clean:
	rm -f smoketest

.PHONY: docker-ensure
docker-ensure: docker-ensure-default

.PHONY: docker-build
docker-build:
	docker build \
		--build-arg GO_BUILD_LDFLAGS \
		-f Dockerfile \
		-t ${DOCKER_IMAGE} .

.PHONY: docker-push
docker-push:
	docker push ${DOCKER_IMAGE}

.PHONY: docker
docker: docker-build docker-push

.PHONY: docker-run
docker-run:
	docker run  -e GRAFANA_API_TOKEN=${GRAFANA_API_TOKEN} \
				-e SENTRY_DSN=${SENTRY_DSN} \
				-e GROUP_ERROR_TRACKING_ENDPOINT=${GROUP_ERROR_TRACKING_ENDPOINT} \
				-e ERROR_TRACKING_INGESTION_DELAY_SEC=${ERROR_TRACKING_INGESTION_DELAY_SEC} \
				-e TESTS_PERIOD_SEC=${TESTS_PERIOD_SEC} \
				-e RETRIES=${RETRIES} \
				-p 2112:2112 \
			    ${DOCKER_IMAGE}


.PHONY: build
build:
	go build -o smoketest ./cmd

.PHONY: run
run:
	go run ./cmd

.PHONY: unit-tests
unit-tests:
	go test -v ./...

.PHONY: regenerate
regenerate: #noop

.PHONY: print-docker-images
print-docker-images: print-docker-image-name-tag
