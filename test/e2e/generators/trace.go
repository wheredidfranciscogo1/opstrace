package main

import (
	"bytes"
	"crypto/tls"
	"flag"
	"fmt"
	"html/template"
	"net/http"
	"strconv"
	"strings"
	"time"
)

type traceData struct {
	TraceID string
	// root span service name
	ServiceName string
	// trace start
	Start int64
	// span start delta from start
	Span1Start  int64
	Span1End    int64
	Span2Start  int64
	Span2End    int64
	Span3Start  int64
	Span3End    int64
	Span4Start  int64
	Span4End    int64
	Span5Start  int64
	Span5End    int64
	Span6Start  int64
	Span6End    int64
	Span7Start  int64
	Span7End    int64
	Span8Start  int64
	Span8End    int64
	Span9Start  int64
	Span9End    int64
	Span10Start int64
	Span10End   int64
}

type traceRequest struct {
	ID          string
	start       time.Time
	duration    int64
	serviceName string
}

// Sends traces to ingest API with configurable
// start time and duration. This can be used in
// GoLang tests or Cypress tests
//
// Traces are represented by args of the form:
//
//	traceId:startTimeUnix:durationNano:serviceName
//
// traceId: UUID string
// startTimeUnix: Unix timestamp in seconds
// durationNano: duration in nano seconds
// serviceName: serviceName for root span/trace. ServiceName can
// be used to scope traces for specific tests
//
// Example:
//
//	go run trace.go \
//		-url https://gob.local/v3/22/1/traces/ingest \
//		-token gl-pat-token \
//		"8adaf418-326b-c34c-0695-32bc607cfa07:1699039700:400000000:foo" \
//		"9adaf418-326b-c34c-0695-32bc607cfa05:1699039701:500000000":bar \
//		"1adaf418-326b-c34c-0695-32bc607cfa02:1699039702:400397312:baz"
func main() {
	token := flag.String("token", "", "GitLab access token")
	u := flag.String("endpoint", "", "GOB endpoint for posting traces to")
	flag.Parse()
	if *token == "" {
		panic("-token required")
	}
	if *u == "" {
		panic("-endpoint required")
	}
	args := flag.Args()
	var reqs = make([]traceRequest, len(args))

	for i, arg := range args {
		parts := strings.Split(arg, ":")
		if len(parts) != 4 {
			panic("arg must be of form traceId:startTimeUnix:durationNano:serviceName")
		}
		t, err := strconv.ParseInt(parts[1], 10, 64)
		if err != nil {
			panic(err)
		}
		d, err := strconv.ParseInt(parts[2], 10, 64)
		if err != nil {
			panic(err)
		}
		reqs[i] = traceRequest{
			ID:          parts[0],
			start:       time.Unix(t, 0),
			duration:    d,
			serviceName: parts[3],
		}
	}

	for _, req := range reqs {
		err := sendTrace(req, *u, *token)
		if err != nil {
			panic(err)
		}
	}
}

var httpClient = http.Client{Transport: &http.Transport{
	// #nosec
	TLSClientConfig: &tls.Config{InsecureSkipVerify: true},
}}

func sendTrace(req traceRequest, endpoint, token string) error {
	t, err := generateTrace(req)
	if err != nil {
		return fmt.Errorf("generateTrace: %w", err)
	}
	r, err := http.NewRequest("POST", endpoint, strings.NewReader(t))
	if err != nil {
		return fmt.Errorf("sendTrace http.NewRequest: %w", err)
	}
	r.Header.Set("Content-Type", "application/json")
	r.Header.Set("Private-Token", token)
	resp, err := httpClient.Do(r)
	if err != nil {
		return fmt.Errorf("sendTrace httpClient.Do: %w", err)
	}
	defer resp.Body.Close()
	if resp.StatusCode < 300 && resp.StatusCode > 199 {
		return nil
	} else {
		return fmt.Errorf("http statusCode for sendTrace: %d", resp.StatusCode)
	}
}

func generateTrace(req traceRequest) (string, error) {
	if req.duration < 400000000 {
		return "", fmt.Errorf(
			"req.duration must be greater than 400000000, provided value is: %d",
			req.duration,
		)
	}
	// strip "-" or API won't accept it
	req.ID = strings.ReplaceAll(req.ID, "-", "")

	tmpl, err := template.New("trace").Parse(traceTemplate)
	if err != nil {
		return "", fmt.Errorf("template parse: %w", err)
	}

	s := req.start.UnixNano()

	details := traceData{
		TraceID:     req.ID,
		ServiceName: req.serviceName,
		Start:       s,
		// root span starts at start, so delta is zero
		Span1Start: s + 0,
		// model span that starts before root span to
		// test root span ordering is correct
		Span2Start:  s + -41726976,
		Span3Start:  s + 8920832,
		Span4Start:  s + 0,
		Span5Start:  s + 6911232,
		Span6Start:  s + 9422592,
		Span7Start:  s + 16088832,
		Span8Start:  s + 29270784,
		Span9Start:  s + 33990912,
		Span10Start: s + 47637248,

		Span1End:  s + req.duration,
		Span2End:  s + 304039168,
		Span3End:  s + 400397312,
		Span4End:  s + 242758144,
		Span5End:  s + 290819840,
		Span6End:  s + 245012224,
		Span7End:  s + 104359168,
		Span8End:  s + 230762752,
		Span9End:  s + 212003840,
		Span10End: s + 184039168,
	}

	var b bytes.Buffer
	err = tmpl.Execute(&b, details)

	return b.String(), err
}

var traceTemplate = `
{
	"resourceSpans": [
	  {
		"resource": {
		  "attributes": [
			{
			  "key": "k6",
			  "value": {
				"stringValue": "true"
			  }
			},
			{
			  "key": "service.name",
			  "value": {
				"stringValue": "{{.ServiceName}}"
			  }
			}
		  ],
		  "droppedAttributesCount": 0
		},
		"scopeSpans": [
		  {
			"spans": [
			  {
				"traceId": "{{.TraceID}}",
				"spanId": "0b6556502c8cd0d9",
				"parentSpanId": "",
				"traceState": "",
				"name": "article-to-cart",
				"kind": "SPAN_KIND_SERVER",
				"startTimeUnixNano": {{.Span1Start}},
				"endTimeUnixNano": {{.Span1End}},
				"attributes": [
				  {
					"key": "net.transport",
					"value": {
					  "stringValue": "ip_tcp"
					}
				  },
				  {
					"key": "net.sock.family",
					"value": {
					  "stringValue": "inet"
					}
				  },
				  {
					"key": "net.sock.host.addr",
					"value": {
					  "stringValue": "192.168.147.72"
					}
				  },
				  {
					"key": "net.host.name",
					"value": {
					  "stringValue": "shop-backend.local"
					}
				  },
				  {
					"key": "net.host.port",
					"value": {
					  "intValue": 8888
					}
				  },
				  {
					"key": "http.flavor",
					"value": {
					  "stringValue": "1.1"
					}
				  },
				  {
					"key": "http.scheme",
					"value": {
					  "stringValue": "https"
					}
				  },
				  {
					"key": "http.target",
					"value": {
					  "stringValue": "/article-to-cart"
					}
				  },
				  {
					"key": "http.response_content_length",
					"value": {
					  "intValue": 292929
					}
				  },
				  {
					"key": "http.method",
					"value": {
					  "stringValue": "GET"
					}
				  },
				  {
					"key": "http.url",
					"value": {
					  "stringValue": "https://shop-backend.local:8888/article-to-cart"
					}
				  },
				  {
					"key": "http.status_code",
					"value": {
					  "intValue": 201
					}
				  }
				],
				"droppedAttributesCount": 0,
				"droppedEventsCount": 0,
				"droppedLinksCount": 0,
				"status": {
				  "code": 0,
				  "message": ""
				}
			  }
			],
			"scope": {
			  "name": "k6",
			  "version": ""
			}
		  }
		]
	  },
	  {
		"resource": {
		  "attributes": [
			{
			  "key": "k6",
			  "value": {
				"stringValue": "true"
			  }
			},
			{
			  "key": "service.name",
			  "value": {
				"stringValue": "shop-backend"
			  }
			}
		  ],
		  "droppedAttributesCount": 0
		},
		"scopeSpans": [
		  {
			"spans": [
			  {
				"traceId": "{{.TraceID}}",
				"spanId": "ec5a65d86a5d499e",
				"parentSpanId": "0b6556502c8cd0d9",
				"traceState": "",
				"name": "place-articles",
				"kind": "SPAN_KIND_CLIENT",
				"startTimeUnixNano": {{.Span2Start}},
				"endTimeUnixNano": {{.Span2End}},
				"attributes": [
				  {
					"key": "net.transport",
					"value": {
					  "stringValue": "ip_tcp"
					}
				  },
				  {
					"key": "net.sock.family",
					"value": {
					  "stringValue": "inet"
					}
				  },
				  {
					"key": "net.peer.port",
					"value": {
					  "intValue": 8755
					}
				  },
				  {
					"key": "http.flavor",
					"value": {
					  "stringValue": "1.1"
					}
				  },
				  {
					"key": "net.sock.peer.addr",
					"value": {
					  "stringValue": "192.168.231.63"
					}
				  },
				  {
					"key": "net.peer.name",
					"value": {
					  "stringValue": "cart-service.local"
					}
				  },
				  {
					"key": "http.response_content_length",
					"value": {
					  "intValue": 988945
					}
				  },
				  {
					"key": "http.method",
					"value": {
					  "stringValue": "PUT"
					}
				  },
				  {
					"key": "http.url",
					"value": {
					  "stringValue": "https://cart-service.local:8974/place-articles"
					}
				  },
				  {
					"key": "http.status_code",
					"value": {
					  "intValue": 201
					}
				  }
				],
				"droppedAttributesCount": 0,
				"droppedEventsCount": 0,
				"droppedLinksCount": 0,
				"status": {
				  "code": 0,
				  "message": ""
				}
			  },
			  {
				"traceId": "{{.TraceID}}",
				"spanId": "b858b31f0961edf0",
				"parentSpanId": "0b6556502c8cd0d9",
				"traceState": "",
				"name": "authenticate",
				"kind": "SPAN_KIND_CLIENT",
				"startTimeUnixNano": {{.Span3Start}},
				"endTimeUnixNano": {{.Span3End}},
				"attributes": [
				  {
					"key": "net.transport",
					"value": {
					  "stringValue": "ip_tcp"
					}
				  },
				  {
					"key": "net.sock.family",
					"value": {
					  "stringValue": "inet"
					}
				  },
				  {
					"key": "net.peer.port",
					"value": {
					  "intValue": 8010
					}
				  },
				  {
					"key": "http.flavor",
					"value": {
					  "stringValue": "1.1"
					}
				  },
				  {
					"key": "net.sock.peer.addr",
					"value": {
					  "stringValue": "192.168.249.243"
					}
				  },
				  {
					"key": "net.peer.name",
					"value": {
					  "stringValue": "auth-service.local"
					}
				  },
				  {
					"key": "http.response_content_length",
					"value": {
					  "intValue": 217122
					}
				  },
				  {
					"key": "http.method",
					"value": {
					  "stringValue": "PATCH"
					}
				  },
				  {
					"key": "http.url",
					"value": {
					  "stringValue": "https://auth-service.local:8409/authenticate"
					}
				  },
				  {
					"key": "http.status_code",
					"value": {
					  "intValue": 200
					}
				  }
				],
				"droppedAttributesCount": 0,
				"droppedEventsCount": 0,
				"droppedLinksCount": 0,
				"status": {
				  "code": 0,
				  "message": ""
				}
			  },
			  {
				"traceId": "{{.TraceID}}",
				"spanId": "77fecd69d987a3fc",
				"parentSpanId": "0b6556502c8cd0d9",
				"traceState": "",
				"name": "get-article",
				"kind": "SPAN_KIND_CLIENT",
				"startTimeUnixNano": {{.Span4Start}},
				"endTimeUnixNano": {{.Span4End}},
				"attributes": [
				  {
					"key": "net.transport",
					"value": {
					  "stringValue": "ip_tcp"
					}
				  },
				  {
					"key": "net.sock.family",
					"value": {
					  "stringValue": "inet"
					}
				  },
				  {
					"key": "net.peer.port",
					"value": {
					  "intValue": 8959
					}
				  },
				  {
					"key": "http.flavor",
					"value": {
					  "stringValue": "1.1"
					}
				  },
				  {
					"key": "net.sock.peer.addr",
					"value": {
					  "stringValue": "192.168.58.234"
					}
				  },
				  {
					"key": "net.peer.name",
					"value": {
					  "stringValue": "article-service.local"
					}
				  },
				  {
					"key": "http.response_content_length",
					"value": {
					  "intValue": 151230
					}
				  },
				  {
					"key": "http.method",
					"value": {
					  "stringValue": "GET"
					}
				  },
				  {
					"key": "http.url",
					"value": {
					  "stringValue": "https://article-service.local:8340/get-article"
					}
				  },
				  {
					"key": "http.status_code",
					"value": {
					  "intValue": 204
					}
				  }
				],
				"droppedAttributesCount": 0,
				"droppedEventsCount": 0,
				"droppedLinksCount": 0,
				"status": {
				  "code": 0,
				  "message": ""
				}
			  }
			],
			"scope": {
			  "name": "k6",
			  "version": ""
			}
		  }
		]
	  },
	  {
		"resource": {
		  "attributes": [
			{
			  "key": "k6",
			  "value": {
				"stringValue": "true"
			  }
			},
			{
			  "key": "service.name",
			  "value": {
				"stringValue": "auth-service"
			  }
			}
		  ],
		  "droppedAttributesCount": 0
		},
		"scopeSpans": [
		  {
			"spans": [
			  {
				"traceId": "{{.TraceID}}",
				"spanId": "3087334e85583cb2",
				"parentSpanId": "b858b31f0961edf0",
				"traceState": "",
				"name": "authenticate",
				"kind": "SPAN_KIND_SERVER",
				"startTimeUnixNano": {{.Span5Start}},
				"endTimeUnixNano": {{.Span5End}},
				"attributes": [
				  {
					"key": "net.transport",
					"value": {
					  "stringValue": "ip_tcp"
					}
				  },
				  {
					"key": "net.sock.family",
					"value": {
					  "stringValue": "inet"
					}
				  },
				  {
					"key": "net.sock.host.addr",
					"value": {
					  "stringValue": "192.168.249.243"
					}
				  },
				  {
					"key": "net.host.name",
					"value": {
					  "stringValue": "auth-service.local"
					}
				  },
				  {
					"key": "net.host.port",
					"value": {
					  "intValue": 8409
					}
				  },
				  {
					"key": "http.flavor",
					"value": {
					  "stringValue": "1.1"
					}
				  },
				  {
					"key": "http.scheme",
					"value": {
					  "stringValue": "https"
					}
				  },
				  {
					"key": "http.target",
					"value": {
					  "stringValue": "/authenticate"
					}
				  },
				  {
					"key": "http.response_content_length",
					"value": {
					  "intValue": 217122
					}
				  },
				  {
					"key": "http.request_content_length",
					"value": {
					  "intValue": 41863
					}
				  },
				  {
					"key": "http.method",
					"value": {
					  "stringValue": "PATCH"
					}
				  },
				  {
					"key": "http.url",
					"value": {
					  "stringValue": "https://auth-service.local:8409/authenticate"
					}
				  },
				  {
					"key": "http.status_code",
					"value": {
					  "intValue": 200
					}
				  }
				],
				"droppedAttributesCount": 0,
				"droppedEventsCount": 0,
				"droppedLinksCount": 0,
				"status": {
				  "code": 0,
				  "message": ""
				}
			  }
			],
			"scope": {
			  "name": "k6",
			  "version": ""
			}
		  }
		]
	  },
	  {
		"resource": {
		  "attributes": [
			{
			  "key": "k6",
			  "value": {
				"stringValue": "true"
			  }
			},
			{
			  "key": "service.name",
			  "value": {
				"stringValue": "cart-service"
			  }
			}
		  ],
		  "droppedAttributesCount": 0
		},
		"scopeSpans": [
		  {
			"spans": [
			  {
				"traceId": "{{.TraceID}}",
				"spanId": "1c1eaca9bba068d9",
				"parentSpanId": "ec5a65d86a5d499e",
				"traceState": "",
				"name": "place-articles",
				"kind": "SPAN_KIND_SERVER",
				"startTimeUnixNano": {{.Span6Start}},
				"endTimeUnixNano": {{.Span6End}},
				"attributes": [
				  {
					"key": "article.count",
					"value": {
					  "intValue": 1
					}
				  },
				  {
					"key": "net.transport",
					"value": {
					  "stringValue": "ip_tcp"
					}
				  },
				  {
					"key": "net.sock.family",
					"value": {
					  "stringValue": "inet"
					}
				  },
				  {
					"key": "net.sock.host.addr",
					"value": {
					  "stringValue": "192.168.231.63"
					}
				  },
				  {
					"key": "net.host.name",
					"value": {
					  "stringValue": "cart-service.local"
					}
				  },
				  {
					"key": "net.host.port",
					"value": {
					  "intValue": 8974
					}
				  },
				  {
					"key": "http.flavor",
					"value": {
					  "stringValue": "1.1"
					}
				  },
				  {
					"key": "http.scheme",
					"value": {
					  "stringValue": "https"
					}
				  },
				  {
					"key": "http.target",
					"value": {
					  "stringValue": "/place-articles"
					}
				  },
				  {
					"key": "http.response_content_length",
					"value": {
					  "intValue": 988945
					}
				  },
				  {
					"key": "http.request_content_length",
					"value": {
					  "intValue": 96890
					}
				  },
				  {
					"key": "http.method",
					"value": {
					  "stringValue": "PUT"
					}
				  },
				  {
					"key": "http.url",
					"value": {
					  "stringValue": "https://cart-service.local:8974/place-articles"
					}
				  },
				  {
					"key": "http.status_code",
					"value": {
					  "intValue": 201
					}
				  }
				],
				"droppedAttributesCount": 0,
				"droppedEventsCount": 0,
				"droppedLinksCount": 0,
				"status": {
				  "code": 0,
				  "message": ""
				}
			  },
			  {
				"traceId": "{{.TraceID}}",
				"spanId": "7f35fd8c149498e8",
				"parentSpanId": "1c1eaca9bba068d9",
				"traceState": "",
				"name": "persist-cart",
				"kind": "SPAN_KIND_INTERNAL",
				"startTimeUnixNano": {{.Span7Start}},
				"endTimeUnixNano": {{.Span7End}},
				"attributes": [],
				"droppedAttributesCount": 0,
				"droppedEventsCount": 0,
				"droppedLinksCount": 0,
				"status": {
				  "code": 0,
				  "message": ""
				}
			  }
			],
			"scope": {
			  "name": "k6",
			  "version": ""
			}
		  }
		]
	  },
	  {
		"resource": {
		  "attributes": [
			{
			  "key": "k6",
			  "value": {
				"stringValue": "true"
			  }
			},
			{
			  "key": "service.name",
			  "value": {
				"stringValue": "article-service"
			  }
			}
		  ],
		  "droppedAttributesCount": 0
		},
		"scopeSpans": [
		  {
			"spans": [
			  {
				"traceId": "{{.TraceID}}",
				"spanId": "485b6310e5d95a57",
				"parentSpanId": "77fecd69d987a3fc",
				"traceState": "",
				"name": "get-article",
				"kind": "SPAN_KIND_SERVER",
				"startTimeUnixNano": {{.Span8Start}},
				"endTimeUnixNano": {{.Span8End}},
				"attributes": [
				  {
					"key": "net.transport",
					"value": {
					  "stringValue": "ip_tcp"
					}
				  },
				  {
					"key": "net.sock.family",
					"value": {
					  "stringValue": "inet"
					}
				  },
				  {
					"key": "net.sock.host.addr",
					"value": {
					  "stringValue": "192.168.58.234"
					}
				  },
				  {
					"key": "net.host.name",
					"value": {
					  "stringValue": "article-service.local"
					}
				  },
				  {
					"key": "net.host.port",
					"value": {
					  "intValue": 8340
					}
				  },
				  {
					"key": "http.flavor",
					"value": {
					  "stringValue": "1.1"
					}
				  },
				  {
					"key": "http.scheme",
					"value": {
					  "stringValue": "https"
					}
				  },
				  {
					"key": "http.target",
					"value": {
					  "stringValue": "/get-article"
					}
				  },
				  {
					"key": "http.response_content_length",
					"value": {
					  "intValue": 151230
					}
				  },
				  {
					"key": "http.method",
					"value": {
					  "stringValue": "GET"
					}
				  },
				  {
					"key": "http.url",
					"value": {
					  "stringValue": "https://article-service.local:8340/get-article"
					}
				  },
				  {
					"key": "http.status_code",
					"value": {
					  "intValue": 204
					}
				  }
				],
				"droppedAttributesCount": 0,
				"droppedEventsCount": 0,
				"droppedLinksCount": 0,
				"status": {
				  "code": 0,
				  "message": ""
				}
			  },
			  {
				"traceId": "{{.TraceID}}",
				"spanId": "7731f938ce0b52d1",
				"parentSpanId": "485b6310e5d95a57",
				"traceState": "",
				"name": "select-articles",
				"kind": "SPAN_KIND_CLIENT",
				"startTimeUnixNano": {{.Span9Start}},
				"endTimeUnixNano": {{.Span9End}},
				"attributes": [
				  {
					"key": "net.transport",
					"value": {
					  "stringValue": "ip_tcp"
					}
				  },
				  {
					"key": "net.sock.family",
					"value": {
					  "stringValue": "inet"
					}
				  },
				  {
					"key": "net.peer.port",
					"value": {
					  "intValue": 8686
					}
				  },
				  {
					"key": "net.sock.peer.addr",
					"value": {
					  "stringValue": "192.168.211.34"
					}
				  },
				  {
					"key": "net.peer.name",
					"value": {
					  "stringValue": "postgres.local"
					}
				  }
				],
				"droppedAttributesCount": 0,
				"droppedEventsCount": 0,
				"droppedLinksCount": 0,
				"status": {
				  "code": 0,
				  "message": ""
				}
			  }
			],
			"scope": {
			  "name": "k6",
			  "version": ""
			}
		  }
		]
	  },
	  {
		"resource": {
		  "attributes": [
			{
			  "key": "k6",
			  "value": {
				"stringValue": "true"
			  }
			},
			{
			  "key": "service.name",
			  "value": {
				"stringValue": "postgres"
			  }
			}
		  ],
		  "droppedAttributesCount": 0
		},
		"scopeSpans": [
		  {
			"spans": [
			  {
				"traceId": "{{.TraceID}}",
				"spanId": "bd41823b2aca2ae6",
				"parentSpanId": "7731f938ce0b52d1",
				"traceState": "",
				"name": "query-articles",
				"kind": "SPAN_KIND_SERVER",
				"startTimeUnixNano": {{.Span10Start}},
				"endTimeUnixNano": {{.Span10End}},
				"attributes": [
				  {
					"key": "k6.7qPFcP1ZWQVgzf5",
					"value": {
					  "stringValue": "9nwjRPynmAXXH8LBXEX1qInlOrlaig"
					}
				  },
				  {
					"key": "k6.9Xz2U0J5u8Pggvj",
					"value": {
					  "stringValue": "hXt7tbHXJ8COzglMLXpjpdGanCrxwZ"
					}
				  },
				  {
					"key": "net.transport",
					"value": {
					  "stringValue": "ip_tcp"
					}
				  },
				  {
					"key": "net.sock.family",
					"value": {
					  "stringValue": "inet"
					}
				  },
				  {
					"key": "net.sock.host.addr",
					"value": {
					  "stringValue": "192.168.211.34"
					}
				  },
				  {
					"key": "net.host.name",
					"value": {
					  "stringValue": "postgres.local"
					}
				  },
				  {
					"key": "net.host.port",
					"value": {
					  "intValue": 8562
					}
				  }
				],
				"droppedAttributesCount": 0,
				"droppedEventsCount": 0,
				"droppedLinksCount": 0,
				"status": {
				  "code": 0,
				  "message": ""
				}
			  }
			],
			"scope": {
			  "name": "k6",
			  "version": ""
			}
		  }
		]
	  }
	]
}`
