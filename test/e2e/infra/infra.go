package infra

import (
	"context"
	"fmt"
	"net/http"

	gocommon "gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/common"
	schedulerv1alpha1 "gitlab.com/gitlab-org/opstrace/opstrace/scheduler/api/v1alpha1"
	"gitlab.com/gitlab-org/opstrace/opstrace/test/e2e/infra/common"
	"gitlab.com/gitlab-org/opstrace/opstrace/test/e2e/infra/devvm"
	"gitlab.com/gitlab-org/opstrace/opstrace/test/e2e/infra/gcp"
	"gitlab.com/gitlab-org/opstrace/opstrace/test/e2e/infra/kind"
	"k8s.io/client-go/rest"
)

type ConfigurationBuilder interface {
	// LoadConfiguration sets up infra config
	LoadConfiguration() error

	// Configuration returns infra's configuration. Some providers obtain their
	// configuration during the provisioning, hence we can't fetch it upfront.
	Configuration() *common.Configuration

	// GetClusterDefinition configures the cluster with infra specific overrides
	GetClusterDefinition(context.Context) (*schedulerv1alpha1.Cluster, error)

	// InternalConfiguration returns the internal configuration for the provider.
	// used for debugging purposes, e.g. TF_VARs for GCP.
	InternalConfiguration() (map[string]string, error)
	GetHTTPClient() *http.Client
}

type GitLabInstanceBuilder interface {
	CreateGitLabInstance(context.Context) error
	DestroyGitLabInstance(context.Context) error
}

type K8sClusterBuilder interface {
	CreateK8sCluster(context.Context) error
	DestroyK8sCluster(context.Context) error
	// GetRestConfig returns the rest config for the cluster.
	// This is used for creating clients.
	GetRestConfig(context.Context) (*rest.Config, error)
}

type InfrastructureBuilder interface {
	ConfigurationBuilder
	GitLabInstanceBuilder
	K8sClusterBuilder
}

// New returns a new InfrastructureBuilder based on the provider.
// The identifier is used as a unique reference for any created resources.
func New(provider gocommon.EnvironmentTarget, identifier string) (InfrastructureBuilder, error) {
	var builder InfrastructureBuilder

	switch provider {
	case gocommon.KIND:
		builder = kind.New()
	case gocommon.DEVVM:
		builder = devvm.New(identifier)
	case gocommon.GCP:
		builder = gcp.New(identifier)
	default:
		return nil, fmt.Errorf("unknown provider: %s", provider)
	}

	return builder, nil
}
