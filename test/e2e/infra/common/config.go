package common

import (
	"fmt"

	"github.com/gruntwork-io/terratest/modules/logger"
	"github.com/gruntwork-io/terratest/modules/testing"
	. "github.com/onsi/ginkgo/v2"
	schedulerv1alpha1 "gitlab.com/gitlab-org/opstrace/opstrace/scheduler/api/v1alpha1"
)

// Some shared environment variable names for infra providers.
const (
	TestEnvSchedulerImage = "TEST_SCHEDULER_IMAGE"
	TestEnvSSHKeyPath     = "TEST_SSH_KEY_PATH"
	//nolint:gosec // no, it is not hardcoded credential
	TestEnvSSHKeyPassphrase = "TEST_SSH_KEY_PASSPHRASE"
	TestEnvGitLabImage      = "TEST_GITLAB_IMAGE"
	TestGitLabEELicensePath = "TEST_GITLAB_EE_LICENSE_PATH"
	TestEnvGitLabCommit     = "TEST_GITLAB_COMMIT"
)

// Configuration is shared config between the different infra providers.
type Configuration struct {
	// GOBHost is the host of the GOB instance
	GOBHost string
	// GOBScheme is the protocol scheme of the GOB instance
	GOBScheme string
	// GitLabHost is the host of the GitLab instance
	GitLabHost string
	// GitLabScheme is the protocol scheme of the GitLab instance
	GitLabScheme string
	// InstanceName is the name of the instance
	InstanceName string
	// GitLabImage is the image used for GitLab
	GitLabImage string
	// SchedulerImage is the image used for the scheduler
	SchedulerImage string
	// GitLabAdminToken is the admin token for the GitLab instance.
	GitLabAdminToken string

	// NOTE(prozlach): We are using a hybrid approach here: e2e tests use
	// port-forwarding through SSH, whereas k8s and http clients use SOCKS
	// proxy.
	// This way developers do not need to modify their code in order to be able
	// to run it against devvm. SOCKS library we use is mature and allows k8s
	// and http clients to work transparently.
	// It is not possible to make all test use SOCKS proxy as this would
	// require modifying tests and e.g. otel collector does not allow for
	// custom transports, while e.g. cypress does not support SOCKS proxies at
	// all.
	// Tunneling everything is doable, but not sure if it is worth it. k8s/http
	// clients "just work". TBD.

	// SOCKSProxyAddress proxy is the address of the proxy to be used when
	// making http requests. No need to use proxy if empty.
	SOCKSProxyAddress string
	// GOBPortForwardAddress is the address of a local socket that forwards
	// through ssh to GOB.
	GOBPortForwardAddress string

	// GDKPortForwardAddress is the address of a local socket that forwards
	// through ssh to GOB.
	GDKPortForwardAddress string

	GOBCluster *schedulerv1alpha1.Cluster
}

func (c Configuration) GOBAddress() string {
	p := c.GOBScheme
	if p == "" {
		p = "https"
	}
	return fmt.Sprintf("%s://%s", p, c.GOBHost)
}

func (c Configuration) GitLabAddress() string {
	p := c.GitLabScheme
	if p == "" {
		p = "https"
	}
	return fmt.Sprintf("%s://%s", p, c.GitLabHost)
}

type ginkgoLogger struct{}

func (ginkgoLogger) Logf(t testing.TestingT, format string, args ...interface{}) {
	logger.DoLog(t, 3, GinkgoWriter, fmt.Sprintf(format, args...))
}

var GinkgoLogger = logger.New(ginkgoLogger{})
