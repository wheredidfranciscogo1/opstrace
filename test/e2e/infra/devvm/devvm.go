package devvm

import (
	"context"
	"crypto/tls"
	"errors"
	"fmt"
	"net"
	"net/http"
	"os"
	"regexp"
	"time"

	compute "cloud.google.com/go/compute/apiv1"
	"cloud.google.com/go/compute/apiv1/computepb"
	"github.com/pkg/sftp"
	"gitlab.com/gitlab-org/opstrace/devvm/go/pkg/constants"
	"gitlab.com/gitlab-org/opstrace/devvm/go/pkg/provisioning"
	"gitlab.com/gitlab-org/opstrace/devvm/go/pkg/status"
	"gitlab.com/gitlab-org/opstrace/devvm/go/pkg/systemops"
	schedulerv1alpha1 "gitlab.com/gitlab-org/opstrace/opstrace/scheduler/api/v1alpha1"
	"gitlab.com/gitlab-org/opstrace/opstrace/test/e2e/infra/common"
	"gitlab.com/gitlab-org/opstrace/opstrace/test/e2e/infra/gcp"
	"golang.org/x/crypto/ssh"
	"golang.org/x/net/proxy"
	"k8s.io/client-go/rest"
	"sigs.k8s.io/yaml"
)

type Devvm struct {
	config *common.Configuration

	name string

	sshSigner         ssh.Signer
	sshPubkey         string
	sshKeyIsEphemeral bool
	imageTag          string
	gitLabCommit      string

	zone   string
	region string

	sshConn                *ssh.Client
	sftpClient             *sftp.Client
	socksListener          net.Listener
	gobPortForwardListener net.Listener
	gdkPortForwardListener net.Listener

	restConfig *rest.Config
}

func New(id string) *Devvm {
	res := new(Devvm)
	// `onfailure` is a special case where we run in CI but want to keep devvm
	// in case e2e fails
	if skip, ok := os.LookupEnv("TEST_SKIP_CLEANUP"); ok && skip != "onfailure" {
		// in case when we skip cleanup, we need to create a predictable name
		// for devvm in order for the e2e code to be able to reuse it, instead
		// of creating a new one.
		res.name = fmt.Sprintf("devvm-e2e-%s", id)
	} else {
		// adding a time suffix to the TestIdentifier allows multiple parallel
		// test infrastructures to exist that map to the same commit SHA. Prior
		// to this, we'd get a 409 when rerunning a test that hadn't yet
		// destroyed the VM from the last run.
		timeSuffix := time.Now().UnixMilli()

		res.name = fmt.Sprintf("devvm-e2e-%s-%d", id, timeSuffix)
	}

	return res
}

// getOutboundIP gets preferred outbound ip of this machine.
func getOutboundIP() (string, error) {
	conn, err := net.Dial("udp", "8.8.8.8:80")
	if err != nil {
		return "", fmt.Errorf("unable to send test udp packet: %w", err)
	}
	defer conn.Close()

	//nolint:errcheck
	localAddr := conn.LocalAddr().(*net.UDPAddr)

	return localAddr.IP.String(), nil
}

func (g *Devvm) readSSHKey() error {
	var err error

	sshKeyPath, _ := os.LookupEnv(common.TestEnvSSHKeyPath)

	var sshKeyPassphrase []byte
	if val, ok := os.LookupEnv(common.TestEnvSSHKeyPassphrase); ok {
		sshKeyPassphrase = []byte(val)
	}
	g.sshSigner, g.sshPubkey, err = provisioning.ReadSSHKeys(sshKeyPath, sshKeyPassphrase)
	//nolint:nestif
	if err != nil {
		var ppErr *ssh.PassphraseMissingError
		if errors.As(err, &ppErr) {
			if len(sshKeyPassphrase) == 0 {
				sshKeyPassphrase, err = provisioning.ReadPasswordFromTerm()
				if err != nil {
					return fmt.Errorf("unable to read passphrase from stdin: %w", err)
				}
			}

			g.sshSigner, g.sshPubkey, err = provisioning.ReadSSHKeys(sshKeyPath, sshKeyPassphrase)
			if err != nil {
				return fmt.Errorf("unable to read ssh key from %s: %w", sshKeyPath, err)
			}
		} else {
			return fmt.Errorf("unable to read ssh key from %s: %w", sshKeyPath, err)
		}
	}
	return nil
}

func (g *Devvm) LoadConfiguration() error {
	schedulerImage, err := common.GetEnv(common.TestEnvSchedulerImage)
	if err != nil {
		return err
	}
	gitLabCommit, err := common.GetEnv(common.TestEnvGitLabCommit)
	if err == nil {
		g.gitLabCommit = gitLabCommit
	}

	// Example: registry.gitlab.com/gitlab-org/opstrace/opstrace/scheduler:0.3.0-9c7e6269
	re := regexp.MustCompile(`^(.*)/scheduler:((\d+\.\d+\.\d+)(-(\S{8})(\.dirty)?)?)$`)
	matches := re.FindStringSubmatch(schedulerImage)
	if len(matches) != 7 {
		return fmt.Errorf("unable to extract image tag ID from the scheduler image %q", schedulerImage)
	}
	if matches[1] != "registry.gitlab.com/gitlab-org/opstrace/opstrace" {
		// TODO(prozlach): It's perfectly doable, just requires some more time
		// to implement it.
		return fmt.Errorf("images from custom registries are not supported at the moment by devvm provider")
	}
	// NOTE(prozlach): We want to signal to devvm's booter script that it is
	// dealing with a imageTag/registry and not with a git branch name. The `:`
	// char is not valid for git branch names.
	g.imageTag = "imageTag:" + matches[2]
	if matches[5] != "" {
		g.imageTag += ":commit:" + matches[5]
	}

	if _, ok := os.LookupEnv(common.TestEnvSSHKeyPath); ok {
		err = g.readSSHKey()
		if err != nil {
			return fmt.Errorf("unable to read ssh key: %w", err)
		}
	} else {
		// Use ephemeral key, folks will still be able to log in using GCE
		// cloud login
		g.sshSigner, g.sshPubkey, err = provisioning.GenerateECDSAKeys(256)
		if err != nil {
			return fmt.Errorf("unable to generate ephemeral ssh key: %w", err)
		}
		g.sshKeyIsEphemeral = true
	}

	// NOTE(prozlach): Hardcoded, just like for the GCE provider:
	g.zone = "us-west2-a"
	g.region, err = provisioning.GetGCERegion(g.zone)
	if err != nil {
		// programming error, not an user-input error
		panic(fmt.Sprintf("zone %s is incorrect: %v", g.zone, err))
	}

	g.config = &common.Configuration{
		GOBHost:      "gob.devvm",
		GitLabHost:   "gdk.devvm:3443",
		GOBScheme:    "https",
		GitLabScheme: "https",
	}
	return nil
}

func (g *Devvm) Configuration() *common.Configuration {
	return g.config
}

func (g *Devvm) InternalConfiguration() (map[string]string, error) {
	// nothing to see here
	return map[string]string{}, nil
}

func (g *Devvm) ensureVM(
	ctx context.Context,
	instanceName, gceProject, zone, pubKey, region, gobBranch string,
	instancesClient *compute.InstancesClient,
) error {
	instanceInfo, notFound, err := provisioning.GetVMInfo(
		ctx,
		instanceName,
		gceProject,
		zone,
		instancesClient,
	)
	gitLabBranch := "master"
	if g.gitLabCommit != "" {
		gitLabBranch = "commit:" + g.gitLabCommit
	}
	//nolint:nestif // required by the program logic
	if err == nil {
		if g.sshKeyIsEphemeral {
			// NOTE(prozlach): we can't re-use devvm instance when using ephemeral
			// ssh-keys, hence we remove the existing instance and create a new one
			// with a correct key
			op, err := instancesClient.Delete(ctx, &computepb.DeleteInstanceRequest{
				Project:  gceProject,
				Zone:     zone,
				Instance: instanceName,
			})
			if err != nil {
				return fmt.Errorf("unable to delete VM %s: %w", instanceName, err)
			}
			if err = op.Wait(ctx); err != nil {
				return fmt.Errorf("error occurred while waiting for the devvm delete operation: %w", err)
			}

			err = provisioning.CreateVM(
				ctx,
				instanceName,
				pubKey,
				gceProject,
				zone,
				region,
				gobBranch,
				gitLabBranch,
				false,
				instancesClient,
				map[string]string{
					"source": "e2e",
				},
			)
			if err != nil {
				return fmt.Errorf("unable to create VM: %w", err)
			}
		} else if *instanceInfo.Status != "RUNNING" {
			return fmt.Errorf("VM exists and is in state %s", *instanceInfo.Status)
		}
	} else {
		if !notFound {
			return fmt.Errorf("unable to fetch VM instance info: %w", err)
		}

		err = provisioning.CreateVM(
			ctx,
			instanceName,
			pubKey,
			gceProject,
			zone,
			region,
			gobBranch,
			gitLabBranch,
			false,
			instancesClient,
			map[string]string{
				"source": "e2e",
			},
		)

		if err != nil {
			return fmt.Errorf("unable to create VM: %w", err)
		}
	}

	return nil
}

// CreateGitLabInstance creates devvm and waits until GDK is up,
// CreateK8sCluster is just a NO-OP. Provisioning devm differs wildly from the
// provisioning of the GCE cluster, hence we try to fit in to the current
// architecture as much as  we can and adhere to the general interface that
// other provisers use.
//
//nolint:funlen,cyclop
func (g *Devvm) CreateGitLabInstance(ctx context.Context) error {
	eeLicensePath, err := common.GetEnv(common.TestGitLabEELicensePath)
	if err != nil || eeLicensePath == "" {
		return fmt.Errorf("gitLab ee license path required and not provided: %w", err)
	}

	instancesClient, err := compute.NewInstancesRESTClient(ctx)
	if err != nil {
		return fmt.Errorf("failed to create instance client: %w", err)
	}

	err = g.ensureVM(
		ctx,
		g.name,
		gcp.ProjectID,
		g.zone,
		g.sshPubkey,
		g.region,
		g.imageTag,
		instancesClient,
	)
	if err != nil {
		return fmt.Errorf("ensure VM is running failed: %w", err)
	}

	instanceInfo, _, err := provisioning.GetVMInfo(
		ctx,
		g.name,
		gcp.ProjectID,
		g.zone,
		instancesClient,
	)
	if err != nil {
		return fmt.Errorf(
			"unable to fetch information about instance %s: %w", g.name, err,
		)
	}

	publicIP, err := provisioning.ExtractPublicIPFromVMInfo(instanceInfo)
	if err != nil {
		return fmt.Errorf(
			"unable to determine public IP of the instance %s: %w", g.name, err,
		)
	}

	g.sshConn, err = provisioning.EstablishSSHConn(ctx, g.sshSigner, publicIP)
	if err != nil {
		return fmt.Errorf("unable to connect via SSH: %w", err)
	}

	// open an SFTP session over an existing ssh connection.
	g.sftpClient, err = sftp.NewClient(g.sshConn)
	if err != nil {
		return fmt.Errorf("unable to establish sftp client: %w", err)
	}

	err = provisioning.UploadLicenseFile(g.sftpClient, eeLicensePath)
	if err != nil {
		return fmt.Errorf("unable to upload license file: %w", err)
	}

	sd, err := g.waitDevvmReady(ctx)
	if err != nil {
		return fmt.Errorf("devvm did not become ready: %w", err)
	}

	g.config.GitLabAdminToken, err = sd.AdminToken()
	if err != nil {
		return fmt.Errorf("unable to find token data in booter's status: %w", err)
	}

	gdkIP, err := sd.GDKIP()
	if err != nil {
		return fmt.Errorf("gdk IP can't be determined: %w", err)
	}
	clusterYaml := systemops.ClusterYAML(gdkIP)
	g.config.GOBCluster = new(schedulerv1alpha1.Cluster)
	err = yaml.Unmarshal([]byte(clusterYaml), &g.config.GOBCluster)
	if err != nil {
		return fmt.Errorf("failed to unmarshal object into unstructured.Unstructured: %w", err)
	}

	// NOTE(prozlach) The problem we have here, is that we can't use loopback
	// address for socks proxy/DNAT, as it is not available inside containers.
	// We can't even launch containers in host-mode networking as it is
	// Linux-only and would prevent tests from succeding on Macs. Hence we
	// start proxy on the preffered outbound IP address of this machine as it
	// has best chances for being accessible to containers.
	outboundIP, err := getOutboundIP()
	if err != nil {
		return fmt.Errorf("unable to determine outbound address of this machine: %w", err)
	}

	g.socksListener, err = provisioning.StartSOCKSProxy(outboundIP, g.sshConn)
	if err != nil {
		return fmt.Errorf("failed to start SOCKS5 proxy server: %w", err)
	}
	g.config.SOCKSProxyAddress = "socks5://" + g.socksListener.Addr().String()

	g.gobPortForwardListener, err = provisioning.StartPortForward(
		outboundIP+":56789", "10.15.16.129:443", g.sshConn,
	)
	if err != nil {
		return fmt.Errorf("failed to start gob port forward on port %s:56789: %w", outboundIP, err)
	}
	g.config.GOBPortForwardAddress = g.gobPortForwardListener.Addr().String()

	g.gdkPortForwardListener, err = provisioning.StartPortForward(
		outboundIP+":56790", gdkIP+":3443", g.sshConn,
	)
	if err != nil {
		return fmt.Errorf("failed to start gdk port forward on port %s:56790: %w", outboundIP, err)
	}
	g.config.GDKPortForwardAddress = g.gobPortForwardListener.Addr().String()

	g.restConfig, err = sd.RestConfig(g.socksListener.Addr().String())
	if err != nil {
		return fmt.Errorf("unable to determine k8s rest config: %w", err)
	}

	return nil
}

func (g *Devvm) waitDevvmReady(ctx context.Context) (*status.StatusData, error) {
	var sd *status.StatusData
	var err error

	ticker := time.NewTicker(5 * time.Second)
	defer ticker.Stop()

	timeout := 25 * time.Minute
	to := time.NewTimer(timeout)
	defer to.Stop()

LOOP:
	for {
		select {
		case <-ctx.Done():
			return nil, errors.New("context canceled while waiting for booter to set up VM")
		case <-to.C:
			return nil, fmt.Errorf("timed out after %s while waiting for booter to set up devvm", timeout.String())
		case <-ticker.C:
			sd, err = provisioning.FetchStatusData(g.sftpClient)
			if err != nil {
				if errors.Is(err, os.ErrNotExist) {
					continue
				}
				return nil, fmt.Errorf("unable to fetch status data from devvm: %w", err)
			}
			allDone, _ := sd.StatusSummary()
			if allDone {
				break LOOP
			}
		}
	}

	return sd, nil
}

func (g *Devvm) DestroyGitLabInstance(ctx context.Context) error {
	g.socksListener.Close()
	g.gobPortForwardListener.Close()
	g.sftpClient.Close()
	g.sshConn.Close()

	instancesClient, err := compute.NewInstancesRESTClient(ctx)
	if err != nil {
		return fmt.Errorf("failed to create instance client: %w", err)
	}

	op, err := instancesClient.Delete(ctx, &computepb.DeleteInstanceRequest{
		Project:  constants.GCEProject,
		Zone:     g.zone,
		Instance: g.name,
	})
	if err != nil {
		return fmt.Errorf("unable to delete VM %s: %w", g.name, err)
	}
	if err = op.Wait(ctx); err != nil {
		return fmt.Errorf("error occurred while waiting for the devvm delete operation: %w", err)
	}
	return nil
}

func (g *Devvm) CreateK8sCluster(ctx context.Context) error {
	return nil
}

func (g *Devvm) DestroyK8sCluster(ctx context.Context) error {
	return nil
}

func (g *Devvm) GetHTTPClient() *http.Client {
	// Safe to ignore this error, as stdlib does not emit it anyway
	//nolint:errcheck
	dialSocksProxy, _ := proxy.SOCKS5(
		"tcp", g.socksListener.Addr().String(), nil, proxy.Direct,
	)

	return &http.Client{
		// TODO(prozlach): GDK can be really slow, it would be nice to some day
		// debug why and improve this. For now we just bump the timeout to an
		// arbitrary high value
		Timeout: 60 * time.Second,
		Transport: &http.Transport{
			Dial: dialSocksProxy.Dial,
			TLSClientConfig: &tls.Config{
				InsecureSkipVerify: true, //nolint:gosec
			},
		},
	}
}

// GetRestConfig returns the default go-client rest config.
// See https://pkg.go.dev/sigs.k8s.io/controller-runtime/pkg/client/config#GetConfig for more details.
//
//nolint:unparam
func (g *Devvm) GetRestConfig(_ context.Context) (*rest.Config, error) {
	return g.restConfig, nil
}

//nolint:unparam
func (g *Devvm) GetClusterDefinition(_ context.Context) (*schedulerv1alpha1.Cluster, error) {
	return g.config.GOBCluster.DeepCopy(), nil
}
