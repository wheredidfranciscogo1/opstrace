package gcp

import (
	"context"
	"fmt"
	"os"
	"path/filepath"

	acmev1 "github.com/cert-manager/cert-manager/pkg/apis/acme/v1"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/common"
	schedulerv1alpha1 "gitlab.com/gitlab-org/opstrace/opstrace/scheduler/api/v1alpha1"
	v1 "k8s.io/api/core/v1"
	"k8s.io/utils/ptr"
	"sigs.k8s.io/yaml"
)

func (g *GCP) GetClusterDefinition(_ context.Context) (*schedulerv1alpha1.Cluster, error) {
	clusterObjPath, err := filepath.Abs("../../../scheduler/config/examples/Cluster.yaml")
	if err != nil {
		return nil, fmt.Errorf("unable to resolve relative path to base cluster object data: %w", err)
	}

	clusterObjBytes, err := os.ReadFile(clusterObjPath)
	if err != nil {
		return nil, fmt.Errorf("unable to load base cluster object data: %w", err)
	}

	res := new(schedulerv1alpha1.Cluster)
	err = yaml.Unmarshal(clusterObjBytes, res)
	if err != nil {
		return nil, fmt.Errorf("unable to unmarshal cluster object data: %w", err)
	}

	tfvars := g.config.tfVars

	res.ObjectMeta.Name = g.config.InstanceName

	res.Spec.Target = common.GCP // do not change

	res.Spec.DNS.CertificateIssuer = tfvars.CertIssuer
	res.Spec.DNS.Domain = ptr.To(g.config.GOBHost)
	res.Spec.DNS.ACMEEmail = tfvars.AcmeEmail
	res.Spec.DNS.DNS01Challenge = acmev1.ACMEChallengeSolverDNS01{
		CloudDNS: &acmev1.ACMEIssuerDNS01ProviderCloudDNS{
			Project: tfvars.ProjectID,
		},
	}

	res.Spec.DNS.ExternalDNSProvider = schedulerv1alpha1.ExternalDNSProviderSpec{
		GCP: &schedulerv1alpha1.ExternalDNSGCPSpec{
			DNSServiceAccountName: g.config.tfOutputs.ExternalDNSServiceAccount,
			ManagedZoneName:       &tfvars.CloudDNSZone,
		},
	}
	res.Spec.DNS.FirewallSourceIPsAllowed = []string{"0.0.0.0/0"}

	res.Spec.DNS.GCPCertManagerServiceAccount = ptr.To(g.config.tfOutputs.CertManagerServiceAccount)

	res.Spec.GitLab.InstanceURL = g.config.GitLabAddress()
	res.Spec.GitLab.AuthSecret = v1.LocalObjectReference{
		Name: tfvars.ClusterSecretName,
	}

	return res, nil
}
