package gcp

import (
	"bufio"
	"context"
	"encoding/base64"
	"fmt"
	"os"
	"time"

	"github.com/gruntwork-io/terratest/modules/logger"
	"github.com/gruntwork-io/terratest/modules/testing"
	"google.golang.org/api/container/v1"
	"k8s.io/client-go/rest"
	"k8s.io/client-go/tools/clientcmd/api"

	. "github.com/onsi/ginkgo/v2"
)

const (
	// TODO(joe): all these should be configurable
	ManagedZoneName        = "opstrace-dev-com"
	GitlabDomainNameTmpl   = "test-%s-gitlab.opstrace-dev.com"
	PlatformDomainNameTmpl = "test-%s-platform.opstrace-dev.com"
	ProjectID              = "opstrace-dev-bee41fca"
)

type CustomLogger struct { // helps redirect Terratest logs as needed
	writer *bufio.Writer
}

func (l CustomLogger) Logf(t testing.TestingT, format string, args ...interface{}) {
	logger.DoLog(t, 2, l.writer, fmt.Sprintf(format, args...))
	l.writer.Flush() // flush log lines as soon as they're written
}

type GCP struct {
	identifier         string
	config             *configuration
	logfile            *os.File
	clusterProvisioner *terragruntProvisioner
	gitlabProvider     *gitLabProvider
}

func New(identifier string) *GCP {
	return &GCP{
		identifier: identifier,
	}
}

func (g *GCP) CreateGitLabInstance(_ context.Context) error {
	// terragrunt create GCP VM instance
	if err := g.gitlabProvider.provision(GinkgoT()); err != nil {
		return err
	}
	// setup SSH for the instance just provisioned
	if err := g.gitlabProvider.setupSSH(); err != nil {
		return err
	}
	// on the instance, install GitLab
	if err := g.gitlabProvider.installGitLab(); err != nil {
		return err
	}
	// await GitLab to be ready
	if err := g.gitlabProvider.awaitGitLabReadiness(); err != nil {
		return err
	}
	// setup dependencies -> fetch root secret, create PAT, create application, export credentials
	if err := g.gitlabProvider.setupDependencies(); err != nil {
		return err
	}
	return nil
}

func (g *GCP) DestroyGitLabInstance(_ context.Context) error {
	return g.gitlabProvider.deprovision(GinkgoT())
}

func (g *GCP) CreateK8sCluster(_ context.Context) error {
	// provision the platform specific to this test run
	if err := g.clusterProvisioner.provision(GinkgoT()); err != nil {
		return fmt.Errorf("provision cluster: %w", err)
	}
	return nil
}

func (g *GCP) DestroyK8sCluster(_ context.Context) error {
	// deprovision the platform provisioned specific to this test run
	if err := g.clusterProvisioner.deprovision(GinkgoT()); err != nil {
		return err
	}
	return nil
}

func (g *GCP) GetRestConfig(ctx context.Context) (*rest.Config, error) {
	svc, err := container.NewService(ctx)
	if err != nil {
		return nil, fmt.Errorf("new container service: %w", err)
	}
	cluster, err := svc.Projects.Locations.Clusters.Get(fmt.Sprintf("projects/%s/locations/%s/clusters/%s",
		g.config.tfVars.ProjectID, g.config.tfVars.Location, g.config.tfVars.InstanceName)).Do()
	if err != nil {
		return nil, fmt.Errorf("get cluster: %w", err)
	}

	caCert, err := base64.StdEncoding.DecodeString(cluster.MasterAuth.ClusterCaCertificate)
	if err != nil {
		return nil, fmt.Errorf("decode cluster CA certificate: %w", err)
	}

	return &rest.Config{
		Host:    "https://" + cluster.Endpoint,
		Timeout: time.Duration(10) * time.Second,
		TLSClientConfig: rest.TLSClientConfig{
			CAData: caCert,
		},
		// mimic `gcloud container clusters get-credentials`.
		// see also https://cloud.google.com/kubernetes-engine/docs/deprecations/auth-plugin.
		ExecProvider: &api.ExecConfig{
			Command:            "gke-gcloud-auth-plugin",
			APIVersion:         "client.authentication.k8s.io/v1beta1",
			ProvideClusterInfo: true,
			InteractiveMode:    api.NeverExecInteractiveMode,
		},
	}, nil
}

func (g *GCP) Close() error {
	if g.logfile == nil {
		return nil
	}
	if err := g.logfile.Close(); err != nil {
		return fmt.Errorf("close logfile: %w", err)
	}

	return nil
}
