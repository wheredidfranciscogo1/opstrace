package gcp

import (
	"bufio"
	"fmt"
	"net/http"
	"os"
	"path/filepath"
	"time"

	"github.com/gruntwork-io/terratest/modules/logger"
	"github.com/mitchellh/mapstructure"
	gcommon "gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/common"
	"gitlab.com/gitlab-org/opstrace/opstrace/test/e2e/infra/common"
)

type configuration struct {
	common.Configuration
	tfVars    tfVars
	tfOutputs tfOutputs
	// path to terraform directory in this project
	terraformDir string
}

// tfVars are the variables used by terraform.
type tfVars struct {
	BucketName             string `mapstructure:"TF_VAR_bucket_name"`
	RemoteStatePrefix      string `mapstructure:"TF_VAR_remote_state_prefix"`
	ProjectID              string `mapstructure:"TF_VAR_project_id"`
	InstanceName           string `mapstructure:"TF_VAR_instance_name"`
	Location               string `mapstructure:"TF_VAR_location"`
	GKEMachineType         string `mapstructure:"TF_VAR_gke_machine_type"`
	GitLabDomain           string `mapstructure:"TF_VAR_gitlab_domain"`
	CloudDNSZone           string `mapstructure:"TF_VAR_cloud_dns_zone"`
	AcmeServer             string `mapstructure:"TF_VAR_acme_server"`
	AcmeEmail              string `mapstructure:"TF_VAR_acme_email"`
	CertIssuer             string `mapstructure:"TF_VAR_cert_issuer"`
	Domain                 string `mapstructure:"TF_VAR_domain"`
	GitLabImage            string `mapstructure:"TF_VAR_gitlab_image"`
	ClusterSecretName      string `mapstructure:"TF_VAR_cluster_secret_name"`
	ClusterSecretNamespace string `mapstructure:"TF_VAR_cluster_secret_namespace"`
	SchedulerImage         string `mapstructure:"TF_VAR_scheduler_image"`
	RegistryUsername       string `mapstructure:"TF_VAR_registry_username"`
	RegistryAuthToken      string `mapstructure:"TF_VAR_registry_auth_token"`
	GitLabAdminToken       string `mapstructure:"TF_VAR_admin_token"`
	InternalEndpointToken  string `mapstructure:"TF_VAR_internal_endpoint_token"`
	GitLabRootPassword     string `mapstructure:"TF_VAR_root_password"`
	OAuthClientID          string `mapstructure:"TF_VAR_oauth_client_id"`
	OAuthClientSecret      string `mapstructure:"TF_VAR_oauth_client_secret"`
	ReleaseChannel         string `mapstructure:"TF_VAR_release_channel"`
	AutoRepair             string `mapstructure:"TF_VAR_pool_auto_repair"`
	GitLabTracesEndpoint   string `mapstructure:"TF_VAR_gitlab_traces_endpoint"`
	GitLabTracesToken      string `mapstructure:"TF_VAR_gitlab_traces_endpoint_private_token"`
}

// tfOutputs are the outputs from the terraform state.
type tfOutputs struct {
	CertManagerServiceAccount string `mapstructure:"certmanager_service_account"`
	ExternalDNSServiceAccount string `mapstructure:"externaldns_service_account"`
}

func (g *GCP) GetHTTPClient() *http.Client {
	return &http.Client{
		Timeout: 10 * time.Second,
	}
}

// LoadConfiguration loads the common configuration and the required TFVars.
func (g *GCP) LoadConfiguration() error {
	g.config = &configuration{}
	cfg := common.Configuration{}
	logger, err := g.configureLogging()
	if err != nil {
		return err
	}

	gitLabImage := common.GetGitLabImage()

	schedulerImage, err := common.GetEnv(common.TestEnvSchedulerImage)
	if err != nil {
		return err
	}

	registryUsername, err := common.GetEnv("TF_VAR_registry_username")
	if err != nil {
		return err
	}

	registryAuthToken, err := common.GetEnv("TF_VAR_registry_auth_token")
	if err != nil {
		return err
	}

	terraformDir, err := common.GetEnv("TEST_TERRAFORM_DIR")
	if err != nil {
		return err
	}
	terraformDir, err = filepath.Abs(terraformDir)
	if err != nil {
		return fmt.Errorf("getting absolute path: %w", err)
	}

	gitlabDomain := g.gitLabHost()

	cfg.GitLabHost = gitlabDomain
	cfg.GOBHost = g.gobHost()
	cfg.GitLabImage = gitLabImage
	cfg.SchedulerImage = schedulerImage
	cfg.InstanceName = fmt.Sprintf("testplatform-%s", g.identifier)
	cfg.GitLabAdminToken = gcommon.RandStringRunes(20)

	g.config.terraformDir = terraformDir
	g.config.Configuration = cfg
	g.config.tfVars = tfVars{
		BucketName:             "terragrunt-e2e-ci-pipelines",
		RemoteStatePrefix:      g.identifier,
		ProjectID:              ProjectID,
		InstanceName:           cfg.InstanceName,
		Location:               "us-west2-a",
		GKEMachineType:         "n1-standard-8",
		GitLabDomain:           gitlabDomain,
		CloudDNSZone:           ManagedZoneName,
		AcmeServer:             "",
		AcmeEmail:              "monitorobservability@gitlab.com",
		CertIssuer:             "letsencrypt-prod",
		Domain:                 gitlabDomain,
		GitLabImage:            gitLabImage,
		ClusterSecretName:      "auth-secret",
		ClusterSecretNamespace: "default",
		SchedulerImage:         schedulerImage,
		RegistryUsername:       registryUsername,
		RegistryAuthToken:      registryAuthToken,
		ReleaseChannel:         "UNSPECIFIED",
		AutoRepair:             "false",
		GitLabTracesEndpoint:   "FIXME_WHEN_NEEDED",
		GitLabTracesToken:      "FIXME_WHEN_NEEDED",
	}

	g.clusterProvisioner = newTerragruntProvisioner(logger, g.identifier, g.config)
	g.gitlabProvider = newGitLabProvider(logger, g.identifier, g.config)

	return nil
}

func (g *GCP) Configuration() *common.Configuration {
	return &g.config.Configuration
}

func (g *GCP) InternalConfiguration() (map[string]string, error) {
	return g.config.tfVars.encode()
}

func (g *GCP) gitLabHost() string {
	return fmt.Sprintf(GitlabDomainNameTmpl, g.identifier)
}

func (g *GCP) gobHost() string {
	return fmt.Sprintf(PlatformDomainNameTmpl, g.identifier)
}

func (g *GCP) configureLogging() (*logger.Logger, error) {
	var l *logger.Logger
	var f *os.File
	if _, ok := os.LookupEnv("TERRAGRUNT_LOG_FILE"); ok {
		filepath := os.Getenv("TERRAGRUNT_LOG_FILE")
		var err error
		f, err = os.OpenFile(filepath, os.O_WRONLY|os.O_CREATE|os.O_APPEND, 0600)
		if err != nil {
			return nil, fmt.Errorf("open log file: %w", err)
		}
		g.logfile = f
		l = logger.New(CustomLogger{writer: bufio.NewWriter(f)})
	} else {
		// use GinkgoWriter if no customer log file provided
		l = common.GinkgoLogger
	}
	return l, nil
}

func (c *configuration) terragruntDir(environmentPath string) string {
	p := filepath.Join(c.terraformDir, environmentPath)
	return p
}

func (t tfVars) encode() (map[string]string, error) {
	v := map[string]string{}
	err := mapstructure.Decode(t, &v)
	if err != nil {
		return nil, fmt.Errorf("convert tfvars to map: %w", err)
	}
	return v, nil
}

func (t tfVars) backendConfig() map[string]interface{} {
	const remoteStatePrefixTmpl = "%s/gitlab/terraform.tfstate"

	return map[string]interface{}{
		"bucket": t.BucketName,
		"prefix": fmt.Sprintf(remoteStatePrefixTmpl, t.RemoteStatePrefix),
	}
}
