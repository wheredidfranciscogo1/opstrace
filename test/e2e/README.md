# E2E Tests Suite

## Summary

The E2E test-suite aims to provide test-coverage for all application-features provided as part of the Gitlab Observability Component.
It allows for:

* testing platform and/or subsystem-specific features.
* testing provisioning & deprovisioning of the infrastructure that runs the platform via `terraform`/`terragrunt`.
* testing GitLab-specific integrations via a dedicated GitLab instance.

These tests are written using [ginkgo](https://onsi.github.io/ginkgo/).

Most tests have also been written through a single entrypoint - the creation of `Cluster`/`GitlabNamespace` custom resources similar to how our backend is intended to be used.

The environment variable `TEST_IDENTIFIER` is used to identify a given test-run.
It is used to name all resources provisioned by the test-suite.
It is also used to identify the `terraform` state path in the GCS bucket.
This variable is set to the `CI_COMMIT_SHORT_SHA` by default when running inside CI, or `tag-$CI_COMMIT_TAG` when running against a tagged release.
The e2e code normalizes this variable, replacing any non DNS compatible characters with `-`.

## Target Environments

The test-suite can be run against multiple infrastructure providers, namely:

### KIND (aka bring your own kubeconfig)

This is the default provider.

> When `kind` is used as the target provider, the test-suite does NOT install the `CRD`s required for the scheduler OR the `scheduler-manager` controller itself.
> These must be installed onto a target cluster beforehand.

The reason for this is to allow the user to decide how the cluster is set up, where the manager runs (could be on host machine, or in CI).

Instead of GDK, you can also use GitLab Compose Kit to run gitlab instance locally.
See [the guide](../../docs/guides/contributor/gitlab-compose-kit.md) for details.

Set these environment variables:

```bash
export gitlab_oauth_client_id="$your_oauth_application_client_id"
export gitlab_oauth_client_secret="$your_oauth_application_secret"
export internal_endpoint_token="$error_tracking_internal_endpoint_token"
export TEST_GOB_HOST="$custom_gob_host" # default localhost
export TEST_GITLAB_HOST="$custom_gitlab_host" # default gitlab.com
export TEST_GITLAB_SCHEME="$custom_gitlab_scheme" # default https
export TEST_GOB_SCHEME="$custom_gob_scheme" # default http
export TEST_GITLAB_ADMIN_TOKEN="$your_gitlab_admin_token"
```

When running the tests locally at the root of the project, run:

```bash
make kind deploy e2e-test
```

This sets up a `kind` cluster, deploys the scheduler and starts the tests.

If running against an existing cluster, with everything deployed, just do `make e2e-test`.


### Devvm

Compared to KIND, using [devvm](https://gitlab.com/gitlab-org/opstrace/devvm) as a target environment requires less setup.

You will need to set the following environment variables:

```bash
export TEST_SCHEDULER_IMAGE=$(make -sC ../../scheduler print-docker-images)
export TEST_TARGET_PROVIDER=devvm
export TEST_IDENTIFIER=<some unique identifier>
export TEST_GITLAB_EE_LICENSE_PATH=<path to EE license file>
```

Install frontend dependencies:

```bash
make -C test/e2e/ setup-frontend
```

Executing the e2e tests can be done with the following command. Note this will also create a new Devvm:

```bash
cd test/e2e
ginkgo run -v ./...
```

#### Re-using devvm while working with tests

If you want to run your tests against an existing devvm follow these guidelines:

Executing the e2e tests against the existing devvm (will also run Cypress tests):

```bash
export TEST_SKIP_CLEANUP=true # avoids teardown on completion
export TEST_SSH_KEY_PATH=<path to ssh-key>
export TEST_SSH_KEY_PASSPHRASE=<ssh-key passphrase in case one is needed.
cd test/e2e
ginkgo run -v ./...
```

By default, e2e tests create ephemeral ssh-keys that are lost once e2e tests finish executing.
Hence we need to provide our own ssh-key so that subsequent e2e tests runs can use to log into the devvm.
E2E could use GCE cloud-login, but this requires some more work and ssh-keys were much simpler to implement.
You can use your own ssh key or create a dedicated one, for example:

```
ssh-keygen -t ecdsa -f <path to ssh-key>
```

The`TEST_IDENTIFIER` env variable is used to construct devvm name like follows:

```
name = fmt.Sprintf("devvm-e2e-%s", testIdentifier)
```

If you want to run the Cypress tests interactively:

```bash
export TEST_GITLAB_ADMIN_TOKEN="$your_gitlab_admin_token" # Can be found in the booter output of your devvm as "API Token".
export TEST_GITLAB_ADDRESS="https://gdk.devvm:3443" # change if using a different host
export TEST_GOB_ADDRESS="https://gob.devvm" # change if using a different host
make -C test/e2e/ setup-frontend # install dependencies, can be skipped if already run and dependencies haven't changed
cd test/e2e/frontend
npm run cypress:open
```

#### Logging-into/debugging devvm

##### Cloud-login based login to devvm

Developers can log into the devvm using either the ssh key or GCE cloud-login.
Logging into cloud login is as simple as:
```
$ gcloud compute ssh <devvm-name>
$ sudo -i -u dev
```

##### SSH-Key based login to devvm

In order to log into devvm using an ssh-key you will need its public IP.
It can be obtained using [devvm tool](https://gitlab.com/gitlab-org/opstrace/devvm/#use-devvm-cli-tool):

```
$ ./devvm list
europe-west1-b
- arun-dev RUNNING 34.140.237.21
- arun-trace-q RUNNING 34.140.185.197
- pr-sandbox RUNNING 35.187.62.241
- rossetd-0510 RUNNING 34.38.125.33
- rossetd-1110 RUNNING 34.79.217.223
- rossetd-1110-2 RUNNING 34.79.43.26
us-east1-b
- devvm-1 RUNNING 34.23.49.153
us-west1-a
- mat RUNNING 34.83.121.46
us-west2-a
- devvm-e2e-pr-e2e-devvm-1 RUNNING 34.94.64.1
```

Once you have the IP, simply issue:

```
$ ssh -i ./devvm_key.ssh_key dev@1.2.3.4
Enter passphrase for key '/home/vespian/devvm_key.ssh_key':
Welcome to devvm-1!
(...)
dev@devvm-1:~$
```

More information can be obtained in [devvm's documentation](https://gitlab.com/gitlab-org/opstrace/devvm/#log-into-the-devvm).
The docs also contain many useful information about inspecting devvm's state.

#### Debugging live e2e tests

It is possible to log into Gitlab Runner, devvm as well as the docker container where the e2e test is executing.
Steps are as follows:
* log into the executor:
```
you@your-laptop:~# gcloud compute ssh runner-e2e-netadmin
you@runner-e2e-netadmin:~# sudo -i
root@runner-e2e-netadmin:~#
```
* list running docker containers, find the one where your CI is executing:
```
root@runner-e2e-netadmin:~# docker ps
CONTAINER ID   IMAGE          COMMAND                  CREATED          STATUS          PORTS           NAMES
5fd379b712ba   9a446e1ad4ad   "sh -c 'if [ -x /usr…"   21 minutes ago   Up 21 minutes                   runner-zum-bnzrh-project-32149347-concurrent-0-871d7231bb1dba9a-build
35f78ac69b93   a072474332af   "dockerd-entrypoint.…"   21 minutes ago   Up 21 minutes   2375-2376/tcp   runner-zum-bnzrh-project-32149347-concurrent-0-871d7231bb1dba9a-docker-0
root@runner-e2e-netadmin:~#
```
* exec into executor
```
root@runner-e2e-netadmin:~# docker exec -ti 5fd379b712ba /bin/bash
root@runner-zum-bnzrh-project-32149347-concurrent-0:/# ps axfw
    PID TTY      STAT   TIME COMMAND
  16698 pts/0    Ss     0:00 /bin/bash
  16704 pts/0    R+     0:00  \_ ps axfw
      1 ?        Ss     0:00 /bin/bash
     10 ?        S      0:00 /bin/bash
     30 ?        S      0:00  \_ make e2e-test-suite
    138 ?        S      0:00      \_ /bin/sh -c TEST_SCHEDULER_IMAGE=registry.gitlab.com/gitlab-org/opstrace/opstrace/scheduler:0.3.0-1c8204cf make -C test/e2e e2e-test
    139 ?        S      0:00          \_ make -C test/e2e e2e-test
    199 ?        Sl     0:14              \_ go run github.com/onsi/ginkgo/v2/ginkgo -v ./...
   1630 ?        Sl     0:00                  \_ /tmp/go-build2566513138/b001/exe/ginkgo -v ./...  16690 ?        Sl     0:00                      \_ /builds/gitlab-org/opstrace/opstrace/test/e2e/tests/tests.test --test.timeout=0 --ginkgo.seed=1697695367 --ginkgo.timeout=51m49.503630536s
root@runner-zum-bnzrh-project-32149347-concurrent-0:/#
```

Logging into devvm requires finding its name first:
```
$ ./devvm list
europe-west1-b
- arun-dev RUNNING 34.79.71.79
- arun-trace-q RUNNING 34.140.185.197
- pr-sandbox RUNNING 34.78.187.138
- rossetd-0510 RUNNING 34.38.125.33
- rossetd-1110 RUNNING 34.79.217.223
- rossetd-1110-2 RUNNING 34.79.43.26
us-east1-b
- devvm-1 RUNNING 34.23.49.153
us-west1-a
- mat RUNNING 34.83.121.46
us-west2-a
- devvm-e2e-1c8204cf RUNNING 35.235.100.158
```

We can use cloud-login then to ssh into it:
```
$ gcloud compute ssh devvm-e2e-1c8204cf
No zone specified. Using zone [us-west2-a] for instance: [devvm-e2e-1c8204cf].
Warning: Permanently added 'compute.4928208150464394909' (ED25519) to the list of known hosts.
Welcome to devvm-e2e-1c8204cf!
...
```

#### Implementation details

We are using a hybrid approach when connecting to devvm: e2e tests use port-forwarding through SSH, whereas k8s and http clients use SOCKS proxy.

![devvm_connection_overview](devvm_connection_diagram.drawio.png)

This way developers do not need to modify their code in order to be able to run it against devvm.
OTOH SOCKS library we use is mature and allows k8s and http clients to work transparently.
It is not possible to make all test use SOCKS proxy as this would require modifying tests and e.g. otel collector does not allow for custom transports, while e.g. cypress does not support SOCKS proxies at all.
Tunneling everything is doable, but not sure if it is worth it.
k8s/http clients "just work". TBD.

We also can't easily use Wireguard inside docker container that runs on Gitlab Runners.
Setting up wireguard differs wildly between Mac, Linux and CI/CD.
Automating it would be complex thing and very likelly there would still be some corner-cases we will not be able to cover.

#### FAQ:
* Q: Does devvm provisioner update scheduler image used during the tests when rerunning them against the same devvm?
  A: No, once provisioned, the devvm uses the same image.
     You can easily update it yourself if you log onto the devvm.
     Automating it is very much doable, just was not the part of the first iteration.
* Q: What about SSH-Agent support instead of setting passphrase via env-vars?
  A: Totally doable, just was not a priority during the first iteration. Contributions are welcome.
* Q: How long does it take to spin up a Devvm?
  A: Around 15 minutes.
* Q: 15 minutes is too long. Would it be possible to speed up spinning up Devvms?
  A: Yes, but this would require changes in how we deploy GOB, including but not limited to:
     * pararelize the download of containers
     * pararelize deploying GOB components in the scheduler
     There are no low-hanging fruits left in devvm anymore.
* Q: I broke my devvm during tests. What should I do.
  A: If you are lazy or do not care much - just delete the devvm and let e2e tests create a new one.
     ```
     $ ./devvm rm -z us-west2-a devvm-e2e-pr-01
     Deleting instance devvm-e2e-pr-01... - OK!
     $
     ```
     If you do not want to wait, you can simply change the image reference used by scheduler's deployment.
     Instructions on how to connect to devvm using kubectl can be found in the [devvm's documentation](https://gitlab.com/gitlab-org/opstrace/devvm/#set-up-kubernetes-configuration).

### GCP

> When `GCP` is used as the target provider, the test-suite does provision all the necessary infrastructure, i.e DNS entries, GCP cloud instance to run a self-managed version of Gitlab & a GKE cluster to host all platform resources with a bunch of additional resources as well.

[Application Default Credentials](https://cloud.google.com/docs/authentication/application-default-credentials) will be assumed when running the tests.

The test-suite uses the same `terraform`/`terragrunt` setup as the one
we use for provisioning our own `prod`/`staging` environments. This helps us test the correctness of our provisioning machinery in addition to the tests targeting application-level correctness.

When running against GCP, the setup needs access to the underlying
credentials/environment variables. At the root of the project, run:

```bash
export TF_VAR_registry_username="$secret"
export TF_VAR_registry_auth_token="$secret"
export TEST_IDENTIFIER="$testSHA"
export TEST_TARGET_PROVIDER="gcp"
make e2e-test
```

## Writing (new) tests

In principle, we try to write tests through external interfaces (e.g. ingress) only when testing user-facing features.

Use the custom resources of the scheduler (`Cluster` and `GitLabNamespace`) to create necessary cluster/tenant state. In cases where applicable, this allows us to set up tenant resources without actually exercising the provisioning step via GitLab OAuth.

## E2E Helpers

The E2E test-suite is built around a series of helpers that abstracts a lot of details around resource provisioning, state management and testing utilities.
It allows for a developer to only focus on the logic that's being tested and the involved test-code.

[Namespace tests](./tests/namespace_test.go) are a good example of how to use these helpers.

## Infrastructure

When using a non-local environment provider, e.g. GCP, the test-suite spins up infrastructure resources (mostly) managed via `terraform`/`terragrunt` right now. The backing `terraform` state is also stored remotely inside a GCS bucket with each test-run using a dedicated path in the bucket, for instance:

```
gs://$BUCKET_NAME/$TEST_IDENTIFIER/$COMPONENT/terraform.tfstate/
```

Therefore, state specific to a given test-run is managed in complete isolation providing system debuggability should one need to access an inventory of all provisioned resources. Note, both `$BUCKET_NAME` and `$PATH_PREFIX` (`TEST_IDENTIFIER` above) are configurable via environment variables as seen [here](https://gitlab.com/gitlab-org/opstrace/opstrace/-/blob/main/    terraform/environments/integration/terragrunt.hcl).

Leveraging `terraform` also ensures managed provisioning & deprovisioning of all resources backing our tests. In some cases though, the test-suite can be interrupted abruptly leaving resources orphaned. To mitigate against it, the test-suite has been written in a way to be retried at any point in time.

Since underlying resources are tagged with the `CI_COMMIT_SHORT_SHA` the tests were first run against, when a test run is re-run against the same `CI_COMMIT_SHORT_SHA` (via `TEST_IDENTIFIER`) but with a non-breaking `scheduler` image, it attempts to setup & teardown the same set of resources again. This ensures that the suite runs to completion and eventually cleanup any orphaned resources. This makes it trivial for this feature to be leveraged inside an "out-of-band reaper script" to ensure that the project stays in good hygiene at all times.

This also allows for a non-local provider to be targeted outside of your CI infrastructure by simply providing the right environment variables and/or project credentials to run the tests against. Refer to using [GCP](#gcp) section above.

> Set `TEST_SKIP_CLEANUP` env var to any value to skip cleanup of resources after a test run.

## Manual Destroy

It's common for the cleanup phase to fail due to broken `Cluster` CR state, if a bug has been introduced,
or if for some reason the kustomize resource state is out of sync with the actual state of the cluster.

In this case, you can manually destroy the cluster by running:

```bash
make destroy
```

This is also available as a GitLab pipeline job, which can be triggered through the web UI.
Set `TEST_IDENTIFIER_OVERRIDE` to override the default identifier in the pipeline job.

Set `TEST_IDENTIFIER` when running locally to target a particular test run.

## Debugging Terraform

To use `terraform` or `terragrunt` to inspect the state of the infrastructure, you'll first need all the relevant environment variables.

You can use the `make` target `printenv` to print all the relevant environment variables, assuming you have first set the correct `TEST_IDENTIFIER` var.

You can export all the environment variables to your shell by running:

```bash
export $(make printenv | xargs)
```
