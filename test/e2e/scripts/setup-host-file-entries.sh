#!/bin/bash

set -eu -o pipefail

key="${TEST_TARGET_PROVIDER:-kind}-$(uname -s)"
# CI/CD runs as root and there is no sudo installed
if [ $(whoami) = "root" ]; then export SUDO_CMD=""; else export SUDO_CMD=sudo; fi
case "${key}" in
    devvm-Linux*)
        if grep -qE "^${GDK_IP} " /etc/hosts
        then
            echo "gdk.devvm entry found in /etc/hosts"
        else
            echo "${GDK_IP} gdk.devvm" | ${SUDO_CMD} tee -a /etc/hosts > /dev/null
        fi
        if grep -qE "^${GOB_IP} " /etc/hosts
        then
            echo "gob.devvm entry found in /etc/hosts"
        else
            echo "${GOB_IP} gob.devvm" | ${SUDO_CMD} tee -a /etc/hosts > /dev/null
        fi
        echo "host entries ensured"
        ;;
    devvm-Darwin*)
        echo "Hello fellow traveler! The /etc/hosts setup scripts currently work on Linux only."
        echo "A brave soul that uses Mac will need to extend it if you want this automated :)"
        echo "I did not write it myself as I would not be able to test it - I do not have Mac."
        echo "Use the linux code as documentation of what is needed."
        echo "Without iptables and hosts-file setup, devvm-based e2e tests will not work."
        false
        ;;
    *)
        echo "Skipping hosts-file setup."
        ;;
esac
