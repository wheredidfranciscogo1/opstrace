#!/bin/bash

set -eu -o pipefail

key="${TEST_TARGET_PROVIDER:-kind}-$(uname -s)"
# CI/CD runs as root and there is no sudo installed
if [ $(whoami) = "root" ]; then export SUDO_CMD=""; else export SUDO_CMD=sudo; fi
case "${key}" in
    devvm-Linux*)
        OUTBOUND_IP=$(ip r get 8.8.8.8  | grep -oP "src\s\K\d+\.\d+\.\d+\.\d+")
        ${SUDO_CMD} iptables-save -t nat 2>&1 | egrep "PREROUTING.*to-destination ${OUTBOUND_IP}:56790" > /dev/null || \
            ${SUDO_CMD} iptables -t nat -A PREROUTING -d ${GDK_IP}/32 -p tcp -m tcp --dport 3443 -j DNAT --to-destination ${OUTBOUND_IP}:56790
        ${SUDO_CMD} iptables-save -t nat 2>&1 | egrep -E "OUTPUT.*to-destination ${OUTBOUND_IP}:56790" > /dev/null || \
            ${SUDO_CMD} iptables -t nat -A OUTPUT -d ${GDK_IP}/32 -p tcp -m tcp --dport 3443 -j DNAT --to-destination ${OUTBOUND_IP}:56790
        ${SUDO_CMD} iptables-save -t nat 2>&1 | egrep -E "PREROUTING.*to-destination ${OUTBOUND_IP}:56789" > /dev/null || \
            ${SUDO_CMD} iptables -t nat -A PREROUTING -d ${GOB_IP}/32 -p tcp -m tcp --dport 443 -j DNAT --to-destination ${OUTBOUND_IP}:56789
        ${SUDO_CMD} iptables-save -t nat 2>&1 | egrep -E "OUTPUT.*to-destination ${OUTBOUND_IP}:56789" > /dev/null || \
            ${SUDO_CMD} iptables -t nat -A OUTPUT -d ${GOB_IP}/32 -p tcp -m tcp --dport 443 -j DNAT --to-destination ${OUTBOUND_IP}:56789
        echo "DNAT rules were ensured"
        ;;
    devvm-Darwin*)
        echo "Hello fellow traveler! The iptables setup scripts currently work on Linux only."
        echo "A brave soul that uses Mac will need to extend it if you want this automated :)"
        echo "I did not write it myself as I would not be able to test it - I do not have Mac."
        echo "Use the linux code as documentation of what is needed."
        echo "Without iptables and hosts-file setup, devvm-based e2e tests will not work."
        false
        ;;
    *)
        echo "Skipping iptables setup."
        ;;
esac
