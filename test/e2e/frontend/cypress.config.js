const { defineConfig } = require("cypress");

module.exports = defineConfig({
  fixturesFolder: false,
  defaultCommandTimeout: 10000,
  requestTimeout: 10000,
  retries: 2,
  e2e: {
    video: true,
    baseUrl: process.env.TEST_GITLAB_ADDRESS,
    specPattern: [
      // run the enable test first so other tests
      // can use the enabled tenant. Tests are run
      // in same order as this specPattern array
      "suite/specs/**/enable.cy.js",
      // other tests
      "suite/specs/**/*.cy.{js,jsx,ts,tsx}"
    ],
    setupNodeEvents(on, config) {
      config.env = config.env || {};
      // setup configuration env
      TEST_GITLAB_ADDRESS = process.env.TEST_GITLAB_ADDRESS;
      TEST_GOB_ADDRESS = process.env.TEST_GOB_ADDRESS;
      TEST_GITLAB_ADMIN_TOKEN = process.env.TEST_GITLAB_ADMIN_TOKEN;

      on("before:run", () => {
        assertVar("TEST_GITLAB_ADDRESS", TEST_GITLAB_ADDRESS);
        assertVar("TEST_GOB_ADDRESS", TEST_GOB_ADDRESS);
        assertVar("TEST_GITLAB_ADMIN_TOKEN", TEST_GITLAB_ADMIN_TOKEN);
      });

      config.env.TEST_GITLAB_ADDRESS = TEST_GITLAB_ADDRESS;
      config.env.TEST_GOB_ADDRESS = TEST_GOB_ADDRESS;
      config.env.TEST_GITLAB_ADMIN_TOKEN = TEST_GITLAB_ADMIN_TOKEN;

      return config;
    },
  },
});

function assertVar(k, v) {
  if (!v) {
    const msg = `Error: ${k} env var must be specified`;
    console.log(msg);
    throw msg;
  }
}