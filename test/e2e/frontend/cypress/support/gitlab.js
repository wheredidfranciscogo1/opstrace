
const { Gitlab } = require("@gitbeaker/rest");

const userIDs = {};
const resources = {};
const tokens = {};
const sentryDSNs = {};

function getClient() {
  return new Gitlab({
    host: Cypress.env("TEST_GITLAB_ADDRESS"),
    token: Cypress.env("TEST_GITLAB_ADMIN_TOKEN")
  });
}

/**
 * Gets the group by name. Creates it if doesn't already exist.
 * @param {string} name
 * @returns {id: integer}
 */
export function getGroup(name) {
  return new Promise(res => {
    const respond = () => res(resources[name]);
    const g = getClient();

    if (resources[name] && resources[name].id != null) {
      respond();
    } else {
      g.Groups.all({ search: name }).then(groups => {
        groups.forEach(gr => {
          if (resources[gr.name] == undefined) {
            resources[gr.name] = {};
          }
          resources[gr.name].id = gr.id;
        });

        if (resources[name] && resources[name].id != null) {
          respond();
        } else {
          g.Groups.create(name, name).then(gr => {
            resources[name] = { id: gr.id };
            respond();
          });
        }
      });
    }
  });
};

/**
 * Gets the project by path. Only supports projects nested one level deep under
 * a root group. Will create the root group and project if it doesn't exist.
 * @param {string} path
 * @returns {id: integer}
 */
export function getProject(path) {
  const parts = path.split("/");
  if (parts.length != 2) {
    throw new Error("only supports <root-group-name>/<project-name>");
  }
  const [group, name] = parts;

  return new Promise(res => {
    getGroup(group).then(gr => {
      const respond = () => res(gr[name]);
      const g = getClient();
      if (gr[name] && gr[name].id != null) {
        respond();
      } else {
        g.Groups.allProjects(gr.id, { search: name }).then(projects => {
          projects.forEach(p => {
            if (gr[p.name] == undefined) {
              gr[p.name] = {}
            }
            gr[p.name].id = p.id;
            gr[p.name].groupID = gr.id;
          });

          if (gr[name] && gr[name].id != null) {
            respond();
          } else {
            g.Projects.create({ name, namespaceId: gr.id }).then(p => {
              if (gr[name] == undefined) {
                gr[name] = {}
              }
              gr[name].id = p.id;
              gr[p.name].groupID = gr.id;
              respond();
            });
          }
        });
      }
    });
  });
};

/**
 * Gets test user. Creates if does not exist.
 * @returns {id: integer, username: string, password: string}
 */
export function getUser(user) {
  const { username, password } = user;
  return new Promise(res => {
    const respond = () => {
      // mutate this to make it available in tests
      user.id = userIDs[username];
      res(user);
    };

    if (userIDs[username]) {
      respond();
    } else {
      const g = getClient();
      g.Users.all({ username }).then(users => {
        users.forEach(u => {
          userIDs[u.username] = u.id;
        });
        if (userIDs[username]) {
          respond();
        } else {
          g.Users.create({
            password,
            username,
            name: username,
            skipConfirmation: true,
            email: `${username}@gitlab.local`
          }).then(u => {
            userIDs[username] = u.id;
            respond();
          })
        }
      })
    }
  })
};

/**
 * Adds user to project and ensures desired access level
 * @param {integer} projectID
 * @param {integer} userID
 * @param {integer} accessLevel
 * @returns
 */
export function addProjectMembership(projectID, userID, accessLevel) {
  return new Promise(res => {
    const g = getClient();
    g.ProjectMembers.show(projectID, userID)
      .then(() => g.ProjectMembers.edit(projectID, userID, accessLevel).then(res))
      .catch(() => g.ProjectMembers.add(projectID, userID, accessLevel).then(res))
  });
};

/**
 * Returns a project access token for API use. Creates if doesn't exist.
 * @param {string} projectID
 * @param {string} name
 * @param {array[string]} scopes
 * @returns
 */
export function getProjectAccessToken(projectID, name, scopes) {
  return new Promise(res => {
    const respond = () => {
      res(tokens[projectID][name]);
    };

    if (tokens[projectID] && tokens[projectID][name]) {
      respond();
    } else {
      const g = getClient();
      g.ProjectAccessTokens.all(projectID).then(tokenList => {
        if (!tokens[projectID]) {
          tokens[projectID] = {};
        }
        tokenList.forEach(t => {
          tokens[projectID][t.name] = t.token;
        });
        if (tokens[projectID] && tokens[projectID][name]) {
          respond();
        } else {
          const currentDate = new Date();
          const elevenMonthsFromNow = new Date(currentDate);
          elevenMonthsFromNow.setMonth(currentDate.getMonth() + 11);

          const isoStringElevenMonthsFromNow = elevenMonthsFromNow.toISOString();
          g.ProjectAccessTokens.create(
            projectID,
            name,
            scopes,
            {
              accessLevel: 30,
              expiresAt: isoStringElevenMonthsFromNow
            }
          ).then(t => {
            tokens[projectID][name] = t.token;
            respond();
          })
        }
      })
    }
  });
};

/**
 * Returns the sentry DSN for this project. Creates if
 * there are no active DSNs.
 * @param {integer} projectID
 * @returns
 */
export function getProjectSentryDSN(projectID) {
  return new Promise(res => {
    const respond = () => {
      res(sentryDSNs[projectID]);
    };

    if (sentryDSNs[projectID]) {
      respond();
    } else {
      const g = getClient();
      enableErrorTracking().then(() => {
        g.ErrorTrackingSettings.edit(projectID, true, { integrated: true })
          .then(() => g.ErrorTrackingClientKeys.all(projectID).then((keys) => {
            keys.forEach(k => {
              if (k.active) {
                sentryDSNs[projectID] = k.sentry_dsn;
              }
            });
            if (sentryDSNs[projectID]) {
              respond();
            } else {
              g.ErrorTrackingClientKeys.create(projectID).then(key => {
                sentryDSNs[projectID] = key.sentry_dsn;
                respond();
              })
            }
          }))
          .catch(() => {
            g.ErrorTrackingSettings.create(projectID, true, true).then(() => {
              g.ErrorTrackingClientKeys.create(projectID).then(key => {
                sentryDSNs[projectID] = key.sentry_dsn;
                respond();
              })
            });
          })
      });
    }
  });
};

/**
 * Enables error tracking in application settings
 */
function enableErrorTracking() {
  return new Promise(res => {
    const g = getClient();
    g.ApplicationSettings.show()
      .then(s => {
        if (s["error_tracking_enabled"] == true) {
          res();
        } else {
          g.ApplicationSettings.edit({
            error_tracking_enabled: "true",
            error_tracking_api_url: Cypress.env("TEST_GOB_ADDRESS")
          }).then(res);
        }
      })
  })
};