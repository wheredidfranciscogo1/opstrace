import * as gitlab from "./gitlab";

Cypress.Commands.add("loginWithUser", (user) => {
  if (!user.password) {
    throw Error("user.password not specified");
  }
  if (!user.username) {
    throw Error("user.username not specified");
  }
  cy.wrap(null).then(() => gitlab.getUser(user).then(u => {
    cy.session([u.username, u.password], () => {
      cy.visit("/users/sign_in");
      cy.get("[data-testid=username-field]").type(u.username);
      cy.get("[data-testid=password-field]").type(u.password);
      cy.get("[data-testid=sign-in-button]").click();
      cy.url().should("eq", `${Cypress.env("TEST_GITLAB_ADDRESS")}/`);
    });
  }));
});

Cypress.Commands.add("gitlabSearchFilters", () => {
  return cy.get("[data-testid=filtered-search-token]");
});

Cypress.Commands.add("gitLabSortBy", () => {
  return cy.get(".sort-dropdown-container");
});

Cypress.Commands.add("getUser", (user) => {
  cy.wrap(null).then(() => gitlab.getUser(user))
});

Cypress.Commands.add("getProject", (path) => {
  cy.wrap(null).then(() => gitlab.getProject(path))
});

Cypress.Commands.add("getGroup", (path) => {
  cy.wrap(null).then(() => gitlab.getGroup(path))
});

Cypress.Commands.add("addProjectMembership", (projectID, userID, accessLevel) => {
  cy.wrap(null).then(() => gitlab.addProjectMembership(projectID, userID, accessLevel))
});

Cypress.Commands.add("getProjectAccessToken", (projectID, name, scopes) => {
  cy.wrap(null).then(() => gitlab.getProjectAccessToken(projectID, name, scopes))
});

Cypress.Commands.add("getProjectSentryDSN", (projectID) => {
  cy.wrap(null).then(() => gitlab.getProjectSentryDSN(projectID))
});