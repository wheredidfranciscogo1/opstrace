import {
  defaultUser,
  metricsTestProject,
  tracingTestProject
} from "./constants";

describe("enable_observability", () => {
  beforeEach(() => {
    cy.loginWithUser(defaultUser);
  });

  describe("enablement page", () => {
    [
      {
        signal: "tracing",
        projectPath: tracingTestProject
      },
      {
        signal: "metrics",
        projectPath: metricsTestProject
      }
    ].forEach(({ signal, projectPath }) =>
      it(`can be enabled on ${signal} page`, () => {
        // make sure test project exists
        cy.getProject(projectPath).then((project) => {
          cy.addProjectMembership(project.id, defaultUser.id, 30).then(() => {

            cy.visit(`${projectPath}/-/${signal}`);
            cy.request({
              url: `${Cypress.env("TEST_GOB_ADDRESS")}/v3/tenant/${project.id}`,
              failOnStatusCode: false
            }).then(res => {
              // When running tests multiple times against the same instance,
              // this could already be enabled. In that case, just skip the test.
              // When running in CI, alreadyEnabled will always be false because
              // tests run against a fresh instance.
              if (res.status == 404) {
                cy.contains(".gl-button-text", "Enable")
                  .should("be.visible")
                  .click();
              }

              // check table view rendered.
              // TODO: add a testid to this page in GitLab UI to test against instead
              // of the following if/else block
              if (signal == "tracing") {
                cy.contains("Date");
                cy.contains("Service");
                cy.contains("Operation");
                cy.contains("Duration");
              } else if (signal == "metrics") {
                cy.contains("Name");
                cy.contains("Description");
                cy.contains("Type");
              } else {
                throw Error(`signal not tested for: ${signal}`);
              }

            });
          });
        })
      })
    )
  });
});

