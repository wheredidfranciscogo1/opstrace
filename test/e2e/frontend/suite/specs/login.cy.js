import { defaultUser } from "./constants";

describe("login", function () {
  it("the home page should redirect for authorisation for non logged-in users", () => {
    cy.request({
      url: "/",
      followRedirect: false,
    }).then((resp) => {
      expect(resp.status).to.eq(302);
      expect(resp.redirectedToUrl).to.eq(
        `${Cypress.env("TEST_GITLAB_ADDRESS")}/users/sign_in`
      );
    });
  });

  it("successfully logs in", () => {
    cy.loginWithUser(defaultUser);
  });
});
