import { v4 } from "uuid";
import dateFormat from "dateformat";
import { defaultUser, tracingTestProject } from "./constants";

describe("tracing", () => {
  const now = new Date();
  // for now our trace generator rounds to the
  // nearest second so this ensures we can test
  // for the right value to show on the trace detail page
  now.setMilliseconds(0);

  const traces = [
    {
      id: generateTraceID(),
      duration: 400000000,
      start: now
    },
    {
      id: generateTraceID(),
      duration: 450000000,
      start: new Date(now - 60000) // 1 min ago
    },
    {
      id: generateTraceID(),
      duration: 410000000,
      start: new Date(now - 120000) // 2 mins ago
    }
  ];

  before(() => {
    // get user first to ensure "id" is injected into user object
    cy.getUser(defaultUser).then(() => {
      cy.getProject(tracingTestProject).then(project => {
        cy.addProjectMembership(project.id, defaultUser.id, 30).then(() => {
          cy.getProjectAccessToken(project.id, "trace-write", ["read_api", "write_observability"]).then(token => {
            const endpoint = `${Cypress.env("TEST_GOB_ADDRESS")}/v3/${project.groupID}/${project.id}/ingest/traces`;
            seedTraces(token, endpoint, traces);
            cy.wait(5000);
          });
        });
      });
    });
  });

  beforeEach(() => {
    cy.loginWithUser(defaultUser);
  });

  describe("tracing list page", () => {
    it("contains correct content for row", () => {
      cy.visit(`${tracingTestProject}/-/tracing`);
      // default params
      cy.url().should("include", "/-/tracing?period[]=1h&sortBy=timestamp_desc");

      // test filters show in filter bar
      cy.gitlabSearchFilters()
        .contains("Time range = 1h")
        .should("be.visible");
      // test correct sort directive rendered
      cy.gitLabSortBy()
        .contains("Timestamp")
        .should("be.visible");
      cy.gitLabSortBy()
        .get("[data-testid=sort-highest-icon]")
        .should("be.visible");

      // const nowFormattedString = formatDate(traces[0].start);
      const traceService = getTraceService(traces[0].id);

      cy.contains(traceService)
        .parents("tr").as("row");
      

      // TODO temporarily skipping this as it causes flakiness https://gitlab.com/gitlab-org/opstrace/opstrace/-/issues/2615
      // cy.get("@row").contains(nowFormattedString)
      //   .should("be.visible");

      cy.get("@row").contains("article-to-cart")
        .should("be.visible");

      cy.get("@row").contains("400ms")
        .should("be.visible");

      cy.contains("Failed to load traces")
        .should("not.exist");
    });

    describe("period filters", () => {
      const filters = {
        "5m": "Time range = 5m",
        "15m": "Time range = 15m",
        "30m": "Time range = 30m",
        "1h": "Time range = 1h",
        "4h": "Time range = 4h",
        "12h": "Time range = 12h",
        "24h": "Time range = 24h",
        "7d": "Time range = 7d",
        "14d": "Time range = 14d",
        "30d": "Time range = 30d",
      }
      Object.entries(filters).forEach(([period, filterText]) => {
        it(`filters ${period} correctly`, () => {
          cy.getProject(tracingTestProject).then(project => {
            cy.visit(`${tracingTestProject}/-/tracing?period[]=${period}`);
            // intercept the API request
            cy.intercept(`/v3/query/${project.id}/traces*`, (req) => {
              // expect the correct query param to be passed to the API
              expect(req.query.period).to.eq(period);
              req.continue();
            });
            // test filters show in filter bar
            cy.gitlabSearchFilters()
              .contains(filterText)
              .should("be.visible");

            cy.contains("Failed to load traces")
              .should("not.exist");
          });
        });
      })
    });

    describe("sorting", () => {
      [
        {
          param: "timestamp_desc",
          text: "Timestamp",
          icon: "sort-highest-icon"
        },
        {
          param: "timestamp_asc",
          text: "Timestamp",
          icon: "sort-lowest-icon"
        },
        {
          param: "duration_desc",
          text: "Duration",
          icon: "sort-highest-icon"
        },
        {
          param: "duration_asc",
          text: "Duration",
          icon: "sort-lowest-icon"
        }
      ].forEach(sort => {
        it(`sorts ${sort.param} correctly`, () => {
          cy.getProject(tracingTestProject).then(project => {
            cy.visit(`${tracingTestProject}/-/tracing?sortBy=${sort.param}`);
            // intercept the API request
            cy.intercept(`/v3/query/${project.id}/traces*`, (req) => {
              // expect the correct query param to be passed to the API
              expect(req.query.sort).to.eq(sort.param);
              req.continue();
            });
            // test sort renders correctly
            cy.gitLabSortBy()
              .contains(sort.text)
              .should("be.visible");

            cy.gitLabSortBy()
              .get(`[data-testid=${sort.icon}]`)
              .should("be.visible");

            cy.contains("Failed to load traces")
              .should("not.exist");

          })
        });
      });
    });

    describe("trace detail", () => {
      it("opens from trace list", () => {
        cy.visit(`${tracingTestProject}/-/tracing`);

        const traceService = getTraceService(traces[0].id);
        cy.contains(traceService).click();

        cy.url().should("include", `/-/tracing/${traces[0].id}`);
      });

      it("has correct metadata", () => {
        cy.visit(`${tracingTestProject}/-/tracing/${traces[0].id}`);


        cy.get("[data-testid=trace-date-card]")
          .contains(formatDate(traces[0].start, 'mmm d, yyyy'));
        cy.get("[data-testid=trace-date-card]")
          .contains(formatDate(traces[0].start, 'H:MM:ss.l Z'));

        cy.get("[data-testid=trace-duration-card]")
          .contains("400ms");

        cy.get("[data-testid=trace-spans-card]")
          .contains("10");
      })
    });
  });
});

function seedTraces(token, endpoint, traces) {
  let cmd = `go run ../generators/trace.go -token ${token} -endpoint ${endpoint}`;
  traces.forEach(trace => {
    const traceID = trace.id;
    const nowMs = Math.round(trace.start.getTime() / 1000);

    cmd += ` ${traceID}:${nowMs}:${trace.duration}:${getTraceService(traceID)}`;
  })
  cy.log(cmd)
  cy.exec(cmd, { failOnNonZeroExit: false }).then(res => {
    expect(res.code).to.eq(0, `seedTraces exit code ${res.code}: ${res.stderr}`);
  })

};

function getTraceService(id) {
  return `test-${id}`;
}

// generates a random traceID
function generateTraceID() {
  return v4();
}

// formats datetime to be same as GitLab UI
function formatDate(datetime, fmt = 'mmm d, yyyy h:MMtt Z') {
  return dateFormat(datetime.toISOString(), fmt, false)
}