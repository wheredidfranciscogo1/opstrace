import * as Sentry from "@sentry/browser";
import { defaultUser, errorTrackingTestProject } from "./constants";

describe("error tracking", () => {
  beforeEach(() => {
    cy.loginWithUser(defaultUser);
  });

  it("error tracking smoke test", () => {
    cy.getProject(errorTrackingTestProject).then(project => {
      cy.addProjectMembership(project.id, defaultUser.id, 30).then(() => {
        cy.getProjectSentryDSN(project.id).then(dsn => {
          Sentry.init({
            dsn,
            debug: true
          });
          const errorText = "My test failure";
          // reports an error
          failureFromE2ETest(errorText);

          cy.visit(`${errorTrackingTestProject}/-/error_tracking`);

          // check error appears in the error list
          cy.contains(errorText, { timeout: 20000 })
            .should("be.visible")
            .click();

          // sanity check for error details
          cy.contains("Events").should("be.visible");
          cy.contains("Stack trace").should("be.visible");
          cy.contains("error_tracking.cy.js").should("be.visible");
        });
      });
    });
  });
});

function failureFromE2ETest(errorText) {
  Sentry.captureException(new Error(errorText));
}
