package tests

import (
	"context"
	"fmt"
	"net/http"
	"strings"
	"time"

	"github.com/davecgh/go-spew/spew"
	. "github.com/onsi/ginkgo/v2"
	. "github.com/onsi/gomega"
	gogitlab "github.com/xanzy/go-gitlab"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/common"
)

// gitLab specific configuration for the test run
// each run will generate a new project, all other resources
// will only be created if they don't exist.
var gitLab struct {
	// user is set up with the credentials for the test run
	user         *gogitlab.User
	userPassword string
	// user read only token to use for access testing.
	userReadOnlyToken *gogitlab.PersonalAccessToken

	//NOTE(prozlach): Some of the tests require tenant to be pre-provisioned
	//while others do not. Hence we provision two groups: main and an
	//auxiliary. The main group has e.g. gitlab observability tenant
	//pre-provisioned.
	group               *gogitlab.Group
	project             *gogitlab.Project
	groupReadOnlyToken  *gogitlab.GroupAccessToken
	groupWriteOnlyToken *gogitlab.GroupAccessToken
	groupReadWriteToken *gogitlab.GroupAccessToken

	auxGroup               *gogitlab.Group
	auxProject             *gogitlab.Project
	auxGroupReadOnlyToken  *gogitlab.GroupAccessToken
	auxGroupWriteOnlyToken *gogitlab.GroupAccessToken
	auxGroupReadWriteToken *gogitlab.GroupAccessToken

	// error tracking configuration for the test run
	errorTrackingConfig struct {
		clientKey    *gogitlab.ErrorTrackingClientKey
		listEndpoint string
	}
}

// Init GitLab group/project and sentry related settings.
// Should only be called from suite setup.
func initGitLab(ctx context.Context) {
	Expect(testInfra.Configuration().GitLabAdminToken).NotTo(BeEmpty())
	// Test asserts actions using gitlab API like enabling error tracking which shouldn't be done on prod env yet.
	Expect(strings.Index(testInfra.Configuration().GitLabHost, "gitlab.com")).To(Equal(-1))

	By("Creating a GitLab API client")
	glClient, err := gogitlab.NewClient(
		testInfra.Configuration().GitLabAdminToken,
		gogitlab.WithBaseURL(testInfra.Configuration().GitLabAddress()),
		gogitlab.WithHTTPClient(httpClient),
		gogitlab.WithRequestOptions(gogitlab.WithContext(ctx)),
		gogitlab.WithCustomLogger(&glClientLogger{}),
	)
	Expect(err).ToNot(HaveOccurred())

	By("Enabling error tracking on GitLab via application settings")
	req, err := glClient.NewRequest(http.MethodPut, "application/settings", nil, nil)
	Expect(err).ToNot(HaveOccurred())

	q := req.URL.Query()
	q.Add("error_tracking_enabled", "true")
	q.Add("error_tracking_api_url", testInfra.Configuration().GOBAddress())
	req.URL.RawQuery = q.Encode()

	_, err = glClient.Do(req, nil)
	Expect(err).ToNot(HaveOccurred())

	By(fmt.Sprintf("Creating a GitLab user %q", testIdentifier))
	// generate a password that GitLab will accept.
	// it doesn't like pure alpha passwords, even if random.
	userPassword := common.ClickHouseCloudPassword(10)

	user, _, err := glClient.Users.CreateUser(&gogitlab.CreateUserOptions{
		Email:            gogitlab.String(fmt.Sprintf("test-%s@%s", testIdentifier, testInfra.Configuration().GitLabHost)),
		Password:         gogitlab.String(userPassword),
		Username:         gogitlab.String(testIdentifier),
		Name:             gogitlab.String(testIdentifier),
		SkipConfirmation: gogitlab.Bool(true),
	})
	Expect(err).ToNot(HaveOccurred())

	DeferCleanup(func(ctx SpecContext) {
		By(fmt.Sprintf("Deleting the test GitLab user %q", testIdentifier))
		_, err := glClient.Users.DeleteUser(user.ID, gogitlab.WithContext(ctx))
		Expect(err).ToNot(HaveOccurred())
	})

	expiresAt := gogitlab.ISOTime(time.Now().Add(time.Hour * 24))
	scopes := []string{"read_api"}
	pat, _, err := glClient.Users.CreatePersonalAccessToken(user.ID, &gogitlab.CreatePersonalAccessTokenOptions{
		Name:      gogitlab.String("readonly"),
		ExpiresAt: &expiresAt,
		Scopes:    &scopes,
	})
	Expect(err).ToNot(HaveOccurred())

	gitLab.user = user
	gitLab.userPassword = userPassword
	gitLab.userReadOnlyToken = pat

	//nolint:contextcheck // the client already captures the context
	initTestGroup(
		"e2e-main-testgroup-"+testIdentifier, "e2e-main-testproject-"+testIdentifier,
		&gitLab.group, &gitLab.project,
		&gitLab.groupReadOnlyToken, &gitLab.groupWriteOnlyToken, &gitLab.groupReadWriteToken,
		glClient,
	)
	//nolint:contextcheck // the client already captures the context
	initTestGroup(
		"e2e-aux-testgroup-"+testIdentifier, "e2e-aux-testproject-"+testIdentifier,
		&gitLab.auxGroup, &gitLab.auxProject,
		&gitLab.auxGroupReadOnlyToken, &gitLab.auxGroupWriteOnlyToken, &gitLab.auxGroupReadWriteToken,
		glClient,
	)
	initErrorTracking(glClient)
}

// Init a test group and project for this test.
// Reuse a single group to keep the number of groups in the system low.
// Create relevant tokens here too.
func initTestGroup(
	groupName, projectName string,
	groupVar **gogitlab.Group, projectVar **gogitlab.Project,
	groupReadOnlyTokenVar, groupWriteOnlyTokenVar, groupReadWriteTokenVar **gogitlab.GroupAccessToken,
	client *gogitlab.Client,
) {
	var err error

	By(fmt.Sprintf("Ensuring test group %q", groupName))
	g, resp, err := client.Groups.GetGroup(groupName, nil)
	if err != nil {
		if resp != nil && resp.StatusCode == http.StatusNotFound {
			g, _, err = client.Groups.CreateGroup(&gogitlab.CreateGroupOptions{
				Name: &groupName,
				Path: &groupName,
			})
			Expect(err).ToNot(HaveOccurred())
		} else {
			Fail(fmt.Sprintf("Failed to fetch information about group %q: %v", groupName, err))
		}
	}
	*groupVar = g

	By(fmt.Sprintf("Adding user %q to the group %q as a developer", gitLab.user.Username, groupName))
	_, _, err = client.GroupMembers.AddGroupMember(
		(*groupVar).ID,
		&gogitlab.AddGroupMemberOptions{
			UserID:      &gitLab.user.ID,
			AccessLevel: gogitlab.AccessLevel(gogitlab.DeveloperPermissions),
		},
	)
	Expect(err).ToNot(HaveOccurred())

	By(fmt.Sprintf("creating test project %q in the group %q", projectName, groupName))
	*projectVar, _, err = client.Projects.CreateProject(
		&gogitlab.CreateProjectOptions{
			Name:        &projectName,
			NamespaceID: &((*groupVar).ID),
		},
	)
	Expect(err).ToNot(HaveOccurred())
	By(fmt.Sprintf("test project %q created in the group %q with ID %d", projectName, groupName, (*projectVar).ID))

	DeferCleanup(func(ctx SpecContext) {
		By(fmt.Sprintf("deleting the test project %q", projectName))
		_, err := client.Projects.DeleteProject((*projectVar).ID, gogitlab.WithContext(ctx))
		Expect(err).ToNot(HaveOccurred())
	})

	By(fmt.Sprintf("Setting up Access Tokens for the group %q", groupName))
	baseScopes := []string{"read_api"}
	readScope := "read_observability"
	writeScope := "write_observability"
	expiresAt := gogitlab.ISOTime(time.Now().Add(time.Hour * 24))
	readTokenName := fmt.Sprintf("test-e2e-read-%s", testIdentifier)
	writeTokenName := fmt.Sprintf("test-e2e-write-%s", testIdentifier)
	readWriteTokenName := fmt.Sprintf("test-e2e-read-write-%s", testIdentifier)
	createToken := func(name string, scopes []string, gid int) *gogitlab.GroupAccessToken {
		t, _, err := client.GroupAccessTokens.CreateGroupAccessToken(
			gid,
			&gogitlab.CreateGroupAccessTokenOptions{
				Name:      &name,
				Scopes:    &scopes,
				ExpiresAt: &expiresAt,
			})
		Expect(err).ToNot(HaveOccurred())

		DeferCleanup(func(ctx SpecContext) {
			By(fmt.Sprintf("Deleting the test token %s", name))
			_, err := client.GroupAccessTokens.RevokeGroupAccessToken(gid, t.ID, gogitlab.WithContext(ctx))
			Expect(err).ToNot(HaveOccurred())
		})
		return t
	}
	*groupReadOnlyTokenVar = createToken(readTokenName, append(baseScopes, readScope), (*groupVar).ID)
	*groupWriteOnlyTokenVar = createToken(writeTokenName, append(baseScopes, writeScope), (*groupVar).ID)
	*groupReadWriteTokenVar = createToken(readWriteTokenName, append(baseScopes, readScope, writeScope), (*groupVar).ID)
}

// Init error tracking related settings.
// Requires initGitLab to be called first.
func initErrorTracking(client *gogitlab.Client) {
	var err error

	By("Setting up GitLab Error Tracking")

	By("Enabling error tracking on the project")
	// Enable per-project error tracking and get the Sentry URL from settings and test against it.
	// The go-gitlab client doesn't support this API yet, so we use the raw client.
	req, err := client.NewRequest(
		http.MethodPut,
		fmt.Sprintf("projects/%d/error_tracking/settings", gitLab.project.ID), nil, nil,
	)
	Expect(err).ToNot(HaveOccurred())
	q := req.URL.Query()
	q.Add("active", "true")
	q.Add("integrated", "true")
	req.URL.RawQuery = q.Encode()

	_, err = client.Do(req, nil)
	Expect(err).ToNot(HaveOccurred())

	By("Creating project SENTRY_DSN key")
	key, _, err := client.ErrorTracking.CreateClientKey(gitLab.project.ID, nil)
	Expect(err).ToNot(HaveOccurred())

	listErrorEndpoint := fmt.Sprintf(
		"%s/errortracking/api/v1/projects/%d/errors",
		testInfra.Configuration().GOBAddress(), gitLab.project.ID,
	)

	gitLab.errorTrackingConfig.clientKey = key
	gitLab.errorTrackingConfig.listEndpoint = listErrorEndpoint

	s := spew.Sdump(struct {
		ProjectPath        string
		SentryDSN          string
		ListErrorsEndpoint string
	}{
		ProjectPath:        gitLab.project.PathWithNamespace,
		SentryDSN:          key.SentryDsn,
		ListErrorsEndpoint: listErrorEndpoint,
	})
	By("using error tracking configuration: " + s)
}

type glClientLogger struct{}

func (l *glClientLogger) Printf(format string, v ...interface{}) {
	GinkgoWriter.Printf(format+"\n", v...)
}
