package tests

import (
	"os"
	"os/exec"

	. "github.com/onsi/ginkgo/v2"
	. "github.com/onsi/gomega"
)

var _ = Describe("frontend", Ordered, Serial, func() {
	ReportAfterEach(func(report SpecReport) {
		e2eFailed = e2eFailed || report.Failed()
	})

	It("should run the frontend test suite", func(ctx SpecContext) {
		cmd := exec.CommandContext(ctx, "make", "-C", "../frontend", "run-suite")
		cmd.Env = os.Environ()
		cmd.Env = append(cmd.Env,
			"TEST_GITLAB_ADDRESS="+testInfra.Configuration().GitLabAddress(),
			"TEST_GOB_ADDRESS="+testInfra.Configuration().GOBAddress(),
			"TEST_GITLAB_ADMIN_TOKEN="+testInfra.Configuration().GitLabAdminToken,
		)
		cmd.Stdout = os.Stdout
		cmd.Stderr = os.Stderr
		Expect(cmd.Run()).To(Succeed())
	})
})
