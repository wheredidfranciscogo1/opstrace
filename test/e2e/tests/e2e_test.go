package tests

import (
	"context"
	"fmt"
	"net/http"
	"os"
	"testing"
	"time"

	"github.com/davecgh/go-spew/spew"
	. "github.com/onsi/ginkgo/v2"
	. "github.com/onsi/gomega"

	traefik "github.com/traefik/traefik/v2/pkg/provider/kubernetes/crd/traefikio/v1alpha1"
	v1 "k8s.io/api/core/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/types"
	"k8s.io/client-go/kubernetes"
	"k8s.io/client-go/kubernetes/scheme"
	"k8s.io/client-go/rest"
	"sigs.k8s.io/controller-runtime/pkg/client"
	"sigs.k8s.io/controller-runtime/pkg/log/zap"

	gocommon "gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/common"
	schedulerapi "gitlab.com/gitlab-org/opstrace/opstrace/scheduler/api"
	schedulerv1alpha1 "gitlab.com/gitlab-org/opstrace/opstrace/scheduler/api/v1alpha1"
	"gitlab.com/gitlab-org/opstrace/opstrace/test/e2e/infra"
	"gitlab.com/gitlab-org/opstrace/opstrace/test/e2e/infra/common"
	apiextensionsv1 "k8s.io/apiextensions-apiserver/pkg/apis/apiextensions/v1"
	logf "sigs.k8s.io/controller-runtime/pkg/log"
)

// Shared test components that are initialized in BeforeSuite.
// These should not be updated after initialisation.
var (
	// testIdentifier is a unique identifier for the test run
	testIdentifier string
	// testTarget is the target environment for the test run
	testTarget gocommon.EnvironmentTarget
	// k8sClient for accessing created cluster
	k8sClient client.Client
	// defaultClient is a client scoped to the default namespace
	defaultNSClient client.Client
	// restConfig is the rest config for the clients
	restConfig *rest.Config
	// clientset is the kubernetes clientset
	clientset *kubernetes.Clientset
	// testInfra is the infrastructure builder for the suite.
	// This is not intended to be used in tests, but is used in helpers.
	testInfra infra.InfrastructureBuilder
	// default HTTP client configured for general use in tests
	httpClient *http.Client
	// cluster is the cluster CR for the test run
	cluster *schedulerv1alpha1.Cluster

	// gitlab observability tenant
	tenant *schedulerv1alpha1.GitLabObservabilityTenant

	// Global flag signalizing that suite has failed
	e2eFailed bool
)

func TestE2E(t *testing.T) {
	RegisterFailHandler(Fail)

	SetDefaultEventuallyTimeout(time.Minute * 15)
	SetDefaultEventuallyPollingInterval(time.Second)

	suiteConfig, reporterConfig := GinkgoConfiguration()
	reporterConfig.Verbose = true
	reporterConfig.FullTrace = true
	suiteConfig.Timeout = 2 * time.Hour
	suiteConfig.GracePeriod = 1 * time.Hour
	RunSpecs(t, "E2E Test Suite", suiteConfig, reporterConfig)
}

var _ = BeforeSuite(func(ctx SpecContext) {
	By("init client schemes")
	initSchemes()
	logf.SetLogger(zap.New(zap.WriteTo(GinkgoWriter), zap.UseDevMode(true)))

	By("initializing test variables")
	c, err := LoadConfig()
	Expect(err).ToNot(HaveOccurred())
	setTestTarget(c)
	setTestIdentifier(c)

	testInfra, err = infra.New(testTarget, testIdentifier)
	Expect(err).ToNot(HaveOccurred())

	By("loading infrastructure configuration")
	err = testInfra.LoadConfiguration()
	Expect(err).ToNot(HaveOccurred())
	printConfigData(*testInfra.Configuration())

	By("creating GitLab Instance")
	Expect(testInfra.CreateGitLabInstance(ctx)).To(Succeed())
	DeferCleanup(makeCleanupConditional(&e2eFailed, testInfra.DestroyGitLabInstance))

	By("creating k8s cluster")
	Expect(testInfra.CreateK8sCluster(ctx)).To(Succeed())
	DeferCleanup(makeCleanupConditional(&e2eFailed, testInfra.DestroyK8sCluster))

	By("ensuring we can construct clients for the underlying cluster")
	httpClient = testInfra.GetHTTPClient()
	// This is done inside an `Eventually` block because freshly-minted
	// clusters, esp. inside cloud environments can undergo repairs OR
	// auto-scaling events which causes them to be unreachable for a
	// brief period of time. As soon the clients can be successfully
	// built, we move on downstream.
	//
	Eventually(buildK8sClients, 5*time.Minute, 10*time.Second).WithContext(ctx).Should(Succeed())

	if testTarget != gocommon.KIND && testTarget != gocommon.DEVVM {
		By("checking that the API server can be consistently queried")
		nsObjectLookupKey := types.NamespacedName{
			Name:      v1.NamespaceDefault,
			Namespace: v1.NamespaceDefault,
		}
		nsObject := &v1.Namespace{}
		Consistently(ctx, func(g Gomega) {
			g.Expect(k8sClient.Get(ctx, nsObjectLookupKey, nsObject)).To(Succeed())
		}, 10*time.Second, 3*time.Second).Should(Succeed())
	}

	By("checking that the scheduler-manager pod is ready & running")
	Eventually(ctx, func(g Gomega) {
		pods, err := clientset.CoreV1().Pods(v1.NamespaceDefault).List(
			ctx,
			metav1.ListOptions{LabelSelector: "control-plane=controller-manager"},
		)
		g.Expect(err).NotTo(HaveOccurred())
		g.Expect(pods.Items).To(HaveLen(1), fmt.Sprintf("expected to found a single scheduler-manager pod, found %d", len(pods.Items)))

		g.Expect(pods.Items[0].Status.Phase).To(Equal(v1.PodRunning))
	}, 10*time.Second, 3*time.Second).Should(Succeed())

	By("installing the infra cluster definition")
	ensureCluster(ctx)
	DeferCleanup(makeCleanupConditional(&e2eFailed, teardownCluster))

	By("configuring GitLab for this run")
	initGitLab(ctx)

	By("setup GitLabObservabilityTenant for group " + gitLab.group.Name)
	setupGitLabObservabilityTenant(ctx)
})

func setTestTarget(c Configuration) {
	testTarget = c.TestTarget
	By("using test target: " + string(testTarget))
}

func setTestIdentifier(c Configuration) {
	testIdentifier = c.TestIdentifier
	By("using test identifier: " + testIdentifier)
}

func initSchemes() {
	Expect(scheme.AddToScheme(scheme.Scheme)).To(Succeed())
	Expect(apiextensionsv1.AddToScheme(scheme.Scheme)).To(Succeed())
	Expect(schedulerapi.AddToScheme(scheme.Scheme)).To(Succeed())
	Expect(traefik.AddToScheme(scheme.Scheme)).To(Succeed())
}

func buildK8sClients(g Gomega, ctx SpecContext) {
	var err error
	restConfig, err = testInfra.GetRestConfig(ctx)
	g.Expect(err).ToNot(HaveOccurred())
	k8sClient, err = client.New(restConfig, client.Options{
		Scheme: scheme.Scheme,
	})
	g.Expect(err).NotTo(HaveOccurred())

	defaultNSClient = client.NewNamespacedClient(k8sClient, "default")

	clientset, err = kubernetes.NewForConfig(restConfig)
	g.Expect(err).NotTo(HaveOccurred())
}

func printConfigData(c common.Configuration) {
	c.GitLabAdminToken = "**********"
	s := spew.Sdump(c)
	By("using configuration: " + s)
}

// Load the infra configured cluster and ensure it's ready.
func ensureCluster(ctx SpecContext) {
	var err error
	cluster, err = testInfra.GetClusterDefinition(ctx)
	Expect(err).NotTo(HaveOccurred())
	// Treat KIND clusters as "bring your own stack for testing"
	// and the test harness just runs against the stack you have locally.
	// This prevents the test harness from
	// clobbering the existing Cluster CR when run against
	// a local Kind cluster - unless there's no cluster at all.
	if testTarget != gocommon.KIND {
		createOrUpdate(ctx, cluster)
	} else {
		ls := &schedulerv1alpha1.ClusterList{}
		Expect(k8sClient.List(ctx, ls)).To(Succeed())
		if len(ls.Items) == 0 {
			createOrUpdate(ctx, cluster)
		}
	}
	expectClusterReady(ctx, cluster)
	expectIngressCertificateReady(ctx)
	// refresh the cluster so the inventory is up to date.
	Expect(k8sClient.Get(ctx, client.ObjectKeyFromObject(cluster), cluster)).To(Succeed())

	// NOTE(joe): don't auto cleanup with DeferCleanup.
	// Creating the cluster is slow and we want to reuse it for multiple tests.
}

func teardownCluster(ctx SpecContext) {
	if testTarget == gocommon.KIND || testTarget == gocommon.DEVVM {
		// don't clean up Cluster in kind, so we can inspect it after tests.
		// For devvm it does not matter as the whole machine is removed anyway
		return
	}

	By("clean up cluster CR if it exists")
	// need to clean up cluster so terragrunt can tear down any CRDs it uses
	cs := &schedulerv1alpha1.ClusterList{}
	Expect(defaultNSClient.List(ctx, cs)).To(Succeed())
	for _, c := range cs.Items {
		cr := c
		deleteCustomResourceAndVerify(ctx, &cr)
	}
}

// deferCleanupOptional makes DeferCleanup run if the suite has not failed. Due
// to the fact that we are checking if any of the tests failed, we need to
// deffer a wrapped function instead of conditionally call DefferCleanup()
func makeCleanupConditional(suiteFailed *bool, f any) any {
	skipCleanup, ok := os.LookupEnv("TEST_SKIP_CLEANUP")

	// No skipping of cleanup
	if !ok {
		return f
	}

	switch v := f.(type) {
	case func(context.Context) error:
		return func(ctx context.Context) error {
			if skipCleanup == "true" || skipCleanup == "onfailure" && *suiteFailed {
				return nil
			}
			return v(ctx)
		}
	case func(SpecContext):
		return func(ctx SpecContext) {
			if skipCleanup == "true" || skipCleanup == "onfailure" && *suiteFailed {
				return
			}
			v(ctx)
		}
	default:
		panic("unsupported cleanup function type passed, please adjust the switch statement")
	}
}

func setupGitLabObservabilityTenant(ctx context.Context) {
	tenant = new(schedulerv1alpha1.GitLabObservabilityTenant)
	tenant.Name = fmt.Sprintf("tenant-%d", gitLab.group.ID)
	tenant.Spec.TopLevelNamespaceID = int64(gitLab.group.ID)

	createOrUpdate(ctx, tenant)
	expectGitLabObservabilityTenantReady(ctx, tenant)

	DeferCleanup(func(ctx SpecContext) {
		By("delete GitLabObservabilityTenant for group " + gitLab.group.Name)
		expectNamespaceDeleted(ctx, cluster.Namespace())
	})
}
