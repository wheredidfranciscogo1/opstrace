package tests

import (
	"crypto/tls"
	"fmt"
	"net/http"
	"strings"

	. "github.com/onsi/ginkgo/v2"
	. "github.com/onsi/gomega"

	gocommon "gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/common"
)

var _ = Describe("cluster provisioning", Ordered, Serial, func() {
	ReportAfterEach(func(report SpecReport) {
		e2eFailed = e2eFailed || report.Failed()
	})

	Context("on creating an example cluster", func() {
		It("ensures it can be created successfully", func(ctx SpecContext) {
			expectClusterPodsReady(ctx)
		})
	})

	Context("ensures it is served over HTTPS", func() {
		It("by serving a valid certificate", func() {
			if testTarget == gocommon.KIND || testTarget == gocommon.DEVVM {
				Skip("skipping on KIND/Devvm clusters as we use self-signed certs")
			}
			conn, err := tls.Dial("tcp", fmt.Sprintf("%s:443", testInfra.Configuration().GOBHost), nil)
			Expect(err).ToNot(HaveOccurred())
			Expect(conn).ToNot(BeNil())

			err = conn.VerifyHostname(testInfra.Configuration().GOBHost)
			Expect(err).ToNot(HaveOccurred())

			issuer := conn.ConnectionState().PeerCertificates[0].Issuer
			found := strings.Contains(issuer.String(), "O=Let's Encrypt")
			Expect(found).To(BeTrue())
		})
	})

	Context("ensures when accessing a new namespace", func() {
		It("the user is first authorized", func() {
			r, err := httpClient.Get(fmt.Sprintf("%s/-/1", testInfra.Configuration().GOBAddress()))
			Expect(err).ToNot(HaveOccurred())
			defer r.Body.Close()
			Expect(r).To(HaveHTTPStatus(http.StatusOK))
			Expect(r).To(HaveHTTPBody(ContainSubstring("<title>Sign in · GitLab</title>")))
		})
	})
})
