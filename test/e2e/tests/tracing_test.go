package tests

import (
	"bytes"
	"context"
	"crypto/tls"
	"encoding/hex"
	"encoding/json"
	"errors"
	"fmt"
	"io"
	"math"
	"math/rand"
	"net/http"
	"net/url"
	"os/exec"
	"path/filepath"
	"runtime"
	"strings"
	"time"

	"github.com/davecgh/go-spew/spew"
	"github.com/google/uuid"
	. "github.com/onsi/ginkgo/v2"
	"go.opentelemetry.io/otel/attribute"
	"go.opentelemetry.io/otel/codes"
	"go.opentelemetry.io/otel/exporters/otlp/otlptrace/otlptracehttp"
	"go.opentelemetry.io/otel/sdk/resource"
	sdktrace "go.opentelemetry.io/otel/sdk/trace"
	semconv "go.opentelemetry.io/otel/semconv/v1.4.0"
	"go.opentelemetry.io/otel/trace"

	. "github.com/onsi/gomega"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/common"
	query "gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/trace-query-api"
)

// service name for generated spans compatible query
const tracerServiceName = "test_otlp_tracing_api"

var _ = Describe("Tracing", Ordered, Serial, func() {
	// tracer configured with serviceName to create spans
	var tracer trace.Tracer

	// tokens for accessing the group/project
	var readOnlyToken string
	var writeOnlyToken string

	// ingestion endpoint for the project
	var ingestEndpoint string

	// send a test span to the otel trace http endpoint
	sendSpans := func(ctx SpecContext, apiKey string, spans ...trace.Span) error {
		opts := []otlptracehttp.Option{
			// #nosec G402
			otlptracehttp.WithTLSClientConfig(&tls.Config{
				InsecureSkipVerify: true,
			}),
			otlptracehttp.WithEndpoint(testInfra.Configuration().GOBHost),
			otlptracehttp.WithURLPath(fmt.Sprintf("/v3/%d/%d/ingest/traces", gitLab.group.ID, gitLab.project.ID)),
			otlptracehttp.WithHeaders(
				map[string]string{
					"Private-Token": apiKey,
				},
			),
		}

		exporter, err := otlptracehttp.New(ctx, opts...)
		Expect(err).NotTo(HaveOccurred())
		defer func() { Expect(exporter.Shutdown(ctx)).To(Succeed()) }()
		return exporter.ExportSpans(ctx, readSpans(spans...))
	}

	// creates a span from the tracer and returns the span context and span.
	// span properties are randomly assigned.
	getSingleSpan := func(ctx SpecContext, tracer trace.Tracer) (context.Context, trace.Span) {
		spanLabel := getRandomSpanLabel()

		kind := randItem(
			trace.SpanKindUnspecified,
			trace.SpanKindInternal,
			trace.SpanKindServer,
			trace.SpanKindClient,
			trace.SpanKindProducer,
			trace.SpanKindConsumer,
		)

		status := randItem(
			codes.Unset,
			codes.Ok,
			codes.Error,
		)

		// provide a unique name for lookup convenience
		operationName := "test_" + spanLabel
		spanCtx, span := tracer.Start(ctx, operationName,
			trace.WithSpanKind(kind),
			trace.WithAttributes(
				attribute.String("testing", spanLabel),
			))
		span.AddEvent("emitting test span", trace.WithAttributes(attribute.String("testing", spanLabel)))
		span.SetStatus(status, "test status")

		By(fmt.Sprintf("created span %s for trace %s",
			span.SpanContext().SpanID(), span.SpanContext().TraceID()))

		return spanCtx, span
	}

	makeTraceRequest := func(ctx SpecContext, apiKey string, root trace.Span) *http.Request {
		By("building query for a trace by ID")

		rs := readSpan(root)
		req, err := http.NewRequest("GET",
			fmt.Sprintf("%s/v3/query/%d/traces", testInfra.Configuration().GOBAddress(), gitLab.project.ID), nil)
		Expect(err).NotTo(HaveOccurred())
		req.Header.Set("Private-Token", apiKey)
		q := req.URL.Query()
		traceID := rs.SpanContext().TraceID().String()
		q.Add("trace_id", traceID)
		req.URL.RawQuery = q.Encode()

		return req
	}

	queryForTrace := func(ctx SpecContext, apiKey string, root trace.Span) *query.TracesResult {

		rs := readSpan(root)
		req := makeTraceRequest(ctx, apiKey, root)

		var res *query.TracesResult
		// wait for up to ~60s for span to appear in queries.
		Eventually(func(g Gomega) {
			resp, err := httpClient.Do(req)
			g.Expect(err).NotTo(HaveOccurred())
			defer resp.Body.Close()
			g.Expect(resp).To(HaveHTTPStatus(http.StatusOK))

			traces := &query.TracesResult{}
			g.Expect(json.NewDecoder(resp.Body).Decode(traces)).To(Succeed())
			g.Expect(traces.ProjectID).To(BeNumerically("==", gitLab.project.ID), "trace project id")
			g.Expect(traces.TotalTraces).To(BeNumerically("==", 1), "total traces")
			g.Expect(traces.Traces).To(HaveLen(1), "traces")

			res = traces
		}).WithTimeout(time.Minute).Should(Succeed())

		trace := res.Traces[0]
		hexStringsEqual(trace.TraceID, rs.SpanContext().TraceID().String(), "trace id")
		Expect(trace.Timestamp.UnixNano()).To(Equal(rs.StartTime().UnixNano()), "start time")
		Expect(trace.TimestampNano).To(BeEquivalentTo(rs.StartTime().UnixNano()), "start time nano")

		return res
	}

	makeTracer := func(serviceName string) trace.Tracer {
		r, err := resource.Merge(
			resource.Default(),
			resource.NewWithAttributes(
				"",
				semconv.ServiceNameKey.String(serviceName),
				semconv.ServiceVersionKey.String("v0.1.0"),
				attribute.String("environment", "e2e"),
			))
		Expect(err).NotTo(HaveOccurred())
		tp := sdktrace.NewTracerProvider(sdktrace.WithResource(r))
		return tp.Tracer("otel_tracer")
	}

	makeQueryRequest := func(values url.Values) *http.Request {
		req, err := http.NewRequest(
			"GET",
			fmt.Sprintf("%s/v3/query/%d/traces",
				testInfra.Configuration().GOBAddress(),
				gitLab.project.ID,
			), nil)
		Expect(err).NotTo(HaveOccurred())
		req.Header.Set("Private-Token", readOnlyToken)
		req.URL.RawQuery = values.Encode()
		return req
	}

	makeQueryRequestForRED := func(values url.Values) *http.Request {
		req, err := http.NewRequest(
			"GET",
			fmt.Sprintf("%s/v3/query/%d/traces/analytics",
				testInfra.Configuration().GOBAddress(),
				gitLab.project.ID,
			), nil)
		Expect(err).NotTo(HaveOccurred())
		req.Header.Set("Private-Token", readOnlyToken)
		req.URL.RawQuery = values.Encode()
		return req
	}

	doQueryRequest := func(
		ctx SpecContext,
		req *http.Request,
		totalTraces int64,
		matchedSpans uint64,
		errorSpanCount uint64) {
		Eventually(ctx, func(g Gomega) {
			res, err := httpClient.Do(req)
			g.Expect(err).NotTo(HaveOccurred())
			defer res.Body.Close()

			g.Expect(res).To(HaveHTTPStatus(http.StatusOK))
			body, err := io.ReadAll(res.Body)
			g.Expect(err).NotTo(HaveOccurred())

			var trace query.TracesResult
			err = json.Unmarshal(body, &trace)
			g.Expect(err).NotTo(HaveOccurred())

			g.Expect(trace.TotalTraces).To(Equal(totalTraces), "total traces")
			g.Expect(trace.Traces[0].MatchedSpanCount).To(Equal(matchedSpans), "matched span count")
			g.Expect(trace.Traces[0].ErrorSpanCount).To(Equal(errorSpanCount))
		}, "60s").Should(Succeed())
	}

	doQueryRequestForRates := func(
		ctx SpecContext,
		req *http.Request,
		expectedCount uint64,
		expectedErrCount uint64) {
		Eventually(ctx, func(g Gomega) {
			res, err := httpClient.Do(req)
			g.Expect(err).NotTo(HaveOccurred())
			defer res.Body.Close()

			g.Expect(res).To(HaveHTTPStatus(http.StatusOK))
			body, err := io.ReadAll(res.Body)
			g.Expect(err).NotTo(HaveOccurred())

			var trace query.TracesCountResult
			err = json.Unmarshal(body, &trace)
			g.Expect(err).NotTo(HaveOccurred())
			GinkgoWriter.Printf("trace : %v", trace.Results)

			g.Expect(trace.Results).ToNot(BeEmpty())
			g.Expect(trace.Results[0].TraceCount).To(Equal(expectedCount))
			g.Expect(trace.Results[0].ErrorCount).To(Equal(expectedErrCount))

		}, "60s").Should(Succeed())
	}

	ReportAfterEach(func(report SpecReport) {
		e2eFailed = e2eFailed || report.Failed()
	})

	BeforeAll(func(ctx SpecContext) {
		By("set up a shared tracer for our tests")
		tracer = makeTracer(tracerServiceName)

		readOnlyToken = gitLab.groupReadOnlyToken.Token
		writeOnlyToken = gitLab.groupWriteOnlyToken.Token
		// use the /v1/traces endpoint here just to show this also works
		ingestEndpoint = fmt.Sprintf(
			"%s/v3/%d/%d/ingest/v1/traces",
			testInfra.Configuration().GOBAddress(),
			gitLab.group.ID, gitLab.project.ID,
		)
	})

	Context("Auth handling", func() {
		Context("Ingest", func() {
			It("should not accept an empty api token", func(ctx SpecContext) {
				_, span := getSingleSpan(ctx, tracer)
				span.End()
				Expect(sendSpans(ctx, "", span)).NotTo(Succeed())
			})

			It("should not accept an invalid api token", func(ctx SpecContext) {
				_, span := getSingleSpan(ctx, tracer)
				span.End()
				Expect(sendSpans(ctx, "bad"+writeOnlyToken, span)).NotTo(Succeed())
			})

			It("should not accept a read-only token", func(ctx SpecContext) {
				_, span := getSingleSpan(ctx, tracer)
				span.End()
				Expect(sendSpans(ctx, readOnlyToken, span)).NotTo(Succeed())
			})
		})

		Context("Query", func() {
			It("should not accept an empty api token", func(ctx SpecContext) {
				_, span := getSingleSpan(ctx, tracer)
				span.End()
				Expect(sendSpans(ctx, writeOnlyToken, span)).To(Succeed())
				res, err := httpClient.Do(makeTraceRequest(ctx, "", span))
				Expect(err).NotTo(HaveOccurred())
				By("check we got the GitLab login redirect")
				Expect(res).To(HaveHTTPStatus(http.StatusOK))
				Expect(res.Request.URL.Path).To(Equal("/users/sign_in"))
			})

			It("should not accept an invalid api token", func(ctx SpecContext) {
				_, span := getSingleSpan(ctx, tracer)
				span.End()
				Expect(sendSpans(ctx, writeOnlyToken, span)).To(Succeed())
				res, err := httpClient.Do(makeTraceRequest(ctx, "bad"+readOnlyToken, span))
				Expect(err).NotTo(HaveOccurred())
				Expect(res).To(HaveHTTPStatus(http.StatusForbidden))
			})

			It("should not accept a write-only token", func(ctx SpecContext) {
				_, span := getSingleSpan(ctx, tracer)
				span.End()
				Expect(sendSpans(ctx, writeOnlyToken, span)).To(Succeed())
				res, err := httpClient.Do(makeTraceRequest(ctx, writeOnlyToken, span))
				Expect(err).NotTo(HaveOccurred())
				Expect(res).To(HaveHTTPStatus(http.StatusForbidden))
			})
		})
	})

	Context("Spans", func() {
		It("can save and retrieve single span", func(ctx SpecContext) {
			_, span := getSingleSpan(ctx, tracer)
			span.End()
			Expect(sendSpans(ctx, writeOnlyToken, span)).To(Succeed())
			t := queryForTrace(ctx, readOnlyToken, span)
			verifyTraceItem(readSpan(span), *t.Traces[0])
		})

		It("can save and retrieve fragmented spans", func(ctx SpecContext) {
			By("create chain of nested spans")
			rootContext, rootSpan := getSingleSpan(ctx, tracer)
			rootSpan.SetAttributes(attribute.String("fragment", "root"))
			rootSpan.AddEvent("emitting root test span")

			childContext, childSpan := tracer.Start(rootContext,
				"test_"+getRandomSpanLabel(),
				trace.WithSpanKind(trace.SpanKindServer))
			childSpan.SetAttributes(attribute.String("fragment", "child"))
			childSpan.AddEvent("emitting child test span")

			_, childSiblingSpan := tracer.Start(rootContext,
				"test"+getRandomSpanLabel(),
				trace.WithLinks(trace.Link{SpanContext: childSpan.SpanContext()}),
				trace.WithSpanKind(trace.SpanKindProducer))

			_, grandchildSpan := tracer.Start(childContext, "test_"+getRandomSpanLabel())
			grandchildSpan.SetAttributes(attribute.String("fragment", "grandchild"))
			grandchildSpan.AddEvent("emitting grandchild test span")
			grandchildSpan.RecordError(errors.New("error"))
			grandchildSpan.SetStatus(codes.Error, "error")

			grandchildSpan.End()
			childSpan.End()
			rootSpan.End()
			childSiblingSpan.End()

			getTraceID := func(span trace.Span) trace.TraceID {
				return readSpan(span).SpanContext().TraceID()
			}

			traceID := getTraceID(rootSpan)
			// ensure all the trace ids are identical
			Expect(
				[]trace.TraceID{getTraceID(rootSpan), getTraceID(childSpan), getTraceID(grandchildSpan)}).
				Should(HaveEach(traceID))

			By("sending spans separately")
			Expect(sendSpans(ctx, writeOnlyToken, rootSpan)).To(Succeed())
			Expect(sendSpans(ctx, writeOnlyToken, childSpan, childSiblingSpan)).To(Succeed())
			Expect(sendSpans(ctx, writeOnlyToken, grandchildSpan)).To(Succeed())

			By("query by trace id")
			var result *query.TracesResult
			// allow 1m for spans to appear
			Eventually(func(g Gomega) {
				res := queryForTrace(ctx, readOnlyToken, rootSpan)
				g.Expect(res.Traces[0].TotalSpans).To(BeNumerically("==", 4), "total spans")
				result = res
			}).WithTimeout(time.Minute).Should(Succeed())

			verifyTraceItem(readSpan(rootSpan), *result.Traces[0])
		})
	})

	Context("Query /traces/services", func() {
		It("can get unique service names", func(ctx SpecContext) {
			By("create tracers with different service names")
			_, s1 := getSingleSpan(ctx, tracer)
			t2 := makeTracer(tracerServiceName + "_2")
			_, s2 := getSingleSpan(ctx, t2)
			t3 := makeTracer(tracerServiceName + "_3")
			_, s3 := getSingleSpan(ctx, t3)

			s1.End()
			s2.End()
			s3.End()

			Expect(sendSpans(ctx, writeOnlyToken, s1, s2, s3)).To(Succeed())

			By("querying for service names")
			req, err := http.NewRequest("GET", fmt.Sprintf("%s/v3/query/%d/traces/services", testInfra.Configuration().GOBAddress(), gitLab.project.ID), nil)
			Expect(err).NotTo(HaveOccurred())
			req.Header.Set("Private-Token", readOnlyToken)

			Eventually(ctx, func(g Gomega) {
				res, err := httpClient.Do(req)
				g.Expect(err).NotTo(HaveOccurred())
				defer res.Body.Close()

				g.Expect(res).To(HaveHTTPStatus(http.StatusOK))
				g.Expect(res).To(HaveHTTPBody(MatchJSON(`{
					"services": [
						{
							"name": "test_otlp_tracing_api"
						},
						{
							"name": "test_otlp_tracing_api_2"
						},
						{
							"name": "test_otlp_tracing_api_3"
						}
					]
				}`)))
			}, "10s").Should(Succeed())
		})
	})

	Context("Query /services/<name>/operations", func() {
		It("can get unique operation names", func(ctx SpecContext) {
			By("creating spans with different operation names")
			service := tracerServiceName + "_opstest"
			t := makeTracer(service)
			c1, s1 := t.Start(ctx, "op1")
			_, s2 := t.Start(c1, "op2")
			s1.End()
			s2.End()

			Expect(sendSpans(ctx, writeOnlyToken, s1, s2)).To(Succeed())
			By("querying for operation names")
			req, err := http.NewRequest("GET",
				fmt.Sprintf("%s/v3/query/%d/traces/services/%s/operations", testInfra.Configuration().GOBAddress(), gitLab.project.ID, service), nil)
			Expect(err).NotTo(HaveOccurred())
			req.Header.Set("Private-Token", readOnlyToken)

			Eventually(ctx, func(g Gomega) {
				res, err := httpClient.Do(req)
				g.Expect(err).NotTo(HaveOccurred())
				defer res.Body.Close()

				g.Expect(res).To(HaveHTTPStatus(http.StatusOK))
				g.Expect(res).To(HaveHTTPBody(MatchJSON(`{
					"operations": [
						{
							"name": "op1"
						},
						{
							"name": "op2"
						}
					]
				}`)))
			}, "10s").Should(Succeed())
		})
	})

	Context("Query /traces/<id>", func() {
		It("returns all spans in correct order", func(ctx SpecContext) {
			By("create spans where a child span starts before root span")

			start := time.Now()
			duration := 500000000
			id := generateTraceID()
			traces := []string{fmt.Sprintf("%s:%d:%d:test", id, start.Unix(), duration)}

			Expect(seedTraces(writeOnlyToken, ingestEndpoint, traces)).To(Succeed())

			By("querying for trace")
			req, err := http.NewRequest(
				"GET",
				fmt.Sprintf("%s/v3/query/%d/traces/%s",
					testInfra.Configuration().GOBAddress(),
					gitLab.project.ID,
					id,
				), nil)

			Expect(err).NotTo(HaveOccurred())
			req.Header.Set("Private-Token", readOnlyToken)

			Eventually(ctx, func(g Gomega) {
				res, err := httpClient.Do(req)
				g.Expect(err).NotTo(HaveOccurred())
				defer res.Body.Close()

				g.Expect(res).To(HaveHTTPStatus(http.StatusOK))
				body, err := io.ReadAll(res.Body)
				g.Expect(err).NotTo(HaveOccurred())

				var trace query.TraceDetailedItem
				err = json.Unmarshal(body, &trace)
				g.Expect(err).NotTo(HaveOccurred())

				g.Expect(trace.DurationNano).To(Equal(int64(duration)), "trace duration")
				g.Expect(trace.Timestamp.Format("2006-01-02 15:04:05Z")).To(
					Equal(start.UTC().Format("2006-01-02 15:04:05Z")), "trace start")
				g.Expect(trace.Operation).To(Equal("article-to-cart"), "trace operation")
				g.Expect(trace.ServiceName).To(Equal("test"), "trace service name")
				g.Expect(trace.TotalSpans).To(Equal(int64(10)), "trace total spans")
				g.Expect(trace.Spans).To(HaveLen(10), "spans slice length")

				g.Expect(trace.Spans[0].ParentSpanID).To(Equal(""), "first span is root")

				g.Expect(trace.Spans[0].SpanID).To(Equal("0B6556502C8CD0D9"))
				g.Expect(trace.Spans[1].SpanID).To(Equal("EC5A65D86A5D499E"))
				g.Expect(trace.Spans[2].SpanID).To(Equal("77FECD69D987A3FC"))
				g.Expect(trace.Spans[3].SpanID).To(Equal("3087334E85583CB2"))
				g.Expect(trace.Spans[4].SpanID).To(Equal("B858B31F0961EDF0"))
				g.Expect(trace.Spans[5].SpanID).To(Equal("1C1EACA9BBA068D9"))
				g.Expect(trace.Spans[6].SpanID).To(Equal("7F35FD8C149498E8"))
				g.Expect(trace.Spans[7].SpanID).To(Equal("485B6310E5D95A57"))
				g.Expect(trace.Spans[8].SpanID).To(Equal("7731F938CE0B52D1"))
				g.Expect(trace.Spans[9].SpanID).To(Equal("BD41823B2ACA2AE6"))

			}, "10s").Should(Succeed())
		})
	})

	Context("Query /traces/analytics?", func() {

		It("Default period with span name in filters", func(ctx SpecContext) {
			By("creating traces")

			service := tracerServiceName + "rate_span_test_1"
			t := makeTracer(service)
			c1, s1 := t.Start(ctx, "rate_span_test_1")
			_, s2 := t.Start(c1, "rate_span_test_2")
			s1.SetStatus(codes.Error, "some error")
			s1.End()
			s2.End()

			Expect(sendSpans(ctx, writeOnlyToken, s1, s2)).To(Succeed())

			By("querying RED metrics")

			req := makeQueryRequestForRED(map[string][]string{
				"operation": {"rate_span_test_1"},
			})

			doQueryRequestForRates(ctx, req, 1, 1)

		})
	})

	Context("Query /traces?", func() {
		Context("Multiple trace_ids provided & no time period", func() {
			DescribeTable("traces are sorted correctly",
				func(ctx SpecContext, sort string, expector func([]*query.TraceItem)) {
					By("create traces")
					start := time.Now()
					// 5 days ago
					start2 := start.Add(time.Duration(-24*5) * time.Hour)

					duration := 500000000
					duration2 := 600000000

					id := generateTraceID()
					id2 := generateTraceID()
					identifier, err := common.RandStringASCIIBytes(8)
					Expect(err).NotTo(HaveOccurred())

					traces := []string{
						fmt.Sprintf("%s:%d:%d:%s", id, start.Unix(), duration, identifier),
						fmt.Sprintf("%s:%d:%d:%s", id2, start2.Unix(), duration2, identifier),
					}
					Expect(seedTraces(writeOnlyToken, ingestEndpoint, traces)).To(Succeed())

					By("querying for traces")
					queryParams := url.Values{
						"trace_id": {id, id2},
					}
					if sort != "" {
						queryParams.Add("sort", sort)
					}
					req := makeQueryRequest(queryParams)

					Eventually(ctx, func(g Gomega) {
						res, err := httpClient.Do(req)
						g.Expect(err).NotTo(HaveOccurred())
						defer res.Body.Close()

						g.Expect(res).To(HaveHTTPStatus(http.StatusOK))
						body, err := io.ReadAll(res.Body)
						g.Expect(err).NotTo(HaveOccurred())

						var trace query.TracesResult
						err = json.Unmarshal(body, &trace)
						g.Expect(err).NotTo(HaveOccurred())

						g.Expect(trace.TotalTraces).To(Equal(int64(2)), "total traces")
						g.Expect(trace.Traces).To(HaveLen(2), "traces slice length")
						expector(trace.Traces)

					}, "10s").Should(Succeed())
				},
				Entry("Default Sorting", "", expectTimeStampDesc),
				Entry("Timestamp DESC", query.SortTimestampDesc, expectTimeStampDesc),
				Entry("Duration DESC", query.SortDurationDesc, expectDurationDesc),
				Entry("Timestamp ASC", query.SortTimestampAsc, expectTimestampAsc),
				Entry("Duration ASC", query.SortDurationAsc, expectDurationAsc),
			)
		})

		It("SpanName provided in filter", func(ctx SpecContext) {
			By("creating traces")

			service := tracerServiceName + "_span_test"
			t := makeTracer(service)
			c1, s1 := t.Start(ctx, "operation_span_test_1")
			_, s2 := t.Start(c1, "operation_span_test_2")
			s1.SetStatus(codes.Error, "some error")
			s1.End()
			s2.End()

			Expect(sendSpans(ctx, writeOnlyToken, s1, s2)).To(Succeed())

			By("searching with spanName")

			req := makeQueryRequest(map[string][]string{
				"operation": {"operation_span_test_1"},
			})

			doQueryRequest(ctx, req, 1, 1, 1)
		})

		It("ServiceName provided in filter", func(ctx SpecContext) {
			By("creating traces")

			service := tracerServiceName + "_service_test"
			t := makeTracer(service)
			c1, s1 := t.Start(ctx, "op1")
			_, s2 := t.Start(c1, "op2")
			s1.End()
			s2.End()

			Expect(sendSpans(ctx, writeOnlyToken, s1, s2)).To(Succeed())

			By("searching with service name")

			req := makeQueryRequest(map[string][]string{
				"service_name": {tracerServiceName + "_service_test"},
			})

			doQueryRequest(ctx, req, 1, 2, 0)
		})

		Context("Pagination", func() {
			DescribeTable("Sort direction paginates correctly",
				func(ctx SpecContext, sort string, expector func([]*query.TraceItem)) {
					By("create traces")
					last := time.Now()

					// by creating 3 traces with the same time,
					// we can query with LIMIT=3 to test the cursor
					// doesn't "drop traces" when sorting by timestamp.
					startTimes := []time.Time{
						last.Add(time.Duration(-10) * time.Minute),
						last.Add(time.Duration(-9) * time.Minute),
						last.Add(time.Duration(-6) * time.Minute), // same timestamp
						last.Add(time.Duration(-6) * time.Minute), // same timestamp <- will set page break here in tests
						last.Add(time.Duration(-6) * time.Minute), // same timestamp
						last.Add(time.Duration(-5) * time.Minute),
						last,
					}
					// by creating 3 traces with the same duration,
					// we can query with LIMIT=3 to test the cursor
					// doesn't "drop traces" when sorting by duration.

					// Trace duration order isn't linear so we can verify
					// results are different to timestamp ordered results.
					durations := []int{
						400000000, // same duration
						1100000000,
						700000000,
						800000000,
						800000000, // same duration
						1400000000,
						800000000, // same duration
					}
					traceIDs := make([]string, 7)
					for i := 0; i < 7; i++ {
						traceIDs[i] = generateTraceID()
					}
					traces := make([]string, 7)
					// generate unique id for this test run and use as service name for traces
					// so we can create and query these specifically
					identifier, err := common.RandStringASCIIBytes(8)
					Expect(err).NotTo(HaveOccurred())

					for i, id := range traceIDs {
						traces[i] = fmt.Sprintf("%s:%d:%d:%s", id, startTimes[i].Unix(), durations[i], identifier)
					}
					Expect(seedTraces(writeOnlyToken, ingestEndpoint, traces)).To(Succeed())

					spew.Dump(traces)

					returnedTraces := []*query.TraceItem{}
					// Flakes can occur if we send traces to the ingestion API
					// and they are then batch inserted asynchronously due to the
					// nature of our ingest pipeline while also being queried.
					// The full set of expected traces from seedTraces isn't
					// guaranteed to be available in ClickHouse all at the same
					// time. To combat this, we wait for all expected traces
					// to be available before we start the sort/pagination tests.
					By("querying for traces: wait till all traces available")
					Eventually(ctx, func(g Gomega) {

						req := makeQueryRequest(map[string][]string{
							"service_name": {identifier},
							"period":       {"1h"},
							"page_size":    {"7"},
						})

						res, err := httpClient.Do(req)
						g.Expect(err).NotTo(HaveOccurred())
						defer res.Body.Close()

						g.Expect(res).To(HaveHTTPStatus(http.StatusOK))
						body, err := io.ReadAll(res.Body)
						g.Expect(err).NotTo(HaveOccurred())

						var trace query.TracesResult
						err = json.Unmarshal(body, &trace)
						g.Expect(err).NotTo(HaveOccurred())
						// This is the critical expectation before proceeding to
						// the next phase
						g.Expect(trace.Traces).To(HaveLen(7), "number of traces returned")
					}, "10s").Should(Succeed())

					By("querying for traces: start pagination")
					// Paginate 3 times
					var pageToken *string
					for i := 1; i <= 3; i++ {
						Eventually(ctx, func(g Gomega) {
							i := i
							queryParams := url.Values{
								"service_name": {identifier},
								"period":       {"1h"},
								"page_size":    {"3"},
							}
							if sort != "" {
								queryParams.Add("sort", sort)
							}
							if pageToken != nil {
								queryParams.Add("page_token", *pageToken)
							}
							req := makeQueryRequest(queryParams)

							By("page " + fmt.Sprint(i))
							spew.Dump(req.URL.String())

							res, err := httpClient.Do(req)
							g.Expect(err).NotTo(HaveOccurred())
							defer res.Body.Close()

							g.Expect(res).To(HaveHTTPStatus(http.StatusOK))
							body, err := io.ReadAll(res.Body)
							g.Expect(err).NotTo(HaveOccurred())

							var trace query.TracesResult
							err = json.Unmarshal(body, &trace)
							g.Expect(err).NotTo(HaveOccurred())
							var passed bool
							if i != 3 {
								// first 2 pages should return the limit of 3
								passed = g.Expect(trace.Traces).To(HaveLen(3), "number of traces returned")
							} else {
								// last page should return 1 trace
								passed = g.Expect(trace.Traces).To(HaveLen(1), "number of traces returned")
							}
							if passed && trace.NextPageToken != "" {
								pageToken = &trace.NextPageToken
							}
							returnedTraces = append(returnedTraces, trace.Traces...)
						}, "10s").Should(Succeed())
					}
					Expect(returnedTraces).To(HaveLen(7), "number of paginated traces")
					uniqueIDs := map[string]bool{}
					for _, trace := range returnedTraces {
						uniqueIDs[trace.TraceID] = true
					}
					Expect(uniqueIDs).To(HaveLen(7), "all traces are unique")
					expector(returnedTraces)
				},
				Entry("Default Sorting", "", expectTimeStampDesc),
				Entry("Timestamp DESC", query.SortTimestampDesc, expectTimeStampDesc),
				Entry("Duration DESC", query.SortDurationDesc, expectDurationDesc),
				Entry("Timestamp ASC", query.SortTimestampAsc, expectTimestampAsc),
				Entry("Duration ASC", query.SortDurationAsc, expectDurationAsc),
			)
		})
	})
})

func readSpans(span ...trace.Span) []sdktrace.ReadOnlySpan {
	spans := make([]sdktrace.ReadOnlySpan, len(span))
	for i, s := range span {
		spans[i] = readSpan(s)
	}
	return spans
}

func readSpan(span trace.Span) sdktrace.ReadOnlySpan {
	ro, ok := span.(sdktrace.ReadOnlySpan)
	Expect(ok).To(BeTrue(), "convert to readonly span")
	return ro
}

func getRandomSpanLabel() string {
	return common.RandStringRunes(10)
}

func randItem[A any](items ...A) A {
	//#nosec
	return items[rand.Intn(len(items))]
}

// verify a generated OTEL span against a trace result from our query API
func verifyTraceItem(a sdktrace.ReadOnlySpan, b query.TraceItem) {
	tid := a.SpanContext().TraceID()
	hexStringsEqual(tid.String(), b.TraceID, "trace id")
	Expect(a.Name()).To(Equal(b.Operation), "span name")

	Expect(tracerServiceName).To(Equal(b.ServiceName), "service name")

	Expect(a.StartTime().UnixNano()).To(BeEquivalentTo(b.Timestamp.UnixNano()), "start time")
	Expect(a.EndTime().UnixNano()).To(BeEquivalentTo(
		b.Timestamp.Add(time.Duration(b.DurationNano)*time.Nanosecond).UnixNano()), "end time")

	// we return the status code as a string like "STATUS_CODE_OK"
	Expect("STATUS_CODE_"+strings.ToUpper(a.Status().Code.String())).To(Equal(b.StatusCode), "status code")

	// NOTE(joe): various items are not exported from our tracing API
	// These used to be tested when we had a Jaeger backend,
	// referenced here from the OTEL span API:
	// - a.SpanKind()
	// - a.Links()
	// - a.Events()
	// - a.Attributes()
	// - a.SpanContext().TraceState()
	// There are other methods, but these are the most important.
}

// normalize a hex string to remove dashes and uppercase
func normalizeHexString(s string) string {
	return strings.ReplaceAll(s, "-", "")
}

// Check two hex strings are equivalent.
// Trace ids 16 bytes and span ids are 8 bytes.
// We cannot assume everything is a uuid.
// Our trace query service returns TraceIDs formatted as uuids
// and SpanIDs as upper case hex strings.
func hexStringsEqual(a, b string, msg ...interface{}) {
	// remove any - chars from potential uuids
	ah := normalizeHexString(a)
	bh := normalizeHexString(b)
	ab, err := hex.DecodeString(ah)
	Expect(err).NotTo(HaveOccurred())
	bb, err := hex.DecodeString(bh)
	Expect(err).NotTo(HaveOccurred())
	Expect(ab).To(Equal(bb), msg...)
}

// generates a random traceID
func generateTraceID() string {
	return uuid.NewString()
}

func seedTraces(token, endpoint string, traces []string) error {
	//nolint:dogsled
	_, filename, _, _ := runtime.Caller(0)
	d := filepath.Join(filename, "..", "..", "generators")

	cmd := append([]string{
		"run",
		"trace.go",
		"-token",
		token,
		"-endpoint",
		endpoint,
	}, traces...)

	var out bytes.Buffer
	var stderr bytes.Buffer

	c := exec.Command("go", cmd...)
	c.Dir = d
	c.Stdout = &out
	c.Stderr = &stderr

	err := c.Run()
	if err != nil {
		return fmt.Errorf("seedTraces error: %s: %s", fmt.Sprint(err), stderr.String())
	}
	return nil
}

func expectTimeStampDesc(res []*query.TraceItem) {
	prev := int64(math.MaxInt64)
	for i, trace := range res {
		passed := Expect(prev-trace.TimestampNano >= 0).To(BeTrue(), fmt.Sprintf("trace in position %d is older than previous", i))
		if passed {
			prev = trace.TimestampNano
		}
	}
}

func expectDurationDesc(res []*query.TraceItem) {
	prev := int64(math.MaxInt64)
	for i, trace := range res {
		passed := Expect(prev-int64(trace.DurationNano) >= 0).To(BeTrue(), fmt.Sprintf("trace in position %d is shorter than previous", i))
		if passed {
			prev = int64(trace.DurationNano)
		}
	}
}

func expectTimestampAsc(res []*query.TraceItem) {
	prev := int64(0)
	for i, trace := range res {
		passed := Expect(prev-trace.TimestampNano <= 0).To(BeTrue(), fmt.Sprintf("trace in position %d is newer than previous", i))
		if passed {
			prev = trace.TimestampNano
		}
	}
}

func expectDurationAsc(res []*query.TraceItem) {
	prev := int64(0)
	for i, trace := range res {
		passed := Expect(prev-int64(trace.DurationNano) <= 0).To(BeTrue(), fmt.Sprintf("trace in position %d is longer than previous", i))
		if passed {
			prev = int64(trace.DurationNano)
		}
	}
}
