package tests

import (
	"context"
	"crypto/rand"
	"crypto/tls"
	"encoding/json"
	"fmt"
	"math/big"
	"net/http"
	"time"

	. "github.com/onsi/ginkgo/v2"
	. "github.com/onsi/gomega"
	"github.com/onsi/gomega/gstruct"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/common"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/query-api/metrics"
	"go.opentelemetry.io/otel/attribute"
	"go.opentelemetry.io/otel/exporters/otlp/otlpmetric/otlpmetrichttp"
	"go.opentelemetry.io/otel/metric"
	sdkmetric "go.opentelemetry.io/otel/sdk/metric"
	"go.opentelemetry.io/otel/sdk/metric/metricdata"
	"go.opentelemetry.io/otel/sdk/resource"
	semconv "go.opentelemetry.io/otel/semconv/v1.4.0"
)

var _ = Describe("Metrics", Ordered, Serial, func() {
	// tokens for accessing the group/project
	var readOnlyToken string
	var writeOnlyToken string

	// common attributes for generated test-metrics
	commonAttrs := []attribute.KeyValue{
		{
			Key:   attribute.Key("foo"),
			Value: attribute.StringValue("bar"),
		},
	}

	makeMetricsExporter := func(ctx SpecContext, apiKey, urlPath string) *otlpmetrichttp.Exporter {
		opts := []otlpmetrichttp.Option{
			// #nosec G402
			otlpmetrichttp.WithTLSClientConfig(&tls.Config{InsecureSkipVerify: true}),
			otlpmetrichttp.WithEndpoint(testInfra.Configuration().GOBHost),
			otlpmetrichttp.WithURLPath(urlPath),
			otlpmetrichttp.WithHeaders(
				map[string]string{
					"Private-Token": apiKey,
				},
			),
		}

		exporter, err := otlpmetrichttp.New(ctx, opts...)
		Expect(err).NotTo(HaveOccurred())

		return exporter
	}

	sendMetrics := func(ctx SpecContext, apiKey string, metrics *metricdata.ResourceMetrics) error {
		// use standard ingest URL by default here as opposed to /v1/metrics
		url := fmt.Sprintf("/v3/%d/%d/ingest/metrics", gitLab.group.ID, gitLab.project.ID)
		exporter := makeMetricsExporter(ctx, apiKey, url)
		defer exporter.Shutdown(ctx)
		return exporter.Export(ctx, metrics)
	}

	makeMeterReader := func() (metric.Meter, sdkmetric.Reader) {
		const service = "test_otlp_metric_api"
		r, err := resource.Merge(
			resource.Default(),
			resource.NewWithAttributes(
				"",
				semconv.ServiceNameKey.String(service),
				semconv.ServiceVersionKey.String("v0.1.0"),
				attribute.String("environment", "e2e"),
			))
		Expect(err).ToNot(HaveOccurred())

		reader := sdkmetric.NewManualReader()
		mp := sdkmetric.NewMeterProvider(sdkmetric.WithResource(r), sdkmetric.WithReader(reader))
		return mp.Meter(service), reader
	}

	generateMetric := func(ctx SpecContext) *metricdata.ResourceMetrics {
		meter, reader := makeMeterReader()

		randStr, err := common.RandStringASCIIBytes(8)
		Expect(err).ToNot(HaveOccurred())

		c, err := meter.Int64Counter("test_counter_"+randStr, metric.WithDescription("int64 test counter"))
		Expect(err).ToNot(HaveOccurred())
		c.Add(ctx, 2, metric.WithAttributes(commonAttrs...))

		g, err := meter.Float64ObservableGauge(
			"test_float_gauge_"+randStr,
			metric.WithDescription("float64 gauge"),
		)
		Expect(err).ToNot(HaveOccurred())
		n, err := rand.Int(rand.Reader, big.NewInt(100))
		Expect(err).ToNot(HaveOccurred())
		_, err = meter.RegisterCallback(func(_ context.Context, o metric.Observer) error {
			o.ObserveFloat64(g, float64(n.Int64()), metric.WithAttributes(commonAttrs...))
			return nil
		}, g)
		Expect(err).ToNot(HaveOccurred())

		h, err := meter.Float64Histogram(
			"test_histogram_"+randStr,
			metric.WithDescription("float64 histogram"))
		Expect(err).ToNot(HaveOccurred())
		h.Record(ctx, float64(n.Int64()), metric.WithAttributes(commonAttrs...))

		rm := &metricdata.ResourceMetrics{}
		Expect(reader.Collect(ctx, rm)).To(Succeed())
		return rm
	}

	makeMetricsRequest := func(ctx SpecContext, apiKey string) *http.Request {
		By("building query for metrics names")

		req, err := http.NewRequest("GET",
			fmt.Sprintf("%s/v3/query/%d/metrics/autocomplete", testInfra.Configuration().GOBAddress(), gitLab.project.ID), nil)
		Expect(err).ToNot(HaveOccurred())
		req.Header.Set("Private-Token", apiKey)

		return req
	}

	makeMetricSearchRequest := func(ctx SpecContext, apiKey, metricName, metricType string) *http.Request {
		By("building search query for metric names")

		req, err := http.NewRequest(
			"GET",
			fmt.Sprintf("%s/v3/query/%d/metrics/search?mname=%s&mtype=%s",
				testInfra.Configuration().GOBAddress(), gitLab.project.ID, metricName, metricType),
			nil)
		Expect(err).ToNot(HaveOccurred())
		req.Header.Set("Private-Token", apiKey)

		return req
	}

	queryForMetrics := func(ctx SpecContext, apiKey string, rm *metricdata.ResourceMetrics) *metrics.MetricNameResponse {
		By("querying for metric names")

		req := makeMetricsRequest(ctx, apiKey)

		var res *metrics.MetricNameResponse
		// wait for up to ~60s for metric names to appear.
		Eventually(func(g Gomega) {
			resp, err := httpClient.Do(req)
			g.Expect(err).NotTo(HaveOccurred())
			defer resp.Body.Close()
			g.Expect(resp).To(HaveHTTPStatus(http.StatusOK))

			names := &metrics.MetricNameResponse{}
			g.Expect(json.NewDecoder(resp.Body).Decode(names)).To(Succeed())

			// verify that all the named metrics are present
			for _, sm := range rm.ScopeMetrics {
				for _, m := range sm.Metrics {
					g.Expect(names.Metrics).To(ContainElement(
						gstruct.MatchFields(gstruct.IgnoreExtras, gstruct.Fields{
							"Name": Equal(m.Name),
						}),
					))
				}
			}

			res = names
		}).WithTimeout(time.Minute).Should(Succeed())

		return res
	}

	searchForMetrics := func(ctx SpecContext, apiKey string, rm *metricdata.ResourceMetrics) {
		By("searching for metrics")

		// verify that all the named metrics are present
		for _, sm := range rm.ScopeMetrics {
			for _, m := range sm.Metrics {
				var metricType string
				switch v := m.Data.(type) {
				case metricdata.Gauge[int64], metricdata.Gauge[float64]:
					metricType = "Gauge"
				case metricdata.Sum[int64], metricdata.Sum[float64]:
					metricType = "Sum"
				case metricdata.Histogram[int64], metricdata.Histogram[float64]:
					metricType = "Histogram"
				case metricdata.Summary:
					metricType = "Summary"
				default:
					metricType = fmt.Sprintf("%T is not handled", v)
				}
				req := makeMetricSearchRequest(ctx, apiKey, m.Name, metricType)

				Eventually(func(g Gomega) {
					resp, err := httpClient.Do(req)
					g.Expect(err).NotTo(HaveOccurred())
					defer resp.Body.Close()
					g.Expect(resp).To(HaveHTTPStatus(http.StatusOK))

					results := &metrics.MetricsResponse{}
					g.Expect(json.NewDecoder(resp.Body).Decode(results)).To(Succeed())

					g.Expect(results.Results).To(ContainElement(
						gstruct.MatchFields(gstruct.IgnoreExtras, gstruct.Fields{
							"MetricName": Equal(m.Name),
						}),
					))

				}).WithTimeout(time.Minute).Should(Succeed())
			}
		}
	}

	verifyMetricNames := func(rm *metricdata.ResourceMetrics, names *metrics.MetricNameResponse) {
		By("verifying metric details")

		ns := make(map[string]*metrics.MetricName, len(names.Metrics))
		for _, n := range names.Metrics {
			np := n
			ns[n.Name] = &np
		}

		for _, sm := range rm.ScopeMetrics {
			for _, m := range sm.Metrics {
				Expect(ns).To(HaveKey(m.Name), "metric name exists")
				n := ns[m.Name]
				var metricType string
				switch v := m.Data.(type) {
				case metricdata.Gauge[int64], metricdata.Gauge[float64]:
					metricType = "Gauge"
				case metricdata.Sum[int64], metricdata.Sum[float64]:
					metricType = "Sum"
				case metricdata.Histogram[int64], metricdata.Histogram[float64]:
					metricType = "Histogram"
				case metricdata.Summary:
					metricType = "Summary"
				default:
					metricType = fmt.Sprintf("%T is not handled", v)
				}
				Expect(n.Type).To(Equal(metricType), "metric type")
				Expect(n.Description).To(Equal(m.Description), "metric description")

				attrs := make([]string, 0)
				for _, a := range commonAttrs {
					attrs = append(attrs, string(a.Key))
				}
				Expect(n.Attributes).To(Equal(attrs))
			}
		}
	}

	ReportAfterEach(func(report SpecReport) {
		e2eFailed = e2eFailed || report.Failed()
	})

	BeforeAll(func(ctx SpecContext) {
		readOnlyToken = gitLab.groupReadOnlyToken.Token
		writeOnlyToken = gitLab.groupWriteOnlyToken.Token
	})

	Context("Auth handling", func() {
		Context("Ingest", func() {
			It("should not accept an empty api token", func(ctx SpecContext) {
				m := generateMetric(ctx)
				Expect(sendMetrics(ctx, "", m)).NotTo(Succeed())
			})

			It("should not accept an invalid api token", func(ctx SpecContext) {
				m := generateMetric(ctx)
				Expect(sendMetrics(ctx, "bad"+writeOnlyToken, m)).NotTo(Succeed())
			})

			It("should not accept a read-only token", func(ctx SpecContext) {
				m := generateMetric(ctx)
				Expect(sendMetrics(ctx, readOnlyToken, m)).NotTo(Succeed())
			})
		})

		Context("Query", func() {
			It("should not accept an empty api token", func(ctx SpecContext) {
				m := generateMetric(ctx)
				Expect(sendMetrics(ctx, writeOnlyToken, m)).To(Succeed())
				res, err := httpClient.Do(makeMetricsRequest(ctx, ""))
				Expect(err).NotTo(HaveOccurred())
				By("check we got the GitLab login redirect")
				Expect(res).To(HaveHTTPStatus(http.StatusOK))
				Expect(res.Request.URL.Path).To(Equal("/users/sign_in"))
			})

			It("should not accept an invalid api token", func(ctx SpecContext) {
				m := generateMetric(ctx)
				Expect(sendMetrics(ctx, writeOnlyToken, m)).To(Succeed())
				res, err := httpClient.Do(makeMetricsRequest(ctx, "bad"+readOnlyToken))
				Expect(err).NotTo(HaveOccurred())
				Expect(res).To(HaveHTTPStatus(http.StatusForbidden))
			})

			It("should not accept a write-only token", func(ctx SpecContext) {
				m := generateMetric(ctx)
				Expect(sendMetrics(ctx, writeOnlyToken, m)).To(Succeed())
				res, err := httpClient.Do(makeMetricsRequest(ctx, writeOnlyToken))
				Expect(err).NotTo(HaveOccurred())
				Expect(res).To(HaveHTTPStatus(http.StatusForbidden))
			})
		})
	})

	Context("Storage", func() {
		It("can save metrics and retrieve names", func(ctx SpecContext) {
			m := generateMetric(ctx)
			Expect(sendMetrics(ctx, writeOnlyToken, m)).To(Succeed())
			ns := queryForMetrics(ctx, readOnlyToken, m)
			verifyMetricNames(m, ns)
		})

		It("can use the /v1/metrics endpoint to save metrics", func(ctx SpecContext) {
			m := generateMetric(ctx)
			exporter := makeMetricsExporter(ctx, writeOnlyToken, fmt.Sprintf("/v3/%d/%d/ingest/v1/metrics", gitLab.group.ID, gitLab.project.ID))
			defer exporter.Shutdown(ctx)
			Expect(exporter.Export(ctx, m)).To(Succeed())
			ns := queryForMetrics(ctx, readOnlyToken, m)
			verifyMetricNames(m, ns)
		})
	})

	Context("Querying metrics", func() {
		var generatedMetrics *metricdata.ResourceMetrics
		BeforeEach(func(ctx SpecContext) {
			generatedMetrics = generateMetric(ctx)
			Expect(sendMetrics(ctx, writeOnlyToken, generatedMetrics)).To(Succeed())
		})

		It("can search for metrics", func(ctx SpecContext) {
			searchForMetrics(ctx, readOnlyToken, generatedMetrics)
		})
	})
})
