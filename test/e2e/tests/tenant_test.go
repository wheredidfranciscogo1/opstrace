package tests

import (
	"fmt"
	"net"
	"net/url"
	"reflect"

	. "github.com/onsi/ginkgo/v2"
	. "github.com/onsi/gomega"
	"gopkg.in/yaml.v3"

	traefik "github.com/traefik/traefik/v2/pkg/provider/kubernetes/crd/traefikio/v1alpha1"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/common"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/constants"
	appsv1 "k8s.io/api/apps/v1"
	corev1 "k8s.io/api/core/v1"
	"sigs.k8s.io/controller-runtime/pkg/client"
)

// TODO(joe): many of these checks would be better suited to integration tests
// via the envtest framework. We should consider moving them into the operator.
var _ = Describe("tenant provisioning", Ordered, Serial, func() {
	ReportAfterEach(func(report SpecReport) {
		e2eFailed = e2eFailed || report.Failed()
	})

	Context("on creating an example tenant", func() {
		It("ensures it can be created successfully", func(ctx SpecContext) {
			expectGitLabObservabilityTenantReady(ctx, tenant)
			expectNamespaceCreated(ctx, tenant.Namespace())
			expectNamespacePodsReady(ctx, tenant.Namespace())
		})
	})

	Context("ensures resources are configured for the tenant properly", func() {
		deployment := &appsv1.Deployment{}
		It("by checking collector deployment", func(ctx SpecContext) {
			Expect(k8sClient.Get(
				ctx,
				client.ObjectKey{
					Name:      constants.OtelCollectorComponentName,
					Namespace: tenant.Namespace(),
				}, deployment),
			).To(Succeed())
			// we're expecting the deployment to have a single collector container
			Expect(deployment.Spec.Template.Spec.Containers).To(HaveLen(1))
		})

		It("by checking tenant ID", func() {
			tenantIDConfigured := false
			collectorContainer := deployment.Spec.Template.Spec.Containers[0]
			for _, args := range collectorContainer.Args {
				if args == fmt.Sprintf(
					"--set=exporters.gitlabobservability.tenant_id=%d",
					tenant.Spec.TopLevelNamespaceID,
				) {
					tenantIDConfigured = true
					break
				}
			}
			Expect(tenantIDConfigured).To(BeTrue())
		})

		It("by checking ClickHouse credentials", func(ctx SpecContext) {
			secret := &corev1.Secret{}
			Expect(k8sClient.Get(
				ctx,
				client.ObjectKey{
					Name:      constants.GitLabObservabilityTenantCHCredentialsSecretName,
					Namespace: tenant.Namespace(),
				},
				secret,
			)).To(Succeed())

			var nativeURL *url.URL
			if _, ok := secret.Data[constants.ClickHouseCredentialsNativeEndpointKey]; ok {
				nativeURL = common.MustParse(
					string(secret.Data[constants.ClickHouseCredentialsNativeEndpointKey]),
				)
			}
			Expect(nativeURL).ToNot(BeNil())

			chCredentialsConfigured := false
			collectorContainer := deployment.Spec.Template.Spec.Containers[0]
			for _, args := range collectorContainer.Args {
				if args == fmt.Sprintf(
					"--set=exporters.gitlabobservability.clickhouse_dsn=%s/tracing",
					nativeURL.String(),
				) {
					chCredentialsConfigured = true
					break
				}
			}
			Expect(chCredentialsConfigured).To(BeTrue())
		})

		It("by checking ingress", func(ctx SpecContext) {
			ingress := &traefik.IngressRoute{}
			Expect(k8sClient.Get(
				ctx,
				client.ObjectKey{
					Name:      constants.OtelCollectorComponentTraces,
					Namespace: tenant.Namespace(),
				},
				ingress,
			)).To(Succeed())
			// remove port if exists
			gobHost, _, err := net.SplitHostPort(testInfra.Configuration().GOBHost)
			if err != nil {
				// No port found in string
				gobHost = testInfra.Configuration().GOBHost
			}
			expectedSpecYAML := fmt.Sprintf(`
entrypoints:
- websecure
routes:
- kind: Rule
  match: Host("%s") && Path("/v3/%d/{projectID:[0-9]+}/ingest/traces", "/v3/%[2]d/{projectID:[0-9]+}/ingest/v1/traces")
  middlewares:
  - name: cors-headers-otel
    namespace: default
  - name: client-body-max
    namespace: default
  - name: forward-auth
    namespace: default
  - name: strip-group-path-otel
    namespace: default
  - name: path-replace-otel
    namespace: default
  - name: header-transformation-otel
    namespace: default
  priority: 50
  services:
  - loadbalancerspec:
      kind: Service
      name: otel-collector
      namespace: tenant-%d
      nativelb: true
      passhostheader: null
      port:
          intval: 0
          strval: otlp-http
          type: 1
      responseforwarding: null
      scheme: http
      serverstransport: ""
      sticky: null
      strategy: RoundRobin
      weight: null
tls:
  certresolver: ""
  domains: []
  options: null
  secretname: ""
  store: null
`,
				gobHost,
				tenant.Spec.TopLevelNamespaceID,
				tenant.Spec.TopLevelNamespaceID,
			)
			currentSpecYaml, err := yaml.Marshal(ingress.Spec)
			Expect(err).ToNot(HaveOccurred())

			Expect(currentSpecYaml).To(MatchYAML(expectedSpecYAML))
		})

		It("by checking collector definition", func(ctx SpecContext) {
			configmap := &corev1.ConfigMap{}
			Expect(k8sClient.Get(
				ctx,
				client.ObjectKey{
					Name:      constants.OtelCollectorComponentName,
					Namespace: tenant.Namespace(),
				},
				configmap,
			)).To(Succeed())
			// ensure config unmarshalling
			var config map[string]any
			Expect(yaml.Unmarshal([]byte(configmap.Data["config.yaml"]), &config)).To(Succeed())
			// ensure processor configuration
			processors, ok := config["processors"].(map[string]any)
			if ok {
				attributeProcessor := processors["attributes/pid"]
				Expect(
					reflect.DeepEqual(
						attributeProcessor,
						map[string]any{
							"actions": []any{
								map[string]any{
									"action":       "upsert",
									"key":          "gitlab.target_project_id",
									"from_context": "metadata.x-target-projectid",
								},
								map[string]any{
									"action":       "upsert",
									"key":          "gitlab.target_namespace_id",
									"from_context": "metadata.x-target-namespaceid",
								},
							},
						},
					),
				).To(BeTrue())
			}
		})
	})
})
