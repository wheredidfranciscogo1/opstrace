package tests

import (
	"context"
	"crypto/tls"
	"encoding/json"
	"fmt"
	"net/http"
	"os"
	"time"

	"github.com/agoda-com/opentelemetry-logs-go/exporters/otlp/otlplogs"
	"github.com/agoda-com/opentelemetry-logs-go/exporters/otlp/otlplogs/otlplogshttp"
	"github.com/agoda-com/opentelemetry-logs-go/logs"
	sdk "github.com/agoda-com/opentelemetry-logs-go/sdk/logs"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/common"
	qlogs "gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/query-api/logs"
	"go.opentelemetry.io/otel"
	"go.opentelemetry.io/otel/attribute"
	"go.opentelemetry.io/otel/sdk/instrumentation"
	"go.opentelemetry.io/otel/sdk/resource"
	semconv "go.opentelemetry.io/otel/semconv/v1.21.0"
	"go.opentelemetry.io/otel/trace"

	. "github.com/onsi/ginkgo/v2"
	. "github.com/onsi/gomega"
	"github.com/onsi/gomega/gstruct"
)

const (
	traceID = "tidtidtidtidtid_"
	spanID  = "sidsid__"
)

var _ = Describe("Logging", func() {
	var errOtel error

	ReportAfterEach(func(report SpecReport) {
		e2eFailed = e2eFailed || report.Failed()
	})

	BeforeEach(func() {
		errOtel = nil

		otel.SetErrorHandler(otel.ErrorHandlerFunc(
			func(err error) {
				errOtel = err
			}),
		)
	})

	fetchSDKLogger := func(ctx context.Context, token string) logs.Logger {
		hostName, _ := os.Hostname()
		resource := resource.NewWithAttributes(
			semconv.SchemaURL,
			semconv.ServiceName("gob-e2e-logstest"),
			semconv.ServiceVersion("1.0.0"),
			semconv.HostName(hostName),
		)

		logExporter, _ := otlplogs.NewExporter(
			ctx,
			otlplogs.WithClient(
				otlplogshttp.NewClient(
					otlplogshttp.WithProtobufProtocol(),
					// #nosec G402
					otlplogshttp.WithTLSClientConfig(&tls.Config{
						InsecureSkipVerify: true,
					}),
					otlplogshttp.WithEndpoint(testInfra.Configuration().GOBHost),
					otlplogshttp.WithURLPath(fmt.Sprintf("/v3/%d/%d/ingest/logs", gitLab.group.ID, gitLab.project.ID)),
					otlplogshttp.WithHeaders(
						map[string]string{
							"Private-Token": token,
						},
					),
					otlplogshttp.WithTimeout(
						// GDK on devvm can be super slow sometimes
						1*time.Minute,
					),
				),
			),
		)
		loggerProvider := sdk.NewLoggerProvider(
			sdk.WithSyncer(logExporter),
			sdk.WithResource(resource),
		)

		DeferCleanup(func(ctx SpecContext) {
			loggerProvider.Shutdown(ctx)
		})

		sdkLogger := loggerProvider.Logger("gob-e2e-logger")
		return sdkLogger
	}

	emitSDKLog := func(
		ctx context.Context,
		logTime time.Time,
		body, attrBody string,
		sdkLogger logs.Logger,
	) {
		// NOTE(prozlach): We are tapping directly into logging bridge here
		// to reduce the amount of boilerplate needed and force synchronous
		// operation
		tid := trace.TraceID{}
		copy(tid[:], traceID)
		sid := trace.SpanID{}
		copy(sid[:], spanID)
		tf := trace.TraceFlags(0)
		attrs := []attribute.KeyValue{
			attribute.String("gob-e2e-attrbody", attrBody),
		}
		lrc := logs.LogRecordConfig{
			Timestamp:         &logTime,
			ObservedTimestamp: logTime,
			TraceId:           &tid,
			SpanId:            &sid,
			TraceFlags:        &tf,
			SeverityText:      common.Ptr("INFO"),
			SeverityNumber:    common.Ptr(logs.INFO),
			Body:              common.Ptr(body),
			Resource:          nil,
			InstrumentationScope: &instrumentation.Scope{
				Name:      "gitlab.com/vespian",
				Version:   "1.0.0",
				SchemaURL: semconv.SchemaURL,
			},
			Attributes: &attrs,
		}
		lr := logs.NewLogRecord(lrc)
		sdkLogger.Emit(lr)
	}

	fetchLogs := func(
		ctx context.Context,
		apiKey string,
		periodArg, startTimeArg, endTimeArg string,
	) (*http.Response, error) {
		By("quering logs API")

		req, err := http.NewRequest(
			"GET",
			fmt.Sprintf("%s/v3/query/%d/logs/search",
				testInfra.Configuration().GOBAddress(), gitLab.project.ID),
			nil)
		Expect(err).ToNot(HaveOccurred())
		req.Header.Set("Private-Token", apiKey)

		q := req.URL.Query()
		if periodArg != "" {
			q.Add("period", periodArg)
		}
		if startTimeArg != "" {
			q.Add("start_time", startTimeArg)
		}
		if endTimeArg != "" {
			q.Add("end_time", endTimeArg)
		}
		req.URL.RawQuery = q.Encode()

		cl := testInfra.GetHTTPClient()
		cl.CheckRedirect = func(req *http.Request, via []*http.Request) error {
			return http.ErrUseLastResponse
		}

		return cl.Do(req)
	}

	Context("Auth handling", func() {
		Context("Ingestion", func() {
			It("should not accept an empty api token", func(ctx SpecContext) {
				sdkLogger := fetchSDKLogger(ctx, "")
				emitSDKLog(ctx, time.Now().UTC(), "test body", "test attr", sdkLogger)
				Expect(errOtel).To(MatchError(ContainSubstring("401 Unauthorized")))
			})

			It("should not accept an invalid api token", func(ctx SpecContext) {
				sdkLogger := fetchSDKLogger(ctx, "__FOO_MARYNAAAAAAAA")
				emitSDKLog(ctx, time.Now().UTC(), "test body", "test attr", sdkLogger)
				Expect(errOtel).To(MatchError(ContainSubstring("403 Forbidden")))
			})

			It("should not accept a read-only token", func(ctx SpecContext) {
				sdkLogger := fetchSDKLogger(ctx, gitLab.groupReadOnlyToken.Token)
				emitSDKLog(ctx, time.Now().UTC(), "test body", "test attr", sdkLogger)
				Expect(errOtel).To(MatchError(ContainSubstring("403 Forbidden")))
			})
		})
		Context("Quering", func() {
			It("should not accept an empty api token", func(ctx SpecContext) {
				response, err := fetchLogs(ctx, "", "1m", "", "")
				defer func() { _ = response.Body.Close() }()

				Expect(err).NotTo(HaveOccurred())
				Expect(response.StatusCode).To(Equal(http.StatusFound))
			})

			It("should not accept an invalid api token", func(ctx SpecContext) {
				response, err := fetchLogs(ctx, "__FOO_MARYNAAAAAAAA", "1m", "", "")
				defer func() { _ = response.Body.Close() }()

				Expect(err).NotTo(HaveOccurred())
				Expect(response.StatusCode).To(Equal(http.StatusForbidden))
			})

			It("should not accept a write-only token", func(ctx SpecContext) {
				response, err := fetchLogs(ctx, gitLab.groupWriteOnlyToken.Token, "1m", "", "")
				defer func() { _ = response.Body.Close() }()

				Expect(err).NotTo(HaveOccurred())
				Expect(response.StatusCode).To(Equal(http.StatusForbidden))
			})
		})
	})

	Context("Ingestion", func() {
		timeNow := time.Now().UTC()

		It("log records can be ingested and then retrieved", func(ctx SpecContext) {
			uniqPart, err := common.RandStringASCIIBytes(10)
			Expect(err).ToNot(HaveOccurred())
			logBody := uniqPart + " body"
			logAttr := uniqPart + " attr"

			sdkLogger := fetchSDKLogger(ctx, gitLab.groupWriteOnlyToken.Token)
			emitSDKLog(ctx, timeNow, logBody, logAttr, sdkLogger)
			Expect(errOtel).ToNot(HaveOccurred())

			var matchingLogRow qlogs.LogsRow

			Eventually(func(g Gomega) {
				// NOTE(prozlach): Do not make this filter too tight, as the
				// time between the query-api's pod host and the host that is
				// running e2e may be not in sync!
				response, err := fetchLogs(ctx, gitLab.groupReadWriteToken.Token, "4h", "", "")
				defer func() { _ = response.Body.Close() }()

				g.Expect(err).NotTo(HaveOccurred())
				g.Expect(response).To(HaveHTTPStatus(http.StatusOK))

				logsData := new(qlogs.LogsResponse)
				g.Expect(json.NewDecoder(response.Body).Decode(logsData)).To(Succeed())

				g.Expect(logsData.Results).To(ContainElement(
					gstruct.MatchFields(
						gstruct.IgnoreExtras,
						gstruct.Fields{
							"Body": Equal(logBody),
						},
					),
					&matchingLogRow,
				))

			}).WithTimeout(time.Minute).Should(Succeed())

			Expect(matchingLogRow.Timestamp).To(Equal(timeNow))
			Expect(matchingLogRow.TraceID).To(Equal(traceID))
			Expect(matchingLogRow.SpanID).To(Equal(spanID))
			Expect(matchingLogRow.SeverityText).To(Equal("INFO"))
			Expect(matchingLogRow.SeverityNumber).To(Equal(uint8(logs.INFO)))
			Expect(matchingLogRow.TraceFlags).To(Equal(uint32(0)))
			Expect(matchingLogRow.LogAttributes).To(HaveKeyWithValue(
				"gob-e2e-attrbody", logAttr,
			))
			Expect(matchingLogRow.ResourceAttributes).To(HaveKeyWithValue(
				"service.name", "gob-e2e-logstest",
			))
		})

		DescribeTable("filtering by time",
			func(ctx SpecContext,
				prePeriodArg, preStartTimeArg, preEndTimeArg string,
				postPeriodArg, postStartTimeArg, postEndTimeArg string,
			) {
				sdkLogger := fetchSDKLogger(ctx, gitLab.groupWriteOnlyToken.Token)
				uniqPart, err := common.RandStringASCIIBytes(10)
				Expect(err).ToNot(HaveOccurred())
				logBody := uniqPart + " body"

				emitSDKLog(ctx, timeNow.Add(-8*time.Hour), logBody, "", sdkLogger)
				Expect(errOtel).ToNot(HaveOccurred())

				emitSDKLog(ctx, timeNow, logBody, "", sdkLogger)
				Expect(errOtel).ToNot(HaveOccurred())

				var matchingLogRows []qlogs.LogsRow

				assertFunc := func(
					ctx context.Context,
					g Gomega,
					periodArg, startTimeArg, endTimeArg string,
					times int,
				) {
					response, err := fetchLogs(
						ctx,
						gitLab.groupReadWriteToken.Token,
						periodArg, startTimeArg, endTimeArg,
					)
					defer func() { _ = response.Body.Close() }()
					g.Expect(err).NotTo(HaveOccurred())
					g.Expect(response).To(HaveHTTPStatus(http.StatusOK))

					matchingLogRows = nil
					logsData := new(qlogs.LogsResponse)
					g.Expect(json.NewDecoder(response.Body).Decode(logsData)).To(Succeed())
					g.Expect(logsData.Results).To(ContainElement(
						gstruct.MatchFields(
							gstruct.IgnoreExtras,
							gstruct.Fields{
								"Body": Equal(logBody),
							},
						),
						&matchingLogRows,
					))

					g.Expect(matchingLogRows).To(HaveLen(times), fmt.Sprintf("found %d rows instead of %d", len(matchingLogRows), times))
				}

				Eventually(func(g Gomega) {
					assertFunc(ctx, g, prePeriodArg, preStartTimeArg, preEndTimeArg, 1)
					assertFunc(ctx, g, postPeriodArg, postStartTimeArg, postEndTimeArg, 2)
				}).WithTimeout(time.Minute).Should(Succeed())
			},
			Entry("symbolic time filters",
				// NOTE(prozlach): Do not make these filters too tight, as the
				// time between the query-api's pod host and the host that is
				// running e2e may be not in sync!
				"4h", "", "",
				"12h", "", "",
			),
			Entry("timestmamp filters",
				"", timeNow.Add(-5*time.Hour).Format(time.RFC3339), timeNow.Add(1*time.Second).Format(time.RFC3339),
				"", timeNow.Add(-11*time.Hour).Format(time.RFC3339), timeNow.Add(1*time.Second).Format(time.RFC3339),
			),
		)
	})
})
