# Sentry-based instrumentation for Java

This sample app demonstrates how to instrument your code using Sentry SDK for Java.

To get started:

* Export your Sentry SDN as an environment variable `SENTRY_DSN`

```
export SENTRY_DSN=http://username@localhost:8080/projects/12345
```

* After that run this program:

```
mvn clean compile package
...
java -jar target/Application.jar
```

The output from the application should look like:

```
➜  java git:(main) ✗ java -jar target/Application.jar
INFO: Initializing SDK with DSN: 'http://public:private@localhost:8888/projects/12345'
INFO: No outbox dir path is defined in options.
INFO: GlobalHubMode: 'false'
DEBUG: UncaughtExceptionHandlerIntegration enabled: true
DEBUG: UncaughtExceptionHandlerIntegration installed.
DEBUG: ShutdownHookIntegration installed.
DEBUG: Capturing event: f41252da8c7f483b8b73fcb6da9c7ba9
INFO: sentry-external-modules.txt file was not found.
INFO: Session is null on scope.withSession
DEBUG: Capturing event: 13a3241d9bd54fc0ae43afa640df2826
INFO: Session is null on scope.withSession
DEBUG: Serializing object: {...}
DEBUG: Envelope sent successfully.
DEBUG: Envelope flushed
DEBUG: Serializing object: {...}
DEBUG: Envelope sent successfully.
DEBUG: Envelope flushed
```
