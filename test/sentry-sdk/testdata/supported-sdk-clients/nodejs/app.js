const Sentry = require('@sentry/node');
const https = require('https');

const unsafeHttpsModule = {
  request(options, callback) {
    const allowUnauthorized = process.env.INSECURE_OK == "true";
    console.info("(Making HTTP request with unsafe https module with rejectUnauthorized: %s)", !allowUnauthorized);
    return https.request({ ...options, rejectUnauthorized: !allowUnauthorized }, callback);
  },
};

Sentry.init({
  dsn: process.env.SENTRY_DSN,
  debug: true,
  release:"my-javascriprt-project@1.0.0",
  environment: "dev",
  transportOptions: {
    httpModule: unsafeHttpsModule,
  },
});

Sentry.captureException(new Error("Hello from Sentry Javascript SDK"));
