# Sentry-based instrumentation for NodeJS

This sample app demonstrates how to instrument your code using Sentry SDK for NodeJS.

To get started:

* Export your Sentry SDN as an environment variable `SENTRY_DSN`

```
export SENTRY_DSN=http://username@localhost:8080/projects/12345
```

* After that run this program:

```
npm install
npm start
```

The output from the application should look like:

```
➜  nodejs git:(main) ✗ npm start
$ node app.ts
Sentry Logger [log]: Integration installed: InboundFilters
Sentry Logger [log]: Integration installed: FunctionToString
Sentry Logger [log]: Integration installed: Console
Sentry Logger [log]: Integration installed: Http
Sentry Logger [log]: Integration installed: OnUncaughtException
Sentry Logger [log]: Integration installed: OnUnhandledRejection
Sentry Logger [log]: Integration installed: ContextLines
Sentry Logger [log]: Integration installed: LocalVariables
Sentry Logger [log]: Integration installed: Context
Sentry Logger [log]: Integration installed: Modules
Sentry Logger [log]: Integration installed: RequestData
Sentry Logger [log]: Integration installed: LinkedErrors
About to crash!
Error: We crashed!!!!!
    at Timeout._onTimeout (/Users/ankitbhatnagar/go/src/gitlab.com/gitlab-org/opstrace/test/sentry-sdk/testdata/supported-sdk-clients/nodejs/app.ts:31:9)
    at listOnTimeout (node:internal/timers:573:17)
    at processTimers (node:internal/timers:514:7)
(Making HTTP request with unsafe https module with rejectUnauthorized: true)
(Making HTTP request with unsafe https module with rejectUnauthorized: true)
(Making HTTP request with unsafe https module with rejectUnauthorized: true)
error Command failed with exit code 1.
info Visit https://yarnpkg.com/en/docs/cli/run for documentation about this command.
```