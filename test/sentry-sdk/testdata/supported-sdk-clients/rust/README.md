# Sentry-based instrumentation for Rust

This sample app demonstrates how to instrument your code using Sentry SDK for Rust.

To get started:

* Export your Sentry SDN as an environment variable `SENTRY_DSN`

```
export SENTRY_DSN=http://username@localhost:8080/projects/12345
```

* After that run this program:

```sh
cargo run
```

The output from the application should look like:

```sh
    Finished dev [unoptimized + debuginfo] target(s) in 21.89s
     Running `target/debug/rust`
thread 'main' panicked at 'Everything is on fire!', src/main.rs:8:3
note: run with `RUST_BACKTRACE=1` environment variable to display a backtrace
```
