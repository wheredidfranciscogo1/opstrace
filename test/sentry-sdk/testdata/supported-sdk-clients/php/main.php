<?
require_once __DIR__ . '/vendor/autoload.php';

$dsn = $_ENV['SENTRY_DSN'];
$insecure = $_ENV['INSECURE_OK'] == 'true';

Sentry\init([
    'http_ssl_verify_peer' => !$insecure,
    'dsn' => $dsn,
    'attach_stacktrace' => true,
    'logger' => new \Sentry\Logger\DebugStdOutLogger()
]);

Sentry\captureException(new BadMethodCallException('Some Method'));

Sentry\captureMessage('Something went wrong, some message');
?>
