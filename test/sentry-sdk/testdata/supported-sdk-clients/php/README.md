# Sentry-based instrumentation for PHP

This sample app demonstrates how to instrument your code using Sentry SDK for PHP.

To get started:

* Export your Sentry SDN as an environment variable `SENTRY_DSN`

```
export SENTRY_DSN=http://username@localhost:8080/projects/12345
```

* After that run this program

```
php main.php
```
