module gitlab.com/gitlab-org/opstrace/opstrace/test/sentry-sdk/testdata/supported-sdk-clients

go 1.21

require github.com/getsentry/sentry-go v0.26.0

require (
	golang.org/x/sys v0.16.0 // indirect
	golang.org/x/text v0.14.0 // indirect
)
