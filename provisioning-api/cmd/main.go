package main

import (
	"context"
	"flag"
	"fmt"
	"net/http"
	"os"
	"os/signal"
	"syscall"
	"time"

	ginzap "github.com/gin-contrib/zap"
	"github.com/gin-gonic/gin"
	"github.com/prometheus/client_golang/prometheus"
	"github.com/prometheus/client_golang/prometheus/collectors"
	"github.com/prometheus/client_golang/prometheus/promhttp"
	"go.uber.org/zap"
	kruntime "k8s.io/apimachinery/pkg/runtime"
	"k8s.io/client-go/dynamic"
	"sigs.k8s.io/controller-runtime/pkg/client"
	"sigs.k8s.io/controller-runtime/pkg/client/config"

	_ "go.uber.org/automaxprocs"

	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/common"
	"gitlab.com/gitlab-org/opstrace/opstrace/provisioning-api/pkg/provisioningapi"
	schedulerapi "gitlab.com/gitlab-org/opstrace/opstrace/scheduler/api"
)

var (
	metricsAddr string
	serviceAddr string
	logLevel    string
)

func parseCmdline() {
	config.RegisterFlags(flag.CommandLine)
	flag.StringVar(&metricsAddr, "metrics-bind-address", ":8081", "The address the metric endpoint binds to.")
	flag.StringVar(&serviceAddr, "service-bind-address", ":8080", "The address the service endpoint binds to.")
	flag.StringVar(&logLevel, "log-level", "info", "The log-level to use.")
	flag.Parse()
}

// setupK8sClients creates a k8s clients
func setupK8sClients(
	logger *zap.SugaredLogger,
) (client.Client, *dynamic.DynamicClient, error) {
	clientScheme := kruntime.NewScheme()
	if err := schedulerapi.AddToScheme(clientScheme); err != nil {
		return nil, nil, fmt.Errorf("unable to add opstrace scheme: %w", err)
	}

	restConfig, err := config.GetConfig()
	if err != nil {
		return nil, nil, fmt.Errorf("unable to create rest config: %w", err)
	}

	crK8sClient, err := client.New(restConfig, client.Options{Scheme: clientScheme})
	if err != nil {
		return nil, nil, fmt.Errorf("unable to create clientset client: %w", err)
	}

	dynamicK8sClient, err := dynamic.NewForConfig(restConfig)
	if err != nil {
		return nil, nil, fmt.Errorf("unable to create dynamic client: %w", err)
	}

	logger.Debug("k8s clients have been set up")

	return crK8sClient, dynamicK8sClient, nil
}

func main() {
	// NOTE(prozlach) This nesting is used to make all the defered calls work
	// when exiting the program
	os.Exit(runProvisioningAPI())
}

func runProvisioningAPI() int {
	parseCmdline()

	logger, syncF, _ := common.SetupLogger(logLevel)
	defer syncF()

	ctx, cancelFunc := signal.NotifyContext(context.Background(), os.Interrupt, syscall.SIGTERM)
	defer cancelFunc()

	metricsRegistry := prometheus.NewRegistry()
	metricsRegistry.MustRegister(collectors.NewProcessCollector(collectors.ProcessCollectorOpts{}))
	metricsRegistry.MustRegister(collectors.NewGoCollector())
	metricsHandler := promhttp.HandlerFor(metricsRegistry, promhttp.HandlerOpts{
		ErrorHandling:     promhttp.HTTPErrorOnError,
		EnableOpenMetrics: true,
	})
	routerMetrics := http.NewServeMux()
	routerMetrics.Handle("/metrics", metricsHandler)

	if logLevel == "debug" {
		gin.SetMode(gin.DebugMode)
	} else {
		gin.SetMode(gin.ReleaseMode)
	}
	routerMain := gin.New()
	routerMain.Use(
		ginzap.GinzapWithConfig(
			logger.Desugar(),
			&ginzap.Config{
				TimeFormat: time.RFC3339,
				UTC:        true,
				SkipPaths:  []string{"/readyz"},
			},
		),
	)
	routerMain.Use(ginzap.RecoveryWithZap(logger.Desugar(), true))
	routerMain.Use(common.RouteMetrics(metricsRegistry))

	crK8sClient, dynamicK8sClient, err := setupK8sClients(logger)
	if err != nil {
		logger.Errorw("unable to create k8s clients", "error", err)
		return 1
	}
	tenantsDB, err := provisioningapi.NewGitlabTenantDB(logger, ctx, dynamicK8sClient)
	if err != nil {
		logger.Errorw("unable to create tenantsDB", "error", err)
		return 1
	}

	cfg := provisioningapi.Config{
		K8sClient: crK8sClient,
		TenantsDB: tenantsDB,
		Logger:    logger,
	}

	provisioningapi.SetRoutes(routerMain, cfg)

	logger.Infof("log level: %s", logLevel)
	logger.Infow("starting main HTTP server", "address", serviceAddr)
	logger.Infow("starting metrics HTTP server", "address", metricsAddr)

	metricServer := &http.Server{
		Addr:              metricsAddr,
		Handler:           routerMetrics,
		ReadHeaderTimeout: time.Second * 3,
	}

	mainServer := &http.Server{
		Addr:              serviceAddr,
		Handler:           routerMain,
		ReadHeaderTimeout: time.Second * 3,
	}

	err = common.ServeWithGracefulShutdown(ctx, logger, mainServer, metricServer)
	if err != nil {
		logger.Errorf("error occurred while shutting down http servers: %w", err)
		return 1
	}

	logger.Info("server shutdown is complete")
	return 0
}
