package provisioningapi

import (
	"net/http"

	"github.com/gin-gonic/gin"
)

func SetRoutes(router *gin.Engine, cfg Config) {
	v3 := router.Group("/v3")
	{
		auth := v3.Group("/tenant")
		{
			// `GET /v3/tenant/:project_id` fetches information about the
			// GitlabObservabilityTenant for the top-level namespace of the
			// project with given :project_id.
			//
			// Two responses are possible:
			// * HTTP 200 - tenant already exist
			// * HTTP 404 - tenant has not been created yet
			//
			// The 200 response is a JSON with the following fields:
			// ```
			// {
			//   "name": "tenant-22",
			//   "topLevelNamespaceID": 22,
			//   "status": "not ready"
			// }
			// ```
			// Name and topLevelNamespaceID are the name of the
			// GitlabObservabilityTenant object and the top-level namespace ID
			// that it represents. The status field can have one of three values:
			//
			// * `ready` - tenant has been provisioned and is ready to serve
			//   traffic
			// * `not ready` - tenant is provisioning
			// * `unknown` - provisioning API is unable to determine the state of
			//   the tenant. This happens when the status fields are not set for
			//   the object, e.g. right after object creation, where the
			//   controller has not yet started to reconcile the objects.
			auth.GET("/:project_id", FetchTenantHandlerFactory(cfg))
			// `PUT /v3/tenant/:project_id` creates a new
			// GitlabObservabilityTenant for the top-level namespace of the
			// project with given :project_id.
			//
			// Two responses are possible:
			// * HTTP 201 - tenant was created
			// * HTTP 409 - tenant already exists
			//
			// Due to the fact that ATM tenant does not have any
			// user-modifyable fields, the PUT call takes no arguments.
			//
			// In the long term we will probably start defining things like
			// e.g. t-shirt sizes which will be passed by a json in a request
			// body and then validated+decoded by Gin using a dedicated struct.
			// This requires more thought as the tenant creation calls are
			// project-scoped (i.e. user pases only their projectID in the
			// call) and e.g. t-shirt size affects whole top-level group. On
			// the other hand the provisioning API uses user's credentials
			// (GLPAT,Oauth token), and changing of the tenant's t-shirt size
			// would require some extra billing checks and authorization - user
			// should not be allowed to do it themselves.
			auth.PUT("/:project_id", CreateTenantHandlerFactory(cfg))
			// `DELETE /v3/tenant/:project_id` call is not implemented yet and
			// always returns `HTTP 501`
			auth.DELETE("/:project_id", DeleteTenantHandlerFactory(cfg))
		}
	}

	// `GET /readyz` is used by k8s for healtchecking the application.
	router.GET("/readyz", func(ginCtx *gin.Context) {
		ginCtx.String(http.StatusOK, "Success")
	})

	router.NoRoute(func(ginCtx *gin.Context) {
		ginCtx.AbortWithStatus(http.StatusNotFound)
	})
}
