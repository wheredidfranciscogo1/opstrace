package provisioningapi_test

import (
	"context"
	"testing"
	"time"

	. "github.com/onsi/ginkgo/v2"
	. "github.com/onsi/gomega"

	uberzap "go.uber.org/zap"
	"go.uber.org/zap/zapcore"
	kruntime "k8s.io/apimachinery/pkg/runtime"
	"k8s.io/client-go/dynamic"
	"k8s.io/client-go/rest"
	"sigs.k8s.io/controller-runtime/pkg/client"
	"sigs.k8s.io/controller-runtime/pkg/envtest"
	logf "sigs.k8s.io/controller-runtime/pkg/log"
	"sigs.k8s.io/controller-runtime/pkg/log/zap"

	schedulerapi "gitlab.com/gitlab-org/opstrace/opstrace/scheduler/api"
)

var (
	crK8sClient      client.Client
	dynamicK8sClient *dynamic.DynamicClient
	restConfig       *rest.Config

	testEnv *envtest.Environment

	rootCtx context.Context
	cancelF context.CancelFunc

	logger *uberzap.SugaredLogger
)

func TestProvisioningAPI(t *testing.T) {
	RegisterFailHandler(Fail)
	suiteConfig, reporterConfig := GinkgoConfiguration()
	SetDefaultEventuallyTimeout(30 * time.Second)
	SetDefaultEventuallyPollingInterval(2 * time.Second)

	RunSpecs(t, "Provisioning API Test Suite", suiteConfig, reporterConfig)
}

var _ = BeforeSuite(func() {
	var err error

	By("setting up logger")
	setTimeEncoderOpt := func(o *zap.Options) {
		o.TimeEncoder = zapcore.RFC3339TimeEncoder
	}

	logf.SetLogger(
		zap.New(
			zap.WriteTo(GinkgoWriter),
			zap.UseDevMode(true),
			setTimeEncoderOpt,
		),
	)
	logger = zap.NewRaw(
		zap.WriteTo(GinkgoWriter),
		zap.UseDevMode(true),
		setTimeEncoderOpt,
	).Sugar()

	rootCtx, cancelF = context.WithCancel(context.Background())

	By("bootstrapping test environment")
	testEnv = &envtest.Environment{
		CRDDirectoryPaths:     []string{"../../../scheduler/config/crd/bases"},
		ErrorIfCRDPathMissing: true,
	}
	restConfig, err = testEnv.Start()
	Expect(err).NotTo(HaveOccurred())
	Expect(restConfig).NotTo(BeNil())

	By("setting up k8s clients")
	clientScheme := kruntime.NewScheme()
	Expect(schedulerapi.AddToScheme(clientScheme)).To(Succeed())

	crK8sClient, err = client.New(restConfig, client.Options{Scheme: clientScheme})
	Expect(err).NotTo(HaveOccurred())
	Expect(crK8sClient).NotTo(BeNil())

	dynamicK8sClient, err = dynamic.NewForConfig(restConfig)
	Expect(err).NotTo(HaveOccurred())
	Expect(dynamicK8sClient).NotTo(BeNil())
})

var _ = AfterSuite(func() {
	By("tearing down the test environment")
	Expect(testEnv.Stop()).To(Succeed())

	cancelF()
})
