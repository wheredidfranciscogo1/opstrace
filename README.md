<!-- markdownlint-disable MD041 -->
<!-- markdownlint-disable MD033 -->

<!-- TODO(joe): Add GitLab pipeline status badge -->

<img alt="logo" src="https://user-images.githubusercontent.com/19239758/97793010-00161b00-1ba3-11eb-949b-e62eae6fdb9c.png" width="350">

# GitLab Observability Backend

GitLab Observability Backend (GOB) is the backend for GitLab Observability features. It is a horizontally scalable, long-term storage solution for observability data.

It is designed to integrate natively with the GitLab UX.

## Highlights

* Horizontally **scalable**.
* Inexpensive **long-term** retention of observability data.
* Rigoriously **tested** end-to-end.
* Easy and reliable **upgrades**.
* **Secure** by default with [TLS](https://letsencrypt.org) and authenticated endpoints.
* **Easy to configure** with GUIs and APIs.

We walk on the shoulders of giants; GOB uses open source projects you know and love:

* [ClickHouse](https://github.com/ClickHouse)
* [Kubernetes](https://github.com/kubernetes/kubernetes)
* [Prometheus](https://github.com/prometheus/prometheus)
* [Prometheus Operator](https://github.com/prometheus-operator/prometheus-operator)
* [OpenTelemetry](https://github.com/open-telemetry)
* and many more

## Community

Please join us to learn more, get support, or contribute to the project.

* Join our [Community](https://about.gitlab.com/community/)
* Ask questions in our [Community Forum](https://forum.gitlab.com/c/observability/)
* For problems, review [issues](https://gitlab.com/gitlab-org/opstrace/opstrace/-/issues) and/or open a [bug report](https://gitlab.com/gitlab-org/opstrace/opstrace/-/issues/new?issuable_template=Bug%20report)
* Contribute a [feature proposal](https://gitlab.com/gitlab-org/opstrace/opstrace/-/issues/new?issuable_template=Proposal)


## Documentation

<!-- TODO: docs.gitlab.com -->
You can find documentation for GOB in [/docs](./docs).
We invite you to improve these docs together with us, and have a [corresponding guide](./docs/guides/contributor/writing-docs.md) for that.

## Contributing

We believe in a world where [everyone can contribute](https://about.gitlab.com/company/mission/).

Please join us and help with your contributions!

<!-- TODO: Unify into GitLab docs. -->
* Start by reading the [Contributing guide](./CONTRIBUTING.md) to become familiar with the process.
* Then review our [Development guide](./docs/guides/contributor/setting-up-your-dev-env.md) to learn how to set up your environment and build the code.

Take a look at our [handbook page](https://about.gitlab.com/handbook/engineering/development/ops/monitor/observability/) to see where we're heading.

IMPORTANT NOTE: We welcome contributions from developers of all backgrounds. We encode that in our [Community Code of Conduct](https://about.gitlab.com/community/contribute/code-of-conduct/).
By participating in this project, you agree to abide by its terms.

### Repository reference

This repository holds the primary components of GOB. This per-directory listing overview should help developers by providing a brief description of where things are:

* `.gitlab`: scripts/tooling to help run our CI pipelines on Gitlab
* `benchmark`: scripts/tooling for running our benchmarking test suite, currently using `k6`
* `ci`: scripts/tooling for running tests in our CI pipelines
* `clickhouse-operator`: A kubebuilder-based K8s Operator for managing one or more ClickHouse instances. This is structured as an independent project and may soon be broken out into a dedicated repo. We previously used the [Altinity ClickHouse Operator](https://github.com/Altinity/clickhouse-operator/) but in practice it required manually editing Pod templates in order to get a working cluster
* `containers/ci`: Dockerfile definitions for our CI build setup
* `docs`: Mostly a living document, includes user guides, details of our architecture and a quickstart to help you get started
* `gatekeeper`: Our authn/authz service written in Go. It is integrated with a separate GitLab instance which helps user-management.
* `go`: Peripheral Go components/packages that don't quite warrant their own subdirectory (yet).
  * `cmd`: Standalone executables, run as their own containers in a GOB instance. These do not implement their own authn/authz, instead it is handled at ingress.
    * `errortracking`: Runner for an OpenAPI based REST API for Gitlab's error tracking feature
    * `tracing`: Stock build of modules from [`opentelemetry-collector`](https://github.com/open-telemetry/opentelemetry-collector/)+[`opentelemetry-collector-contrib`](https://github.com/open-telemetry/opentelemetry-collector-contrib/). The modules are selected to support accepting otel-format data, converting it to Jaeger format, then sending it to a tenant Jaeger instance, deployed per tenant.
  * `pkg`: Library packages that may be shared by the above `cmd` executables, or by Go code elsewhere in the repo.
* `scheduler`: A kubebuilder-based K8s operator that provisions & manages a GOB instance as a whole. This deals with initial setup of tenants/groups that have enabled monitoring, which are then internally owned by `tenant-operator`.
* `provisioning-api`: An API to allow enabling of GitLab group level tenants.
* `support`: Collection of `make` files and scripts to help install developer environment dependencies.
* `tenant-operator`: A kubebuilder-based K8s operator that manages individual tenants within a GOB instance, also managing per-tenant components/deployments, e.g. `errortracking`, `tracing`, etc.
* `terraform`: TF modules and examples for setting up new GOB instances, currently only adding support for GCP.
* `test`: Directory containing tests and/or test-setups.
  * `e2e`: Directory containing E2E test-suite, learn more about them [here](https://gitlab.com/gitlab-org/opstrace/opstrace/-/tree/main/test/e2e).

## Important note on security

Please note that Opstrace is not offered as a self-hosted solution and hence backporting security fixes on previous Opstrace release versions is not supported.

## Security Reports

Please report suspected security vulnerabilities by following the [disclosure process on the GitLab.com website](https://about.gitlab.com/security/disclosure/).

