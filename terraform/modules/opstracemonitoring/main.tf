# -------------------------------------------------------------------------------------------
# utils - BlackBox-exporter, smoke tests - additional scrape configs
# -------------------------------------------------------------------------------------------

# BlackBox Exporter secret holding grafana API key used for auth
# for the GOUI probe
resource "kubernetes_secret" "bbe-api-key" {
  count = var.provision_blackbox_exporter ? 1 : 0
  metadata {
    name = "bbe-api-key"
  }

  data = {
    apikey = var.blackbox_exporter_api_key
  }
}

resource "kubernetes_secret" "smoketests-errortracking-secret" {
  metadata {
    name = "smoketests-errortracking-secret"
  }

  data = {
    gitlabobservabilityapitoken = var.gitlab_observability_api_token
    grafanaapitoken             = var.blackbox_exporter_api_key
    sentrydsn                   = var.sentry_dsn
  }
}

resource "kubernetes_config_map" "smoketests-errortracking-config" {
  metadata {
    name = "smoketests-errortracking-config"
  }

  data = {
    group_error_tracking_endpoint      = var.group_error_tracking_endpoint
    error_tracking_ingestion_delay_sec = "10"
    log_level                          = "INFO"
    test_period_sec                    = "10"
    retries                            = "15"
  }

}



data "kustomization_build" "alerting" {
  path = "${path.module}/alerting/overlays/${var.environment}"
}

resource "kustomization_resource" "alerting" {
  for_each = var.provision_alerting ? data.kustomization_build.alerting.ids : []
  manifest = data.kustomization_build.alerting.manifests[each.value]
}

data "kustomization_overlay" "blackbox-exporter-manifest" {
  resources = ["${path.module}/blackbox-exporter"]
  patches {

    target {
      kind = "ServiceMonitor"
      name = "gatekeeper-probe"
    }
    patch = <<-EOF
      - op: replace
        path: /spec/endpoints/0/params/target/0
        value: "${var.gatekeeper_probe_url}"
    EOF

  }
}

resource "kustomization_resource" "blackbox-exporter-manifest" {
  for_each = var.provision_blackbox_exporter ? data.kustomization_overlay.blackbox-exporter-manifest.ids : []
  manifest = data.kustomization_overlay.blackbox-exporter-manifest.manifests[each.value]
  depends_on = [
    kubernetes_secret.bbe-api-key,
  ]
}

resource "kustomization_resource" "smoketests-manifest" {
  for_each = data.kustomization_overlay.smoke-tests.ids
  manifest = data.kustomization_overlay.smoke-tests.manifests[each.value]
  depends_on = [
    kubernetes_secret.smoketests-errortracking-secret,
  ]
}

data "kustomization_overlay" "smoke-tests" {
  resources = ["${path.module}/smoketests"]
  patches {
    patch = <<-EOF
      - op: replace
        path: /spec/template/spec/containers/0/image
        value: ${var.smoketest_image}
    EOF
    target {
      group   = "apps"
      version = "v1"
      kind    = "Deployment"
      name    = "smoketests-errortracking"
    }
  }
}

resource "kubernetes_secret" "ch-cloud-monitoring-secret" {
  count = var.provision_ch_cloud_scrape_config ? 1 : 0
  metadata {
    name = "ch-cloud-monitoring-secret"
  }

  data = {
    user     = var.ch_cloud_scrape_user
    password = var.ch_cloud_scrape_password
  }
}

resource "kustomization_resource" "scrape-configs" {
  for_each = var.provision_ch_cloud_scrape_config ? data.kustomization_overlay.ch-cloud-scrape-config.ids : []
  manifest = data.kustomization_overlay.ch-cloud-scrape-config.manifests[each.value]
  depends_on = [
    kubernetes_secret.ch-cloud-monitoring-secret,
  ]
}

data "kustomization_overlay" "ch-cloud-scrape-config" {
  resources = ["${path.module}/scrape-configs"]
  patches {
    patch = <<-EOF
      - op: replace
        path: /spec/staticConfigs/0/targets/0
        value: ${var.ch_cloud_url}
    EOF
    target {
      name = "static-config-clickhouse-cloud"
      kind = "ScrapeConfig"
    }
  }
}