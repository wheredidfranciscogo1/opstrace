data "kustomization_overlay" "traefik-manifest" {
  resources = ["${path.module}/traefik"]
  patches {
    target {
      kind = "Service"
      name = "traefik"
    }
    patch = jsonencode({
      "apiVersion" = "v1",
      "kind"       = "Service",
      "metadata" = {
        "name" = "traefik"
      }
      "spec" = {
        "loadBalancerIP" : var.traefik_address
        "externalTrafficPolicy" : "Local"
      }
    })
  }
}

resource "kubernetes_namespace" "traefik-namespace" {
  provider = kubernetes
  metadata {
    name = "traefik"
  }
}

resource "kustomization_resource" "traefik" {
  for_each   = data.kustomization_overlay.traefik-manifest.ids
  manifest   = data.kustomization_overlay.traefik-manifest.manifests[each.value]
  depends_on = [kubernetes_namespace.traefik-namespace]
}
