resource "kubernetes_ingress_v1" "thanos-receiver-ingress" {
  metadata {
    name        = "thanos-receiver-remote-write"
    namespace   = var.namespace
    annotations = {}
  }

  spec {
    ingress_class_name = "nginx"
    rule {
      host = var.thanos_receive_remote_write_domain
      http {
        path {
          path_type = "Prefix"
          path      = "/"
          backend {
            service {
              name = "thanos-receive"
              port {
                number = 19291 # remote write port
              }
            }
          }
        }
      }
    }
  }
}




