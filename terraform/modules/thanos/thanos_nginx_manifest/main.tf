
locals {
  allowed_ips = split(",", var.source_ip_ranges_allowed_access)
}

data "kustomization_overlay" "nginx-ingress-manifest" {
  resources = [
    "${path.module}/nginx-ingress",
  ]

  patches {
    target {
      kind = "Service"
      name = "ingress-nginx-controller"
    }
    patch = jsonencode({
      "apiVersion" = "v1",
      "kind"       = "Service",
      "metadata" = {
        "name" = "ingress-nginx-controller"
      }
      "spec" = {
        "loadBalancerSourceRanges" = [for ip in local.allowed_ips : ip]
        "loadBalancerIP" : var.ing_address
      }
    })
  }

  patches {
    target {
      kind      = "ConfigMap"
      name      = "ingress-nginx-controller"
      namespace = "ingress-nginx"
    }
    patch = jsonencode({
      "apiVersion" = "v1",
      "kind"       = "ConfigMap",
      "metadata" = {
        "name" = "ingress-nginx-controller"
      }
      "data" = {
        "client-body-buffer-size" : "10M"
        "proxy-body-size" : "10M"
      }
    })
  }
}

resource "kustomization_resource" "nginx-ingress" {
  for_each = data.kustomization_overlay.nginx-ingress-manifest.ids
  manifest = data.kustomization_overlay.nginx-ingress-manifest.manifests[each.value]
}


# Adds a firewall rule to allow connections on 8443 from cluster master nodes to worker nodes
# This is required for the nginx-ingress
# See https://kubernetes.github.io/ingress-nginx/deploy/#gce-gke
resource "google_compute_firewall" "allow_ingress_rule" {

  name        = "gke-${var.gke_cluster_name}-allow-nginx-ingress-fw"
  network     = var.gke_vpc_name
  project     = var.project_id
  description = "Allow traffic from cluster master nodes to work nodes in thanos cluster on 8443/tcp"

  priority  = 1000
  direction = "INGRESS"

  allow {
    protocol = "tcp"
    ports    = [8443]
  }

  # node tags for thanos cluster
  target_tags = ["${var.project_id}-${var.gke_cluster_name}-gke-node"]
  # This is hard-coded in `gke` module
  # master_ipv4_cidr_block of GKE configuration
  source_ranges = ["172.16.0.16/28"]

  # turn on logging
  # log_config {
  #    metadata = "INCLUDE_ALL_METADATA"
  #  }
}