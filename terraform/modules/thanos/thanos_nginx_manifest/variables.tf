variable "project_id" {
  type        = string
  description = "project id"
}

# region and zone used for gke_auth, one must be set
variable "region" {
  type        = string
  default     = ""
  description = "The region to manage resources in. If not set, the zone will be used instead."
}

variable "zone" {
  type        = string
  default     = ""
  description = "The zone referencing the region to manage resources in. If not set, the region will be used instead."
}

variable "gke_cluster_name" {
  type        = string
  description = "Name of the GKE cluster"
}

variable "namespace" {
  type        = string
  default     = "thanos"
  description = "Namespace for thanos components"
}

variable "gke_vpc_name" {
  type        = string
  description = "VPC name of the Cluster where the thanos components will be installed"
}

variable "thanos_receive_remote_write_domain" {
  type        = string
  description = "Domain name that will be used in Ingress resource that receives remote writes"
}

variable "source_ip_ranges_allowed_access" {
  type        = string
  description = "List of IPs that will be allowed access to the ingress resources (thanos-receiver & thanos-store) in comma separated format"
}

variable "ing_address" {
  type        = string
  description = "Ingress IP address"
}