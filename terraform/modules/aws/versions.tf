terraform {
  required_providers {
    kubernetes = {
      source  = "hashicorp/kubernetes"
      version = "2.20.0"
    }

    aws = {
      source  = "hashicorp/aws"
      version = "~> 4.22.0"
    }

  }

  required_version = ">= 1.4.0"
}