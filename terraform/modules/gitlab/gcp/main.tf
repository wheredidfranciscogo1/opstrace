provider "google" {
  project = var.project_id
  region  = var.region
  zone    = var.zone
}

data "google_compute_image" "cos" {
  project = "cos-cloud"
  family  = "cos-97-lts"
}

data "google_client_openid_userinfo" "me" {}

resource "google_compute_address" "gitlab" {
  name = var.instance_name
}

# NOTE(prozlach): The standard disk is not enough for the Gitlab instance -
# executing simple `docker exec` results in `D`-blocked processes. The 8Gi of
# memory could is also unsufficient (i.e. machine-type=n1-standard-4) as there
# are OOM-kills of the `bundle` process visible in the logs.
resource "google_compute_instance" "gitlab" {
  name         = var.instance_name
  machine_type = "n1-standard-4"
  tags         = ["ssh", var.instance_name]

  boot_disk {
    initialize_params {
      image = data.google_compute_image.cos.self_link
      size  = 20
      type  = "pd-ssd"
    }
  }
  lifecycle {
    ignore_changes = [
      boot_disk
    ]
  }

  metadata = {
    ssh-keys = "${split("@", data.google_client_openid_userinfo.me.email)[0]}:${trimspace(tls_private_key.ssh.public_key_openssh)} ${split("@", data.google_client_openid_userinfo.me.email)[0]}"
  }

  network_interface {
    subnetwork = "default"
    access_config {
      nat_ip = google_compute_address.gitlab.address
    }
  }
}

resource "google_dns_record_set" "gitlab" {
  project      = var.project_id
  name         = "${var.domain}."
  managed_zone = var.cloud_dns_zone

  type    = "A"
  ttl     = 300
  rrdatas = [google_compute_instance.gitlab.network_interface[0].access_config[0].nat_ip]
}

resource "google_compute_firewall" "rules" {
  project     = var.project_id
  name        = var.instance_name
  network     = "default"
  description = "Creates firewall rule targeting tagged instances"

  allow {
    protocol = "tcp"
    ports    = ["80", "443", "22"]
  }

  source_ranges = ["0.0.0.0/0"]
  target_tags   = [var.instance_name]
}

resource "tls_private_key" "ssh" {
  algorithm = "ED25519"
}

resource "local_file" "ssh_private_key_pem" {
  content         = tls_private_key.ssh.private_key_openssh
  filename        = ".ssh/${var.instance_name}"
  file_permission = "0600"
}
