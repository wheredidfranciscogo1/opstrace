//go:generate go run github.com/swaggo/swag/cmd/swag init --pd
// swaggo parameters:
// @Title Gatekeeper
// @Description IAM and ratelimiting for GOB

package main

import (
	"context"
	"crypto/tls"
	"encoding/gob"
	"flag"
	"fmt"
	"net/http"
	"net/http/pprof"
	"net/url"
	"os"
	"os/signal"
	"path"
	"strconv"
	"syscall"
	"time"

	"github.com/gin-gonic/gin"
	"github.com/go-redis/redis_rate/v10"
	"github.com/prometheus/client_golang/prometheus/promhttp"
	redisprometheus "github.com/redis/go-redis/extra/redisprometheus/v9"
	redis "github.com/redis/go-redis/v9"
	log "github.com/sirupsen/logrus"
	metricsserver "sigs.k8s.io/controller-runtime/pkg/metrics/server"

	"go.uber.org/zap/zapcore"
	"golang.org/x/oauth2"

	"sigs.k8s.io/controller-runtime/pkg/client"
	"sigs.k8s.io/controller-runtime/pkg/log/zap"
	"sigs.k8s.io/controller-runtime/pkg/metrics"
	"sigs.k8s.io/controller-runtime/pkg/webhook"

	"gitlab.com/gitlab-org/opstrace/opstrace/gatekeeper/docs"
	"gitlab.com/gitlab-org/opstrace/opstrace/gatekeeper/gatekeeper"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/common"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/constants"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/fileobserver"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/ratelimiting"
	apis "gitlab.com/gitlab-org/opstrace/opstrace/scheduler/api"

	_ "go.uber.org/automaxprocs"

	// Import all Kubernetes client auth plugins (e.g. Azure, GCP, OIDC, etc.)
	// to ensure that exec-entrypoint and run can make use of them.
	_ "k8s.io/client-go/plugin/pkg/client/auth"

	k8sruntime "k8s.io/apimachinery/pkg/runtime"
	utilruntime "k8s.io/apimachinery/pkg/util/runtime"
	clientgoscheme "k8s.io/client-go/kubernetes/scheme"
	ctrl "sigs.k8s.io/controller-runtime"
	"sigs.k8s.io/controller-runtime/pkg/healthz"

	swaggerfiles "github.com/swaggo/files"
	ginswagger "github.com/swaggo/gin-swagger"
	opstracev1alpha1 "gitlab.com/gitlab-org/opstrace/opstrace/scheduler/api/v1alpha1"
)

var (
	scheme      = k8sruntime.NewScheme()
	setupLog    = ctrl.Log.WithName("setup")
	metricsAddr string
	probeAddr   string
	debugAddr   string
	port        = common.GetEnv("PORT", ":3001")
	// redis addr is typically sentinel address (USE_REDIS_SENTINEL).
	redisAddr     = common.GetEnv("REDIS_ADDRESS", "127.0.0.1:6379")
	redisPassword = common.GetEnv("REDIS_PASSWORD", "")
	// redis HA is the default via sentinel client.
	useRedisSentinel            = getEnvBool("USE_REDIS_SENTINEL", true)
	useSecureCookie             = getEnvBool("USE_SECURE_COOKIE", false)
	cookieName                  = common.GetEnv("COOKIE_NAME", constants.SessionCookieName)
	cookieSecret                = common.GetEnv("COOKIE_SECRET", "")
	oauthClientID               = common.GetEnv("GITLAB_OAUTH_CLIENT_ID", "")
	oauthClientSecret           = common.GetEnv("GITLAB_OAUTH_CLIENT_SECRET", "")
	serverDomain                = common.GetEnv("DOMAIN", "http://localhost:3001")
	gitlabAddr                  = common.GetEnv("GITLAB_INSTANCE_URL", "https://gitlab.com")
	gitlabInternalEndpointToken = common.GetEnv("GITLAB_INTERNAL_ENDPOINT_TOKEN", "")
	redisConnectionPoolSize     = getEnvInt("REDIS_CONNECTION_POOL_SIZE", 100)
	rateLimitingConfigFile      = common.GetEnv("RATE_LIMITING_CONFIG_FILE", "")
	debugMode                   = false
)

func assignOpts() {
	flag.StringVar(&metricsAddr, "metrics-bind-address", ":8080", "The address the metric endpoint binds to.")
	flag.StringVar(&probeAddr, "health-probe-bind-address", ":8081", "The address the probe endpoint binds to.")
	flag.StringVar(&debugAddr, "debug-address", ":9080", "The address the pprof endpoint binds to.")
	flag.BoolVar(&debugMode, "debug-mode", false, "enable debug paths in the HTTP router code, including pprof")

	opts := zap.Options{
		Development: true,
		// ISO8601TimeEncoder serializes a time.Time to an ISO8601-formatted
		// string with millisecond precision.
		TimeEncoder: zapcore.ISO8601TimeEncoder,
	}
	opts.BindFlags(flag.CommandLine)
	flag.Parse()

	ctrl.SetLogger(zap.New(zap.UseFlagOptions(&opts)))
}

//nolint:funlen
func main() {
	utilruntime.Must(clientgoscheme.AddToScheme(scheme))
	utilruntime.Must(opstracev1alpha1.AddToScheme(scheme))

	assignOpts()

	if gitlabInternalEndpointToken == "" {
		log.Fatal("GITLAB_INTERNAL_ENDPOINT_TOKEN is a required parameter")
	}

	logLevel, err := log.ParseLevel(os.Getenv("LOG_LEVEL"))
	if err != nil {
		logLevel = log.InfoLevel
	}
	log.SetLevel(logLevel)

	// avoid [Error] gob: type not registered for interface: oauth2.Token
	gob.Register(oauth2.Token{})

	// Setup prometheus metrics:
	registry := metrics.Registry
	metricsHandler := promhttp.HandlerFor(registry, promhttp.HandlerOpts{
		ErrorHandling:     promhttp.HTTPErrorOnError,
		EnableOpenMetrics: true,
	})
	routerMetrics := http.NewServeMux()
	routerMetrics.Handle("/metrics", metricsHandler)

	routerMain := gin.Default()
	routerMain.Use(gatekeeper.ErrorLogger())
	routerMain.Static("/assets", "./assets")
	initDocs(routerMain)
	// For now, disable all proxies for X-Forwarded-For client IP purposes.
	// This affects context.ClientIP(), which we aren't using yet anyway.
	// See also https://pkg.go.dev/github.com/gin-gonic/gin#readme-don-t-trust-all-proxies
	err = routerMain.SetTrustedProxies(nil)
	if err != nil {
		log.WithError(err).Fatal("failed to set trusted proxies")
	}
	routerMain.LoadHTMLGlob("templates/*.gohtml")

	// Parse gitlab address and set up the internal API endpoint
	internalEndpointAddr, err := url.Parse(gitlabAddr)
	if err != nil {
		log.WithError(err).Fatal("invalid gitlab addr")
	}
	internalEndpointAddr.Path = path.Join(internalEndpointAddr.Path, constants.GitlabInternalErrorTrackingEndpoint)

	namespace, err := GetWatchNamespace()
	if err != nil {
		log.Error(err, "failed to get namespace for watching/creating GitLabNamespace CRs")
		os.Exit(1)
	}

	redisClient := getRedis(registry)

	// This client is a split client that reads objects from the cache and writes to the apiserver.
	// Keeping a cache that is automatically updated objects we care about (GitLabNamespace) reduces
	// load on the apiserver and provides realtime event updates
	k8sClient := StartControllerManager(namespace)

	routerMain.Use(gatekeeper.Config(&gatekeeper.ConfigOptions{
		Port:                        port,
		UseSecureCookie:             useSecureCookie,
		CookieName:                  cookieName,
		CookieSecret:                cookieSecret,
		OauthClientID:               oauthClientID,
		OauthClientSecret:           oauthClientSecret,
		ServerDomain:                serverDomain,
		GitlabAddr:                  gitlabAddr,
		GitlabInternalEndpointAddr:  internalEndpointAddr.String(),
		GitlabInternalEndpointToken: gitlabInternalEndpointToken,
		K8sClient:                   k8sClient,
		Namespace:                   namespace,
	}))

	routerMain.Use(common.RouteMetrics(registry))

	// Setup rate-limiter
	rateLimiter, doneFunc := getRateLimiter(redisClient)
	defer doneFunc()
	routerMain.Use(gatekeeper.SetRateLimiter(rateLimiter))

	// Setup our session middleware to use Redis for session persistence
	routerMain.Use(gatekeeper.Session(redisClient, &gatekeeper.SessionOptions{
		CookieName:      cookieName,
		CookieSecret:    cookieSecret,
		UseSecureCookie: useSecureCookie,
		Registry:        registry,
	})...)
	// Configure auth-metrics and make it available to the auth controllers
	routerMain.Use(gatekeeper.AuthMetrics(registry))
	// Configure OAuth2 and make it available to the auth controllers
	routerMain.Use(gatekeeper.OAuth2(&gatekeeper.AuthOptions{
		ClientID:     oauthClientID,
		ClientSecret: oauthClientSecret,
		ServerDomain: serverDomain,
		GitlabAddr:   gitlabAddr,
	}))
	// Configure cache client and make them available
	// to downstream handlers
	routerMain.Use(gatekeeper.Cache(redisClient, registry))

	gatekeeper.SetRoutes(routerMain, debugMode)

	// If the request got here, it means that the Gin server along with the
	// middlewares are up. We use this information to let k8s know that we are
	// ready to serve traffic.
	routerMain.GET("/readyz", func(ctx *gin.Context) {
		sts := redisClient.Ping(ctx)
		if err := sts.Err(); err != nil {
			log.WithError(err).Error("readyz redis ping")
			ctx.String(500, "Redis not available")
			return
		}
		ctx.String(200, "Success")
	})

	log.Infof("log level: %s", log.GetLevel())
	log.Infof("starting HTTP server on %s", port)

	srvs := make([]*http.Server, 2)

	srvs[0] = &http.Server{
		Addr:    port,
		Handler: routerMain,
		// NOTE(prozlach): Arbitrary amount of time, may need tuning.
		ReadHeaderTimeout: 10 * time.Second,
	}
	srvs[1] = &http.Server{
		Addr:    metricsAddr,
		Handler: routerMetrics,
		// NOTE(prozlach): Arbitrary amount of time, may need tuning.
		ReadHeaderTimeout: 10 * time.Second,
	}
	if debugMode {
		// NOTE(prozlach): Gatekeeper does not use the default Mux and let's
		// keep it this way as it nicelly segregates metrics, debugging, main
		// router, etc.. into their own http servers. Additionally, importing
		// pprof implicitly adds pprof routes to the default Mux which could be
		// a security risk in case we start using it and it got exposed to the
		// outside.
		r := http.NewServeMux()
		r.HandleFunc("/debug/pprof/", pprof.Index)
		r.HandleFunc("/debug/pprof/cmdline", pprof.Cmdline)
		r.HandleFunc("/debug/pprof/profile", pprof.Profile)
		r.HandleFunc("/debug/pprof/symbol", pprof.Symbol)
		r.HandleFunc("/debug/pprof/trace", pprof.Trace)
		r.Handle("/debug/pprof/allocs", pprof.Handler("allocs"))
		r.Handle("/debug/pprof/block", pprof.Handler("block"))
		r.Handle("/debug/pprof/goroutine", pprof.Handler("goroutine"))
		r.Handle("/debug/pprof/heap", pprof.Handler("heap"))
		r.Handle("/debug/pprof/mutex", pprof.Handler("mutex"))
		r.Handle("/debug/pprof/threadcreate", pprof.Handler("threadcreate"))

		//nolint:gosec,makezero
		srvs = append(srvs, &http.Server{
			Addr:    debugAddr,
			Handler: r,
		})
		log.Infof("starting debug server on address %s", debugAddr)
	}

	// TODO(prozlach) Wire up all the contexts in Gatekeeper to use the one
	// below so that we get consistent, ordered cleanup after receiving
	// interrupt/sigterm signals.
	ctx, _ := signal.NotifyContext(context.Background(), os.Interrupt, syscall.SIGTERM)

	err = common.ServeWithGracefulShutdown(ctx, log.StandardLogger(), srvs...)

	// TODO(prozlach) We shouldn't be exiting main directly via os.Exit, but
	// instead have a wrapper function so that any deffered calls have a chance
	// to run. Once such call could be for example Zap's logs sync function.
	if err != nil {
		log.Errorf("error occurred while shutting down http servers: %s", err)
	}
}

func initDocs(r *gin.Engine) {
	docs.SwaggerInfo.Version = constants.DockerImageTag
	r.GET("/swagger/*any", ginswagger.WrapHandler(swaggerfiles.Handler))
}

func StartControllerManager(namespace string) client.Client {
	mgr, err := ctrl.NewManager(ctrl.GetConfigOrDie(), ctrl.Options{
		Scheme: scheme,
		Metrics: metricsserver.Options{
			BindAddress: "0",
		},
		WebhookServer: webhook.NewServer(webhook.Options{
			Port: 9443,
		}),
		HealthProbeBindAddress: probeAddr,
		// we already have a metrics server
		LeaderElection:   false,
		LeaderElectionID: "",
	})
	if err != nil {
		setupLog.Error(err, "unable to create manager")
		os.Exit(1)
	}

	log.Info("Registering Scheme")

	if err := apis.AddToScheme(mgr.GetScheme()); err != nil {
		log.Error(err, "")
		os.Exit(1)
	}

	log.Info("Starting the controller manager")

	if err = (&gatekeeper.NamespaceWatcher{
		Client:    mgr.GetClient(),
		Scheme:    mgr.GetScheme(),
		Log:       ctrl.Log.WithName("NamespaceWatcher"),
		Namespace: namespace,
		/* #nosec G402 */
		Transport: &http.Transport{
			TLSClientConfig: &tls.Config{
				InsecureSkipVerify: true,
			},
		},
	}).SetupWithManager(mgr); err != nil {
		setupLog.Error(err, "unable to create controller", "controller", "Cluster")
		os.Exit(1)
	}

	// /healthz only checks if pod is up and running, and not if e.g. Redis
	// Sentinel is operational. For that we use /readyz endpoint in Gin server
	// itself.
	if err := mgr.AddHealthzCheck("healthz", healthz.Ping); err != nil {
		setupLog.Error(err, "unable to set up health check")
		os.Exit(1)
	}

	setupLog.Info("starting manager")
	go func() {
		if err := mgr.Start(ctrl.SetupSignalHandler()); err != nil {
			setupLog.Error(err, "problem running manager")
			os.Exit(1)
		}
	}()

	return mgr.GetClient()
}

func getEnvBool(key string, fallback bool) bool {
	v := os.Getenv(key)
	if len(v) == 0 {
		return fallback
	}
	bv, err := strconv.ParseBool(v)
	if err != nil {
		log.Fatalf("failed to convert env var %s into bool: %v", key, err)
	}
	return bv
}

func getEnvInt(key string, fallback int) int {
	v := os.Getenv(key)
	if len(v) == 0 {
		return fallback
	}
	iv, err := strconv.Atoi(v)
	if err != nil {
		log.Fatalf("failed to convert env var %s into bool: %v", key, err)
	}
	return iv
}

func getRedis(registry metrics.RegistererGatherer) *redis.Client {
	var redisClient *redis.Client
	if useRedisSentinel {
		redisClient = redis.NewFailoverClient(&redis.FailoverOptions{
			MasterName:    "mymaster",
			SentinelAddrs: []string{redisAddr},
			Password:      redisPassword,
			DB:            0, // use default DB
			PoolSize:      redisConnectionPoolSize,
		})
	} else {
		redisClient = redis.NewClient(&redis.Options{
			Addr:     redisAddr,
			Password: redisPassword,
			DB:       0,
			PoolSize: redisConnectionPoolSize,
		})
	}
	registry.MustRegister(redisprometheus.NewCollector("redis", "", redisClient))

	return redisClient
}

func getRateLimiter(redisClient *redis.Client) (gatekeeper.RateLimiter, func()) {
	var rateLimiter gatekeeper.RateLimiter

	if rateLimitingConfigFile != "" {
		redisRateLimiter := redis_rate.NewLimiter(redisClient)
		rateLimiter = gatekeeper.NewRateLimiter(redisRateLimiter)

		actionFn := func(_ chan struct{}) error {
			newLimits, err := ratelimiting.ReadConfig(rateLimitingConfigFile)
			if err != nil {
				return fmt.Errorf(
					"unable to load rate-limiting config %s: %w", rateLimitingConfigFile, err)
			}
			rateLimiter.SetLimits(newLimits)
			log.Info("New limits configuration has been set")
			return nil
		}

		fo := fileobserver.NewFileObserver(rateLimitingConfigFile, actionFn)
		err := fo.Run()
		if err != nil {
			log.WithError(err).Fatal("failed to set up rate-limiting config reloading")
		}
		return rateLimiter, func() { fo.Stop() }
	} else {
		log.Warn("rate-limiting config file path is not set, rate-limiting is disabled")
		return gatekeeper.NewNullRateLimiter(), func() {}
	}
}

// GetWatchNamespace returns the Namespace the operator should be watching for changes.
func GetWatchNamespace() (string, error) {
	// WatchNamespaceEnvVar is the constant for env variable NAMESPACE
	// which specifies the Namespace to watch and create resource in.
	// An empty value means the operator is running with cluster scope.
	var watchNamespaceEnvVar = "NAMESPACE"

	ns, found := os.LookupEnv(watchNamespaceEnvVar)
	if !found {
		return "", fmt.Errorf("%s must be set", watchNamespaceEnvVar)
	}
	return ns, nil
}
