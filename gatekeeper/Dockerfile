FROM --platform=${BUILDPLATFORM} golang:1.21 as builder

ENV CGO_ENABLED=0

WORKDIR /workspace/go
# Copy common go packages
COPY go/go.mod ./
COPY go/go.sum ./
# cache deps before building and copying source so that we don't need to re-download as much
# and so that source changes don't invalidate our downloaded layer
RUN --mount=type=cache,target=/go/pkg go mod download

COPY go/pkg/ pkg/

WORKDIR /workspace/tenant-operator
# Copy tenant operator
COPY tenant-operator/go.mod ./
COPY tenant-operator/go.sum ./
# cache deps before building and copying source so that we don't need to re-download as much
# and so that source changes don't invalidate our downloaded layer
RUN --mount=type=cache,target=/go/pkg go mod download

COPY tenant-operator/api/ api/
COPY tenant-operator/controllers/ controllers/
COPY tenant-operator/version/ version/

WORKDIR /workspace/clickhouse-operator
# Copy clickhouse operator
COPY clickhouse-operator/go.mod ./
COPY clickhouse-operator/go.sum ./
# cache deps before building and copying source so that we don't need to re-download as much
# and so that source changes don't invalidate our downloaded layer
RUN --mount=type=cache,target=/go/pkg go mod download

COPY clickhouse-operator/api/ api/
COPY clickhouse-operator/controllers/ controllers/

WORKDIR /workspace/scheduler
# Copy the Go Modules manifests
COPY scheduler/go.mod ./
COPY scheduler/go.sum ./
# cache deps before building and copying source so that we don't need to re-download as much
# and so that source changes don't invalidate our downloaded layer
RUN --mount=type=cache,target=/go/pkg go mod download

# Copy the go source
COPY scheduler/api/ api/
COPY scheduler/controllers/ controllers/
COPY scheduler/version/ version/

WORKDIR /workspace/gatekeeper
# Copy common go packages
COPY gatekeeper/go.mod ./
COPY gatekeeper/go.sum ./
# cache deps before building and copying source so that we don't need to re-download as much
# and so that source changes don't invalidate our downloaded layer
RUN --mount=type=cache,target=/go/pkg go mod download

COPY gatekeeper/gatekeeper/ gatekeeper/
COPY gatekeeper/templates/ templates/
COPY gatekeeper/assets/ assets/
COPY gatekeeper/docs docs/
COPY gatekeeper/main.go ./main.go

ARG GO_BUILD_LDFLAGS
ARG TARGETARCH
ARG TARGETOS
# Build. Using `bash -c` format to avoid quoting issues.
RUN --mount=type=cache,target=/root/.cache/go-build \
    --mount=type=cache,target=/go/pkg \
    ["/bin/bash", "-c", "GOOS=${TARGETOS} GOARCH=${TARGETARCH} go build -ldflags \"$GO_BUILD_LDFLAGS\" -a -o server main.go"]

FROM gcr.io/distroless/base-debian11:nonroot as release
COPY --from=builder /workspace/gatekeeper/server /
COPY --from=builder /workspace/gatekeeper/templates/ /templates/
COPY --from=builder /workspace/gatekeeper/assets/ /assets/

ENV GIN_MODE=release

WORKDIR /
USER 65532:65532

ENTRYPOINT ["/server"]
