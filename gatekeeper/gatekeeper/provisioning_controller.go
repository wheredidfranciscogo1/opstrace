package gatekeeper

import (
	"context"
	stderrors "errors"
	"fmt"
	"net/http"
	"reflect"
	"strings"

	"github.com/gin-gonic/gin"
	log "github.com/sirupsen/logrus"
	"github.com/xanzy/go-gitlab"
	"k8s.io/apimachinery/pkg/api/errors"
	v1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"sigs.k8s.io/controller-runtime/pkg/client"

	opstracev1alpha1 "gitlab.com/gitlab-org/opstrace/opstrace/scheduler/api/v1alpha1"
)

type NamespaceParams struct {
	ID     string `uri:"namespace_id" binding:"required"`
	Action string `uri:"action"`
}

// Handles incoming requests for a Gitlab namespace for namespaces that do not
// yet exist in Opstrace. If the request lands here, we need to provision the
// namespace in Opstrace. This is a lazy provisioner that only provisions
// namespaces when the first request comes in for that namespace. Request must
// have Owner permissions on the respective GitLab namespace to proceed.
//
// See the namespace API docs in gitlab
// https://docs.gitlab.com/ee/api/namespaces.html
//
// @Id NamespaceProvisioning
// @Summary Handles provisioning of new namespaces.
// @Router /v1/provision/{namespace_id}/{action} [get]
// @Param namespace_id path string true "Namespace ID"
// @Param action path string false "Action URL part"
// @Success 302 "Redirect to provisioned UI"
// @Failure 401 "Unauthorized"
// @Failure 404 "Namespace not found"
// @Deprecated
//
//nolint:funlen,gocyclo,cyclop
func HandleNamespaceProvisioning(ctx *gin.Context) {
	var params NamespaceParams
	if err := ctx.ShouldBindUri(&params); err != nil {
		negotiateError(ctx, http.StatusBadRequest, "invalid namespace_id")
		return
	}
	g := GetGitLabService(ctx)
	user, err := g.CurrentUser()
	if err != nil {
		HandleError(ctx, err)
		return
	}

	var topLevelNamespace *GitLabNamespace
	var namespace *GitLabNamespace
	canAccess, accessLevel, err := g.CanAccessNamespace(params.ID)
	// Only abort if it's due to an authorization issue
	if stderrors.Is(err, ErrUnauthorized) {
		HandleError(ctx, err)
		return
	}

	if canAccess && accessLevel != gitlab.OwnerPermissions {
		// Show message that owner must enable observability for this namespace
		ctx.HTML(200, "error.gohtml", gin.H{
			"title":   "Please contact an Owner of this namespace",
			"message": "Must be an Owner to enable observability for this namespace",
		})
		return
	}

	//nolint:nestif
	if canAccess && accessLevel == gitlab.OwnerPermissions {
		// Has all the permissions to enable observability for this namespace
		namespace, err = g.GetNamespace(params.ID)
		if err != nil {
			HandleError(ctx, err)
			return
		}
		// Get top level namespace because that maps to an Opstrace namespace
		if namespace.IsTopLevel() {
			topLevelNamespace = namespace
		} else {
			topLevelID := namespace.GetTopLevelNamespaceID()
			topLevelNamespace, err = g.GetNamespace(topLevelID)
		}
		if err != nil {
			HandleError(ctx, err)
			return
		}
	} else {
		// User does not have any access to this namespace
		ctx.AbortWithStatus(404)
		log.WithFields(log.Fields{
			"user":      user.ID,
			"namespace": params.ID,
		}).Info("attempting to access namespace without membership")

		return
	}

	c := GetConfig(ctx)

	avatar := ""

	if namespace.AvatarURL != nil {
		// Prepend with gitlab instance host
		avatar = strings.Join([]string{c.GitlabAddr, *namespace.AvatarURL}, "")
	}

	selector := client.ObjectKey{
		Name:      fmt.Sprint(namespace.ID),
		Namespace: c.Namespace,
	}
	obj := opstracev1alpha1.GitLabNamespace{}

	err = c.K8sClient.Get(context.TODO(), selector, &obj)
	if err != nil && !errors.IsNotFound(err) {
		HandleError(ctx, fmt.Errorf("failed to read namespace %s from kubernetes client cache: %w", namespace.FullPath, err))
		return
	}

	current := &opstracev1alpha1.GitLabNamespace{
		ObjectMeta: v1.ObjectMeta{
			Name:      fmt.Sprint(namespace.ID),
			Namespace: c.Namespace,
		},
	}

	//nolint:nestif
	if errors.IsNotFound(err) {
		// Create it
		current.Spec.ID = int64(namespace.ID)
		current.Spec.TopLevelNamespaceID = int64(topLevelNamespace.ID)
		current.Spec.Name = namespace.Name
		current.Spec.Path = namespace.Path
		current.Spec.FullPath = namespace.FullPath
		current.Spec.AvatarURL = avatar
		current.Spec.WebURL = namespace.WebURL

		err := c.K8sClient.Create(context.TODO(), current)
		// Don't care if there is a conflict because it means another process succeeded with the update
		if err != nil && !errors.IsConflict(err) {
			HandleError(ctx, fmt.Errorf("create failed for namespace %s: %w", namespace.FullPath, err))
			return
		}
		log.Info(fmt.Sprintf(
			"namespace %s [ID:%d] created",
			namespace.FullPath,
			namespace.ID,
		))
	} else {
		// Examine DeletionTimestamp to determine if object is under deletion
		if !obj.ObjectMeta.DeletionTimestamp.IsZero() {
			HandleError(ctx, fmt.Errorf("namespace %s is deleting", namespace.FullPath))
			return
		}
		// Create a copy, mutate it and then check if anything changed before updating.
		current = obj.DeepCopy()
		current.Spec.ID = int64(namespace.ID)
		current.Spec.TopLevelNamespaceID = int64(topLevelNamespace.ID)
		current.Spec.Name = namespace.Name
		current.Spec.Path = namespace.Path
		current.Spec.FullPath = namespace.FullPath
		current.Spec.AvatarURL = avatar
		current.Spec.WebURL = namespace.WebURL

		if !reflect.DeepEqual(obj, current) {
			err := c.K8sClient.Update(context.TODO(), current)
			// Don't care if there is a conflict because it means another process succeeded with the update
			if err != nil && !errors.IsConflict(err) {
				HandleError(ctx, fmt.Errorf("update failed for namespace %s: %w", namespace.FullPath, err))
				return
			}
			log.Info(fmt.Sprintf(
				"namespace %s [ID:%d] updated",
				namespace.FullPath,
				namespace.ID,
			))
		}
	}

	// examine DeletionTimestamp to determine if object is under deletion
	if !current.ObjectMeta.DeletionTimestamp.IsZero() {
		ctx.HTML(200, "deleting.gohtml", gin.H{
			"namespaceFullPath": namespace.FullPath,
		})
		return
	}

	// NOTE(prozlach): This code in theory should at least wait for the
	// GitlabNamespace object to get ready, but in practice, the current
	// tenant-based system is going away and it is unlikelly that we will be
	// provisioning new tenants using old system before this code is removed
	// altogether.

	// Good to go
	queryParams := ctx.Request.URL.Query()
	// NOTE(prozlach): This is only a stop gap measure meant to cordon GOUI off
	// before removing it. We will not be doing provisioning via a button in
	// the future and jager UI will go away eventually too.
	ctx.Redirect(302, fmt.Sprintf("/v1/jaeger/%d%s?%s", topLevelNamespace.ID, params.Action, queryParams.Encode()))
}
