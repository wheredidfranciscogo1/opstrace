package gatekeeper

import (
	"errors"
	"fmt"
	"net/http"
	"strconv"

	"github.com/gin-gonic/gin"
	log "github.com/sirupsen/logrus"
	gitlab "github.com/xanzy/go-gitlab"
)

// NOTE: It's really confusing until we rename all the things but an orgId is
// synonymous with a groupID and a groupID in GitLab is synonymous
// with a namespaceID.

type EmptyValue struct{}

// Handles the token-based external auth request from ingress, both
// oauth-based session as well as Gitlab Access Token.
func HandleTokenAuth(ctx *gin.Context) {
	g := GetGitLabService(ctx)
	namespaceID := GetNamespace(ctx)

	if canAccess, _, err := g.CanAccessNamespace(namespaceID); canAccess {
		if err != nil {
			ctx.AbortWithError(http.StatusForbidden, fmt.Errorf("access namespace: %w", err))
			return
		}
		log.Debug("HandleTokenAuth decision OK")
		return
	}

	log.Debug("HandleTokenAuth decision deny")
	ctx.AbortWithStatus(http.StatusForbidden)
}

func HandleGroupAccessAuth(ginCtx *gin.Context, namespaceID, minAccessLevel int) {
	g := GetGitLabService(ginCtx)

	if canAccess, accessLevel, err := g.CanAccessNamespace(strconv.Itoa(namespaceID)); canAccess {
		if err != nil {
			ginCtx.AbortWithError(http.StatusForbidden, fmt.Errorf("access namespace: %w", err))
			return
		}
		if accessLevel < gitlab.AccessLevelValue(minAccessLevel) {
			log.Debug("HandleGroupAccessAuth decision deny - insufficient perm")
			ginCtx.AbortWithStatus(http.StatusForbidden)
			return
		}
		log.Debug("HandleGroupAccessAuth decision OK")
		return
	}

	log.Debug("HandleGroupAccessAuth decision deny")
	ginCtx.AbortWithStatus(http.StatusForbidden)
}

func GetTopLevelNamespaceFromGroup(ginCtx *gin.Context, groupID int) int {
	g := GetGitLabService(ginCtx)

	group, err := g.GetGroup(strconv.Itoa(groupID))
	if err != nil {
		ginCtx.AbortWithError(
			http.StatusForbidden,
			fmt.Errorf("GH api call to get group details has failed: %w", err),
		)
		return -1
	}
	// Get top level namespace because that maps to an Opstrace namespace
	if group.IsTopLevel() {
		return group.ID
	}

	topLevelID := group.GetTopLevelNamespaceID()
	// TODO(prozlach): Gitlab API has a quirk that does not allow a gitlab
	// access token to query top-level's group information directly, only
	// indirectly.
	//
	// The workaround is to simply fetch all the groups the given
	// token belongs to and see if any of them has the same ID as the top-level
	// one we determined above. If so - return the ID of this group, if not -
	// it means we do not have enough access to query this information and
	// should return 403 anyway.
	//
	// The workaround is not optimal because in case of an admin user, the list
	// of all groups is returned, which in case of a big instance can be a lot.
	// Hence this approach should be treated as temporary.
	//
	// group, err = g.GetGroup(topLevelID)
	// if err != nil {
	// 	ginCtx.AbortWithError(
	// 		http.StatusForbidden,
	// 		fmt.Errorf("GH api call to get group details has failed: %w", err),
	// 	)
	// 	return -1
	// }
	//
	// return group.ID

	groups, err := g.GetTopLevelGroups()
	if err != nil {
		ginCtx.AbortWithError(
			http.StatusForbidden,
			fmt.Errorf("GH api call to list groups has failed: %w", err),
		)
		return -1
	}
	for _, group := range groups {
		if group.FullPath == topLevelID {
			return group.ID
		}
	}

	ginCtx.AbortWithError(
		http.StatusForbidden,
		errors.New("unable to determine the top-level group"),
	)
	return -1
}

func GetTopLevelNamespaceFromProject(ginCtx *gin.Context, projectID int) int {
	g := GetGitLabService(ginCtx)

	pgs, err := g.ListProjectsGroups(projectID)
	if err != nil {
		ginCtx.AbortWithError(
			http.StatusForbidden,
			fmt.Errorf("GH api call to list project groups has failed: %w", err),
		)
		return -1
	}

	if len(pgs) == 0 {
		log.Debugf("Project %d does not belong to any group", projectID)
		ginCtx.AbortWithStatus(http.StatusForbidden)
		return -1
	}
	return pgs[0].ID
}

func VerifyProjectGroupMembership(
	ginCtx *gin.Context,
	namespaceID, projectID int,
) {
	g := GetGitLabService(ginCtx)

	// NOTE(prozlach): We need to try to determine if the project is in the
	// group or a user-ns. In case it is a user-ns project, the response to
	// Gitlab API call to list project's ancestor groups will be empty.
	isGroupMember, err := g.IsProjectInGroup(projectID, namespaceID)
	if err != nil {
		ginCtx.AbortWithError(
			http.StatusForbidden,
			fmt.Errorf("error occurred, unable to determine if project belongs to the group: %w", err),
		)
		return
	}
	isUserNSMember, err := g.IsProjectInUserNamespace(projectID, namespaceID)
	if err != nil {
		ginCtx.AbortWithError(
			http.StatusForbidden,
			fmt.Errorf("error occurred, unable to determine if project belongs to a user ns: %w", err),
		)
		return
	}

	if !isGroupMember && !isUserNSMember {
		log.Debugf(
			"project %d does not belong to neirther a group nor namespace %d",
			projectID, namespaceID,
		)
		ginCtx.AbortWithStatus(http.StatusForbidden)
		return
	}
}

// Handles the token-based external auth request from ingress, both
// oauth-based session as well as Gitlab Access Token.
func HandleProjectAccessAuth(
	ginCtx *gin.Context,
	projectID, minAccessLevelKey int,
) {
	g := GetGitLabService(ginCtx)

	canAccess, err := g.CanAccessProject(projectID, minAccessLevelKey)
	if err != nil {
		ginCtx.AbortWithError(
			http.StatusForbidden,
			fmt.Errorf("error occurred, unable to determine project access: %w", err),
		)
		return
	}
	if !canAccess {
		log.Debug("HandleProjectAccessAuth decision deny")
		ginCtx.AbortWithStatus(http.StatusForbidden)
		return
	}

	log.Debug("HandleProjectAccessAuth decision OK")
}
