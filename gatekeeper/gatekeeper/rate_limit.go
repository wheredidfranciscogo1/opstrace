//go:generate go run github.com/vektra/mockery/v2  --name RateLimiter --dir . --structname RateLimiterMock --output ./test/ --outpkg test --filename rate_limiter.go
package gatekeeper

import (
	"context"
	"errors"
	"fmt"
	"net/http"
	"strconv"
	"sync"
	"time"

	"github.com/gin-gonic/gin"
	"github.com/go-redis/redis_rate/v10"
	"github.com/prometheus/client_golang/prometheus"
	log "github.com/sirupsen/logrus"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/ratelimiting"
)

const (
	IDTypeProject = "project"
	IDTypeGroup   = "organization"

	EndpointTypeETWrite = "et_write"
	EndpointTypeETRead  = "et_read"
	EndpointTypeTRWrite = "tr_write"

	rateLimiterKey     = "rateLimiter"
	topLevelGroupIDKey = "topLevelGroupID"
)

type LimitIDRateLimitHandlerFormParams struct {
	LimitID *string `json:"limit_id" form:"limit_id" binding:"omitempty,oneof=tr_write et_read et_write"`
}

func NewRateLimiter(limiter RateLimiterBackend) RateLimiter {
	res := new(rateLimiterT)

	res.ratelimitCounter = prometheus.NewCounterVec(
		prometheus.CounterOpts{
			Name: "http_ratelimit_total",
			Help: "number of requests by status code, method and path",
		},
		[]string{"id", "endpointName", "allowed"},
	)
	prometheus.MustRegister(res.ratelimitCounter)

	res.limiter = limiter

	return res
}

type RateLimiter interface {
	IsAllowed(context.Context, int, string, uint64) (*redis_rate.Result, error)
	SetLimits(*ratelimiting.LimitsConfig)
}

type RateLimiterBackend interface {
	AllowN(context.Context, string, redis_rate.Limit, int) (*redis_rate.Result, error)
}

func NewNullRateLimiter() RateLimiter {
	return new(nullRateLimiterT)
}

// nullRateLimiterT is a limiter that does no limiting at all. It is used in
// tests and in cases when rate-limiting should be disabled.
type nullRateLimiterT struct{}

func (rl *nullRateLimiterT) SetLimits(_ *ratelimiting.LimitsConfig) {
}

func (rl *nullRateLimiterT) IsAllowed(_ context.Context, _ int, _ string, _ uint64) (*redis_rate.Result, error) {
	return nil, nil
}

type rateLimiterT struct {
	ratelimitCounter *prometheus.CounterVec
	limiter          RateLimiterBackend
	limits           *ratelimiting.LimitsConfig
	limitsMutex      sync.RWMutex
}

func (rl *rateLimiterT) SetLimits(newLimits *ratelimiting.LimitsConfig) {
	rl.limitsMutex.Lock()
	defer rl.limitsMutex.Unlock()

	rl.limits = newLimits
}

func (rl *rateLimiterT) getLimit(topLevelGroupID int, limitID string) (*redis_rate.Limit, error) {
	// NOTE(prozlach): We should be using PerMInute limits here instead of
	// per-hour, as redis-rate does not adjust the data stored in Redis after
	// updating the limits, so even though we e.g. bumped limits for the given
	// sizing, we would get inconsistent limits for the remaining part of the
	// hour.
	// The solution is to use PerMinute instead, so the limits once changed,
	// will get relativelly quickly refreshed.
	rl.limitsMutex.RLock()
	defer rl.limitsMutex.RUnlock()

	if rl.limits == nil {
		return nil, errors.New("rate limits configuration is not set")
	}

	// Check for overrides first:
	var sizing ratelimiting.Limits
	binding, ok := rl.limits.Bindings.Groups[topLevelGroupID]
	if ok {
		sizing = rl.limits.Sizings[binding]
	} else {
		// No group/project-specific overrides, return defaults:
		sizing = rl.limits.Sizings["default"]
	}

	var res redis_rate.Limit
	switch limitID {
	case EndpointTypeETWrite:
		res = redis_rate.PerMinute(int(sizing.ETAPIWritesLimit))
	case EndpointTypeETRead:
		res = redis_rate.PerMinute(int(sizing.ETAPIReadsLimit))
	case EndpointTypeTRWrite:
		res = redis_rate.PerMinute(int(sizing.TRBytesWriteLimit))
	default:
		// This is a programming error, and should be treated as such:
		panic(fmt.Sprintf("unrecognized limitID %q", limitID))
	}

	return &res, nil
}

// IsAllowed determines if the given request is allowed
func (rl *rateLimiterT) IsAllowed(
	rootCtx context.Context, topLevelGroupID int, limitID string, n uint64,
) (*redis_rate.Result, error) {
	// NOTE(prozlach): 5s value is chosen arbitrary, wy need tunning depending
	// on how redis behaves
	ctx, cancelF := context.WithTimeout(rootCtx, 5*time.Second)
	defer cancelF()

	limit, err := rl.getLimit(topLevelGroupID, limitID)
	if err != nil {
		return nil, fmt.Errorf("unable to determine if rate limits: %w", err)
	}
	limitRedisKey := fmt.Sprintf("%d:%s", topLevelGroupID, limitID)
	// NOTE(prozlach): current implementation (redis_rate) accepts only int.
	result, err := rl.limiter.AllowN(ctx, limitRedisKey, *limit, int(n))
	if err != nil {
		return nil, fmt.Errorf("unable to determine if rate limit quota is exceeded: %w", err)
	}

	rl.ratelimitCounter.WithLabelValues(
		strconv.FormatUint(uint64(topLevelGroupID), 10),
		limitID,
		fmt.Sprintf("%v", result.Allowed > 0),
	)

	return result, nil
}

// Middleware to set the ratelimiter in the Gin's context for downstream
// handlers..
func SetRateLimiter(rl RateLimiter) gin.HandlerFunc {
	return func(ctx *gin.Context) {
		ctx.Set(rateLimiterKey, rl)
	}
}

// Helper to get the ratelimiter from the context.
func GetRateLimiter(ctx *gin.Context) RateLimiter {
	return ctx.MustGet(rateLimiterKey).(RateLimiter)
}

func GetTopLevelGroupID(ctx *gin.Context) int {
	return ctx.MustGet(topLevelGroupIDKey).(int)
}

// handleRateLimiting checks if the request that belongs to given `limitID`
// group is permitted/below the quota limits for given topLevelGroupID. The
// return value signifies whether the request was rate limited or not, in case
// e.g. extra headers need to be added.
func RateLimitingHandler() gin.HandlerFunc {
	return func(ginCtx *gin.Context) {
		rl := GetRateLimiter(ginCtx)
		topLevelGroupID := GetTopLevelGroupID(ginCtx)

		var formParams LimitIDRateLimitHandlerFormParams
		if err := ginCtx.ShouldBindQuery(&formParams); err != nil {
			ginCtx.AbortWithError(403, fmt.Errorf("uri parameters validation failed: %w", err))
			return
		}
		if formParams.LimitID == nil {
			// limit_id param was not specified, this means that the endpoint
			// is not rate-limited.
			return
		}
		limitID := *formParams.LimitID

		n := uint64(0)

		switch limitID {
		case EndpointTypeTRWrite:
			// For tracing we are calculating the number of bytes ingested
			tmpN := ginCtx.GetHeader("X-Content-Length")
			origMethod := ginCtx.GetHeader("X-Forwarded-Method")
			nTmp, err := strconv.Atoi(tmpN)

			if origMethod != http.MethodPost || tmpN == "" || err != nil {
				ginCtx.AbortWithError(
					http.StatusForbidden, fmt.Errorf(
						"unsupported HTTP request method (%s!=%s) or content-length header missing",
						origMethod, http.MethodPost,
					),
				)
				return
			}
			n = uint64(nTmp)
		default:
			// NOTE(prozlach): This is a programming error, as the limitID
			// is an enum in request params. If we arrived here, then it
			// means that somebody did not adjust all code-paths when
			// adding new limitID.
			panic(fmt.Sprintf("unrecognized limitID %s", limitID))
		}

		limitsData, err := rl.IsAllowed(ginCtx, topLevelGroupID, limitID, n)
		if err != nil {
			ginCtx.AbortWithError(
				http.StatusForbidden,
				fmt.Errorf("rate-limiting failed: %w", err),
			)
			return
		}
		if limitsData == nil {
			// There are no limits set, treat it as if rate-limiting was disabled,
			// and do not set any HTTP headers.
			return
		}
		if limitsData.Allowed == 0 {
			log.
				WithField("retryAfter", limitsData.ResetAfter.Seconds()).
				WithField("limitLimit", limitsData.Limit.Rate).
				WithField("limitReset", time.Now().Add(limitsData.ResetAfter).Unix()).
				WithField("limitID", limitID).
				WithField("topLevelGroupID", topLevelGroupID).
				Info("request rejected, requests limits exceeded")

			// Depending on the type of the limits, we add different
			// headers/http status codes as different protocols/components
			// follow different standards of communicating to client that
			// limits are exceeded.
			switch limitID {
			case EndpointTypeTRWrite:
				// Follow the spec from:
				//
				// https://opentelemetry.io/docs/specs/otlp/#failures-1
				// https://opentelemetry.io/docs/specs/otlp/#otlphttp-throttling
				//
				// TODO(prozlach): The source code of the otelhttp exporter
				// does not make use of protobuf message though, hence skipping
				// it here.
				//nolint:lll
				// https://github.com/open-telemetry/opentelemetry-collector/blob/744eb93307a6607ebeb1e3da53365ab3995dc6cc/exporter/otlphttpexporter/otlp.go#L162-L167
				retryAfter := int64(limitsData.ResetAfter.Seconds())
				ginCtx.Header("Retry-After", strconv.FormatInt(retryAfter, 10))
				ginCtx.AbortWithStatus(http.StatusTooManyRequests)
			default:
				// NOTE(prozlach): This is a programming error, as the limitID
				// is an enum in request params. If we arrived here, then it
				// means that somebody did not adjust all code-paths when
				// adding new limitID.
				panic(fmt.Sprintf("unrecognized limitID %s", limitID))
			}
			return
		}
	}
}
