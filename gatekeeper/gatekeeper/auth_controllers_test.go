package gatekeeper_test

import (
	"bytes"
	"errors"
	"net/http"
	"net/http/httptest"
	"time"

	"github.com/go-redis/redis_rate/v10"
	. "github.com/onsi/ginkgo/v2"
	. "github.com/onsi/gomega"
	"github.com/stretchr/testify/mock"

	"github.com/gin-contrib/sessions"
	"github.com/gin-contrib/sessions/memstore"
	"github.com/gin-gonic/gin"
	"gitlab.com/gitlab-org/opstrace/opstrace/gatekeeper/gatekeeper"
	"gitlab.com/gitlab-org/opstrace/opstrace/gatekeeper/gatekeeper/test"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/testutils"
	schedulerv1alpha1 "gitlab.com/gitlab-org/opstrace/opstrace/scheduler/api/v1alpha1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	kruntime "k8s.io/apimachinery/pkg/runtime"
	"sigs.k8s.io/controller-runtime/pkg/client/fake"
)

func newAuthTestRequest(path string, headers []Pair) *http.Request {
	GinkgoHelper()

	req, err := http.NewRequest(http.MethodGet, path, nil)
	Expect(err).NotTo(HaveOccurred())

	// add headers to test case request
	for _, h := range headers {
		req.Header.Set(h.Key, h.Value)
	}

	return req
}

var _ = Context("Webhook API authentication", func() {
	var (
		gitlabPHS *testutils.ProgrammableHTTPServer
		router    *gin.Engine
		logOutput *bytes.Buffer
	)

	var glatDummyTokens = map[string]struct{}{
		"glpat-rwrwrwrwrwrwrwrwrwrw": {},
		"glpat-rrrrrrrrrrrrrrrrrrrr": {},
		"glpat-wwwwwwwwwwwwwwwwwwww": {},
	}

	BeforeEach(func() {
		gitlabPHS = testutils.NewProgrammableHTTPServer(logger, "")
		configurePHSforGitlabTokenAuthnz(gitlabPHS, glatDummyTokens, testTokenTypeGroup)
		gitlabPHS.Start()

		gin.SetMode(gin.DebugMode)
		gin.DefaultWriter = GinkgoWriter

		router = gin.Default()

		// logs capture
		logOutput = new(bytes.Buffer)
		GinkgoWriter.TeeTo(logOutput)

		// in-memory session storage
		store := memstore.NewStore([]byte("secret"))
		router.Use(sessions.Sessions("mysession", store))

		// templates needed to render error pages
		router.LoadHTMLGlob("../templates/*.gohtml")

		// setup fake k8s client
		scheme := kruntime.NewScheme()
		Expect(schedulerv1alpha1.AddToScheme(scheme)).To(Succeed())
		fakek8sCLient := fake.NewClientBuilder().WithScheme(scheme).WithRuntimeObjects(
			&schedulerv1alpha1.GitLabNamespace{
				ObjectMeta: metav1.ObjectMeta{
					Name:      "1234",
					Namespace: "default",
				},
			},
			&schedulerv1alpha1.GitLabNamespace{
				ObjectMeta: metav1.ObjectMeta{
					Name:      "13",
					Namespace: "default",
				},
			},
		).Build()

		router.Use(gatekeeper.ErrorLogger())
		// config middleware with mocks
		router.Use(gatekeeper.Config(&gatekeeper.ConfigOptions{
			Namespace:  "default",
			K8sClient:  fakek8sCLient,
			GitlabAddr: gitlabPHS.ServerURL(),
		}))
		router.Use(func(ctx *gin.Context) {
			ctx.Set(gatekeeper.CacheClientKey, NewMockRedis())
		})
	})

	AfterEach(func() {
		gitlabPHS.Stop()
		GinkgoWriter.ClearTeeWriters()
	})

	Context("/v1/auth/check path", func() {
		BeforeEach(func() {
			gatekeeper.SetRoutes(router, false)
		})

		It("should redirect to start the auth flow", func() {
			recorder := httptest.NewRecorder()
			testReq := newAuthTestRequest("/v1/auth/check",
				[]Pair{
					{
						Key:   "X-Forwarded-Proto",
						Value: "https",
					},
					{
						Key:   "X-Forwarded-Host",
						Value: "gob.local",
					},
					{
						Key:   "X-Forwarded-Port",
						Value: "443",
					},
					{
						Key:   "X-Forwarded-Uri",
						Value: "/foo/bar?baz=1",
					}})
			router.ServeHTTP(recorder, testReq)

			Expect(recorder.Code).To(Equal(302))
			headers := recorder.Result().Header
			Expect(recorder.Result().Cookies()).To(BeEmpty())
			redirectDestination := headers.Get("Location")
			Expect(redirectDestination).To(Equal("https://gob.local:443/v1/auth/start?rt=%2Ffoo%2Fbar%3Fbaz%3D1"))
		})
	})

	Context("/v1/auth/webhook/ path", func() {
		Describe("common token based auth tests", func() {
			BeforeEach(func() {
				router.Use(gatekeeper.SetRateLimiter(gatekeeper.NewNullRateLimiter()))
				gatekeeper.SetRoutes(router, false)
			})

			DescribeTable("should 404 if namespace is missing",
				func(urlPath string) {
					recorder := httptest.NewRecorder()
					testReq := newAuthTestRequest(urlPath, nil)

					router.ServeHTTP(recorder, testReq)

					Expect(recorder.Code).To(Equal(404))

				},
				Entry("Both Auth Handler", "/v1/auth/both/webhook/?min_accesslevel=20&auto_auth=false"),
				Entry("Group Auth Handler", "/v1/auth/group/webhook/?min_accesslevel=20&auto_auth=false"),
			)

			DescribeTable("should 404 if action is missing",
				func(urlPath string) {
					recorder := httptest.NewRecorder()
					testReq := newAuthTestRequest(urlPath, nil)

					router.ServeHTTP(recorder, testReq)

					Expect(recorder.Code).To(Equal(404))

				},
				Entry("Both Auth Handler", "/v1/auth/both/webhook/13/115?min_accesslevel=20&auto_auth=false"),
				Entry("Group Auth Handler", "/v1/auth/group/webhook/13?min_accesslevel=20&auto_auth=false"),
				Entry("Project Auth Handler", "/v1/auth/project/webhook/115?min_accesslevel=20&auto_auth=false"),
			)

			DescribeTable("should 404 if project is missing",
				func(urlPath string) {
					recorder := httptest.NewRecorder()
					testReq := newAuthTestRequest(urlPath, nil)

					router.ServeHTTP(recorder, testReq)

					Expect(recorder.Code).To(Equal(404))

				},
				Entry("Both Auth Handler", "/v1/auth/both/webhook/13?min_accesslevel=20&auto_auth=false"),
				Entry("Project Auth Handler", "/v1/auth/project/webhook?min_accesslevel=20&auto_auth=false"),
			)

			DescribeTable("should 401 if auth headers are missing and auto-auth is disabled",
				func(urlPath string) {
					recorder := httptest.NewRecorder()
					testReq := newAuthTestRequest(urlPath, nil)

					router.ServeHTTP(recorder, testReq)

					Expect(recorder.Code).To(Equal(401))

					Expect(logOutput.String()).To(ContainSubstring("handling session cookie-based auth"))
				},
				Entry("Both Auth Handler", "/v1/auth/both/webhook/13/115/read?min_accesslevel=20&auto_auth=false"),
				Entry("Group Auth Handler", "/v1/auth/group/webhook/13/read?min_accesslevel=20&auto_auth=false"),
				Entry("Project Auth Handler", "/v1/auth/project/webhook/115/read?min_accesslevel=20&auto_auth=false"),
			)

			DescribeTable("should redirect if auth headers are missing and auto-auth is enabled",
				func(urlPath string) {
					recorder := httptest.NewRecorder()
					testReq := newAuthTestRequest(
						urlPath,
						[]Pair{
							{
								Key:   "X-Forwarded-Proto",
								Value: "https",
							},
							{
								Key:   "X-Forwarded-Host",
								Value: "gob.devvm",
							},
							{
								Key:   "X-Forwarded-Port",
								Value: "443",
							},
							{
								Key:   "X-Forwarded-Uri",
								Value: "/foo/bar?baz=1",
							},
						},
					)

					router.ServeHTTP(recorder, testReq)

					Expect(recorder.Code).To(Equal(302))
					headers := recorder.Result().Header
					redirectDestination := headers.Get("Location")
					Expect(redirectDestination).To(Equal("https://gob.devvm:443/v1/auth/start?rt=%2Ffoo%2Fbar%3Fbaz%3D1"))

					Expect(logOutput.String()).To(ContainSubstring("handling session cookie-based auth"))
				},
				Entry("Both Auth Handler", "/v1/auth/both/webhook/14/115/read?min_accesslevel=20&auto_auth=true"),
				Entry("Group Auth Handler", "/v1/auth/group/webhook/13/read?min_accesslevel=20&auto_auth=true"),
				Entry("Project Auth Handler", "/v1/auth/project/webhook/115/read?min_accesslevel=20&auto_auth=true"),
			)

			DescribeTable("should 403 if namespace is invalid",
				func(urlPath string) {
					recorder := httptest.NewRecorder()
					testReq := newAuthTestRequest(urlPath, nil)

					router.ServeHTTP(recorder, testReq)

					Expect(recorder.Code).To(Equal(http.StatusForbidden))

					Expect(logOutput.String()).To(ContainSubstring("validation failed: strconv.ParseInt"))
				},
				Entry("Both Auth Handler", "/v1/auth/both/webhook/@/115/read?min_accesslevel=20&auto_auth=false"),
				Entry("Group Auth Handler", "/v1/auth/group/webhook/@/read?min_accesslevel=20&auto_auth=false"),
			)

			DescribeTable("should 403 if project is invalid",
				func(urlPath string) {
					recorder := httptest.NewRecorder()
					testReq := newAuthTestRequest(urlPath, nil)

					router.ServeHTTP(recorder, testReq)

					Expect(recorder.Code).To(Equal(http.StatusForbidden))

					Expect(logOutput.String()).To(ContainSubstring("path parameters validation failed: strconv.ParseInt: parsing"))
				},
				Entry("Both Auth Handler", "/v1/auth/both/webhook/13/@/read?min_accesslevel=20&auto_auth=false"),
				Entry("Project Auth Handler", "/v1/auth/project/webhook/@/read?min_accesslevel=20&auto_auth=false"),
			)

			DescribeTable("should 403 if action is invalid",
				func(urlPath string) {
					recorder := httptest.NewRecorder()
					testReq := newAuthTestRequest(urlPath, nil)

					router.ServeHTTP(recorder, testReq)

					Expect(recorder.Code).To(Equal(http.StatusForbidden))

					Expect(logOutput.String()).To(ContainSubstring("Field validation for 'Action' failed on the 'oneof' tag"))
				},
				Entry("Both Auth Handler", "/v1/auth/both/webhook/13/115/foobar?min_accesslevel=20&auto_auth=false"),
				Entry("Group Auth Handler", "/v1/auth/group/webhook/13/foobar?min_accesslevel=20&auto_auth=false"),
				Entry("Project Auth Handler", "/v1/auth/project/webhook/115/foobar?min_accesslevel=20&auto_auth=false"),
			)

			DescribeTable("should 403 if min_accesslevel is invalid",
				func(urlPath string) {
					recorder := httptest.NewRecorder()
					testReq := newAuthTestRequest(urlPath, nil)

					router.ServeHTTP(recorder, testReq)

					Expect(recorder.Code).To(Equal(http.StatusForbidden))

					Expect(logOutput.String()).To(ContainSubstring(`uri parameters validation failed: strconv.ParseInt: parsing \"100s\": invalid syntax`))
				},
				Entry("Both Auth Handler", "/v1/auth/both/webhook/13/115/read?min_accesslevel=100s&auto_auth=false"),
				Entry("Project Auth Handler", "/v1/auth/project/webhook/115/read?min_accesslevel=100s&auto_auth=false"),
				Entry("Group Auth Handler", "/v1/auth/group/webhook/13/read?min_accesslevel=100s&auto_auth=false"),
			)

			DescribeTable("should 403 if min_accesslevel is missing",
				func(urlPath string) {
					recorder := httptest.NewRecorder()
					testReq := newAuthTestRequest(urlPath, nil)

					router.ServeHTTP(recorder, testReq)

					Expect(recorder.Code).To(Equal(http.StatusForbidden))

					Expect(logOutput.String()).To(ContainSubstring("Error:Field validation for 'MinAccessLevel' failed on the 'required' tag"))
				},
				Entry("Project Auth Handler", "/v1/auth/project/webhook/115/read"),
				Entry("Both Auth Handler", "/v1/auth/both/webhook/13/115/read"),
				Entry("Group Auth Handler", "/v1/auth/group/webhook/13/read"),
			)
		})

		Describe("Rate-limiting", func() {

			DescribeTable("should 403 if invalid rate-limit ID was given",
				func(urlPath string) {
					recorder := httptest.NewRecorder()
					testReq := newAuthTestRequest(
						urlPath,
						[]Pair{
							{
								Key:   "private-token",
								Value: "glpat-rwrwrwrwrwrwrwrwrwrw",
							},
							{
								Key:   "X-Forwarded-Method",
								Value: "POST",
							},
							{
								Key:   "X-Content-Length",
								Value: "1000",
							},
						},
					)

					router.Use(gatekeeper.SetRateLimiter(gatekeeper.NewNullRateLimiter()))

					gatekeeper.SetRoutes(router, false)
					router.ServeHTTP(recorder, testReq)

					// Was the request permitted?
					Expect(recorder.Code).To(Equal(http.StatusForbidden))
				},
				Entry("Project Auth Handler", "/v1/auth/project/webhook/156/read?min_accesslevel=20&auto_auth=false&limit_id=fooBar"),
				Entry("Both Auth Handler", "/v1/auth/both/webhook/153/156/read?min_accesslevel=20&auto_auth=false&limit_id=fooBar"),
				Entry("Group Auth Handler", "/v1/auth/group/webhook/153/read?min_accesslevel=20&auto_auth=false&limit_id=fooBar"),
			)

			DescribeTable("should 403 if trace-limiting got non-POST method",
				func(urlPath string) {
					recorder := httptest.NewRecorder()
					testReq := newAuthTestRequest(
						urlPath,
						[]Pair{
							{
								Key:   "private-token",
								Value: "glpat-rwrwrwrwrwrwrwrwrwrw",
							},
							{
								Key:   "X-Forwarded-Method",
								Value: "GET",
							},
							{
								Key:   "X-Content-Length",
								Value: "1000",
							},
						},
					)

					router.Use(gatekeeper.SetRateLimiter(gatekeeper.NewNullRateLimiter()))

					gatekeeper.SetRoutes(router, false)
					router.ServeHTTP(recorder, testReq)

					// Was the request permitted?
					Expect(recorder.Code).To(Equal(http.StatusForbidden))
				},
				Entry("Project Auth Handler", "/v1/auth/project/webhook/156/read?min_accesslevel=20&auto_auth=false&limit_id=tr_write"),
				Entry("Both Auth Handler", "/v1/auth/both/webhook/153/156/read?min_accesslevel=20&auto_auth=false&limit_id=tr_write"),
				Entry("Group Auth Handler", "/v1/auth/group/webhook/153/read?min_accesslevel=20&auto_auth=false&limit_id=tr_write"),
			)

			DescribeTable("should 403 if X-Content-Length header is missing",
				func(urlPath string) {
					recorder := httptest.NewRecorder()
					testReq := newAuthTestRequest(
						urlPath,
						[]Pair{
							{
								Key:   "private-token",
								Value: "glpat-rwrwrwrwrwrwrwrwrwrw",
							},
							{
								Key:   "X-Forwarded-Method",
								Value: "POST",
							},
						},
					)

					router.Use(gatekeeper.SetRateLimiter(gatekeeper.NewNullRateLimiter()))

					gatekeeper.SetRoutes(router, false)
					router.ServeHTTP(recorder, testReq)

					// Was the request permitted?
					Expect(recorder.Code).To(Equal(http.StatusForbidden))
				},
				Entry("Project Auth Handler", "/v1/auth/project/webhook/156/read?min_accesslevel=20&auto_auth=false&limit_id=tr_write"),
				Entry("Both Auth Handler", "/v1/auth/both/webhook/153/156/read?min_accesslevel=20&auto_auth=false&limit_id=tr_write"),
				Entry("Group Auth Handler", "/v1/auth/group/webhook/153/read?min_accesslevel=20&auto_auth=false&limit_id=tr_write"),
			)

			DescribeTable("should 200 if limits are not set",
				func(urlPath string) {
					recorder := httptest.NewRecorder()
					testReq := newAuthTestRequest(
						urlPath,
						[]Pair{
							{
								Key:   "private-token",
								Value: "glpat-rwrwrwrwrwrwrwrwrwrw",
							},
							{
								Key:   "X-Forwarded-Method",
								Value: "POST",
							},
							{
								Key:   "X-Content-Length",
								Value: "1000",
							},
						},
					)

					rlMock := test.NewRateLimiterMock(GinkgoT())
					rlMock.On(
						"IsAllowed", mock.Anything, int(13), "tr_write", uint64(1000),
					).Once().Return(nil, nil)
					router.Use(gatekeeper.SetRateLimiter(rlMock))

					gatekeeper.SetRoutes(router, false)
					router.ServeHTTP(recorder, testReq)

					// Was the request permitted?
					Expect(recorder.Code).To(Equal(http.StatusOK))
				},
				Entry("Project Auth Handler", "/v1/auth/project/webhook/156/read?min_accesslevel=20&auto_auth=false&limit_id=tr_write"),
				Entry("Both Auth Handler", "/v1/auth/both/webhook/153/156/read?min_accesslevel=20&auto_auth=false&limit_id=tr_write"),
				Entry("Group Auth Handler", "/v1/auth/group/webhook/153/read?min_accesslevel=20&auto_auth=false&limit_id=tr_write"),
			)

			DescribeTable("should 403 if error occurred",
				func(urlPath string) {
					recorder := httptest.NewRecorder()
					testReq := newAuthTestRequest(
						urlPath,
						[]Pair{
							{
								Key:   "private-token",
								Value: "glpat-rwrwrwrwrwrwrwrwrwrw",
							},
							{
								Key:   "X-Forwarded-Method",
								Value: "POST",
							},
							{
								Key:   "X-Content-Length",
								Value: "1000",
							},
						},
					)

					rlMock := test.NewRateLimiterMock(GinkgoT())
					rlMock.On(
						"IsAllowed", mock.Anything, int(13), "tr_write", uint64(1000),
					).Once().Return(nil, errors.New("foo is baring"))
					router.Use(gatekeeper.SetRateLimiter(rlMock))

					gatekeeper.SetRoutes(router, false)
					router.ServeHTTP(recorder, testReq)

					// Was the request permitted?
					Expect(recorder.Code).To(Equal(http.StatusForbidden))
				},
				Entry("Project Auth Handler", "/v1/auth/project/webhook/156/read?min_accesslevel=20&auto_auth=false&limit_id=tr_write"),
				Entry("Both Auth Handler", "/v1/auth/both/webhook/153/156/read?min_accesslevel=20&auto_auth=false&limit_id=tr_write"),
				Entry("Group Auth Handler", "/v1/auth/group/webhook/153/read?min_accesslevel=20&auto_auth=false&limit_id=tr_write"),
			)

			DescribeTable("should 200 if within limits",
				func(urlPath string) {
					recorder := httptest.NewRecorder()
					testReq := newAuthTestRequest(
						urlPath,
						[]Pair{
							{
								Key:   "private-token",
								Value: "glpat-rwrwrwrwrwrwrwrwrwrw",
							},
							{
								Key:   "X-Forwarded-Method",
								Value: "POST",
							},
							{
								Key:   "X-Content-Length",
								Value: "1000",
							},
						},
					)

					res := &redis_rate.Result{
						Allowed: 5000,
					}
					rlMock := test.NewRateLimiterMock(GinkgoT())
					rlMock.On(
						"IsAllowed", mock.Anything, int(13), "tr_write", uint64(1000),
					).Once().Return(res, nil)
					router.Use(gatekeeper.SetRateLimiter(rlMock))

					gatekeeper.SetRoutes(router, false)
					router.ServeHTTP(recorder, testReq)

					// Was the request permitted?
					Expect(recorder.Code).To(Equal(http.StatusOK))
				},
				Entry("Project Auth Handler", "/v1/auth/project/webhook/156/read?min_accesslevel=20&auto_auth=false&limit_id=tr_write"),
				Entry("Both Auth Handler", "/v1/auth/both/webhook/153/156/read?min_accesslevel=20&auto_auth=false&limit_id=tr_write"),
				Entry("Group Auth Handler", "/v1/auth/group/webhook/153/read?min_accesslevel=20&auto_auth=false&limit_id=tr_write"),
			)

			DescribeTable("should 429 if outside of limits",
				func(urlPath string) {
					recorder := httptest.NewRecorder()
					testReq := newAuthTestRequest(
						urlPath,
						[]Pair{
							{
								Key:   "private-token",
								Value: "glpat-rwrwrwrwrwrwrwrwrwrw",
							},
							{
								Key:   "X-Forwarded-Method",
								Value: "POST",
							},
							{
								Key:   "X-Content-Length",
								Value: "1000",
							},
						},
					)

					res := &redis_rate.Result{
						Allowed:    0,
						ResetAfter: 10 * time.Second,
					}
					rlMock := test.NewRateLimiterMock(GinkgoT())
					rlMock.On(
						"IsAllowed", mock.Anything, int(13), "tr_write", uint64(1000),
					).Once().Return(res, nil)
					router.Use(gatekeeper.SetRateLimiter(rlMock))

					gatekeeper.SetRoutes(router, false)
					router.ServeHTTP(recorder, testReq)

					// Was the request permitted?
					Expect(recorder.Code).To(Equal(http.StatusTooManyRequests))

					headers := recorder.Result().Header
					proxyAuthEmail := headers.Get("Retry-After")
					Expect(proxyAuthEmail).To(Equal("10"))

				},
				Entry("Project Auth Handler", "/v1/auth/project/webhook/156/read?min_accesslevel=20&auto_auth=false&limit_id=tr_write"),
				Entry("Both Auth Handler", "/v1/auth/both/webhook/153/156/read?min_accesslevel=20&auto_auth=false&limit_id=tr_write"),
				Entry("Group Auth Handler", "/v1/auth/group/webhook/153/read?min_accesslevel=20&auto_auth=false&limit_id=tr_write"),
			)
		})

		Describe("Gitlab Access Token-based auth", func() {
			BeforeEach(func() {
				router.Use(gatekeeper.SetRateLimiter(gatekeeper.NewNullRateLimiter()))
				gatekeeper.SetRoutes(router, false)
			})

			It("should 200 if token is valid - Group", func() {
				recorder := httptest.NewRecorder()
				testReq := newAuthTestRequest(
					"/v1/auth/group/webhook/13/read?min_accesslevel=20&auto_auth=false",
					[]Pair{
						{
							Key:   "private-token",
							Value: "glpat-rwrwrwrwrwrwrwrwrwrw",
						},
					},
				)

				router.ServeHTTP(recorder, testReq)

				// Was the request permitted?
				Expect(recorder.Code).To(Equal(http.StatusOK))

				verifyPHSRequestMade(gitlabPHS, http.MethodGet, "/api/v4/personal_access_tokens/self")
				verifyPHSRequestMade(gitlabPHS, http.MethodGet, "/api/v4/namespaces/13")
				verifyPHSRequestMade(gitlabPHS, http.MethodGet, "/api/v4/user")
				verifyPHSRequestMade(gitlabPHS, http.MethodGet, "/api/v4/groups/13/members/all/61")

				// And finally - check if log message is produced
				Expect(logOutput.String()).To(ContainSubstring("HandleGroupAccessAuth decision OK"))
			})

			It("should not issue GOUI calls by default - Group", func() {
				recorder := httptest.NewRecorder()
				testReq := newAuthTestRequest(
					"/v1/auth/group/webhook/13/read?min_accesslevel=20&auto_auth=false",
					[]Pair{
						{
							Key:   "private-token",
							Value: "glpat-rwrwrwrwrwrwrwrwrwrw",
						},
					},
				)

				router.ServeHTTP(recorder, testReq)

				// Was the request permitted?
				Expect(recorder.Code).To(Equal(http.StatusOK))

				// Check the authProxy headers
				headers := recorder.Result().Header
				proxyAuthEmail := headers.Get("X-WEBAUTH-EMAIL")
				Expect(proxyAuthEmail).To(BeEmpty())
				proxyAuthOrgID := headers.Get("X-GRAFANA-ORG-ID")
				Expect(proxyAuthOrgID).To(BeEmpty())

				// And finally - check if log message is produced
				Expect(logOutput.String()).To(ContainSubstring("HandleGroupAccessAuth decision OK"))
			})

			It("should 200 if token is valid - Project+Group", func() {
				recorder := httptest.NewRecorder()
				testReq := newAuthTestRequest(
					"/v1/auth/both/webhook/13/115/read?min_accesslevel=20&auto_auth=false",
					[]Pair{
						{
							Key:   "private-token",
							Value: "glpat-rwrwrwrwrwrwrwrwrwrw",
						},
					},
				)

				router.ServeHTTP(recorder, testReq)

				// Was the request permitted?
				Expect(recorder.Code).To(Equal(http.StatusOK))
				verifyPHSRequestMade(gitlabPHS, http.MethodGet, "/api/v4/personal_access_tokens/self")
				verifyPHSRequestMade(gitlabPHS, http.MethodGet, "/api/v4/projects/115")
				verifyPHSRequestMade(gitlabPHS, http.MethodGet, "/api/v4/user")
				verifyPHSRequestMade(gitlabPHS, http.MethodGet, "/api/v4/projects/115/groups")
				verifyPHSRequestMade(gitlabPHS, http.MethodGet, "/api/v4/projects/115/members/all/61")

				// And finally - check if log message is produced
				Expect(logOutput.String()).To(ContainSubstring("HandleProjectAccessAuth decision OK"))
			})

			It("should 200 if token is valid - Project", func() {
				recorder := httptest.NewRecorder()
				testReq := newAuthTestRequest(
					"/v1/auth/project/webhook/115/read?min_accesslevel=20&auto_auth=false",
					[]Pair{
						{
							Key:   "private-token",
							Value: "glpat-rwrwrwrwrwrwrwrwrwrw",
						},
					},
				)

				router.ServeHTTP(recorder, testReq)

				// Was the request permitted?
				Expect(recorder.Code).To(Equal(http.StatusOK))

				headers := recorder.Result().Header
				toplevelNS := headers.Get("x-top-level-namespace")
				Expect(toplevelNS).To(Equal("13"))

				verifyPHSRequestMade(gitlabPHS, http.MethodGet, "/api/v4/personal_access_tokens/self")
				verifyPHSRequestMade(gitlabPHS, http.MethodGet, "/api/v4/user")
				verifyPHSRequestMade(gitlabPHS, http.MethodGet, "/api/v4/projects/115/members/all/61")

				// And finally - check if log message is produced
				Expect(logOutput.String()).To(ContainSubstring("HandleProjectAccessAuth decision OK"))
			})

			It("should 403 if gitlab namespace does not exist - Group", func() {
				// Namespace info query
				nsReply := func(r *http.Request) (int, string) {
					if _, ok := glatDummyTokens[r.Header.Get("private-token")]; ok {
						return 404, ``
					}
					return http.StatusForbidden, GitlabReplyUnauthorized
				}
				gitlabPHS.SetResponseFunc(
					http.MethodGet, "/api/v4/namespaces/13",
					nsReply,
				)

				recorder := httptest.NewRecorder()
				testReq := newAuthTestRequest(
					"/v1/auth/group/webhook/13/read?min_accesslevel=20&auto_auth=false",
					[]Pair{
						{
							Key:   "private-token",
							Value: "glpat-rwrwrwrwrwrwrwrwrwrw",
						},
					},
				)

				router.ServeHTTP(recorder, testReq)

				// Was the request permitted?
				Expect(recorder.Code).To(Equal(http.StatusForbidden))
				verifyPHSRequestMade(gitlabPHS, http.MethodGet, "/api/v4/personal_access_tokens/self")
				verifyPHSRequestMade(gitlabPHS, http.MethodGet, "/api/v4/namespaces/13")
			})

			DescribeTable("should 403 if gitlab project does not exist",
				func(urlPath string) {
					httpReply := func(r *http.Request) (int, string) {
						if _, ok := glatDummyTokens[r.Header.Get("private-token")]; ok {
							return 404, ``
						}
						return http.StatusForbidden, GitlabReplyUnauthorized
					}
					gitlabPHS.SetResponseFunc(
						http.MethodGet, "/api/v4/projects/115",
						httpReply,
					)

					httpReply = func(r *http.Request) (int, string) {
						if _, ok := glatDummyTokens[r.Header.Get("private-token")]; ok {
							return 404, ``
						}
						return http.StatusForbidden, GitlabReplyUnauthorized
					}
					gitlabPHS.SetResponseFunc(
						http.MethodGet, "/api/v4/projects/115/members/all/61",
						httpReply,
					)

					recorder := httptest.NewRecorder()
					testReq := newAuthTestRequest(
						urlPath,
						[]Pair{
							{
								Key:   "private-token",
								Value: "glpat-rwrwrwrwrwrwrwrwrwrw",
							},
						},
					)

					router.ServeHTTP(recorder, testReq)

					// Was the request permitted?
					Expect(recorder.Code).To(Equal(http.StatusForbidden))
				},
				Entry("Project Auth Handler", "/v1/auth/project/webhook/115/read?min_accesslevel=20&auto_auth=false"),
				Entry("Both Auth Handler", "/v1/auth/both/webhook/13/115/read?min_accesslevel=20&auto_auth=false"),
			)

			It("should 403 if token group membership is inactive - Group", func() {
				membershipReply := func(r *http.Request) (int, string) {
					if _, ok := glatDummyTokens[r.Header.Get("private-token")]; ok {
						return http.StatusOK, `{
			                                "state": "inactive",
			                                "access_level": 30
			                              }`
					}
					return http.StatusForbidden, GitlabReplyUnauthorized
				}
				gitlabPHS.SetResponseFunc(
					http.MethodGet, "/api/v4/groups/13/members/all/61",
					membershipReply,
				)

				recorder := httptest.NewRecorder()
				testReq := newAuthTestRequest(
					"/v1/auth/group/webhook/13/read?min_accesslevel=20&auto_auth=false",
					[]Pair{
						{
							Key:   "private-token",
							Value: "glpat-rwrwrwrwrwrwrwrwrwrw",
						},
					},
				)

				router.ServeHTTP(recorder, testReq)

				// Was the request permitted?
				Expect(recorder.Code).To(Equal(http.StatusForbidden))
				verifyPHSRequestMade(gitlabPHS, http.MethodGet, "/api/v4/personal_access_tokens/self")
				verifyPHSRequestMade(gitlabPHS, http.MethodGet, "/api/v4/namespaces/13")
				verifyPHSRequestMade(gitlabPHS, http.MethodGet, "/api/v4/user")
				verifyPHSRequestMade(gitlabPHS, http.MethodGet, "/api/v4/groups/13/members/all/61")
			})

			DescribeTable("should 403 if token project membership is inactive",
				func(urlPath string) {
					projectGroups := func(r *http.Request) (int, string) {
						if _, ok := glatDummyTokens[r.Header.Get("private-token")]; ok {
							return http.StatusOK, `{
                                "state": "inactive",
                                "access_level": 30
                            }`
						}
						return http.StatusForbidden, GitlabReplyUnauthorized
					}
					gitlabPHS.SetResponseFunc(
						http.MethodGet, "/api/v4/projects/115/members/all/61",
						projectGroups,
					)

					recorder := httptest.NewRecorder()
					testReq := newAuthTestRequest(
						urlPath,
						[]Pair{
							{
								Key:   "private-token",
								Value: "glpat-rwrwrwrwrwrwrwrwrwrw",
							},
						},
					)

					router.ServeHTTP(recorder, testReq)

					// Was the request permitted?
					Expect(recorder.Code).To(Equal(http.StatusForbidden))
					verifyPHSRequestMade(gitlabPHS, http.MethodGet, "/api/v4/personal_access_tokens/self")
					verifyPHSRequestMade(gitlabPHS, http.MethodGet, "/api/v4/projects/115/members/all/61")
				},
				Entry("Project Auth Handler", "/v1/auth/project/webhook/115/read?min_accesslevel=20&auto_auth=false"),
				Entry("Both Auth Handler", "/v1/auth/both/webhook/13/115/read?min_accesslevel=20&auto_auth=false"),
			)

			It("should 403 if token group membership is too low - Group", func() {
				membershipReply := func(r *http.Request) (int, string) {
					if _, ok := glatDummyTokens[r.Header.Get("private-token")]; ok {
						return http.StatusOK, `{
			                                "state": "active",
			                                "access_level": 20
			                              }`
					}
					return http.StatusForbidden, GitlabReplyUnauthorized
				}
				gitlabPHS.SetResponseFunc(
					http.MethodGet, "/api/v4/groups/13/members/all/61",
					membershipReply,
				)

				recorder := httptest.NewRecorder()
				testReq := newAuthTestRequest(
					"/v1/auth/group/webhook/13/read?min_accesslevel=50&auto_auth=false",
					[]Pair{
						{
							Key:   "private-token",
							Value: "glpat-rwrwrwrwrwrwrwrwrwrw",
						},
					},
				)

				router.ServeHTTP(recorder, testReq)

				// Was the request permitted?
				Expect(recorder.Code).To(Equal(http.StatusForbidden))
				verifyPHSRequestMade(gitlabPHS, http.MethodGet, "/api/v4/personal_access_tokens/self")
				verifyPHSRequestMade(gitlabPHS, http.MethodGet, "/api/v4/namespaces/13")
				verifyPHSRequestMade(gitlabPHS, http.MethodGet, "/api/v4/user")
				verifyPHSRequestMade(gitlabPHS, http.MethodGet, "/api/v4/groups/13/members/all/61")
			})

			DescribeTable("should 403 if token project membership is too low",
				func(urlPath string) {
					projectGroups := func(r *http.Request) (int, string) {
						if _, ok := glatDummyTokens[r.Header.Get("private-token")]; ok {

							//nolint:goconst
							return http.StatusOK, `{
                                "state": "active",
                                "access_level": 30
                            }`
						}
						return http.StatusForbidden, GitlabReplyUnauthorized
					}
					gitlabPHS.SetResponseFunc(
						http.MethodGet, "/api/v4/projects/115/members/all/61",
						projectGroups,
					)

					recorder := httptest.NewRecorder()
					testReq := newAuthTestRequest(
						urlPath,
						[]Pair{
							{
								Key:   "private-token",
								Value: "glpat-rwrwrwrwrwrwrwrwrwrw",
							},
						},
					)

					router.ServeHTTP(recorder, testReq)

					// Was the request permitted?
					Expect(recorder.Code).To(Equal(http.StatusForbidden))
					verifyPHSRequestMade(gitlabPHS, http.MethodGet, "/api/v4/personal_access_tokens/self")
					verifyPHSRequestMade(gitlabPHS, http.MethodGet, "/api/v4/projects/115/members/all/61")
				},
				Entry("Project Auth Handler", "/v1/auth/project/webhook/115/read?min_accesslevel=50&auto_auth=false"),
				Entry("Both Auth Handler", "/v1/auth/both/webhook/13/115/read?min_accesslevel=50&auto_auth=false"),
			)

			DescribeTable("should 403 if token does not have correct scopes",
				func(urlPath string) {
					patSelfReply := func(r *http.Request) (int, string) {
						if _, ok := glatDummyTokens[r.Header.Get("private-token")]; ok {
							return http.StatusOK, GitlabTokenRegistryScopes
						}
						return http.StatusForbidden, GitlabReplyUnauthorized
					}
					gitlabPHS.SetResponseFunc(
						http.MethodGet, "/api/v4/personal_access_tokens/self",
						patSelfReply,
					)

					recorder := httptest.NewRecorder()
					testReq := newAuthTestRequest(
						urlPath,
						[]Pair{
							{
								Key:   "private-token",
								Value: "glpat-rwrwrwrwrwrwrwrwrwrw",
							},
						},
					)

					router.ServeHTTP(recorder, testReq)

					// Was the request permitted?
					Expect(recorder.Code).To(Equal(http.StatusForbidden))
					verifyPHSRequestMade(gitlabPHS, http.MethodGet, "/api/v4/personal_access_tokens/self")
				},
				Entry("Project Auth Handler", "/v1/auth/project/webhook/115/read?min_accesslevel=50&auto_auth=false"),
				Entry("Both Auth Handler", "/v1/auth/both/webhook/13/115/read?min_accesslevel=50&auto_auth=false"),
				Entry("Group Auth Handler", "/v1/auth/group/webhook/13/read?min_accesslevel=30&auto_auth=false"),
			)

			DescribeTable("should 403 if token has only some of the scopes",
				func(urlPath string) {
					recorder := httptest.NewRecorder()
					testReq := newAuthTestRequest(
						urlPath,
						[]Pair{
							{
								Key:   "private-token",
								Value: "glpat-rrrrrrrrrrrrrrrrrrrr",
							},
						},
					)

					router.ServeHTTP(recorder, testReq)

					// Was the request permitted?
					Expect(recorder.Code).To(Equal(http.StatusForbidden))
					verifyPHSRequestMade(gitlabPHS, http.MethodGet, "/api/v4/personal_access_tokens/self")
				},
				Entry("Project Auth Handler", "/v1/auth/project/webhook/115/write?min_accesslevel=50&auto_auth=false"),
				Entry("Both Auth Handler", "/v1/auth/both/webhook/13/115/write?min_accesslevel=50&auto_auth=false"),
				Entry("Group Auth Handler", "/v1/auth/group/webhook/13/write?min_accesslevel=30&auto_auth=false"),
			)

			DescribeTable("should 200 if min_accesslevel=0",
				func(urlPath string) {
					recorder := httptest.NewRecorder()
					testReq := newAuthTestRequest(
						urlPath,
						[]Pair{
							{
								Key:   "private-token",
								Value: "glpat-rwrwrwrwrwrwrwrwrwrw",
							},
						},
					)

					router.ServeHTTP(recorder, testReq)

					Expect(recorder.Code).To(Equal(http.StatusOK))
				},
				Entry("Project Auth Handler", "/v1/auth/project/webhook/115/write?min_accesslevel=0&auto_auth=false"),
				Entry("Both Auth Handler", "/v1/auth/both/webhook/13/115/write?min_accesslevel=0&auto_auth=false"),
				Entry("Group Auth Handler", "/v1/auth/group/webhook/13/write?min_accesslevel=0&auto_auth=false"),
			)
		})
	})
})
