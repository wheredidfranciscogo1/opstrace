package gatekeeper

import (
	"fmt"
	"net"
	"net/url"

	"github.com/gin-gonic/gin"
	"github.com/sirupsen/logrus"
	log "github.com/sirupsen/logrus"
	"golang.org/x/oauth2"
)

type AuthOptions struct {
	ClientID     string
	ClientSecret string
	GitlabAddr   string
	ServerDomain string
}

// Ensures request is authenticated. If autoAuth=true, will redirect
// client through the auth flow and then return back to this route.
// If autoAuth=false, will return an unauthorized response to client.
func AuthenticatedSessionRequired(autoAuth bool, next gin.HandlerFunc) gin.HandlerFunc {
	return func(ctx *gin.Context) {
		token := GetAuthToken(ctx)

		if token == nil && autoAuth {
			host, port, err := net.SplitHostPort(ctx.Request.Header.Get("X-Forwarded-Host"))
			if err != nil {
				// No port found in string
				host = ctx.Request.Header.Get("X-Forwarded-Host")
				port = ctx.Request.Header.Get("X-Forwarded-Port")
			}
			authURI, err := url.Parse(
				fmt.Sprintf(
					"%s://%s:%s/v1/auth/start",
					ctx.Request.Header.Get("X-Forwarded-Proto"),
					host,
					port,
				),
			)
			if err != nil {
				ctx.AbortWithError(500, fmt.Errorf("parse authURI: %w", err))
				return
			}
			queryString := authURI.Query()
			// Add current URL as the rt parameter so we'll return
			// back to this route upon successful authentication
			queryString.Set("rt", ctx.Request.Header.Get("X-Forwarded-Uri"))
			authURI.RawQuery = queryString.Encode()

			ctx.Redirect(302, authURI.String())
			ctx.Abort()
			return
		}

		if token == nil {
			log.WithFields(logrus.Fields{
				"autoAuth": autoAuth,
			}).Debug("can't authenticate, token is missing")
			ctx.AbortWithStatus(401)
			return
		}

		gitLabService, err := NewGitLabServiceFromOauthToken(ctx)
		if err != nil {
			ctx.AbortWithError(500, fmt.Errorf("create gitlabservice: %w", err))
			return
		}

		// Make gitLabService available to all down stream handlers
		SetGitLabService(ctx, gitLabService)
		// Invoke next
		next(ctx)

		// If we refreshed the token, we need to ensure we save it in the session
		// for future requests
		latestToken, err := gitLabService.Token()
		if err != nil {
			ctx.AbortWithError(500, fmt.Errorf("refresh token: %w", err))
			return
		}

		if latestToken == nil {
			// If we failed to refresh the token and the current token
			// is also invalid, then clear the token from the session so
			// user can reauthenticate
			if err := ClearAuthToken(ctx); err != nil {
				log.Errorf("failed to clear authToken from session: %+v", err)
			}
			return
		}

		if err := SetAuthToken(ctx, latestToken); err != nil {
			log.Errorf("failed to save authToken in session: %+v", err)
		}
	}
}

// Make the oauth2 config available on the request context.
// Handlers can then invoke the the oauth flow and use the
// authenticated http.client for interacting with the gitlab API that
// is available on the oauth2.Config object.
func OAuth2(o *AuthOptions) gin.HandlerFunc {
	ac := &oauth2.Config{
		ClientID:     o.ClientID,
		ClientSecret: o.ClientSecret,
		Scopes:       []string{"api"},
		RedirectURL:  fmt.Sprintf("%s/v1/auth/callback", o.ServerDomain),
		Endpoint: oauth2.Endpoint{
			AuthURL:  fmt.Sprintf("%s/oauth/authorize", o.GitlabAddr),
			TokenURL: fmt.Sprintf("%s/oauth/token", o.GitlabAddr),
		},
	}

	return func(ctx *gin.Context) {
		// Make authConfig available to all downstream handlers
		SetAuthConfig(ctx, ac)
	}
}
