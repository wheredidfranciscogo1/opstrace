package gatekeeper

import (
	"errors"
	"fmt"
	"net/http"

	"github.com/gin-gonic/gin"
	log "github.com/sirupsen/logrus"
	"github.com/xanzy/go-gitlab"
)

type GroupParams struct {
	ID string `uri:"group_id" binding:"required"`
}

// Projects alias returned from handler
type Projects = []*gitlab.BasicProject

// ListGroupProjectsHandler lists all projects in a group.
// @Id ListGroupProjectsHandler
// @Summary Lists all projects in a group, used internally.
// @Router /v1/{group_id}/projects [get]
// @Param group_id path string true "Group ID"
// @Success 200 {array} Projects "GitLab projects"
// @Provides json
// @Deprecated
func ListGroupProjectsHandler(ctx *gin.Context) {
	var params GroupParams
	if err := ctx.ShouldBindUri(&params); err != nil {
		negotiateError(ctx, http.StatusBadRequest, "invalid group_id")
		return
	}

	g := GetGitLabService(ctx)
	user, err := g.CurrentUser()
	if err != nil {
		HandleError(ctx, err)
		return
	}
	// TODO: remove when we have enough test coverage. For now, be extra careful
	// and validate we have valid user struct.
	if user == nil {
		ctx.AbortWithError(http.StatusUnauthorized, fmt.Errorf("invalid gitlab user"))
		return
	}

	projects, err := g.GetGroupProjects(params.ID)
	if errors.Is(err, ErrUnauthorized) {
		HandleError(ctx, err)
		return
	}
	if err != nil {
		HandleError(ctx, fmt.Errorf("failed to fetch projects for the groupID(%s): %w", params.ID, err))
		return
	}
	log.WithField("projects", projects).Debugf("fetched projects for groupID(%s)", params.ID)

	ctx.JSON(200, projects)
}
