package gatekeeper

import (
	"errors"
	"fmt"
	"net/http"

	"github.com/gin-gonic/gin"
	log "github.com/sirupsen/logrus"
	gitlab "github.com/xanzy/go-gitlab"
	opstracev1alpha1 "gitlab.com/gitlab-org/opstrace/opstrace/scheduler/api/v1alpha1"
	k8serrors "k8s.io/apimachinery/pkg/api/errors"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"sigs.k8s.io/controller-runtime/pkg/client"
)

type RoutingParams struct {
	ID     string `uri:"namespace_id" binding:"required"`
	Action string `uri:"action"`
}

// HandlePath catches any gitlab namespace and redirects to UI if already provisioned,
// or returns HTML with button to provision.
// @Id HandleNamespacePath
// @Summary Handles wildcard paths to namespace related components.
// @Router /-/{namespace_id}/{action} [get]
// @Param namespace_id path string true "Namespace ID"
// @Param action path string false "Action URL part"
// @Deprecated
func HandlePath(ctx *gin.Context) {
	var params RoutingParams
	if err := ctx.ShouldBindUri(&params); err != nil {
		negotiateError(ctx, 400, "invalid requested URL")
		return
	}

	g := GetGitLabService(ctx)
	user, err := g.CurrentUser()
	if err != nil {
		HandleError(ctx, err)
		return
	}
	// TODO: remove when we have enough test coverage. For now, be extra careful
	// and validate we have valid user struct.
	if user == nil {
		ctx.AbortWithError(http.StatusUnauthorized, fmt.Errorf("invalid gitlab user"))
		return
	}
	// check if valid namespace
	namespace, err := g.GetNamespace(params.ID)
	if errors.Is(err, ErrUnauthorized) {
		HandleError(ctx, err)
		return
	}

	canAccess, membership, err := g.CanAccessNamespace(params.ID)
	if err != nil {
		ctx.AbortWithError(404, fmt.Errorf("access namespace: %w", err))
		return
	}
	log.WithFields(log.Fields{
		"id":         params.ID,
		"user":       user,
		"namespace":  namespace,
		"canAccess":  canAccess,
		"membership": membership,
	}).Debugf("routing_controller#canAccessNamespace")

	if !canAccess {
		ctx.AbortWithStatus(404)
		log.Infof("user %d attempting to access namespace %s without membership", user.ID, params.ID)
		return
	}

	c := GetConfig(ctx)

	selector := client.ObjectKey{
		Name:      fmt.Sprint(namespace.ID),
		Namespace: c.Namespace,
	}
	current := opstracev1alpha1.GitLabNamespace{
		ObjectMeta: metav1.ObjectMeta{
			Name:      fmt.Sprint(namespace.ID),
			Namespace: c.Namespace,
		},
	}

	// Check if exists
	err = c.K8sClient.Get(ctx, selector, &current)
	if err != nil {
		if k8serrors.IsNotFound(err) {
			ctx.HTML(200, "confirm.gohtml", gin.H{
				"namespaceFullPath": namespace.FullPath,
				"provisionEndpoint": fmt.Sprintf("/v1/provision/%d%s", namespace.ID, params.Action),
				"isOwner":           membership == gitlab.OwnerPermissions,
			})
			return
		}
		ctx.AbortWithError(500, fmt.Errorf("read GitLabNamespace: %w", err))
		return
	}

	// Good to go
	queryParams := ctx.Request.URL.Query()
	queryParams.Set("groupId", fmt.Sprintf("%d", current.Spec.ID))
	ctx.Redirect(302, fmt.Sprintf("/%d%s?%s", current.Spec.TopLevelNamespaceID, params.Action, queryParams.Encode()))
}
