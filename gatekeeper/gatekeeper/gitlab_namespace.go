package gatekeeper

import (
	"strings"

	gitlab "github.com/xanzy/go-gitlab"
)

const (
	UserNamespaceType  = "user"
	GroupNamespaceType = "group"
)

type GitLabNamespace struct {
	*gitlab.Namespace
}

// IsTopLevel returns true if this namespace is a top-level namespace.
// Returns false if this is a child namespace.
func (n *GitLabNamespace) IsTopLevel() bool {
	return n.ParentID == 0
}

// GetTopLevel returns top-level parent namespace for this child namespace.
func (n *GitLabNamespace) GetTopLevelNamespaceID() string {
	return strings.Split(n.FullPath, "/")[0]
}

// GetTopLevel returns top-level parent namespace for this child namespace.
func (n *GitLabNamespace) Equals(other *GitLabNamespace) bool {
	return n.ID == other.ID
}

type GitLabGroup struct {
	*gitlab.Group
}

// IsTopLevel returns true if this namespace is a top-level namespace.
// Returns false if this is a child namespace.
func (n *GitLabGroup) IsTopLevel() bool {
	return n.ParentID == 0
}

// GetTopLevel returns top-level parent namespace for this child namespace.
func (n *GitLabGroup) GetTopLevelNamespaceID() string {
	return strings.Split(n.FullPath, "/")[0]
}

// GetTopLevel returns top-level parent namespace for this child namespace.
func (n *GitLabGroup) Equals(other *GitLabGroup) bool {
	return n.ID == other.ID
}
